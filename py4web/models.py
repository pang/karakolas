"""
This file defines the database models
"""

from .common import db, Field
from pydal.validators import *

from .db_models import *

### Define your table below

db.define_table('auth_membership', Field('user_id'), Field('group_id'))
db.define_table('auth_group', Field('role'), Field('description'))

#
# db.define_table('thing', Field('name'))
#
## always commit your models to avoid problems later
#
# db.commit()
#

