"""
This file defines actions, i.e. functions the URLs are mapped into
The @action(path) decorator exposed the function at URL:

    http://127.0.0.1:8000/{app_name}/{path}

If app_name == '_default' then simply

    http://127.0.0.1:8000/{path}

If path == 'index' it can be omitted:

    http://127.0.0.1:8000/

The path follows the bottlepy syntax.

@action.uses('generic.html')  indicates that the action uses the generic.html template
@action.uses(session)         indicates that the action uses the session
@action.uses(db)              indicates that the action uses the db
@action.uses(T)               indicates that the action uses the i18n & pluralization
@action.uses(auth.user)       indicates that the action requires a logged in user
@action.uses(auth)            indicates that the action requires the auth object

session, db, T, auth, and tempates are examples of Fixtures.
Warning: Fixtures MUST be declared with @action.uses({fixtures}) else your app will result in undefined behavior
"""

from py4web import action, request, response, HTTP, abort, redirect, URL
from yatl.helpers import A
from .common import db, session, T, cache, auth, logger, authenticated, unauthenticated, flash, cors
import os, time
from pydal.restapi import RestAPI, Policy
from .utils import isAdmin

from .modules.authentication import generate_jwt, authentication

@action("my_endpoint", method=["GET", "POST", "OPTIONS"])
@action.uses(cors)
def my_endpoint():
    # tu lógica del endpoint
    return dict(message="Hello, CORS!")

@action('/test', method=['GET', 'OPTIONS'])
@action.uses(cors)
def test():
    response.status = 204
    registros = db().select(db.auth_user.id)
    
    for registro in registros:
        print(registro)

    return dict(registros=registros)
    # db.commit()

# Endpoint de prueba para ver si el usuario está autenticado
@action("check_auth", method=['GET', 'OPTIONS'])
# @authentication
@action.uses(cors)
def auth_checking(user_id=None):
    if not user_id:
        return dict(error=f"User not logged in")
    return dict(message=f"Logged")

@action('api/login', method=['POST', 'OPTIONS'])
@action.uses(cors, db)
def login():
    username = request.json.get('username')
    password = request.json.get('password')

    user = db(db.auth_user.username == username).select().first()
    if user and (db.auth_user.password.requires.validate(password) == user.password):
        token = generate_jwt(user.id)
        response.headers['Set-Cookie'] = f'jwt={token}; HttpOnly; Path=/'
        groups = db((db.personaXgrupo.persona == user.id) & (db.personaXgrupo.grupo == db.grupo.id )).select(db.grupo.id, db.grupo.nombre)
        for group in groups:
            group["isAdmin"] = isAdmin(user.id, group.id)
        return dict( 
            user = dict(
                username=user.username,
                user_id=user.id,
                groups=groups
            ))
    else:
        response.status = 401
        return dict(error='Invalid credentials')
    
@action('api/logout', method=['POST', 'OPTIONS'])
@action.uses(db, cors)
def logout():
    response.headers['Set-Cookie'] = f'jwt=; HttpOnly; Path=/'
    return dict(message='Logged out')


@action('get_user', method=['GET', 'OPTIONS'])
# @authentication
@action.uses(cors, db)
def get_user(user_id=None):
    if not user_id:
        return dict(error=f"User not logged in")
    user = db(db.auth_user.id == user_id).select().first()
    groups = db((db.personaXgrupo.persona == user_id) & (db.personaXgrupo.grupo == db.grupo.id )).select(db.grupo.id, db.grupo.nombre)
    return dict( 
        user = dict(
            username=user.username,
            user_id=user.id,
            groups=groups
        ))

@action('api/records/<g_id>', method=['GET'])
# @authentication
@action.uses(db, cors)
def records(g_id, user_id = None):
    if not user_id:
        return dict(error=f"User not logged in")
    #Comprobar que el usuario pertenece a ese grupo
    group = db((db.personaXgrupo.persona == user_id) & (db.personaXgrupo.grupo == g_id )).select().first()
    if group:
        #Recoger todos los pedidos de ese grupo y devolverlos
        records = db((db.proxy_pedido.grupo == g_id) & (db.proxy_pedido.pedido == db.pedido.id) & (db.pedido.productor == db.productor.id)).select(
            db.pedido.ALL,
            db.productor.nombre
        )
        # Si se añade a la query el "estado=abierto" se enviarán solo los que se encuentren abiertos
        record_state=request.query.get('estado')
        if record_state:
            if record_state == 'abierto':
                open_records = [row for row in records if row.pedido.abierto == True]
                return dict(pedidos=open_records)
        # Si no se añade el filtro, se muestran todos
        return dict(pedidos=records)
    else:
        return dict(error=f"User has no access to this group")

@action('api/orders/recent', method=['GET'])
@authentication
@action.uses(db, cors)
def recent_orders( user_id = None):
    
    print(f"{request.method} :: {request.url}")
    # print(request)
    # print(request.params)
    
    # group id is passed along with query params
    g_id = request.params.group_id
    print(f"g_id : { g_id}") # debug
    
    if not user_id:
        print(f"User not logged in")
        # user_id=1315 # bilbo # TODO get from authgrupo
        # return dict(error=f"User not logged in")
    
    # Check user belongs to given group
    group = db((db.personaXgrupo.persona == user_id) & (db.personaXgrupo.grupo == g_id )).select().first()
    if group:
        #Recoger todos los pedidos de ese grupo y devolverlos
        records = db(
            (db.proxy_pedido.grupo == g_id) & 
            (db.proxy_pedido.pedido == db.pedido.id) &
            (db.pedido.productor == db.productor.id)
            # & (db.pedido.abierto == True)
        ).select(
            db.pedido.ALL,
            db.productor.id, db.productor.nombre,
            limitby=(0,10) # Only N records
        )
        # Si se añade a la query el "estado=abierto" se enviarán solo los que se encuentren abiertos
        record_state=request.query.get('estado')
        if record_state:
            if record_state == 'abierto':
                open_records = [row for row in records if row.pedido.abierto == True]
                data = open_records
        # Si no se añade el filtro, se muestran todos
        else:
            data = records
        
        print(f"returning {len(data)} orders")
        return dict(pedidos=data)
    else:
        response.status = 403 # Forbidden
        return dict(error=f"User {user_id} has no access to this group")


        
@action('api/producers/<g_id>', method=['GET'])
# @authentication
@action.uses(db, cors)
def get_producers(g_id, user_id = None):
    if not user_id:
        print(f"GET [/producers] User not logged in")
        # return dict(error=f"User not logged in")
        user_id = 1317 # Mock Gandalf TODO-WIP
    #Comprobar que el usuario pertenece a ese grupo
    group = db((db.personaXgrupo.persona == user_id) & (db.personaXgrupo.grupo == g_id )).select().first()
    if group:
        #Recoger todos los productores de ese grupo y devolverlos
        producers = db((db.proxy_productor.grupo == g_id) & (db.proxy_productor.productor == db.productor.id)).select(
            db.productor.ALL,
        )
        return dict(productores=producers)
    else:
        return dict(error=f"User has no access to this group")


@action('/newuser', method=['GET'])
def test():
    response.status = 201
    somepass="sha512$a5c2e68c57337a2d$4f8c3dabb034f839d7ff88e4b25d26dc133a06d1e5eff9478d70b352f54927e6ab24214d10e472af06a830132978997abf03b902b39086f45b25733698c3df2f"
    nuevo_usuario_id = db.auth_user.insert(
            username='nuevo_usuarios',
            email='nuevo_usuarios@example.com',
            password=somepass,
            first_name='Nombrse',
            last_name='Apellidso',
        )
    db.commit()
    return "Usuario insertado correctamente con ID: %s" % nuevo_usuario_id

@action('/api/<tablename>', method=['GET', 'POST', 'OPTIONS'])
@action('/api/<tablename>/<rec_id>', method=['GET','PUT', 'DELETE', 'OPTIONS'])
# @authentication
@action.uses(db, cors, session)
def api(tablename, rec_id=None, user_id=None):
    if request.method != 'GET':
        if 'user' in session:
            user = db(db.auth_user.id == session.user).select().first()
            return dict(success=True, message=f"Logged in as {user.username}")
        else:
            return dict(success=True, message="Not logged in")

    # policy definitions
    policy = Policy()
    policy.set('*', 'GET', authorize=True, allowed_patterns=['*'])
    policy.set('*', 'PUT', authorize=False)
    policy.set('*', 'POST', authorize=True)
    policy.set('*', 'DELETE', authorize=True)

    return RestAPI(db, policy)(request.method,
                            tablename,
                            rec_id,
                            request.GET,
                            request.POST
                            )

""" Sample /users endpoint 
- just fetch id, email and first_name
- from auth_user table
"""

@action('/users', method=['GET', 'POST', 'OPTIONS'])
@action('/users/<user_id>', method=['PUT', 'DELETE'])
@action.uses(db, cors)
def users(user_id=None):

    mandatory_fields = ['username', 'email', 'password', 'first_name', 'last_name']

    if request.method == 'GET':
        
        rows = db().select(db.auth_user.id, db.auth_user.first_name, db.auth_user.email)
        print (f"GET [/users] endpoint :: retrieved [{len(rows)}] users")
        response.status = 200
        return dict(rows=rows)
    
    elif request.method == 'POST':

        data = request.json
        for field in mandatory_fields:
            if field not in data:
                response.status = 400
                return dict(success=False, message=f'Missing mandatory field: {field}')
        
        username = data['username']
        email=data['email']
        password = db.auth_user.password.requires.validate(data['password'])
        first_name = data['first_name']
        last_name = data['last_name']

        if db(db.auth_user.username == username).select().first():
            return dict(success=False, message=f'Unable to create user with username {username}. It already exists')
        elif db(db.auth_user.email == email).select().first():
            return dict(success=False, message=f'Unable to create user with email {email}. It already exists')
        db.auth_user.insert(
            username=username,
            email=email,
            password=password,
            first_name=first_name,
            last_name=last_name,
        )
        print(f"POST [/users] endpoint :: created new user")
        db.commit()
        response.status = 201
        return dict(success=True, message="User created")
    
    elif request.method == 'PUT':
        data = request.json
        for field in mandatory_fields:
            if field not in data:
                return dict(success=False, message=f'Missing mandatory field: {field}')

        username = data['username']
        email=data['email']
        password = db.auth_user.password.requires.validate(data['password'])
        first_name = data['first_name']
        last_name = data['last_name']

        db.auth_user.update_or_insert((db.auth_user.id == user_id),
            id=user_id,
            username=username,
            email=email,
            password=password,
            first_name=first_name,
            last_name=last_name,
        )
        db.commit()
        print(f"PUT [/users/{user_id}] endpoint :: modified user with id {user_id}")


    elif request.method == 'DELETE':
        db(db.auth_user.id == user_id).delete()
        db.commit()
        print(f"DELETE [/users/{user_id}] endpoint :: deleted user with id {user_id}")
    
""" Sample /pedidos endpoint 
- just fetch id, id_productor and fecha_activacion
- from table 'pedido'
"""
@action('/pedidos', method=['GET', 'POST', 'OPTIONS'])
@action('/pedidos/<p_id>', method=['GET', 'PUT', 'DELETE', 'OPTIONS'])
@action.uses(db, cors)
def pedidos(p_id=None):
    
    print (request)
    
    # mandatory_fields = ['id_productor', 'fecha_activacion']
    mandatory_fields = []

    if request.method == 'GET':
        
        if p_id:
            # Specific record
            print (f"GET [/pedidos/{p_id}]") 
            rows = db(
              (db.pedido.id == p_id)
              & (db.pedido.productor == db.productor.id)
            ).select(
                db.pedido.id, 
                db.pedido.info_cierre, 
                db.pedido.fecha_reparto, 
                db.pedido.fecha_activacion, 
                db.pedido.productor,
                # db.productor.nombre # TODO - handle different JSON 
            )
            print (f"GET [/pedidos/{p_id}]  :: retrieved [{len(rows)}] pedidos")
        else:
            # All of it
            max_records = 10 # top up - TODO - option
            time.sleep(1)
            print (f"GET [/pedidos] - retrieve all data") 
            # rows = db().select(db.pedido.id, db.pedido.fecha_activacion, db.pedido.productor).first(10)
            rows = db().select(
                db.pedido.ALL,
                # db.pedido.id, db.pedido.fecha_activacion, db.pedido.productor, # Specific fields
                orderby=~db.pedido.id,
                limitby=(0,10)
            )
            print (f"GET [/pedidos]  :: retrieved [{len(rows)}] pedidos")
            
        # GET Field mapping
        for r in rows:
            # print(f"mapping {r} : {r.productor}") # debug
            r['id_productor']=r.pop('productor')
        
        # Print last
        if len(rows) > 0 :
            print(f"Last element : {rows[len(rows)-1]}")
        
        # Response
        response.status = 200
        return dict(success=True, message=f"Retrieved {len(rows)} records", data=rows)
    
    elif request.method == 'POST':

        data = request.json
        for field in mandatory_fields:
            if field not in data:
                response.status = 400
                return dict(success=False, message=f'Missing mandatory field: {field}')
        
        # Check if already existing (?)
        
        # Field mapping
        id_productor = data['id_productor']
        fecha_activacion = data['fecha_activacion']

        # Actual insert
        new_pedido = db.pedido.insert(
            productor=id_productor,
            fecha_activacion=fecha_activacion,
        )
        
        # TODO - Return whole object (?)
        print(f"POST [/pedidos] record [{new_pedido.id}] created")
        db.commit()
        # time.sleep(2)
        response.status = 201
        return dict(success=True, message=f"record {new_pedido} created", id=new_pedido.id, data=new_pedido)
    
    elif request.method == 'PUT':
        
        data = request.json
        
        # Validate data
        for field in mandatory_fields:
            if field not in data:
                response.status = 400
                return dict(success=False, message=f'Missing mandatory field: {field}')

        # Massage data (id_productor)
        if 'id_productor' in data:
            data['productor']=data.pop('id_productor')
                
        if 'info_cierre' in data:
            print(f"massaging info_cierre")
            data['info_cierre'] = str(data.pop('info_cierre'))
                
                
        # Check existing
        pedido = db(db.pedido.id == p_id).select().first()
        if pedido:
            print(f"PUT :: pedido [{p_id}] is exists; will update with data:")
            print(f"{data}")
            action="updated"
            response.status=200
        else: 
            print(f"pedido [{p_id}] not found; will insert")
            action="inserted"
            response.status=201
        
        db.pedido.update_or_insert((db.pedido.id == p_id),
            id=p_id,
            **data
        )
        db.commit()
        # Fetch updated data
        new_data = pedido = db(db.pedido.id == p_id).select().first()
        # print(f"New data: ")
        # print(new_data)
        print(f"PUT [/pedidos/{p_id}] endpoint :: record {action}")
        # return dict(success=True, message=f"Pedido {p_id} {action}")
        return dict(success=True, message=f"Pedido {p_id} {action}", data=new_data)

    elif request.method == 'DELETE':
        
        pedido = db(db.pedido.id == p_id).select().first()
        if pedido:
            db(db.pedido.id == p_id).delete()
            db.commit()
            # if returning 204 - connection is not closed..?
            response.status=200
            print(f"DELETE [/pedidos/{p_id}] endpoint :: deleted pedido with id {p_id}")
            time.sleep(3)
            print(f"return")
            return dict(success=True, message=f"Pedido {p_id} deleted")
        else:
            print(f"DELETE [/pedidos/{p_id}] :: [{p_id}] not found")
            response.status=200
            return dict(success=True, message=f"Pedido [{p_id}] not found")
    
    
@action('/index')
def catch_all(path=None):
    print("default page being served")
    # Construct an absolute path to the React index.html file.
    APP_FOLDER = os.path.dirname(__file__)
    file_path = os.path.join(APP_FOLDER, 'static', 'build', 'index.html')

    # Ensure the file exists
    if not os.path.isfile(file_path):
        # Handle the error appropriately (e.g., return a 404 page)
        return 'File not found', 404

    with open(file_path, 'rb') as f:
        response.headers['Content-Type'] = 'text/html'
        return f.read()


        
""" Sample /devtest endpoint 
- Used to play around with backend responses
"""
@action('/devtest', method=['GET', 'OPTIONS'])
@action.uses(cors)
def test_endpt():
    
    # Whatever we need to test
    response.status = 404
    
    print (f"GET [/test] endpoint")
        
    return response

""" Sample /error endpoint 
- Always returns a 500
"""
@action('/error', method=['GET', 'OPTIONS'])
@action.uses(cors)
def test_endpt():
    
    # Whatever we need to test
    response.status = 500
    # response.body = dict(message="This endpoint always returns error")
    response.body = "This endpoint always returns error"
    
    print (f"GET [/error] endpoint : returning 500")
        
    return response


# @action('index')
# @action('react-app/<path:path>', method=['GET'])
# def catch_all(path=None):
#     # Path to your React app's build directory
#     app_build_folder = os.path.join(os.path.dirname(__file__), 'static', 'build')

#     # Serve static files (e.g., CSS, JS, images)
#     if path is not None and "." in path:  # Checks if the path is likely a file
#         file_path = os.path.join(app_build_folder, path)
#         if os.path.isfile(file_path):
#             # Infer the content type (e.g., text/css, application/javascript)
#             content_type, _ = mimetypes.guess_type(file_path)
#             if content_type:
#                 response.headers['Content-Type'] = content_type
#                 print(f"serving {content_type}")
#             return open(file_path, 'rb').read()

#     # Serve index.html for any other path
#     index_file_path = os.path.join(app_build_folder, 'index.html')
#     if os.path.isfile(index_file_path):
#         print("serving index.html")
#         response.headers['Content-Type'] = 'text/html'
#         return open(index_file_path, 'rb').read()

#     return 'Not Found', 404
