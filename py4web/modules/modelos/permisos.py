# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################

from gluon import current
from modelos.grupos import grupos_y_subredes_de_la_red

### Utiles para permisos de acceso  ###

##función que comprueba da permisos a los admin de los grupos que pertenecen a una red
def admin_del_grupo_de_la_red(ired):
    #grupos_y_subredes_de_la_red incluye las redes intermedias y los grupos finales
    # que descienden de esta red, pero no las redes ascendientes que les ofrecen productos
    for igrupo in grupos_y_subredes_de_la_red(ired):
        if current.auth.has_membership('admins_%s'%igrupo):
            return True
    return False

def admin_o_miembro_de_un_grupo_de_la_red(ired):
    #grupos_y_subredes_de_la_red incluye las redes intermedias y los grupos finales
    # que descienden de esta red, pero no las redes ascendientes que les ofrecen productos
    for igrupo in grupos_y_subredes_de_la_red(ired):
        if (current.auth.has_membership('admins_%s'%igrupo) or
            current.auth.has_membership('miembros_%s'%igrupo)):
            return True
    return False

def grupo_del_productor(productor):
    try:
        grupo      = current.db.productor(productor).grupo
        return 'admins_%s'%grupo
    except:
        return 'webadmins'

def tiene_acceso_al_grupo(productor):
    db = current.db
    try:
        grupo      = db.productor(productor).grupo
        def miembro_o_admin(grupo):
            return (current.auth.has_membership(role='admins_%s'%grupo) or
                    current.auth.has_membership(role='miembros_%s'%grupo))
        return (miembro_o_admin(grupo) or
                current.auth.has_membership(role='webadmins') or
                any(miembro_o_admin(row.grupo)
                    for row in db((db.grupoXred.red==grupo)  &
                                  (db.grupoXred.activo==True))\
                               .select(db.grupoXred.grupo)))
    except:
        return current.auth.has_membership(role='webadmins')

def puede_leer_productos(iproductor, auth_code):
    try:
        productor = current.db.productor(iproductor)
        grupo     = productor.grupo
        random_string = productor.random_string
        return (current.auth.has_membership(role='admins_%s'%grupo) or
                 current.auth.has_membership(role='miembros_%s'%grupo) or
                 current.auth.has_membership(role='webadmins') or
                 random_string==auth_code)
    except:
        return current.auth.has_membership(role='webadmins')

def autoriza_pedido(vars, perms = 'rwa'):
    '''...

    perms pueden ser *r*ead, *w*rite, *a*dmin, *s*uper (para no permitir el acceso a los grupos de la red)

    En este momento (enero 2021) no estamos usando todas las opciones, solo 'r' y 'rwa'
    '''
    v    = current.request.vars
    db   = current.db
    auth = current.auth
    try:
        ipedido = int(v.pedido)
        grupo = v.get('grupo')
        if grupo:
            igrupo = int(grupo)
            ired  = db.pedido(ipedido).productor.grupo.id
            if ired != igrupo and (not igrupo in grupos_y_subredes_de_la_red(ired)
                                   or 's' in perms):
                return False
        else:
            igrupo  = db.pedido(ipedido).productor.grupo.id
    except:
        return False
    result = (auth.has_membership(role='admins_%s'%igrupo) or
              auth.has_membership(role='webadmins') )
    if perms=='r':
        result = result or auth.has_membership(role='miembros_%s'%igrupo)
    return result

#def grupo_para_pedir(pedido):
#    try:
#        productor = current.db.pedido(pedido).productor
#        grupo     = current.db.productor(productor).grupo
#        return 'miembros_%s'%grupo
#    except:
#        return 'webadmins'

def autoriza_pedido_coordinado(pedido, grupo, perms = 'r'):
    '''Comprueba permisos para acceder a un proxy_pedido, con permisos de lectura o escritura
    '''
    db = current.db
    auth = current.auth
    if not pedido or not grupo:
        return False
    ipedido, igrupo = int(pedido), int(grupo)
    pp = db.proxy_pedido(pedido=ipedido, grupo=igrupo)
    if not pp:
        p = db.pedido(ipedido)
        if not p:
            return False
        ired = p.productor.grupo.id
        if ired != igrupo:
            return False
        grupo = str(ired)
    es_admin = (auth.has_membership(role='admins_%s'%grupo) or
                auth.has_membership(role='webadmins') )
    if 'w' in perms:
        return es_admin
    return es_admin or auth.has_membership(role='miembros_%s'%grupo)

def grupo_del_pedido(pedido):
    db = current.db
    try:
        grupo = db.pedido(pedido).productor.grupo
        return 'admins_%s'%grupo
    except:
        return 'webadmins'

#def puede_pedir(pedido):
#    db = current.db
#    try:
#        ipersona  = current.auth.user.id
#        productor = db.pedido(pedido).productor
#        grupop    = db.productor(productor).grupo
#        return esta_en_red(ipersona, grupop)
#    except:
#        return current.auth.has_membership('webadmins')

def puede_gestionar_plantilla(plantilla):
    plantilla = current.db.plantilla_pedidos(plantilla)
    return plantilla and current.auth.has_membership(role='admins_%s'%plantilla.grupo)
