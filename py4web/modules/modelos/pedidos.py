# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################

import os
from datetime import date, timedelta
from decimal import Decimal
from collections import defaultdict

from gluon import current
from gluon.html import TABLE, TR, TD, URL, THEAD, TBODY, TH
from gluon.http import redirect

from kutils import enum, pprint_p, pprint_c
from utils.db import copia_datos

## Pedidos ###

# TiposPedido depende tanto del pedido como del grupo
# El mismo pedido puede ser de tipo RED para la red que lo lanza, SUBRED para las redes intermedias
# COORDINADO para los grupos finales
# El TiposPedido de un (pedido, grupo) no cambia a lo largo de la vida del pedido
TiposPedido = enum('RED', 'COORDINADO', 'PROPIO', 'SUBRED')

# El EstadosPedido, sin embargo, no depende del grupo, y cambia a lo largo del ciclo de vida del pedido
# Los ciclos típicos son
#  abierto -> cerrado -> histórico
#  abierto -> pendiente -> histórico (si había productos a pesar)
#  abierto -> cerrado -> pendiente -> histórico (si se reportaron incidencias a posteriori)
# pero puede haber otros, si por ejemplo un pedido se cierra y luego se reabre para otra fecha
EstadosPedido = enum(
    ABIERTOS='abiertos',
    CERRADOS='cerrados',
    HISTORICOS='historicos',
    ACTIVOS='activos', 
    PENDIENTES='pendientes',
    PENDIENTES_RECIENTES='pendientes recientes',
    RECIENTES='recientes'
)
# ¿Sería útil un enum EstadosDiscrepancia? :-/
TiposDiscrepancia = enum(
    INTERNA='discrepancia_interna',
    INTERNA_ASUMIDA='discrepancia_interna_asumida',
    ASCENDIENTE='discrepancia_ascendiente',
    PRODUCTOR='discrepancia_productor',
    DESCENDIENTE='discrepancia_descendiente',
    PENDIENTE_CONSOLIDACION='pendiente_consolidacion'
)

TablasItem = enum('PETICION', 'ITEM', 'ITEM_GRUPO')

def tipo_pedido(pedido, grupo):
    '''
    Indica el TiposPedido de un pedido, para un grupo
    '''
    db = current.db
    if isinstance(pedido, int):
        pedido = db.pedido(pedido)
    if isinstance(grupo, int):
        igrupo = grupo
        grupo = db.grupo(grupo)
    else:
        igrupo = grupo.id
    if pedido.productor.grupo==igrupo and grupo.red:
        return TiposPedido.RED
    elif pedido.productor.grupo==igrupo:
        return TiposPedido.PROPIO
    elif grupo.red:
        return TiposPedido.SUBRED
    else:
        return TiposPedido.COORDINADO

### Textos descriptivos

#No podemos llamar a T en el cuerpo del módulo
def str_discrepancias():
    T = current.T
    return enum(
        discrepancia_descendiente = T('Hay discrepancias entre las cantidades repartidas al nivel inferior y lo que estos grupos o redes declaran haber recibido. '),
        discrepancia_interna_asumida = T('El grupo asumió la discrepancia entre la cantidad recibida de %s y la repartida. '),
        discrepancia_interna = T('Hay discrepancias entre la cantidad recibida de %s y la repartida. '),
        discrepancia_ascendiente = T('Hay discrepancias entre la cantidad enviada por %s y la recibida por %s. '),
        pendiente_consolidacion_desdered = T('Hay productos que es necesario pesar, y quedan grupos que todavía no han introducido esos pesos')
    )

#No podemos llamar a T en el cuerpo del módulo
def explicaciones_discrepancias():
    '''Explica en qué consiste cada tipo de discrepancia sin usar los nombres del productor, subredes o redes,
    pero usando el nombre del grupo que ayuda a entender mejor las explicaciones

    Se devuelve como un dict donde las keys son TiposDiscrepancia y los valores son lazyT T(' ... ')
    '''
    T = current.T
    return dict(
        discrepancia_descendiente = T('Algunos de los grupos y subredes que han recibido este pedido han declarado haber recibido una cantidad de algún producto diferente a la que %s declara haberles repartido. '),
        discrepancia_interna = T('%s ha recibido una cantidad de algún producto diferente de la que ha repartido. '),
        discrepancia_interna_asumida = T('%s pagará al productor la cantidad que ha recibido, aunque sea distinta de la que se ha repartido. '),
        discrepancia_ascendiente = T('La red declara haber enviado una cantidad de algún producto diferente a la que %s declara haber recibido. '),
        discrepancia_productor = T('El productor declara haber enviado una cantidad de algún producto diferente a la que %s declara haber recibido. '),
        pendiente_consolidacion = T('Hay productos que %s debe pesar, y reportar los pesos en karakolas. ')
    )

#No podemos llamar a T en el cuerpo del módulo
def str_estados_pedido(plural=False):
    T = current.T
    if plural:
        return {'abiertos':T('Pedidos abiertos'),
                'cerrados':T('Pedidos cerrados'),
                'historicos':T('Pedidos históricos'),
                'activos':T('Pedidos activos'),
                'pendientes':T('Pedidos pendientes de consolidar'),
#                    pendientes_recientes=T('Pedidos recientes pendientes de consolidar'),
                'activos recientes':T('Pedidos activos recientes')}
    else:
        return {'abiertos':T('abierto'),
                'cerrados':T('cerrado'),
                'historicos':T('histórico'),
                'activos':T('activo'),
                'pendientes':T('pendiente'),
#                    pendientes_recientes=T('pendiente reciente'),
                'activos recientes':T('activo reciente')}

## Nuevo pedido / Pedido fantasma ##

class HayPedidoParaEsaFecha(Exception):
    pass

def nuevo_pedido(iproductor, fecha_reparto, data=None):
    db = current.db
    tp = db.productor
    data = data or {}
    d = dict(productor=iproductor, fecha_reparto=fecha_reparto)
    if db.pedido(**d):
        raise HayPedidoParaEsaFecha(iproductor, fecha_reparto)
    d['abierto'] = d.get('abierto',True)
    d.update(data)
    d.update( db(tp.id==iproductor).select(
        *[tp[f] for f in ['formula_precio_final', 'formula_precio_productor',
                          'formula_coste_extra_pedido', 'formula_coste_extra_productor']
                if f not in data], limitby=(0,1)
    ).first() )

    return db.pedido.insert(**d)

def crea_pedido_fantasma(iproductor):
    r = current.db.pedido(productor=iproductor, fecha_reparto=current.C.FECHA_FANTASMA)
    if r:
        return r.id
    else:
        return nuevo_pedido(iproductor, current.C.FECHA_FANTASMA, dict(abierto=False))

def limpia_pedido(pedido):
    '''Por ahora el único uso que tiene es limpiar el pedido fantasma antes de volcarle
    datos de otro pedido. Mantiene el pedido, pero elimina los productos, peticiones, etc

    Elimina todas las peticiones asociadas al pedido.
    '''
    db = current.db
    ipedido = pedido.id
    #no es necesario eliminar los peticion, item, item_grupo, valor_extra_XXX_pedido,
    #se borran ON_CASCADE al eliminar las columnas
    db(db.productoXpedido.pedido==ipedido).delete()
    db(db.campo_extra_pedido.pedido==ipedido).delete()
    db(db.columna_extra_pedido.pedido==ipedido).delete()

def _mueve_productos(pedido1, pedido2, lista_columnas=None):
    '''Mueve los productoXpedido del ipedido1 al ipedido2
    No comprueba que ipedido2 esté vacío antes!

    Usa el argumento adicional lista_columnas para reusar columnas_existentes
    '''
    db = current.db
    ipedido1 = pedido1.id
    ipedido2 = pedido2.id
    tppp = db.productoXpedido
    tcamep = db.campo_extra_pedido
    tcolep = db.columna_extra_pedido

    copia_datos(tppp, {tppp.pedido:ipedido1}, {tppp.pedido:ipedido2})

    copia_datos(tcamep, {tcamep.pedido:ipedido1}, {tcamep.pedido:ipedido2})
    if not lista_columnas:
        copia_datos(tcolep, {tcolep.pedido:ipedido1}, {tcolep.pedido:ipedido2})

        columnas_antiguas = tcolep.with_alias('columnas_antiguas')
        columnas_nuevas = tcolep.with_alias('columnas_nuevas')
        lista_columnas = [(row['columnas_antiguas'].id,
                           row['columnas_nuevas'].id,
                           row['columnas_antiguas'].tipo)
            for row in db((columnas_antiguas.pedido==ipedido1) &
                          (columnas_nuevas.pedido==ipedido2) &
                          (columnas_antiguas.nombre==columnas_nuevas.nombre) ).select(
                      columnas_antiguas.id, columnas_nuevas.id, columnas_antiguas.tipo
        )]

    for id_columna_antigua, id_columna_nueva, tipo in lista_columnas:
        tabla = db.TABLAS_VALORES_EXTRA_PEDIDO[tipo]
        copia_datos(tabla, {tabla.columna:id_columna_antigua},
                           {tabla.columna:id_columna_nueva})
        #sql a pelo para poner el valor correcto de tabla.productoXpedido!
        sql = '''UPDATE %(nombre_tabla)s AS t,
                        productoXpedido AS ppp
                    SET t.productoXpedido=ppp.id
                    WHERE (t.columna=%(id_columna_nueva)d) AND
                          (ppp.producto=t.producto) AND
                          (ppp.pedido=%(ipedido)d);'''%dict(
            nombre_tabla=tabla.sql_shortref,
            id_columna_nueva=id_columna_nueva,
            ipedido=ipedido2
        )
        db.executesql(sql)
        # #Eliminamos los valor_extra_XXX_pedido que no tengan completo el campo productoXpedido
        # #Es igual a None si hay productos inactivos, porque existe el valor_extra_XXX_pedido
        # #pero no el productoXpedido. Es más fácil copiar todo y luego borrar...
        # db((tabla.columna==id_columna_nueva) & (tabla.productoXpedido==None)).delete()

    pedido2.update_record(
        formula_precio_productor=pedido1.formula_precio_productor,
        formula_precio_final=pedido1.formula_precio_final
    )

def mueve_productos_al_pedido(ipedido):
    '''Define los productoXpedido para un pedido *nuevo*, volcando todos los productos
    desde el pedido fantasma
    '''
    db = current.db
    pedido     = db.pedido[ipedido]
    iproductor = pedido.productor
    productor  = db.productor[iproductor]
    pedidof  = pedido_fantasma(iproductor)
    _mueve_productos(pedidof, pedido)

def elimina_proxy_pedido(ipedido, grupos, solo_desactivar=False):
    from modelos.grupos import grupos_y_subredes_de_la_red
    db = current.db
    if solo_desactivar:
        db( db.proxy_pedido.grupo.belongs(grupos) &
           (db.proxy_pedido.pedido==ipedido) ).update(activo=False)
    else:
        db( db.proxy_pedido.grupo.belongs(grupos) &
           (db.proxy_pedido.pedido==ipedido) ).delete()
    # en cualquier caso, aunque estos proxy_pedido solo se desactiven, no se ofrece el pedido
    # a los descendientes, y se eliminan todas las peticiones
    descendientes = set()
    for igrupo in grupos:
        descendientes.update(grupos_y_subredes_de_la_red(igrupo))
    db( db.proxy_pedido.grupo.belongs(descendientes) &
       (db.proxy_pedido.pedido==ipedido) ).delete()

    # Eliminamos toda la petición, de los grupos y sus descendientes
    descendientes.update(grupos)
    pxps = db(db.productoXpedido.pedido==ipedido)._select(db.productoXpedido.id)
    for tabla in (db.peticion, db.item, db.item_grupo):
        db((tabla.productoXpedido.belongs(pxps)) &
           (tabla.grupo.belongs(descendientes)) ).delete()

def query_mask(igrupo):
    '''Uso: unir con & a otra query

    Por ej: solo los productos del pedido que el grupo ha aceptado

    from kutils import query_mask
    q = (current.db.productoXpedido.pedido==ipedido) & query_mask(igrupo)
    '''
    db = current.db
    return ((db.mascara_producto.productoXpedido==db.productoXpedido.id) &
            (db.mascara_producto.grupo==igrupo) &
            (db.mascara_producto.aceptado==True))

def activa_productos_coordinados(ipedido, igrupo=None, via=None, solo_descendientes=False):
    '''
    Llama a activa_productos_coordinados para igrupo y todos sus descendientes, pero
    de forma ordenada, bajando el árbol de redes, para que se haya actualizado el mascara_producto
    de los ascendientes cuando se llama a activa_productos_coordinados

    Se puede llamar de dos formas:
     - Sin el argumento opcional igrupo, asume que igrupo es la red madre que ofrece el pedido
     - Con el argumento opcional igrupo, que puede ser:
        - un grupo => llama a _activa_productos_coordinados_no_recursivo sin más
        - una subred => primero llama a _activa_productos_coordinados_no_recursivo,
                        luego a activa_productos_coordinados de forma recursiva hasta llegar a todos
                        los grupos
    Si es solo_descendientes=True, no actualiza mascara_producto, pero sí que desciende la cadena
    de grupos aplicando la fórmula, para los grupos que la tienen...
    '''
    db = current.db
    tpp = db.proxy_pedido
    via = via or via_pedido_en_grupo(ipedido, igrupo)
    if igrupo:
        # Creo que no es nec comprobar que proxy_pedido está activo, pero mejor asegurar
        if not tpp(grupo=igrupo, pedido=ipedido, activo=True):
            raise AttributeError('debería haber un proxy_pedido para este pedido y grupo...')
        if not solo_descendientes:
            _activa_productos_coordinados_no_recursivo(ipedido, igrupo, via=via)
        grupo = db.grupo(igrupo)
    #Si igrupo es None, no tenemos que rellenar mascara_producto, pero seguimos de forma recursiva
    else:
        grupo = db.pedido(ipedido).productor.grupo
        igrupo = grupo.id
    if grupo.red:
        for row in db((db.grupoXred.red==igrupo) &
                      (db.grupoXred.activo==True) &
                      (tpp.grupo==db.grupoXred.grupo) &
                      (tpp.pedido==ipedido) &
                      (tpp.activo==True)) \
                   .select(tpp.grupo, tpp.formula_sobreprecio):
            # si la fórmula es 'manual', no es necesario seguir descendiendo...
            # TODO: quizá habría que avisar con un email por si quieren cambiar los precios :-/
            if row.formula_sobreprecio != 'manual':
                activa_productos_coordinados(ipedido, row.grupo, via=igrupo)

def _activa_productos_coordinados_no_recursivo(ipedido, igrupo, via=None):
    '''Actualiza los precios y el campo 'activo' de la mascara_producto.
    Si el pedido es el más actual del productor, guarda la formula para el futuro.
    Si activa productos que haya que pesar, marca el flag pendiente_consolidacion

    Puede lanzar SyntaxError y EvalAritException

    No es recursivo, solo actualiza los mascara_producto de igrupo, no de sus descendientes

    Si la formula es 'manual', no actualiza los precios, pero si el precio_red, que puede haber cambiado
    '''
    from eval_arit import EvalArit

    db = current.db
    tppp = db.productoXpedido

    rows = db((db.mascara_producto.grupo==igrupo) &
              (db.mascara_producto.productoXpedido==tppp.id) &
              (tppp.pedido==ipedido)) \
            .select(tppp.id,
                    db.mascara_producto.id)
    mascaras_existentes = dict((r.productoXpedido.id, r.mascara_producto.id) for r in rows)

    proxy   = db.proxy_pedido(pedido=ipedido, grupo=igrupo)
    pedido  = db.pedido(ipedido)
    formula = proxy.formula_sobreprecio
    estado_proxy_productor = db.proxy_productor(productor=pedido.productor, grupo=igrupo).estado
    aceptado_por_defecto = (estado_proxy_productor in ('avisar_productos', 'auto_productos'))

    ipedido_anterior = db(
        (db.pedido.productor==pedido.productor) &
        (db.pedido.fecha_reparto<pedido.fecha_reparto) &
        (db.proxy_pedido.pedido==db.pedido.id) &
        (db.proxy_pedido.grupo==igrupo) &
        (db.proxy_pedido.activo==True) ).select(
        db.pedido.id,
        orderby=~db.pedido.fecha_reparto,
        limitby=(0,1)).first()

    rows = db((db.mascara_producto.grupo==igrupo) &
              (db.mascara_producto.productoXpedido==tppp.id) &
              (tppp.pedido==ipedido_anterior)) \
            .select(tppp.nombre,
                    db.mascara_producto.aceptado)
    mascaras_anteriores = dict(
        (r.productoXpedido.nombre, r.mascara_producto.aceptado)
    for r in rows)

    #Desactivamos esta parte (que funciona), porque no tenemos tiempo de activarlo
    #en la version javascript, y no es necesario
#    vars_globales = dict((row.nombre, row.valor)
#        for row in db(db.campo_extra_pedido.pedido==ipedido) \
#                  .select(db.campo_extra_pedido.ALL) )
    vars_globales = {}

    #Ahorramos tiempo si no evaluamos la formula cuando es 'precio_red'
    if formula=='precio_red':
        evalua = lambda d: d['precio_red']
    else:
        #puede lanzar SyntaxError
        Evaluador = EvalArit(formula, vars_globales, current.C.PRECISION_DECIMAL)
        evalua = Evaluador.evalua

    columnas_extra = db(db.columna_extra_pedido.pedido==ipedido) \
                    .select(db.columna_extra_pedido.id,
                            db.columna_extra_pedido.tipo,
                            db.columna_extra_pedido.nombre)
    aliases = [(row.id,
                db['valor_extra_%s_pedido'%row.tipo.split('(')[0]] \
                   .with_alias('_' + row.nombre.replace('-','_')))
               for row in columnas_extra]

    fields = [tppp.id, tppp.precio_final, tppp.nombre]
    fields_extra = [calias.valor for cid, calias in aliases]
    fields.extend(fields_extra)

    rows = db((tppp.pedido==ipedido) & (tppp.temporada==True) & (tppp.activo==True)
             ).select(*fields, left=[
            calias.on((calias.productoXpedido==tppp.id) & (calias.columna==cid))
            for cid, calias in aliases])

    if via != pedido.productor.grupo:
        rows_ascendiente = db((db.mascara_producto.grupo==via) &
                              (db.mascara_producto.productoXpedido==tppp.id) &
                              (tppp.pedido==ipedido)) \
                .select(tppp.id,
                        db.mascara_producto.precio)
        precios_ascendiente = {
            r.productoXpedido.id:r.mascara_producto.precio for r in rows_ascendiente
        }
    elif fields_extra:
        #web2py quirk: si hay fields_extra, cada fila tiene campos de dos tablas distintas,
        # y hay que poner el nombre de la tabla seguido del nombre del campo
        precios_ascendiente = {r.productoXpedido.id:r.productoXpedido.precio_final for r in rows}
    else:
        # pero si no hay fields_extra, solo hay campos de una tabla,
        # y no hay que poner el nombre de la tabla
        precios_ascendiente = {r.id:r.precio_final for r in rows}

    for row in rows:
        #Si hay aliases, cada row tiene campos de varias tablas, mientras que si no hay aliases,
        #todos los campos son de productoXpedido, y el DAL de web2py devuelve las row en distinto
        #formato
        row_producto = row['productoXpedido'] if aliases else row
        pid, pnombre = row_producto.id, row_producto.nombre
        #El nombre de la columna extra comienza con '_' por motivos tecnicos
        vars_producto = dict((calias._tablename[1:], row[calias.valor] or Decimal())
                             for _, calias in aliases)
        precio_red = precios_ascendiente[pid]
        vars_producto['precio_red'] = precio_red
        vars_mascara = {'precio_red': precio_red}
        if formula!='manual':
            #aquí puede lanzar EvalAritException
            vars_mascara['precio'] = evalua(vars_producto)
        mascara_id = mascaras_existentes.get(pid, None)
        if mascara_id:
            db.mascara_producto(mascara_id).update_record(**vars_mascara)
        else:
            # si la formula es manual, ya existe mascara_id
            mascara_id_anterior = mascaras_anteriores.get(pnombre, aceptado_por_defecto)
            vars_mascara['productoXpedido'] = pid
            vars_mascara['grupo'] = igrupo
            vars_mascara['aceptado'] = mascara_id_anterior
            db.mascara_producto.insert(**vars_mascara)

    #Si el pedido es el más actual del productor, guarda la formula para el futuro
    #a menos que haya lanzado SyntaxError o EvalAritException
    fecha_max = db.proxy_pedido.fecha_reparto.max()
    fecha_ultimo_pedido = db(
        (db.proxy_pedido.pedido==db.pedido.id) &
        (db.pedido.productor==pedido.productor) &
        (db.proxy_pedido.grupo==igrupo) ) \
        .select(fecha_max).first()[fecha_max]
    if proxy.fecha_reparto >= fecha_ultimo_pedido:
        # Pero si la formula es 'manual', para el proximo pedido comenzaremos con 'precio_red'
        db.proxy_productor(productor=pedido.productor, grupo=igrupo) \
          .update_record(formula_sobreprecio=formula if formula!='manual' else 'precio_red')

def abre_pedidos_en_grupos_de_la_red(pedidos, grupos):
    errores_email = []
    for igrupo in grupos:
        errores_email.extend(abre_pedidos_en_grupos_y_subgrupos(pedidos, igrupo))
    return errores_email

def abre_pedidos_en_grupos_y_subgrupos(pedidos, igrupo, fuerza_activo=False, via=None):
    '''Ofrece cada pedido de la lista a igrupo y sus descendientes
    si una subred de igrupo bloquea el pedido, no seguimos bajando por el grafo
    '''
    from modelos.usuarios import email_blacklist, emails_de_todos_los_admins
    if not pedidos: return []
    from kutils import represent
    db = current.db
    C  = current.C
    T  = current.T

    grupo = db.grupo(igrupo)
    pedido0 = db.pedido[pedidos[0]]
    productor0 = pedido0.productor
    nombre_red = productor0.grupo.nombre
    # Asumimos que todos los pedidos vienen por la misma vía
    # Si no fuera así, bastaría con mover esta línea dentro del bucle
    via = via or via_pedido_en_grupo(pedidos[0], igrupo)

    errores_email = []
    pedidos_abiertos = []
    for ipedido in pedidos:
        pedido    = db.pedido(ipedido)
        fecha_reparto = pedido.fecha_reparto
        productor = pedido.productor
        pp = db.proxy_productor(productor=productor.id, grupo=igrupo)
        estado = pp.estado if pp else 'bloqueado'
        if estado=='bloqueado':
            continue
        pp_activo = (estado in ('auto', 'auto_productos')) or fuerza_activo
        db.proxy_pedido.update_or_insert(
                (db.proxy_pedido.grupo==igrupo) &
                (db.proxy_pedido.pedido==ipedido),
                   grupo=igrupo, pedido=ipedido,
                   fecha_reparto=fecha_reparto,
                   formula_coste_extra_pedido=pp.formula_coste_extra_pedido,
                   formula_sobreprecio=pp.formula_sobreprecio,
                   activo=pp_activo,
                   via=via
                   )
        if pp_activo:
            #Activa todos los productos y calcula sus precios usando las formulas de cada subred
            # Las llamadas se hacen en el orden correcto, descendiendo
            # el árbol de subredes para que mascara_producto de los ascendientes esté relleno antes
            # de rellenar mascara_producto de igrupo
            _activa_productos_coordinados_no_recursivo(ipedido, igrupo, via=via)
            for row in db((db.grupoXred.red==igrupo) & (db.grupoXred.activo==True)) \
                        .select(db.grupoXred.grupo):
                errores_email.extend(abre_pedidos_en_grupos_y_subgrupos([ipedido], row.grupo, via=igrupo))
        pedidos_abiertos.append((pedido, estado))
    # Envia emails a los grupos que tenian algun pedido en 'auto' o 'avisar'
    if pedidos_abiertos:
        if len(pedidos)>1:
            asunto = C.EMAIL_PEDIDOS_COORDINADOS_ASUNTO%nombre_red
            cuerpo = C.EMAIL_PEDIDOS_COORDINADOS%dict(
                    nombre_red=nombre_red,
                    nombre_grupo=grupo.nombre,
                    lista_pedidos=TABLE(
                        THEAD(TR(TH(T('Pedido')), TH(T('Aceptado')),
                                 TH(T('Productos nuevos aceptados')))),
                        TBODY(*[TR(
                         TD(represent(db.pedido,pedido)),
                         TD(T('Sí') if pp_activo else T('No')),
                         TD(T('Sí') if (estado in ('auto_productos', 'avisar_productos'))
                            else T('No')) )
                                for pedido, estado in pedidos_abiertos])
                    ).xml()
            )
        else:
            asunto = C.EMAIL_PEDIDO_COORDINADO_ASUNTO%(productor0.nombre)
            # Si sólo hay un pedido, pp_activo nos dice si se abrió y activó porque estaba en auto
            # o se abrió pero no se ha activado porque estaba en modo 'avisar'
            plantilla = (C.EMAIL_PEDIDO_COORDINADO_AUTO if pp_activo else
                         C.EMAIL_PEDIDO_COORDINADO_AVISAR
            )
            cuerpo = plantilla%dict(nombre_red=nombre_red,
                                    nombre_productor=productor0['nombre'],
                                    nombre_grupo=grupo.nombre)

        admin_group_id = current.auth.id_group('admins_%d'%igrupo)
        emails_admins = [admin.email
                         for admin in db((db.auth_user.id == db.auth_membership.user_id) &
                                         (db.auth_membership.group_id==admin_group_id)) \
                         .select(db.auth_user.email)]
        # Quitamos los emails que están en la blacklist
        # TODO: ¿deberíamos desactivar a los usuarios que tienen un email inválido?
        #  Quizá sí, pero habría que avisarles al hacer login, o avisar a los otros admins del grupo
        #  o algo... por lo pronto avisamos al resto de admins
        blacklist = email_blacklist()
        emails_ok, emails_black = [], []
        for email in emails_admins:
            if email in blacklist:
                emails_black.append(email)
            else:
                emails_ok.append(email)
        if not emails_ok:
            emails_ok = emails_de_todos_los_admins()
        if emails_black:
            cuerpo = C.EMAIL_AVISO_BLACKLIST%(emails_black) + '\n\n' + cuerpo
        if (not emails_admins or
            not current.mail.send(to=emails_ok, subject=asunto, message=cuerpo) or
            # aunque enviemos los emails a los grupos ok, avisamos de que ha habido problemas si
            #  algún email está en la blacklist
            emails_black):
            errores_email.append(grupo.nombre)
    return errores_email

def pedido_fantasma(iproductor):
    return current.db.pedido(productor=iproductor,fecha_reparto=current.C.FECHA_FANTASMA)

def mueve_productos_al_pedido_fantasma(ipedido):
    '''Al editar el pedido mas reciente, o cargar desde hoja de cálculo los productos
    de un productor para un pedido nuevo, queremos guardar esos productos en el pedido
    fantasma, para usar esos datos para el próximo pedido.

    hacer un barrido del pedido fantasma seguido de una copia de los productos
    '''
    db = current.db
    pedido     = db.pedido[ipedido]
    iproductor = pedido.productor
    productor  = db.productor[iproductor]
    pedidof  = pedido_fantasma(iproductor)

    limpia_pedido(pedidof)
    _mueve_productos(pedido, pedidof)

    if pedido.sin_confirmar:
        db.pedido(ipedido).update_record(sin_confirmar=False)

## Peticiones / Pedidos abiertos

def actualiza_precios_productos(ipedido, formulas):
    '''Al cambiar la formula o las variables globales en un pedido, es necesario recalcular
    los precio_final y precio_productor de los productos.

    No recalcula los precios de peticion, item, item_grupo, pq eso se hace de oficio, aunque
    no haya que evaluar la formula (por ejemplo, si cambia el precio de un producto), y ademas
    puede no ser necesario (por ejemplo si es el pedido fantasma).

    Puede lanzar SyntaxError y EvalAritException
    '''
    from eval_arit import EvalArit

    db = current.db
    tppp = db.productoXpedido
    pedido  = db.pedido(ipedido)

    #Ahorra tiempo si alguna formula es 'precio_base'
    formulas_triviales, formulas_no_triviales = [], []
    for (nombre_formula, formula) in formulas:
        if formula == 'precio_base':
            formulas_triviales.append( (nombre_formula, formula) )
        else:
            formulas_no_triviales.append( (nombre_formula, formula) )

    for nombre_formula, formula in formulas_triviales:
        db(db.productoXpedido.pedido==ipedido).update(**{
            nombre_formula:db.productoXpedido.precio_base
        })
    if formulas_no_triviales:
        vars_globales = dict((row.nombre, row.valor)
            for row in db(db.campo_extra_pedido.pedido==ipedido) \
                      .select(db.campo_extra_pedido.ALL) )
        #puede lanzar SyntaxError
        evaluadores = [
            EvalArit(formula, vars_globales, current.C.PRECISION_DECIMAL).evalua
            for _,formula in formulas_no_triviales
        ]

        columnas_extra = db(db.columna_extra_pedido.pedido==ipedido) \
                        .select(db.columna_extra_pedido.id,
                                db.columna_extra_pedido.tipo,
                                db.columna_extra_pedido.nombre)
        aliases = [(row.id,
                    db['valor_extra_%s_pedido'%row.tipo.split('(')[0]] \
                       .with_alias('_' + row.nombre.replace('-','_')))
                   for row in columnas_extra]

        fields = [tppp.id, tppp.precio_base]
        fields.extend(calias.valor for cid, calias in aliases)

        rows = db((tppp.pedido==ipedido) & (tppp.temporada==True) & (tppp.activo==True)
                 ).select(*fields, left=[
                calias.on((calias.productoXpedido==tppp.id) & (calias.columna==cid))
                for cid, calias in aliases])

        for row_producto in rows:
            pid, precio_base = row_producto[fields[0]], row_producto[fields[1]]
            #El nombre de la columna extra comienza con '_' por motivos tecnicos
            vars_producto = dict((calias._tablename[1:], row_producto[calias.valor] or Decimal())
                                 for _, calias in aliases)
            vars_producto['precio_base'] = precio_base
            #aquí puede lanzar EvalAritException
            dict_precios = dict((nombre_formula, evaluador(vars_producto))
                for (nombre_formula, formula), evaluador in
                    zip(formulas_no_triviales, evaluadores)
            )
            db(tppp.id==pid).update(**dict_precios)

def actualiza_precios_peticion(ipedido, igrupo=None):
    '''Actualiza los precios de peticion, item e item_grupo, es necesario por ejemplo
    cuando se cambia el precio de los productos en un pedido real.

    v3.6.3: Asume que ya se ha llamado a activa_productos_coordinados si era necesario, así que
            actualiza_precios_peticion ya no llama a activa_productos_coordinados
    '''
    from modelos.grupos import grupos_de_la_red
    db = current.db
    tppp = db.productoXpedido
    pedido = db.pedido(ipedido)
    igrupo = igrupo or pedido.productor.grupo
    grupo  = db.grupo(igrupo)
    # grupo puede ser:
    #  - 1. la red que ofrece el pedido
    #  - 2. una subred intermedia que pasa el pedido de la red madre a los grupos finales
    #  - 3. el grupo final que recibe el pedido, en un pedido propio
    #  - 4. el grupo final que recibe el pedido, en un pedido coordinado
    if grupo.red:
        #  - 1. la red que ofrece el pedido
        #  - 2. una subred intermedia que pasa el pedido de la red madre a los grupos finales
        #No hay peticiones en redes ni subredes, solo en grupos
        # hay que actualizar los precios de todos los grupos que cuelgan de esta red,
        # pero no de los grupos que no cuelgan de esta red
        #TODO redderedes: si la formula es 'manual', no seguimos bajando precios, pero marcamos
        # discrepancia_interna_asumida=False :-/
        grupos_descendientes = list(grupos_de_la_red(igrupo).keys())
        for row in db((db.proxy_pedido.pedido==ipedido) &
                      (db.proxy_pedido.activo==True) &
                      (db.proxy_pedido.grupo.belongs(grupos_descendientes))) \
                   .select(db.proxy_pedido.grupo):
            #Una llamada por cada descendiente, que no será recursiva
            #al tener el argumento opcional grupo
            # Las llamadas no se hacen descendiendo el árbol de subredes, pero no es necesario
            # pq no hay peticiones en redes ni subredes, solo en grupos
            actualiza_precios_peticion(ipedido, row.grupo)
    elif pedido.productor.grupo.red: # and not grupo.red => pedido coordinado
        #  - 4. el grupo final que recibe el pedido, en un pedido coordinado
        #Actualiza los precios de peticion, item, etc usando los precios de db.mascara_producto
        for r in db((tppp.pedido==ipedido) &
                    (tppp.temporada==True) &
                    (tppp.activo==True) &
                    (db.mascara_producto.productoXpedido==tppp.id) &
                    (db.mascara_producto.grupo==igrupo)) \
            .select(tppp.id,
                    tppp.precio_final,
                    db.mascara_producto.precio):
            #TODO: redderedes no importa el precio de la red madre
            # sino el precio de la red que ofrece el producto
            #TODO: redderedes la cantidad que declara haber repartido a los descendientes
            # puede ser distinta de la que declara haber recibido de los ascendientes
            precio_red = r.productoXpedido.precio_final
            precio_grupo = r.mascara_producto.precio

            for t in (db.peticion, db.item, db.item_grupo, db.item_red):
                db(t.productoXpedido==r.productoXpedido.id).update(
                    precio_total=t.cantidad*precio_grupo,
                    precio_red=t.cantidad*precio_red,
                    precio_productor=None
                )
    else: #pedido propio
        #  - 3. el grupo final que recibe el pedido, en un pedido propio
        #Actualiza los precios de peticion, item, etc usando los precios de db.productoXpedido
        for r in db((tppp.pedido==ipedido) &
                    (tppp.temporada==True) &
                    (tppp.activo==True)) \
            .select(tppp.id,
                    tppp.precio_final,
                    tppp.precio_productor):
            precio = r.precio_final
            precio_productor = r.precio_productor
            for t in (db.peticion, db.item, db.item_grupo, db.item_red):
                db(t.productoXpedido==r.id).update(
                    precio_total=t.cantidad*precio,
                    precio_red=t.cantidad*precio,
                    precio_productor=t.cantidad*precio_productor
                )

def post_eliminar_pedido(ipedido):
    '''No es necesario eliminar proxy_pedido, productoXpedido, peticiones, item etc porque
    se eliminan en cascada al desaparecer el pedido.

    Solo es necesario cuando se ha usado una plantilla para abrir varios pedidos'''
    db = current.db
    for row in db(db.plantilla_pedidos.pedidos.contains(ipedido)).select():
        pedidos = row.pedidos
        pedidos.remove(ipedido)
        if pedidos:
            row.update_record(pedidos=pedidos)
        else:
            row.delete_record()

def elimina_item_fuera_de_temporada(ipedido, solo_inactivos=False):
    '''elimina db.peticion, db.item, db.item_grupo de productos que se marcan
    como inactivos y opcionalmente fuera de temporada.
    '''
    db = current.db
    tppp = db.productoXpedido
    for t in (db.peticion, db.item, db.item_grupo):
        q = ((t.productoXpedido==tppp.id) &
             (tppp.pedido==ipedido))
        if solo_inactivos:
            q &= (tppp.activo!=True)
        else:
            q &= ((tppp.temporada!=True) | (tppp.activo!=True))

        tids = [r.id for r in db(q).select(t.id)]
        db(t.id.belongs(tids)).delete()

## Cerrar pedidos / Pedidos cerrados / Incidencias ##

def limpia_item(pedidos):
    '''Limpia las tablas db.item y db.item_grupo para una lista de pedidos, pero no toca
    db.peticion
    '''
    db = current.db
    pxps = db(db.productoXpedido.pedido.belongs(pedidos))._select(db.productoXpedido.id)
    for tabla in (db.item, db.item_grupo):
        db(tabla.productoXpedido.belongs(pxps)).delete()

def cerrar_pedidos(ipedidos, forzar_cierre=False):
    '''cierra los pedidos, calcula los totales por unidad (item) y por grupo (item_grupo) y
    calcula los costes extra del pedido

    Asume que todos los pedidos son de productores del mismo grupo!

    Atención: desde 2.5.1 no cierra el pedido si hay incidencias o el grupo usa
    OpcionPedidos.PARA_Y_PIENSA y alguna unidad no tiene saldo suficiente, aunque
    esto ultimo se puede evitar pasando el argumento forzar_cierre=True.

    Si ya está cerrado ejecuta todas las operaciones igualmente, excepto que no
    hace ninguna anotacion contable.

    Desde v3.1, también llama a decide_si_necesita_consolidar
    '''
    from contabilidad import OpcionPedidos, pedido_tiene_saldo_suficiente
    db = current.db
    session = current.session
    TO = current.CONT.TIPOS_OPERACION

    grupo = db.pedido(ipedidos[0]).productor.grupo
    spedidos = ','.join(map(str, ipedidos))

    for ipedido in ipedidos:
        peticion_completa_a_item(ipedido, grupo.id)

    incidencias = []
    for ipedido in ipedidos:
        incidencias.extend(recalcula_costes_extra_pedido(ipedido))
    if incidencias:
        #Hemos tenido que pasar las peticiones a item para poder calcular los costes extra
        #correctamente, pero si ha habido incidencias el pedido no va a ser cerrado.
        #No cerramos los pedidos con problemas al calcular los costes extra!
        return incidencias

    #TODO: la red podria querer anular el pedido de los grupos que no tengan saldo...
    #pero vamos a lanzar la primera version sin esa posibilidad que nadie ha pedido

    #Comprueba si las unidades tienen saldo suficiente
    if (not grupo.red and
        grupo.opcion_pedidos==OpcionPedidos.PARA_Y_PIENSA and
        not forzar_cierre):
        if not session.unidades_con_saldo_insuficiente:
            session.unidades_con_saldo_insuficiente = {}
        excepciones, total_pedido, total_saldos = pedido_tiene_saldo_suficiente(ipedidos)
        if excepciones:
            session.unidades_con_saldo_insuficiente[spedidos] = (
                excepciones, total_pedido, total_saldos
            )
            d = dict(pedidos=spedidos)
            if current.request.vars.plantilla:
                d['plantilla'] = current.request.vars.plantilla
            redirect(URL(c='contabilidad', f='piensa_antes_de_cerrar', vars=d),
                     client_side=True
            )
    for ipedido in ipedidos:
        decide_si_necesita_consolidar(ipedido)

    #Anota las operaciones correspondientes, solo para los pedidos que no estaban ya cerrados.
    #Tampoco hacemos anotaciones si hay productos que pesar
    listos_para_cierre_contable = [row.id
        for row in db( db.pedido.id.belongs(ipedidos) &
                      (db.pedido.abierto==True) &
                      (db.pedido.pendiente_consolidacion==False)
                     ).select(db.pedido.id)
    ]

    CierrePedido = TO[TO.CERRAR_PEDIDO]
    for ipedido in listos_para_cierre_contable:
        CierrePedido.ejecuta([ipedido])

    #Si el pedido es de una red...
    # 07-08-21 no nos molestamos en calcular listos_para_cierre_parcial pq CierrePedidoParcial no hace nada
    if grupo.red and False:
        #Cierre Parcial para los grupos que se pueda...
        pendientes = set(ipedidos).difference(listos_para_cierre_contable)
        listos_para_cierre_parcial = db(
            db.pedido.id.belongs(pendientes) &
            (db.pedido.abierto==True)  &
            (db.proxy_pedido.pedido==db.pedido.id) &
            (db.proxy_pedido.pendiente_consolidacion==False) &
            (db.proxy_pedido.grupo==db.grupo.id) &
            (db.grupo.opcion_pedidos>0)
        ).select(db.proxy_pedido.pedido, db.proxy_pedido.grupo)
        CierrePedidoParcial = TO[TO.CERRAR_PEDIDO_PARCIAL]
        for row in listos_para_cierre_parcial:
            CierrePedidoParcial.ejecuta([row.pedido, row.grupo])

    db(db.pedido.id.belongs(ipedidos)).update(abierto=False)
    return []

def peticion_completa_a_item(ipedido, igrupo):
    '''Suma toda db.peticion a db.item, y luego a db.item_grupo

    Se llama al cerrar un pedido, es más eficiente que llamar a peticion_a_item para cada subgrupo,
    porque sólo se suma item_grupo de la red una vez
    '''
    from modelos.grupos import niveles_pedido
    db = current.db
    grupo = db.grupo(igrupo)

    niveles = niveles_pedido(igrupo, ipedido)
    for nivel, igrupos in enumerate(niveles):
        if nivel==0: # si es un grupo
            for isubgrupo in igrupos:
                # No elevamos el item a item_grupo porque lo haremos con más cuidado, nivel a nivel
                peticion_a_item(ipedido, isubgrupo, sube_item_grupo_al_terminar=False)
        else: # si es una red
            for isubgrupo in igrupos:
                suma_item_grupo_red([ipedido], isubgrupo, actualizar_cantidad_red=True)

def peticion_a_item(ipedido, igrupo, unidad=None, sube_item_grupo_al_terminar=True):
    db = current.db

    #Borra items antiguos
    q_items = ((db.item.grupo==igrupo) &
               (db.item.productoXpedido==db.productoXpedido.id) &
               (db.productoXpedido.pedido==ipedido))
    if unidad:
        q_items &= (db.item.unidad==unidad)
    d = db(q_items) \
        .select(db.item.id)
    ls = [row.id for row in d]
    db(db.item.id.belongs(ls)).delete()

    #Nuevos items asumiendo que llegan todas las peticiones
    cantidad_total = db.peticion.cantidad.sum()
    q_pets = ((db.peticion.persona==db.auth_user.id) &
              (db.peticion.grupo==igrupo) &
              (db.personaXgrupo.persona==db.auth_user.id) &
              (db.personaXgrupo.grupo==igrupo) &
              (db.peticion.productoXpedido==db.productoXpedido.id) &
              (db.productoXpedido.pedido==ipedido))
    if unidad:
        q_pets &= (db.personaXgrupo.unidad==unidad)
    s = db(q_pets) \
        .select(cantidad_total,
                db.personaXgrupo.unidad,
                db.productoXpedido.id,
                groupby=db.personaXgrupo.unidad|db.productoXpedido.id)
    for row in s:
        db.item.insert(grupo=igrupo,
                       productoXpedido=row.productoXpedido.id,
                       unidad=row.personaXgrupo.unidad,
                       cantidad=row[cantidad_total],
                       cantidad_pedida=row[cantidad_total])
    prepara_item_grupo([ipedido], igrupo, sube_item_grupo_al_terminar)

def suma_item_grupo_red(ipedidos, ired, actualizar_cantidad_red=False):
    '''Rellena db.item_grupo.cantidad de una red sumando db.item_grupo.cantidad_red del nivel inferior,

    Si actualizar_cantidad_red=False, no altera item_grupo.cantidad_red ni item_grupo.cantidad_recibida.
     Es lo correcto por ejemplo al reportar incidencias de una subred.

    Si actualizar_cantidad_red=True, se ponen item_grupo.cantidad_red e item_grupo.cantidad_recibida
     igual a item_grupo.cantidad.
     Es lo correcto por ejemplo al cerrar pedidos.
    '''
    from modelos.grupos import nodos_en_la_red
    db = current.db
    pxps = [r.id for r in
            db(db.productoXpedido.pedido.belongs(ipedidos)).select(db.productoXpedido.id)]
    cantidades = {}
    cantidad_total = db.item_grupo.cantidad_red.sum()
    # Recolectamos item_grupo.cantidad_red de los descendientes directos. Esa es la cantidad que
    # rellenamos desde el formulario de incidencias.
    subredes = nodos_en_la_red(ired, ipedidos=ipedidos)
    for r in db((db.item_grupo.grupo.belongs(subredes)) &
                (db.item_grupo.productoXpedido.belongs(pxps))) \
             .select(db.item_grupo.grupo,
                     cantidad_total,
                     db.item_grupo.productoXpedido,
                     groupby=db.item_grupo.productoXpedido):
        cantidades[r[db.item_grupo.productoXpedido]] = r[cantidad_total]
    #TODO: ¿se puede hacer con un solo, o dos comandos pydal o sql? INSERT SELECT SUM
    #uno para actualizar los item_grupo existentes, otro para crear los nuevos

    #1: actualizar los item_grupo existentes
    for r in db((db.item_grupo.grupo==ired) &
                (db.item_grupo.productoXpedido.belongs(pxps))) \
             .select(db.item_grupo.id, db.item_grupo.productoXpedido, db.item_grupo.cantidad):
        # actualizamos item_grupo y sacamos ese producto del diccionario cantidades
        # si no está en el diccionario, actualizamos a cero
        cantidad = cantidades.pop(r.productoXpedido, Decimal())
        if cantidad==r.cantidad:
            continue
        campos = {'cantidad':cantidad}
        if actualizar_cantidad_red:
            campos['cantidad_red'] = cantidad
            campos['cantidad_recibida'] = cantidad
        db.item_grupo(r.id).update_record(**campos)

    #2: insertar los item_grupo restantes
    for ipxp, cantidad in cantidades.items():
        # cantidad_pedida, cantidad_recibida, cantidad_red se dejan a cero
        # dudo: cantidad_recibida y cantidad_red se podrían fijar siempre igual a cantidad:
        #       es probablemente
        #       cierto, pero al ponerlo a cero detectamos errores, obligamos a fijarlo explícitamente
        #       en incidencias_globales. Un producto que no tuviera item, no había sido pedido, pero
        #       se reparte, es potencial fuente de confusiones
        campos = dict(
            grupo=ired,
            cantidad=cantidad,
            productoXpedido=ipxp
        )
        if actualizar_cantidad_red:
            campos['cantidad_red'] = cantidad
            campos['cantidad_recibida'] = cantidad
        db.item_grupo.insert(**campos)

def suma_item_grupo(ipedidos, igrupo):
    '''Rellena db.item_grupo.cantidad sumando db.item, pero no altera
    item_grupo.cantidad_recibida ni item_grupo.cantidad_red

    Se llama típicamente al cerrar un pedido de un grupo, o al reportar incidencias a nivel de unidades
    '''
    db = current.db
    pxps = [r.id for r in
            db(db.productoXpedido.pedido.belongs(ipedidos)).select(db.productoXpedido.id)]
    cantidades = {}
    cantidad_total = db.item.cantidad.sum()
    for r in db((db.item.grupo==igrupo) &
                (db.item.productoXpedido.belongs(pxps))) \
             .select(cantidad_total, db.item.productoXpedido, groupby=db.item.productoXpedido):
        cantidades[r[db.item.productoXpedido]] = r[cantidad_total]
    #TODO: ¿se puede hacer con un solo, o dos comandos pydal o sql? INSERT SELECT SUM
    #uno para actualizar los item_grupo existentes, otro para crear los nuevos

    #1: actualizar los item_grupo existentes
    for r in db((db.item_grupo.grupo==igrupo) &
                (db.item_grupo.productoXpedido.belongs(pxps))) \
             .select(db.item_grupo.id, db.item_grupo.productoXpedido):
        # actualizamos item_grupo y sacamos ese producto del diccionario cantidades
        # si no está en el diccionario, actualizamos a cero
        db.item_grupo(r.id).update_record(cantidad=cantidades.pop(r.productoXpedido, Decimal()))

    #2: insertar los item_grupo restantes
    for ipxp, cantidad in cantidades.items():
        # cantidad_pedida, cantidad_recibida, cantidad_red se dejan a cero
        # dudo: cantidad_recibida y cantidad_red se podrían fijar igual a cantidad: es probablemente
        #       cierto, pero al ponerlo a cero detectamos errores, obligamos a fijarlo explícitamente
        #       en incidencias_globales. Un producto que no tuviera item, no había sido pedido, pero
        #       se reparte, es potencial fuente de confusiones
        db.item_grupo.insert( grupo=igrupo,
                              cantidad=cantidad,
                              productoXpedido=ipxp)

def prepara_item_grupo(ipedidos, igrupo, suma_item_grupo_al_terminar=True):
    '''Elimina todo el db.item_grupo de ipedido, y lo regenera a partir de db.item

    Solo se llama al cerrar el pedido

    Desde la version 3.6.4, también actualiza todos los db.item_grupo de las redes ascendientes

    Rellena item_grupo.cantidad, item_grupo.cantidad_red, item_grupo.cantidad_recibida
    y item_grupo.cantidad_pedida
    '''
    db = current.db
    pxps = [r.id for r in
            db(db.productoXpedido.pedido.belongs(ipedidos)).select(db.productoXpedido.id)]
    db((db.item_grupo.grupo==igrupo) &
        db.item_grupo.productoXpedido.belongs(pxps)).delete()

    #TODO: ¿se puede hacer con un solo comando pydal o sql?
    cantidad_total = db.item.cantidad.sum()
    for r in db((db.item.grupo==igrupo) &
                (db.item.productoXpedido.belongs(pxps))) \
             .select(cantidad_total, db.item.productoXpedido, groupby=db.item.productoXpedido):
        ct = r[cantidad_total]
        db.item_grupo.insert(grupo=igrupo,
                             cantidad=ct,
                             cantidad_pedida=ct,
                             cantidad_recibida=ct,
                             cantidad_red=ct,
                             productoXpedido=r[db.item.productoXpedido])
    if suma_item_grupo_al_terminar:
        sube_item_grupo(ipedidos, igrupo, pxps)

def sube_item_grupo(ipedidos, igrupo, lista_productos):
    '''Limpia item_grupo y lo regenera sumando desde las peticion de los grupos

    Se llama al modificar peticion, item, o item_grupo, de cualquier grupo

    Rellena item_grupo.cantidad, item_grupo.cantidad_red, item_grupo.cantidad_recibida
    y item_grupo.cantidad_pedida
    '''
    from modelos.grupos import nodos_sobre_el_grupo, nodos_en_la_red
    db = current.db

    for ired in nodos_sobre_el_grupo(igrupo, ipedidos=ipedidos):
        #primero borramos el item_grupo de esta red
        db((db.item_grupo.grupo==ired) &
            db.item_grupo.productoXpedido.belongs(lista_productos)).delete()

        #después rellenamos item_grupo a este nivel usando todo el item_grupo del nivel inferior
        #TODO: ¿se puede hacer con un solo comando pydal o sql (INSERT SELECT SUM()...)? :-/
        hijos = nodos_en_la_red(ired, ipedidos)
        cantidad_total = db.item_grupo.cantidad.sum()
        for r in db((db.item_grupo.grupo.belongs(hijos)) &
                    (db.item_grupo.productoXpedido.belongs(lista_productos))) \
                 .select(cantidad_total,
                         db.item_grupo.productoXpedido,
                         groupby=db.item_grupo.productoXpedido):
            ct = r[cantidad_total]
            db.item_grupo.insert(grupo=ired,
                                 cantidad=ct,
                                 cantidad_pedida=ct,
                                 cantidad_recibida=ct,
                                 cantidad_red=ct,
                                 productoXpedido=r[db.item_grupo.productoXpedido])

        #finalmente, subimos un nivel, si tal nivel existe
        sube_item_grupo(ipedidos, ired, lista_productos)

### Incidencias obligatorias / consolidar pedidos / discrepancias ###
def grupo_ha_consolidado(ipedido, igrupo, hacia_abajo=True, hacia_arriba=False):
    '''Comprueba si hay diferencias entre item_grupo.cantidad, item_grupo.cantidad_recibida e
    item_grupo.cantidad_red,
    y establece los booleanos de pedido y proxy_pedido como sea necesario.

    Si hacia_abajo==True, asume que se han modificado item_grupo.cantidad, por lo que
    podría ser necesario modificar la discrepancia_ascendiente de los grupos del nivel inferior

    Si hacia_arriba==True, asume que se ha modificado cantidad_repartida o cantidad_red,
    y podría ser necesario modificar la discrepancia_descendiente de la red del nivel superior
    '''
    from modelos.grupos import nodos_en_la_red
    db = current.db
    pedido = db.pedido(ipedido)
    tp = tipo_pedido(ipedido, igrupo)
    di = hay_discrepancia_interna(ipedido, igrupo)
    da = hay_discrepancia_ascendiente(ipedido, igrupo)

    if (tp==TiposPedido.PROPIO) or (tp==TiposPedido.COORDINADO):
        dd = False
    else:
        isubgrupos_discrepancia = hay_discrepancia_descendiente(ipedido, igrupo, devolver_grupos=True)
        isubgrupos_pedido = nodos_en_la_red(igrupo, [ipedido])
        isubgrupos_sin_discrepancia = set(isubgrupos_pedido).difference(isubgrupos_discrepancia)
        dd = bool(isubgrupos_discrepancia)
        if hacia_abajo:
            # Marca la discrepancia_ascendiente en cada uno de los grupos de la lista
            db( db.proxy_pedido.grupo.belongs(isubgrupos_discrepancia) &
               (db.proxy_pedido.pedido==ipedido)).update(discrepancia_ascendiente=True)
            # Desmarca la discrepancia_ascendiente en cada uno de los grupos que no están en la lista
            db( db.proxy_pedido.grupo.belongs(isubgrupos_sin_discrepancia) &
               (db.proxy_pedido.pedido==ipedido)).update(discrepancia_ascendiente=False)

    if tp==TiposPedido.PROPIO or tp==TiposPedido.RED:
        pedido.update_record(
            pendiente_consolidacion=False,
            discrepancia_interna=di,
            discrepancia_interna_asumida=False,
            discrepancia_productor=da,
            discrepancia_descendiente=dd
        )
        return (di, da, dd)
    else: #if tp==TiposPedido.COORDINADO or tp==TiposPedido.SUBRED
        proxy = db.proxy_pedido(pedido=ipedido, grupo=igrupo)
        proxy.update_record(
            pendiente_consolidacion=False,
            discrepancia_interna=di,
            discrepancia_interna_asumida=False,
            discrepancia_ascendiente=da,
            discrepancia_descendiente=dd
        )
        igrupo_productor = pedido.productor.grupo
        via = proxy.via
        ppedido_sup = (
            pedido if igrupo_productor==via else
            db.proxy_pedido(pedido=ipedido, grupo=via)
        )
        # Comprobamos si en el nivel superior se ha resuelto la discrepancia_descendiente, por
        # si acaso era ésta la última discrepancia pendiente.
        # También puede ocurrir que no hubiera discrepancia en el nivel superior
        # y la acabemos de crear...
        # La discrepancia_ascendiente no escala por el árbol de redes
        if hacia_arriba:
            ppedido_sup.update_record(
                discrepancia_descendiente=hay_discrepancia_descendiente(ipedido, via)
            )
        comprueba_pedido_pendiente_consolidacion(ipedido, via, ppedido_sup)
        return (di, da, dd)

def comprueba_pedido_pendiente_consolidacion(ipedido, ired, ppedido):
    '''Comprobamos si en este nivel queda algún grupo pendiente_consolidacion en el nivel
    inferior, y escalamos por el árbol de redes

    ppedido puede ser un pedido o un proxy_pedido
    '''
    db = current.db
    # ipedido e ired son redundantes, se pueden sacar de ppedido...
    pedido_pendiente = not db((db.proxy_pedido.pedido == ipedido) &
                              (db.proxy_pedido.grupo==db.grupoXred.grupo) &
                              (db.proxy_pedido.activo==True) &
                              (db.proxy_pedido.pendiente_consolidacion==True) &
                              (db.grupoXred.activo==True) &
                              (db.grupoXred.red==ired)
    ).isempty()
    # Si pedido_pendiente es True no es necesario hacer nada más, porque pendiente_consolidacion
    # empieza a True y luego solo puede cambiar a False, no hay vuelta atrás
    if not pedido_pendiente:
        ppedido.update_record(
            pendiente_consolidacion=pedido_pendiente
        )
        # Si ppedido es un registro de la tabla proxy_pedido, ascendemos:
        if ppedido.has_key('via'):
            via = ppedido.via
            pedido = db.pedido(ipedido)
            igrupo_productor = pedido.productor.grupo
            ppedido_sup = (
                pedido if igrupo_productor==via else
                db.proxy_pedido(pedido=ipedido, grupo=via)
            )
            comprueba_pedido_pendiente_consolidacion(ipedido, via, ppedido_sup)

def pedido_consolidado(ipedido):
    '''Comprueba las flags del pedido para decidir si está listo para cierre contable
    '''
    db = current.db
    pedido = db.pedido(ipedido)
    #El pedido sigue pendiente, y no se anota en la contabilidad, mientras:
    # - no se hayan pesado los productos obligatorios (p.pendiente_consolidacion) *o*
    # Esto es fácil, porque cuando un grupo consolida, el flag proxy_pedido.pendiente_consolidacion=False
    #  se propaga hacia arriba
    # - existen discrepancias, ascendiente o descendiente
    # Estas son sencillas, pero hay que mirar todos los proxy_pedido
    # - existe alguna discrepancia interna no asumida
    if (pedido.pendiente_consolidacion or pedido.discrepancia_productor
        or (pedido.discrepancia_interna and not pedido.discrepancia_interna_asumida)
        or pedido.discrepancia_descendiente):
        return False
    return db((db.proxy_pedido.pedido==ipedido) &
              ((db.proxy_pedido.discrepancia_descendiente==True) |
              # No es necesario comprobar la discrepancia_ascendiente porque si hay alguna
              # discrepancia_ascendiente, también habrá una discrepancia_descendiente
#               (db.proxy_pedido.discrepancia_ascendiente==True) |
               ((db.proxy_pedido.discrepancia_interna==True) &
                (db.proxy_pedido.discrepancia_interna_asumida==False)))).isempty()

def proxy_pedido_pendiente(ipedido, igrupo):
    '''DEPRECATED
    '''
    raise DeprecationWarning('')
    pp = current.db.proxy_pedido(pedido=ipedido, grupo=igrupo)
    return  ( pp.pendiente_consolidacion or
              ((pp.pendiente_incidencias_unidades or
                pp.incidencias_declaradas) and
               pp.discrepancia_asumida==0 ) )

def productos_discrepancia(ipedidos, igrupo):
    '''Se usa para informes pdf desde tabla_discrepancias, que forma parte de varios informes

    v4.0 incluye también las discrepancias en pedidos propios
    '''
    db = current.db
    tig = db.item_grupo
    ngrupo = db.grupo(igrupo).nombre

    pedidos_con_discrepancias = defaultdict(list)
    for row in db((tig.productoXpedido == db.productoXpedido.id) &
                  db.productoXpedido.pedido.belongs(ipedidos) &
                  (tig.grupo == igrupo)  &
                  ((tig.cantidad!=tig.cantidad_red) | (tig.cantidad!=tig.cantidad_recibida))
                ).select(tig.cantidad, tig.cantidad_recibida, tig.cantidad_red,
                         tig.precio_total, tig.precio_recibido, tig.precio_red,
                         db.productoXpedido.nombre, db.productoXpedido.pedido,
                         orderby=db.productoXpedido.pedido|db.productoXpedido.nombre):
        pedidos_con_discrepancias[row.productoXpedido.pedido].append(
            (row.item_grupo.cantidad, row.item_grupo.cantidad_recibida, row.item_grupo.cantidad_red,
             row.item_grupo.precio_total, row.item_grupo.precio_recibido, row.item_grupo.precio_red,
             row.productoXpedido.nombre)
        )
    if pedidos_con_discrepancias:
        ipedidos_redes = [row.id for row in
            db( db.pedido.id.belongs(list(pedidos_con_discrepancias)) &
                (db.pedido.productor==db.productor.id) &
                (db.productor.grupo!=igrupo)).select(db.pedido.id)
        ]
        ipedidos_propios = set(pedidos_con_discrepancias).difference(ipedidos_redes)

        nombres_productores = dict(
            (row.pedido.id,
             (row.productor.nombre, ngrupo, True, #True: es PROPIO o RED
              row.pedido.discrepancia_interna,
              row.pedido.discrepancia_interna_asumida, row.pedido.discrepancia_productor))
            for row in  db( db.pedido.id.belongs(ipedidos_propios) &
                            (db.pedido.productor==db.productor.id)
                            ).select(
                                db.pedido.id, db.productor.nombre,
                                db.pedido.discrepancia_interna,
                                db.pedido.discrepancia_interna_asumida,
                                db.pedido.discrepancia_productor,
                                orderby=db.productor.nombre)
        )
        nombres_productores_redes = dict(
            (row.pedido.id,
             (row.productor.nombre, row.grupo.nombre, False, #False: es COORDINADO o SUBRED
              row.proxy_pedido.discrepancia_interna,
              row.proxy_pedido.discrepancia_interna_asumida, row.proxy_pedido.discrepancia_ascendiente))
            for row in  db( db.pedido.id.belongs(ipedidos_redes) &
                            (db.pedido.productor==db.productor.id) &
                            (db.pedido.id==db.proxy_pedido.pedido) &
                            (db.grupo.id==db.proxy_pedido.via) &
                            (db.proxy_pedido.grupo==igrupo)
                            ).select(
                                db.pedido.id, db.productor.nombre, db.grupo.nombre,
                                db.proxy_pedido.discrepancia_interna,
                                db.proxy_pedido.discrepancia_interna_asumida,
                                db.proxy_pedido.discrepancia_ascendiente,
                                orderby=db.productor.grupo|db.productor.nombre)
        )
        nombres_productores.update(nombres_productores_redes)
    else:
        nombres_productores = dict()
    return pedidos_con_discrepancias, nombres_productores

def decide_si_necesita_consolidar(ipedido):
    '''Establece el valor del booleano pedido.pendiente_consolidacion al cerrar el pedido

    Establece pendiente_consolidacion = True sii hay algún producto de temporada
    con pesar=True del que haya pedido alguien.
    '''
    db = current.db
    tppp = db.productoXpedido
    pedido = db.pedido(ipedido)
    pedido_coordinado = pedido.productor.grupo.red
    q = ((tppp.pesar==True) &
         (tppp.pedido==ipedido) &
         (tppp.temporada==True) &
         (tppp.activo==True) &
         (db.peticion.productoXpedido==tppp.id) &
         (db.peticion.cantidad>0))
    if pedido_coordinado:
        pendiente_consolidacion = False
        #Comprueba ahora los proxy_pedido para este pedido
        for row in db((db.proxy_pedido.pedido==ipedido) &
                      (db.proxy_pedido.activo==True)
                     ).select(db.proxy_pedido.id, db.proxy_pedido.grupo):
            proxy_pendiente = not db(q & (db.peticion.grupo==row.grupo)).isempty()
            db.proxy_pedido(row.id).update_record(pendiente_consolidacion=proxy_pendiente)
            pendiente_consolidacion = pendiente_consolidacion or proxy_pendiente
    else:
        pendiente_consolidacion = not db(q).isempty()
    pedido.update_record(pendiente_consolidacion=pendiente_consolidacion)

def decide_booleanos_pedido(ipedido):
    '''Calcula los booleanos discrepancia_descendiente en base a las discrepancia_ascendiente
    del nivel inferior

    DEPRECATED
    '''
    raise DeprecationWarning
    db = current.db
    tpp = db.proxy_pedido
    pp_superior = tpp(pedido=ipedido, grupo=via)

    incidencias_declaradas = not db((tpp.pedido==ipedido) &
                                    (tpp.activo==True) &
                                    (tpp.incidencias_declaradas==True)).isempty()
    pte_inci_unid = not db((tpp.pedido==ipedido) &
                           (tpp.activo==True) &
                           (tpp.pendiente_incidencias_unidades==True)).isempty()
    pte_consolidar = not db((tpp.pedido==ipedido) &
                            (tpp.activo==True) &
                            (tpp.pendiente_consolidacion==True)).isempty()
    discrepancias_cerradas = (3 if (incidencias_declaradas or pte_inci_unid) and
                                   db((tpp.pedido==ipedido) &
                                      (tpp.activo==True) &
                                      ((tpp.pendiente_incidencias_unidades==True) |
                                       (tpp.incidencias_declaradas==True)) &
                                      (tpp.discrepancia_asumida==0)).isempty()
                              else 0)

    pedido = db.pedido(ipedido)
    pedido.update_record(pendiente_incidencias_unidades=pte_inci_unid
                        ,incidencias_declaradas=incidencias_declaradas
                        ,pendiente_consolidacion=pte_consolidar
                        ,discrepancia_asumida=discrepancias_cerradas
                         )
    return pedido

def _productos_con_discrepancia_interna(ipedido, igrupo):
    db = current.db
    ti = db.item
    tig = db.item_grupo
    return {row.productoXpedido.id:(row.item_grupo.cantidad,
                                    row.item_grupo.cantidad_recibida,
                                    row.productoXpedido.granel)
            for row in db((tig.grupo==igrupo) &
                          (tig.productoXpedido==db.productoXpedido.id) &
                          (db.productoXpedido.pedido==ipedido) &
                          (tig.cantidad!=tig.cantidad_recibida))
                       .select(db.productoXpedido.id,
                               tig.cantidad,
                               tig.cantidad_recibida,
                               db.productoXpedido.granel)
    }

def reparto_redondeo(ipedido, igrupo):
    '''
    '''
    from utils.reparto_proporcional import reparto_proporcional, reparto_con_redondeo
    db = current.db
    ti = db.item
    tp = tipo_pedido(ipedido, igrupo)
    ppedido = (db.pedido(ipedido) if (tp==TiposPedido.PROPIO or tp==TiposPedido.RED)
               else db.proxy_pedido(pedido=ipedido, grupo=igrupo))

    productos_discrepancia = _productos_con_discrepancia_interna(ipedido, igrupo)
    if not productos_discrepancia:
        ppedido.update_record(
            discrepancia_interna_asumida=False,
            discrepancia_interna=False
        )
        return True
    iproductos = list(productos_discrepancia.keys())
    d_items_pedido = defaultdict(list)
    for row in db( ti.productoXpedido.belongs(iproductos) &
                  (ti.grupo==igrupo) &
                  (ti.cantidad>0)) \
               .select(ti.id, ti.cantidad, ti.productoXpedido):
        d_items_pedido[row.productoXpedido].append((row.id, row.cantidad))
    for ipxp, cantidades_unidad in d_items_pedido.items():
        repartido, recibido, granel = productos_discrepancia.pop(ipxp)
        cantidades = [cantidad for id,cantidad in cantidades_unidad]
        if granel:
            nuevas_cantidades = reparto_proporcional(recibido, cantidades)
        else:
            nuevas_cantidades = reparto_con_redondeo(recibido, cantidades)
        for (id,_),cantidad_nueva in zip(cantidades_unidad, nuevas_cantidades):
            ti(id).update_record(cantidad=cantidad_nueva)
    db((db.item_grupo.grupo==igrupo) &
       (db.item_grupo.productoXpedido.belongs(d_items_pedido))).update(
       cantidad=db.item_grupo.cantidad_recibida
       )
    # Si queda algún producto que no se ha podido repartir de esta forma...
    resuelto = not productos_discrepancia
    ppedido.update_record(
        discrepancia_interna_asumida=False,
        discrepancia_interna=not resuelto
    )
    return resuelto

def ajusta_precio_para_compensar_discrepancias(ipedido, igrupo):
    db = current.db
    tpxp = db.productoXpedido
    tmp = db.mascara_producto
    ti = db.item
    tig = db.item_grupo
    grupo = db.grupo(igrupo)
    productos_discrepancia = _productos_con_discrepancia_interna(ipedido, igrupo)
    tp = tipo_pedido(ipedido, igrupo)
    ppedido = (db.pedido(ipedido) if (tp==TiposPedido.PROPIO or tp==TiposPedido.RED)
               else db.proxy_pedido(pedido=ipedido, grupo=igrupo))
    if not productos_discrepancia:
        ppedido.update_record(
            discrepancia_interna=False,
            discrepancia_interna_asumida=False
        )
        return True

    iproductos = list(productos_discrepancia.keys())
    # si el pedido es propio, ajusta el precio de productoXpedido
    # si es coordinado, ajusta el precio de la mascara_producto
    precios = {}
    # si algún producto se ha cobrado pero no se ha repartido nada, no se puede
    # resolver la discrepancia
    no_resuelto = False
    if tp==TiposPedido.PROPIO or tp==TiposPedido.RED:
        for ipxp, (repartido, recibido, _) in productos_discrepancia.items():
            rec = tpxp(ipxp)
            if repartido==0:
                no_resuelto = True
                continue
            nuevo_precio = rec.precio_productor*recibido/repartido
            precios[ipxp] = nuevo_precio
            rec.update_record(precio_final=nuevo_precio)
    else:
        for ipxp, (repartido, recibido, _) in productos_discrepancia.items():
            rec = tmp(productoXpedido=ipxp, grupo=igrupo)
            if repartido==0:
                no_resuelto = True
                continue
            nuevo_precio = rec.precio_red*recibido/repartido
            precios[ipxp] = nuevo_precio
            rec.update_record(precio=nuevo_precio)

    # Ajusta item en los productos resueltos
    if not grupo.red:
        for row in db((ti.grupo==igrupo) &
                       ti.productoXpedido.belongs(precios)) \
                   .select(ti.id, ti.cantidad, ti.productoXpedido):
            ti(row.id).update_record(precio_total=row.cantidad*precios[row.productoXpedido])

    # Ajusta item_grupo en los productos resueltos
    for row in db((tig.grupo==igrupo) &
                   tig.productoXpedido.belongs(precios)) \
               .select(tig.id, tig.cantidad, tig.productoXpedido):
        tig(row.id).update_record(precio_total=row.cantidad*precios[row.productoXpedido])
    ppedido.update_record(
        formula_sobreprecio='manual',
        #Si hemos llegado hasta aquí, es que hay productos con discrepancia
        discrepancia_interna=True,
        #Pero se ha asumido la discrepancia, si se pudo
        discrepancia_interna_asumida=not no_resuelto
    )
    return not no_resuelto

def comprobar_no_suministrado(ipedido, igrupos):
    ''' Si la red no suministra algunos productos,
    Actualiza item_grupo e item de las subredes y grupos a cero.
    Después comprueba si se debe poner pendiente_consolidacion a False.
    '''
    from modelos.grupos import grupos_y_subredes_de_la_red

    db = current.db
    pedido = db.pedido(ipedido)
    pedido_coordinado = pedido.productor.grupo.red
    if not pedido_coordinado:
        assert len(igrupos)==1

    #actualizar productos no suministrados
    q = query_productos_con_discrepancia(ipedido, igrupos)
    q &= (db.item_grupo.cantidad_red==0)
    grupo_producto_no_suministrado = defaultdict(list)
    for row in db(q).select(db.item_grupo.grupo, db.item_grupo.productoXpedido):
        grupo_producto_no_suministrado[row.grupo].append(row.productoXpedido)
    if not grupo_producto_no_suministrado:
        return
    for igrupo, lista_productos in grupo_producto_no_suministrado.items():
        #desciende el grafo del pedido, elimina todo ese producto
        descendientes = grupos_y_subredes_de_la_red(igrupo)
        db( db.item_grupo.grupo.belongs(descendientes) &
           (db.item_grupo.productoXpedido.belongs(lista_productos))) \
        .update(cantidad=0, cantidad_red=0)
        #y también de item
        db((db.item.grupo==igrupo) &
           (db.item.productoXpedido.belongs(lista_productos))) \
        .update(cantidad=0, cantidad_red=0)

    igrupos_no_suministrado = list(grupo_producto_no_suministrado)
    igrupos_no_suministrado = [
        row.grupo for row in
        db(db.proxy_pedido.grupo.belongs(igrupos_no_suministrado) &
           (db.proxy_pedido.pedido==ipedido) &
           (db.proxy_pedido.pendiente_consolidacion==True)).select(
           db.proxy_pedido.grupo
           )
       ]
    igrupos_que_necesitan_consolidar = [
        row.grupo for row in
        db((db.item_grupo.cantidad_red>0) &
           (db.item_grupo.productoXpedido==db.productoXpedido.id) &
           (db.productoXpedido.pesar==True) &
           (db.item_grupo.grupo.belongs(igrupos_no_suministrado)) &
           (db.productoXpedido.pedido==ipedido)) \
        .select(db.item_grupo.grupo, groupby=db.item_grupo.grupo)
    ]
    igrupos_que_ya_no_necesitan_consolidar = set(igrupos_no_suministrado)\
                                             .difference(igrupos_que_necesitan_consolidar)

    db( db.proxy_pedido.grupo.belongs(igrupos_que_ya_no_necesitan_consolidar) &
       (db.proxy_pedido.pedido==ipedido)
    ).update(pendiente_consolidacion=False)

def separa_pedidos_pendientes(igrupo, ipedidos, coordinados=False):
    '''Separa una lista de pedidos en una de pedidos pendientes de declarar incidencias
    por unidades y otra que no

    DEPRECATED
    '''
    raise DeprecationWarning('')

    db = current.db
    ipedidos1, ipedidos2 = [], []
    if coordinados:
        ppedidos = db( db.proxy_pedido.pedido.belongs(ipedidos) &
                      (db.proxy_pedido.grupo==igrupo)).select(
            db.proxy_pedido.pedido, db.proxy_pedido.pendiente_incidencias_unidades
        )
        for row in ppedidos:
            if row.pendiente_incidencias_unidades:
                ipedidos2.append(row.pedido)
            else:
                ipedidos1.append(row.pedido)
    else:
        pedidos = db(db.pedido.id.belongs(ipedidos)).select(
            db.pedido.id, db.pedido.pendiente_incidencias_unidades
        )
        for row in pedidos:
            if row.pendiente_incidencias_unidades:
                ipedidos2.append(row.id)
            else:
                ipedidos1.append(row.id)
    return ipedidos1, ipedidos2

def hay_discrepancia_interna(ipedido, igrupo):
    '''Comprueba si hay diferencias entre item_grupo.cantidad_recibida e
    item_grupo.cantidad

    No mira item_grupo.cantidad_red
    '''
    db = current.db
    return not db((db.item_grupo.productoXpedido == db.productoXpedido.id) &
                  (db.productoXpedido.pedido == ipedido) &
                  (db.item_grupo.grupo == igrupo)  &
                  (db.item_grupo.cantidad_recibida!=db.item_grupo.cantidad)).isempty()

def hay_discrepancia_ascendiente(ipedido, igrupo):
    '''Comprueba si hay diferencias entre item_grupo.cantidad_recibida e
    item_grupo.cantidad_red

    No mira item_grupo.cantidad
    '''
    db = current.db
    return not db((db.item_grupo.productoXpedido == db.productoXpedido.id) &
                  (db.productoXpedido.pedido == ipedido) &
                  (db.item_grupo.grupo == igrupo)  &
                  (db.item_grupo.cantidad_recibida!=db.item_grupo.cantidad_red)).isempty()

def hay_discrepancia_descendiente(ipedido, igrupo, devolver_grupos=False):
    '''Comprueba si hay diferencias entre item_grupo.cantidad_recibida e
    item_grupo.cantidad_red, pero de alguno de los descendientes directos

    No mira item_grupo.cantidad

    Si se pasa el argumento adicional devolver_grupos=True, devuelve una lista de los grupos
    del nivel inferior en los que hay una discrepancia_ascendiente
    '''
    db = current.db
    query = ((db.item_grupo.productoXpedido == db.productoXpedido.id) &
             (db.productoXpedido.pedido == ipedido) &
             (db.grupoXred.activo==True) &
             (db.grupoXred.red==igrupo) &
             (db.item_grupo.grupo == db.grupoXred.grupo)  &
             (db.item_grupo.cantidad_recibida!=db.item_grupo.cantidad_red))
    if devolver_grupos:
        return [row.grupo for row in db(query).select(db.grupoXred.grupo,groupby=db.grupoXred.grupo)]
    else:
        return not db(query).isempty()

def query_productos_con_discrepancia(ipedido, igrupos):
    db = current.db
    return  ((db.item_grupo.productoXpedido == db.productoXpedido.id) &
             (db.productoXpedido.pedido == ipedido) &
             (db.item_grupo.grupo.belongs(igrupos))  &
             (db.item_grupo.cantidad!=db.item_grupo.cantidad_red))

def filtra_por_discrepancia(ipedido, igrupos):
    db = current.db
    q = query_productos_con_discrepancia(ipedido, igrupos)
    return  [row.grupo for row in
               db(q).select(
                 db.item_grupo.grupo, groupby=db.item_grupo.grupo
              )]

### Coste extra ###

def recalcula_costes_extra_pedido(ipedido, igrupo=None):
    from evalua_coste_extra import CalculadoraCostesExtra
    Calc = CalculadoraCostesExtra(ipedido, igrupo)
    incidencias = Calc.recalcula_costes_extra()
    return incidencias

def coste_extra_pedido(ipedido, igrupo, es_red=False):
    db = current.db
    coste_extra_total = db.coste_pedido.coste.sum()
    query = (db.coste_pedido.pedido==ipedido)
    if not es_red:
        query &= (db.coste_pedido.grupo==igrupo)
        agrupar = db.coste_pedido.grupo
    else:
        agrupar = db.coste_pedido.id
    row = db(query) \
          .select(coste_extra_total, groupby=agrupar) \
          .first()
    return row[coste_extra_total] if row else Decimal()

### Recopila y agrupa información (sólo lectura) ###

def pedidos_del_dia_por_redes(fecha_reparto, igrupo):
    db = current.db
    query_p = ((db.pedido.fecha_reparto==fecha_reparto) &
               (db.pedido.productor==db.productor.id) &
               (db.productor.grupo==igrupo))
    repartosp = [r.pedido.id for r in db(query_p) \
                 .select(db.pedido.id, db.productor.nombre,
                         orderby=db.productor.nombre)]

    query_c = ((db.proxy_pedido.fecha_reparto==fecha_reparto) &
               (db.proxy_pedido.pedido==db.pedido.id) &
               (db.proxy_pedido.grupo==igrupo) &
               (db.proxy_pedido.activo==True) &
               (db.pedido.productor==db.productor.id) &
               (db.productor.grupo==db.grupo.id))
    repartosc = db(query_c) \
                .select(db.pedido.id, db.grupo.nombre, db.productor.nombre,
                        orderby=db.productor.nombre)
    repartos_por_redes = defaultdict(list)
    for row in repartosc:
        repartos_por_redes[row.grupo.nombre].append(row.pedido.id)
    return (repartosp, repartos_por_redes)

def pedidos_del_dia(fecha_reparto, igrupo,
                    solo_abiertos=False, solo_cerrados=False,
                    devuelve_solo_ids=False):
    db = current.db
    query_p = ((db.pedido.fecha_reparto==fecha_reparto) &
               (db.pedido.productor==db.productor.id) &
               (db.productor.grupo==igrupo))
    query_c = ((db.proxy_pedido.fecha_reparto==fecha_reparto) &
               (db.proxy_pedido.pedido==db.pedido.id) &
               (db.proxy_pedido.grupo==igrupo) &
               (db.proxy_pedido.activo==True))
    if solo_abiertos:
        query_p &= (db.pedido.abierto==True)
        query_c &= (db.pedido.abierto==True)
    elif solo_cerrados:
        query_p &= (db.pedido.abierto==False)
        query_c &= (db.pedido.abierto==False)
    campos = db.pedido.id if devuelve_solo_ids else db.pedido.ALL
    repartosp = db(query_p).select(campos)
    repartosc = db(query_c).select(campos)
    return (repartosp, repartosc)

def fechas_pedidos(igrupo):
    '''Todas las fechas en que hay pedidos abiertos o cerrados, coordinados o no,
    pero no historicos, en formato 2014-01-01
    '''
    today = date.today()
    db = current.db
    sfechas = set()
    for r in db((db.pedido.productor==db.productor.id) &
                 (db.productor.grupo==igrupo) &
                 (db.pedido.fecha_reparto>=today)) \
              .select(db.pedido.fecha_reparto,
                      groupby=db.pedido.fecha_reparto):
        sfechas.add(r.fecha_reparto)
    for r in db((db.proxy_pedido.grupo==igrupo) &
                 (db.proxy_pedido.fecha_reparto>=today) &
                 (db.proxy_pedido.activo==True)) \
              .select(db.proxy_pedido.fecha_reparto,
                      groupby=db.proxy_pedido.fecha_reparto):
        sfechas.add(r.fecha_reparto)
    fechas = list(sfechas)
    fechas.sort()
    return fechas

def dame_pedidos_por_fecha(igrupo, q, todos_los_coordinados=False,
                           fecha_ini=None, fecha_fin=None):
    db = current.db
    tp = db.pedido
    tpp = db.proxy_pedido
    C  = current.C
    today = date.today()
    query_base = ((tp.productor==db.productor.id) &
                  (db.productor.grupo==igrupo))
    query_c_base = ((tpp.pedido==tp.id) &
                    (tp.productor==db.productor.id) &
                    (db.proxy_productor.productor==db.productor.id) &
                    (db.proxy_productor.grupo==igrupo) &
                    (tpp.grupo==igrupo))
    if q==EstadosPedido.ABIERTOS:
        query   = query_base & (tp.abierto==True)
        query_c = query_c_base & (tp.abierto==True)
        reverse = False
    elif q==EstadosPedido.PENDIENTES:
        query   = query_base & ((tp.abierto==False) &
                                ((tp.discrepancia_productor==True) |
                                 (tp.discrepancia_descendiente==True)  |
                                 ((tp.discrepancia_interna==True) &
                                  (tp.discrepancia_interna_asumida==False)) )
                               )
        query_c = query_c_base & ((tp.abierto==False) &
                                  ((tpp.discrepancia_ascendiente==True) |
                                   (tpp.discrepancia_descendiente==True)  |
                                   ((tpp.discrepancia_interna==True) &
                                    (tpp.discrepancia_interna_asumida==False)) )
                                 )

        reverse = False
    elif q==EstadosPedido.PENDIENTES_RECIENTES:
        grupo = db.grupo(igrupo)
        dias_pedido_reciente = timedelta(days=grupo.dias_pedido_reciente)

        query   = query_base & ((tp.abierto==False) &
                                ((tp.discrepancia_productor==True) |
                                 (tp.discrepancia_descendiente==True)  |
                                 ((tp.discrepancia_interna==True) &
                                  (tp.discrepancia_interna_asumida==False)) ) &
                                (tp.fecha_reparto>=today-dias_pedido_reciente)
                               )
        query_c = query_c_base & ((tp.abierto==False) &
                                  ((tpp.discrepancia_ascendiente==True) |
                                   (tpp.discrepancia_descendiente==True)  |
                                   ((tpp.discrepancia_interna==True) &
                                    (tpp.discrepancia_interna_asumida==False)) )&
                                   (tpp.fecha_reparto>=today-dias_pedido_reciente)
                                 )
        reverse = False
    elif q==EstadosPedido.CERRADOS:
        query   = query_base & ((tp.abierto==False) &
                                (tp.discrepancia_productor==False) &
                                (tp.discrepancia_descendiente==False) &
                                ((tp.discrepancia_interna==False) |
                                 (tp.discrepancia_interna_asumida==True)) &
                                (tp.fecha_reparto>=today))
        query_c = query_c_base & ((tp.abierto==False) &
                                  (tpp.discrepancia_ascendiente==False) &
                                  (tpp.discrepancia_descendiente==False) &
                                  ((tpp.discrepancia_interna==False) |
                                   (tpp.discrepancia_interna_asumida==True)) &
                                  (tpp.fecha_reparto>=today))
        reverse = False
    elif q==EstadosPedido.HISTORICOS:
        one_day = timedelta(1)
        fecha_ini = fecha_ini or (C.FECHA_FANTASMA + one_day)
        fecha_fin = fecha_fin or (today - one_day)

        query   = query_base & ((tp.abierto==False) &
                                #(((tp.pendiente_consolidacion==False) &
                                # (tp.incidencias_declaradas==False)  &
                                # (tp.pendiente_incidencias_unidades==False)) |
                                # (tp.discrepancia_asumida>0)) &
                                (tp.fecha_reparto<=fecha_fin) &
                                (tp.fecha_reparto>=fecha_ini))
        query_c = query_c_base & ((tp.abierto==False) &
                                #(((db.proxy_pedido.pendiente_consolidacion==False) &
                                # (db.proxy_pedido.incidencias_declaradas==False)  &
                                # (db.proxy_pedido.pendiente_incidencias_unidades==False)) |
                                # (db.proxy_pedido.discrepancia_asumida>0)) &
                                  (tpp.fecha_reparto<=fecha_fin) &
                                  (tpp.fecha_reparto>=fecha_ini))
        reverse = True
    if not todos_los_coordinados:
        query_c &= (db.proxy_pedido.activo==True)
    s = db(query) \
        .select(tp.ALL, db.productor.nombre, db.productor.id)
    s2 = db(query_c)  \
         .select(tp.ALL, tpp.ALL, db.productor.nombre, db.productor.id,
                 orderby=tp.fecha_reparto)

    fechas = set()
    for row in s:
        fechas.add(row.pedido.fecha_reparto)
    for row in s2:
        fechas.add(row.proxy_pedido.fecha_reparto)
    pedidos = dict((f, (list(), list())) for f in fechas)
    for row in s:
        pedidos[row.pedido.fecha_reparto][0].append(row)
    for row in s2:
        pedidos[row.proxy_pedido.fecha_reparto][1].append(row)
    ls = list(pedidos.items())
    ls.sort(reverse=reverse)
    return ls

def via_pedido_en_grupo(ipedido, igrupo):
    '''Devuelve el id de la red que ofrece el pedido al grupo de forma directa.
    Puede ser la red primigenia o no.
    Puede ser el propio igrupo.
    '''
    db = current.db
    ascendientes_intermedios = db((db.grupoXred.grupo==igrupo) &
                                  (db.grupoXred.activo==True) &
                                  (db.proxy_pedido.grupo==db.grupoXred.red) &
                                  (db.proxy_pedido.pedido==ipedido) &
                                  (db.proxy_pedido.activo==True)) \
          .select(db.grupoXred.red, limitby=(0,1))
    if ascendientes_intermedios:
        via = ascendientes_intermedios.first().red
    else:
        via = db.pedido(ipedido).productor.grupo
    return via

def dame_valores_extra(ipedido):
    db = current.db
    extra = defaultdict(dict)
    for n,t in db.TABLAS_VALORES_EXTRA_PEDIDO.items():
        extras = db((db.columna_extra_pedido.pedido==ipedido) &
                    (db.columna_extra_pedido.tipo==n) &
                    (t.columna==db.columna_extra_pedido.id)) \
                 .select(t.producto, t.valor, db.columna_extra_pedido.nombre)
        for row in extras:
            #TODO: procesa el valor (ej, Si/no en vez de True/None, o cambia punto por coma decimal)
            extra[row[t].producto][row.columna_extra_pedido.nombre] = row[t].valor
    return extra

def toda_la_peticion(igrupo, fecha_reparto, ipersona0=-1, unidad=-1):
    from modelos.productores import ordena_productos

    db = current.db
    pedidos_p = [(row.id, row.abierto) for row in
                  db((db.pedido.fecha_reparto==fecha_reparto) &
                     (db.pedido.productor==db.productor.id)   &
                     (db.productor.grupo==igrupo)) \
                  .select(db.pedido.id,
                          db.pedido.abierto)
                ]
    pedidos_c = [(row.id,row.abierto) for row in
                    db((db.proxy_pedido.pedido==db.pedido.id) &
                       (db.proxy_pedido.activo==True) &
                       (db.proxy_pedido.grupo==igrupo) &
                       (db.proxy_pedido.fecha_reparto==fecha_reparto)) \
                    .select(db.pedido.id,
                            db.pedido.abierto)]

    if unidad >= 0:
        ipersonas = [row.persona for row in
            db((db.personaXgrupo.grupo==igrupo) &
               (db.personaXgrupo.unidad==unidad) &
               (db.personaXgrupo.activo==True)) \
            .select(db.personaXgrupo.persona)]
        totales_personas = {}
    else:
        ipersonas = [ipersona0]

    gran_total = Decimal()
    tablas = []
    for ipedidos, coordinado in [(pedidos_p, False), (pedidos_c, True)]:
        for ipedido, pedido_abierto in ipedidos:
            totales_personas_pedido = defaultdict(
                Decimal,
                ((ipersona, Decimal()) for ipersona in ipersonas))
            peticiones = db((db.peticion.persona.belongs(ipersonas)) &
                            (db.peticion.grupo==igrupo) &
                            (db.peticion.productoXpedido==db.productoXpedido.id) &
                            (db.productoXpedido.pedido==ipedido)) \
                        .select(db.productoXpedido.id,
                                db.peticion.cantidad,
                                db.peticion.persona)
            if unidad>=0:
                items = db((db.item.unidad==unidad) &
                           (db.item.grupo==igrupo) &
                           (db.item.productoXpedido==db.productoXpedido.id) &
                           (db.productoXpedido.pedido==ipedido)) \
                        .select(db.productoXpedido.id,
                                db.item.cantidad)
            else:
                items = []

            if not pedido_abierto and unidad>=0:
                row_coste_extra = db.coste_pedido(pedido=ipedido, grupo=igrupo, unidad=unidad)
                coste_extra     = row_coste_extra.coste if row_coste_extra else Decimal()
            else:
                coste_extra     = Decimal()

            if peticiones or items or coste_extra:
                peticiones_d = dict(((row.productoXpedido.id, row.peticion.persona), row.peticion.cantidad)
                                     for row in peticiones)
                items_d = dict((row.productoXpedido.id, row.item.cantidad)
                                for row in items)
                ppps = ordena_productos(set(
                            [row.productoXpedido.id for row in peticiones] +
                            [row.productoXpedido.id for row in items]
                            ))
                ppps_d = db(db.productoXpedido.id.belongs(ppps)) \
                               .select(db.productoXpedido.id, db.productoXpedido.nombre) \
                               .as_dict()

                if coordinado:
                    precios_d = dict(
                     (row.productoXpedido.id, row.mascara_producto.precio)
                     for row in db(db.productoXpedido.id.belongs(ppps) &
                                   (db.productoXpedido.pedido==ipedido) &
                                   (db.mascara_producto.grupo==igrupo) &
                                   (db.mascara_producto.productoXpedido==db.productoXpedido.id) ) \
                                   .select(db.productoXpedido.id, db.mascara_producto.precio) )
                else:
                    precios_d = dict(
                     (row.id, row.precio_final)
                     for row in db(db.productoXpedido.id.belongs(ppps) &
                                   (db.productoXpedido.pedido==ipedido)) \
                                   .select(db.productoXpedido.id, db.productoXpedido.precio_final))

                total = Decimal()
                data = []
                for p in ppps:
                    precio_u = Decimal(precios_d[p])
                    data_row = [ppps_d[p]['nombre'], pprint_p(precio_u)]
                    if unidad>=0:
                        cantidad_t = Decimal()
                        precio_t   = Decimal()
                        for ipersona in ipersonas:
                            cantidad = peticiones_d.get((p, ipersona), Decimal())
                            precio   = precio_u*cantidad
                            data_row.append(pprint_c(cantidad))
                            data_row.append(pprint_p(precio))
                            cantidad_t += cantidad
                            precio_t   += precio
                            totales_personas_pedido[ipersona] += precio
                        data_row.append(pprint_c(cantidad_t))
                        data_row.append(pprint_p(precio_t))

                        cantidad_prod = items_d.get(p, Decimal())
                        precio_prod   = precio_u*cantidad_prod
                        data_row.append(pprint_c(cantidad_prod))
                        data_row.append(pprint_p(precio_prod))
                    else:
                        cantidad_prod = peticiones_d.get((p, ipersona0), Decimal())
                        precio_prod   = precio_u*cantidad_prod
                        data_row.append(pprint_c(cantidad_prod))
                        data_row.append(pprint_p(precio_prod))

                    data.append(data_row)
                    total += precio_prod

                tablas.append((
                    ipedido, data,
                    pprint_p(total),
                    pprint_p(coste_extra) if coste_extra else '',
                    pprint_p(total+coste_extra)
                    ))
                gran_total += total + coste_extra
            if unidad>=0:
                totales_personas[ipedido] = totales_personas_pedido
    if unidad >= 0:
        if ipersona0>0:
            total_personal = pprint_p(sum(
                (tpp[ipersona0] for tpp in totales_personas.values()),
                Decimal()))
        else:
            total_personal = ''
        totales_personas = dict(
                (ipedido,
                 dict((ipersona, pprint_p(total))
                      for ipersona,total in totales_personas_pedido.items()))
                for ipedido,totales_personas_pedido in totales_personas.items())
        return ipersonas, totales_personas, tablas, pprint_p(gran_total), total_personal
    else:
        return tablas, pprint_p(gran_total)

def recoge_y_ordena_items(ipedidos, igrupo, tipotabla=TablasItem.PETICION):
    from modelos.usuarios import representa_usuario_2
    from modelos.grupos import nodos_en_la_red

    db = current.db
    tppp = db.productoXpedido
    tratar_discrepancias = False
    if tipotabla==TablasItem.PETICION:
        tabla  = db.peticion
        sujeto = db.peticion.persona
        grupos = [igrupo]
    elif tipotabla==TablasItem.ITEM:
        tabla  = db.item
        sujeto = db.item.unidad
        grupos = [igrupo]
    else:
        tabla  = db.item_grupo
        sujeto = tabla.grupo
        grupos = nodos_en_la_red(igrupo)
        if not db( (db.proxy_pedido.pedido.belongs(ipedidos)) &
                   (db.proxy_pedido.discrepancia_asumida==2) &
                   (db.proxy_pedido.grupo.belongs(grupos)) ).isempty():
            tratar_discrepancias = True
    cantidades = {}
    sujetos = set()
    productos  = {}
    for ipedido in ipedidos:
        pedido = db.pedido(ipedido)
        #hacemos la query con los grupos que no esten en gruposD,
        #que tratamos por separado
        if tratar_discrepancias:
            gruposD = [row.grupo for row in
                       db((db.proxy_pedido.pedido==ipedido) &
                          (db.proxy_pedido.discrepancia_asumida==2) &
                           db.proxy_pedido.grupo.belongs(grupos)
                       ).select(db.proxy_pedido.grupo)]
            if gruposD:
                sujetoD = db.item_grupo.grupo
                gruposR = list(set(grupos).difference(set(gruposD)))
            else:
                gruposR = grupos
        else:
            gruposR = grupos

        q = ( tabla.grupo.belongs(gruposR) &
             (tabla.productoXpedido==tppp.id) &
             (tppp.pedido==ipedido) )
        tp = tipo_pedido(ipedido, igrupo)
        if tp==TiposPedido.COORDINADO or tp==TiposPedido.SUBRED:
            q &= ((db.mascara_producto.productoXpedido==tppp.id) &
                  (db.mascara_producto.grupo==igrupo) &
                  (db.mascara_producto.aceptado==True))
            field_precio = db.mascara_producto.precio
        else:
            field_precio = tppp.precio_final

        fields = [tabla.cantidad, sujeto, field_precio, tppp.nombre, tppp.id,
                  tppp.granel, tppp.pesar]
        rows = db(q).select(*fields)
        cs = defaultdict(lambda : defaultdict(Decimal))
        for row in rows:
            row_cantidad = row[tabla.cantidad]
            #si la cantidad es 0 no incluimos ese producto
            if row_cantidad>0:
                rppp = row.productoXpedido
                productos[rppp.id] = (
                    rppp.nombre, rppp.granel, rppp.pesar,
                    row[field_precio]
                )
                sujetos.add(row[sujeto])
                cs[rppp.id][row[sujeto]] += row_cantidad
        #si el pedido es de red y hay grupos con discrepancias asumidas por el grupo
        #hay que obtener las cantidades de item_grupo, en vez de item_red
        #cada grupo del pedido de tipo RED aparece en gruposD o en gruposR
        if tratar_discrepancias and gruposD:
            rows = db((db.item_grupo.grupo.belongs(gruposD)) &
                      (db.productoXpedido.pedido==ipedido) &
                      (db.item_grupo.productoXpedido==db.productoXpedido.id)\
                      ).select(db.item_grupo.cantidad, db.item_grupo.grupo, field_precio,
                               tppp.id, tppp.nombre, tppp.granel, tppp.pesar)
            for row in rows:
                row_cantidad = row[db.item_grupo.cantidad]
                #si la cantidad es 0 no incluimos ese producto
                if row_cantidad:
                    rppp = row.productoXpedido
                    productos[rppp.id] = (
                        rppp.nombre, rppp.granel, rppp.pesar,
                        row[field_precio]
                    )
                    sujetos.add(row[sujetoD])
                    cs[rppp.id][row.item_grupo.grupo] += row_cantidad

        cantidades[ipedido] = cs
    if tipotabla==TablasItem.PETICION:
        personas = {}
        for row in db((db.personaXgrupo.grupo==igrupo) &
                       db.personaXgrupo.persona.belongs(sujetos)) \
                   .select(db.personaXgrupo.unidad, db.personaXgrupo.persona):
            personas[(row.unidad, row.persona)] = \
                (representa_usuario_2(row.persona))
        lpersonas = [(u,id,un) for ((u,id), un) in personas.items()]
        lpersonas.sort()
        return lpersonas, productos, cantidades
    else:
        lsujetos = list(sujetos)
        lsujetos.sort()
        return lsujetos, productos, cantidades

def recoge_y_ordena_peticion(ipedidos, igrupo):
    return recoge_y_ordena_items(ipedidos, igrupo, tipotabla=TablasItem.PETICION)

def recoge_y_ordena_item(ipedidos, igrupo):
    return recoge_y_ordena_items(ipedidos, igrupo, tipotabla=TablasItem.ITEM)

def recoge_y_ordena_item_por_grupos(ipedidos, ired):
    return recoge_y_ordena_items(ipedidos, ired, tipotabla=TablasItem.ITEM_GRUPO)

def peticiones_por_unidades(ipedidos, igrupo, coordinados=False):
    '''Devuelve las peticiones agrupados por unidades, mismo formato que recoge_y_ordena_item
    '''
    db = current.db
    tppp = db.productoXpedido
    peticion_total = db.peticion.cantidad.sum()
    q = ((db.peticion.grupo==igrupo) &
         (db.peticion.productoXpedido==tppp.id) &
         (tppp.pedido.belongs(ipedidos)) &
         (db.peticion.persona == db.personaXgrupo.persona) &
         (db.personaXgrupo.grupo==igrupo) )
    if coordinados:
        q &= ((db.mascara_producto.productoXpedido==tppp.id) &
              (db.mascara_producto.grupo==igrupo) &
              (db.mascara_producto.aceptado==True))

    fields = [tppp.id, tppp.pedido, peticion_total, db.personaXgrupo.unidad]
    res = db(q).select(*fields, groupby=tppp.pedido|tppp.id|db.personaXgrupo.unidad)
    unidades = set()
    items = defaultdict(lambda : defaultdict(lambda: defaultdict(Decimal)))
    for row in res:
        items[row.productoXpedido.pedido] \
             [row.productoXpedido.id] \
             [row.personaXgrupo.unidad] = row[peticion_total]
        unidades.add(row.personaXgrupo.unidad)
    return unidades, items

### Contabilidad ###
def anota_incidencias_contabilidad(ipedido, igrupo=None):
    consolidado = pedido_consolidado(ipedido)
    if consolidado:
        TO = current.CONT.TIPOS_OPERACION
        TO[TO.INCIDENCIA].ejecuta([ipedido])
    elif igrupo:
        # No estamos usando esta posibilidad ahora y probablemente no la lleguemos a usar
        raise NotImplementedError

### Otros ###

def mover_pedidos_al_futuro(dias, solo_abiertos=True):
    '''Avanza unos cuantos días la fecha de todos los pedidos (excepto los pedidos fantasma)

    No se llama desde ningún sitio, pero es útil para hacer pruebas
    '''
    db = current.db
    # Lo hacemos con sql porque
    # db(db.pedido.fecha_reparto>date(2000,1,1)).update(fecha_reparto=db.pedido.fecha_reparto+3)
    # no funciona, no traduce bien db.pedido.fecha_reparto+3 a sql
    comando_p = ('update pedido set fecha_reparto=ADDDATE(fecha_reparto, %d) '
                 'WHERE (`pedido`.`fecha_reparto` > \'2000-01-01\')'
                )%dias
    comando_pp = ('update pedido p join proxy_pedido pp ON pp.pedido =p.id '
                  'set pp.fecha_reparto=p.fecha_reparto')
    if solo_abiertos:
        comando_p  += ' and abierto=\'T\''
        comando_pp += ' where p.abierto=\'T\''
    db.executesql(comando_p)
    db.executesql(comando_pp)
