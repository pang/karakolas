# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################

from collections import defaultdict

from gluon import current


### Redes y grupos ###

def redes_del_grupo(igrupo):
    '''Sube todo el árbol de redes que ofrecen productos al grupo, de forma directa o indirecta
    '''
    db = current.db
    s = set()
    for row in db((db.grupoXred.grupo==igrupo) & (db.grupoXred.activo==True)) \
                .select(db.grupoXred.red):
        s.add(row.red)
        s.update(redes_del_grupo(row.red))
    return s

def grupos_y_subredes_de_la_red(ired):
    '''Baja todo el árbol de subredes y grupos a los que esta red ofrece productos
    '''
    db = current.db
    s = set()
    for row in db((db.grupoXred.red==ired) & (db.grupoXred.activo==True)) \
                .select(db.grupoXred.grupo):
        s.add(row.grupo)
        s.update(grupos_y_subredes_de_la_red(row.grupo))
    return s

def grupos_y_redes_conectados(ired):
    '''Devuelve un conjunto de ids de db.grupo que son grupos de esta red, o redes a las que esta
    red pertenece, o redes a las que pertenece otra red a la que esta red pertenece...

    No comprobamos en runtime que el grafo sea un árbol.
    Confiamos en que usando el interfaz no se permite introducir ciclos en el grafo.
    Desde la base de datos sí se pueden introducir, pero esperamos que el sysadmin
    no sea tan cretino
    '''
    db = current.db
    #hacia arriba
    s = set(redes_del_grupo(ired).keys())
    #hacia abajo
    s.update(grupos_y_subredes_de_la_red(ired))
    #y la propia red
    s.add(ired)
    return s

def nodos_en_la_red(ired, ipedidos=None):
    '''Grupos y otras redes que cuelgan de esta red de forma directa

    Si se pasa el argumento opcional ipedidos, solo se usa una conexión red->grupo si hay un
    proxy_pedido activo para esa red y algún pedido de la lista
    '''
    db = current.db
    if db.grupo(ired).red:
        d = {}
        if ipedidos:
            query = ((db.proxy_pedido.grupo==db.grupo.id) &
                     (db.proxy_pedido.activo==True) &
                     (db.proxy_pedido.pedido.belongs(ipedidos)) &
                     (db.proxy_pedido.via==ired))
        else:
            query = ((db.grupoXred.red==ired) &
                     (db.grupoXred.activo==True) &
                     (db.grupoXred.grupo==db.grupo.id))
        for row in db(query).select(db.grupo.id, db.grupo.nombre, db.grupo.red, groupby=db.grupo.id):
            d[row.id] = row.nombre
        return d
    else:
        return {}

def nodos_sobre_el_grupo(igrupo, ipedidos=None):
    '''Redes que sirven a este grupo de forma directa

    Si se pasa el argumento opcional ipedidos, solo se usa una conexión red->grupo si hay un
    proxy_pedido activo para esa red y algún pedido de la lista
    '''
    db = current.db
    d = {}
    if ipedidos:
        query = ((db.proxy_pedido.grupo==igrupo) &
                 (db.proxy_pedido.activo==True) &
                 (db.proxy_pedido.pedido.belongs(ipedidos)) &
                 (db.proxy_pedido.via==db.grupo.id))
    else:
        query = ((db.grupoXred.grupo==igrupo) &
                 (db.grupoXred.activo==True) &
                 (db.grupoXred.red==db.grupo.id))
    for row in db(query).select(db.grupo.id, db.grupo.nombre, db.grupo.red, groupby=db.grupo.red):
        d[row.id] = row.nombre
    return d

def arbol_redes_del_grupo(igrupo):
    '''devuelve un diccionario anidado con las redes a las que pertenece el grupo,
    indicando a través de qué subredes llega al grupo cada red.'''
    db = current.db
    s = {}
    for row in db((db.grupoXred.grupo==igrupo) &
                  (db.grupoXred.activo==True) &
                  (db.grupoXred.red==db.grupo.id)) \
                .select(db.grupo.id, db.grupo.nombre, orderby=db.grupo.nombre):
        s[row.id] = (row.nombre, arbol_redes_del_grupo(row.id))
    return s

def arbol_grupos_de_la_red(ired, ipedido=0, recursiva=False):
    '''Desciende el grafo red->grupo hasta alcanzar a todos los grupos que reciben pedidos
    de esta red y todas las subredes intermedias.

    Se devuelve en formato anidado de diccionario anidado

    Si se pasa el argumento ipedido, el árbol se poda cada vez que una subred no ha aceptado el pedido
    '''
    db = current.db
    red = db.grupo(ired)
    d = {}
    if red.red:
        if ipedido:
            query = ((db.proxy_pedido.grupo==db.grupo.id) &
                     (db.proxy_pedido.activo==True) &
                     (db.proxy_pedido.pedido==ipedido) &
                     (db.proxy_pedido.via==ired))
        else:
            query = ((db.grupoXred.red==ired) &
                     (db.grupoXred.activo==True) &
                     (db.grupoXred.grupo==db.grupo.id))
        for row in db(query).select(db.grupo.id, db.grupo.nombre, db.grupo.red):
            d[row.id] = (row.nombre, arbol_grupos_de_la_red(row.id, ipedido=ipedido, recursiva=True))
    if recursiva:
        return d
    return {red.id: (red.nombre, d)}

def esquema_grupos_pedido(ired, ipedido):
    '''Similar a arbol_grupos_de_la_red, pero en otro formato: una lista de pares

    (id_grupo, [lista de ids de redes y subredes a las que ese grupo pertenece])

    Si se pasa el argumento ipedido, el árbol se poda cada vez que una subred no ha aceptado el pedido

    las redes y subredes se mezclan, para ciertos propósitos no es necesario
    '''
    db = current.db
    if db.grupo(ired).red:
        directos = nodos_en_la_red(ired, [ipedido])
        listas = [esquema_grupos_pedido(isubred, ipedido)
                  for isubred in directos]
        return {igrupo:lista_redes_en_grupo + [ired]
             for lista in listas
             for igrupo, lista_redes_en_grupo in lista.items()}
    else:
        return {ired:[]}

def niveles_pedido(ired, ipedido=None):
    '''Calcula el nivel de cada grupo o red, donde un grupo tiene nivel 0, y una red max(subredes) + 1

    Devuelve un diccionario {id_grupo: nivel}

    El nivel 0 corresponde a grupos, los niveles superiores a redes, de modo que incluso aunque no haya
    pedido, niveles_pedido(ired, ipedido) para una red, devolverá [[], [ired]]

    Si se pasa el argumento ipedido, el árbol se poda cada vez que una subred no ha aceptado el pedido

    Útil al cerrar pedidos, por ejemplo: podemos sumar item_grupo para nivel 0, después 1, etc,
    '''
    db = current.db
    if db.grupo(ired).red:
        directos = nodos_en_la_red(ired, [ipedido])
        niveles_subgrupos = [
            niveles_pedido(isubred, ipedido)
            for isubred in directos
        ]
        maxnivel = max((len(niveles) for niveles in niveles_subgrupos), default=1)
        niveles_grupo = [[] for _ in range(maxnivel)]
        for niveles in niveles_subgrupos:
            for j,nivel in enumerate(niveles):
                niveles_grupo[j].extend(nivel)
        niveles_grupo.append([ired])
        return niveles_grupo
    else:
        # Es decir: solo hay un igrupo, de nivel 0
        return [[ired]]

def grupos_de_la_red(ired, ipedido=0):
    '''Desciende el grafo red->grupo hasta alcanzar a todos los grupos que reciben pedidos
    de esta red.

    Si se pasa el argumento ipedido, el árbol se poda cada vez que una subred no ha aceptado el pedido
    '''
    db = current.db
    if db.grupo(ired).red:
        d = {}
        if ipedido:
            query = ((db.proxy_pedido.grupo==db.grupo.id) &
                     (db.proxy_pedido.activo==True) &
                     (db.proxy_pedido.pedido==ipedido) &
                     (db.proxy_pedido.via==ired))
        else:
            query = ((db.grupoXred.red==ired) &
                     (db.grupoXred.activo==True) &
                     (db.grupoXred.grupo==db.grupo.id))
        for row in db(query).select(db.grupo.id, db.grupo.nombre, db.grupo.red):
            if row.red:
                d.update(grupos_de_la_red(row.id, ipedido=ipedido))
            else:
                d[row.id] = row.nombre
        return d
    else:
        return {}

def redes_del_grupo(igrupo, ipedido=0):
    '''Asciende el grafo red->grupo hasta alcanzar a todas las redes que sirven pedidos
    a este grupo

    Si se pasa el argumento ipedido, el árbol se poda cada vez que una subred no ha aceptado el pedido
    '''
    db = current.db
    d = {}
    if ipedido:
        query = ((db.proxy_pedido.grupo==igrupo) &
                 (db.proxy_pedido.activo==True) &
                 (db.proxy_pedido.pedido==ipedido) &
                 (db.proxy_pedido.via==db.grupo.id))
    else:
        query = ((db.grupoXred.red==db.grupo.id) &
                 (db.grupoXred.activo==True) &
                 (db.grupoXred.grupo==igrupo))
    for row in db(query).select(db.grupo.id, db.grupo.nombre):
        d[row.id] = row.nombre
        d.update(redes_del_grupo(row.id))
    return d

def mapa_grupo(igrupo):
    db = current.db
    mapa = defaultdict(list)
    #Duda: ¿debería ordenar por otro criterio?
    for row in db(db.personaXgrupo.grupo==igrupo)  \
               .select(db.personaXgrupo.unidad, db.personaXgrupo.persona,
                       orderby=db.personaXgrupo.persona
        ):
        mapa[row.unidad].append(row.persona)
    return mapa

#def collect_users(ired):
#    db = current.db
#    s = set()
#    if db.grupo(ired).red:
#        for subred in db((db.grupoXred.red==ired) &
#                         (db.grupoXred.grupo==db.grupo.id)) \
#                      .select(db.grupo.id):
#            s.update(collect_users(subred.id))
#            return s
#    else:
#        return set(row.id
#                    for row in  db((db.personaXgrupo.grupo==ired) &
#                       (db.personaXgrupo.persona==db.auth_user.id)) \
#                    .select(db.auth_user.id))

def incluir_grupo_en_red(igrupo, ired):
    from modelos.pedidos import abre_pedidos_en_grupos_y_subgrupos
    from modelos.productores import crea_proxy_productor_subgrupos

    db = current.db
    C  = current.C
    r = db.grupoXred(grupo=igrupo, red=ired)
    if r:
        r.update_record(activo=True)
    else:
        db.grupoXred.insert(grupo=igrupo, red=ired, activo=True)

    #Ofrece los productores de la red (y de sus redes) al grupo
    #(y a sus subredes y subgrupos si el grupo es una red)
    ascendientes = set(redes_del_grupo(ired).keys())
    ascendientes.add(ired)
    iproductores = [row.id
        for row in db(db.productor.grupo.belongs(ascendientes)).select(db.productor.id)
    ]
    for iproductor in iproductores:
        db.proxy_productor.insert(productor=iproductor, grupo=igrupo, via=ired)
        crea_proxy_productor_subgrupos(igrupo, iproductor)

    #Ofrece los pedidos abiertos de la red al grupo y si igrupo es una red, tb a sus subgrupos
    ipedidos = [row.id for row in
            db((db.pedido.productor.belongs(iproductores)) &
                 (db.pedido.abierto==True)
                ).select(db.pedido.id)
    ]
    abre_pedidos_en_grupos_y_subgrupos(ipedidos, igrupo)

def sacar_grupo_de_la_red(igrupo, ired):
    db = current.db
    db((db.grupoXred.red==ired) &
       (db.grupoXred.grupo==igrupo)
    ).update(activo=False)
    #Borra los proxy_productor de la red (y de sus redes) en el grupo
    #(y a sus subredes y subgrupos si el grupo es una red)
    ascendientes = set(redes_del_grupo(ired).keys())
    ascendientes.add(ired)
    descendientes = grupos_y_subredes_de_la_red(igrupo)
    descendientes.add(igrupo)
    iproductores = [row.id
        for row in db(db.productor.grupo.belongs(ascendientes)).select(db.productor.id)
    ]
    db((db.proxy_productor.productor.belongs(iproductores)) &
       (db.proxy_productor.grupo.belongs(descendientes))
    ).delete()

    #Borra los proxy_pedido de los pedidos de la red y sus ascendientes
    #en el grupo y sus descendientes
    #¡Solo de los pedidos abiertos!
    ipedidos = [row.id for row in
            db((db.pedido.productor.belongs(iproductores)) &
                 (db.pedido.abierto==True)
                ).select(db.pedido.id)
    ]
    db((db.proxy_pedido.pedido.belongs(ipedidos)) &
       (db.proxy_pedido.grupo.belongs(descendientes))
    ).delete()

    #07-09-23: No borra las peticiones de los pedidos anteriores
    # para conservar los históricos

def unidades_en_grupo(igrupo):
    db = current.db
    return [row.unidad
       for row in db((db.personaXgrupo.grupo==igrupo) &
                     (db.personaXgrupo.activo==True)) \
                 .select(db.personaXgrupo.unidad,
                         groupby=db.personaXgrupo.unidad,
                         orderby=db.personaXgrupo.unidad)]

def textos_grupo_por_defecto(igrupo, nombre_grupo=None, host=None):
    from .gluon.html import URL
    db = current.db
    T  = current.T
    host = host or True
    textos_por_defecto = {
    'email_incorporacion_lista_de_espera':
T('''<p>Hola:</p>

<p>Te hemos incorporado a la lista de espera de nuestro grupo de consumo %(nombre_grupo)s. Para ello, hemos incorporado tus datos al programa karakolas que usamos para la gestión del grupo de consumo. Puedes consultar tus datos en la url:</p>

<a href="%(url_karakolas)s">%(url_karakolas)s</a>

<p>Si no habías entrado antes en karakolas, te hemos envíado otro email para que puedas elegir una contraseña. Tu nombre de usuaria es tu dirección de email, pero puedes cambiarlo si quieres.</p>

<p>Tu primera tarea es completar los datos de tu perfil, que tus compañeras necesitan para poder contactar contigo. Es importante que escribas tu nombre, porque se usa en las tablas de reparto.</p>

<p>Puedes borrar tu cuenta del programa y todos tus datos serán borrados, pero si lo haces, no podremos contactarte si se abre una plaza en el grupo. Para cualquier duda puedes consultar la <a href="%(url_privacidad)s">política de privacidad del sitio</a></p>

<p>Un saludo!</p>
'''), 'email_incorporacion_grupo':
T('''<p>Bienvenida!</p>

<p>Te hemos incorporado a nuestro grupo de consumo, %(nombre_grupo)s. Para que puedas participar en los pedidos, te hemos abierto una cuenta en karakolas, el programa que usamos para la gestión del grupo de consumo. La url de nuestra instalación de karakolas es:</p>

<a href="%(url_karakolas)s">%(url_karakolas)s</a>

<p>Si no habías entrado antes en karakolas, te hemos envíado otro email para que puedas elegir una contraseña. Tu nombre de usuaria es tu dirección de email, pero puedes cambiarlo si quieres.</p>

<p>Tu primera tarea es completar los datos de tu perfil, que tus compañeras necesitan para poder contactar contigo. Es importante que escribas tu nombre, porque se usa en las tablas de reparto.</p>

<p>Puedes a aprender a manejar la aplicación los tutoriales de <a href="http://karakolas.org/tutoriales/">karakolas.org</a>, o leyendo la ayuda de la propia aplicación, siguiendo los links a la "ayuda de esta página" que encontrarás en cualquier página de karakolas, pero lo mejor es que te pongas en contacto con nosotras para aprender a manejarlo.</p>

<p>Saludos</p>
''')}
    nombre_grupo = nombre_grupo or db.grupo(igrupo).nombre
    contexto = {'url_karakolas':URL(c='default',f='index.html', scheme='https',host=host),
                'url_privacidad':URL(c='default',f='index.html',args=['privacidad'], scheme='https',host=host),
                'url_recuperar_password':URL(c='default',f='user.html',args=['request_reset_password'], scheme='https',host=host),
                'nombre_grupo':nombre_grupo}
    for slug,texto in textos_por_defecto.items():
        r = db.textos_grupo(grupo=igrupo, slug=slug)
        if not r:
            db.textos_grupo.insert(grupo=igrupo, slug=slug, texto=texto%contexto)

def grupos_activos(fecha_de_corte):
    '''Devuelve una lista con los ids de los grupos que han hecho al menos un pedido después de
    la fecha_de_corte
    '''
    grupos_activos_1 = db((db.pedido.fecha_reparto>fecha_de_corte) & (db.pedido.productor==db.productor.id)).select(db.productor.grupo, groupby=db.productor.grupo)
    s1 = set(r.grupo for r in grupos_activos_1)
    grupos_activos_2 = db((db.pedido.fecha_reparto>fecha_de_corte) & (db.pedido.id==db.productoXpedido.pedido) & (db.item_grupo.productoXpedido==db.productoXpedido.id)  & (db.item_grupo.cantidad>0) ).select(db.item_grupo.grupo, groupby=db.item_grupo.grupo)
    s2 = set(r.grupo for r in grupos_activos_2)
    return s1.union(s2)

def grupos_inactivos(fecha_de_corte):
    '''Devuelve una lista con los ids de los grupos que *no* han hecho al menos un pedido después de
    la fecha_de_corte
    '''
    sall = set(r.id for r in db(db.grupo).select(db.grupo.id))
    return sall.difference(grupos_activos(fecha_de_corte))

def usuarios_activos(fecha_de_corte):
    '''Devuelve una lista con los ids de los auth_user que han hecho al menos un pedido después de
    la fecha_de_corte
    '''
    return set(r.persona for r in
        db((db.peticion.cantidad>0) &
           (db.peticion.productoXpedido==db.productoXpedido.id) &
           (db.productoXpedido.pedido==db.pedido.id) &
           (db.pedido.fecha_reparto>fecha_de_corte))
        .select(db.peticion.persona, groupby=db.peticion.persona))

def usuarios_inactivos(fecha_de_corte):
    '''Devuelve una lista con los ids de los auth_user que *no* han hecho al menos un pedido después de
    la fecha_de_corte
    '''
    sall = set(r.id for r in db(db.auth_user).select())
    return sall.difference(usuarios_activos(fecha_de_corte))
