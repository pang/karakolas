# -*- coding: utf-8 -*-
# (C) Copyright (C) 2015 Pablo Angulo
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################

from random import random

def reparto_con_redondeo(total, xs):
    '''Reparte 'total' en len(xs) partes, buscando aproximar lo más posible un reparto
    proporcional, pero usando enteros.
    Asume que len(xs)>0 y sum(xs)>0
    '''
    #TODO: (opcional) se puede hacer otra versión que use los restos acumulados
    # para repartir compensando en vez de al azar: si en el primer reparto no te toca,
    # es más probable que te toque en el reparto del siguiente producto
    total = int(total)
    total_xs = int(sum(xs))
    #sum(target) es total, pero sus entradas no son enteros
    target = [(x*total)/total_xs for x in xs]
    ys = [(x*total)//total_xs for x in xs]
    #Quedan por repartir 0<=total - sum(ys)<len(xs) unidades
    #Ordenamos los restos de mayor a menor y corregimos
    #el segundo numero desempata para que no le toque siempre al mismo
    restos = [(t-y, random(), j) for j,(t,y) in enumerate(zip(target, ys))]
    restos.sort(reverse=True)
    for _,_,j in restos[:(total-int(sum(ys)))]:
        ys[j] += 1
    return ys

def reparto_proporcional(total, xs):
    '''Reparte 'total' en len(xs) partes, buscando aproximar lo más posible un reparto
    proporcional, pero usando enteros.
    Asume que len(xs)>0 y sum(xs)>0
    '''
    total_xs = sum(xs)
    return [(x*total)/total_xs for x in xs]

# def test1():
#     L = 10
#     ls = [randint(0,10) for _ in range(L)]
#     S = sum(ls)
#     if any(x!=y for x,y in zip(ls,reparto(S,ls))):
#         return 'Error 1', ls
#     return 'ok'
#
# def test2():
#     L = 10
#     ls = [randint(0,10) for _ in range(L)]
#     for k in 2*range(L):
#         if sum(reparto(k,ls))!=k:
#             return 'Error 2', ls, k
#     return 'ok'
#
# def test3():
#     L = 10
#     ls = [randint(0,10) for _ in range(L)]
#     for k in range(L):
#         rs = reparto(k,ls)
#         if any(rs[j]>ls[j] for j in range(L)):
#             return 'Error 3', ls, k
#     return 'ok'
#
# def test4():
#     L = 10
#     ls = [randint(0,10) for _ in range(L)]
#     S = sum(ls)
#     for k in range(L):
#         rs = reparto(k,ls)
#         rs2 = reparto(k+S,ls)
#         if any(rs[j]+ls[j]!=rs2[j] for j in range(L)):
#             return 'Error 4', ls, k
#     return 'ok'
#
# if __name__ == '__main__':
#     for f in (test1, test2, test3, test4):
#         print '_'*30
#         print f()
