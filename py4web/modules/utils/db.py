# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-15 Pablo Angulo
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################

from gluon import current
from pydal.objects import Query

def copia_datos(tabla, params1, params2):
    '''Duplica los datos de tabla parametrizados por params1,
    pero los inserta usando los valores de params2.
    '''
    from itertools import chain
    represent = current.db._adapter.represent
    nombre_tabla = tabla.sql_shortref
    campos_no_copia = set(f._rname for f in chain(params1,params2))
    campos_no_copia.update(('id', '`id`'))
    campos_copia = ', '.join(f._rname
        for f in tabla if f._rname not in campos_no_copia
    )
    where_params1 = ' AND '.join(
        '(%s=%s)'%(f._rname, represent(val, f.type))
        for f,val in params1.items())
    campos_nuevos = ', '.join(f._rname  for f in params2)
    vals_nuevos = ', '.join(represent(val, f.type) for f,val in params2.items())
    #https://mariadb.com/kb/en/mariadb/insert-select/
    #aviso: no lo hemos probado en postgresql, aunque posiblemente funcione
    #http://www.postgresql.org/docs/current/static/sql-insert.html
    sql = "INSERT INTO %s (%s, %s) SELECT %s, %s FROM %s WHERE %s;"%(
        nombre_tabla, campos_nuevos, campos_copia, vals_nuevos, campos_copia,
        nombre_tabla, where_params1
    )
    current.db.executesql(sql)

def copia_registros(tabla, record_ids, nuevos_vals):
    '''Duplica los registros de la tabla, pero modifica los campos presentes en el
    diccionario vals'''
    if not record_ids: return
    represent = current.db._adapter.represent
    nombre_tabla = tabla.sql_shortref
    fields = ', '.join(f._rname
                       for f in tabla if (f not in nuevos_vals) and
                                         (f.type!='id'))
    campos_nuevos = ', '.join(f._rname  for f in nuevos_vals)
    vals_nuevos = ', '.join(represent(val, f.type) for f,val in nuevos_vals.items())
    ids = ','.join(str(int(rid)) for rid in record_ids)

    #https://mariadb.com/kb/en/mariadb/insert-select/
    #aviso: no lo hemos probado en postgresql, aunque posiblemente funcione
    #http://www.postgresql.org/docs/current/static/sql-insert.html
    sql = '''INSERT INTO %(nombre_tabla)s (%(campos_copia)s, %(campos_nuevos)s)
SELECT %(campos_copia)s, %(vals_nuevos)s FROM %(nombre_tabla)s WHERE (id IN (%(ids)s));'''%dict(
        nombre_tabla=nombre_tabla,
        campos_copia=fields,
        campos_nuevos=campos_nuevos,
        vals_nuevos=vals_nuevos,
        ids=ids
    )
    current.db.executesql(sql)

def my_bulk_insert(tabla, campos, rs):
    '''Puede suponer una mejora importante del rendimiento sobre table.bulk_insert

    rs es una lista de diccionarios, solo se usarán los campos declarados en el segundo
    argumento, y se completará con el valor por defecto si algún diccionario no tiene
    todos los campos declarados

    Si la tabla tiene campos _before_insert o _after_insert
    se limita a llamar a tabla.bulk_insert

    Atención: no devuelve la lista de ids insertados.
    Sin embargo, si que calcula los campos de tipo compute
    '''
    #Me curo en salud por si alguien reusa esta funcion en el futuro
    #En realidad se podrían recopilar los ids en mariadb usando last_insert_id
    #y luego usar esos ids para llamar a tabla._after_insert:
    #http://dev.mysql.com/doc/refman/5.6/en/innodb-auto-increment-handling.html
    if (tabla._before_insert or tabla._after_insert):
        return tabla.bulk_insert(rs)
    represent = current.db._adapter.represent
    fields = [tabla[c] for c in campos]
    for f in fields:
        fcompute = f.compute
        if fcompute:
            fname = f.name
            for r in rs:
                r[fname] = fcompute(r)
    default_vals = [(f._rname, f.type, f.default) for c,f in zip(campos, fields)]
    sql = "INSERT INTO %s (%s) VALUES %s"%(
        tabla.sql_shortref,
        ','.join(campos),
        ','.join('(%s)'%','.join(represent(r.get(campo, default), ftype)
                                 for campo, ftype, default in default_vals)
                 for r in rs)
    )
    current.db.executesql(sql)

def operator_field_starts(field, first_int, query_env={}):
    if not field.type.startswith('list:'):
        raise AttributeError("Este método sólo funciona con Field.type de tipo lst:")
    dialect = field._dialect
    second = '|%d|'%first_int
    return "(%s = '%s')"%(
        dialect.substring(field, (1,len(second))),
        second
    )

def field_starts(field, first_int):
    '''Para un campo de tipo list:integer, esta query pregunta a la base de datos
    por las filas que comienzan por cierto valor, al contrario que field.contains(),
    que permite que el valor esté en cualquier posición.

    Ejemplo:

    db( field_starts(db.operacion.parametros, 1) &
        (db.operacion.cancelada==False)
    ).count()
    '''
    db = field._db
    return Query(db, operator_field_starts, field, first_int)
