import jwt
from py4web import response, request
from ..settings import SESSION_SECRET_KEY, CORS_ORIGIN
from datetime import datetime, timedelta

active_tokens = {}

def generate_jwt(user_id):
    payload = {
        "user_id" : user_id,
        "exp" : datetime.utcnow() + timedelta(hours=1), # Token expiration
        "iat": datetime.utcnow() # Token expedition
    }
    token = jwt.encode(payload, SESSION_SECRET_KEY, "HS256")
    return token

def verify_jwt():
    token = request.cookies.get('jwt')

    if not token:
        return None
    
    try:
        payload = jwt.decode(token, SESSION_SECRET_KEY, "HS256")
        user_id = payload['user_id']
        return dict(user_id=user_id)
    except jwt.ExpiredSignatureError:
        response.status = 401
        return dict(error="Token has expired, please login again.")
    except jwt.InvalidTokenError:
        response.status = 401
        return dict(error="Invalid token, please login.")
    except Exception as error:
        print(error)
        return None
    
def authentication(func):
    def wrapper(*args, **kwargs):
        if request.method == 'OPTIONS':
            return func(*args, **kwargs)
        verification = verify_jwt()
        if not verification or 'error' in verification:
            response.status = 401
            response.headers['Access-Control-Allow-Origin'] = CORS_ORIGIN
            response.headers["Access-Control-Allow-Credentials"] = "true"
            response.headers['Access-Control-Allow-Methods'] = '*'
            response.headers['Access-Control-Allow-Headers'] = 'Content-Type, Authorization'
            if not verification:
                return dict(error="User not logged")
            return dict(error=verification['error'])
        return func(user_id=verification['user_id'], *args, **kwargs)
    return wrapper