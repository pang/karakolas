# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################

import re
from datetime import date
from decimal import Decimal, InvalidOperation
import string
import random
import unicodedata

from gluon import current

def nombre2slug(s):
    if isinstance(s, bytes):
        return nombre2slug_str(s.decode('utf8')).encode('utf8')
    return nombre2slug_str(s)

def nombre2slug_str(s):
    s = str(s).lower().strip()
    # https://github.com/django/django/blob/main/django/utils/text.py
    s = unicodedata.normalize("NFKD", s).encode("ascii", "ignore").decode("ascii")
    s = re.sub('\s+', '_', s)
    s = re.sub(r"(?u)[^-\w]", "", s)
    return s

def reduce_espacios_nombre(s):
    return re.sub('\s+', ' ', s.strip())

def parse_decimal(v):
    try:
        return Decimal(v.replace(',','.')).quantize(current.C.PRECISION_DECIMAL)
    except InvalidOperation:
        return v

def enum(*sequential, **named):
    enums = dict(list(zip(sequential, list(range(len(sequential))))), **named)
    return type('Enum', (), enums)

CAMPO_EN_FILA_CSV = re.compile('[^,"]+|"(?:[^"]|"")+"|\A(?:""|)(?=,)|(?<=,)(?:""|)(?=,)|(?<=,)(?:""|)\Z')
def separa_por_coma(s):
    cs = CAMPO_EN_FILA_CSV.findall(s)
    return [c.replace('""','_DOUBLE COMMA_').replace('"','').replace('_DOUBLE COMMA_','"')
             if c!='""' else ''
             for c in cs]

def separa_lineas(s):
    ls1 = s.splitlines()
    ls2 = []
    xs  = []
    for l in ls1:
        comillas_impares = l.count('"')%2
        if xs and comillas_impares:
            xs.append(l)
            ls2.append('\n'.join(xs))
            xs = []
        elif xs or comillas_impares:
            xs.append(l)
        else:
            ls2.append(l)
    return ls2

def ascii_safe(s, filename_safe=False):
    for c1,c2 in [('á','a'), ('é','e'), ('í','i'), ('ó','o'), ('ú','u'), ('ñ','ny')]:
        s = s.replace(c1,c2)
    # codificamos el objeto string como bytes usando el codec ascii,
    # errors='replace' hace que los caracteres desconocidos se sustituyan por ?
    if filename_safe:
        for c in ' ,-():?':
            s = s.replace(c, '_')
    return s.encode('ascii',errors='replace').decode('ascii')

def parse_fecha(s, invertida=False):
    if invertida:
        dia,mes,ano = s.split('-')
    else:
        ano, mes, dia = s.split('-')
    return date(int(ano),int(mes),int(dia))

def format_fecha(fecha):
    if isinstance(fecha, str):
        fecha = parse_fecha(fecha)
    y = fecha.year; m = fecha.month; d = fecha.day
    return '%d-%d-%d'%(d,m,y)

def denormalize(x):
    sign, digits, expo = x.normalize().as_tuple()
    if expo>=0:
        return Decimal( (sign, digits + (0,)*expo, 0 ) )
    return x.normalize()

def pprint_p(x):
    return '%s'%x.quantize(current.C.PRECISION_DECIMAL)

def pprint_c(x):
    return '%s'%denormalize(x.quantize(current.C.PRECISION_DECIMAL))

def represent(table, record):
    try:
        format = table._format
        if callable(format):
            return format(record)
        else:
            return format % record
    except TypeError:
        return str(record.id) if record else ''

def random_string(*args, **kwds):
    L = int(kwds.get('len', 32))
    return ''.join(random.choice(string.ascii_letters) for i in range(L))
