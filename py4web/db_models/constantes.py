# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################

#########################################################################
#  Constantes
# Se complica un poco porque queremos poder traducirlas con T y verlas
#tanto desde los controladores como desde los módulos
#########################################################################
# from py4web.utils import Storage
from ..common import T
from decimal import Decimal
from ..modules.gluon.html import SPAN
import datetime
date = datetime.date
from ..modules.storage import Storage

C = Storage()

#TODO: busca un mecanismo para no tener que ejecutar todo este codigo todo el rato: ¿una tabla en la base de datos seria igual de comodo para traducir?

C.MENSAJE_ERRORES = T('El formulario contiene errores. Por favor repasa los campos marcados en rojo...')
C.PRECISION_DECIMAL = Decimal('0.01')
C.MESES = {1:T('Enero'), 2:T('Febrero'), 3:T('Marzo'), 4:T('Abril'),
         5:T('Mayo'), 6:T('Junio'), 7:T('Julio'), 8:T('Agosto'),
         9:T('Septiembre'), 10:T('Octubre'), 11:T('Noviembre'), 12:T('Diciembre'),}
C.AYUDA_PRODUCTOR = T('''
<h3>Abrir pedido</h3>
<p>bla bla bla</p>
<h3>Hacer el pedido</h3>
<p>bla bla bla</p>
<h3>Para el reparto</h3>
<p>bla bla bla</p>
<h3>Pagar</h3>
<p>bla bla bla</p>
''')
C.OPCIONES_PROXY_PRODUCTOR = {
    'bloqueado': T('Productor bloqueado.'),
    'avisar': T('Productor aceptado, pero los pedidos coordinados no se aceptan automaticamente.'),
    'avisar_productos': T('Productor aceptado, pero los pedidos coordinados no se aceptan automaticamente. Los productos nuevos se aceptan automáticamente.'),
    'auto': T('Cuando se abra un pedido coordinado, se acepta automaticamente.'),
    'auto_productos': T('Cuando se abra un pedido coordinado, se acepta automaticamente. Los productos nuevos se aceptan automáticamente.'),
}
C.ICONOS_PROXY_PRODUCTOR = {
    'bloqueado': SPAN(_class='glyphicon glyphicon-stop',
                     _title=T('Este grupo ha bloqueado a este productor')),
    'avisar': SPAN(_class='glyphicon glyphicon-pause',
                  _title=T('Este grupo decidirá con calma si acepta este pedido concreto')),
    'avisar_productos': SPAN(_class='glyphicon glyphicon-pause',
                  _title=T('Este grupo decidirá con calma si acepta este pedido concreto')),
    'auto': SPAN(_class='glyphicon glyphicon-play',
                _title=T('Este grupo acepta los pedidos de este productor')),
    'auto_productos': SPAN(_class='glyphicon glyphicon-play',
                _title=T('Este grupo acepta los pedidos de este productor')),
}
C.OPCIONES_PROXY_PRODUCTOR_DEFAULT = 'avisar_productos'
C.FECHA_FANTASMA = date(1970,1,1)
C.NOMBRES_TIPOS_COLUMNA={
    'decimal(9,2)':T('Número'),
    'string': T('Texto'),
    'boolean':T('Valor Sí o No')
}
#C.TIPOS_COLUMNA=['decimal(9,2)', 'string', 'boolean']
C.TIPOS_COLUMNA=list(C.NOMBRES_TIPOS_COLUMNA.keys())

C.COBRO_DESGLOSADO        = 0
C.COBRO_AGRUPADO          = 1
C.REPARTO_FACIL           = 2
C.DESGLOSADO_POR_GRUPOS   = 3
C.COBRO_TOTALES           = 4
C.DISCREPANCIAS           = 5
C.TIPOS_INFORME_GRUPO = {
    C.COBRO_DESGLOSADO:       T('Cobro por personas'),
    C.COBRO_AGRUPADO:         T('Cobro por unidades'),
    C.REPARTO_FACIL:          T('Reparto fácil'),
#    COBRO_TOTALES:          T('Precio a pagar a cada productor, por personas'),
}
C.TOOLTIPS_INFORME_GRUPO = {
    C.COBRO_DESGLOSADO:       T('Se listas precios, cantidades, costes desglosados y totales por personas, unidad y productor, para los productores seleccionados. Personas agrupadas por unidad.'),
    C.COBRO_AGRUPADO:         T('Se listas precios, cantidades, costes desglosados y totales, unidad y productor, para los productores seleccionados.'),
    C.REPARTO_FACIL:          T('Se listan las cantidades pedidas de cada productor por unidad, para los productores seleccionados.'),
#    COBRO_TOTALES:          T('Precio a pagar a cada productor, por personas'),
}
C.TIPOS_INFORME_RED = {
    C.DESGLOSADO_POR_GRUPOS:  T('Informe para la red, desglosado por grupos'),
    C.DISCREPANCIAS:  T('Grupos con discrepancias en las cantidades recibidas')
}
C.TOOLTIPS_INFORME_RED = {
    C.DESGLOSADO_POR_GRUPOS:  T('Se listan las cantidades pedidas de cada productor por grupo, para los productores seleccionados.'),
    C.DISCREPANCIAS:  T('Se listan los grupos que han declarado haber recibido una cantidad diferente a la que la red declara haber enviado, desgloado por productos')
}
C.TIPOS_INFORME = dict(list(C.TIPOS_INFORME_GRUPO.items()) + list(C.TIPOS_INFORME_RED.items()))

############### email ####################
C.EMAIL_PEDIDOS_COORDINADOS   = T('<html><h1>Se han abierto varios pedidos coordinados para el grupo %(nombre_grupo)s desde la red %(nombre_red)s</h1><p>Has recibido este mensaje porque eres admin del grupo %(nombre_grupo)s, que forma parte de la red %(nombre_red)s.</p><p>Cuando desde una red se abren pedidos, algunos se aceptan de forma automática en este grupo, mientras que otros pedidos no se activan, pero se crea una entrada en la lista de pedidos para que los podáis activar a discreción. Finalmente, algunos pedidos se ignoran porque habéis bloqueado al productor. Puedes cambiar la configuración local de cada productor en la página de productores, editando la "Info de %(nombre_grupo)s".</p><p>A continuación detallamos la lista de pedidos incluyendo su estado actual:</p>%(lista_pedidos)s</html>')
C.EMAIL_PEDIDOS_COORDINADOS_ASUNTO = T('Se han abierto varios pedidos coordinados desde la red %s')
C.EMAIL_PEDIDO_COORDINADO_AUTO   = T('<html><h1>Se ha abierto un pedido coordinado de %(nombre_productor)s desde la red %(nombre_red)s</h1><p>El pedido se ha aceptado de forma automática en tu grupo (%(nombre_grupo)s) por la configuración local que tenéis del productor %(nombre_productor)s, y ahora todas las personas del grupo pueden pedir. Puedes cambiar la configuración local del productor en la página de productores, editando la "Info de %(nombre_grupo)s" en la fila de %(nombre_productor)s</p></html>')
C.EMAIL_PEDIDO_COORDINADO_AVISAR = T('<html><h1>Se ha abierto un pedido coordinado de %(nombre_productor)s desde la red %(nombre_red)s</h1><p>Has recibido este mensaje porque eres admin del grupo %(nombre_grupo)s, que forma parte de la red %(nombre_red)s.</p><p>El pedido de %(nombre_productor)s no se ha aceptado en este grupo por la configuración local que tenéis del productor %(nombre_productor)s. Puedes aceptar el pedido en la sección de gestión de pedidos, y cambiar la configuración local del productor en la página de productores, editando la "Info de %(nombre_grupo)s" en la fila de %(nombre_productor)s</p></html>')
C.EMAIL_PEDIDO_COORDINADO_ASUNTO = T('Se ha abierto un pedido coordinado de %s')
C.EMAIL_AVISO_BLACKLIST = T('Algunos emails a admins de este grupo no se han podido enviar: %s')

#############################################

C.FORMULA_COSTE_PEDIDO_COORDINADO_POR_DEFECTO = 'COSTE_PEDIDO_RED*COSTE_UNIDAD_EN_PEDIDOS_RED/COSTE_GRUPO_EN_PEDIDOS_RED'
C.FORMULA_COSTE_PEDIDO_COORDINADO_POR_DEFECTO_RED = 'COSTE_PEDIDO_RED*COSTE_GRUPO_EN_PEDIDOS_RED/COSTE_RED_EN_PEDIDOS_RED'
C.FORMULA_SOBREPRECIO_PEDIDO_COORDINADO_POR_DEFECTO = 'precio_red'

#El texto es distinto si se trata de una red o no
C.AYUDA_FORMULA_COSTE_EXTRA_PEDIDO = {
# Para una red
True:T('''
## Fórmula para el coste extra del pedido

El coste extra sólo se puede calcular después de haber cerrado el pedido. Cada **grupo** debe pagar una cierta cantidad.
El total a pagar por cada grupo puede depender de variables como el coste del pedido total que no se puede conocer a priori.

La **fórmula** es una expresión aritmética que depende de varias **variables** que se escriben siempre en mayúsculas.

Si tu red usa la contabilidad, aparecerá también una fórmula para el coste extra que se debe pagar al productor.
En la fórmula para el coste a pagar al productor sólo se pueden usar las variables marcadas con (*):

- ``COSTE_GRUPO``: Coste total del pedido de todo el grupo.
- ``COSTE_RED (*)``: Coste total del pedido de todos los grupos de la red.
- ``CANTIDAD_GRUPO``: Cantidad total del pedido de todo el grupo, medido en las mismas unidades que al pedir.
- ``CANTIDAD_RED (*)``: Cantidad total del pedido de toda la red, medido en las mismas unidades que al pedir.
- ``GRUPO_HA_PEDIDO``: Esta variable vale 1 si el grupo ha participado en este pedido, y 0 en otro caso.
- ``GRUPOS_EN_PEDIDO (*)``: Número de grupos que han pedido algo en este pedido.
- ``COSTE_GRUPO_EN_PEDIDOS_RED``: Coste total del pedido del grupo para todos los pedidos de la red en esa fecha.
- ``GRUPO_HA_PEDIDO_A_LA_RED``: Esta variable vale 1 si el grupo ha participado en algún pedido de la red en esa fecha, y 0 en otro caso.

También se pueden usar variables definidas en la página para *Editar los productos*, en los *Campos avanzados*:
- Si se incluye un **campo global**, se sustituye la variable por el valor del campo tal cual.
- Si se usa una **columna extra**, que toma un valor distinto para cada producto, se multiplica la cantidad pedida de cada producto por el valor de esa columna, y se suma para obtener el total. Por ejemplo, si hay una columna extra de nombre 'peso', podemos usar la variable:
 -- ``PESO_GRUPO``: Peso total de todos los productos pedidos por el grupo
 -- ``PESO_RED (*)``: Peso total de todos los productos pedidos por la red

**Importante**: los campos usados deben ser numéricos! La expresión aritmética debe ser correcta, etc...

### Ejemplos
- ``0.10*COSTE_GRUPO``:code[python] Esta fórmula asigna al grupo un coste de transporte y gestión del 10%% del total del pedido.
- ``25*GRUPO_HA_PEDIDO``:code[python] Se pasa un gasto de 25 euros a los grupos que han pedido algún producto en este pedido.
- ``TRANSPORTE_KG*PESO_GRUPO+COSTE_FIJO*GRUPO_HA_PEDIDO``:code[python] Esta fórmula asigna un coste fijo más una parte variable que depende del peso total, haciendo referencia a dos variables globales: `TRANSPORTE_KG` y `COSTE_FIJO`, y a una columna extra `PESO`, que es necesario definir en la página de productos del pedido.
- ``25*GRUPO_HA_PEDIDO_A_LA_RED``:code[python] Se pasa un gasto de 25 euros a cada grupos que ha pedido algún producto en algún pedido de la red en esta fecha.
- ``25*GRUPO_HA_PEDIDO_A_LA_RED/GRUPOS_EN_PEDIDOS_RED``:code[python] Se reparten 25 euros a pagar a partes iguales entre todos los grupos que han pedido algún producto en algún pedido de la red en esta fecha.
- ``25``:code[python] Se pasa un gasto de 25 euros a todos los grupos para los que se abra el pedido, que llegará a los grupos aunque el pedido no tenga productos y sea el único pedido de la red para esa fecha.
- ``MAX(20, 0.05*COSTE_RED)``:code[python] Si la red usa la contabilidad, esta fórmula podría servir para pagar al productor 5%% del coste del pedido, con un minimo de 20.
- ``10*COSTE_GRUPO_EN_PEDIDOS_RED/COSTE_RED_EN_PEDIDOS_RED``:code[python] Se reparten 10 euros entre los grupos que participan en este reparto, proporcionalmente a su gasto en todos los pedidos de este reparto.
'''),
False:T('''
## Fórmula para el coste extra del pedido

El coste extra sólo se puede calcular después de haber cerrado el pedido. Cada **unidad** debe pagar una cierta cantidad.
El total a pagar por cada unidad puede depender de variables como el coste del pedido de la unidad que no se puede conocer a priori, incluso de variables como el coste del pedido de todo el grupo que no se puede conocer hasta el día del reparto.

La **fórmula** es una expresión aritmética que depende de varias **variables** que se escriben siempre en mayúsculas.

Si el grupo usa la contabilidad, aparecerá también una fórmula para el coste extra que se debe pagar al productor.
En la fórmula para el coste a pagar al productor sólo se pueden usar las variables marcadas con (*):

- ``COSTE_UNIDAD``: Coste total del pedido de la unidad.
- ``COSTE_GRUPO (*)``: Coste total del pedido de todo el grupo.
- ``CANTIDAD_UNIDAD``: Cantidad total del pedido de la unidad, medido en las mismas unidades que al pedir (*atención*: puedes terminar sumando kg y número de tarros).
- ``CANTIDAD_GRUPO (*)``: Cantidad total del pedido de todo el grupo, medido en las mismas unidades que al pedir.
- ``HA_PEDIDO``: Esta variable vale 1 si la unidad participa en el pedido, y 0 si no.
- ``UNIDADES_EN_PEDIDO (*)``: Número de unidades que han pedido algo en este pedido.
- ``UNIDADES_EN_GRUPO (*)``: Número de unidades activas en el grupo, aunque no hayan pedido. Al cerrar el pedido, todas las unidades tendrán que afrontar este coste extra.

También se pueden usar variables definidas en la página para *Editar los productos*, en los *Campos avanzados*:
- Si se incluye un **campo global**, se sustituye la variable por el valor del campo tal cual.
- Si se usa una **columna extra**, que toma un valor distinto para cada producto, se multiplica la cantidad pedida de cada producto por el valor de esa columna, y se suma para obtener el total. Por ejemplo, si hay una columna extra de nombre 'peso', podemos usar dos variables:
 -- ``PESO_UNIDAD``: Peso total de todos los productos pedidos por la unidad
 -- ``PESO_GRUPO (*)``: Peso total de todos los productos pedidos por el grupo

### Ejemplos
- ``10*COSTE_UNIDAD/COSTE_GRUPO``:code[python] Esta fórmula reparte un gasto de transporte de 10 euros entre todas las unidades proporcionalmente al gasto de cada unidad.
- ``TRANSPORTE*HA_PEDIDO/UNIDADES_EN_PEDIDO``:code[python] Esta fórmula reparte un gasto de transporte definido en el campo global ``TRANSPORTE`` a partes iguales entre todas las unidades que han participado, independientemente de cuánto hayan pedido.
- ``10*COSTE_UNIDAD/COSTE_GRUPO + 2*HA_PEDIDO``:code[python] Esta fórmula añade a la anterior un coste de 2 euros para cada unidad que participa en el pedido
- ``TRANSPORTE*PESO_UNIDAD/PESO_GRUPO``:code[python] Esta fórmula reparte un gasto de transporte definido en el campo global ``TRANSPORTE`` proporcionalmente al peso de los productos.
- ``2*UNIDADES_EN_PEDIDO``:code[python] Si la red usa la contabilidad, esta fórmula podría servir para pagar a un productor 2 euros por cada unidad que participe en el pedido.
- ``MAX(20, 0.05*COSTE_GRUPO)``:code[python] Si el grupo usa la contabilidad, esta fórmula podría servir para pagar al productor 5%% del coste del pedido, con un minimo de 20.

## Pedidos coordinados

Adicionalmente, en la fórmula para el coste extra que pagarán las unidades del grupo en un pedido coordinado, también se pueden usar las siguientes variables:

- ``COSTE_PEDIDO_RED``: el coste de transporte y gestión que la red pide a todo el grupo. ''Atención'', desde la red tb pueden aplicar un porcentaje fijo a cada precio u otro mecanismo para repercutir sus costes, pero si usan este mecanismo, es vuestra responsabilidad repartir este coste de alguna manera entre las unidades participantes.
- ``COSTE_UNIDAD_EN_PEDIDOS_RED``: el coste para esta unidad de todos los productos para este reparto ofrecidos por la red que gestiona este pedido.
- ``HA_PEDIDO_A_LA_RED``: vale 1 si esta unidad ha pedido algún producto para este reparto que provenga de la misma red que este pedido.
- ``COSTE_GRUPO_EN_PEDIDOS_RED``: el coste total para todo el grupo de todos los pedidos para este reparto que provienen de la misma red que este pedido.
- ``UNIDADES_EN_PEDIDOS_DE_LA_RED``: el número de unidades del grupo que han pedido para este reparto algún producto que provenga de la red que gestiona este pedido.

**Importante**: los campos usados deben ser numéricos! La expresión aritmética debe ser correcta, etc...

### Ejemplos
- ``COSTE_PEDIDO_RED*HA_PEDIDO/UNIDADES_EN_PEDIDO``:code[python] El coste de transporte y gestión que la red pide al grupo se reparte a partes iguales entre las unidades que participan en el pedido.
- ``COSTE_PEDIDO_RED*COSTE_UNIDAD_EN_PEDIDOS_RED/COSTE_GRUPO_EN_PEDIDOS_RED``:code[python] La fórmula recomendada por ''La ecomarca'' para repartir los costes de transporte. Estos costes sólo se incluyen una vez, en un pedido de la red, y se reparten proporcionalmente al gasto de cada unidad en todos los otros pedidos de la red.
- ``COSTE_PEDIDO_RED*HA_PEDIDO_A_LA_RED/UNIDADES_EN_PEDIDOS_DE_LA_RED``:code[python] El COSTE_PEDIDO_RED se reparte a partes iguales entre todas las unidades que participaron en alguno de los pedidos ofrecidos por la red.
''')
}
