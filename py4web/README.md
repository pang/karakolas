# Karakolas py4web

En el inicio del desarrollo de este proyecto, la aplicación **karakolas** se encuentra desarrollada en [web2py](http://www.web2py.com/). El objetivo principal es el de actualizar la aplicación a [py4web](https://py4web.com). Como objetivo secundario se encuentra la remodelación visual y la de las funcionalidades de la aplicación.

El primer commit parte del scaffold por defecto de py4web con versión ```1.20231115.1``` que va a ir evolucionando hasta convertirse en la aplicación de karakolas.