# utils.py

from .common import db

def isAdmin(user_id, group_id):
    """True si el usuario tiene permisos para acceder al grupo"""
    print(user_id)
    print(group_id)
    adminMember = db((db.auth_membership.user_id == user_id) & (db.auth_group.id == db.auth_membership.group_id) & (db.auth_group.role == 'admins_%d'%group_id )).select()
    if adminMember:
        return True
    return False