# Coexistence py4web and web2py

When trying to use the same database for web2py and py4web, we have realized that both use the same table to manage the users named `auth_user` but it's different for each application. We need to create the `auth_user` table with the fields from both frameworks.

## Tables auth_user

First of all, we're going to see the fields from karakolas web2py and the default py4web

Fields web2py karakolas:
    id, first_name, last_name, email, username, password, registration_key, reset_password_key, registration_id, direccion, telefono, informar_nuevos_grupos

> The last 3 fields (direccion, telefono, informar_nuevos_grupos) were added manually, they are not from web2py framework.

Default fields py4web:
    id, username, email, password, first_name, last_name, sso_id, action_token

Later, we need to add the fields missing from both cases into the `auth_user` description. On web2py was on `web2py/models/20_db_auth_etc.py`

```python
auth.settings.extra_fields['auth_user']= [
    Field('sso_id', 'string', default=''),
    Field('action_token', 'string', default='')
```

And on py4web was on `py4web/common.py`

```python
auth.extra_auth_user_fields = [
    Field('direccion','string'),
    Field('telefono','string'),
    Field('registration_key', 'string'),
    Field('reset_password_key', 'string'),
    Field('registration_id', 'string'),
    Field('informar_nuevos_grupos', 'boolean', default=False)
]
```

We don't want both applications trying to create a database, so py4web will use `migrate_enabled=False`, the documentation about py4web says: _This is the recommended behavior when two apps share the same database. Only one of the two apps should perform migrations, the other should disable them._ So we used the following DAL (database abstraction layer):

```python
db = DAL(
    settings.DB_URI,
    migrate_enabled=False
)
```

## Database test

We need to add the new 2 fields into the SQL, to do so we'll unzip de file `web2py/private/testdata/lotr_deep/lotr_deep.sql.gz`, add the modifications that we want, and zip it again. Those modifications were adding the 2 fields `sso_id` and `action_token` in the creation of the table `auth_user` and then modify the user inputs adding null on both fields.

Also changed the `karakolas_test_data_lotr.csv` in order to maintain consistency.

### Change database

If you want to do changes on the example database, you need first to unzip the database.

```bash
cd web2py/private/testdata/lotr_deep
gzip -d lotr_deep.sql.gz
```

Then make the changes on the sql file (`lotr_deep.sql`) and then zip it again.

```bash
gzip lotr_deep.sql
```
