# Tablas karakolas de web2py a py4web

En este documento se explica cómo se han migrado los componentes de web2py a py4web para reutilizar el código ya creado de web2py.

Inicialmente se copiaron los modelos de la base de datos de la carpeta `web2py/models` a `py4web/db_models` y también los modules de `web2py/modules` a `py4web/modules`. Al hacer esto encontramos dos errores distintos, el primero era que en web2py los módulos de la carpeta `modules` se importaban directamente como un paquete de python, hubo que modificar en el py4web el archivo `py4web/common.py` para añadir la carpeta modules al `PYTHON_PATH` y mantener las importaciones como estaban en el web2py.

```python
# Agrega el directorio `modules` al PYTHONPATH
modules_path = os.path.join(os.path.dirname(__file__), 'modules')
if modules_path not in sys.path:
    sys.path.append(modules_path)
```

Por otro lado estaba el incovneniente del paquete `gluon`. Este paquete se instala por defecto con el web2py y había que instalarlo en el py4web para poder utilizar las funcionalidades que ya estaban con el mismo. Para ello se crea la carpeta `py4web/modules/gluon` con todos los componentes del mismo y se sube al repositorio.
