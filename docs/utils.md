# Utils

Some useful notes to help in development

## react / ionic

### npm install

#### How-to : add new libraries to the frontend

#### Current setup

- A volume is created in the `docker-compose` as:

```sh
    volumes:
    - "../client-v:/app:rw" # mapping to local ionic project directory
    - /app/node_modules
    ...
```

If using dockerize images to develop:

we need to get INTO the container:

```sh
docker exec -it devops-ionic-1 sh
npm install [whatever]
npm install axios
npm install react-hook-form
```

- We may need to do chown /node_modules folder
```sh
sudo chown admin1:admin1 /node_modules
npm install react-query
```

### Routing - navigating

#### Router component
- *IMPORTANT* use `exact` inside `<Route>` for parent route (e.g. `records/`) if we are using sub-routes:
```js
  
  <Route exact path="/records">
    <RecordsList />
  </Route>
  <Route path="/records/:r_id/edit">
    <RecordEditor form_type='edit' endpoint='pedidos/' />
  </Route>

```

#### Programatically

- useNavigation:
```js
import { useNavigate } from "react-router-dom";

...

  let navigate = useNavigate();
 
  ...

  const goSomewhere = () => {
    navigate("/somewhere");
  };

```


- useHistory:
```sh

import { useHistory } from "react-router-dom";
...

  let history = useHistory();

  const goSomewhere = () => {
    history.push("/somewhere");
  };

```
## py4web

### controllers

Most basic GET:
- (just a couple of columns in a table)
- Attributes of `select()` fn are the column names of a table:
```python
rows = db().select(db.auth_user.id, db.auth_user.email)
```

- Use `ALL` to select all columns:
```python
db().select(db.person.ALL)
```

More in [py4web documentation - select](https://py4web.com/_documentation/static/en/chapter-07.html#select-command)

- `limitby`: Only a few (10) records
```python
# First 10 records
db().select(db.person.ALL, limitby=(0,10)

# Newest 10 records
rows = db().select(db.person.ALL, 
                   orderby=db.person.id,
                   limitby=(0,10)
)
```




