# API Intermedia - mutations

_Lógica para hacer inserts/updates/deletes (POST/PUT/DELETE)_

Ampliación/continuación de lo descrito en [api_basica.md](api_basica.md)

## Backend
- Creamos un endpoint muy básico (y artesanal) 
    - en `py4web/controllers.py`
- Con las rutas
    - `/pedidos`
    - `/pedidos/<id>`
- Y los métodos
    - GET
    - POST
    - PUT
    - DELETE
    
### Varios
- Se un mapeo extra-básico exponiendo la columna `productor` como `id_productor` en la API
    
### Notas a investigar:
- si devolvemos un `204`, un cliente de prueba JS (usando `fetch()`) se queda "esperando"

## Frontend
- Enriquecemos el `adapters/api.ts` con 
    - método `_mydatareq()` basado en [axios]()
    - export `_mut()` exponiendo el hook `useMutation`
        - refactorizado para aceptar tanto `POST` como `PUT`

### Records
- Creamos una página nueva completa `Records.tsx` para montar el esqueleto de cualquier componente que interactúe con la API


### useMutation
- Para ejecutar / disparar la query, hay que llamar al método `mutate()` del objeto devuelto por `useMutation`
    - en nuestro caso por `_mut` (antes `_mpost`)

### `<Input>` and `<Form>` components
- Empezaremos probando con `react-hook-forms` y el hook `useForm`

### KForm
Para el tutorial bueno:
- Un componente `<KInput/>` basado en la referencia
    - (de momento) le hemos conseguido usando `register` en lugar del `Control`
    - Podemos pasar el tipo de input
    - TODO : ver cómo pasar el warning del label que ahora es obligatorio en el `<IonInput>`

Creamos componente reusables a partir de [este tutorial](https://www.smashingmagazine.com/2020/08/forms-validation-ionic-react/):
    - Hay que sustituir la parte de `as` por un función en la prop `render`
- Un componente `<KForm/>` que abstrae un nivel más y acepta:
    - un titulo básico
    - un array de campos `kfields[]`
        - deja abierto una prop `component`, donde se le puede sustituir el `<IonInput>` que va por defecto por cualquier otro elemento más sofisticado
    - (opcional) un set de valores pre-rellenados `given_values`
    - Props del botón submit:
        - El texto del botón (ej. "Crear" / "Creando..."
        - un boolean (enable/disable) para desactivarlo mientras la API del back está procesando
        - Estas props se manejan con `useState()` en el elemento (padre) que contiene al `<KForm/>`
            - (y lo gestiona)

#### Submit
"Pasamos" 2 piezas de lógica al componente `<KForm>`:
- Por un lado el handler del botón "Submit": prop `submitFn()`
- Y también (implícitamente) la función `onSuccess()`
    - esta se especifica al declarar el objeto que hace uso de `useMutation` (en nuestro caso `_mut`)

- En este caso de ejemplo, en el `onSuccess()` (cuando la API devuelve un OK 200):
    - 1. Resetamos el estado del botón (rehabilitamos)
    - 2. Mostramos un toast con el mensaje del back (si lo hay)
    - 3. Volvemos a la pantalla anterior (`useHistory.back()`)


### PUT - Modificación
Desde la pantalla de lista (`<Records.tsx>`)
- Añadimos un botón de 'editar' al lado del de borrar
    - Utilizamos un href para que se vea la url
        - y `e.preventDefault()` para evitar la redirección simple con el click (y poder pasar datos)
    - Navegamos al editor con `useHistory` : `history.push('/route', data)`
- Que abrirá el mismo componente de crear un `/record/new` (en el futuro será un `<KEditor>` genérico)
    - Pero con los datos pre-cargados:
        - llegan en `useLocation().state`
        - y se pasan con la prop `given_values`
        - **IMPORTANTE** para evitar problemas con los PUTs : pasar correctamente el value dentro del registre
            - el onChange también se customiza distinto: (ver al final)
    - Y un handler de `useMutation` para `PUT` (`_mput()`)
- Esto viene según la prop `form_type` (`'new'` o `'edit'`)
    - Se determinan algunos campos de la UI (new record / update record)
    - y el tipo de petición (POST/PUT)
- (TODO) Hacer un get si es un PUT pero vienen los datos vacíos:
    - ej. si el usuario abre directamente una url de edición `/records/14217edit` (o le da al F5)

#### Ruta
- Añadimos un path en `App.tsx`
- Y navegación con useHistory desde el botoncito de editar
    - ej `/records/1574/edit`
    - *IMPORTANTE* usar el `exact` dentro del `<Route>` para `records/` si vamos a usar sub-rutas :)
- Para acceder los parámetros, usamos: (router-dom v5)`useParams`
```
const { url_id } = useParams()
```

#### Register

Para un `<IonInput>`: **`onIonChange`**
```js

<IonInput
  { ...register(name, {
    value: given_value,
  })} 
  onIonChange={on_change_fn}
/>
```
Para un `<input>` normal: `onChange` **dentro del register**

```js
<input
    type={type}
    { ...register(name, {
        value: given_value,
        onChange: on_change_fn,
    })} 
/>
```


## Readings
- [ionic](https://ionicreacthub.com/blog/using-react-hook-form-validation-ionic-react)
- [useMutation PRO](https://profy.dev/article/react-query-usemutation)
- [Forms with](https://www.smashingmagazine.com/2020/08/forms-validation-ionic-react/):

