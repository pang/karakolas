# API Básica

Montamos una lógica básica para gestionar los accesos al backend (`py4web`)

En este apartado se describen los pasos para implementar un GET.

[Descripción de API intermedia - useMutation](./mutations.md)


Lecturas sobre react:
[Definite Guide - refactors varios](https://dev.to/wolfflucas/the-definitive-guide-to-make-api-calls-in-react-2c1i)
 
next -error handling: 
[centralizing error handling](https://itnext.io/centralizing-api-error-handling-in-react-apps-810b2be1d39d)

Buena pinta:
[React Query](https://dev.to/abeinevincent/data-fetching-from-an-api-using-react-query-usequery-hook-explained-in-plain-english-4eei)


## Solución con React Query (useQuery)

Pasos:
- `npm install react-query`

- meter el `<QueryClientProvider>` envolviendo la APP completa: a nivel `main.tsx`

- incluir el `useQuery`
    - primero en la page que lo va a cargar
    
- Desactivamos a nivel global el reload on focus (dispara un refetch cada vez que se cambia de pestaña/ventana)
    
- Para poder utilizar el boton de "reload":
    - utilizamos el `refetch` que devuelve

- Para evitar el 3 x retry:
    - se le pasa el parámetro `retry` dentro del objeto de opciones:

```js
  ...
  useQuery( ... { retry: false } )
  ...
```
   
- Sacamos toda la lógica del useQuery a un utils: `adapters/api.tsx`
    - añadimos un param `display: Boolean` para poder debuggear las responses que nos interesen
        - (false por defecto)
    - lo usaremos como `_queryget(...)`

- Uso de datos
     - En la tab2 ponemos un "indicador del estado de la query" (mostrando el id)
     - Incluimos otro useQuery (_queryget) en el componente `Tab2.tsx`
        - con la opción `enabled: false`
        - utilizando el mismo queryKey ('jokes') 
            - se puede entender como la misma "colección"
            - (podríamos utilizar refetch) cuando quisiéramos
        - Una vez el componente `<Jokes>` lanza  
            - se rellena el indicador e
        - Y cuando la refresca ("botón another one")

- Para gestionar errores y toasts:
    - Usamos `react-hot-toast`
        - Incluimos un componente <Toaster> a alto nivel:
            - de momento va en `main.tsx`
        - Y en el componente que queramos que lo lance hacemos el `toast.error( ... )`
            - de momento en el `adapters/api.ts`
```js
import toast from 'react-hot-toast'

...

const handleError = (error: unknown, query: Query) => {
    ...
    toast.error(`${error.message}`, { position: 'bottom-center'} )
    ...
}
```
        
- Y pasamos esa función como opción al `useQuery`:

```js
  ...
  onError : handleError,
  ...
```
    
- Aquí se incluye lógica custom pero el error se lanza y se propaga automáticamente hasta el <QueryClientProvider>
    - y se ejecuta **también** (y antes por maravillas javascript) la lógica que le pasemos al `onError` del `queryCache` que definamos en el `main.tsx`.


## Solución previa (useQuery)

Empezamos usando la API fetch que viene por defecto en javascript moderno
- [MDN Docs](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch)

- [Ejemplos interesantes](https://bobbyhadz.com/blog/javascript-get-response-status-code-fetch)

- En el componente `Tab3.tsx` dejamos un ejemplo con una función custom `loadData()`
    - sigue accesible (de momento) en [/tab3](http://localhost:5173/tab3)

- con `useEffect()` se hace la primera llamada


### Siguientes pasos: 
- Investigar sobre ErrorBoundaries en React
- Definir cómo querríamos gestionar llamadas y errores en karakolas
