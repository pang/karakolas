# Toast Component

Objetivo: mostrar un pequeño mensaje durante unos segundos si la API da algún error

- el formato de objetos de error está definido [aquí](./api_basica.md)

Para montar un pequeño toast y mostrar errores en el caso de que el back no responda bien

## Solución escogida (react-hot-toast)
- Lo montamos directamente sobre `adapters/api.tsx`
- Más detalle en [api_basica](./api_basica.md)

## Solución anterior (IonToast)
1. Creamos el componente react `KToast.tsx`
    - bajo la carpeta `components`
    
2. Añadimos un `<IonToast>` y le abrimos una prop `text: string;`

3. Lo configuramos para que se abra programáticamente:
- NO es tan sencillo...
    - a. utilizamos la property `isOpen` del componente `IonToast`
    - b. Definimos la prop (
    - c. Utilizamos un `useState` con su `setIsToastOpen` en la página
    - d.  definimos también y le pasamos un handler para que se ejecute el onDismiss
        - y en ese handler (`dismissHandler`)
        - en la interface de props:
            `dismissHandler: () => void;`
            
Dejamos un ejemplo funcionando en el `Tab3.tsx`:
- sigue accesible (de momento) en [/tab3](http://localhost:5173/tab3)
- El botón "Fail" siempre da un 500 (endpoint `/error` del py4web)

Ejemplo:
```js
<KToast message={toastProps.message} open={toastProps.isToastOpen} dismissHandler={dismissHandler}/>
```

- Tal y como está montado, habría que incluir el `<KToast>` y las fns **en cada componente** que interactúe con una API.
