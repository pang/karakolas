# Manejo de usuarios

En este archivo se recoge el funcionamiento de los usuarios en py4web y su relación con la BBDD.

## Setup

- hay que crear (manualmente?) un archivo auth.key en la carpeta `py4web/private`:

```sh
cd py4web/
mkdir -p private && mktemp -u XXXXXXXXXXXXXX > private/auth.key
```

## Generación y edición de usuarios

Py4web utiliza un objeto `Auth` para la gestión de usuarios y habilita una API para la misma. El problema encontrado con este objeto y web2py es la gestión de contraseñas. Web2py utiliza sha256 y py4web utiliza pbkdf2. Se puede utilizar el pbkdf2 para cifrar mediante sha256 e incluso utilizar la misma clave para cifrar pero ocurre un problema con py4web y es que no permite autenticar las claves generadas del web2py por lo que se utilizará el objeto `Auth` pero habrá que personalizar la gestión de los ususarios.

Cuando se introduce un usuario en la BBDD hay que hacerlo mediante la función `insert`, esta función no va a implementar los requisitos definidos en el objeto `Auth` al introducir directamente el elemento en la BBDD por lo que insertar un usuario nuevo hay que cifrar la contraseña previamente.

## Endpoint py4web /users

Se generan los endpoints `/users` y `/users/<user_id>` para el manejo de los ususarios soportando las peticiones GET, POST, PUT y DELETE.

Para hacer consultas a la BBDD se puede utilizar la sentencia `db().select()` y añadir dentro de `select` los campos que queremos consultar. Además, dentro de `db` se puede aplicar el filtro que queremos que haya. Cuando queremos introducir un elemento en la base de datos se hará mediante `db.<table>.insert()` y dentro del `insert` estarán los campos necesarios para añadir el elemento en la tabla.

## Login (WIP)
