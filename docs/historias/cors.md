# CORS and API

CORS básicamente es un mecanismo de seguridad *del navegador* para que una página web no pueda hacer llamadas a otros servidores (muy resumido)

En nuestro caso:
- el front está corriendo en `localhost:5173` (ionic servido con vite)
- el back de py4web lo corremos en `localhost:7000`
- cuando desde el react intentamos hacer una llamada a la API de py4web, nos dice:

```
Cross-Origin Request Blocked: The Same Origin Policy disallows 
reading the remote resource at http://localhost:7000/test/. 
(Reason: CORS header ‘Access-Control-Allow-Origin’ missing). 
Status code: 405.
```

## How to Fix

Cómo se resuelve:
- hay que decirle al back que todo OK con devolver llamadas desde una web que llamaremos "ionic"
    - en nuestro caso `localhost:5173`
    
### Django
    
En django hay un middleware específico
- se añade el primero de la lista
- y se puede definir una CORS-WHITELIST en el settings:

```python

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware', 
    ...
]

...

CORS_ORIGIN_WHITELIST = [
    'http://localhost:4000',
    'http://localhost:5173'
]
```

### py4web
En py4web (segunda solución):
- En el `common.py`
    - se importa el cors de py4web
    - se inicializa
    - se utiliza en el resto de decoradores y en el auth
- En el `controllers.py`
    - se utiliza el cors como parte de la acción

`common.py`:
```python
from py4web.utils.cors import CORS

...

cors=CORS(origin=settings.CORS_ORIGIN)
auth.enable(uses=(session, T, db, cors), env=dict(T=T))

unauthenticated = ActionFactory(db, session, T, flash, auth, cors)
authenticated = ActionFactory(db, session, T, flash, auth.user, cors)
```

Definimos la variable `CORS_ORIGIN` en `settings.py`
```python
# CORS origin
CORS_ORIGIN = 'http://localhost:5173'
```


`controllers.py`:
```python
from .common import cors, ...

...

@action("my_endpoint", method=["GET", "POST", "OPTIONS"])
@action.uses(cors)
def my_endpoint():
    # tu lógica del endpoint
    return dict(message="Hello, CORS!")

```

En py4web (primera solución):
- en el `controllers.py`
- definimos un CORSMIddleware
- lo instanciamos
- anotamos los endpoints con `@cors`, e incluimos la request `OPTIONS`

```python

class CORSMiddleware(Fixture):
    def __call__(self, next):
        def middleware():
            response.headers['Access-Control-Allow-Origin'] = 'http://localhost:5173'  # Cambia '*' por el dominio del frontend si deseas restringir el acceso
            response.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, DELETE, OPTIONS'
            response.headers['Access-Control-Allow-Headers'] = 'Content-Type, Authorization'
            if request.method == 'OPTIONS':
                response.status=200
                return 
            return next()
        return middleware

...

cors = CORSMiddleware()

...

@action("my_endpoint", method=["GET", "POST", "OPTIONS"])
@cors
def my_endpoint():
    # tu lógica del endpoint
    return dict(message="Hello, CORS!")

```

