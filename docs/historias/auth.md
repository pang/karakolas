# Autenticación

Se utilizará JWT (JSON Web Tokens) para la autenticación entre el cliente y el servidor utilizando cookies modificando la cabecera `Set-Cookie` almacenando el token.

## Py4web

Se crean los endpoints `/api/login` y `/api/logout` para la identificación del usuario y el cierre de sesión del mismo respectivamente. 
- En el login comprueba si el usuario es el correcto y en ese caso genera el token jwt y lo añade a las cookies del navegador, 
    - si es incorrecto, responde con un error 401 - Invalid credentials. 
- En cuanto al login simplemente elimina la cookie del navegador reemplazandola por una string vacía.

Para la gestión de usuarios se usan:
- un archivo [authentication.py](/py4web/modules/authentication.py) con las funciones necesarias para generar el JWT y verificarlo 
- una función que se utilizará como decorador en los controladores denominada `@authentication` que verificará el usuario y devolverá el mismo como parámetro de la siguiente función o devolverá un error si el usuario no se ha podido verificar.

Además, se inlcuye el endpoint `/get_user` en el que se devuelve el nombre del usuario, su id y los grupos a lo que pertenece, indicando si es administrador o no.

## Ionic

Al utilizar las cookies para almacenar el JWT hay que añadir en las peticiones el campo `credentials: 'include'`. 

Se genera un [Contexto](https://react.dev/learn/passing-data-deeply-with-context) para gestionar el inicio y el cierre de la sesión almacenando el nombre del usuario en una variable. Este contexto está definido en en [AuthContext.tsx](/client-v/src/context/AuthContext.tsx), hay que importar el componente `<AuthProvider>` y envolver a la aplicación con el mismo:

```html
      <AuthProvider>
        <App />
      </AuthProvider>
```

Una vez se ha hecho eso, se puede acceder al resto de funciones desde los componentes de dentro de la aplicación al importar la función `useAuth` del Contexto:

```ts
import { useAuth } from '../context/AuthContext';

const { user, group, setGroup, setUser, login, logout } = useAuth()

// Usuario, se actualiza al hacer login o logout
console.log(user)
```

El usuario y el grupo tienen los siguientes campos:
```js
user: {
    "groups": [{ "id":<group_id>, "nombre":<group_name>}, ...],
    "user_id": <user_id>,
    "username": <username> 
},
group: {
    "id":<group_id>,
    "isAdmin": <boolean>
    "name": <groupname>
}
```

Se genera una nueva vista llamada `Login.tsx` bajo la ruta `/login` para el inicio de sesión del mismo, existen actualmente 3 botones:

- Login: para iniciar sesión con el usuario que se haya indicado
- Sign up (**TODO**): te debería llevar a un formulario para introducir un nuevo usuario
- Prueba login: manda una petición de prueba a un endpoint del py4web denominado `prueba_auth` y muestra por consola si el usuario está loggeado o no.

También se ha generado el componente `Header` bajo la ruta [/client-v/src/components/Header.tsx](/client-v/src/components/Header.tsx) para redirigir al inicio de sesión o al cierre de la misma y mostrar el usuario. Además, también muestra el título de la página cuando se pasa como prop del mismo como en el siguiente ejemplo:

```html
<Header title="Login"/>
```

Se crea un componente denominado [ProtectedRoute](/client-v/src/components/ProtectedRoute.tsx) para usarse igual que el elemento `Router` de React pero comprobando previamente que el usuario está con la sesión iniciada cuando intenta acceder a un recurso y, en caso de no estarlo, redirigir al login mencionado anteriormente. 

```html
<ProtectedRoute exact path="/producers">
    <Producers />
</ProtectedRoute>
```

## TODO:

- Ver si se quiere añadir más seguridad en el logout de py4web. Ejemplos: guardando los token de los usuarios que ya han cerrado la sesión.

- Añadir el `SESSION_SECRET_KEY` del archivo settings mediante el fichero `.env`.

- Crear formulario de Sign up

- Comprobación de si el usuario es administrador o no de ese grupo

- Envío de formulario al pulsar enter en la contraseña o en el nombre de usuario

- Redirección del login haciendo el refresh en la página (cambia la URL pero no renderiza de nuevo)

