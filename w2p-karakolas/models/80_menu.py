# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################

from menu import build_menu, build_user_menu

response.menu = build_menu()
response.menu_usuario = build_user_menu(auth.user_id)

#########################################################################

#response.title = ' '.join(word.capitalize() for word in request.application.split('_'))
response.subtitle = T('Coordinación de grupos de consumo')

## read more at http://dev.w3.org/html5/markup/meta.name.html
response.meta.author = 'karakolas.org'
response.meta.description = 'Herramienta para la gestión de grupos de consumo y para la coordinación de grupos en redes'
response.meta.keywords = 'grupo de consumo, logística, pedidos, coordinación, python, web2py'
response.meta.generator = 'Web2py Web Framework, jquery, texlive, corefive file manager, xlutils, numpy y otros'
response.meta.copyright = 'Karakolas es software libre: Affero GPL. Más detalles en https://karakolas.org'

## your http://google.com/analytics id
response.google_analytics_id = None

#########################################################################

