# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################

from bloques_contabilidad import TweakedRadioWidget

#Las cadenas de karakolas están en castellano, pero tb usa cadenas en ingles de gluon
#asi que hay que traducir las cadenas y no se puede descomentar la linea siguiente
#T.set_current_languages('es','es-es')

#Consistencia: opcion_ingresos es NO_CONT sii opcion_pedidos es NO_CONT
db.define_table('grupo',
    Field('nombre','string', length=255, unique=True, writable=False),
    Field('direccion'),
    Field('descripcion', 'text'),
    Field('red', 'boolean', default=False, writable=False),
    Field('lista_de_espera', 'boolean', default=True),
    #El telefono es un string pq permite agrupar las cifras, poner prefijos, dos teléfonos, etc.
    #Además, vete a saber cuántas cifras tienen los teléfonos en cada país.
    #La mayoría de grupos no necesitan un teléfono, pero las redes típicamente sí lo querrán.
    Field('telefono', 'string'),
    Field('discreto', 'boolean', default=False),
    Field('exportar_oferta', 'boolean', default=False),
    Field('prueba', 'boolean', default=False, writable=False),
    Field('opcion_ingresos', 'integer',
          requires=IS_IN_SET(CONT.OPCIONES_INGRESOS),
          default=CONT.OPCIONES_INGRESOS.NO_CONT,
          widget=TweakedRadioWidget.widget),
    Field('opcion_pedidos', 'integer',
          requires=IS_IN_SET(CONT.OPCIONES_PEDIDOS),
          default=CONT.OPCIONES_PEDIDOS.NO_CONT,
          widget=TweakedRadioWidget.widget),
    Field('dias_pedido_reciente', 'integer', default=14),
    Field('activo', 'boolean', default=True),
    format='%(nombre)s'
    )

db.define_table('grupoXred',
    Field('grupo', db.grupo),
    Field('red', db.grupo),
    Field('activo', 'boolean', default=True)
    )
db.grupoXred._mariadb_index_command = 'ALTER TABLE grupoXred ADD UNIQUE (grupo,red)'

db.define_table('personaXgrupo',
    Field('grupo', db.grupo),
    Field('persona', db.auth_user),
    Field('unidad', 'integer', notnull=True, requires=IS_INT_IN_RANGE(1,1000)),
#Las personas que se van del grupo no se borran, se desactivan
    Field('activo', 'boolean', default=True)
    )
db.personaXgrupo._mariadb_index_command = 'ALTER TABLE personaXgrupo ADD UNIQUE (grupo, persona)'

db.define_table('personaXlistaespera',
    Field('grupo', db.grupo),
    Field('persona', db.auth_user),
    Field('fecha_de_solicitud', 'datetime'),
    Field('comentario', 'string'),
#Las personas que se van de la lisa de espera se desactivan
#asi no se pierde la antiguedad en ningun caso (por ejemplo si se incorpora por error
#y luego se borra)
    Field('activo', 'boolean', default=True)
    )
db.personaXlistaespera._mariadb_index_command = 'ALTER TABLE personaXlistaespera ADD UNIQUE (grupo, persona)'

db.define_table('productor',
    Field('nombre','string', length=255, notnull=True, requires=IS_NOT_EMPTY()),
    Field('email', 'string', length=255, requires=IS_EMPTY_OR(IS_EMAIL())),
    Field('grupo', db.grupo, writable=False),
    #El telefono es un string pq permite agrupar las cifras, poner prefijos y de todo.
    Field('telefono', 'string'),
    Field('direccion','string'),
    # Esta información es pública, se mostrará a todas las subredes y grupos
    Field('descripcion','text'),
    # Comentarios internos que no se compartirán con las subredes y grupos
    Field('comentarios_internos', 'text'),
    # Esta información es pública, se mostrará a todas las subredes y grupos
    Field('info_pedido', 'text', default=C.AYUDA_PRODUCTOR),
    Field('random_string', 'string', compute=K.random_string, readable=False, writable=False),
    Field('activo', 'boolean', default=True),
    #Los campos siguientes son solo para guardar el ultimo valor usado en un pedido a este productor
    #TODO: podríamos prescindir de ellos y leerlo todo del pedido
    Field('formula_precio_final', 'text', default='precio_base'),
    Field('formula_precio_productor', 'text', default='precio_base'),
    Field('formula_coste_extra_pedido', 'text', default='0'),
    Field('formula_coste_extra_productor', 'text', default='0'),
    Field('reglas', 'text', default='', writable=False),
    format='%(nombre)s')
#db.productor.email.requires = IS_EMAIL()
db.productor._mariadb_index_command = 'ALTER TABLE productor ADD INDEX (grupo, nombre)'

def format_proxy(r):
    return 'Comentarios de %s a %s'%(r.grupo.nombre, r.productor.nombre)

db.define_table('proxy_productor',
    Field('productor', db.productor, requires = IS_IN_DB(db, db.productor, '%(nombre)s')),
    Field('grupo', db.grupo, requires = IS_IN_DB(db, db.grupo, '%(nombre)s')),
    # via es la red que ofrece este productor al grupo de forma directa, no la red primigenia
    Field('via', db.grupo, requires = IS_IN_DB(db, db.grupo, '%(nombre)s')),
    # Comentarios internos que no se compartirán con las subredes y grupos, si es subred
    Field('comentarios', 'text'),
    # Comentarios para los subgrupos, si es subred
    Field('comentarios_subgrupos', 'text'),
    Field('aceptado', 'boolean', default=True), #TODO: DEPRECATED
    Field('estado', 'string', requires=IS_IN_SET(C.OPCIONES_PROXY_PRODUCTOR),
                              default=C.OPCIONES_PROXY_PRODUCTOR_DEFAULT),
    #Esta formula permite aumentar el precio de cada producto de un pedido coordinado
    #No metemos variables por ahora, veamos cómo evoluciona...
    Field('formula_sobreprecio', 'text', default=C.FORMULA_SOBREPRECIO_PEDIDO_COORDINADO_POR_DEFECTO),
    # distinta fórmula si es red o no
    # TODO: parece que si lo pongo con un compute luego no se puede editar en
    # productores/edita_productor_coordinado :-/
    Field('formula_coste_extra_pedido', 'text'),
    format=format_proxy)
db.proxy_productor._mariadb_index_command = 'ALTER TABLE proxy_productor ADD UNIQUE (grupo, productor)'

def formula_coste_extra_pedido_por_defecto(fields, id):
    '''valor por defecto para proxy_productor.formula_coste_extra_pedido,
    si no se ha fijado ya'''
    if 'formula_coste_extra_pedido' in fields:
        return
    pp = db.proxy_productor[id]
    if pp and db.grupo(pp['grupo']).red:
        formula = C.FORMULA_COSTE_PEDIDO_COORDINADO_POR_DEFECTO_RED
    else:
        formula = C.FORMULA_COSTE_PEDIDO_COORDINADO_POR_DEFECTO
    pp.update_record(formula_coste_extra_pedido=formula)
db.proxy_productor._after_insert.append(formula_coste_extra_pedido_por_defecto)

db.define_table('categorias_productos',
    Field('nombre', 'string', length=255, unique=True),
    Field('codigo', 'integer', unique=True),
    format='%(nombre)s'
    )

db.define_table('producto',
    Field('productor', db.productor,requires=IS_IN_DB(db,db.productor,'%(nombre)s')),#, readable=False, writable=False),
    #El resto de campos de producto ya no se usan, y serán eliminados próximamente
    Field('nombre','string', length=255),
    Field('descripcion','string', length=2048, requires=IS_LENGTH(2048)),
    Field('categoria', db.categorias_productos,
          requires=IS_IN_DB(db,db.categorias_productos,'%(nombre)s',
                        error_message=T('Por favor escoje una categoría'),
                        zero=T('Por favor escoje una categoría'))),
    #A granel si se pueden pedir fracciones (típicamente, se vende por kg)
    #Manojos, botellas, latas, pollos... no son a granel
    Field('granel','boolean', default=False),
    Field('activo','boolean', default=True),
    Field('temporada','boolean', default=True),
    #los campos siguientes se ponen por conveniencia, pero tienen un valor
    #distinto en cada pedido
    #el valor para el proximo pedido se guarda en productoXpedido
    #pero en el pedido "fantasma" del 1-1-1970
    #'precio_base' es el precio base, sin aplicar la formula
    Field('precio_base','decimal(9,2)',
          requires = IS_DECIMAL_IN_RANGE(-1e6,1e6,dot=',')),
    #'precio_final' es el precio despues de aplicar la formula
    Field('precio_final','decimal(9,2)',
          requires = IS_DECIMAL_IN_RANGE(-1e6,1e6,dot=',')),
    #'precio_productor' es el precio que hay que pagar al productor
    #solo es util si se usa la contabilidad
    Field('precio_productor','decimal(9,2)',
          requires = IS_DECIMAL_IN_RANGE(-1e6,1e6,dot=',')),
    Field('destacado','boolean', default=False),
    Field('fila','integer', readable=False),
    Field('hoja','integer', readable=False),
    Field('columna_peticion','integer', readable=False),
    format='%(nombre)s')

def format_pedido(r):
    return 'Pedido de %s del %s'%(r.productor.nombre, K.format_fecha(r.fecha_reparto))

db.define_table('pedido',
    Field('productor', db.productor,
          requires = IS_IN_DB(db, db.productor, '%(nombre)s'),
          readable=False, writable=False),
    Field('fecha_activacion', 'date', requires=IS_DATE(),
          compute=lambda r:request.now.date),
    Field('info_cierre', 'text', default=T('El pedido se cierra el día...')),
    Field('fecha_reparto', 'date', requires=IS_DATE()),
    Field('abierto', 'boolean', default=False),
    Field('usa_plantilla', 'boolean', default=False),
    Field('xls', 'upload', default=None, autodelete=True, uploadseparate=True),
    Field('xls_filename', readable=False, writable=False),
    Field('formula_precio_final', 'text'),
    Field('formula_precio_productor', 'text'),
    Field('formula_coste_extra_pedido', 'text'),
    Field('formula_coste_extra_productor', 'text'),
    Field('coste_extra_productor', 'decimal(9,2)', default=Decimal()),
    Field('reglas', 'text'),

    # pendiente_consolidacion: Si es True, se considerará activo hasta que se declaren incidencias.
    # Si el pedido es coordinado, se considerará pedido activo mientras exista algun grupo pte de consolidar
    # refleja si hay productos a pesar.
    Field('pendiente_consolidacion', 'boolean', default=False),
    # Nuevos flags versión 4
    # discrepancia_interna es True si para algún producto
    #  item_grupo.cantidad != item_grupo.cantidad_recibida
    Field('discrepancia_interna', 'boolean', default=False),
    # discrepancia_interna_asumida es True si para algún producto
    #  item_grupo.cantidad != item_grupo.cantidad_recibida
    # pero el grupo lo asume en su contabilidad, o bien ajusta los precios de mascara_producto
    # pero manteniendo item_grupo.cantidad != item_grupo.cantidad_recibida
    Field('discrepancia_interna_asumida', 'boolean', default=False),
    # discrepancia_productor es True si para algún producto
    #  item_grupo.cantidad_red != item_grupo.cantidad_recibida
    Field('discrepancia_productor', 'boolean', default=False),
    # solo para pedidos coordinados, si el grupo es una red
    # discrepancia_descendiente es True si y solo si para algún descendiente directo,
    #  proxy_pedido.discrepancia_ascendiente es True
    Field('discrepancia_descendiente', 'boolean', default=False),
    # DEPRECATED
    # pendiente_incidencias_unidades es True si no se ha consolidado o
    # si es coordinado y hay discrepancias declaradas por la Red con algun grupo
    Field('pendiente_incidencias_unidades', 'boolean', default=False),
    # DEPRECATED
    #(solo para pedidos coordinados)incidencias_declaradas es True si hay discrepancias
    #declaradas por algun Grupo. Se actualiza al declarar incidencias, o importar la tabla del reparto.
    Field('incidencias_declaradas', 'boolean', default=False),
    # DEPRECATED
    # discrepancia_asumida estara a 0 cuando no hay discrepancia con ningún grupo
    #  o cuando hay discrepancias, pero al menos una no ha sido asumida
    # discrepancia_asumida estara a 3 cuando hay discrepancias con al menos un grupo
    #   y todas han sido asumidas
    #   más detalles en el campo discrepancia_asumida de proxy_pedido
    Field('discrepancia_asumida', 'integer', default=0),
    # problemas_calculo_coste_extra: Si es True, el pedido se mostrará como pedido activo hasta que se calcule el coste
    # extra sin errores
    # Si el pedido es coordinado, este booleano es True si hubo problemas al calcular el
    # coste extra, sea a nivel de red o de alguno de los grupos
    Field('problemas_calculo_coste_extra', 'boolean', default=False),
    # Esta sin confirmar cuando hemos importado una hoja de calculo y todavia no hemos
    # pasado los productos al pedido fantasma
    Field('sin_confirmar', 'boolean', default=False),
    format=format_pedido
    )
db.pedido._mariadb_index_command = 'ALTER TABLE pedido ADD INDEX (productor,fecha_reparto)'

# El coste por participar en el pedido
#  se calcula usando la formula_coste_extra_pedido del pedido
# Consistencia: coste_pedido.unidad es una unidad de coste_pedido.grupo
db.define_table('coste_pedido',
    Field('pedido', db.pedido),
    Field('grupo', db.grupo),
    Field('unidad', 'integer'),
    Field('coste', 'decimal(9,2)'),
    )
db.coste_pedido._mariadb_index_command = 'ALTER TABLE coste_pedido ADD INDEX (pedido, grupo, unidad)'

db.define_table('proxy_pedido',
    Field('pedido', db.pedido, requires = IS_IN_DB(db, db.pedido, format_pedido)),
    Field('grupo', db.grupo, requires = IS_IN_DB(db, db.grupo, '%(nombre)s')),
    # Indica la red-federacion-etc que ofrece el pedido directamente al 'grupo' de arriba
    Field('via', db.grupo, requires = IS_IN_DB(db, db.grupo, '%(nombre)s')),
    Field('comentarios', 'text', default='El pedido se cierra el jueves XX de XXXXXX por la noche.'),
    Field('fecha_reparto', 'date', requires=IS_DATE()),
    Field('activo', 'boolean', default=True),
    # Esta formula permite aumentar el precio de cada producto de un pedido coordinado
    # No metemos variables por ahora, veamos cómo evoluciona...
    # Desde v4, puede ser 'manual'
    Field('formula_sobreprecio', 'text'),
    # La formula_coste_extra_pedido del pedido calcula la cantidad que tiene que pagar cada grupo a la red
    # La formula_coste_extra_pedido del proxy_pedido calcula la cantidad que tiene que pagar cada unidad
    Field('formula_coste_extra_pedido', 'text'),
    # El coste extra que nos piden desde la red, no se puede calcular hasta que no se cierra el pedido
    Field('coste_red', 'decimal(9,2)', default=Decimal() ),
    # Nuevos flags versión 4
    # discrepancia_interna es True si para algún producto
    #  item_grupo.cantidad != item_grupo.cantidad_recibida
    Field('discrepancia_interna', 'boolean', default=False),
    # discrepancia_interna_asumida es True si para algún producto
    #  item_grupo.cantidad != item_grupo.cantidad_recibida
    # pero el grupo lo asume en su contabilidad, o bien ajusta los precios de mascara_producto
    # pero manteniendo item_grupo.cantidad != item_grupo.cantidad_recibida
    Field('discrepancia_interna_asumida', 'boolean', default=False),
    # discrepancia_ascendiente es True si para algún producto
    #  item_grupo.cantidad_red != item_grupo.cantidad_recibida
    Field('discrepancia_ascendiente', 'boolean', default=False),
    # solo para pedidos coordinados, si el grupo es una red
    # discrepancia_descendiente es True si y solo si para algún descendiente directo,
    #  proxy_pedido.discrepancia_ascendiente es True
    Field('discrepancia_descendiente', 'boolean', default=False),
    # DEPRECATED
    # Si es True, el pedido coordinado se considerará activo hasta que se declaren incidencias.
    # refleja si hay productos a pesar.
    Field('pendiente_consolidacion', 'boolean', default=False),
    # DEPRECATED
    # pendiente_incidencias_unidades es True si hay discrepancias declaradas por la Red
    # Se actualiza al declarar incidencias, o importar la tabla del reparto.
    Field('pendiente_incidencias_unidades', 'boolean', default=False),
    # DEPRECATED
    # incidencias_declaradas es True si hay discrepancias declaradas por el Grupo
    # Se actualiza al declarar incidencias, o importar la tabla del reparto.
    Field('incidencias_declaradas', 'boolean', default=False),
    # DEPRECATED
    # discrepancia_asumida estara a 0 cuando
    #  - no hay discrepancias: el grupo dice haber recibido la misma cantidad que la red dice haber enviado.
    #  - hay discrepancias, y no las ha asumido ni el grupo ni la red.
    # discrepancia_asumida estara a 1 cuando el grupo asume la version de la red
    #   se utilizara item_red para la contabilidad entre la red y el productor
    #   item_red para la contabilidad entre el grupo y la red
    #   item_grupo para la contabilidad entre el grupo y las unidades
    # discrepancia_asumida estara a 2 cuando la red asume la version del grupo
    #   se utilizara item_red para la contabilidad entre la red y el productor
    #   item_grupo para la contabilidad entre la red y el grupo
    #   item_grupo para la contabilidad entre el grupo y las unidades
    Field('discrepancia_asumida', 'integer', default=0),
    # problemas_calculo_coste_extra: Si es True, el pedido coordinado se mostrará como pedido activo hasta que se calcule
    # el coste extra sin errores
    Field('problemas_calculo_coste_extra', 'boolean', default=False),
    #    format=format_proxy_pedido
    )
db.proxy_pedido._mariadb_index_command = 'ALTER TABLE proxy_pedido ADD UNIQUE (grupo, pedido)'

def formulas_para_proxy_pedido_por_defecto(fields, id):
    pp = db.proxy_pedido(id)
    proxy_productor = db.proxy_productor(productor=pp.pedido.productor, grupo=pp.grupo)

    valores_por_defecto = {}
    for campo in ('formula_sobreprecio', 'formula_coste_extra_pedido'):
        if campo not in fields:
            v = proxy_productor[campo]
            valores_por_defecto[campo] = v
    pp.update_record(**valores_por_defecto)

db.proxy_pedido._after_insert.append(formulas_para_proxy_pedido_por_defecto)

def compute_slug_field(r):
    #Fallamos con toda la intencion si no se ha actualizado el campo nombre:
    #web2py reconoce el KeyError y no actualiza el campo slug
    return K.nombre2slug(r['nombre'])

#Consistencia: pedido.productor==producto.productor
db.define_table('productoXpedido',
    Field('pedido', db.pedido,
          requires = IS_IN_DB(db, db.pedido, format_pedido)),
    Field('producto', db.producto,
          requires = IS_IN_DB(db, db.producto, '%(nombre)s')),
    Field('nombre','string', length=255, notnull=True),
    Field('slug','string', length=255, compute=compute_slug_field),
    Field('descripcion','string', length=2048, requires=IS_LENGTH(2048)),
    Field('categoria', db.categorias_productos,
          requires=IS_IN_DB(db,db.categorias_productos,'%(nombre)s',
                        error_message=T('Por favor escoje una categoría'),
                        zero=T('Por favor escoje una categoría'))),
    #A granel si se pueden pedir fracciones (típicamente, se vende por kg)
    #Manojos, botellas, latas, pollos... no son a granel
    Field('granel','boolean', default=False),
    #Se marca este booleano si es necesario pesarlo tras recibir el pedido
    #Por ejemplo, un melón no se compra a granel pero hay que pesarlo
    Field('pesar','boolean', default=False),
    Field('activo','boolean', default=True),
    Field('temporada','boolean', default=True),
    #'precio_base' es el precio base, sin aplicar la formula
    Field('precio_base','decimal(9,2)',
          requires = IS_DECIMAL_IN_RANGE(-1e6,1e6,dot=',')),
    #'precio_final' es el precio despues de aplicar la formula
    Field('precio_final','decimal(9,2)',
          requires = IS_DECIMAL_IN_RANGE(-1e6,1e6,dot=',')),
    #'precio_productor' es el precio que hay que pagar al productor
    #solo es util si se usa la contabilidad
    Field('precio_productor','decimal(9,2)',
          requires = IS_DECIMAL_IN_RANGE(-1e6,1e6,dot=',')),
    Field('destacado','boolean', default=False),
    Field('fila','integer', readable=False),
    Field('hoja','integer', readable=False),
    Field('columna_peticion','integer', readable=False),
    )
db.productoXpedido._mariadb_index_command = (
    'ALTER TABLE productoXpedido ADD UNIQUE (pedido, producto)',
    'ALTER TABLE productoXpedido ADD UNIQUE (pedido, slug, activo)')

db.define_table('campo_extra_pedido',
    Field('pedido', db.pedido, requires=IS_IN_DB(db,db.pedido,format_pedido)),
    Field('nombre', 'string', length=255),
    Field('valor', 'decimal(9,2)'),
    format='%(nombre)s'
    )
db.campo_extra_pedido._mariadb_index_command = 'ALTER TABLE campo_extra_pedido ADD INDEX (pedido, nombre)'

db.define_table('columna_extra_pedido',
    Field('pedido', db.pedido, requires=IS_IN_DB(db,db.pedido,format_pedido)),
    Field('nombre', 'string', length=255),
    Field('tipo', 'string', length=64, requires = IS_IN_SET(C.TIPOS_COLUMNA),
                            default  = 'decimal(9,2)'),
    format='%(nombre)s'
    )
db.columna_extra_pedido._mariadb_index_command = 'ALTER TABLE columna_extra_pedido ADD INDEX (pedido, nombre)'

#Consistencia: columna.pedido.productor==producto.productor
db.define_table('valor_extra_decimal_pedido',
    Field('columna', db.columna_extra_pedido, requires=IS_IN_DB(db,db.columna_extra_pedido)),
    #Este requires solo sirve para appadmin, y de hecho internamente hay momentos en que
    #toma el valor null, pero solo temporalmente
    Field('productoXpedido', db.productoXpedido, requires=IS_IN_DB(db,db.productoXpedido)),
    #Es practico que tengan la referencia al producto para copiar facilmente filas
    #de un pedido a otro
    Field('producto', db.producto, requires=IS_IN_DB(db,db.producto)),
    Field('valor', 'decimal(9,2)', default=Decimal()),
    format='%(valor)s'
    )
db.valor_extra_decimal_pedido._mariadb_index_command = 'ALTER TABLE valor_extra_decimal_pedido ADD INDEX (columna, productoXpedido)'

#Consistencia: columna.pedido.productor==producto.productor
db.define_table('valor_extra_string_pedido',
    Field('columna', db.columna_extra_pedido, requires=IS_IN_DB(db,db.columna_extra_pedido)),
    Field('productoXpedido', db.productoXpedido, requires=IS_IN_DB(db,db.productoXpedido)),
    Field('producto', db.producto, requires=IS_IN_DB(db,db.producto)),
    Field('valor', 'string', default=''),
    format='%(valor)s'
    )
db.valor_extra_string_pedido._mariadb_index_command = 'ALTER TABLE valor_extra_string_pedido ADD INDEX (columna, productoXpedido)'
#Pueden ser utiles dos indices? Creo que no, solo se busca por producto una vez, así que
#no compensa el tiempo invertido en crear el indice
#db.valor_extra_string_pedido._mariadb_index_command = 'ALTER TABLE valor_extra_string_pedido ADD UNIQUE (columna, producto)'

#Consistencia: columna.pedido.productor==producto.productor
db.define_table('valor_extra_boolean_pedido',
    Field('columna', db.columna_extra_pedido, requires=IS_IN_DB(db,db.columna_extra_pedido)),
    Field('productoXpedido', db.productoXpedido, requires=IS_IN_DB(db,db.productoXpedido)),
    Field('producto', db.producto, requires=IS_IN_DB(db,db.producto)),
    Field('valor', 'boolean', default=False),
    format='%(valor)s'
    )
db.valor_extra_boolean_pedido._mariadb_index_command = 'ALTER TABLE valor_extra_boolean_pedido ADD INDEX (columna, productoXpedido)'

TABLAS_VALORES_EXTRA_PEDIDO = {
    'decimal(9,2)' : db.valor_extra_decimal_pedido,
    'string'       : db.valor_extra_string_pedido,
    'boolean'      : db.valor_extra_boolean_pedido
}

db.TABLAS_VALORES_EXTRA_PEDIDO = TABLAS_VALORES_EXTRA_PEDIDO

# #Esta tabla almacena el último valor de mascara_producto, para que funcione como producto y las demas...
# # ENE2021: no hemos usado esta tabla porque no la necesitamos, aunque podría ser interesante para
# #  mejorar el rendimiento :-/
# db.define_table('proxy_producto',
#     Field('producto', db.producto, requires = IS_IN_DB(db, db.producto)),
#     Field('grupo', db.grupo, requires = IS_IN_DB(db, db.grupo, '%(nombre)s')),
#     Field('aceptado', 'boolean', default=True),
#     #el campo precio no es necesario porque recalcularemos usando la formula
# #    Field('precio','decimal(9,2)',
# #          requires = IS_DECIMAL_IN_RANGE(-1e6,1e6,dot=',')),
# )
# db.proxy_producto._mariadb_index_command = 'ALTER TABLE proxy_producto ADD UNIQUE (grupo, producto)'

db.define_table('mascara_producto',
    Field('productoXpedido', db.productoXpedido, requires = IS_IN_DB(db, db.productoXpedido)),
    Field('grupo', db.grupo, requires = IS_IN_DB(db, db.grupo, '%(nombre)s')),
    Field('aceptado', 'boolean', default=True),
    #el campo precio permite a cada grupo modificar los precios de los productos del pedido coordinado
    #usando la formula_sobreprecio
    Field('precio','decimal(9,2)',
          requires = IS_DECIMAL_IN_RANGE(-1e6,1e6,dot=',')),
    #información redundante, pero que nos conviene tener a mano:
    # puede ser productoXpedido.precio_final o db.mascara_producto.precio del nivel superior
    Field('precio_red','decimal(9,2)',
          requires = IS_DECIMAL_IN_RANGE(-1e6,1e6,dot=',')),
)
db.mascara_producto._mariadb_index_command = 'ALTER TABLE mascara_producto ADD UNIQUE (grupo, productoXpedido)'

db.define_table('peticion',
    Field('productoXpedido', db.productoXpedido,
          requires = IS_IN_DB(db, db.productoXpedido)),
    Field('persona', db.auth_user),
    Field('grupo', db.grupo),
    Field('cantidad', 'decimal(9,2)',
          requires = IS_EMPTY_OR(IS_DECIMAL_IN_RANGE(0,1e6,dot=',')),
          default=Decimal(0)),
    Field('precio_total', 'decimal(9,2)'),
    Field('precio_red', 'decimal(9,2)'),
    Field('precio_productor', 'decimal(9,2)')
    )
db.peticion._mariadb_index_command = 'ALTER TABLE peticion ADD UNIQUE (grupo, productoXpedido, persona)'

#TODO: esta tabla esta cayendo en desuso, pero o bien la elimino,
#o bien le pongo otro campo 'grupo'
db.define_table('comentarioApeticion',
    Field('pedido', db.pedido),
    Field('persona', db.auth_user),
    Field('texto', 'text'),
    format='%(texto)s'
    )
db.comentarioApeticion._mariadb_index_command = 'ALTER TABLE comentarioApeticion ADD UNIQUE (pedido, persona)'

#Cuando el pedido se cierra, item se rellena automáticamente agregando las cantidades
#de peticion, pero a partir de ese momento se pueden declarar incidencias, y ambas tablas
#pueden seguir vidas paralelas. Sin embargo, tenemos que garantizar que existe una entrada
#en la tabla para un ppp y una unidad, siempre que hay una entrada en peticion para ese
#ppp y una persona de esa unidad
db.define_table('item',
    Field('productoXpedido', db.productoXpedido,
          requires = IS_IN_DB(db, db.productoXpedido)),
    Field('unidad', 'integer'),
    #pensando en pedidos compartidos, donde el pedido no lleva la info del grupo
    Field('grupo', db.grupo),
    Field('cantidad', 'decimal(9,2)',
          requires = IS_EMPTY_OR(IS_DECIMAL_IN_RANGE(0,1e6,dot=',')),
          default=Decimal(0)),
    # La suma de las cantidades que la unidad ha pedido,
    # no se modifica después de cerrar el pedido, aunque haya incidencias
    # es interesante para no tener que hacer la suma sobre la marcha en vista_totales_red y similares
    Field('cantidad_pedida', 'decimal(9,2)',
          requires = IS_EMPTY_OR(IS_DECIMAL_IN_RANGE(0,1e6,dot=',')),
          default=Decimal(0)),
    Field('precio_total', 'decimal(9,2)'),
    Field('precio_pedido', 'decimal(9,2)'),
    # item.precio_red es el precio pagado al productor si el pedido es propio,
    #  y el precio pagado a la red si el pedido es coordinado. En caso de pedido coordinado,
    #  el grupo no tiene acceso al precio del productor, no lo necesita para nada
    Field('precio_red', 'decimal(9,2)'),
    # TODO DEPRECATED TO BE REMOVED
    Field('precio_productor', 'decimal(9,2)')
    )
db.item._mariadb_index_command = 'ALTER TABLE item ADD UNIQUE (grupo, productoXpedido, unidad)'

#Se aplica el mismo comentario que está antes de item
db.define_table('item_grupo',
    Field('productoXpedido', db.productoXpedido,
          requires = IS_IN_DB(db, db.productoXpedido)),
    # Puede ser un grupo o una red intermedia
    Field('grupo', db.grupo),
    # La suma de las cantidades que el grupo ha repartido (item.cantidad)
    #  o que la red ha repartido (item_grupo.cantidad_red)
    Field('cantidad', 'decimal(9,2)',
          requires = IS_EMPTY_OR(IS_DECIMAL_IN_RANGE(0,1e6,dot=',')),
          default=Decimal(0)),
    # La suma de las cantidades que el grupo ha pedido,
    # o que los grupos de la red ha pedido
    # no se modifica después de cerrar el pedido, aunque haya incidencias
    # es interesante para no tener que hacer la suma sobre la marcha en vista_totales_red y similares
    Field('cantidad_pedida', 'decimal(9,2)',
          requires = IS_EMPTY_OR(IS_DECIMAL_IN_RANGE(0,1e6,dot=',')),
          default=Decimal(0)),
    #La cantidad que el grupo declara haber repartido
    # puede ser distinta de la anterior, e.g. se cayeron unos huevos al repartir
    Field('cantidad_recibida', 'decimal(9,2)',
          requires = IS_EMPTY_OR(IS_DECIMAL_IN_RANGE(0,1e6,dot=',')),
          default=Decimal(0)),
    #La cantidad que el grupo declara haber recibido de la red
    # puede ser distinta de la anterior, e.g. se perdieron durante el transporte
    # Para la red/grupo primigenia, es la cantidad que el productor declara haber enviado
    Field('cantidad_red', 'decimal(9,2)',
          requires = IS_EMPTY_OR(IS_DECIMAL_IN_RANGE(0,1e6,dot=',')),
          default=Decimal(0)),
    # precio de este item_grupo: producto de cantidad por el precio unitario, que puede estar en
    # la tabla productoXpedido, o en la tabla mascara_producto
    # el resultado es la suma de lo que pagan las unidades o grupos o subredes por ese producto
    Field('precio_total', 'decimal(9,2)'),
    # precio del item_grupo tal y como se pidió, para referencia, no se modifica después de incidencias
    Field('precio_pedido', 'decimal(9,2)'),
    # precio de este item_grupo a pagar a la red, según versión del grupo:
    # producto de cantidad_recibida por el precio_red, que puede estar en
    # la tabla productoXpedido, o en la tabla mascara_producto
    Field('precio_recibido', 'decimal(9,2)'),
    # precio de este item_grupo a pagar a la red, según versión de la red:
    # producto de cantidad_red por el precio unitario que baja de la red,
    # que puede estar en la tabla productoXpedido, o en la tabla mascara_producto de la red que
    # sirve a este grupo
    Field('precio_red', 'decimal(9,2)'),
    # TODO DEPRECATED TO BE REMOVED: item_grupo.precio_red de la red primigenia será el precio_productor
    Field('precio_productor', 'decimal(9,2)')
    )
db.item_grupo._mariadb_index_command = 'ALTER TABLE item_grupo ADD UNIQUE (grupo, productoXpedido)'

# TODO DEPRECATED TO BE REMOVED: reemplazaremos item_red por más entradas en item_grupo,
# con item_grupo.grupo igual al ired de la red que ofrece el pedido por primera vez
#Se aplica el mismo comentario que está antes de item
db.define_table('item_red',
    Field('productoXpedido', db.productoXpedido,
          requires = IS_IN_DB(db, db.productoXpedido)),
    Field('grupo', db.grupo),
    Field('cantidad', 'decimal(9,2)',
          requires = IS_EMPTY_OR(IS_DECIMAL_IN_RANGE(0,1e6,dot=',')),
          default=Decimal(0)),
    Field('precio_total', 'decimal(9,2)'),
    Field('precio_red', 'decimal(9,2)'),
    Field('precio_productor', 'decimal(9,2)')
    )
db.item_red._mariadb_index_command = 'ALTER TABLE item_red ADD UNIQUE (grupo, productoXpedido)'

def funcion_para_calcular_precio_total(t):
    actualizar_precio_recibido = (t._tablename=='item_grupo')
    def fun(f, id):
        r = t[id]
        pppid = r.productoXpedido
        igrupo = r.grupo
        q = r.cantidad
        qrecibida = r.get('cantidad_recibida') or Decimal()
        qpedida = r.get('cantidad_pedida') or Decimal()
        qred = r.get('cantidad_red') or Decimal()
        ppp = db.productoXpedido(pppid)
        precio_productor = ppp.precio_productor or Decimal()
        # pedido PROPIO o RED
        if r.grupo==ppp.pedido.productor.grupo:
            precio_fin = ppp.precio_final
            precio_red = ppp.precio_productor or Decimal()
        else: # pedido COORDINADO o SUBRED
#            precio_productor = ppp.precio_productor or Decimal()
            mascara = db.mascara_producto(productoXpedido=pppid, grupo=igrupo)
            if mascara:
                precio_fin = mascara.precio
                precio_red = mascara.precio_red or Decimal()
            else:
                precio_fin = ppp.precio_final
                precio_red = ppp.precio_productor or Decimal()

        r.update_record(**{
            'precio_total': q*precio_fin,
            'precio_pedido': qpedida*precio_fin,
            'precio_red': qred*precio_red,
#            'precio_productor': qred*precio_productor
        })
        if actualizar_precio_recibido:
            r.update_record(precio_recibido=qrecibida*precio_red)
    return fun

def funcion_para_actualizar_precio_total(t):
    fun = funcion_para_calcular_precio_total(t)
    def fun_update(s, f):
        for row in s.select(t.id):
            fun(f,row.id)
    return fun_update

for t in (db.peticion, db.item, db.item_grupo):
    t._after_insert.append(funcion_para_calcular_precio_total(t))
    t._after_update.append(funcion_para_actualizar_precio_total(t))

##Por ahora no se usa...
#db.define_table('comentarioAproducto',
#    Field('producto', db.producto),
#    Field('persona', db.auth_user),
#    Field('fecha', 'datetime'),
#    Field('texto', 'text'),
#    )
#db.comentarioAproducto._mariadb_index_command = 'ALTER TABLE comentarioAproducto ADD UNIQUE (producto, persona)'

############# Actas y archivos del grupo ################

db.define_table('allfiles',
    Field('filename'),
    Field('filepath'),
    Field('parentpath'),
    Field('filetype'),
    Field('file','upload', autodelete=True, uploadseparate=True),
    Field('content','text'),
    Field('datecreated','datetime',default=lambda r:datetime.datetime.now(), writable=False),
    Field('datemodified','datetime',compute=lambda r:datetime.datetime.now()),
    Field('filesize','integer'),
    Field('destacado','boolean', default=False),
    Field('grupo',db.grupo)
    )

db.define_table('paginas_especiales',
                Field('slug','string', length=255, writable=False,
                      requires=IS_SLUG()),
                Field('language','string',default='es',writable=False),
                Field('title',
                      requires=IS_NOT_EMPTY()),
                Field('body','text',default=''),
                format = '%(slug)s')

db.define_table('textos_grupo',
    Field('grupo', db.grupo),
    Field('slug', 'string', length=255, writable=False, requires=IS_SLUG()),
    Field('texto', 'text', default='')
    )
db.textos_grupo._mariadb_index_command = 'ALTER TABLE textos_grupo ADD UNIQUE (grupo,slug)'

# DEPRECATED: Hasta nuevo aviso, no vamos a enviar estos emails, pero
# mantenemos la tabla, puede que volvamos a hacerlo más adelante...
db.define_table('aviso_email',
    Field('persona', db.auth_user),
    Field('grupo', db.grupo),
#    Field('pedidos', 'list:reference pedido'),
    Field('fecha_reparto', 'date'),
    Field('enviado', 'boolean', default=False),
    Field('random_string', 'string',
          compute=K.random_string,
          readable=False, writable=False),
    )
db.aviso_email._mariadb_index_command = 'ALTER TABLE aviso_email ADD UNIQUE (persona,grupo,fecha_reparto)'

#Todos los pedidos deben compartir la fecha de reparto y los productores deben ser del mismo grupo
db.define_table('plantilla_pedidos',
    #Guardo el grupo y la fecha para buscar mas rapido
    Field('grupo', db.grupo, requires = IS_IN_DB(db, db.grupo, '%(nombre)s')),
    Field('fecha_reparto', 'date'),
    Field('pedidos', 'list:reference pedido'),
    Field('plantilla', 'upload', default=None, autodelete=True, uploadseparate=True),
    Field('plantilla_filename', readable=False, writable=False),
    #Las guardo por referencia, pero no es estrictamente necesario
    Field('reglas', 'text'),
    Field('nombre_bloque', notnull=True, requires=IS_NOT_EMPTY())
    )
#Es cierto que pedidos es un indice UNIQUE, pero no creo que busque nunca por este campo
db.plantilla_pedidos._mariadb_index_command = 'ALTER TABLE plantilla_pedidos ADD INDEX (grupo, fecha_reparto)'
#Otra opcion, poco importante y por ahora solo admito un comando sql por tabla...
#db.plantilla_pedidos._mariadb_index_command = 'ALTER TABLE plantilla_pedidos ADD UNIQUE (pedidos(64))'
#Otra opcion, poco importante y por ahora solo admito un comando sql por tabla...
#db.plantilla_pedidos._mariadb_index_command = 'ALTER TABLE plantilla_pedidos ADD UNIQUE (grupo, fecha_reparto, nombre_bloque)'

db.define_table('reglas_plantilla',
    Field('grupo', db.grupo, requires = IS_IN_DB(db, db.grupo, '%(nombre)s')),
    Field('nombre_reglas', 'string', requires = IS_NOT_EMPTY()),
    Field('reglas', 'text'),
    )
db.reglas_plantilla._mariadb_index_command = 'ALTER TABLE reglas_plantilla ADD UNIQUE (grupo, nombre_reglas(64))'
