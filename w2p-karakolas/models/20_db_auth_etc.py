# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################

#Crea la conexión a la base de datos

# A veces es necesario usar fake_migrate=True: al definir `db` y también
#  al llamar a `auth.define_tables`, un poco más abajo:
# http://web2py.com/books/default/chapter/29/06/the-database-abstraction-layer?search=fake_migrate#table_migrations
# Esta opción se activa manualmente en 00_load_appconfig.py
db = DAL(myconf.take('db.uri'),
         migrate=migrate,
         lazy_tables=(not migrate),
         fake_migrate=FAKE_MIGRATE,
         fake_migrate_all=FAKE_MIGRATE)

## by default give a view/generic.extension to all actions from localhost
## none otherwise. a pattern can be 'controller/function.extension'
#All views can be accessed in json format
response.generic_patterns = ['*'] if request.is_local else ['*.json']

## (optional) optimize handling of static files
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'

#########################################################################

from gluon.tools import Auth, Crud, Service, PluginManager, prettydate
auth = Auth(db, hmac_key=Auth.get_or_create_key())
service, plugins = Service(), PluginManager()

if request.is_https:
    session.secure()

## create all tables needed by auth if not custom tables
auth.settings.extra_fields['auth_user']= [
    # Custom para usuarios de karakolas
    Field('direccion','string'),
    Field('telefono','string'),
    Field('informar_nuevos_grupos', 'boolean', default=False),
    
    # Required for py4web to coexist
    Field('sso_id', 'string', default=''),
    Field('action_token', 'string', default='')
]
    
auth.define_tables(username=True, migrate = migrate, fake_migrate=FAKE_MIGRATE)

## configure auth policy
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True
auth.settings.allow_basic_login = True
auth.settings.prevent_password_reset_attacks = False
auth.settings.client_side = True

## if you need to use OpenID, Facebook, MySpace, Twitter, Linkedin, etc.
## register with janrain.com, write your domain:api_key in private/janrain.key
#from gluon.contrib.login_methods.rpx_account import use_janrain
#use_janrain(auth,filename='private/janrain.key')

## configure email
mail=auth.settings.mailer
mail.settings.server = myconf.take('smtp.server')
mail.settings.sender = myconf.take('smtp.sender')
mail.settings.login =  myconf.take('smtp.login')
mail.settings.ssl =  myconf.take('smtp.ssl', cast=bool)
#########################################################################
