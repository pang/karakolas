# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################

# Este codigo se ocupa de redirigir la primera peticion a la pagina a static/front
#pero pasando la url que se pedia despues de #!, para que el front pueda cargar la url
#original via ajax. Las peticiones sucesivas por ajax no son redirigidas, claro.
import re
whitelist = re.compile('^/?(static|webadmin|appadmin|admin|.*/download|plugin_wiki/attachment|default/user_external)')
#override redirect, client_side=True por defecto
original_redirect = redirect
def redirect(location='', how=303, client_side=True, headers=None):
    original_redirect(location, how, client_side, headers)

# Dejamos de hacer la distinción entre el front de desarrollo y el de produccion,
# que añade complejidad y no mejora el rendimiento
#LINK_TO_FRONT = ('front/src/index.html' if DESARROLLO else 'front/dist/index.html')
LINK_TO_FRONT = 'front/src/index.html'

if ((not request.ajax) and
    (request.extension in ('html', 'load')) and
    (not (request.env.cmd_options and request.env.cmd_options.shell)) and
    (not whitelist.match('%s/%s'%(request.controller, request.function)))
   ):
    if request.extension=='load':
        request.extension='html'
    redirect(URL('static', LINK_TO_FRONT) + '#!' + request.url +
                 ('?' + '&'.join('%s=%s'%(k,v) for k,v in request.vars.items())
                  if request.vars else '')
             )
