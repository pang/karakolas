# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################

import os

from version import get_version, less

SNEW_VERSION,NEW_VERSION = get_version('VERSION')
try:
    _,old_version = get_version('private','DONE_UPGRADE')
except IOError:
    #Primera ejecucion de karakolas, creamos private/DONE_UPGRADE
    filepath = os.path.join('applications', request.application, 'private', 'DONE_UPGRADE')
    f = open(filepath, mode='w')
    f.write('0.0.1')
    f.close()
    f = open(filepath, mode='r')
    #Crea databases/sql.log (no se puede poner bajo control de versiones)
    databases_path = os.path.join('applications', request.application, 'databases')
    if not os.path.exists(databases_path):
        os.mkdir(databases_path)
    open(os.path.join(databases_path, 'sql.log'), 'a').close()
    old_version = (0,0,1)

# esta opción no es para principiantes
# usa con cuidado: sirve para regenerar la conexion entre web2py y la base de datos
# http://web2py.com/books/default/chapter/29/06/the-database-abstraction-layer?search=fake_migrate#Fixing-broken-migrations
# es True si existe el fichero de nombre FAKE_MIGRATE en el root de karakolas
#  (no de web2py)
FAKE_MIGRATE = os.path.exists(os.path.join('applications', request.application, 'FAKE_MIGRATE'))

migrate = (old_version != NEW_VERSION) or DESARROLLO or FAKE_MIGRATE

#########################################################################
#  use browser cache for static files
#########################################################################

if not DESARROLLO:
    response.static_version = SNEW_VERSION
    response.static_version_urls = True
