# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################

#No se puede cargar estos datos antes porque necesitan las
#otras tablas de la base de datos
CONT.TIPOS_CUENTA = CONT.TiposCuenta()
CONT.TIPOS_OPERACION = CONT.TiposOperacion()

### Monedas ###
db.define_table('moneda',
    Field('nombre','string', length=255, unique=True),
    Field('simbolo_html','string', length=16, unique=True),
    Field('simbolo_tex','string', length=16, unique=True),
    Field('activa', 'boolean', default=True),
    format='%(nombre)s'
)

db.define_table('grupoXmoneda',
    Field('grupo', db.grupo),
    Field('moneda', db.moneda),
    Field('activo', 'boolean', default=True)
    )
db.grupoXmoneda._mariadb_index_command = 'ALTER TABLE grupoXmoneda ADD UNIQUE (grupo,moneda)'

### Cuentas ###
def crea_descripcion_cuenta(r):
    tc = CONT.TIPOS_CUENTA[r.tipo]
    return tc.descripcion(r.owner, r.target)

db.define_table('cuenta',
    Field('tipo', 'integer', notnull=True),
    Field('owner', 'integer', notnull=True),
    Field('target', 'integer'),
    Field('activa', 'boolean', default=True),
#Lo dejo comentado, por ahora no le veo uso al siguiente campo
#    Field('record', 'integer', notnull=True),
    #El saldo se ajusta automáticamente al hacer un apunte:
    #Nunca se debe modificar sin registrar un apunte,
    #que a su vez debe pertenecer a una operacion de suma cero
    Field('saldo', 'decimal(9,2)', default=Decimal(), writable=False),
    Field('descripcion', 'string', compute = crea_descripcion_cuenta),
    format='%(descripcion)s'
    )
db.cuenta._mariadb_index_command = (
    'ALTER TABLE cuenta ADD UNIQUE (tipo, owner, target)',
#    'ALTER TABLE cuenta ADD INDEX (record, tipo)'
)

### Operaciones ###

def crea_descripcion_operacion(r):
    to = CONT.TIPOS_OPERACION[r.tipo]
    descripcion = (to.descripcion(r.parametros) +
                   (': '+r.comentario if r.comentario else ''))
    imoneda = r.get('moneda')
    if imoneda:
        descripcion += ' (en %s)'%db.moneda(imoneda).nombre
    return descripcion

#La fecha de la operacion es la misma fecha en que crea el registro, asi que
#asumimos que los ids son correlativos.
db.define_table('operacion',
    Field('tipo', 'integer', notnull=True),
    Field('parametros', 'list:integer', notnull=True, length=255),
    Field('valor', 'decimal(9,2)', default=Decimal()),
    Field('verificada', 'boolean', default=True),
    Field('cancelada', 'boolean', default=False),
    Field('descripcion', 'string', compute = crea_descripcion_operacion),
    #Este campo lo puede rellenar el usuario para aportar detalles, util por ej para GASTO_GRUPO
    Field('comentario','string', default='', length=255),
    #db.operacion.moneda puede ser nulo, se usa para ingresos
    #no se puede hacer un ingreso mixto, hay que usar un ingreso para cada moneda
    #y cada ingreso tiene suma cero, como el resto de operaciones
    Field('moneda', db.moneda),
    auth.signature,
    Field('fecha', 'date', compute=lambda row: row.created_on.date()),
    format='%(descripcion)s'
    )
db.operacion._mariadb_index_command = 'ALTER TABLE operacion ADD INDEX (tipo, parametros(255))'

### Apuntes ###
db.define_table('apunte',
    Field('operacion', db.operacion, notnull=True),
    Field('cuenta', db.cuenta, notnull=True),
    Field('cantidad', 'decimal(9,2)', default=Decimal()),
    #saldo despues de hacer el apunte
    Field('saldo', 'decimal(9,2)')
    )
db.apunte._mariadb_index_command = 'ALTER TABLE apunte ADD UNIQUE (operacion, cuenta)'

### Callbacks ###

def calcula_saldo_tras_apunte(f, id):
    cuenta   = db.cuenta[f.cuenta]
    cantidad = f.cantidad
    nuevo_saldo = cuenta.saldo + Decimal(cantidad)
    cuenta.update_record(saldo=nuevo_saldo)
    db.apunte(id).update_record(saldo=nuevo_saldo)

db.apunte._after_insert.append(calcula_saldo_tras_apunte)
CONT.TIPOS_CUENTA.register_callbacks()
CONT.TIPOS_OPERACION.register_callbacks()

### Cuentas Derivadas ###
CONT.TIPOS_CUENTA_DERIVADA = CONT.TiposCuentaDerivada()

def crea_descripcion_cuenta_der(r):
    tc = CONT.TIPOS_CUENTA_DERIVADA[r.tipo]
    return tc.descripcion(r.owner, r.get('param'))

db.define_table('cuenta_derivada',
    Field('tipo', 'integer', notnull=True),
    Field('owner', 'integer', notnull=True),
    Field('param', 'integer'),
    #El saldo se ajusta automáticamente al insertar en db.saldo_cuenta_derivada
    Field('saldo', 'decimal(9,2)', default=Decimal(), writable=False),
    Field('activa', 'boolean', default=True),
    Field('descripcion', 'string', compute = crea_descripcion_cuenta_der),
    format='%(descripcion)s'
    )
db.cuenta_derivada._mariadb_index_command = 'ALTER TABLE cuenta_derivada ADD UNIQUE (tipo, owner, param)'

CONT.TIPOS_CUENTA_DERIVADA.register_callbacks()

db.define_table('saldo_cuenta_derivada',
    Field('cuenta_derivada', db.cuenta_derivada, notnull=True),
    Field('operacion', db.operacion, notnull=True),
    Field('cantidad', 'decimal(9,2)', default=Decimal(), writable=False),
    Field('saldo', 'decimal(9,2)', default=Decimal(), writable=False)
    )
db.saldo_cuenta_derivada._mariadb_index_command = 'ALTER TABLE saldo_cuenta_derivada ADD UNIQUE (cuenta_derivada, operacion)'
