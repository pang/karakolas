# -*- coding: utf-8 -*-
# This file was developed by Massimo Di Pierro
# who released it under BSD, MIT and GPL2 licenses
# Then it was modified by Pablo Angulo to adapt it to karakolas
# This file is now part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

def is_wiki_editor(uid):
    grupos = []
    for row in db((db.auth_membership.group_id==db.auth_group.id) &
                  (db.auth_membership.user_id==uid)
                 ).select(db.auth_group.role):
        role = row.role
        if role=='webadmins':
            return True
        if role.startswith('miembros_') or role.startswith('admins_'):
            gid = int(role.split('_')[1])
            grupos.append(gid)
    return not db( db.grupo.id.belongs(grupos) &
                  (db.grupo.prueba==False)
                 ).isempty()

###################################################
# required parameters set by default if not set
###################################################
DEFAULT = {
    #Puede editar si es miembro o admin de un grupo o red que no es de prueba
    'editor' : is_wiki_editor(auth.user_id),
    'migrate': migrate,
    'theme'  : 'redmond', # the jquery-ui theme, mapped into plugin_wiki/ui/%(theme)s/jquery-ui-1.8.1.custom.css
    }

def _():
    """
    the mambo jambo here makes sure that
    PluginManager.wiki.xxx is also exposed as plugin_wiki_xxx
    this is to minimize Storage lookup
    """
    if not 'db' in globals() or not 'auth' in globals():
        raise HTTP(500,"plugin_wiki requires 'db' and 'auth'")
    prefix='plugin_wiki_'
    keys=dict(item for item in list(DEFAULT.items()) if not prefix+item[0] in globals())
    globals().update(dict((prefix+key,keys[key]) for key in keys))
_()

###################################################
# js and css modules required by the plugin
###################################################
response.files.append(URL('static', 'jquery-browser-plugin/dist/jquery.browser.min.js'))

###################################################
# required tables
###################################################
db.define_table('plugin_wiki_page',
                Field('slug',writable=False,
                      requires=(IS_SLUG(),IS_NOT_IN_DB(db,'plugin_wiki_page.slug'))),
                Field('title',default='',
                      requires=(IS_NOT_EMPTY(),IS_NOT_IN_DB(db,'plugin_wiki_page.title'))),
                Field('is_public','boolean',default=True),
                Field('body','text',default=''),
                Field('role',db.auth_group,
                      requires=IS_EMPTY_OR(IS_IN_DB(db,'auth_group.id','%(role)s'))),
                Field('changelog',default=''),
                auth.signature,
                format = '%(slug)s', migrate=plugin_wiki_migrate)

db.define_table('plugin_wiki_page_archive',
                Field('current_record',db.plugin_wiki_page),
                db.plugin_wiki_page,
                format = '%(slug) %(modified_on)s', migrate=plugin_wiki_migrate)

db.define_table('plugin_wiki_attachment',
                Field('tablename',writable=False,readable=False),
                Field('record_id','integer',writable=False,readable=False),
                Field('name',requires=IS_NOT_EMPTY()),
                Field('filename','upload',requires=IS_NOT_EMPTY(),autodelete=True, uploadseparate=True),
                auth.signature,
                format='%(name)s', migrate=plugin_wiki_migrate)

###################################################
# main class to intantiate the widget
###################################################
class PluginWiki(object):

    def __init__(self):
        self.extra = self.extra_blocks()

    # this embeds page attachments
    class attachments(object):
        def __init__(self,tablename,record_id=0,
                     caption='Attachments',close="Close",
                     id=None,width=70,height=70,
                     source=None):
            import uuid
            self.tablename=tablename
            self.record_id=record_id
            self.caption=caption
            self.close=close
            self.id=id or str(uuid.uuid4())
            self.width=width
            self.height=height
            if not source:
                source=URL('plugin_wiki','attachments',args=(tablename,record_id))
            self.source = source
        def xml(self):
            return '<div id="%(id)s" style="display:none"><div style="position:fixed;top:0%%;left:0%%;width:100%%;height:100%%;background-color:black;z-index:1001;-moz-opacity:0.8;opacity:.80;opacity:0.8;"></div><div style="position:fixed;top:%(top)s%%;left:%(left)s%%;width:%(width)s%%;height:%(height)s%%;padding:16px;border:2px solid black;background-color:white;opacity:1.0;z-index:1002;overflow:auto;-moz-border-radius: 10px; -webkit-border-radius: 10px;"><span style="font-weight:bold">%(title)s</span><span style="float:right">[<a href="#" onclick="jQuery(\'#%(id)s\').hide();return false;">%(close)s</a>]</span><hr/><div style="width:100%%;height:90%%;" id="c%(id)s"><iframe id="attachments_modal_content" style="width:100%%;height:100%%;border:0">%(loading)s</iframe></div></div></div><a href="#" onclick="jQuery(\'#attachments_modal_content\').attr(\'src\',\'%(source)s\');jQuery(\'#%(id)s\').fadeIn(); return false" id="plugin_wiki_open_attachments"">%(title)s</a>' % dict(title=self.caption,source=self.source,close=self.close,id=self.id,left=(100-self.width)/2,top=(100-self.height)/2,width=self.width,height=self.height,loading=T('loading...'))

    def extra_blocks(self):
        extra = {}
        LATEX = '<img src="https://chart.apis.google.com/chart?cht=tx&chl=%s" align="center"/>'
        extra['latex'] = lambda code: LATEX % code.replace('"','\"')
        extra['verbatim'] = lambda code: cgi.escape(code)
        extra['code'] = lambda code: CODE(code,language=None).xml()
        extra['code_python'] = lambda code: CODE(code,language='python').xml()
        extra['code_html_plain'] = lambda code: CODE(code,language='html_plain').xml()
        extra['code_html'] = lambda code: CODE(code,language='html').xml()
        extra['code_web2py'] = lambda code: CODE(code,language='web2py').xml()
        return extra

    def render(self,text,page_url=URL()):
        import re
        att_url = URL(request.application,'plugin_wiki','attachment')
        session.plugin_wiki_attachments=[]
        def register(match):
            session.plugin_wiki_attachments.append(match.group('h'))
            return '[[%s %s/%s' % (match.group('t'),att_url,match.group('h'))
        text = re.sub('\[\[(?P<t>[^\[\]]+)\s+attachment:\s*(?P<h>[\w\-\.]+)',
                      register,text)
        text = re.sub('\[\[(?P<t>[^\[\]]+) page:','[[\g<t> %s/' % page_url,text)
        return MARKMIN(text,extra=self.extra)

plugin_wiki=PluginWiki()
