#########################################################################

# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################

#python
import datetime
date = datetime.date
import re
from collections import defaultdict
from decimal import Decimal
import string
import random
from itertools import chain

#gluon
if migrate:
    from gluon.custom_import import track_changes; track_changes(True)
from gluon import current
from gluon.storage import Storage
try:#web2py >=2.9.12
   from gluon.dal.objects import Table
except:#web2py <=2.9.11
   from gluon.dal import Table

#karakolas
#from require_request import requires_get_arguments
import kutils as K
import modelos.permisos as Permisos
from scopes import Scope
import contabilidad as CONT
from cuentas_derivadas import TiposCuentaDerivada
from add_libs import add_libs
from vistas.vistas_comunes import *

#########################################################################
#  store instances in current to be accessible from other modules
#########################################################################

current.db   = db
current.auth = auth
current.mail = mail
current.CONT = CONT
CONT.TiposCuentaDerivada = TiposCuentaDerivada

########### ESTADOS_PEDIDO ###########
from modelos.pedidos import EstadosPedido, str_estados_pedido
dEstadosPedidoSingular = str_estados_pedido(plural=False)
dEstadosPedidoPlural = str_estados_pedido(plural=True)

########### Scope ###########
scope = Scope()
def scope_grupo_o_red(scope_grupo='admin-grupo'):
    get_vars = request.get_vars
    g = db.grupo(get_vars.get('grupo'))
    if g:
        return 'admin-red' if g.red else scope_grupo
    prod = db.productor(get_vars.get('productor'))
    if prod:
        return 'admin-red' if prod.grupo.red else scope_grupo
    ped = db.pedido(get_vars.get('pedido'))
    if ped and ped.productor.grupo.red:
        return 'admin-red'
    plant = db.plantilla_pedidos(get_vars.get('plantilla'))
    if plant and plant.grupo.red:
        return 'admin-red'
    return scope_grupo

def scope_miembro_o_red():
    return scope_grupo_o_red(scope_grupo='miembro')

scope.set_scopes({'no-logueado': T('Visitante no logueado'),
                  'logueado': T('Cualquier usuario logueado'),
                  'miembro':T('Miembro de algún grupo'),
                  'admin-grupo':T('Admin de algún grupo'),
                  'admin-red':T('Admin de alguna red'),
                  'admin-web':T('Admin de la toda la web')
})

############## Contabilidad ################
CONT.OPCIONES_INGRESOS = CONT.OpcionIngresos()
CONT.OPCIONES_PEDIDOS = CONT.OpcionPedidos()
