#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os
from os import path
import sys
import subprocess
import shutil

# Este script python primero llama a mysqldump -u dbuser -p dbpass dbame > file.sql
# La segunda parte ...

UPLOADS_PATH = '../../uploads'
MENSAJE_ERROR_SQL_GZ = '''No hemos podido hacer un backup sql.gz de la base de datos.
Usaremos un backup csv, que funciona bastante bien, pero no del todo :-/'''
MENSAJE_ERROR_APPCONFIG = 'No hemos podido recuperar la configuración de la base de datos en el archivo de configuracion private/appconfig.ini'

###########################
if len(sys.argv) != 3:
    print('usage: export_database.py URI name')
    sys.exit(1)

def split_uri(uri):
    dbengine, rest = uri.split('://')
    dbuser, rest = rest.split(':')
    dbpass, rest = rest.split('@')
    dbhost, dbname = rest.split('/')
    return dbengine, dbhost, dbname, dbuser, dbpass

def exportdata(folder, filename, uri):
    bayespath = path.join('..', 'bayes.pickle')
    if path.exists(folder):
        shutil.rmtree(folder)
    os.mkdir(folder)
    newbayespath = path.join(folder, 'bayes.pickle')
    shutil.copyfile(bayespath, newbayespath)
    # Guardamos un archivo zip con todos los archivos guardados en la db en
    # campos de tipo file
    shutil.make_archive(
        path.join(folder, filename + '_attachments'),
        'zip',
        UPLOADS_PATH)
    try:
        dbengine, dbhost, dbname, dbuser, dbpass = split_uri(uri)
    except RuntimeError:
        print(MENSAJE_ERROR_SQL_GZ)
        return 1
    #Intentamos guardar un dump, si el sistema tiene el comando mysqldump
    prox = subprocess.run(['which', 'mysqldump'])
    has_mysqldump = (prox.returncode==0)
    if (dbengine not in ['mysql', 'mariadb']) or (not has_mysqldump):
        print(MENSAJE_ERROR_SQL_GZ)
        return 1
#$MYSQLDUMP -u $MYSQL_PRO_USER -h $MYSQL_HOST -p$MYSQL_PRO_PASSWD $MYSQL_PRO_DATABASE | $GZIP -9
    dumpcommand = 'mysqldump -u {dbuser} -h {dbhost} -p{dbpass} {dbname}'.format(
        dbuser=dbuser, dbhost=dbhost, dbpass=dbpass, dbname=dbname
    )
    prox = subprocess.run(['which', 'gzip'])
    has_gzip = (prox.returncode==0)
    if has_gzip:
        filetype = '.sql.gz'
        filepath = path.join(folder, filename + filetype)
        command = dumpcommand + ' | gzip -9 > ' + filepath
    else:
        filetype = '.sql'
        filepath = path.join(folder, filename + filetype)
        command = dumpcommand + ' > ' + filepath

    exit_code, output = subprocess.getstatusoutput(command)
    if exit_code:
        print(MENSAJE_ERROR_SQL_GZ)
        print('#'*30)
        print(output)
        print('#'*30)
        return 1
    else:
        print('datos guardados en: ', filepath)
        return 0

if __name__=='__main__':
    uri, name = sys.argv[1:3]
    #mismo nombre para la carpeta y los archivos sql, zip, etc
    exit_code = exportdata(name, name, uri)
    if exit_code: sys.exit(exit_code)
