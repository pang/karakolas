#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os
from os import path
import glob
import sys
import subprocess
import shutil
import zipfile

# Este script python primero llama a mysql -u dbuser -p dbpass dbame < file.sql
#  Lo hace desde fuera de web2py porque si no, el lock de la db de web2py hace que no
#  funcione la importación del fichero sql
# La segunda parte la hacemos desde web2py, llamando a sample_data.post_import_sql

###########################

def split_uri(uri):
    dbengine, rest = uri.split('://')
    dbuser, rest = rest.split(':')
    dbpass, rest = rest.split('@')
    dbhost, dbname = rest.split('/')
    return dbengine, dbhost, dbname, dbuser, dbpass

def import_sql(uri, sqlfile):
    dbengine, dbhost, dbname, dbuser, dbpass = split_uri(uri)

    if not path.exists(sqlfile):
        print('Error, no se encuentra el archivo: ' + sqlfile)
        return 1

    command = 'mysql -u {dbuser} -h {dbhost} -p{dbpass} {dbname} < "{filepath}"'.format(
        dbuser=dbuser, dbhost=dbhost, dbpass=dbpass, dbname=dbname, filepath=sqlfile
    )
    exit_code, output = subprocess.getstatusoutput(command)
    if exit_code:
        print('ERROR')
        print(output)
        return 1
    return 0

def extract_attachments(attachmentspath):
    UPLOADS_PATH = '../../uploads'
    if path.exists(UPLOADS_PATH):
        shutil.rmtree(UPLOADS_PATH)

    # todos los archivos adjuntos, que aparecen como campos de tipo "file", están
    # en la carpeta uploads
    with zipfile.ZipFile(attachmentspath) as zip_ref:
        zip_ref.extractall(UPLOADS_PATH)

def regenerate_databases():
    for filepath in glob.glob('../../databases/*.table'):
        os.remove(filepath)
    f = open('../../FAKE_MIGRATE', 'w')
    f.close()
    cwd = os.getcwd()
    command = 'python3 "{cwd}/../../../../web2py.py" -S karakolas/appadmin -M -R "{cwd}/../../cron/daily_tasks.py"'.format(
        cwd=cwd
    )
    exit_code, output = subprocess.getstatusoutput(command)
    if exit_code:
        print('ERROR')
        print(output)
        return exit_code
    os.remove('../../FAKE_MIGRATE')
    return 0

def post_import_sql(bayespath):
    '''Llamada a sample_data.post_import_sql_data(), pero desde dentro de web2py '''
    #Ponemos el bayes.pickle de este conjunto test en la carpeta private/testdata
    # allí lo buscará sample_data.py para importarlo
    # (no se puede pasar el bayespath al script sample_data.py)
    if path.exists('bayes.pickle'):
        os.remove('bayes.pickle')
    if path.exists(bayespath):
        shutil.copyfile(bayespath, 'bayes.pickle')
    cwd = os.getcwd()
    command = 'python3 "{cwd}/../../../../web2py.py" -S karakolas -M -R "{cwd}/../../modules/sample_data.py"'.format(
        cwd=cwd
    )
    exit_code, output = subprocess.getstatusoutput(command)
    if exit_code:
        print('ERROR')
        print(output)
        return exit_code
    return 0

if __name__=='__main__':
    if len(sys.argv) != 3:
        print('usage: import_database.py URI datafolder')
        sys.exit(1)
    uri, datafolder = sys.argv[1:3]
    sqlpath = path.join(datafolder, datafolder + '.sql')
    sqlgzpath = path.join(datafolder, datafolder + '.sql.gz')
    attachmentspath = path.join(datafolder, datafolder + '_attachments.zip')
    extract_attachments(attachmentspath)
    bayespath = path.join(datafolder, 'bayes.pickle')
    if not os.path.exists(sqlpath) and not os.path.exists(sqlgzpath):
        print('No es posible importar este backup, no encontramos los archivos: ' +
              sqlpath + ' ni ' + sqlgzpath)
        sys.exit(1)
    elif not os.path.exists(attachmentspath):
        print('No es posible importar este backup, no encontramos el archivo: ' +
              attachmentspath)
        sys.exit(1)
    elif os.path.exists(sqlgzpath):
        # si hay un archivo sql y otro sql.gz, borra el sql y descomprime el sql.gz,
        # porque es éste el que se actualiza al exportar y el sql podría ser un resto
        # de un testdata obsoleto
        if os.path.exists(sqlpath):
            os.remove(sqlpath)
        prox = subprocess.run(['which', 'gunzip'])
        has_gunzip = (prox.returncode==0)
        if not has_gunzip:
            print('No es posible importar este backup, porque "gunzip" no está instalado')
            sys.exit(1)
        exit_code, output = subprocess.getstatusoutput('gunzip -k ' + sqlgzpath)
        if exit_code:
            print('Error al descomprimir el archivo ' + sqlgzpath)
            sys.exit(1)
    exit_code = import_sql(uri, sqlpath)
    if exit_code: sys.exit(exit_code)
    exit_code = regenerate_databases()
    if exit_code: sys.exit(exit_code)
    exit_code = post_import_sql(bayespath)
    if exit_code: sys.exit(exit_code)
