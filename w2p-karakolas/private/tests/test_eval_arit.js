// -*- coding: utf-8 -*-
// (C) Copyright (C) 2012-14 Pablo Angulo
// This file is part of karakolas <karakolas.org>.

// karakolas is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// karakolas is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU Affero General Public
// License along with karakolas.  If not, see
// <http://www.gnu.org/licenses/>.

/////////////////////////////////////////
/////// rhino test_eval_arit.js   ///////
/////////////////////////////////////////


load('../static/js/esprima.js')
load('../static/js/eval_arit.js')

vals ={
    'condition': true,
    'a':1,
    'b':2,
    'c':2.51
};
tests = ['2+2',
         '2+(-2)',
         '1+2*3',
         '(1+2)*3',
         '1+a',
         'condition?a:b',
         '!condition?a:b',
         '(a>b)?a/3:b/3',
         '1+1>2?a:b',
         '1+1>=2?a:b',
         'c>=5/2?a+c:b',
         '!condition && c>=5/2?a+c:b',
         '!condition || c>=5/2?a+c:b'];
errores = [];
var vkeys = Object.keys(vals);
for(var k=0;k<vkeys.length;k++){
    var key = vkeys[k];
//    print(key + '=' + vals[key]+';');
    eval(key + '=' + vals[key]+';')
}
//print('#################');
for(var i=0;i<tests.length;i++){
    var expr = tests[i];
    if(eval_arit(expr, vals) !== eval(expr)){
        print('Error al evaluar la expresion ' + expr);
        print(eval_arit(expr, vals));
        print(eval(expr));
        print('------------');
        errores.push(expr);
    }

}
if(errores.length==0){
    print('All tests passed');
}
