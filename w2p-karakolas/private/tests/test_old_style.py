#TODO: refactor!! pero espera a que esta funcionalidad termine en un modulo

import unittest
import sys

sys.path.append('../../..')

from gluon.globals import Request

# Create a test database that's laid out just like the "real" database
import copy
test_db = DAL('sqlite://testing.sqlite')  # Name and location of the test DB file

for tablename in db.tables:  # Copy tables!
    table_copy = [copy.copy(f) for f in db[tablename]]
    test_db.define_table(tablename, *table_copy)

db = test_db  # Rename the test database so that functions will use it instead of the real database
for tablename in db.tables:  # Copy tables!
    db[tablename].truncate()

db.commit()
#import commands
#print commands.getoutput('pwd')
db.import_from_csv_file(open("applications/karakolas/private/db_test.csv"))

#execfile("applications/api/controllers/10.py", globals())

db.commit()

class TestImportarTablas(unittest.TestCase):
    def setUp(self):
        pass

    def test_asignar_columnas(self):
        s='''#pone nombres a las columnas de la tabla
nombre, variedad, origen, _, precio_base, unidad, descripcion, IVA = cols()
    '''
        data = ['naranja', 'navelina', 'Valencia', '','1.24', 'kg', 'ultimas semanas','3.4']
        no_cat = db.categorias_productos(codigo=-1).id
        defaults = {'categoria':no_cat,
                    'productor':1}
        fields = dict((f.name,f.type) for f in db.producto)
        rp = RuleParser(s, fields, defaults)
        self.assertEqual(rp.extra_cols,{})
        self.assertEqual( rp.process(0,3,data) ,
            ({'categoria': no_cat, 'nombre': 'naranja', 'descripcion': 'ultimas semanas',
              'precio_base':Decimal('1.24'), 'productor': 1}, {})
            )

    def test_asignaciones(self):
        s='''#pone nombres a las columnas de la tabla
producto, variedad, origen, _, precio_base, unidad, descripcion, IVA = cols()
precio_final = 2.13
nombre = producto + ' ' + variedad
    '''
        data = ['naranja', 'navelina', 'Valencia', '','1.24', 'kg', 'ultimas semanas','3.4']
        no_cat = db.categorias_productos(codigo=-1).id
        defaults = {'categoria':no_cat,
                    'productor':1}
        fields = dict((f.name,f.type) for f in db.producto)
        rp = RuleParser(s, fields, defaults)
        self.assertEqual(rp.extra_cols,{})
        self.assertEqual( rp.process(0,3,data) ,
            ({'categoria': no_cat, 'nombre': 'naranja navelina', 'descripcion': 'ultimas semanas',
              'precio_base':Decimal('1.24'), 'precio_final':Decimal('2.13'), 'productor': 1}, {})
            )

    def test_operaciones(self):
        s='''#pone nombres a las columnas de la tabla
producto, variedad, origen, _, precio_base, unidad, descripcion, IVA = cols()
precio_final = float(precio_base)+1
nombre = producto + ' ' + variedad
    '''
        data = ['naranja', 'navelina', 'Valencia', '','1.24', 'kg', 'ultimas semanas','3.4']
        no_cat = db.categorias_productos(codigo=-1).id
        defaults = {'categoria':no_cat,
                    'productor':1}
        fields = dict((f.name,f.type) for f in db.producto)
        rp = RuleParser(s, fields, defaults)
        self.assertEqual(rp.extra_cols,{})
        self.assertEqual( rp.process(0,3,data) ,
            ({'categoria': no_cat, 'nombre': 'naranja navelina', 'descripcion': 'ultimas semanas',
              'precio_base': Decimal('1.24'), 'precio_final': Decimal('2.24'), 'productor': 1},
             {})
            )

    def test_columnas_extra(self):
        s='''#pone nombres a las columnas de la tabla
producto, variedad, origen, _, precio_base, unidad, descripcion, IVA = cols()
algo = 'texto'
nombre = producto + ' ' + variedad
define_extra_col('IVA','decimal')
define_extra_col('algo','string')
    '''
        data = ['naranja', 'navelina', 'Valencia', '','1.24', 'kg', 'ultimas semanas','3.4']
        no_cat = db.categorias_productos(codigo=-1).id
        defaults = {'categoria':no_cat,
                    'productor':1}
        fields = dict((f.name,f.type) for f in db.producto)
        rp = RuleParser(s, fields, defaults)
        self.assertEqual(rp.extra_cols,{'IVA':'decimal', 'algo':'string'})
        self.assertEqual( rp.process(0,3,data) ,
            ({'categoria': no_cat, 'nombre': 'naranja navelina', 'descripcion': 'ultimas semanas',
              'productor': 1,'precio_base':Decimal('1.24'), },
             {'IVA':Decimal('3.40'), 'algo':'texto'})
            )

    def test_subcadena(self):
        s='''#pone nombres a las columnas de la tabla
nombre, variedad, origen, _, precio_base, unidad, descripcion, IVA = cols()
define_extra_col('algo','string')
algo = variedad[0:3]
    '''
        data = ['naranja', 'navelina', 'Valencia', '','1.24', 'kg', 'ultimas semanas','3.4']
        no_cat = db.categorias_productos(codigo=-1).id
        defaults = {'categoria':no_cat,
                    'productor':1}
        fields = dict((f.name,f.type) for f in db.producto)
        rp = RuleParser(s, fields, defaults)
        self.assertEqual(rp.extra_cols,{'algo':'string'})
        self.assertEqual( rp.process(0,3,data) ,
            ({'categoria': no_cat, 'nombre': 'naranja', 'descripcion': 'ultimas semanas',
              'precio_base':Decimal('1.24'), 'productor': 1}, {'algo':'nav'})
            )

    def test_iva(self):
        s='''#pone nombres a las columnas de la tabla
nombre, precio_base, precio_final = cols()
define_extra_col('iva','decimal')
iva = get_ratio(precio_base, precio_final)
    '''
        data = ['naranja', '1.2', '1.32']
        no_cat = db.categorias_productos(codigo=-1).id
        defaults = {'categoria':no_cat,
                    'productor':1}
        fields = dict((f.name,f.type) for f in db.producto)
        rp = RuleParser(s, fields, defaults)
        self.assertEqual(rp.extra_cols,{'iva':'decimal'})
        self.assertEqual( rp.process(0,3,data) ,
            ({'categoria': no_cat, 'nombre': 'naranja',
              'precio_base':Decimal('1.20'), 'precio_final':Decimal('1.32'),
              'productor': 1}, {'iva':Decimal('10.00')})
            )

    def test_if_hoja(self):
        s='''
if hoja==1:
    nombre='a'
else:
    nombre='b'
    '''
        data = list(range(5))
        fields = dict((f.name,f.type) for f in db.producto)
        defaults = {'productor':1}
        rp = RuleParser(s, fields, defaults)
        self.assertEqual( rp.process(0,3,data, {'hoja':1}),
            ({'nombre': 'a', 'productor': 1,'hoja':1}, {})
            )
        self.assertEqual( rp.process(0,3,data, {'hoja':2}),
            ({'nombre': 'b', 'productor': 1,'hoja':2}, {})
            )

    def test_context(self):
        s='''set_context_col('C')
categoria=context()
nombre, descripcion, precio_base=cols()
    '''
        data = ['naranja', 'navelina', '1.24']
        fields = dict((f.name,f.type) for f in db.producto)
        defaults = {'productor':1}
        rp = RuleParser(s, fields, defaults)
        self.assertEqual( rp.process(0,3,['','','fruta']), ({}, {}) )
        self.assertEqual( rp.process(0,3,data, {'hoja':2}),
            ({'nombre': 'naranja', 'descripcion':'navelina',
              'productor': 1,'hoja':2,
              'categoria':'fruta', 'precio_base':Decimal('1.24')}, {})
            )

class TestPrettyPrintDecimal(unittest.TestCase):
    def setUp(self):
        pass
#        request = Request()  # Use a clean Request object

    def testPrettyPrintDecimal(self):
        cs = {Decimal('7.25'):'7.25',
              Decimal('7.5'):'7.5',
              Decimal('7'):'7',
              Decimal('-7.123'):'-7.12',
              Decimal('70'):'70',
              Decimal('-70.10'):'-70.1',
              }
        for k,v in cs.items():
            self.assertEqual(pprint_c(k),v)

class TestStupid(unittest.TestCase):
    def setUp(self):
        pass

    def testStupid_interaction_with_db(self):
        k = db(db.grupo).count()
        import datetime
        db.grupo.insert(nombre='XXX Grupo de Prueba %s'%datetime.datetime.now())
        self.assertEqual(k+1, db(db.grupo).count())
        db(db.grupo.nombre.startswith('XXX')).delete()
        self.assertEqual(k, db(db.grupo).count())

#class TestListActiveGames(unittest.TestCase):
#    def setUp(self):
#        request = Request()  # Use a clean Request object

#    def testListActiveGames(self):
#        # Set variables for the test function
#        request.post_vars["game_id"] = 1
#        request.post_vars["username"] = "spiffytech"

#        resp = list_active_games()
#        db.commit()
#        self.assertEquals(0, len(resp["games"]))

suite = unittest.TestSuite()
suite.addTest(unittest.makeSuite(TestPrettyPrintDecimal))
suite.addTest(unittest.makeSuite(TestImportarTablas))
suite.addTest(unittest.makeSuite(TestStupid))
unittest.TextTestRunner(verbosity=2).run(suite)


