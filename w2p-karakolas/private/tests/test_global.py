# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-14 Pablo Angulo
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################

#####################################################
#En este fichero interaccionamos con una instalacion de karakolas
#usando python requests
#
# Este fichero se llama desde test.py
#
#En esta bateria de tests inspeccionamos todos los controladores
#que usen el decorador requires_get_arguments y comprobamos que devuelven
#una respuesta ok (http 200, y ningun mensaje de error)
#tras elegir un conjunto de argumentos compatible con el decorador
#####################################################
###################################

import imp
from bs4 import BeautifulSoup
import unittest
import random
import re
import sys
import os
import ast
from test_base import TestBase
import requests
from collections import defaultdict

def set_data(d):
    global URL_KARAKOLAS, PDATA
    PDATA         = d
    URL_KARAKOLAS = d.URL_KARAKOLAS

class TestGET(TestBase):

    def __init__(self, *args, **kwds):
        TestBase.__init__(self, *args, **kwds)
        self._parse_tables()

    def setUp(self):
        TestBase.setUp(self)

    operators = {
        ast.BitAnd: '&',
        ast.BitOr:  '|',
        ast.Add: '+',
        ast.Sub: '-',
        ast.Mult: '*',
        ast.Div: '/',
    }

    relations = {
        ast.Eq     : '==',
        ast.NotEq  : '!=',
        ast.Lt     : '<',
        ast.LtE    : '<=',
        ast.Gt     : '>',
        ast.GtE    : '>=',
    }

    def _eval_DAL_query(self, node):
        ''
        if type(node)==ast.BinOp:
            return '(%s) %s (%s)'%(self._eval_DAL_query(node.left),
                                   self.operators[type(node.op)],
                                   self._eval_DAL_query(node.right))
        elif type(node) in self.relations:
            return self.relations[type(node)]
        elif type(node)==ast.Compare:
            return '%s %s'%(self._eval_DAL_query(node.left),
                            ' '.join('%s %s'%(self._eval_DAL_query(op),
                                              self._eval_DAL_query(comp))
                                for op,comp in zip(node.ops, node.comparators)))
        elif type(node)==ast.Attribute:
            return '%s.%s'%(self._eval_DAL_query(node.value),
                            node.attr)
        elif type(node)==ast.Name:
            return node.id
        else:
            raise NotImplementedError
        return

    def _some_value(self, node, sample_records):
        if type(node)==ast.Str:
            ftype=node.s
            if ftype == 'text' or ftype == 'string' or ftype == 'slug': return self._random_name()
            if self._is_numeric_type(ftype): return random.randint(1,99)
            if ftype == 'boolean': return True
            if ftype == 'date':
                from datetime import date
                return date.fromordinal(random.randint(1,700000)).isoformat()
        if type(node)!=ast.Attribute:
            raise NotImplementedError
        ftype = node.attr
        try:
            if ftype.startswith('list:reference'):
                ftype = ftype[15:]
            sr = sample_records.get(ftype,1)
            return sr[0] if isinstance(sr, (list, tuple)) else sr
        except:
            return None

    def test_010_nuevo_productor(self):
        self._login_to_karakolas(PDATA.admin_grupo[1])
        self._access_admin()
        gid = PDATA.grupo[0]

        nombre_productor = self._random_name()
        data = self._get_form_and_post_it_back('karakolas/productores/vista_productores.html?grupo=%d'%gid,
            {'nombre': nombre_productor,
             'email' : '%s@example.com'%nombre_productor
            })

        pid = self._find_record_by_data('productor',
            data=dict(nombre=nombre_productor, grupo=gid))
        self.assertTrue(pid>0, 'No se pudo crear un productor desde productores/vista_productores')

        #Busca el pedido fantasma
        fid = self._find_record_by_data('pedido',
            data=dict(fecha_reparto='1970-01-01', productor=pid))
        self.assertTrue(fid>0, 'No se creo el pedido fantasma para este productor')

        PDATA.productor = pid, nombre_productor

    def test_015_incluye_persona_en_el_grupo(self):
        self._login_to_karakolas(PDATA.admin_grupo[1])
        gid = PDATA.grupo[0]
        uid, user_data = PDATA.usuario_grupo
        username       = user_data['username']

        data = self._get_form_and_post_it_back('karakolas/grupo/miembros_grupo.load?grupo=%d'%gid,
            {'miembro_username': username,
             'miembro_unidad': 1,
             'incluir_miembro': 'on'
            })

        #Comprueba que la persona esta en el grupo
        r = self.s.get(URL_KARAKOLAS + 'karakolas/grupo/lista_miembros_en_grupo.load?grupo=%d'%gid, verify=False)
        self.assertIn(username, r.content, 'No se pudo incluir el usuario al grupo')

    def test_020_miembros_ven_el_productor(self):
        'comprueba que los usuarios normales pueden ver un productor que ha introducido admin'
        print('TODO')

    def test_030_el_productor_aparece_en_gestion_pedidos(self):
        pid, nombre_productor = PDATA.productor
        gid, nombre_grupo     = PDATA.grupo

        self._login_to_karakolas(PDATA.admin_grupo[1])

        r = self.s.get(URL_KARAKOLAS + 'karakolas/gestion_pedidos/gestion_pedidos.html?grupo=%d'%gid, verify=False)
        soup = BeautifulSoup(r.content)
        self.assertTrue(soup.find('select',{'id':'productor'})
                            .find('option', {'value':pid})
                            .contents[0]==nombre_productor,
            'el productor no aparece en el selector de productos de nuevo pedido' )

        url_nuevos_pedidos = 'karakolas/gestion_pedidos/nuevos_pedidos.load?grupo=%d'%gid
        self.assertTrue(soup.find('div',{'id':'nuevos_pedidos'})
                            .find('div',{'data-w2p_remote':'/'+url_nuevos_pedidos}),
            'no hay un div donde cargar el formulario para abrir varios pedidos' )

        r = self.s.get(URL_KARAKOLAS + url_nuevos_pedidos, verify=False)
        soup = BeautifulSoup(r.content)
        self.assertTrue(any(tr.find('td', text=nombre_productor) and
                            tr.find('input', {'name':'productor_%d'%pid})
                for tr in soup.findAll('tr')) ,
            'el productor no aparece en los checkboxes de productor para abrir varios pedidos a la vez' )

    def test_040_nuevo_pedido(self):
        pid, nombre_productor = PDATA.productor
        gid, nombre_grupo     = PDATA.grupo

        self._login_to_karakolas(PDATA.admin_grupo[1])
        self._access_admin()

        data = self._get_form_and_post_it_back('karakolas/gestion_pedidos/nuevo_pedido.html?grupo=%d&productor=%d'%(gid,pid),
            {'fecha_reparto': self._sample_value('date', 'date', []),
             'abierto'      : 'on',
             'usa_plantilla': ''
            })
        pedido_id = self._find_record_by_data('pedido', data=data, fields=self.tables_dict['pedido'])
        self.assertTrue(pedido_id>0, 'No se pudo abrir un pedido desde gestion_pedidos/nuevo_pedido.html')

        print('TODO: comprueba que el pedido está visible desde gestion_pedidos.html')
        PDATA.pedido = pedido_id

    def test_050_miembros_ven_el_pedido(self):
        print('TODO')

    def test_060_une_grupo_a_red(self):
        self._login_to_karakolas(PDATA.admin_red[1])
        rid, nombre_red = PDATA.red
        gid, _          = PDATA.grupo
        data = self._get_form_and_post_it_back('karakolas/redes/grupos_en_red.load?red=%d'%rid,
            {'grupo': gid
            })

        #Comprobamos que desde el grupo se ve la red
        self._logout()
        self._login_to_karakolas(PDATA.admin_grupo[1])
        r = self.s.get(URL_KARAKOLAS + 'karakolas/productores/productores_coordinados.html?grupo=%d'%gid, verify=False)
        self.assertIn(nombre_red, r.content,
            'La red no es visible desde productores/productores_coordinados.html' )

    def test_065_nuevo_productor_coordinado(self):
        self._login_to_karakolas(PDATA.admin_red[1])
        self._access_admin()
        rid, _ = PDATA.red
        gid, _ = PDATA.grupo

        nombre_productor = self._random_name()
        data = self._get_form_and_post_it_back('karakolas/productores/vista_productores.html?grupo=%d'%rid,
            {'nombre': nombre_productor,
             'email' : '%s@example.com'%nombre_productor
            })

        pid = self._find_record_by_data('productor',
            data=dict(nombre=nombre_productor, grupo=rid))
        self.assertTrue(pid>0, 'No se pudo crear un productor desde productores/vista_productores')

        #El nuevo productor es visible por los grupos de la red
        self._logout()
        self._login_to_karakolas(PDATA.admin_grupo[1])

        r = self.s.get(URL_KARAKOLAS + 'karakolas/productores/productores_coordinados.load?grupo=%d'%gid, verify=False)
        soup = BeautifulSoup(r.content)
        self.assertTrue(soup.find('table',{'id':'tabla_productores_coordinados'})
                            .find('td', text=nombre_productor),
            'el productor no aparece en el selector de productos de nuevo pedido' )

        PDATA.productor_coordinado = pid, nombre_productor

    def test_070_nuevo_pedido_coordinado(self):
        pid, nombre_productor = PDATA.productor_coordinado
        gid, nombre_grupo     = PDATA.grupo
        rid, nombre_red       = PDATA.red

        self._login_to_karakolas(PDATA.admin_red[1])
        self._access_admin()

        data = self._get_form_and_post_it_back('karakolas/gestion_pedidos/nuevo_pedido.html?grupo=%d&productor=%d'%(rid,pid),
            {'fecha_reparto': self._sample_value('date', 'date', []),
             'abierto'      : 'on',
             'usa_plantilla': ''
            })

        pedido_id = self._find_record_by_data('pedido', data=data, fields=self.tables_dict['pedido'])
        self.assertTrue(pedido_id>0, 'No se pudo abrir un pedido desde gestion_pedidos/nuevo_pedido.html para un productor coordinado')

        print('TODO: comprueba que el pedido está visible desde gestion_pedidos.html')
        PDATA.pedido_coordinado = pedido_id

    skip_list = ('actualizar_productos', 'crud_auth_group')

    def test_200_global(self):
        '''Navega por toda la web completando los parametros get que sean necesarios, y haciendo login
        con el usuario apropiado
        '''
        self._access_admin()

        path_to_controllers = '../controllers'
        controllers = [f for f in os.listdir(path_to_controllers)
                         if os.path.splitext(f)[-1]=='.py']
        def GET_decorated_function(n):
            if type(n)!=ast.FunctionDef:
                return False
            for n2 in n.decorator_list:
                if (type(n2)     ==ast.Call and
                    type(n2.func)==ast.Name and
                    n2.func.id=='requires_get_arguments'):
                    return n2

        for c in controllers:
            controller = os.path.splitext(c)[0]
            code = open(os.path.join(path_to_controllers, c)).read()
            tree = ast.parse(code)
            decorated_funcs = [n for n in ast.walk(tree)
                                 if GET_decorated_function(n)]
            for n in decorated_funcs:
                n_call   = GET_decorated_function(n)
                funcname = n.name
                if funcname in self.skip_list:
                    continue

                decorator_args = [t.elts for t in n_call.args]
                get_args       = dict((n1.s, self._some_value(n2, PDATA)) for n1,n2 in decorator_args)
                if n_call.keywords:
                    for k in n_call.keywords:
                        if k.arg == '_consistency':
                            if type(k.value)==ast.Tuple: conditions = k.value.elts
                            else: conditions = [k.value]
                            for elt in conditions:
                                query = self._eval_DAL_query(elt)
                                query += '& db.grupo.id.belongs((%s,%s))'%(PDATA.grupo[0], PDATA.red[0])
                                rows = self._query(query, [n for n,_ in get_args.items()])
                                if rows:
                                    get_args.update(rows[0])
                                    break

                url = 'karakolas/%s/%s%s'%(controller, funcname, ('?' if get_args else '') +
                                  '&'.join('%s=%s'%t for t in get_args.items() ))
                errors = []
                print('Comprobando url: ' + url)
                mensaje_error = 'Problema al acceder a %s.\n'%url
                for user_data, user_type in ((PDATA.usuario_grupo[1], 'usuario_grupo'),
                                             (PDATA.admin_grupo[1], 'admin_grupo'),
                                             (PDATA.admin_red[1], 'admin_red'),
                                             (PDATA.webadmin[1], 'webadmin')):
                    self._logout()
                    self._login_to_karakolas(user_data)
                    r = self.s.get(URL_KARAKOLAS + url, verify=False)
                    self.assertEqual(r.status_code, 200, mensaje_error + r.content)
                    errors.append( [s for s in ('Error', 'Internal error', 'ACCESS DENIED')
                              if s in r.content] )
                    if not errors[-1]:
                        print('Accedemos con éxito como ' + user_type + '\n')
                        break
                if errors[-1]:
                    self.assertTrue(False, mensaje_error + str(errors) + '\n\n')

    def test_210_global(self):
        'similar a test_200_global, pero ahora se usa una red y un productor coordinado cuando se pueda'
        print('TODO')

    def test_300_menus(self):
        '''Hace login con cada posible rol (miembro, admin grupo, admin red, webadmin)
        y recorre la web cual web spyder, anotando las urls para no repetirse y
        comprobando que no hay ningún error
        '''
        PDATA.mapa_web = defaultdict(set)
        PDATA.links = {}
        for user_data, user_type in ((PDATA.usuario_grupo[1], 'usuario_grupo'),
                                     (PDATA.admin_grupo[1], 'admin_grupo'),
                                     (PDATA.admin_red[1], 'admin_red'),
                                     (PDATA.webadmin[1], 'webadmin')):
            self._login_to_karakolas(user_data)
            print('login como %s'%user_type)
            print('#'*30)
            print(user_data)
            print('#'*30)

            links   = [('karakolas/default/index', '')]
            processed = set(('karakolas/default/index',))
            mensaje_error = 'En la página %s hay un link a %s, y esta página da un error de tipo %s:\n'
            while links:
                url, parent = links.pop()
                r = self.s.get(URL_KARAKOLAS + url, verify=False)
                soup = BeautifulSoup(r.content)
                body = soup.body or soup

                self.assertEqual(r.status_code, 200, mensaje_error%(parent, url, r.status_code)+ r.content)
                for s in ('Error', 'Internal error', 'ACCESS DENIED'):
                    self.assertNotIn(s, str(body), mensaje_error%(parent, url, s)+ body.prettify().encode('utf-8'))

                internal_links = [a.get('href','#') for a in soup.findAll('a')]
                new_links = [l[1:] for l in internal_links
                                if l.startswith('/karakolas')]
                new_links.extend(l.split(URL_KARAKOLAS)[1] for l in internal_links
                                if l.startswith('URL_KARAKOLAS'))
                good_links = [(l, url) for l in new_links
                                 if (l not in processed) and ('logout' not in l)]
                links.extend(good_links)
                processed.update(l for l,_ in good_links)

                PDATA.mapa_web[url.split('?')[0]].update(l.split('?')[0] for l in new_links)

            print('Completada la comprobación de la navegación como %s.'%user_type)
            print('#'*30 + '\n')
            PDATA.links[user_type] = processed
#        print
#        print PDATA.links
#        print '#'*30
        print(PDATA.mapa_web)
        import pickle
        pickle.dump(PDATA, open('dump.pickle','w'))

    def test_310_menus(self):
        '''Similar a test_300_menus, pero ademas envía todos los formularios que se encuentra,
        por supuesto entiende que normalmente se recibirá un error, pero debe ser siempre http 200
        '''
        import pickle
        PDATA = pickle.load(open('dump.pickle'))
        for user_data, user_type in ((PDATA.usuario_grupo[1], 'usuario_grupo'),
                                     (PDATA.admin_grupo[1], 'admin_grupo'),
                                     (PDATA.admin_red[1], 'admin_red'),
                                     (PDATA.webadmin[1], 'webadmin')):
            self._login_to_karakolas(user_data)
            print('login como %s'%user_type)
            print('#'*30)
            print(user_data)
            print('#'*30)

            for url in PDATA.links[user_type]:
                r = self.s.post(URL_KARAKOLAS + url, verify=False)
                soup = BeautifulSoup(r.content)
                f = soup.find('form')
                if f:
                    data = dict((input.get('name', input.get('type','')),input.get('value',''))
                             for input in f.findAll('input'))
                    data.update(dict((select.attrs.get('name',''),
                                   max(o.attrs['value'] for o in select.findAll('option')))
                             for select in f.findAll('select')))
                    for k in list(data.keys()):
                        if 'delete' in k:
                            data[k]=''
                    r = self.s.post(URL_KARAKOLAS + url,data=data, verify=False)
                    self.assertEqual(r.status_code, 200,
                    'El formulario en %s lanza errores al enviarlo: \n%s'%(url, r.content))
    def test_320_menus(self):
        '''Casa test_200_global con test_300_menus: comprueba que no haya links escondidos
        '''
        print('TODO')



