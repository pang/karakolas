# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-14 Pablo Angulo
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################

#####################################################
#En este fichero interaccionamos con una instalacion de karakolas
#usando webclient:
#
#http://web2py.com/books/default/chapter/29/14/other-recipes#Functional-testing
#
#Este fichero se llama desde test.py
#
#####################################################
###################################

import imp
from bs4 import BeautifulSoup
import unittest
import random
import re
import sys

sys.path.append('../../..')
from gluon.contrib.webclient import WebClient

def random_name():
    return ''.join(random.choice('qwertyuiasdfgh') for _ in range(10))

def set_data(d):
    global URL_KARAKOLAS, PDATA
    PDATA         = d
    URL_KARAKOLAS = d.URL_KARAKOLAS

class TestUser(unittest.TestCase):

    def setUp(self):
        self.client = WebClient(URL_KARAKOLAS + 'karakolas/default/',
                   postbacks=True)
        self.client.get('index')

    def test_1_Register(self):
        'Registra un usuario de nombre aleatorio. Si el username ya esta usado, busca otro...'
        # register
        registered_user = False
        while not registered_user:
            username = random_name()
            data = dict(first_name=username,
                        last_name='Surname',
                        username=username,
                        email=username+'@example.com',
                        password=PDATA.password,
                        password_two=PDATA.password,
                        submit='Register',
                        _formname='register')
            self.client.post('user/register', data=data)

            soup = BeautifulSoup(self.client.text)
            if soup.find('div', {'id':'username_error'}):
                pass
            elif soup.find('div', {'class':'error'}) or soup.find('form', {'id':'web2py_user_form'}):
                assert False, 'No se pudo registrar el usuario'
            else:
                registered_user = True
                PDATA.usernames.append( username )
        #Despues del registro, el usuario queda logeado (no hay email check)
        assert soup.find('div',{'class':'navbar'}) \
                   .findAll('a',{'href':re.compile('/karakolas/default/user/logout.*')}), \
                   'Despues del registro, el usuario no quedo logeado'

    def test_2_Login_Logout(self):
        'Intenta hacer login y logout con el mismo usuario que registramos antes'
        data = dict(username=PDATA.usernames[-1],
                    password=PDATA.password,
                    _formname='login')
        self.client.post('user/login', data=data)
        soup = BeautifulSoup(self.client.text)
        assert soup.find('div',{'class':'navbar'}) \
                   .findAll('a',{'href':re.compile('/karakolas/default/user/logout.*')}), \
                   'No se pudo hacer login'

        self.client.get('user/logout')
        soup = BeautifulSoup(self.client.text)
        assert soup.find('div',{'class':'navbar'}) \
                   .findAll('a',{'href':re.compile('/karakolas/default/user/login.*')}), \
                   'No se pudo hacer logout'


