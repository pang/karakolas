# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-14 Pablo Angulo
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################

#####################################################
#En este fichero interaccionamos con una instalacion de karakolas
#usando webclient:
#
#http://web2py.com/books/default/chapter/29/14/other-recipes#Functional-testing
#
#Ademas, se pueden correr los tests contra una instalacion remota
#Cuidado!, la base de datos quedará llena de basurilla, que no deberia
#interrumpir el funcionamiento normal pero por ahora no se elimina!
#
# Importante: el archivo debe ejecutarse desde el directorio karakolas/private
# donde esta ubicado
#
#####################################################
import unittest
import sys

sys.path.append('../../..')
from gluon.storage import Storage

PDATA = Storage()

#Se asume que la app se llama 'karakolas'
PDATA.URL_KARAKOLAS  = 'http://127.0.0.1:8000/'
PDATA.PASS_APPADMIN  = 'toma'
#TODO: extrae este numero de otra parte y lo comparas
PDATA.N_TABLAS_KARAKOLAS = 45
PDATA.usernames = []
PDATA.password = 'testTEST123' #usamos el mismo password para todo
PDATA.sample_records = {}
###################################

import test_base
test_base.set_data(PDATA)

#default
import test_default
TestUser = test_default.TestUser
test_default.set_data(PDATA)

#appadmin
import test_appadmin
TestAppadmin = test_appadmin.TestAppadmin
test_appadmin.set_data(PDATA)

#webadmin
#Despues de este test, la base de datos está bien poblada
import test_webadmin
TestWebadmin = test_webadmin.TestWebadmin
test_webadmin.set_data(PDATA)

#requires_get_arguments
import test_global
TestGET = test_global.TestGET
test_global.set_data(PDATA)

#eval_arit (javascript)
# TODO: invocalo si rhino o v8 esta en el path, de otro modo pasa del tema y lanza un aviso
class TestJS(unittest.TestCase):
    pass

#test_old_style
#cubre algunas cosas basicas, la importacion de hojas de calculo y PrettyPrintDecimal
# TODO: incluir esos tests, pero despues de su refactor



suite = unittest.TestSuite()
suite.addTest(unittest.makeSuite(TestUser))
suite.addTest(unittest.makeSuite(TestAppadmin))
suite.addTest(unittest.makeSuite(TestWebadmin))
suite.addTest(unittest.makeSuite(TestGET))
unittest.TextTestRunner(verbosity=2).run(suite)
