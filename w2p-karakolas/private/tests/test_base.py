# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-14 Pablo Angulo
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################

#####################################################
#En este fichero interaccionamos con una instalacion de karakolas
#usando python request:
#
# Este fichero se llama desde test.py
#
#####################################################


from bs4 import BeautifulSoup
import unittest
import random
import re
import sys
import requests
import ast

def set_data(d):
    global URL_KARAKOLAS, PASS_APPADMIN, PDATA
    PDATA              = d
    URL_KARAKOLAS      = d.URL_KARAKOLAS
    PASS_APPADMIN      = d.PASS_APPADMIN


#Webclient no sirve!!! salen mensajes del tipo:
# Changed session ID karakolas
# Changed session ID admin
# y no tira, así que uso requests para esta bateria
class TestBase(unittest.TestCase):

    def setUp(self):
        self.s = requests.Session()

    def _random_name(self):
        return ''.join(random.choice('qwertyuiasdfgh') for _ in range(10))

    def _count(self, query):
        r = self.s.post(URL_KARAKOLAS + 'karakolas/appadmin/select/db', data=dict(query=query), verify=False)
        soup = BeautifulSoup(r.content)
        users = soup.find('h4', text=re.compile('\d* selected'))
        return int(users.contents[0].split()[0])

    def _is_int(self, num):
        try:
            int(num)
            return True
        except ValueError:
            return False

    def _find_record_id(self, query):
        r = self.s.post(URL_KARAKOLAS + 'karakolas/appadmin/select/db', data=dict(query=query), verify=False)
        soup = BeautifulSoup(r.content)
        def is_my_record(tag):
            return tag.has_attr('href') and tag['href'].startswith('/karakolas/appadmin/update/db/')
        users = soup.find(is_my_record)
        return int(users.contents[0]) if users else -1

    def _find_record_by_data(self, tablename, data, fields=None):
        if not fields: fields=[(k,None,None) for k in data]
        def prepare(t):
            if t == 'on':return True
            return "'%s'"%t
        return  self._find_record_id(' & '.join([
                  "(db.%s.%s==%s)"%(tablename, fname, prepare(data[fname]))
                  for fname,ftype,_ in fields
                  if (data.get(fname) and ftype!='password')]
                ))

    def _delete_records(self, query):
        'query must involve a single table'
        r = self.s.post(URL_KARAKOLAS + 'karakolas/appadmin/select/db',
                        data=dict(query=query, delete_check='on'))

    def _query(self, query, tables):
        r = self.s.post(URL_KARAKOLAS + 'karakolas/appadmin/select/db', data=dict(query=query), verify=False)
        soup = BeautifulSoup(r.content)
        def is_rec_link(tag):
            return (tag.name=='a' and
                    tag['href'].startswith('/karakolas/appadmin/update/db/') and
                    self._is_int(tag.contents[0]))
        records = []
        for tr in soup.find('tbody').findAll('tr'):
            record = {}
            for a in tr.findAll(is_rec_link):
                ps = a['href'].split('/')
                if ps[-2] in tables:
                    record[ps[-2]] = int(ps[-1])
            records.append(record)
        return records

    def _find_record(self, tablename, rid):
        rows = self._query('db.%s.id == %s'%(tablename, rid), tablename)
        return rows[0]

    def _include_user_in_group(self, uid, gid):
        new_membership = self._get_form_data('karakolas/appadmin/insert/db/auth_membership')
        new_membership['user_id']  = uid
        new_membership['group_id'] = gid
        r = self.s.post(URL_KARAKOLAS + 'karakolas/appadmin/insert/db/auth_membership', data=new_membership, verify=False)

    def _insert_record(self, tablename, fields=None, data=None):
        data = data or {}
        new_record = self._get_form_data('karakolas/appadmin/insert/db/%s'%tablename)
        if not fields:
            fields = [(k, 'string', []) for k in new_record]
        for fname, ftype, extra in fields:
            if fname in new_record:
                v = data.get(fname) or  self._sample_value(fname, ftype, extra)
                if v != None:
                    new_record[fname] = v

        r = self.s.post(URL_KARAKOLAS + 'karakolas/appadmin/insert/db/%s'%tablename, data=new_record, verify=False)
        return new_record

    def _access_with_all_permissions(self):
        if PDATA.superadmin:
            uid, user_data = PDATA.superadmin
        else:
            uid, user_data = self._create_user_with_all_permissions()
            PDATA.superadmin = uid, user_data
        self._login_to_karakolas(user_data)

    def _login_to_karakolas(self, user_data):
        login_form = self._get_form_data('karakolas/default/user/login')
        login_form['username'] = user_data['username']
        login_form['password'] = user_data['password']
        self.s.post(URL_KARAKOLAS + 'karakolas/default/user/login',
                    data=login_form)

    def _logout(self):
        self.s.get(URL_KARAKOLAS + 'karakolas/default/user/logout', verify=False)

    def _register_user(self):
        user_data = self._get_form_data('karakolas/appadmin/insert/db/auth_user')
        username = self._random_name()
        data = dict(first_name=username,
                    last_name='Surname',
                    username=username,
                    email=username+'@example.com',
                    password=PDATA.password)
        user_data.update(data)

        r = self.s.post(URL_KARAKOLAS + 'karakolas/appadmin/insert/db/auth_user', data=user_data, verify=False)
        del data['password']
        uid = self._find_record_by_data('auth_user', data)
        return uid, user_data

    def _create_user_with_all_permissions(self):
        self._access_admin()
        uid, user = self._register_user()

        #crea grupos miembros_%d y admins_%d
        for row in self._query("db.grupo.id>0", ['grupo']):
            for s in ('miembros_%d', 'admins_%d'):
                gid = self._find_record_by_data('auth_group', {'role':s%row['grupo']})
                if gid < 0:
                    self._insert_record('auth_group',
                        fields=[('role','string',[]),
                                ('description','string',[])],
                        data={'role':s%row['grupo']})

        #da permisos al usuario en todos esos grupos
        auth_groups = self._query("(db.auth_group.id>0) & "
                                  "(~db.auth_group.role.startswith('user'))",
                                  ['auth_group'])
        for row in auth_groups:
            self._insert_record('auth_membership',
                    fields=[('user_id','auth_user',[]),
                            ('group_id','auth_group',[])],
                    data={'user_id':uid, 'group_id':row['auth_group']})
        PDATA.superadmin = uid, user
        return uid, user

    def _get_form_data(self, url):
        r = self.s.post(URL_KARAKOLAS + url, verify=False)
        soup = BeautifulSoup(r.content)
        f = soup.find('form')
        d = dict((input.get('name', input.get('type','')),input.get('value',''))
                 for input in f.findAll('input'))
#        d.update(dict((textarea.get('name',''), textarea.contents[0] if textarea.contents else '')
#                 for textarea in f.findAll('textarea')))
        d.update(dict((select.attrs.get('name',''),
                       max(o.attrs['value'] for o in select.findAll('option')))
                 for select in f.findAll('select')))
        return d

    def _parse_tables(self):
        tables = open('../models/50_tables.py').read()
        self.table_tree = ast.parse(tables)

        def is_define_table_expression(node):
            try:
                return (type(node)==ast.Expr and
                        node.value.func.value.value.id=='db' and
                        node.value.func.attr=='define_table')
            except AttributeError:
                return False

        def get_table(node):
            def get_field_type(n):
                if len(n.args) == 1: return 'text'
                n2 = n.args[1]
                if type(n2)==ast.Str: return n2.s
                elif type(n2)==ast.Constant: return n2.n
                else:                 return n2.attr
            return (node.value.args[0].s,
                    [(n.args[0].s, get_field_type(n), n.keywords)
                    for n in node.value.args
                    if (type(n)==ast.Call and n.func.id=='Field')])

        #El orden es importante...
        self.all_tables = [
            ('auth_user',
                [('username','string',[]), ('email','string',[]),
                 ('first_name','string',[]), ('last_name','string',[]),
                 ('password','password',[])]),
            ('auth_group',
                [('role','string',[]), ('description','string',[])]),
            ('auth_membership',
                [('user_id','auth_user',[]), ('group_id','auth_group',[])]),
        ]
        self.all_tables.extend(get_table(node)
               for node in ast.walk(self.table_tree)
               if (type(node)==ast.Expr and
                type(node.value)==ast.Call and
                type(node.value.func)==ast.Attribute and
                node.value.func.attr=='define_table' and
                node.value.func.value.id=='db'))

        self.tables_dict = dict(self.all_tables)

    def _get_form_and_post_it_back(self, url, data):
        form_data = self._get_form_data(url)
        form_data.update(data)
        r = self.s.post(URL_KARAKOLAS + url,data=form_data, verify=False)
        return form_data

    def _is_numeric_type(self,ftype):
        return ftype == 'integer' or ftype == 'float' or ftype.startswith('decimal')

    def _sample_value(self, fname, ftype, extra):
        for kw in extra:
            if kw.arg=='default':
                v = kw.value
                if type(v)==ast.Str: return v.s
                if type(v)==ast.Num: return v.n
                if not hasattr(v,'id'): continue
                if v.id=='True' : return 'on'
                if v.id=='False': return ''
                if v.id=='now'  :
                    from datetime import datetime
                    return datetime.now().isoformat(' ').split('.')[0]
                return None
        if fname == 'email': return '%s@example.com'%self._random_name()
        if ftype == 'text' or ftype == 'string' or ftype == 'password': return self._random_name()
        if ftype == 'boolean': return True
        if self._is_numeric_type(ftype): return random.randint(1,99)
        if ftype == 'date':
            from datetime import date
            return date.fromordinal(random.randint(1,700000)).isoformat()
        try:
            if ftype.startswith('list:reference'):
                ftype = ftype[15:]
            r = self.s.post(URL_KARAKOLAS + 'karakolas/appadmin/select/db',
                            data=dict(query='db.%s.id>0'%ftype))
            soup = BeautifulSoup(r.content)
            def is_my_record(tag):
                return (tag.has_attr('href') and
                        tag['href'].startswith('/karakolas/appadmin/update/db/%s/'%ftype))
            records = soup.findAll(is_my_record)[-1]
            return int(records.attrs['href'].split('/')[-1]) if records else -1
        except:
            return None

    def _access_admin(self):
        'Entrar en appadmin, tras hacer login en admin'
        login_data = self._get_form_data('admin/default/index')
        login_data['password'] = PASS_APPADMIN
        r = self.s.post(URL_KARAKOLAS + 'admin/default/index', data=login_data, verify=False)

