# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-14 Pablo Angulo
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################

#####################################################
#En este fichero interaccionamos con una instalacion de karakolas
#usando python request:
#
# Este fichero se llama desde test.py
#
#####################################################


import imp
from bs4 import BeautifulSoup
import unittest
import random
import re
import sys
import requests
import ast
from test_base import TestBase

def set_data(d):
    global URL_KARAKOLAS, PASS_APPADMIN, PDATA, N_TABLAS_KARAKOLAS
    PDATA              = d
    URL_KARAKOLAS      = d.URL_KARAKOLAS
    PASS_APPADMIN      = d.PASS_APPADMIN
    N_TABLAS_KARAKOLAS = d.N_TABLAS_KARAKOLAS

#Webclient no sirve!!! salen mensajes del tipo:
# Changed session ID karakolas
# Changed session ID admin
# y no tira, así que uso requests para esta bateria
class TestAppadmin(TestBase):

    def __init__(self, *args, **kwds):
        TestBase.__init__(self, *args, **kwds)
        self._parse_tables()

    def setUp(self):
        TestBase.setUp(self)
        self._access_admin()

    def test_1_access(self):
        'Es posible entrar a appadmin, tras haber hecho login en admin'
        r = self.s.get(URL_KARAKOLAS + 'karakolas/appadmin', verify=False)
        soup = BeautifulSoup(r.content)
        self.assertFalse(soup.find('input', {'id':'password'}))

    def test_2_read_table_list(self):
        'Contamos cuantas tablas se presentan en appadmin/index'
        r = self.s.get(URL_KARAKOLAS + 'karakolas/appadmin/index', verify=False)
        soup = BeautifulSoup(r.content)
        self.assertTrue( len(soup.findAll('a',text=re.compile('^db\.')))>=N_TABLAS_KARAKOLAS)

    def test_3_create_user_appadmin(self):
        'Creamos un registro en auth_user usando appadmin'
        nusers_before = self._count('db.auth_user.id>0')

        user_created = False
        while not user_created:
            new_user = self._get_form_data('karakolas/appadmin/insert/db/auth_user')
            username = self._random_name()
            new_user.update(dict(first_name=username,
                                 last_name='Surname',
                                 username=username,
                                 email=username+'@example.com',
                                 password=PDATA.password,
                                 password_two=PDATA.password))
            r = self.s.post(URL_KARAKOLAS + 'karakolas/appadmin/insert/db/auth_user', data=new_user, verify=False)

            soup = BeautifulSoup(r.content)
            if soup.find('div', {'id':'username__error'}):
                pass
            elif soup.find('div', {'class':'error'}) or soup.find('form', {'id':'web2py_user_form'}):
                assert False, 'No se pudo registrar el usuario'
            else:
                user_created = True

        nusers_after = self._count('db.auth_user.id>0')
        self.assertEqual(nusers_after, nusers_before + 1, 'No se pudo crear un usuario desde appadmin')

        #Busca el resgistro en concreto
        uid = self._find_record_id("(db.auth_user.username=='%s')"%
                                   (username))
        self.assertTrue(uid>=0)
        PDATA.usernames.append( (uid, username) )

    def test_4_include_in_webadmins(self):
        'Intenta incluir un usuario existente en el grupo webadmins, y lo registra en PDATA'
        uid, user_data = self._register_user()

        webadmins_id = self._find_record_id("db.auth_group.role=='webadmins'")
        self.assertTrue(webadmins_id>0, 'El grupo webadmins existe')
        self._include_user_in_group(uid, webadmins_id)
        #Comprueba la insercion
        mid = self._find_record_id("(db.auth_membership.user_id==%d) & "
                                   "(db.auth_membership.group_id==%d)"%
                                   (uid, webadmins_id))
        self.assertTrue(mid>=0)
        PDATA.webadmin = uid, user_data

    def test_5_each_table_is_in_appadmin(self):
        'El interfaz appadmin tiene links a todas las tablas definidas en 50_tables'
        r = self.s.post(URL_KARAKOLAS + 'karakolas/appadmin/index', verify=False)
        soup = BeautifulSoup(r.content)
        ts = [t.contents[0]
              for t in soup.findAll('a',text=re.compile('^db\.'))]
        for tablename,_ in self.all_tables:
            self.assertIn('db.%s'%tablename, ts)

    #Aparco este test por el momento, no es util y falla a menudo, sobre todo si se ejecuta varias veces...
#    def test_6_megatest(self):
#        '''Este megatest lee el archivo de tablas, y para cada tabla
#        hace un insert mediante appadmin y comprueba que se ha insertado un registro.
#        Tb incluye el registro en PDATA'''
#        for tablename, fields in self.all_tables:
#            print tablename, '...',
#
#            #cuenta antes de insertar
#            n_before = self._count('db.%s.id>0'%tablename)
#
#            #lee el formulario presentado por appadmin, y rellena usando la info de 50_tables
#            new_record = self._insert_record(tablename, fields)

#            #cuenta despues de insertar
#            n_after  = self._count('db.%s.id>0'%tablename)
#            self.assertEquals(n_after,n_before + 1, 'No se pudo insertar un registro de tipo %s'%tablename)
#
#            #busca específicamente el registro insertado
#            rid = self._find_record_by_data(tablename, new_record, fields)
#            self.assertTrue(rid>0, 'El registro de tipo %s insertado no se pudo encontrar después'%tablename)

#            #y lo guarda en PDATA
#            PDATA.sample_records[tablename] = rid
#            print 'ok'

#    def test_7_clean_the_mess(self):
#        '''Remove all the data inserted in the previous test, as it is inconsistent.
#
#        For example, a group may not both be a network and have members...'''
#        for tablename, _ in self.all_tables:
#            rid = PDATA.sample_records[tablename]
#            self._delete_records('db.%s.id==%d'%(tablename,rid))

