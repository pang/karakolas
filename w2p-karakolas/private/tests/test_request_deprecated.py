#####################################################
#En este fichero interaccionamos con una instalacion de karakolas
#usando requests, que se parece bastante a una interaccion real
#Ademas, se pueden correr los tests contra una instalacion
#remota, o incluso en produccion

#KARAKOLAS
URL_KARAKOLAS      = 'http://127.0.0.1:8000/karakolas'
USERNAME_KARAKOLAS = 'truco'
PASSWORD_KARAKOLAS = 'truco'
###################################

import requests
from bs4 import BeautifulSoup
from bs4.element import Tag
from decimal import Decimal
import re
import shutil
import os
import sys
import logging
import unittest
import random

def url2data(url):
    return dict(kv.split('=') for kv in s.split('?')[1].split('&'))


class TestLogin(unittest.TestCase):
    def setUp(self):
        self.s = requests.Session()
        rand_username = ''.join(random.choice('qwertyuiasdfgh') for _ in range(10))
        self.username = rand_username

    def test_1_Register(self):
        r = self.s.post(URL_KARAKOLAS + '/default/user/register')
        soupr = BeautifulSoup(r.content)

        register_karakolas = dict((input.attrs.get('name',0),input.attrs.get('value','')) for input in soupr.find('form').findAll('input') )
        register_karakolas['first_name'] = self.username
        register_karakolas['last_name'] = 'Boniato'
        register_karakolas['username'] = self.username
        register_karakolas['email'] = '%s@example.com'%self.username
        register_karakolas['password'] = PASSWORD_KARAKOLAS
        register_karakolas['password_two'] = PASSWORD_KARAKOLAS
        r = self.s.post(URL_KARAKOLAS + '/default/user/register', data=register_karakolas)
        soupr = BeautifulSoup(r.content)
        assert soupr.find(id='navbar').find(text=re.compile('Logout'))

    def test_2_Logout(self):
        'Intenta hacer logout con el mismo usuario que registramos antes'
        r = self.s.post(URL_KARAKOLAS + '/default/user/logout')
        soupr = BeautifulSoup(r.content)
        assert soupr.find(id='navbar').find(text=re.compile('Login'))

    def test_3_Login(self):
        'Intenta hacer login con el mismo usuario que registramos antes'
        r = self.s.post(URL_KARAKOLAS + '/default/user/login')
        soupr = BeautifulSoup(r.content)

        login_karakolas = {input.attrs.get('name',0):input.attrs['value'] for input in soupr.find('form').findAll('input')}
        login_karakolas['username'] = self.username
        login_karakolas['password'] = PASSWORD_KARAKOLAS
        r = self.s.post(URL_KARAKOLAS + '/default/user/login', data=login_karakolas)
        soupr = BeautifulSoup(r.content)
        assert soupr.find(id='navbar').find(text=re.compile('Logout'))


suite = unittest.TestSuite()
suite.addTest(unittest.makeSuite(TestLogin))
unittest.TextTestRunner(verbosity=2).run(suite)





