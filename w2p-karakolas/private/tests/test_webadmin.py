# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-14 Pablo Angulo
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################

#####################################################
#En este fichero interaccionamos con una instalacion de karakolas
#usando python request:
#
# Este fichero se llama desde test.py
#
# Para que TestWebadmin funcione, PDATA.webadmin debe ser (uid, username)
#para un usuario del grupo webadmins
#####################################################

from bs4 import BeautifulSoup
import unittest
import random
import re
import sys
import requests
import ast
from test_base import TestBase

def set_data(d):
    global URL_KARAKOLAS, PASS_APPADMIN, PDATA
    PDATA              = d
    URL_KARAKOLAS      = d.URL_KARAKOLAS
    PASS_APPADMIN      = d.PASS_APPADMIN

#Webclient no sirve!!! salen mensajes del tipo:
# Changed session ID karakolas
# Changed session ID admin
# y no tira, así que uso requests para esta bateria
class TestWebadmin(TestBase):

    def __init__(self, *args, **kwds):
        TestBase.__init__(self, *args, **kwds)
        self._parse_tables()

    def setUp(self):
        TestBase.setUp(self)
        self._access_admin()
        self._login_as_webadmin()

    def _login_as_webadmin(self):
        try:
            _, user_data = PDATA.webadmin
            username = user_data['username']
        except TypeError as ValueError:
            raise Exception("Para que TestWebadmin funcione, PDATA.webadmin debe ser (uid, username) para un usuario del grupo webadmins")
        user_data = dict(username=username, password=PDATA.password)
        self._login_to_karakolas(user_data)

    def _create_new_group(self, nombre_grupo, red=False):
        data = self._get_form_and_post_it_back('karakolas/webadmin/add_group.html',
            {'nombre': nombre_grupo,
             'prueba':'',
             'discreto': '',
             'red': red or ''
            })

        #Busca el id del grupo recien creado
        gid = self._find_record_by_data('grupo', data=dict(nombre=nombre_grupo))

        #Busca el grupo admins_%d (lo crea add_group automaticamente)
        tablename = 'auth_group'
        fields    = self.tables_dict[tablename]
        admins_data ={'role':'admins_%d'%gid}
        admins_id = self._find_record_by_data(tablename, admins_data, fields)

        #Busca el grupo miembros_%d (lo crea add_group automaticamente)
        if not red:
            miembros_data = {'role':'miembros_%d'%gid}
            miembros_id = self._find_record_by_data(tablename, miembros_data, fields)
        else:
            miembros_id = -1
        return gid, miembros_id, admins_id

    def test_1_menus_de_webadmin(self):
        r = self.s.post(URL_KARAKOLAS + 'karakolas/default/index', verify=False)
        soup = BeautifulSoup(r.content)
        #assert there is a menu to create a new group
        self.assertTrue(any(ul.find('a',{'href':'/karakolas/webadmin/add_group.html'})
            for ul in  soup.findAll('ul', {'class':'dropdown-menu'})))
        #assert there is a menu to list all categories
        self.assertTrue(any(ul.find('a',{'href':'/karakolas/productores/grid_all_categorias.html'})
            for ul in  soup.findAll('ul', {'class':'dropdown-menu'})))
        #assert there is a menu to list all groups
        self.assertTrue(any(ul.find('a',{'href':'/karakolas/webadmin/index.html'})
            for ul in  soup.findAll('ul', {'class':'dropdown-menu'})))

    def test_2_nuevo_grupo(self):
        nombre_grupo = self._random_name()
        gid, miembros_id, admins_id = self._create_new_group(nombre_grupo)

        r = self.s.get(URL_KARAKOLAS + 'karakolas/webadmin/lista_grupos.html', verify=False)
        soup = BeautifulSoup(r.content)

        #El grupo está en la lista de grupos
        self.assertTrue(soup.find('table', {'id':'tabla_lista_grupos'}).find('td', text=nombre_grupo))

        #Se han creado el grupo y los auth_group correspondientes
        self.assertTrue(gid>0)
        self.assertTrue(miembros_id>0)
        self.assertTrue(admins_id>0)
        #Incluye en miembros_%d al usuario_grupo
        PDATA.usuario_grupo = self._register_user()
        tablename = 'auth_membership'
        fields    = self.tables_dict[tablename]
        #No lo hago ahora, lo hare despues, tb hay que incluirlo en personaXgrupo...
#        self._insert_record(tablename, fields,
#                data={'group_id':miembros_id, 'user_id':PDATA.usuario_grupo[0]})
        #Incluye en admins_%d al admin_grupo
        PDATA.admin_grupo   = self._register_user()
        self._insert_record(tablename, fields,
                data={'group_id':admins_id, 'user_id':PDATA.admin_grupo[0]})
        PDATA.grupo = gid, nombre_grupo

    def test_3_nueva_red(self):
        nombre_grupo = self._random_name()
        gid, miembros_id, admins_id = self._create_new_group(nombre_grupo, red=True)

        r = self.s.get(URL_KARAKOLAS + 'karakolas/webadmin/lista_grupos.html', verify=False)
        soup = BeautifulSoup(r.content)

        #El grupo está en la lista de grupos
        self.assertTrue(soup.find('table', {'id':'tabla_lista_grupos'}).find('td', text=nombre_grupo))

        #Se han creado el grupo y los auth_group correspondientes
        self.assertTrue(gid>0)
        self.assertTrue(admins_id>0)
        tablename = 'auth_membership'
        fields    = self.tables_dict[tablename]
        #Incluye en admins_%d al admin_grupo
        PDATA.admin_red     = self._register_user()
        self._insert_record(tablename, fields,
                data={'group_id':admins_id, 'user_id':PDATA.admin_red[0]})
        PDATA.red = gid, nombre_grupo

