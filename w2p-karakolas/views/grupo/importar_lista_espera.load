<!--
 (C) Copyright (C) 2012-23 Authors of karakolas
 This file is part of karakolas <karakolas.org>.

 karakolas is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 karakolas is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Affero General Public
 License along with karakolas.  If not, see
 <http://www.gnu.org/licenses/>.
-->

{{=TITLE()}}

<div class="panel panel-info">
    <div class="panel-heading">
    <a role="button" data-toggle="collapse" href="#info-body" aria-expanded="false" aria-controls="info-body">
    Info
    <span class="glyphicon glyphicon-expand">
    </a>
    </div>
    <div class="panel panel-body collapse" id="info-body">
{{=MARKMIN(T('''
Las personas que incluyas debajo serán incorporadas a la lista de espera. Si una persona ya está en la lista de espera, o ya es miembro del grupo, ''no se hace nada''. En otro caso:

 - Se crea un usuario en esta web con los datos que indicas, y se pone un password aleatorio imposible de recuperar, pero que el usuario podrá resetear.
 - Se incorpora a este usuario a la lista de espera del grupo de consumo.
 - El usuario recibirá un email con instrucciones para resetear su password, y se le explicará que está en la lista de espera. El texto que se usa lo puedes modificar en la página para [[editar los datos del grupo  %(link_editar_datos_grupo)s]]


''Nota'': Se enviará un email automático con instrucciones a las nuevas usuarias, pero a veces estos emails terminan en la carpeta de spam, así que por favor aségurate de que lo han recibido y si no es así, hazles llegar esta información. Lo más importante es que reciban el link:

-----------
[[%(url_reset)s %(url_reset)s]]
-----------

En este link podrán pedir el cambio de password para acceder a la aplicación. Cuando lo hagan, probablemente no encuentren gran cosa, porque no pertenecen a ningún grupo de consumo.

''',dict(
    link_editar_datos_grupo=URL(c='grupo',f='editar_datos_grupo.html', vars=dict(grupo=grupo)),
    url_reset=URL(c='default',f='user',args=['request_reset_password'],scheme='https')
)).xml())}}
    </div>
</div>
{{=sheet}}

{{=add_libs(response.files)}}
