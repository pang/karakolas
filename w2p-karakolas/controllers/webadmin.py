# -*- coding: utf-8 -*-
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.


# ##########################################################
# ## make sure administrator is on localhost
# ###########################################################

#import os
import socket
#import datetime
#import copy
import gluon.contenttype
import gluon.fileutils
#from gluon.sqlhtml import SQLHTML

http_host = request.env.http_host.split(':')[0]
remote_addr = request.env.remote_addr
try:
    hosts = (http_host, socket.gethostname(),
             socket.gethostbyname(http_host),
             '::1','127.0.0.1','::ffff:127.0.0.1')
except:
    hosts = (http_host, )

if request.env.http_x_forwarded_for or request.env.wsgi_url_scheme\
     in ['https', 'HTTPS']:
    session.secure()
elif (remote_addr not in hosts) and (remote_addr != "127.0.0.1"):
    raise HTTP(200, T('Tienes que acceder por https'))

if (request.application=='admin' and not session.authorized) or \
        (request.application!='admin' and not gluon.fileutils.check_credentials(request)):
    redirect(URL('admin', 'default', 'index.html',
                 vars=dict(send=URL(a=request.application, c=request.controller, f=request.function,
                                    args=request.args, vars=request.vars))))

ignore_rw = True
#response.view = 'webadmin.html'
#response.menu = [[T('index'), False, URL('index')]]

def index():
    redirect(URL(f='lista_grupos'))

#@scope.set('admin-web')
def add_group():
    response.title = T('Crear nuevo grupo o red')
    form = SQLFORM(db.grupo, ignore_rw=True)
    if form.accepts(request.vars, session):
        gid = form.vars.id
        nombre = form.vars.nombre
        if form.vars.red:
            auth.add_group('admins_%d'%gid, 'Administradores de la red %s'%nombre)
#            redirect(URL('admin_red', vars=dict(grupo=id)))
        else:
            auth.add_group('miembros_%d'%gid, 'Miembros del grupo %s'%nombre)
            auth.add_group('admins_%d'%gid, 'Administradores del grupo %s'%nombre)
            from modelos.grupos import textos_grupo_por_defecto
            textos_grupo_por_defecto(gid)
#            redirect(URL('admin_grupo', vars=dict(grupo=id)))
        #Activa primera moneda
        primera_moneda = db(db.moneda).select(db.moneda.id, limitby=(0,1)).first()
        if primera_moneda:
            db.grupoXmoneda.insert(grupo=gid, moneda=primera_moneda.id, activo=True)

        response.flash = 'form accepted'
        redirect(URL(f='lista_grupos'))
    elif form.errors:
        response.flash = 'form has errors'
    return dict(form=form)

#@scope.set('admin-web')
def admin_grupo():
    igrupo    = int(request.vars.grupo)
    grupo     = db.grupo[igrupo]
    if grupo.red:
        redirect(URL(f='admin_red', vars=dict(grupo=igrupo)))
    imiembros = auth.id_group('miembros_%d'%igrupo)
    iadmins   = auth.id_group('admins_%d'%igrupo)
    new_admin = FORM(FIELDSET(T('username: '), INPUT(_name='admin_username')),
                     INPUT(_type='submit', _name='incluir_admin'))
    if request.vars.incluir_admin:
    #Lo que sigue intenta conseguir que autocomplete nombres de usuario
    #con el widget de autocomplete, pero cuando pongo dos de esos widgets,
    #el segundo ofrece las sugerencias junto al primer widget
#    new_admin = SQLFORM.factory(Field('username',
#                                widgets=SQLFORM.widgets.autocomplete(
#                                        request, db.auth_user.username,
#                                        db.auth_user.id, min_length=2)))
#    if new_admin.accepts(request.vars):
        try:
            user_id = db.auth_user(username=request.vars.admin_username).id
            if not db.auth_membership(group_id=iadmins, user_id=user_id):
                db.auth_membership.insert(**dict(group_id=iadmins, user_id=user_id))
            response.flash = 'ok'
        except:
            response.flash = 'Usuario desconocido'

    new_miembro = FORM(FIELDSET(T('username: '), INPUT(_name='miembro_username')),
                       FIELDSET(T('Número de unidad: '), INPUT(_name='miembro_unidad')),
                       INPUT(_type='submit', _name='incluir_miembro'))
    if request.vars.incluir_miembro:
        try:
            user_id = db.auth_user(username=request.vars.miembro_username).id
            db.personaXgrupo.insert(persona=user_id, grupo=igrupo,
                                    unidad=int(request.vars.miembro_unidad))
            db.auth_membership.insert(**dict(group_id=imiembros, user_id=user_id))
            response.flash = 'ok'
        except:
            response.flash = 'Usuario desconocido'

    miembros = db(db.personaXgrupo.grupo==igrupo) \
               .select(db.personaXgrupo.ALL, orderby=db.personaXgrupo.unidad)
    admins = db((db.auth_membership.group_id==iadmins)) \
               .select(db.auth_membership.user_id)
    return dict(igrupo=igrupo, imiembros=imiembros, iadmins=iadmins,
                nombre_grupo=grupo.nombre,
                new_admin=new_admin, new_miembro=new_miembro,
                miembros=miembros, admins=admins)

#@scope.set('admin-web')
def lista_grupos():
    response.title = T('Lista de grupos que usan la aplicacion')
    for k,v in request.vars.items():
        if k.startswith('del_'):
            id = int(k[4:])
            auth.del_group(auth.id_group('miembros_%d'%id))
            auth.del_group(auth.id_group('admins_%d'%id))
            db(db.grupo.id==id).delete()

    grupos = db().select(db.grupo.ALL)
    f = FORM(TABLE(TR(TD(T('Nombre')), TD(T('Descripción')),
                    TD(T('Editar miembros')),
                    TD(T('Eliminar grupo'))),
                 *[TR(TD(g.nombre),
                      TD(g.descripcion),
                      TD(A('Editar miembros', _href=URL('admin_grupo', vars=dict(grupo=g.id)))),
                      TD(INPUT(_value='Eliminar', _type='checkbox', _name='del_%s'%g.id) ))
                   for g in grupos],
                 **dict(_id='tabla_lista_grupos')),
            INPUT(_type='submit', _name='submit', _value='Eliminar grupos seleccionados'))
    return dict(lista_grupos=f)

#@scope.set('admin-web')
def admin_red():
    ired    = int(request.vars.grupo)
    red     = db.grupo[ired]
    if not red.red:
        redirect(URL(f='admin_grupo', vars=dict(grupo=ired)))
    iadmins = auth.id_group('admins_%d'%ired)

    new_miembro = FORM(FIELDSET(T('username del nuevo administrador: '),
                                INPUT(_name='miembro_username')),
                       INPUT(_type='submit', _name='incluir_miembro'))
    if request.vars.incluir_miembro:
#        raise Exception, request.vars
        try:
            user_id = db.auth_user(username=request.vars.miembro_username).id
            if not db.auth_membership(group_id=iadmins, user_id=user_id):
                db.auth_membership.insert(**dict(group_id=iadmins, user_id=user_id))
            response.flash = 'ok'
        except:
            response.flash = 'Usuario desconocido'

    miembros = db(db.personaXgrupo.grupo==ired) \
               .select(db.personaXgrupo.ALL, orderby=db.personaXgrupo.unidad)
    return dict(ired=ired, iadmins=iadmins,
                nombre_red=red.nombre,
                new_miembro=new_miembro,
                miembros=miembros)

#@scope.set('admin-web')
def incluir_grupo_en_red():
    ired    = int(request.vars.red)
    from modelos.grupos import incluir_grupo_en_red, grupos_y_redes_conectados

    grupos_prohibidos = grupos_y_redes_conectados(ired)
    grupos = [(row.id, row.nombre)
              for row in db(~db.grupo.id.belongs(grupos_prohibidos))\
                        .select(db.grupo.id, db.grupo.nombre)
             ]
    form_grupoXred = SQLFORM.factory(Field('grupo', requires=IS_IN_SET(grupos)))
    if form_grupoXred.process().accepted:
        igrupo = int(form_grupoXred.vars.grupo)
        if db((db.grupoXred.grupo==igrupo) &
              (db.grupoXred.red==ired) &
              (db.grupoXred.activo==True)
             ).count()>0:
            response.flash = T('Ese grupo ya estaba en la red. No hemos hecho nada.')
        elif igrupo in grupos_prohibidos:
            response.flash = T('Ese grupo ya está en la red, aunque no de forma directa. No hemos hecho nada.')
        else:
            incluir_grupo_en_red(igrupo,ired)
            response.flash = T('Hemos incorporado al grupo a la red.')
    return dict(form_grupoXred=form_grupoXred, ired=ired)

@scope.set('admin-web')
@auth.requires_membership('webadmins')
def grid_all_productores():
    grid = SQLFORM.grid(db.productor)
    return locals()

#@scope.set('admin-web')
@auth.requires_membership('webadmins')
def grid_all_categorias():
    grid = SQLFORM.grid(db.categorias_productos)
    return locals()

#@scope.set('admin-web')
def grid_monedas():
    grid = SQLFORM.grid(db.moneda)
    return locals()
