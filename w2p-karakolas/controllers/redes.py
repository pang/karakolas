# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.
from kutils import represent

@scope.set('admin-red')
#@requires_get_arguments(('red', db.grupo))
@auth.requires_membership('admins_%s'%request.vars.red)
def admin_red():
    ired    = int(request.vars.red)
    red     = db.grupo[ired]

    response.title = T('Administración de  %s')%red.nombre

    if not red.red:
        redirect(URL(c='grupo', f='admin_grupo.html', vars=dict(grupo=ired)))
    iadmins = auth.id_group('admins_%d'%ired)
    return dict(ired=ired, iadmins=iadmins)

#@requires_get_arguments(('red', db.grupo))
@auth.requires(auth.has_membership('admins_%s'%request.vars.red) or
               auth.has_membership('webadmins'))
def lista_grupos_en_red():
    from modelos.grupos import sacar_grupo_de_la_red

    ired    = int(request.vars.red)
    for k,v in request.vars.items():
        if k.startswith('del_'):
            igrupo = int(k[4:])
            sacar_grupo_de_la_red(igrupo, ired)

    grupos = db((db.grupoXred.red==ired) &
                (db.grupoXred.activo==True) &
                (db.grupoXred.grupo==db.grupo.id)) \
               .select(db.grupo.ALL)
    f = FORM(TABLE(TR(TH(T('Grupo o Red')),
                      TH(T('Nombre')),
                      TH(T('Eliminar de la red'))),
                   *[TR(TD(T('Red') if g.red else T('Grupo')),
                        TD(g.nombre),
                        TD(INPUT(_value='Eliminar', _type='checkbox', _name='del_%s'%g.id) ))
                     for g in grupos]),
            INPUT(_type='submit', _name='submit', _value='Sacar de la red a los grupos seleccionados'))

    return dict(grupos=f)

#@requires_get_arguments(('red', db.grupo))
@auth.requires_membership('admins_%s'%request.vars.red)
def grupos_en_red():
    ired    = int(request.vars.red)
    return dict(ired=ired)

@scope.set('admin-red')
@auth.requires_membership('admins_%s'%request.vars.red)
def incidencias_desdered():
    ipedido   = int(request.vars.pedido)
    pedido    = db.pedido(ipedido)
    ired = int(request.vars.red)

    response.title = ('Resuelve discrepancias con los grupos para %s')%format_pedido(pedido)
    #cols
    query_hijos = ((db.proxy_pedido.pedido==ipedido) &
                   (db.proxy_pedido.activo==True) &
                   (db.proxy_pedido.grupo==db.grupo.id) &
                   (db.grupoXred.activo==True) &
                   (db.grupoXred.grupo==db.grupo.id) &
                   (db.grupoXred.red==ired))
    ls_grupos = [(row.grupo.id, row.grupo.nombre,
                  row.proxy_pedido.discrepancia_ascendiente)
                    for row in db(query_hijos)
                        .select(db.grupo.id, db.grupo.nombre,
                                db.proxy_pedido.discrepancia_ascendiente,
                                orderby=db.grupo.nombre)]
    col_ids = [id for id,_,_ in ls_grupos]

    return dict(ired=ired,
                ipedido=ipedido,
                grupos=ls_grupos)

@scope.set('admin-red')
#@requires_get_arguments(('pedido', db.pedido))
@auth.requires(Permisos.autoriza_pedido_coordinado(request.vars.pedido, request.vars.grupo))
#@auth.requires(Permisos.autoriza_pedido(request.vars, perms='sa'))
def vista_pedido_red():
    # muestra las subredes descendientes directos, no los grupos
    incidencias = request.vars.incidencias
    ipedido   = int(request.vars.pedido)
    pedido    = db.pedido(ipedido)
    # Mantenemos el nombre grupo para la variable GET por compatibilidad con
    #  gestion_pedidos/vista_pedido
    ired = int(request.vars.grupo)
    nred = db.grupo(ired).nombre

    if not incidencias:
        incidencias = '3' if pedido.abierto else '2'

    if incidencias=='1':
        response.title = (T('Total recibido por grupos del %s') + ' (%s)')%(
            format_pedido(pedido), nred)
    elif incidencias=='2':
        response.title = (T('Total suministrado a los grupos del %s') + ' (%s)')%(
            format_pedido(pedido), nred)
    else:
        response.title = (T('Total pedido por grupos del %s') + ' (%s)')%(
            format_pedido(pedido), nred)

    return request.vars

@scope.set('admin-red')
#@requires_get_arguments(('pedido', db.pedido))
@auth.requires(Permisos.autoriza_pedido_coordinado(request.vars.pedido, request.vars.grupo))
#@auth.requires(Permisos.autoriza_pedido(request.vars, perms='sa'))
def vista_totales_red():
    '''Muestra tabla y grafica de productos x grupo para un pedido fijo,
    con la cantidad total que se pide de cada producto en cada grupo
    '''
    from rsheet import Tabular
    from estadisticas import Estadistica
    from modelos.grupos import nodos_en_la_red
    from modelos.pedidos import recalcula_costes_extra_pedido
    tpxp = db.productoXpedido
    tpp  = db.proxy_pedido

    incidencias = request.vars.incidencias

    ipedido   = int(request.vars.pedido)
    pedido    = db.pedido(ipedido)
    ired      = int(request.vars.grupo)
    red       = db.grupo(ired)
    igrupo_productor = pedido.productor.grupo.id
    es_propio = (ired == igrupo_productor)
    if not red.red:
        redirect(URL(c='gestion_pedidos', f='vista_pedido.html', vars=request.vars))

    #mostrar 3Peticiones(peticiones), 1Recibido(item_grupo), 2Suministrado(item_red)
    if not incidencias:
        incidencias = '3' if pedido.abierto else '2'

    contabilidad = ( red.opcion_ingresos != CONT.OPCIONES_INGRESOS.NO_CONT )

    tabla = db.item_grupo
    if incidencias=='1':
        response.title = T('Total recibido por grupos del %s')%format_pedido(pedido)
        campo_cantidad = tabla.cantidad_recibida
        link_otra_accion = URL(c='redes', f='vista_pedido_red.html',
                               vars=dict(pedido=pedido.id, grupo=ired, incidencias='3'))
        link_otra_accion2 = URL(c='redes', f='vista_pedido_red.html',
                               vars=dict(pedido=pedido.id, grupo=ired, incidencias='2'))
    elif incidencias=='2':
        response.title = T('Total suministrado a grupos del %s')%format_pedido(pedido)
        campo_cantidad = tabla.cantidad_red
        link_otra_accion = URL(c='redes', f='vista_pedido_red.html',
                               vars=dict(pedido=pedido.id, grupo=ired, incidencias='3'))
        link_otra_accion2 = URL(c='redes', f='vista_pedido_red.html',
                               vars=dict(pedido=pedido.id, grupo=ired, incidencias='1'))
    else:  #incidencias=='3'
        response.title = T('Total pedido por grupos del %s')%format_pedido(pedido)
        campo_cantidad = tabla.cantidad_pedida
        link_otra_accion = URL(c='redes', f='vista_pedido_red.html',
                               vars=dict(pedido=pedido.id, grupo=ired, incidencias='1'))
        link_otra_accion2 = URL(c='redes', f='vista_pedido_red.html',
                               vars=dict(pedido=pedido.id, grupo=ired, incidencias='2'))

    if request.vars.recalcula_costes_extra:
        incidencias_coste_pedido = recalcula_costes_extra_pedido(ipedido)
        if incidencias_coste_pedido:
            response.flash = T('Se ha producido un error al calcular el coste fijo del pedido. Por favor atiende a los problemas indicados más abajo')
    else:
        incidencias_coste_pedido = []

    def represent_prods(pid):
        return tpxp[pid].nombre

    ngrupos = nodos_en_la_red(ired, [ipedido])
    id_grupos = list(ngrupos.keys())
    def represent_grupos(gid):
        return ngrupos[gid]

    # Coste de los productos de cada subgrupo
    q = ((tabla.productoXpedido==tpxp.id) &
         (tpxp.categoria==db.categorias_productos.id) &
         (tpxp.pedido==ipedido) &
         (tabla.grupo==db.grupo.id) &
          db.grupo.id.belongs(id_grupos))
    data = Tabular(q,
                   db.grupo.id,
                   tpxp.id,
                   campo_cantidad.sum(),
                   row_represent=represent_grupos,
                   col_represent=represent_prods,
                   orderby=db.categorias_productos.nombre|tpxp.nombre)
    stat = Estadistica(data,
        rosheet_options=dict(_id='tabla_coste', col_totals=True, transpose=True)
    )

    # Coste extra que se cobra a cada subgrupo
    data_extra = [(row.grupo.nombre, row.proxy_pedido.coste_red)
        for row in db((tpp.pedido==ipedido) &
                      (tpp.grupo==db.grupo.id) &
                       db.grupo.id.belongs(id_grupos)
                   ).select(db.grupo.nombre, tpp.coste_red, orderby=db.grupo.nombre)
    ]
    coste_extra_descendiente = sum(coste for _, coste in data_extra)

    # Coste extra que esta red paga a la superred o al productor
    if incidencias_coste_pedido:
        coste_extra_ascendiente = Decimal()
    elif es_propio:
        coste_extra_ascendiente = pedido.coste_extra_productor or Decimal()
        formula_coste_extra_ascendiente = pedido.formula_coste_extra_productor
        formula_coste_extra_descendiente = pedido.formula_coste_extra_pedido
    else:
        ppedido = db.proxy_pedido(pedido=ipedido, grupo=ired)
        coste_extra_ascendiente = ppedido.coste_red or Decimal()
        via = ppedido.via
        if via==igrupo_productor:
            formula_coste_extra_ascendiente = pedido.formula_coste_extra_pedido
        else:
            ppedido_via = db.proxy_pedido(pedido=ipedido, grupo=via)
            formula_coste_extra_ascendiente = ppedido_via.formula_coste_extra_pedido
        formula_coste_extra_descendiente = ppedido.formula_coste_extra_pedido

    # Coste total de los productos que esta red paga a la superred o al productor
    total_productor = db.item_grupo.precio_red.sum()
    coste_total_item_ascendiente = db(
        (db.item_grupo.productoXpedido==tpxp.id) &
        (db.item_grupo.grupo==ired) &
        (tpxp.pedido==ipedido)
    ).select(total_productor).first()[total_productor] or Decimal()

    hay_discrepancias=False
    discrepancia_cerrada = 0
    if pedido.abierto:
        grupos_pendientes_consolidar = []
    else:
        grupos_pendientes_consolidar = [ngrupos.get(row.grupo, '') for row in
            db((db.proxy_pedido.pedido==ipedido) &
               (db.proxy_pedido.activo==True) &
               (db.proxy_pedido.pendiente_consolidacion==True)
            ).select(db.proxy_pedido.grupo) ]

    return dict(stat=stat,
                pedido=ipedido,
                pedido_abierto=pedido.abierto,
                usa_plantilla=pedido.usa_plantilla,
                incidencias=incidencias,
                red=ired,
                es_propio=es_propio,
                #TODO
#                coste_extra=coste_extra,
#                formula_coste_extra=pedido.formula_coste_extra_pedido,
                data_extra=data_extra,
                coste_total_item_ascendiente = coste_total_item_ascendiente,
                coste_extra_ascendiente=coste_extra_ascendiente,
                formula_coste_extra_ascendiente=formula_coste_extra_ascendiente,
                coste_extra_descendiente=coste_extra_descendiente,
                formula_coste_extra_descendiente=formula_coste_extra_descendiente,
                incidencias_coste_pedido=incidencias_coste_pedido,
                link_otra_accion=link_otra_accion,
                link_otra_accion2=link_otra_accion2,
                pendiente_consolidacion=pedido.pendiente_consolidacion,
                grupos_pendientes_consolidar=grupos_pendientes_consolidar)

@scope.set('admin-red')
#@requires_get_arguments(('pedido', db.pedido))
@auth.requires_membership('admins_%s'%request.vars.red)
def elegir_grupos_pedido():
    from modelos.pedidos import elimina_proxy_pedido, \
                                abre_pedidos_en_grupos_de_la_red, \
                                str_discrepancias

    ipedido    = int(request.vars.pedido)
    pedido     = db.pedido(ipedido)
    productor  = db.pedido(ipedido).productor
    ired       = int(request.vars.red)
    red        = db.grupo(ired)
    assert red.red
    response.title = T('Grupos y subredes de %s en el %s')%(red.nombre, format_pedido(pedido))

    tig = db.item_grupo
    textos_discrepancias = str_discrepancias()

    subredes_directas = [row.grupo
        for row in db((db.grupoXred.red==ired) &
                      (db.grupoXred.activo==True)
                   ).select(db.grupoXred.grupo)
    ]
    grupos = db((db.proxy_productor.productor==productor) &
                (db.proxy_productor.grupo==db.grupo.id) &
                 db.proxy_productor.grupo.belongs(subredes_directas)) \
             .select(db.grupo.nombre, db.grupo.id, db.proxy_productor.ALL)

    item_red_total = tig.precio_red.sum()
    item_recibido_total = tig.precio_red.sum()
    rows = db((tig.productoXpedido==db.productoXpedido.id) &
              (db.productoXpedido.pedido==ipedido)) \
           .select(item_red_total, item_recibido_total,
                   tig.grupo, groupby=tig.grupo)
    enviado_grupo = defaultdict(lambda:(Decimal(), Decimal()))
    for r in rows:
        enviado_grupo[r[tig.grupo]] = (r[item_red_total], r[item_recibido_total])

    rows_pp = db((db.proxy_pedido.pedido==ipedido)) \
           .select(db.proxy_pedido.ALL)
    # pedido_grupo devuelve un diccionario vacío si le pasamos un id de grupo que
    # no tiene proxy_pedido, así que pedido_grupo[row.grupo.id].get('activo')
    # es None si no hay proxy_pedido para ese grupo
    pedido_grupo = defaultdict(dict)
    for r in rows_pp:
        pedido_grupo[r.grupo] = r

    trthead = TR(TH(T('Grupo')),
                 TH(T('Estado')),
                 TH(T('Ofrecido')),
                 TH(T('Aceptado')),
                 TH(T('Pedido') if pedido.abierto else T('Entregado')) )
    trstbody = [TR(
         TH(row.grupo.nombre),
         TD(C.ICONOS_PROXY_PRODUCTOR[row.proxy_productor.estado]),
         TD(INPUT(_type='checkbox',
                  value=('on' if (row.grupo.id in pedido_grupo) else ''),
                  **(dict(_disabled='disabled') if row.proxy_productor.estado=='bloqueado' else
                     dict(_name='activar_%d'%row.grupo.id)))
         ),
         TD(SPAN(_class=('glyphicon glyphicon-ok'
                         if pedido_grupo[row.grupo.id].get('activo') else
                         'glyphicon glyphicon-remove'
         ))),
         TD(enviado_grupo[row.grupo.id][0])
                ) for row in grupos
    ]
    if not pedido.abierto:
        trthead.append(TH(T('Recibido')))
        trthead.append(TH(T('Pendiente')))
        trthead.append(TH(T('Discrepancia')))
        for row,tr in zip(grupos, trstbody):
            tr.append(TD(enviado_grupo[row.grupo.id][0]))
            pedido = pedido_grupo[row.grupo.id]
            #TODO HERE icons
            tr.append(TD(
                SPAN(_class='glyphicon glyphicon-warning-sign icon-warning',
                     _title=textos_discrepancias.pendiente_consolidacion_desdered,
                     **{'_data-toggle':'tooltip', '_data-placement':'bottom'})
                if pedido.get('pendiente_consolidacion') else
                SPAN(_class='glyphicon glyphicon-ok icon-ok')
            ))
            tr.append(TD(
                SPAN(_class='glyphicon glyphicon-warning-sign icon-warning',
                     _title=textos_discrepancias.discrepancia_ascendiente,
                     **{'_data-toggle':'tooltip', '_data-placement':'bottom'})
                if pedido.get('discrepancia_ascendiente') else
                SPAN(_class='glyphicon glyphicon-ok icon-ok')
            ))

    f = FORM(DIV(TABLE(
        THEAD(trthead),
        TBODY(*trstbody)),
          _class="pedidos"),
          INPUT(_type='submit', _value=T('Enviar')))
    if f.process().accepted:
        grupos_para_abrir = [row.grupo.id for row in grupos
                 if (row.proxy_productor.estado!='bloqueado' and
                     f.vars['activar_%d'%row.grupo.id] and
                    (not pedido_grupo.get(row.grupo.id)))
        ]
        if grupos_para_abrir:
            abre_pedidos_en_grupos_de_la_red([ipedido], grupos_para_abrir)

        grupos_para_cerrar = [row.grupo.id for row in grupos
              if ((not f.vars['activar_%d'%row.grupo.id]) and
                  (row.grupo.id in pedido_grupo)) ]
        if grupos_para_cerrar:
            # elimina proxy_pedido peticion, item, item_grupo para subredes y grupos
            elimina_proxy_pedido(ipedido, grupos_para_cerrar)
        session.flash = T('Se ha ofrecido el pedido solo para los grupos marcados.')
        redirect(URL(c='gestion_pedidos', f='gestion_pedidos.html', vars=dict(grupo=ired)))
    return dict(form=f)

@scope.set('admin-red')
#@requires_get_arguments(('pedido', db.pedido))
@auth.requires_membership(Permisos.grupo_del_pedido(request.vars.pedido))
def cerrar_discrepancia_red():
    '''DEPRECATED?

    No necesitamos esta página, pero podría ser interesante una página más sencilla,
    que muestre el estado de las discrepancias con cada grupo y ponga un link a arreglarlo.

    incidencias_desdered hace la función, si nadie opina en dirección contraria
    borraré ésta sin dejar rastro...
    '''
    raise DeprecationWarning()
    ipedido    = int(request.vars.pedido)
    pedido     = db.pedido(ipedido)
    productor  = db.pedido(ipedido).productor
    red        = productor.grupo
    assert red.red
    ired = int(red.id)
    response.title = T('Discrepancias con los Grupos en %s')%format_pedido(pedido)

	#todos los grupos con discrepancias
    grupos = db((db.proxy_pedido.pedido==ipedido) &
				(db.proxy_pedido.activo==True) &
				((db.proxy_pedido.pendiente_incidencias_unidades==True) |
				 (db.proxy_pedido.incidencias_declaradas==True) |
				 (db.proxy_pedido.pendiente_consolidacion==True) |
				 (db.proxy_pedido.discrepancia_asumida >0)) &
                (db.proxy_pedido.grupo==db.grupo.id) ) \
             .select(db.grupo.nombre, db.grupo.id, db.proxy_pedido.ALL, orderby=db.grupo.nombre)

    if not grupos:
        session.flash = T('No ha discrepancias con los grupos.')
        redirect(URL(c='gestion_pedidos', f='gestion_pedidos.html', vars=dict(grupo=ired)))

    lista_grupos = [row.grupo.id for row in grupos]
    peticion_total_grupo = db.item_grupo.precio_red.sum()
    peticion_total_red = db.item_red.precio_red.sum()

    rows = db((db.item_grupo.productoXpedido==db.productoXpedido.id) &
              (db.productoXpedido.pedido==ipedido) &
              (db.item_grupo.grupo.belongs(lista_grupos)) &
              (db.item_red.productoXpedido==db.item_grupo.productoXpedido) &
              (db.item_red.grupo==db.item_grupo.grupo)) \
           .select(peticion_total_grupo, peticion_total_red, db.item_grupo.grupo, groupby=db.item_grupo.grupo)
    totales_grupo = defaultdict(Decimal)
    totales_red = defaultdict(Decimal)
    for r in rows:
        totales_grupo[r[db.item_grupo.grupo]] = r[peticion_total_grupo]
        totales_red[r[db.item_grupo.grupo]] = r[peticion_total_red]

    discrepancias_grupo = defaultdict(Decimal)
    pendientes_consolidacion_grupo = defaultdict(Decimal)
    for r in grupos:
        igrupo = r[db.grupo.id]
        discrepancias_grupo[igrupo] = r[db.proxy_pedido.discrepancia_asumida]
        pendientes_consolidacion_grupo[igrupo] = r[db.proxy_pedido.pendiente_consolidacion]

    f = FORM(DIV(TABLE(
            THEAD(TR(TH(T('Grupo')),
                     TH(T('Total según Grupo')),
                     TH(T('Total según Red')),
                     TH(T('Discrepancia')),
                     TH(T('Pendiente de consolidación')))),
            *[TR(TH(row.grupo.nombre),
                 TD(totales_grupo[row.grupo.id]),
                 TD(totales_red[row.grupo.id]),
                 TD(A(SPAN(_class=(  'glyphicon glyphicon-folder-close icon-asume-red' if (discrepancias_grupo[row.grupo.id] == 2)
                                else 'glyphicon glyphicon-folder-close icon-asume-grupo' if (discrepancias_grupo[row.grupo.id] == 1)
                                else 'glyphicon glyphicon-folder-open icon-warning'
                                ),
                            _title=( T('Pincha para deshacer discrepancia asumida') if (discrepancias_grupo[row.grupo.id] == 2)
                                else T('Discrepancia asumida por el grupo') if (discrepancias_grupo[row.grupo.id] == 1)
                                else T('Pincha para asumir la discrepancia')
                                ),
                            #TODO: normalizar a discrepancias_grupo[row.grupo.id] != 1
                            _onclick=('js_cerrar_discrepancia_grupo(%d,%d);'%(ipedido,row.grupo.id)) if (discrepancias_grupo[row.grupo.id] != 1 or discrepancias_grupo[row.grupo.id]==None)
                                    else 'return;',
                            **{'_data-toggle':'tooltip', '_data-placement':'bottom'}
                    ),
                    _id = 'td_grupo_discrepancia_%d'%row.grupo.id)
                    if not pendientes_consolidacion_grupo[row.grupo.id] else ''
                    ),
                 TD(SPAN(_class='glyphicon glyphicon-bell',
                         _title=T('Este grupo tiene que declarar el peso de algunos productos'))
                    if pendientes_consolidacion_grupo[row.grupo.id] else '')
                ) for row in grupos]),
              _class="pedidos"),
            A(T('Gestión de pedidos'),
              _class='btn btn-primary',
              _href=URL(c='gestion_pedidos', f='gestion_pedidos.html',vars=dict(grupo=ired))
              ),
            SCRIPT('''
$(function(){ $('[data-toggle="tooltip"]').tooltip(); });

var js_cerrar_discrepancia_grupo = function(ipedido, igrupo){
var boton_cerrar_discrepancia = $('#td_grupo_discrepancia_'+igrupo).find('span');
var myClass = boton_cerrar_discrepancia.attr("class");
boton_cerrar_discrepancia.attr('disabled', true);
boton_cerrar_discrepancia.removeClass().addClass('glyphicon glyphicon-certificate icon-discrepancia-none');
$.getJSON('/gestion_pedidos/cerrar_discrepancia.json', {
    grupo:igrupo,
    pedido:ipedido,
    red_grupo:2
}, function(respuesta){
    if(respuesta.status=='ok'){
        if (respuesta.cerrada==0){
            web2py.flash('%(mensaje_discrepancia_deshecha)s');
        }else if(respuesta.cerrada==2){
            web2py.flash('%(mensaje_discrepancia_cerrada_red)s');
        }
    }else{
        web2py.flash(respuesta.mensaje);
    }
    if (respuesta.cerrada==0){
        boton_cerrar_discrepancia.removeClass().addClass('glyphicon glyphicon-folder-open icon-warning');
        boton_cerrar_discrepancia.tooltip('hide')
            .attr('title', 'Pincha para asumir la discrepancia')
            .tooltip('fixTitle').tooltip('show');

    }else if(respuesta.cerrada==2){
        boton_cerrar_discrepancia.removeClass().addClass('glyphicon glyphicon-folder-close icon-asume-red');
        boton_cerrar_discrepancia.tooltip('hide')
            .attr('title', 'Pincha para deshacer discrepancia asumida')
            .tooltip('fixTitle').tooltip('show');
    }else{
        boton_cerrar_discrepancia.removeClass().addClass(myClass);
    }
    boton_cerrar_discrepancia.attr('disabled', false);
});
};
'''%dict(mensaje_discrepancia_deshecha=T('Se ha deshecho la discrepancia asumida. El pedido queda pendiente en el grupo.'),
     mensaje_discrepancia_cerrada_red=T('Se ha cerrado la discrepancia. La red asume lo que dice el grupo, respetando lo indicado de cara a los proveedores')),
        _type='text/javascript'),
        _id='form_cerrar_discrepancia_red'
        )

    return dict(form=f)

#@requires_get_arguments(('pedido', db.pedido))
@auth.requires_membership(Permisos.grupo_del_pedido(request.vars.pedido))
def exportar_pedido_redes():
    '''muestra solo los pedidos en los que ha habido diferencias entre lo pedido y lo recibido,
    incluye ademas el desajuste en precio
    '''
    ipedido = int(request.vars.pedido)
    pedido  = db.pedido(ipedido)
    filename = 'pedido_%s.%s'%(
               pedido.productor.nombre,
               request.extension
        )
    if request.extension not in ('ods', 'csv'):
        redirect(URL(c=request.controller, f=request.function, extension='ods',
                    vars=request.get_vars))
    elif request.extension == 'csv':
        from exportador import csv_exportar_pedido_redes

        response.headers['Content-Type'] = 'text/csv'
        response.headers['Content-Disposition'] = 'attachment; filename='+filename
        return dict(csv=csv_exportar_pedido_redes(ipedido))
    else:
        from exportador import ods_exportar_pedido_redes
        from uuid import uuid4

        tmpfilename=os.path.join(request.folder,'private','temp_files','deleteme_' + str(uuid4()))
        ods_exportar_pedido_redes(ipedido, tmpfilename)
        return response.stream(tmpfilename,
                4000,
                attachment=True,
                filename=filename
                )

#@requires_get_arguments(('red', db.grupo), ('fecha_reparto', 'date'))
@auth.requires_membership('admins_%s'%request.vars.red)
def exportar_pedidos_red_por_grupos_fecha():
    from modelos.pedidos import pedidos_del_dia
    from exportador import ods_exportar_pedido_por_grupos
    from uuid import uuid4

    ired  = int(request.vars.red)
    nombre_red = db.grupo(ired).nombre
    fecha_reparto = request.vars.fecha_reparto

    (repartos, repartosc) = pedidos_del_dia(fecha_reparto, ired)
    pedidos = [r.id for r in chain(repartos, repartosc)]

    incidencias = request.vars.incidencias or '3'
    if incidencias=='3':
        prefilename = T('peticiones')
    elif incidencias=='1':
        prefilename = T('recibido')
    else:
        prefilename = T('suministrado')

    filename = '%s_%s_%s.ods'%(
               prefilename,
               K.nombre2slug(nombre_red),
               fecha_reparto
        )

    tmpfilename=os.path.join(request.folder,'private','temp_files','deleteme_' + str(uuid4()))
    ods_exportar_pedido_por_grupos(ired, pedidos, incidencias, tmpfilename)
    return response.stream(tmpfilename,
            4000,
            attachment=True,
            filename=filename
            )

#@requires_get_arguments(('red', db.grupo), ('fecha_reparto', 'date'))
@auth.requires_membership('admins_%s'%request.vars.red)
def exportar_pedidos_redes_fecha():
    from modelos.pedidos import pedidos_del_dia

    ired  = int(request.vars.red)

    #Los pedidos abiertos no tienen incidencias
    (repartos, repartosc) = pedidos_del_dia(request.vars.fecha_reparto, ired, solo_cerrados=True)
    pedidos = [r.id for r in chain(repartos, repartosc)]
    filename = 'pedido_%s.%s'%(
               request.vars.fecha_reparto,
               request.extension
        )
    if request.extension not in ('ods', 'csv'):
        redirect(URL(c=request.controller, f=request.function, extension='csv',
                    vars=request.get_vars))
    elif request.extension == 'csv':
        from exportador import csv_exportar_pedidos_redes

        response.headers['Content-Type'] = 'text/csv'
        response.headers['Content-Disposition'] = 'attachment; filename='+filename

        return dict(csv=csv_exportar_pedidos_redes(pedidos))

    else:
        from exportador import ods_exportar_pedidos_redes
        from uuid import uuid4

        tmpfilename=os.path.join(request.folder,'private','temp_files','deleteme_' + str(uuid4()))
        ods_exportar_pedidos_redes(pedidos, tmpfilename)
        return response.stream(tmpfilename,
                4000,
                attachment=True,
                filename=filename
                )
