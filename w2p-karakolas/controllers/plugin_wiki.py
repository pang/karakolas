# -*- coding: utf-8 -*-
# This file was developed by Massimo Di Pierro
# who released it under BSD, MIT and GPL2 licenses
# Then it was modified by Pablo Angulo to adapt it to karakolas
# This file is now part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

##########################################################
# code to handle wiki pages
##########################################################

@scope.set('admin-web')
#@requires_get_arguments()
@auth.requires_membership('webadmins')
def index():
    w = db.plugin_wiki_page
    pages = db(w.id>0).select(orderby=w.slug)
    form=SQLFORM.factory(Field('slug',requires=w.slug.requires),
                         Field('from_template',requires=IS_EMPTY_OR(IS_IN_DB(db,w.slug))))
    if form.accepts(request.vars):
        redirect(URL(r=request,f='page.html',
                     args=form.vars.slug,
                     vars=dict(template=request.vars.from_template or ''))
        )

    return dict(pages=pages,form=form)

@scope.set('logueado')
#@requires_get_arguments()
def page():
    """
    shows a page
    """
    slug = request.args(0) or 'index'
    response.title = T('Vista de página wiki: %s')%slug

    w = db.plugin_wiki_page
    page = w(slug=slug)
    if request.extension in ('html', 'load'):
        return dict(page=page,slug=slug)
    elif request.extension == 'json':
        contenido = (plugin_wiki.render(page.body) if page else
            T('No hay contenido para esta página. Puedes ayudar a tus compañeras escribiendo tú este texto.')
        )
        return dict(titulo=slug, contenido=contenido)
    return MARKMIN(page.body)

def puede_editar(expose_this=False):
    slug = request.args(0) or 'index'
    if slug.startswith('meta') or slug=='index':
        return auth.has_membership('webadmins')
    else:
        return plugin_wiki_editor

def scope_al_editar(expose_this=False):
    slug = request.args(0) or 'index'
    if slug.startswith('meta') or slug=='index':
        return 'admin-web'
    else:
        return 'logueado'

#@requires_get_arguments()
@auth.requires(puede_editar)
@scope.set('logueado')
def page_archive():
    """
    shows and old version of a page
    """
    id = request.args(0)
    h = db.plugin_wiki_page_archive
    page = h(id)
    response.title = T('Vista de una versión antigua de: %s')%page.title
    if not page or (not plugin_wiki_editor and (not page.is_public or not page.is_active)):
        raise HTTP(404)
    elif page and page.role and not auth.has_membership(page.role):
        raise HTTP(404)
    if request.extension not in ['html', 'load']: return page.body
    return dict(page=page.as_dict())

@scope.set(scope_al_editar)
#@requires_get_arguments()
@auth.requires(puede_editar)
def page_edit():
    """
    edit a page
    """
    slug = request.args(0) or 'index'
    response.title = T('Editando página %s')%slug

    w = db.plugin_wiki_page
    w.role.writable = w.role.readable = False
    page = w(slug=slug)
    if not page:
        page = w.insert(slug=slug,
                        title=slug.replace('-',' ').capitalize(),
                        body=request.vars.template and w(slug=request.vars.template).body or '')
    form = SQLFORM(db.plugin_wiki_page, page)
    if form.process(onsuccess=auth.archive).accepted:
        session.flash = T('La página ha sido actualizada.')
        redirect(URL(r=request, f='page.html', args=slug))

    response.files.append(URL('static', 'plugin_wiki/markitup/jquery.markitup.pack.js'))
    response.files.append(URL('static', 'plugin_wiki/markitup/sets/markmin/set.js'))
    response.files.append(URL('static', 'plugin_wiki/markitup/skins/markitup/style.css'))
    response.files.append(URL('static', 'plugin_wiki/markitup/sets/markmin/style.css'))

    return dict(form=form,page=page)

@scope.set('logueado')
#@requires_get_arguments()
def page_history():
    """
    show page changelog
    """
    slug = request.args(0) or 'index'
    response.title = T('Historial de la página: %s')%slug
    w = db.plugin_wiki_page
    h = db.plugin_wiki_page_archive
    page = w(slug=slug)
    history = db(h.current_record==page.id).select(orderby=~h.modified_on)
    return dict(page=page.as_dict(), history=history)

##########################################################
# ajax callbacks
##########################################################
@auth.requires(plugin_wiki_editor)
def attachments():
    """
    allows to edit page attachments
    """
    a=db.plugin_wiki_attachment
    a.tablename.default=tablename=request.args(0)
    a.record_id.default=record_id=request.args(1)
    #if request.args(2): a.filename.writable=False
    if request.args(2):
        form = SQLFORM(a, request.args(2))
    else:
        form = SQLFORM(a)
    if form.process().accepted:
        response.flash = T('Los cambios se han recibido correctamente')
        
    if request.vars.list_all:
        query = a.id>0
    else:
        query = (a.tablename==tablename)&(a.record_id==record_id)
    rows=db(query).select(orderby=a.name)
    return dict(form=form,rows=rows)

def attachment():
    """
    displays an attachment
    """
    short=request.args(0)
    a=db.plugin_wiki_attachment
    record=a(short.split('.')[0])
    if not record: raise HTTP(400)
    request.args[0]=record.filename
    return response.download(request,db)

