# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

# Datos estadisticos de solo lectura

from estadisticas import Estadistica
from rsheet import Tabular
from bloques_vista_estadistica import SelectorRangoFechas, SelectorDetalle
from kutils import pprint_p

def representa_precio(v):
    return pprint_p(v) if v else ''

@scope.set('miembro')
#@requires_get_arguments(('grupo', db.grupo))
@auth.requires_membership('miembros_%s'%request.vars.grupo)
def index():
    igrupo = int(request.vars.grupo)
    nombre_grupo  =db.grupo(igrupo).nombre
    response.title = T('Tus estadísticas en %s')%nombre_grupo

    ipersona  = auth.user.id

    #Limita las fechas de las estadisticas
    form_rango_fechas = SelectorRangoFechas()
    form_rango_fechas.redirect_if_hit()
    fecha_ini, fecha_fin = form_rango_fechas.rango_fechas
    representa_fecha = form_rango_fechas.representa_fecha

    #FORM para elegir detalle del productor
    productores = db(db.productor.grupo==igrupo)\
                   .select(db.productor.nombre,
                           db.productor.id)
    productores_c = db((db.productor.grupo==db.grupoXred.red) &
                       (db.grupoXred.grupo==igrupo) )\
                    .select(db.productor.nombre,
                            db.productor.id)
    todos_productores = [
        (r.id, r.nombre) for r in chain(productores, productores_c)
    ]
    form_stat_productor = SelectorDetalle(
        todos_productores, 'grafica_personal_productor', 'productor', T('Elige un productor')
    )
    form_stat_productor.redirect_if_hit()

    #Grafica
    data_field = db.peticion.precio_total.sum()
    query = ((db.peticion.productoXpedido==db.productoXpedido.id) &
             (db.peticion.grupo==igrupo) &
             (db.peticion.cantidad>0) &
             (db.productoXpedido.pedido==db.pedido.id) &
             (db.pedido.fecha_reparto>=fecha_ini) &
             (db.pedido.fecha_reparto<=fecha_fin) &
             (db.peticion.persona==ipersona) &
             (db.pedido.productor==db.productor.id))

    data = Tabular(query,
                db.productor.nombre, db.pedido.fecha_reparto, data_field,
                col_represent=representa_fecha
        )
    stat = Estadistica(
        data,
        rosheet_options=dict(_id='tabla_productores', col_totals=True)
    )

    if request.extension=='ods':
        return stat.ods(filename=
            'stat_%s.ods'%K.ascii_safe(nombre_grupo, filename_safe=True)
        )

    return dict(form_stat_productor=form_stat_productor,
                stat=stat,
                form_rango_fechas=form_rango_fechas,
                nombre_grupo=nombre_grupo)

@scope.set('miembro')
#@requires_get_arguments(('grupo', db.grupo))
@auth.requires_membership('admins_%s'%request.vars.grupo)
def stat_grupo():
    igrupo = int(request.vars.grupo)
    grupo  =db.grupo(igrupo)
    response.title = T('Estadísticas del grupo %s')%grupo.nombre
    #Limita las fechas de las estadisticas
    form_rango_fechas = SelectorRangoFechas()
    form_rango_fechas.redirect_if_hit()
    fecha_ini, fecha_fin = form_rango_fechas.rango_fechas
    representa_fecha = form_rango_fechas.representa_fecha

    #FORM para elegir detalle del productor
    productores = db(db.productor.grupo==igrupo)\
                   .select(db.productor.nombre,
                           db.productor.id, orderby=db.productor.nombre)
    productores_c = db((db.productor.grupo==db.grupoXred.red) &
                       (db.grupoXred.grupo==igrupo) )\
                    .select(db.productor.nombre,
                            db.productor.id, orderby=db.grupoXred.red|db.productor.nombre)
    todos_productores = [
        (r.id, r.nombre) for r in chain(productores, productores_c)
    ]
    form_stat_productor = SelectorDetalle(
        todos_productores, 'grafica_grupo_productor', 'productor', T('Elige un productor')
    )
    form_stat_productor.redirect_if_hit()

    #Grafica
    query_pedidos_en_rango = (
             (db.pedido.productor==db.productor.id) &
             (db.pedido.abierto==False) &
             (db.pedido.fecha_reparto>=fecha_ini) &
             (db.pedido.fecha_reparto<=fecha_fin) &
             (db.productoXpedido.pedido==db.pedido.id) &
             (db.item_grupo.productoXpedido==db.productoXpedido.id) &
             (db.item_grupo.grupo==igrupo) &
             (db.item_grupo.precio_recibido>0)
             )
    query_propios = (query_pedidos_en_rango &
                     (db.productor.grupo==igrupo))
    query_coordinados = (query_pedidos_en_rango &
                         (db.proxy_pedido.grupo==igrupo) &
                         (db.proxy_pedido.pedido==db.pedido.id))
    query_item_list = [query_propios, query_coordinados]
    data_field = db.item_grupo.precio_recibido.sum()

    data = Tabular(
        query_item_list, db.productor.nombre, db.pedido.fecha_reparto, data_field,
        col_represent=representa_fecha, data_represent=representa_precio
    )
    stat = Estadistica(
        data,
        rosheet_options=dict(
            _id='tabla_productores',
            col_totals=True,
            row_totals=True)
    )

    if request.extension=='ods':
        return stat.ods(filename=
            'stat_%s.ods'%K.ascii_safe(grupo.nombre, filename_safe=True)
        )

    return dict(form_stat_productor=form_stat_productor,
                form_rango_fechas=form_rango_fechas,
                stat=stat,
                grupo=grupo)

@scope.set('miembro')
#@requires_get_arguments(('productor', db.productor))
@auth.requires(Permisos.tiene_acceso_al_grupo(request.vars.productor))
def grafica_personal_productor():
    '''Muestra una tabla productos x fecha_pedido para un productor fijo,
    con la cantidad que pide una persona de cada producto en cada fecha
    '''
    iproductor = int(request.vars.productor)
    productor  = db.productor[iproductor]
    ipersona  = auth.user.id

    response.title = T('Tus estadísticas personales de %s')%productor.nombre

    #FORM para limitar las fechas de las estadisticas
    form_rango_fechas = SelectorRangoFechas()
    form_rango_fechas.redirect_if_hit()
    fecha_ini, fecha_fin = form_rango_fechas.rango_fechas
    representa_fecha = form_rango_fechas.representa_fecha

    query = ((db.peticion.productoXpedido==db.productoXpedido.id) &
             (db.peticion.persona==ipersona) &
             (db.peticion.cantidad>0) &
             (db.productoXpedido.pedido==db.pedido.id) &
             (db.pedido.fecha_reparto>=fecha_ini) &
             (db.pedido.fecha_reparto<=fecha_fin) &
             (db.pedido.productor==iproductor)
            )

    #Grafica
    data = Tabular(
        query,
        db.productoXpedido.nombre, db.pedido.fecha_reparto, db.peticion.cantidad.sum(),
        col_represent=representa_fecha
    )
    stats = Estadistica(
        data,
        rosheet_options=dict(_id='tabla_productores', col_totals=True)
    )
    if request.extension=='ods':
        return stats.ods(filename='stat_%s.ods'%
            K.ascii_safe(productor.nombre, filename_safe=True))

    if productor.grupo.red:
        grupos = [(r.id, r.nombre)
                for r in
                 db((db.personaXgrupo.persona==ipersona) &
                   (db.personaXgrupo.grupo==db.grupoXred.grupo) &
                   (db.grupoXred.red==productor.grupo) &
                   (db.grupo.id==db.grupoXred.grupo)) \
                 .select(db.grupo.id, db.grupo.nombre) ]
    else:
        g = productor.grupo
        grupos = [(g.id, g.nombre)]
    return dict(stats=stats,
                form_rango_fechas=form_rango_fechas,
                nombre_productor=productor.nombre,
                grupos=grupos)

@scope.set('miembro')
#@requires_get_arguments(('grupo', db.grupo),
                    #     ('productor', db.productor),
                    #     _consistency= ( ((db.grupo.id==db.grupoXred.grupo) &
                    #                      (db.productor.grupo==db.grupoXred.red)),
                    #                      (db.productor.grupo==db.grupo.id) )
                    #    )
@auth.requires_membership('admins_%s'%request.vars.grupo)
def grafica_grupo_productor():
    '''Muestra tabla y grafica de productos x fecha_pedido para un productor fijo,
    con la cantidad total que se pide de cada producto en cada fecha
    '''

    iproductor = int(request.vars.productor)
    productor = db.productor[iproductor]
    nombre_productor = productor.nombre
    igrupo = int(request.vars.grupo)
    nombre_grupo = db.grupo[igrupo].nombre

    response.title = T('Estadísticas de %s con %s')%(nombre_grupo, nombre_productor)

    #FORM para limitar las fechas de las estadisticas
    form_rango_fechas = SelectorRangoFechas()
    form_rango_fechas.redirect_if_hit()
    fecha_ini, fecha_fin = form_rango_fechas.rango_fechas
    representa_fecha = form_rango_fechas.representa_fecha
    #Data
    if productor.grupo==igrupo: # pedido propio
        query = ((db.item_grupo.productoXpedido==db.productoXpedido.id) &
                 (db.item_grupo.cantidad>0) &
                 (db.productoXpedido.pedido==db.pedido.id) &
                 (db.pedido.productor==iproductor) &
                 (db.pedido.fecha_reparto>=fecha_ini) &
                 (db.pedido.fecha_reparto<=fecha_fin) &
                 (db.pedido.abierto==False) &
                 (db.item_grupo.grupo==igrupo)
                )

        data = Tabular(
            query,
            db.productoXpedido.nombre, db.pedido.fecha_reparto, db.item_grupo.cantidad.sum(),
            col_represent=representa_fecha
        )
    else: # pedido coordinado
        query = (
             (db.pedido.productor==iproductor) &
             (db.pedido.abierto==False) &
             (db.pedido.fecha_reparto>=fecha_ini) &
             (db.pedido.fecha_reparto<=fecha_fin) &
             (db.productoXpedido.pedido==db.pedido.id) &
             (db.item_grupo.productoXpedido==db.productoXpedido.id) &
             (db.item_grupo.grupo==igrupo) &
             (db.item_grupo.cantidad>0) &
             (db.proxy_pedido.grupo==igrupo) &
             (db.proxy_pedido.pedido==db.pedido.id))
        data_field = db.item_grupo.cantidad.sum()

        data = Tabular(
            query, db.productoXpedido.nombre, db.pedido.fecha_reparto, data_field,
            col_represent=representa_fecha
        )

    #Gráficas
    stats = Estadistica(
        data,
        rosheet_options=dict(_id='tabla_productores', col_totals=True)
    )
    if request.extension=='ods':
        return stats.ods(filename='stat_%s.ods'%
            K.ascii_safe(nombre_productor, filename_safe=True))

    return dict(stats=stats,
                form_rango_fechas=form_rango_fechas,
                nombre_productor=nombre_productor,
                nombre_grupo=nombre_grupo,
                grupo=igrupo)

@scope.set('admin-red')
#@requires_get_arguments(('red', db.grupo))
@auth.requires_membership('admins_%s'%request.vars.red)
def stat_red():
    ired = int(request.vars.red)
    red  = db.grupo(ired)
    nombre_red = red.nombre
    response.title = T('Estadísticas de la red %s')%red.nombre

    #FORM para limitar las fechas de las estadisticas
    form_rango_fechas = SelectorRangoFechas()
    form_rango_fechas.redirect_if_hit()
    fecha_ini, fecha_fin = form_rango_fechas.rango_fechas
    representa_fecha = form_rango_fechas.representa_fecha

    #FORM para elegir detalle del productor
    productores = [
        (r.id, r.nombre) for r in
        db(db.productor.grupo==ired)\
        .select(db.productor.nombre, db.productor.id, orderby=db.productor.nombre)
    ]
    form_stat_productor = SelectorDetalle(
        productores, 'grafica_red_productor', 'productor', T('Elige un productor')
    )
    form_stat_productor.redirect_if_hit()

    #FORM para elegir detalle del grupo
    grupos = [
        (r.id, r.nombre) for r in
        db((db.grupoXred.red==ired) & (db.grupoXred.grupo==db.grupo.id))\
        .select(db.grupo.nombre, db.grupo.id, orderby=db.grupo.nombre)
    ]
    form_stat_grupo = SelectorDetalle(
        grupos, 'grafica_red_grupo', 'grupo', T('Elige un grupo')
    )
    form_stat_grupo.redirect_if_hit()

    #FORM para elegir detalle de la superred
    redes = [
        (r.id, r.nombre) for r in
        db((db.grupoXred.grupo==ired) & (db.grupoXred.red==db.grupo.id) & (db.grupoXred.activo==True))\
        .select(db.grupo.nombre, db.grupo.id, orderby=db.grupo.nombre)
    ]
    form_stat_redes = SelectorDetalle(
        redes, 'grafica_red_superred', 'superred', T('Elige una red')
    )
    form_stat_redes.redirect_if_hit()

    #Datos comunes a las gráficas:
    #TODO: duda: ¿quitar pedidos no consolidados?
    proxy_pedido_down = db.proxy_pedido.with_alias('proxy_pedido_down')
    query_pedidos_en_rango = (
             (db.pedido.productor==db.productor.id) &
             (db.pedido.abierto==False) &
             (db.pedido.fecha_reparto>=fecha_ini) &
             (db.pedido.fecha_reparto<=fecha_fin) &
             (db.productoXpedido.pedido==db.pedido.id) &
             (db.item_grupo.productoXpedido==db.productoXpedido.id) &
             (db.item_grupo.cantidad_red>0) &
             # 11-12-21 si no, los productos a coste cero (no es lo habitual, pero se usa)
             # provocarán una división por cero
             (db.item_grupo.precio_red>0) &
             (db.item_grupo.grupo==proxy_pedido_down.grupo) &
             (proxy_pedido_down.pedido==db.pedido.id) &
             (proxy_pedido_down.activo==True) &
             # solo proxys de 1 nivel inferior
             # Alternativa: sacar la lista de grupos del nivel inferior en otra consulta
             # y usar grupo.belongs(lista_grupos)
             (proxy_pedido_down.grupo==db.grupo.id) &
             (db.grupoXred.red==ired) &
             (db.grupoXred.activo==True) &
             (db.grupoXred.grupo==db.grupo.id)
    )
    query_propios = (query_pedidos_en_rango &
                     (db.productor.grupo==ired))
    superred = db.grupo.with_alias('superred')
    query_coordinados = (query_pedidos_en_rango &
                        # Este proxy_pedido conecta con la superred que sirve el pedido a esta red
                         (db.proxy_pedido.grupo==ired) &
                         (db.proxy_pedido.pedido==db.pedido.id) &
                         (db.proxy_pedido.via==superred.id))
    query_list = [query_propios, query_coordinados]
    data_field = db.item_grupo.precio_red.sum()

    if request.vars.download_stat == 'stat_completo':
        #Si vamos a descargar todas las estadisticas como paginas de una misma
        #hoja de calculo, es conveniente que las fechas de pedido sean
        #exactamente las mismas
        #Usamos solo las fechas con algun item_red > 0
        query = (query_pedidos_en_rango &
         (db.campo_extra_pedido.pedido==db.pedido.id) &
         (db.item_grupo.productoXpedido==db.productoXpedido.id) &
         (db.item_grupo.cantidad_red>0) &
         (db.productoXpedido.pedido==db.pedido.id)
        )
        fechas = [
            row.fecha_reparto
            for row in db(query).select(
                        db.pedido.fecha_reparto,
                        groupby=db.pedido.fecha_reparto,
                        orderby=db.pedido.fecha_reparto)
        ]
    else:
        fechas = None

    #Total productor x fecha
    data_productor_fecha = Tabular(
        query_propios, db.productor.nombre, db.pedido.fecha_reparto, data_field,
        col_represent=representa_fecha, data_represent=representa_precio,
        col_ids=fechas
    )
    stat_productor_fecha = Estadistica(
        data_productor_fecha,
        T('4.- Ingresos por Productores'),
        rosheet_options=dict(_id='tabla_productores', col_average_non_null=True),
        ods_options=dict(download_stat='stat_productor_fecha')
    )

    #Total grupo x fecha
    data_grupo_fecha = Tabular(
        query_list, db.grupo.nombre, db.pedido.fecha_reparto, data_field,
        col_represent=representa_fecha, data_represent=representa_precio,
        col_ids=fechas
    )
    stat_grupo_fecha = Estadistica(
        data_grupo_fecha,
        T('2.- Grupos x Fecha'),
        rosheet_options=dict(_id='tabla_grupo', col_average_non_null=True),
        ods_options=dict(download_stat='stat_grupo_fecha')
    )

    #Total grupo x productor
    data_grupo_productores = Tabular(
        query_propios, db.productor.nombre, db.grupo.nombre, data_field,
        data_represent=representa_precio
    )
    stat_grupo_productor  = Estadistica(
        data_grupo_productores,
        T('1.- Grupos x Productores'),
        rosheet_options=dict(
            _id='tabla_grupos_productores',
            col_totals=True, row_totals=True
            ),
        ods_options=dict(download_stat='stat_grupo_productor')
    )

    #Coste extra de los pedidos
    query_coste_extra = (
             (db.pedido.productor==db.productor.id) &
             (db.pedido.abierto==False) &
             (db.pedido.fecha_reparto>=fecha_ini) &
             (db.pedido.fecha_reparto<=fecha_fin) &
             (proxy_pedido_down.pedido==db.pedido.id) &
             (proxy_pedido_down.activo==True) &
             # solo proxys de 1 nivel inferior
             # Alternativa: sacar la lista de grupos del nivel inferior en otra consulta
             # y usar grupo.belongs(lista_grupos)
             (proxy_pedido_down.grupo==db.grupo.id) &
             (proxy_pedido_down.coste_red > 0) &
             (db.grupoXred.red==ired) &
             (db.grupoXred.activo==True) &
             (db.grupoXred.grupo==db.grupo.id)
    )
    extra_field = proxy_pedido_down.coste_red.sum()
    query_extra_list = [
        query_coste_extra & (db.productor.grupo==ired),
        query_coste_extra &
            (db.proxy_pedido.grupo==ired) &
            (db.proxy_pedido.pedido==db.pedido.id) &
            (db.proxy_pedido.via==superred.id)
    ]
    data_extra = Tabular(
        query_extra_list,
        db.grupo.nombre, db.pedido.fecha_reparto, extra_field,
        col_represent=representa_fecha, data_represent=representa_precio,
        col_ids=fechas
    )
    stat_extra  = Estadistica(
        data_extra,
        T('3.- Coste extra de los pedidos'),
        rosheet_options=dict(_id='tabla_coste_extra', col_totals=True),
        ods_options=dict(download_stat='stat_extra')
    )

    #Total productor x fecha
    data_precio_productor = Tabular(
        query_propios, db.productor.nombre, db.pedido.fecha_reparto, data_field,
        col_represent=representa_fecha, data_represent=representa_precio,
        col_ids=fechas
    )
    stat_precio_productor = Estadistica(
        data_precio_productor,
        T('5.- Pagos a Productores'),
        rosheet_options=dict(_id='tabla__precio_productor', col_average_non_null=True),
        ods_options=dict(download_stat='stat_precio_productor')
    )

    #Total superredes x fecha
    data_precio_redes = Tabular(
        query_coordinados, superred.nombre, db.pedido.fecha_reparto, data_field,
        col_represent=representa_fecha, data_represent=representa_precio,
        col_ids=fechas
    )
    stat_precio_redes = Estadistica(
        data_precio_redes,
        T('6.- Pagos a Redes'),
        rosheet_options=dict(_id='tabla__precio_redes', col_average_non_null=True),
        ods_options=dict(download_stat='stat_precio_redes')
    )

    #Total grupo x superred
    data_grupo_redes = Tabular(
        query_coordinados, superred.nombre, db.grupo.nombre, data_field,
        data_represent=representa_precio
    )
    stat_grupo_redes  = Estadistica(
        data_grupo_redes,
        T('7.- Grupos x Redes'),
        rosheet_options=dict(
            _id='tabla_grupos_redes',
            col_totals=True, row_totals=True
            ),
        ods_options=dict(download_stat='stat_grupo_redes')
    )

    #Total superred x fecha
    data_red_fecha = Tabular(
        query_coordinados, superred.nombre, db.pedido.fecha_reparto, data_field,
        col_represent=representa_fecha, data_represent=representa_precio,
        col_ids=fechas
    )
    stat_red_fecha = Estadistica(
        data_red_fecha,
        T('8.- Ingresos por Redes'),
        rosheet_options=dict(_id='tabla_redes', col_average_non_null=True),
        ods_options=dict(download_stat='stat_red_fecha')
    )

    if request.extension=='ods':
        stats = {'stat_productor_fecha':stat_productor_fecha,
                 'stat_grupo_fecha':stat_grupo_fecha,
                 'stat_grupo_productor':stat_grupo_productor,
                 'stat_extra':stat_extra,
                 'stat_precio_productor':stat_precio_productor,
                 'stat_precio_redes':stat_precio_redes,
                 'stat_grupo_redes':stat_grupo_redes,
                 'stat_red_fecha':stat_red_fecha,
        }
        if request.vars.download_stat == 'stat_completo':
            from estadisticas import multistat_ods
            query = (query_pedidos_en_rango &
                     (db.campo_extra_pedido.pedido==db.pedido.id)
                    )
            nombres_variables_extra = [
                row.nombre
                for row in db(query).select(
                    db.campo_extra_pedido.nombre,
                    groupby=db.campo_extra_pedido.nombre)]
            data_field = db.campo_extra_pedido.valor
            # Incluye productores propio y coordinados
            productores = [row.nombre
                for row in db(query_pedidos_en_rango).select(
                    db.productor.nombre,
                    groupby=db.productor.nombre
                )
            ]
            stats_campos_extra = dict((nombre, Estadistica(
                Tabular(
                    query & (db.campo_extra_pedido.nombre==nombre),
                    db.productor.nombre, db.pedido.fecha_reparto, data_field,
                    row_ids=productores,
                    col_ids=fechas,
                    col_represent=representa_fecha, data_represent=representa_precio
                ),
                nombre,
                ods_options=dict(download_stat=nombre, no_totales=True)
                ))
                for nombre in nombres_variables_extra
            )
            stats.update(stats_campos_extra)

            return multistat_ods(list(stats.values()),
                filename='stat_%s.ods'%(
                K.ascii_safe(red.nombre, filename_safe=True)
                ))
        else:
            stat = stats[request.vars.download_stat]
            return stat.ods(filename='%s_%s.ods'%(
                request.vars.download_stat,
                K.ascii_safe(red.nombre, filename_safe=True)
                ))

    if form_stat_grupo.process(formname='detalle_grupo').accepted:
        redirect(URL(c='estadisticas', f='grafica_red_grupo.html',
            vars=dict(grupo=form_stat_grupo.vars.grupo, red=ired,
                      fecha_ini=fecha_ini, fecha_fin=fecha_fin)))

    return dict(form_rango_fechas=form_rango_fechas,
                fecha_ini=K.format_fecha(fecha_ini),
                fecha_fin=K.format_fecha(fecha_fin),
                stat_productor_fecha=stat_productor_fecha,
                stat_grupo_fecha=stat_grupo_fecha,
                stat_grupo_productor=stat_grupo_productor,
                stat_precio_productor=stat_precio_productor,
                stat_extra=stat_extra,
                stat_precio_redes=stat_precio_redes,
                stat_grupo_redes=stat_grupo_redes,
                stat_red_fecha=stat_red_fecha,
                form_stat_grupo=form_stat_grupo,
                form_stat_productor=form_stat_productor,
                form_stat_redes=form_stat_redes,
                red=ired,
                nombre_red=nombre_red)

@scope.set('admin-red')
#@requires_get_arguments(('grupo', db.grupo), ('red', db.grupo))
@auth.requires_membership('admins_%s'%request.vars.red)
def grafica_red_grupo():

    igrupo = int(request.vars.grupo)
    nombre_grupo  = db.grupo(igrupo).nombre
    ired = int(request.vars.red)
    nombre_red  = db.grupo(ired).nombre
    if not db.grupoXred(grupo=igrupo, red=ired, activo=True):
        session.flash = T('No tienes permiso. Por favor repasa la url, o accede a la web con otro usuario.')
        redirect(URL(c='estadisticas', f='stat_red.html', vars=dict(red=ired)))
    response.title = T('Estadísticas del grupo %s dentro de la red %s')%(
        nombre_grupo, nombre_red
        )
    #Limita las fechas de las estadisticas
    form_rango_fechas = SelectorRangoFechas()
    form_rango_fechas.redirect_if_hit()
    fecha_ini, fecha_fin = form_rango_fechas.rango_fechas
    representa_fecha = form_rango_fechas.representa_fecha

    #TODO: separa los pedidos con discrepancia asumida por el grupo o la red

    #Grafica
    query = (
             (db.pedido.productor==db.productor.id) &
             (db.pedido.abierto==False) &
             (db.pedido.fecha_reparto>=fecha_ini) &
             (db.pedido.fecha_reparto<=fecha_fin) &
             (db.productoXpedido.pedido==db.pedido.id) &
             (db.pedido.productor==db.productor.id) &
             (db.item_grupo.productoXpedido==db.productoXpedido.id) &
             (db.item_grupo.grupo==igrupo) &
             (db.item_grupo.cantidad>0) &
             (db.item_grupo.precio_red>0) &
             (db.item_grupo.grupo==db.proxy_pedido.grupo) &
             (db.proxy_pedido.pedido==db.pedido.id) &
             (db.proxy_pedido.via==ired)
    )
    data_field = db.item_grupo.precio_red.sum()
    data = Tabular(
        query, db.productor.nombre, db.pedido.fecha_reparto, data_field,
        col_represent=representa_fecha, data_represent=representa_precio
    )
    stats = Estadistica(
        data,
        rosheet_options=dict(
            _id='tabla_productores',
            col_totals=True,
            row_totals=True)
    )

    if request.extension=='ods':
        return stats.ods(filename='stat_%s_en_%s.ods'%(
            K.ascii_safe(nombre_grupo, filename_safe=True),
            K.ascii_safe(nombre_red, filename_safe=True)))

    return dict(stats = stats,
                form_rango_fechas=form_rango_fechas,
                nombre_grupo=nombre_grupo,
                nombre_red=nombre_red,
                red=ired)


@scope.set('admin-red')
#@requires_get_arguments(('superred', db.grupo), ('red', db.grupo))
@auth.requires_membership('admins_%s'%request.vars.red)
def grafica_red_superred():

    isuperred = int(request.vars.superred)
    nombre_superred  = db.grupo(isuperred).nombre
    ired = int(request.vars.red)
    nombre_red  = db.grupo(ired).nombre
    response.title = T('Estadísticas de los productores de %s en %s')%(nombre_superred, nombre_red)

    #Limita las fechas de las estadisticas
    form_rango_fechas = SelectorRangoFechas()
    form_rango_fechas.redirect_if_hit()
    fecha_ini, fecha_fin = form_rango_fechas.rango_fechas
    representa_fecha = form_rango_fechas.representa_fecha

    #Grafica
    #Separa los pedidos con discrepancia asumida con el grupo o por la red
    proxy_pedido_up = db.proxy_pedido.with_alias('proxy_pedido_up')
    query = (
             (db.pedido.abierto==False) &
             (db.pedido.fecha_reparto>=fecha_ini) &
             (db.pedido.fecha_reparto<=fecha_fin) &
             (proxy_pedido_up.pedido==db.pedido.id) &
             (proxy_pedido_up.grupo==ired) &
             (proxy_pedido_up.via==isuperred) &
             (db.productoXpedido.pedido==db.pedido.id) &
             (db.item_grupo.productoXpedido==db.productoXpedido.id) &
             (db.item_grupo.grupo==db.proxy_pedido.grupo) &
             (db.proxy_pedido.pedido==db.pedido.id) &
             (db.proxy_pedido.via==ired)
    )

    query_grupos = (query &
             (db.item_grupo.precio_red>0) &
             (db.proxy_pedido.grupo==db.grupo.id)
    )
    field_precio_red = db.item_grupo.precio_red.sum()
    data_grupo_fecha = Tabular(
        query_grupos, db.grupo.nombre, db.pedido.fecha_reparto, field_precio_red,
        col_represent=representa_fecha, data_represent=representa_precio
    )
    stat_grupo_fecha = Estadistica(
        data_grupo_fecha,
        rosheet_options=dict(
            _id='tabla_grupos',
            col_totals=True,
            row_totals=True),
        ods_options=dict(download_stat='stat_grupo_fecha')
    )

    query_cantidad = (query &
             (db.item_grupo.cantidad>0)
    )
    field_cantidad = db.item_grupo.cantidad_red.sum()
    data_producto_fecha = Tabular(
        query, db.productoXpedido.nombre, db.pedido.fecha_reparto, field_cantidad,
        col_represent=representa_fecha
    )
    stat_producto_fecha = Estadistica(
        data_producto_fecha,
        rosheet_options=dict(
            _id='tabla_productos',
            col_totals=True,
            row_totals=True),
        ods_options=dict(download_stat='stat_producto_fecha')
    )

    if request.extension=='ods':
        stats = {'stat_grupo_fecha':stat_grupo_fecha,
                 'stat_producto_fecha':stat_producto_fecha,
        }
        stat = stats[request.vars.download_stat]
        return stat.ods(filename='%s_%s_en_%s.ods'%(
            request.vars.download_stat,
            K.ascii_safe(nombre_superred, filename_safe=True),
            K.ascii_safe(nombre_red, filename_safe=True)
            ))

    return dict(stat_grupo_fecha = stat_grupo_fecha,
                stat_producto_fecha = stat_producto_fecha,
                form_rango_fechas=form_rango_fechas,
                nombre_superred=nombre_superred,
                nombre_red=nombre_red,
                red=ired)

@scope.set('admin-red')
#@requires_get_arguments(('productor', db.productor), ('red', db.grupo))
@auth.requires_membership('admins_%s'%request.vars.red)
def grafica_red_productor():

    iproductor = int(request.vars.productor)
    nombre_productor  = db.productor(iproductor).nombre
    ired = int(request.vars.red)
    nombre_red  = db.grupo(ired).nombre
    response.title = T('Estadísticas del productor %s')%nombre_productor

    #Limita las fechas de las estadisticas
    form_rango_fechas = SelectorRangoFechas()
    form_rango_fechas.redirect_if_hit()
    fecha_ini, fecha_fin = form_rango_fechas.rango_fechas
    representa_fecha = form_rango_fechas.representa_fecha

    #Grafica
    #Separa los pedidos con discrepancia asumida con el grupo o por la red
    query = (
             (db.pedido.productor==iproductor) &
             (db.pedido.abierto==False) &
             (db.pedido.fecha_reparto>=fecha_ini) &
             (db.pedido.fecha_reparto<=fecha_fin) &
             (db.productoXpedido.pedido==db.pedido.id) &
             (db.item_grupo.productoXpedido==db.productoXpedido.id) &
             (db.item_grupo.grupo==db.proxy_pedido.grupo) &
             (db.proxy_pedido.pedido==db.pedido.id) &
             (db.proxy_pedido.via==ired)
    )

    query_grupos = (query &
             (db.item_grupo.precio_red>0) &
             (db.proxy_pedido.grupo==db.grupo.id)
    )
    field_precio_red = db.item_grupo.precio_red.sum()
    data_grupo_fecha = Tabular(
        query_grupos, db.grupo.nombre, db.pedido.fecha_reparto, field_precio_red,
        col_represent=representa_fecha, data_represent=representa_precio
    )
    stat_grupo_fecha = Estadistica(
        data_grupo_fecha,
        rosheet_options=dict(
            _id='tabla_grupos',
            col_totals=True,
            row_totals=True),
        ods_options=dict(download_stat='stat_grupo_fecha')
    )

    query_cantidad = (query &
             (db.item_grupo.cantidad>0)
    )
    field_cantidad = db.item_grupo.cantidad_red.sum()
    data_producto_fecha = Tabular(
        query, db.productoXpedido.nombre, db.pedido.fecha_reparto, field_cantidad,
        col_represent=representa_fecha
    )
    stat_producto_fecha = Estadistica(
        data_producto_fecha,
        rosheet_options=dict(
            _id='tabla_productos',
            col_totals=True,
            row_totals=True),
        ods_options=dict(download_stat='stat_producto_fecha')
    )

    if request.extension=='ods':
        stats = {'stat_grupo_fecha':stat_grupo_fecha,
                 'stat_producto_fecha':stat_producto_fecha,
        }
        stat = stats[request.vars.download_stat]
        return stat.ods(filename='%s_%s_en_%s.ods'%(
            request.vars.download_stat,
            K.ascii_safe(nombre_productor, filename_safe=True),
            K.ascii_safe(nombre_red, filename_safe=True)
            ))

    return dict(stat_grupo_fecha = stat_grupo_fecha,
                stat_producto_fecha = stat_producto_fecha,
                form_rango_fechas=form_rango_fechas,
                nombre_productor=nombre_productor,
                nombre_red=nombre_red,
                red=ired)
