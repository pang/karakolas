# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################

import os
from kutils import represent

def index():
    redirect(URL(c='default', f='index.html'))

@scope.set(scope_grupo_o_red)
#@requires_get_arguments(('productor', db.productor))
@auth.requires_membership(Permisos.grupo_del_productor(request.vars.productor))
def nuevo_pedido():
    from modelos.pedidos import nuevo_pedido, mueve_productos_al_pedido, \
         mueve_productos_al_pedido_fantasma, HayPedidoParaEsaFecha,\
         abre_pedidos_en_grupos_de_la_red
    from evalua_coste_extra import chequea_coste_extra_en_form

    iproductor = int(request.vars.productor)
    productor  = db.productor[iproductor]
    igrupo_productor = productor.grupo
    contabilidad = ( db.grupo(igrupo_productor).opcion_ingresos !=
                     CONT.OPCIONES_INGRESOS.NO_CONT )
    if request.vars.grupos:
        grupos = [int(g) for g in request.vars.grupos.split(',')]
        assert all(db.grupoXred(grupo=gid,red=igrupo_productor, activo=True) for gid in grupos)
    else:
        grupos = []

    response.title = T('Nuevo Pedido de %s')%productor.nombre
    fields = ['info_cierre', 'fecha_reparto', 'formula_coste_extra_pedido',
              'usa_plantilla', 'xls', 'reglas']
    if contabilidad:
        fields.insert(2,'formula_coste_extra_productor')
    form = SQLFORM(db.pedido, fields=fields)
    form.custom.submit.add_class('btn-primary')
    texto = XML(productor.info_pedido)
    form.vars.formula_coste_extra_pedido = productor.formula_coste_extra_pedido
    form.vars.formula_coste_extra_productor = productor.formula_coste_extra_productor
    form.vars.reglas = productor.reglas
    muestra_plantilla = False

    #No se puede hacer: request.vars.xls.filename if request.vars.xls else ''
    #pq bool(.) de los objetos xls es False
    xls_filename = request.vars.xls.filename if (request.vars.xls not in ['',None]) else ''
    if form.validate(keepvalues=True, onvalidation=chequea_coste_extra_en_form):
        fecha_reparto = form.vars.fecha_reparto
        form.vars.xls_filename = xls_filename
        if not db((db.pedido.fecha_reparto==fecha_reparto) &
                  (db.pedido.productor==iproductor)).isempty():
            form.errors.fecha_reparto = T('Ya hay pedido de este productor para esta fecha')
        elif fecha_reparto < date.today():
            form.errors.fecha_reparto = T('No se puede abrir un pedido en una fecha pasada.')
        try:
            pid = nuevo_pedido(iproductor, fecha_reparto, form.vars)

            if form.vars.usa_plantilla:
                muestra_plantilla = True
                if form.vars.xls:
                    filename,_ = db.pedido.xls.retrieve(form.vars.xls)
                    _,ext = os.path.splitext(filename)
                    if ext=='.odt':
                        form.errors.xls = T('En este momento solo se puede importar la extension xls. Si necesitas importar archivos odt, por favor escríbenos.')
                    elif ext!='.xls':
                        form.errors.xls = T('En este momento solo se puede importar la extension xls.')
                    else:
                        from importar_tablas import importa_plantilla, RuleParsingError
                        try:
                            importa_plantilla(pid)
                        except (SyntaxError, RuleParsingError) as e:
                            form.errors.reglas = (
                        T('Estas reglas no sirven para importar esta hoja de cálculo.') +
                        ' (%s)'%','.join(str(arg) for arg in e.args))
                else:
                    form.errors.xls = T('Por favor sube el archivo con el pedido.')
            else:
                mueve_productos_al_pedido(pid)

            if form.errors:
                db.pedido(pid).delete_record()

        except HayPedidoParaEsaFecha:
            form.errors.fecha_reparto = T('Ya hay un pedido de %s para esa fecha')%productor.nombre

        if not form.errors:
            #Si este es el pedido mas actual del productor...
            pedido_mas_reciente = db((db.pedido.productor==iproductor) &
                                     (db.pedido.fecha_reparto>fecha_reparto)
                                    ).count() == 0
            if pedido_mas_reciente:
                productor.update_record(
                    formula_coste_extra_pedido=form.vars.formula_coste_extra_pedido,
                    formula_coste_extra_productor=form.vars.formula_coste_extra_productor
                )
                if form.vars.usa_plantilla:
                    productor.update_record(reglas=form.vars.reglas)
            if form.vars.usa_plantilla and pedido_mas_reciente:
                db.pedido(pid).update_record(sin_confirmar=True)
                #mueve_productos_al_pedido_fantasma(pid)

            if grupos:
                #Si el productor pertenece a una red, hay que abrir el pedido en los grupos de la red
                errores_email = abre_pedidos_en_grupos_de_la_red([pid], grupos)

                if errores_email:
                    session.flash = T('Se ha intentado avisar a los grupos de la red de que has abierto el pedido, pero algunos emails no han podido enviarse: ') + ','.join(errores_email)
                else:
                    session.flash = T('Se ha avisado por emails a los grupos de que has abierto el pedido')
            else:
                session.flash = T('Se ha abierto el pedido')
            redirect(URL(c='productores',
                         f='productos.html',
                         vars=dict(pedido=pid, productor=iproductor)))
    if form.errors:
        response.flash = C.MENSAJE_ERRORES

    grupos_pedido = db( db.grupo.id.belongs(grupos) &
                       (db.proxy_productor.productor==iproductor) &
                       (db.proxy_productor.grupo==db.grupo.id)) \
                    .select(db.proxy_productor.estado,
                            db.grupo.nombre,
                            orderby=db.grupo.nombre)
    return dict(texto=texto,
                productor=productor,
                nuevo_pedido=form,
                muestra_plantilla=muestra_plantilla,
                grupos_pedido=grupos_pedido)

@scope.set(scope_grupo_o_red)
#@requires_get_arguments(('grupo', db.grupo))
@auth.requires_membership('admins_%s'%request.vars.grupo)
def nuevos_pedidos():
    from modelos.grupos import nodos_en_la_red
    from modelos.pedidos import nuevo_pedido, mueve_productos_al_pedido, \
                                abre_pedidos_en_grupos_de_la_red

    igrupo = int(request.vars.grupo)
    grupo  = db.grupo(igrupo)

    productores = db((db.productor.grupo==igrupo) &
                     (db.productor.activo==True)) \
            .select(db.productor.nombre, db.productor.id, db.productor.grupo,
                    db.productor.formula_coste_extra_pedido, db.productor.formula_coste_extra_productor,
                    db.productor.formula_precio_final, db.productor.formula_precio_productor,
                    orderby=db.productor.nombre)
    js_toggle_all_checkboxes = '''
var checkboxes = $('input.checkbox_%s');
if(checkboxes.is(':checked')) {
    checkboxes.prop('checked', false);
} else {
    checkboxes.prop('checked', true);
};'''
    f = FORM(TABLE(
                 TR(T('Fecha de reparto'),
                    INPUT(_name='fecha_reparto', requires=IS_DATE(), _class='date')),
                 TR(T('Info relevante sobre el cierre'),
                    TEXTAREA(_name='info_cierre', _rows=3))),
             DIV(DIV(
                TABLE(THEAD(TR(TH('Productores'),
                               TH(SPAN(_class='glyphicon glyphicon-check',
                                       _onclick=js_toggle_all_checkboxes%'productor')))),
                    *[TR(TD(row.nombre),
                         TD(INPUT(_type='checkbox',
                                  _name='productor_%d'%row.id,
                                  _class='checkbox_productor')))
                for row in productores]),_class='col-md-6 col-sm-6'),
             DIV(TABLE(THEAD(TR(TH('Grupos'),
                                TH(SPAN(_class='glyphicon glyphicon-check',
                                        _onclick=js_toggle_all_checkboxes%'grupo')))),
                    *[TR(TD(gnombre),
                         TD(INPUT(_type='checkbox',
                                  _name='grupo_%d'%gid,
                                  _class='checkbox_grupo')))
                      for gid,gnombre in
                            sorted(nodos_en_la_red(igrupo).items(),
                                   key=lambda s:s[1].lower())
                     ])
                if grupo.red else '', _class='col-md-6 col-sm-6'),
                _class='row'),
             INPUT(_type='submit',_value='Enviar', _class='btn btn-primary')
    )
    if f.process(formname='nuevos_pedidos', keepvalues=True).accepted:
        pd = productores.as_dict()
        info_cierre   = f.vars.info_cierre
        fecha_reparto = f.vars.fecha_reparto

        productores = []
        grupos      = []
        for s in request.vars:
            if s.startswith('productor_'):
                productores.append(int(s[10:]))
            if s.startswith('grupo_'):
                grupos.append( int(s[6:]))
        if not db((db.pedido.fecha_reparto==fecha_reparto) &
                  (db.pedido.productor.belongs(productores))).isempty():
            f.errors.fecha_reparto = T('Ya hay pedidos de algunos de los productores marcados para esta fecha')
            error_ya_hay_pedido = T('Ya hay pedido de este productor para esta fecha')
            for row in db((db.pedido.fecha_reparto==fecha_reparto) &
                          (db.pedido.productor.belongs(productores))).select(db.pedido.productor):
                f.errors['productor_%d'%row.productor] = error_ya_hay_pedido
            estado = 'errores'
        elif fecha_reparto < date.today():
            f.errors.fecha_reparto = T('No se puede abrir un pedido en una fecha pasada.')
            estado = 'errores'
        elif productores and (grupos if grupo.red else True):
            productores_pedidos = []
            for iproductor in productores:
                productor  = pd[iproductor]
                ipedido = nuevo_pedido(iproductor, fecha_reparto,
                    {'info_cierre':info_cierre}
                )

                mueve_productos_al_pedido(ipedido)

                productores_pedidos.append((productor, ipedido))

            #Si el productor pertenece a una red, hay que abrir el pedido en los grupos de la red
            if grupo.red:
                errores_email = abre_pedidos_en_grupos_de_la_red(
                                    [ipedido for _,ipedido in productores_pedidos],
                                    grupos)
                if errores_email:
                    response.flash = T('Se ha intentado avisar a los grupos de la red de que has abierto el pedido, pero algunos emails no han podido enviarse: ') + ','.join(errores_email)
                else:
                    response.flash = T('Se ha avisado por email a los grupos de que has abierto el pedido')

            f = TABLE(THEAD(TR(TD(T('Productores'), _colspan=2) )),
                      *[TR(TD(productor['nombre'],
                           TD(A(T('Repasar productos'),
                             _href= URL(c='productores', f='productos.html',
                                        vars=dict(pedido=ipedido, productor=productor['id'])),
                             _target='_blank'
                           ))))
                    for productor, ipedido in productores_pedidos])
            estado = 'ok'
        else:
            response.flash = T('Selecciona al menos un productor')
            estado = 'errores'
    elif f.errors:
        response.flash = C.MENSAJE_ERRORES
        estado = 'errores'
    else:
        estado = 'limpio'
    return dict(form=f, estado=estado, grupo=igrupo)

#@requires_get_arguments(('grupo', db.grupo))
@auth.requires_membership('admins_%s'%request.vars.grupo)
def olvidar_regla_ajax():
    grupo = request.vars.grupo
    nombre_reglas = request.vars.nombre_reglas
    if request.vars.olvidar and grupo and nombre_reglas:
        res = db((db.reglas_plantilla.grupo==grupo)&
                 (db.reglas_plantilla.nombre_reglas==nombre_reglas),
                ).delete()
    return DIV(*[CAT(INPUT(_type='button', _value=row.nombre_reglas, _class='btn btn-default',
                              _onclick="$('#nombre_reglas').val('%s');$('#textarea_reglas').val($('#%s').contents()[0].data);"%(row.nombre_reglas,row.nombre_reglas)),
                               SPAN(row.reglas, _id=row.nombre_reglas, _class='hidden',))
                              for row in db((db.reglas_plantilla.grupo==grupo))\
                                         .select(db.reglas_plantilla.nombre_reglas,
                                                 db.reglas_plantilla.reglas)
                            ], _id='lista_reglas', _class='btn-group')

# cisam:
# actualiza pedido_fantasta desde el ultimo pedido procedente de plantilla excel
@scope.set(scope_grupo_o_red)
#@requires_get_arguments(('productor', db.productor))
@auth.requires_membership(Permisos.grupo_del_pedido(request.vars.pedido))
def confirmar_importacion_correcta():
    from modelos.pedidos import mueve_productos_al_pedido_fantasma

    ipedido = int(request.vars.pedido)
    pedido = db.pedido[ipedido]

    form = FORM(DIV(
       INPUT(_value='Validar productos',_type='submit',_class='btn btn-primary'),
       **{'_data-toggle':'tooltip', '_data-placement':'bottom','_title':'Si nos vas a hacer ningún cambio, pulsa para que esta relacion de productos se configuren para el proximo pedido que se abra'}
    ))

    if form.validate(keepvalues=True):
        iproductor = pedido.productor
        fecha_reparto = pedido.fecha_reparto
        pedido_mas_reciente = db((db.pedido.productor==iproductor) &
                 (db.pedido.fecha_reparto>fecha_reparto)
        ).count() == 0
        if pedido_mas_reciente:
            productor = db.productor[iproductor]
            grupo = productor.grupo
            mueve_productos_al_pedido_fantasma(ipedido)
            session.flash = T('Los productos del pedido de %s \n para el %s han sido validados correctamente')%(
                            productor.nombre, pedido.fecha_reparto)
        redirect(URL(c='gestion_pedidos', f='gestion_pedidos.html',
                            vars=dict(grupo=grupo)))

    return dict(form=form, pedido=ipedido)

@scope.set(scope_grupo_o_red)
#@requires_get_arguments(('grupo', db.grupo))
@auth.requires_membership('admins_%s'%request.vars.grupo)
def pedidos_por_plantilla():
    from modelos.grupos import grupos_de_la_red
    from modelos.pedidos import abre_pedidos_en_grupos_de_la_red, HayPedidoParaEsaFecha, \
                                mueve_productos_al_pedido_fantasma

    igrupo = int(request.vars.grupo)
    grupo  = db.grupo(igrupo)

    response.title = T('Importa varios pedidos usando una hoja de cálculo como plantilla.')

    #TODO: cambia el Nombre de las Reglas por el nombre del bloque de pedidos...
    f = FORM(TABLE(
        TR(TD(LABEL(T('Hoja de cálculo'),
                    TOOLTIP(T('Selecciona el fichero con los datos. A día de hoy, sólo soportamos el formato xls. Si quieres usar archivos OpenDocument, por favor ponte en contacto con los desarrolladores.'))),
              INPUT(_type='file', _name='plantilla',
                  requires=IS_NOT_EMPTY(T('Selecciona la hoja de cálculo con los datos')))
         )),
        TR(TD(LABEL(T('Reglas para importar la hoja'),
                    TOOLTIP(T('Estas reglas indican a karakolas como importar la hoja de cálculo.'))
                   ),
              TEXTAREA(_name='reglas', _id='textarea_reglas',
                       requires=IS_NOT_EMPTY(T('Necesitamos que especifiques las "reglas de importación"'))),
              DIV(FIELDSET(LABEL(T('Nombre de las reglas'),
                                 TOOLTIP(T('Puedes dar un nombre a estas reglas para referencia futura. Las reglas no se almacenan si la hoja no se importa con éxito.'))),
                         INPUT(_type='text', _name='nombre_reglas', _id='nombre_reglas'),
            INPUT(_type='button', _value=T('Olvidar esta regla'), _class='btn btn-default',
                  _onclick="web2py.component('/%s/gestion_pedidos/olvidar_regla_ajax.load?olvidar=olvidar&grupo=%d&nombre_reglas='+ $('#nombre_reglas').val(), 'lista_reglas')"%(request.application, igrupo))),
                  olvidar_regla_ajax())
        )),
        TR(TD(LABEL(T('Fecha de reparto')),
                      INPUT(_name='fecha_reparto', requires=IS_DATE(), _class='date'),
                     )),
        TR(TD(LABEL(T('Info relevante sobre el cierre')),
                      INPUT(_name='info_cierre')))),

        DIV(TABLE(THEAD(TR(TH('Grupos',_colspan=2))),
                *[TR(TD(gnombre),
                    TD(INPUT(_type='checkbox',_name='grupo_%d'%gid)))
            for gid,gnombre in grupos_de_la_red(igrupo).items()])
            if grupo.red else ''),
        INPUT(_type='submit',
              _name='enviar_pedidos_por_plantilla',
              _value='Enviar',
              _class='btn btn-primary'),
        SCRIPT('''$(function(){ $('[data-toggle="tooltip"]').tooltip(); });'''),
        _id='form_pedidos_plantilla'
        )
    pedidos = descolgados = None

    if f.validate(formname='pedidos_por_plantilla', keepvalues=True):
        reglas        = f.vars.reglas
        nombre_reglas = f.vars.nombre_reglas
        filename      = f.vars.plantilla.filename
        _,ext = os.path.splitext(filename)
        fecha_reparto = f.vars.fecha_reparto
        if grupo.red:
            grupos      = []
            for s in request.vars:
                if s.startswith('grupo_'):
                    grupos.append( int(s[6:]) )
        if ext=='.ods':
            f.errors.plantilla = T('En este momento solo se puede importar archivos de tipo "xls". Si necesitas importar archivos ods, por favor escríbenos.')
        elif ext!='.xls':
            f.errors.plantilla = T('En este momento solo se puede importar la extension xls.')
        if grupo.red and not grupos:
            for gid,gnombre in grupos_de_la_red(igrupo).items():
                f.errors['grupo_%d'%gid] = T('Selecciona al menos un grupo para este reparto.')
        if not f.errors:
            try:
                from importar_tablas import importa_plantilla_multiple, RuleParsingError
                dpedidos, descolgados = importa_plantilla_multiple(
                    reglas, f.vars.plantilla,
                    igrupo, fecha_reparto)

                ipedidos = list(dpedidos.values())
                if ipedidos:
                    rows = db((db.pedido.productor==db.productor.id) &
                              (db.pedido.id.belongs(ipedidos))) \
                           .select(db.productor.id, db.productor.nombre, db.pedido.id)
                    pedidos = [(row.productor.nombre, row.productor.id, row.pedido.id)
                               for row in rows]

                    #Si este es el pedido mas actual del productor, actualiza los productos
                    #del pedido fantasma
                    pedido_mas_reciente = defaultdict(lambda : True)
                    for row in db((db.pedido.productor.belongs(list(dpedidos.keys()))) &
                                  (db.pedido.fecha_reparto>fecha_reparto)) \
                               .select(db.pedido.productor, groupby=db.pedido.productor):
                        pedido_mas_reciente[row.productor] = False
                    for iproductor, ipedido in dpedidos.items():
                        if pedido_mas_reciente[iproductor]:
                            mueve_productos_al_pedido_fantasma(ipedido)

                    f.vars.plantilla.file.seek(0) #Necesario porque ya he leido el fichero
                    db.plantilla_pedidos.update_or_insert(
                        db.plantilla_pedidos.pedidos==ipedidos,
                        grupo=igrupo,
                        fecha_reparto=fecha_reparto,
                        pedidos=ipedidos,
                        plantilla=db.plantilla_pedidos.plantilla.store(f.vars.plantilla.file, filename),
                        plantilla_filename=filename,
                        reglas=reglas,
                        nombre_bloque=nombre_reglas
                        )
                    if nombre_reglas:
                        db.reglas_plantilla.update_or_insert(
                            (db.reglas_plantilla.grupo==igrupo)&
                            (db.reglas_plantilla.nombre_reglas==nombre_reglas),
                            grupo=igrupo, nombre_reglas=nombre_reglas, reglas=reglas)
                    if grupo.red:
                        #Si el productor pertenece a una red, hay que abrir el pedido en los grupos de la red
                        errores_email = abre_pedidos_en_grupos_de_la_red(ipedidos, grupos)
                        if errores_email:
                            response.flash = T('Se ha intentado avisar por email a los grupos de la red de que has abierto el pedido, pero algunos grupos no han podido ser avisados: ') + ','.join(errores_email)
                        else:
                            response.flash = T('Se ha avisado por email a los grupos de que has abierto el pedido')
                    if descolgados:
                        response.flash = T('Se han abierto *algunos* pedidos con los productos de la hoja de cálculo')
                    else:
                        response.flash = T('Se han abierto pedidos usando todos los productos de la hoja de cálculo')
                elif descolgados:
                    response.flash = T('No se encontró la información necesaria para abrir ningún pedido.')
                else:
                    response.flash = T('No se encontró la información necesaria para abrir ningún pedido. Por favor repasa las reglas de importación y el formato del archivo.')
                    f.errors.reglas = T('Estas reglas son sintácticamente correctas, pero no sirven para importar esta hoja de cálculo.')

            except (SyntaxError, RuleParsingError) as e:
                f.errors.reglas = (
                    T('Estas reglas no sirven para importar esta hoja de cálculo.') +
                    ' (%s)'%','.join(str(arg) for arg in e.args))
                response.flash  = C.MENSAJE_ERRORES
            except HayPedidoParaEsaFecha:
                f.errors.fecha_reparto = T('Ya hay pedidos para esta fecha de algunos productores mencionados en esta plantilla')
                response.flash  = C.MENSAJE_ERRORES

    if f.errors:
        response.flash = C.MENSAJE_ERRORES
    return dict(form=f, grupo=igrupo, pedidos=pedidos, descolgados=descolgados)

@scope.set(scope_grupo_o_red)
#@requires_get_arguments(('pedido', db.pedido))
@auth.requires_membership(Permisos.grupo_del_pedido(request.vars.pedido))
def edita_pedido():
    # ¿al cambiar la fecha cambia la fecha del proxy_pedido? Es US #186
    from modelos.pedidos import post_eliminar_pedido, recalcula_costes_extra_pedido
    from evalua_coste_extra import chequea_coste_extra_en_form

    ipedido = int(request.vars.pedido)
    pedido  = db.pedido(ipedido)
    fecha_reparto_anterior = pedido.fecha_reparto
    productor  = db.productor( pedido.productor )
    iproductor = productor.id
    grupo  = productor.grupo
    igrupo = grupo.id
    contabilidad = ( grupo.opcion_ingresos != CONT.OPCIONES_INGRESOS.NO_CONT )
    incidencias_coste_pedido = None

    response.title = T('Editando %s')%format_pedido(pedido)

    fields = ['info_cierre', 'fecha_reparto',
              'abierto', 'formula_coste_extra_pedido']
    if contabilidad:
        fields.insert(3,'formula_coste_extra_productor')
    form = SQLFORM(db.pedido, ipedido,
                   deletable=True,
                   delete_label=T('Cancelar el pedido'),
                   fields=fields
                   )

    if form.process(onvalidation=chequea_coste_extra_en_form).accepted:
        if form.vars.delete_this_record:
            post_eliminar_pedido(ipedido)
            redirect( URL(f='gestion_pedidos.html', vars=dict(grupo=igrupo)) )
        if db((db.pedido.productor==iproductor) &
              (db.pedido.fecha_reparto>form.vars.fecha_reparto)).count() == 0:
            #Si este es el pedido mas actual del productor...
            fields_to_update = {
                'formula_coste_extra_pedido':form.vars.formula_coste_extra_pedido
            }
            if contabilidad:
                fields_to_update['formula_coste_extra_productor'] = form.vars.formula_coste_extra_productor
            productor.update_record(**fields_to_update)
        if form.vars.fecha_reparto>fecha_reparto_anterior:
            db((db.proxy_pedido.pedido==ipedido) &
               (db.proxy_pedido.fecha_reparto<form.vars.fecha_reparto)).update(
                fecha_reparto=form.vars.fecha_reparto
            )

        if (not db((db.peticion.productoXpedido==db.productoXpedido.id) &
                   (db.productoXpedido.pedido==ipedido)).isempty()
            and not form.vars.abierto):
                incidencias_coste_pedido = recalcula_costes_extra_pedido(ipedido, igrupo)
                if incidencias_coste_pedido:
                    response.flash = T('La fórmula que acabas de introducir para el coste extra del pedido produce errores. Por favor atiende a los problemas indicados más abajo.')
                else:
                    from modelos.pedidos import anota_incidencias_contabilidad
                    anota_incidencias_contabilidad(ipedido)
                    redirect( URL(f='vista_pedido.html', vars=dict(pedido=ipedido)) )
        else:
            redirect( URL(f='gestion_pedidos.html', vars=dict(grupo=igrupo)) )

    return dict(form=form,
                es_red=grupo.red,
                incidencias_coste_pedido=incidencias_coste_pedido,
                grupo=grupo
    )

@scope.set('admin-grupo')
#@requires_get_arguments(('grupo', db.grupo), ('pedido', db.pedido),
                        # _consistency = ( (db.grupo.id==db.grupoXred.grupo) &
                        #                  (db.productor.grupo==db.grupoXred.red) &
                        #                  (db.pedido.productor==db.productor.id) )
#                        )
@auth.requires_membership('admins_%s'%request.vars.grupo)
def aceptar_productos_coordinados():
    from eval_arit import EvalAritException
    from modelos.pedidos import dame_valores_extra, actualiza_precios_peticion, activa_productos_coordinados
    tppp = db.productoXpedido
    mp   = db.mascara_producto
    mp2 = db.mascara_producto.with_alias('mp2')

    ipedido    = int(request.vars.pedido)
    pedido     = db.pedido[ipedido]
    productor  = pedido.productor
    iproductor = productor.id
    igrupo     = int(request.vars.grupo)
    proxy      = db.proxy_pedido(pedido=ipedido, grupo=igrupo)
    formula_sobreprecio = proxy.formula_sobreprecio

    response.title = T('Aceptar/Bloquear productos del %s')%represent(db.pedido, pedido)

    # Si el pedido viene directamente de la red
    directo_de_la_red = (proxy.via == productor.grupo)
    if directo_de_la_red:
        rs = db((mp.grupo==igrupo) &
                (mp.productoXpedido==tppp.id) &
                (tppp.pedido==ipedido) &
                (tppp.temporada==True) &
                (tppp.activo==True) &
                (tppp.categoria==db.categorias_productos.id)) \
             .select(mp.ALL,
                     tppp.ALL,
                     db.categorias_productos.nombre,
                     orderby=db.categorias_productos.nombre|tppp.nombre)
    # Si el pedido viene de otra red
    else:
        rs = db((mp.grupo==igrupo) &
                (mp2.grupo==proxy.via) &
                (mp2.aceptado==True) &
                (mp.productoXpedido==mp2.productoXpedido) &
                (mp.productoXpedido==tppp.id) &
                (tppp.pedido==ipedido) &
                (tppp.categoria==db.categorias_productos.id)) \
             .select(mp.ALL,
                     mp2.ALL,
                     tppp.ALL,
                     db.categorias_productos.nombre,
                     orderby=db.categorias_productos.nombre|tppp.nombre)

    p2extra = dame_valores_extra(ipedido)

    form = FORM(FIELDSET(T('Formula para el precio final: '),
                         TEXTAREA(_name='formula_sobreprecio', _id='formula_sobreprecio',
                                  value=formula_sobreprecio,
                                  _cols="40", _rows="1"),
                         DIV(BUTTON(T('Aplicar'),
                                    _id='boton_aplicar_formula',
                                    _type='button',
                                    _class='btn btn-success'),
                             BUTTON(T('Restaurar'),
                                    _id='boton_restaurar_formula',
                                    _type='button',
                                    _class='btn btn-success'),
                             _class='btn-group',
                             _id='botones_formula_sobreprecio'),
                         _id='field_formula_sobreprecio'),
                SPAN('formato,peso', _id='columnas_extra', _class='hidden'),
                INPUT(_value=formula_sobreprecio, _id='formula_original', _type='hidden'),
                TABLE(THEAD(
                      TR(TH('Producto'),
                         TH('Precio Red'),
                         TH('Aceptado ',
                            SPAN(_class='glyphicon glyphicon-check',
                                 _id='marcar_desmarcar_todos')),
                         TH('Precio Grupo', _id='th_precio_grupo'))),
                  TBODY(*[TR(TD(row.productoXpedido.nombre, ' ',
                          A(IMG(_src=URL('static', 'images/info.png'), _alt="info"),
                            SPAN(row.productoXpedido.descripcion, TAG.br(),
                                 row.categorias_productos.nombre,
                                 TAG.br(), 'Precio base: %s'%row.productoXpedido.precio_base,
                                 UL(*[LI('%s: %s'%(k,v)) for k,v in p2extra[row.productoXpedido.producto].items()])),
                            _href='#', _class='my_tooltip') ),
                       TD(SPAN(row.productoXpedido.precio_final if directo_de_la_red else
                               row['mp2'].precio,
                               _id='precio_red_%d'%row.mascara_producto.id),
                          *[SPAN(v, _id='%s_%d'%(k,row.mascara_producto.id), _class='hidden')
                            for k,v in p2extra[row.productoXpedido.producto].items()]),
                       TD(INPUT(_type='checkbox',
                                _name='aceptado_%d'%row.mascara_producto.id,
                                _class='aceptar_producto',
                                value=row.mascara_producto.aceptado)),
                       TD(INPUT(value=row.mascara_producto.precio,
                                _name='precio_%d'%row.mascara_producto.id,
                                _id='precio_%d'%row.mascara_producto.id,
                                _class='decimal mascara_producto_precio'))
                       )
                       for row in rs])),
              INPUT(_value=T('Enviar'), _type='submit', _name='submit', _class='btn btn-primary')
    )
    def update_ok(form):
        proxy.update_record(formula_sobreprecio=request.vars.formula_sobreprecio)
        # Actualiza mascara_producto.precio y no evalúes la fórmula
        for row in rs:
            mid = row.mascara_producto.id
            db.mascara_producto(mid).update_record(
                aceptado = request.vars['aceptado_%d'%mid] or False,
                precio = request.vars['precio_%d'%mid] or Decimal()
            )
        try:
            activa_productos_coordinados(ipedido, igrupo, solo_descendientes=True)
            #Actualiza el precio de los productos en mascara_producto
            #Actualiza el precio en peticion, item, item_grupo
            # TODO: redderedes hay que actualizar los precios de este grupo, si es grupo, o de todos los
            # grupos que cuelgan de esta red, pero no de los grupos que no cuelgan de esta red :-/
            actualiza_precios_peticion(ipedido, igrupo)
            # Atencion: si algun dia permitimos editar los precios locales de los productos
            # con el pedido cerrado, sera necesario recalcular los precios y costes extra
        except (SyntaxError, EvalAritException) as e:
            form.errors.formula_sobreprecio = e.message or T('Error en la fórmula')
            proxy.update_record(formula_sobreprecio=formula_sobreprecio)

    if form.process(onvalidation=update_ok).accepted:
        if request.vars.formula_sobreprecio != C.FORMULA_SOBREPRECIO_PEDIDO_COORDINADO_POR_DEFECTO:
            session.flash = T('Se han modificado los precios de los productos del pedido de %s')%db.productor[iproductor].nombre
        else:
            session.flash = T('Se han aceptado los productos del pedido de %s que has marcado')%db.productor[iproductor].nombre
        redirect(URL(f='gestion_pedidos.html', vars=dict(grupo=igrupo)))
    elif form.errors:
        response.flash = T('No podemos evaluar la fórmula que has escrito.')

    response.files.append(URL('static', 'js/esprima.js'))
    response.files.append(URL('static', 'js/eval_arit.js'))
    return dict(validar = form, formula=proxy.formula_sobreprecio)

@scope.set(scope_grupo_o_red)
#@requires_get_arguments(('grupo', db.grupo))
@auth.requires_membership('admins_%s'%request.vars.grupo)
def selector_productor_para_nuevo_pedido():
    from modelos.grupos import nodos_en_la_red

    igrupo = int(request.vars.grupo)
    grupo  = db.grupo(igrupo)
    nuevo_pedido = FORM(FIELDSET(T('Abrir nuevo pedido de: '),
                 SELECT(OPTION(T('Elige un productor'), _value=-1),
                        *[OPTION(p.nombre, _value=p.id)
                          for p in db((db.productor.id>0) &
                                      (db.productor.grupo==igrupo) &
                                      (db.productor.activo==True))\
                                    .select(db.productor.nombre,
                                            db.productor.id,
                                            orderby=db.productor.nombre)],
                        **dict(_name='productor', _id='productor'))),
                     DIV(TABLE(THEAD(TR(TH('Grupos',_colspan=2))),
                            *[TR(TD(gnombre),
                                TD(INPUT(_type='checkbox',_name='grupo_%d'%gid)))
                        for gid,gnombre in sorted(nodos_en_la_red(igrupo).items(),
                                                  key=lambda s:s[1].lower())])
                        if grupo.red else ''),
                 INPUT(_type='submit', _name='submit', _value=T('Abrir pedido'), _class='btn btn-primary'),
                 _action=URL(c='gestion_pedidos', f='selector_productor_para_nuevo_pedido.load',
                             vars=dict(grupo=igrupo)))
    def ha_seleccionado_productor(form):
        if form.vars.productor=='-1':
            form.errors.productor = T('Por favor selecciona un productor.')
    if nuevo_pedido.process(onvalidation=ha_seleccionado_productor, formname='nuevo_pedido').accepted:
        grupos = ','.join(v[6:] for v in request.post_vars if v.startswith('grupo_'))
        redirect(URL(c='gestion_pedidos', f='nuevo_pedido.html', vars={'productor':request.vars.productor, 'grupos':grupos}))
    elif nuevo_pedido.errors:
        response.flash = T('Selecciona un productor del desplegable...')
    return dict(nuevo_pedido=nuevo_pedido,
                grupo=igrupo)

@scope.set(scope_grupo_o_red)
#@requires_get_arguments(('grupo', db.grupo))
@auth.requires_membership('admins_%s'%request.vars.grupo)
@service.json
def gestion_pedidos():
    from modelos.pedidos import dame_pedidos_por_fecha, via_pedido_en_grupo
    igrupo = int(request.vars.grupo)
    grupo  = db.grupo(igrupo)
    tp = request.vars.tipo_pedidos or EstadosPedido.RECIENTES

    response.title = T('Gestiona los pedidos de %s')%db.grupo(igrupo).nombre

    links = [A(v, _href=URL(f='gestion_pedidos',
                            vars=dict(tipo_pedidos=k, grupo=igrupo)),
               _role='menuitem')
             for k,v in dEstadosPedidoPlural.items() if k!=tp]

    form_rango_fechas=None
    if tp in (EstadosPedido.ACTIVOS, EstadosPedido.RECIENTES):
        pedidos_a = dame_pedidos_por_fecha(igrupo, EstadosPedido.ABIERTOS,
                                           todos_los_coordinados=True)
        pedidos_p = dame_pedidos_por_fecha(
                igrupo, 
                EstadosPedido.PENDIENTES if tp==EstadosPedido.ACTIVOS
                else EstadosPedido.PENDIENTES_RECIENTES,
                todos_los_coordinados=True)
        pedidos_c = dame_pedidos_por_fecha(igrupo, EstadosPedido.CERRADOS,
                                           todos_los_coordinados=True)
        dabiertos = defaultdict(lambda:((),()),pedidos_a)
        dpendientes = defaultdict(lambda:((),()),pedidos_p)
        dcerrados = defaultdict(lambda:((),()),pedidos_c)
        fechas = list(set(dabiertos.keys()).union(list(dcerrados.keys())).union(list(dpendientes.keys())))
        fechas.sort()
        pedidos = [(f, dabiertos[f], dcerrados[f], dpendientes[f])
                    for f in fechas]
    elif tp == EstadosPedido.HISTORICOS:
        #FORM para limitar las fechas de los pedidos historicos
        from bloques_vista_estadistica import SelectorRangoFechas
        form_rango_fechas = SelectorRangoFechas()
        form_rango_fechas.redirect_if_hit()
        fecha_ini, fecha_fin = form_rango_fechas.rango_fechas
        representa_fecha = form_rango_fechas.representa_fecha

        pedidos = dame_pedidos_por_fecha(
            igrupo, tp, todos_los_coordinados=True,
            fecha_ini=fecha_ini, fecha_fin=fecha_fin
        )
        fechas  = [f for f,_ in pedidos]
    else:
        pedidos = dame_pedidos_por_fecha(igrupo, tp,
                                         todos_los_coordinados=True)
        fechas  = [f for f,_ in pedidos]
    #Pedidos abiertos con una misma plantilla
    bundles = defaultdict(list)
    spedidos = set()
    for row in db( db.plantilla_pedidos.fecha_reparto.belongs(fechas) &
                  (db.plantilla_pedidos.grupo==igrupo)) \
              .select(db.plantilla_pedidos.id,
                      db.plantilla_pedidos.pedidos,
                      db.plantilla_pedidos.fecha_reparto):
        bundles[row.fecha_reparto].append((row.id, row.pedidos))
        spedidos.update(row.pedidos)
    #TODO: Se puede ahorrar esta query, ya hemos sacado la info en dame_pedidos_por_fecha
    nombres_pedidos = dict((row.pedido.id, row.productor.nombre)
                    for row in db(db.pedido.id.belongs(spedidos) &
                                  (db.pedido.productor==db.productor.id)) \
                               .select(db.productor.nombre, db.pedido.id))
    ipedidos_coordinados = [p.pedido.id for f, *pedidos_tipo in pedidos
                                        for peds in pedidos_tipo
                                        for p in peds[1]]
    via_pedidos_ired = { pid:via_pedido_en_grupo(pid, igrupo) for pid in ipedidos_coordinados }
    nombres_redes = {ired:db.grupo(ired).nombre
        for ired in set(via_pedidos_ired.values())
    }
    via_pedidos = {pid:nombres_redes[ired] for pid,ired in via_pedidos_ired.items()}

    #Cuidado: contains(Field) no rula en MySQL
#    for row in db(db.plantilla_pedidos.fecha_reparto.belongs(fechas) &
#                  db.plantilla_pedidos.pedidos.contains(db.pedido.id) &
#                  (db.pedido.productor==db.productor.id))\
#                 .select(...):
    from bloques_vista_gestion_pedidos import PrinterPedidos
    pp = PrinterPedidos(pedidos, nombres_pedidos, via_pedidos, bundles, grupo, tp)

    return dict(fechas=fechas,
                pedidos=pedidos,
                nombres_pedidos=nombres_pedidos,
                html_pedidos = pp.print_pedidos(),
                links=links,
                tp=tp,
                grupo=igrupo,
                form_rango_fechas=form_rango_fechas,
                dias_pedido_reciente=grupo.dias_pedido_reciente)

@scope.set(scope_grupo_o_red)
#@requires_get_arguments(('pedido', db.pedido))
@auth.requires_membership(Permisos.grupo_del_pedido(request.vars.pedido))
def cerrar_pedido():
    from modelos.pedidos import cerrar_pedidos

    ipedido    = int(request.vars.pedido)
    pedido = db.pedido[ipedido]
    productor = pedido.productor
    grupo = productor.grupo

    incidencias_coste_pedido = cerrar_pedidos([ipedido], forzar_cierre=request.vars.forzar_cierre)
    if incidencias_coste_pedido:
        response.title = T('No se ha podido cerrar el pedido de %s')%represent(db.pedido, pedido)
        response.flash=T('No se han podido calcular los costes extra del pedido. Por favor atiende a este problema.')
    else:
        response.title = T('Se ha cerrado el %s')%represent(db.pedido, pedido)
        response.flash=T('El pedido ha sido cerrado')

    from estadisticas import Estadistica
    Estadistica.add_static_files()
    return dict(pedido=pedido,
                grupo=grupo.id,
                productor=productor,
                es_red=grupo.red,
                incidencias_coste_pedido=incidencias_coste_pedido)

@scope.set(scope_grupo_o_red)
#@requires_get_arguments(('plantilla', db.plantilla_pedidos))
@auth.requires(Permisos.puede_gestionar_plantilla(request.vars.plantilla))
def cerrar_pedidos():
    from modelos.pedidos import cerrar_pedidos

    iplantilla = int(request.vars.plantilla)
    plantilla  = db.plantilla_pedidos[iplantilla]
    pedidos    = plantilla.pedidos
    grupo      = db.pedido(pedidos[0]).productor.grupo
    igrupo     = grupo.id

    nombres_pedidos = dict((row.pedido.id, row.productor.nombre)
                    for row in db(db.pedido.id.belongs(pedidos) &
                                  (db.pedido.productor==db.productor.id)) \
                               .select(db.productor.nombre, db.pedido.id))

    response.title = T('Se han cerrado varios pedidos')

    incidencias_coste_pedido = cerrar_pedidos(pedidos, forzar_cierre=request.vars.forzar_cierre)
    if incidencias_coste_pedido:
        response.flash=T('Los pedidos han sido cerrado, pero no se han podido calcular los costes extra del pedido. Por favor atiende a este problema.')
    else:
        response.flash=T('Los pedidos han sido cerrados')

    precio_total = db.item.precio_total.sum()
    cantidades = defaultdict(Decimal, (
           (row.pedido.id, row[precio_total])
           for row in
           db(db.pedido.id.belongs(pedidos) &
              (db.item.productoXpedido==db.productoXpedido.id) &
              (db.productoXpedido.pedido==db.pedido.id)) \
           .select(db.pedido.id, precio_total,
                   groupby=db.pedido.id)))
    return dict(nombres_pedidos=nombres_pedidos,
                cantidades=cantidades,
                grupo=igrupo,
                es_red=grupo.red,
                incidencias_coste_pedido=incidencias_coste_pedido)

@scope.set('miembro')
#@requires_get_arguments(('pedido', db.pedido))
@auth.requires(Permisos.autoriza_pedido(request.vars, perms='r'))
def vista_pedido():
    from modelos.pedidos import TiposPedido, recalcula_costes_extra_pedido
    ipedido         = int(request.vars.pedido)
    pedido          = db.pedido[ipedido]
    if ('grupo' in request.vars):
        igrupo = int(request.vars.grupo)
        grupo  = db.grupo(igrupo)
    else:
        grupo  = pedido.productor.grupo
        igrupo = grupo.id

    if grupo.red:
        redirect(URL(c='redes', f='vista_pedido_red.html', vars=request.vars))

    response.title = (T('Vista global del %s') + ' (%s)')%(represent(db.pedido, pedido), grupo.nombre)

    if request.vars.recalcula_costes_extra:
        incidencias_coste_pedido = recalcula_costes_extra_pedido(ipedido, igrupo)
        if incidencias_coste_pedido:
            response.flash = T('Se ha producido un error al calcular el coste fijo del pedido. Por favor atiende a los problemas indicados más abajo')
    else:
        incidencias_coste_pedido = None

    return dict(productor=pedido.productor,
                incidencias_coste_pedido=incidencias_coste_pedido,
                grupo=igrupo,
                es_red=grupo.red,
                pedido=ipedido)

@scope.set('admin-grupo')
def aviso_productos_mostrados():
    '''Explica qué productos se muestran en un formulario de incidencias o incidencias_globales
    y pone links a las otras variantes
    '''
    igrupo  = int(request.vars.grupo)
    grupo   = db.grupo(igrupo)
    ipedido = int(request.vars.pedido)
    pedido  = db.pedido(ipedido)
    propio  = pedido.productor.grupo==igrupo
    return dict(grupo=grupo, propio=propio)

@scope.set('miembro')
#@requires_get_arguments(('pedido', db.pedido))
@auth.requires(Permisos.autoriza_pedido(request.vars, perms='r'))
def vista_totales():
    from modelos.pedidos import tipo_pedido, TiposPedido, coste_extra_pedido, \
                                hay_discrepancia_interna, hay_discrepancia_ascendiente, \
                                hay_discrepancia_descendiente, explicaciones_discrepancias
    tppp = db.productoXpedido

    ipedido = int(request.vars.pedido)
    pedido  = db.pedido[ipedido]
    hay_incidencias_coste_pedido = request.vars.hay_incidencias_coste_pedido
    if ('grupo' in request.vars):
        igrupo = int(request.vars.grupo)
        grupo  = db.grupo(igrupo)
    else:
        grupo  = pedido.productor.grupo
        igrupo = grupo.id
    contabilidad = ( grupo.opcion_ingresos != CONT.OPCIONES_INGRESOS.NO_CONT )

    tipo_pedido = tipo_pedido(pedido, igrupo)
    #cerrar discrepancia solo para administradores
    es_admin=auth.has_membership(role='admins_%s'%igrupo)
    if tipo_pedido==TiposPedido.PROPIO:
        formula_coste_extra = pedido.formula_coste_extra_pedido
        fecha_reparto = fecha_recogida = pedido.fecha_reparto
        ppedido = pedido
    else:
        ppedido = db.proxy_pedido(pedido=ipedido, grupo=igrupo)
        formula_coste_extra = ppedido.formula_coste_extra_pedido
        fecha_reparto = ppedido.fecha_reparto
        fecha_recogida = pedido.fecha_reparto

    discrepancias = dict(
        discrepancia_interna=ppedido.discrepancia_interna,
        discrepancia_interna_asumida=ppedido.discrepancia_interna_asumida,
        discrepancia_descendiente=ppedido.discrepancia_descendiente,
        pendiente_consolidacion=ppedido.pendiente_consolidacion
    )
    if tipo_pedido==TiposPedido.PROPIO:
        discrepancias['discrepancia_productor'] = ppedido.discrepancia_productor
    else:
        discrepancias['discrepancia_ascendiente'] = ppedido.discrepancia_ascendiente

    if discrepancias.get('discrepancia_ascendiente') or discrepancias.get('discrepancia_productor'):
        link_enlace_discrepancias = URL(c='gestion_pedidos', f='informe.pdf',
            vars=dict(apaisado='1',fecha_reparto=fecha_reparto,grupo=igrupo,
                      pedidos=ipedido, talla='M', tipo=5))
    else:
         link_enlace_discrepancias = ''
    d_explicaciones_discrepancias = explicaciones_discrepancias()

    def item_as_dict(tabla, campos):
        tabla_ppp = tabla.productoXpedido
        campos_id = campos + [tabla_ppp]
        return {
          row[tabla_ppp]:
           tuple(row[f] for f in campos)
          for row in
          db((tabla.productoXpedido==tppp.id) &
             (tabla.cantidad>0) &
             (tppp.pedido==ipedido) &
             (tabla.grupo==igrupo)) \
             .select(*campos_id, groupby=tabla_ppp)
        }
    peticiones = item_as_dict(db.peticion, [
          db.peticion.cantidad.sum(),
          db.peticion.precio_total.sum()
    ])
    items = item_as_dict(db.item_grupo, [
        db.item_grupo.cantidad.sum(),
        db.item_grupo.cantidad_recibida.sum(),
        db.item_grupo.cantidad_red.sum(),
        db.item_grupo.precio_total.sum(),
        db.item_grupo.precio_recibido.sum(),
        db.item_grupo.precio_red.sum(),
    ])

    ppps = set(peticiones.keys()).union(list(items.keys()))
    prods = [
      (row.productoXpedido.id, row.categorias_productos.nombre, row.productoXpedido.nombre)
      for row in
      db((tppp.id.belongs(ppps))  &
         (db.categorias_productos.id==tppp.categoria)) \
         .select(tppp.id,
                 db.categorias_productos.nombre, tppp.nombre,
                 orderby=db.categorias_productos.nombre|tppp.nombre,
                 groupby=tppp.id)
    ]

    totales=[((pnombre, cat_nombre) +
              peticiones.get(pppid, (Decimal(),)*2) +
              items.get(pppid, (Decimal(),)*6)
              )
             for pppid, cat_nombre, pnombre in prods
             if (peticiones.get(pppid) or items.get(pppid))]

    if pedido.abierto or hay_incidencias_coste_pedido:
        coste_extra = coste_extra_productor = Decimal()
    else:
        coste_extra = coste_extra_pedido(ipedido, igrupo) or Decimal()
        coste_extra_productor = pedido.coste_extra_productor or Decimal()

    peticion_precio_total = sum((p for c,p in peticiones.values()), Decimal())

    if pedido.abierto:
        repartido_precio_total = recibido_precio_total = red_precio_total = Decimal()
    else:
        repartido_precio_total = sum((p for _,_,_,p,_,_ in items.values()), Decimal())
        recibido_precio_total = sum((p for _,_,_,_,p,_ in items.values()), Decimal())
        red_precio_total = sum((p for _,_,_,_,_,p in items.values()), Decimal())

    return dict(pedido=ipedido,
                pedido_abierto=pedido.abierto,
                nombre_grupo=grupo.nombre,
                fecha_reparto=fecha_reparto,
                fecha_recogida=fecha_recogida,
                usa_plantilla=pedido.usa_plantilla,
                es_propio=(tipo_pedido==TiposPedido.PROPIO),
                es_red=(tipo_pedido==TiposPedido.RED),
                igrupo=igrupo,
                comentarios=db(db.comentarioApeticion.pedido==ipedido) \
                            .select(db.comentarioApeticion.persona,
                                    db.comentarioApeticion.texto),
                totales=totales,
                es_admin=es_admin,
                discrepancias=discrepancias,
                d_explicaciones_discrepancias=d_explicaciones_discrepancias,
                proxy_pedido=ppedido.as_dict(),
                link_enlace_discrepancias=link_enlace_discrepancias,
                peticion_precio_total=peticion_precio_total,
                repartido_precio_total=repartido_precio_total,
                recibido_precio_total=recibido_precio_total,
                red_precio_total=red_precio_total,
                coste_extra=coste_extra,
                formula_coste_extra=formula_coste_extra,
                contabilidad=contabilidad,
                coste_extra_productor=coste_extra_productor,
                formula_coste_extra_productor=pedido.formula_coste_extra_productor,
                pendiente_consolidacion=pedido.pendiente_consolidacion)

@scope.set('admin-grupo')
#@requires_get_arguments(('grupo', db.grupo), ('pedido', db.pedido),
                        # _consistency = ( (db.grupo.id==db.grupoXred.grupo) &
                        #                  (db.productor.grupo==db.grupoXred.red) &
                        #                  (db.pedido.productor==db.productor.id) )
                        # )
@auth.requires_membership('admins_%s'%request.vars.grupo)
def activa_pedido_coordinado():
    from modelos.pedidos import abre_pedidos_en_grupos_y_subgrupos, elimina_proxy_pedido

    igrupo  = int(request.vars.grupo)
    ipedido = int(request.vars.pedido)
    activar = int(request.vars.activar)
    pp = db.proxy_pedido(grupo=igrupo, pedido=ipedido)
    if pp.activo and activar==0 and (not
        db((db.peticion.grupo==igrupo) &
           (db.peticion.cantidad>0) &
           (db.peticion.productoXpedido==db.productoXpedido.id) &
           (db.productoXpedido.pedido==ipedido)).isempty()
        ):
        return dict(status='error',
                    mensaje=T('No hemos desactivado el pedido porque ya había pedido alguna gente. Si estás seguro de querer desactivarlo elimina las peticiones primero.'),
                    activo=1
        )
    if activar and not pp.activo:
        abre_pedidos_en_grupos_y_subgrupos([ipedido], igrupo, fuerza_activo=True)
    elif not activar and pp.activo:
        elimina_proxy_pedido(ipedido, [igrupo], solo_desactivar=True)

    return dict(status = 'ok', activo=activar)

@scope.set('admin-grupo')
#@requires_get_arguments(('grupo', db.grupo), ('pedido', db.pedido),
                        # _consistency = ( (db.grupo.id==db.grupoXred.grupo) &
                        #                  (db.productor.grupo==db.grupoXred.red) &
                        #                  (db.pedido.productor==db.productor.id) )
                        # )
@auth.requires_membership('admins_%s'%request.vars.grupo)
def edita_pedido_coordinado():
    from modelos.pedidos import abre_pedidos_en_grupos_y_subgrupos, elimina_proxy_pedido
    from evalua_coste_extra import chequea_coste_extra_en_form

    igrupo  = int(request.vars.grupo)
    grupo   = db.grupo(igrupo)
    ipedido = int(request.vars.pedido)
    pedido  = db.pedido(ipedido)
    productor = db.productor(pedido.productor)
    if igrupo==productor.grupo:
        redirect(URL(c='gestion_pedidos', f='edita_pedido.html', vars=dict(pedido=ipedido)))
    pprod     = db.proxy_productor(productor=productor.id, grupo=igrupo)
    via       = pprod.via
    nombre_red = via.nombre
    if via.id==productor.grupo.id:
        info_red_productor = productor.info_pedido
    else:
        info_red_productor = db.proxy_productor(productor=productor.id, grupo=via).comentarios_subgrupos
    comentarios = pprod.comentarios if pprod else ''

    response.title = T('Editando pedido de %s')%productor.nombre
    pp = db.proxy_pedido(grupo=igrupo, pedido=ipedido)
    labels = {'comentarios': T('Comentarios locales al pedido'),
              'fecha_reparto': T('Fecha local de reparto'),
              'activo': T('El pedido ha sido aceptado'),
              'formula_coste_extra_pedido': T('Fórmula para repartir el coste fijo del pedido')}
    #son las "keys" de la variable labels, pero en un cierto orden
    fields = ['activo', 'comentarios', 'fecha_reparto', 'formula_coste_extra_pedido']
    activo = pp and pp.activo
    if pp:
        form = SQLFORM(db.proxy_pedido, pp.id, showid=False,
                fields=fields, labels=labels)
    else:
        #por ahora, solo se puede llegar a esta pagina si existe el proxy_pedido,
        #pero dejo esta parte porque podría tener sentido llegar de otra forma
        form = SQLFORM(db.proxy_pedido, showid=False,
                fields=fields, labels=labels)
        form.vars.fecha_reparto = pedido.fecha_reparto
        form.vars.formula_coste_extra_pedido = pprod.formula_coste_extra_pedido

    #TODO: podria prevenir que me cambien la formula local del coste extra de un
    #pedido cerrado, pero no se puede hacer por accidente
    #También podria dejar que cambien la formula, pero daria trabajo conseguir que
    #el precio que pagan las unidades del grupo se recalcule sin que afecte a la
    #red ni a los demas grupos del pedido
    if form.process(onvalidation=chequea_coste_extra_en_form).accepted:
        if not activo and form.vars.activo:
            abre_pedidos_en_grupos_y_subgrupos([ipedido], igrupo)
            session.flash = T('El pedido se ha activado. Ahora elige los productos que se pedirán en tu grupo.')
            redirect(URL(f='aceptar_productos_coordinados.html', vars=dict(grupo=igrupo, pedido=ipedido)))
        elif activo and not form.vars.activo:
            elimina_proxy_pedido(ipedido, [igrupo], solo_desactivar=True)
            session.flash = T('%s no se une a este pedido coordinado')%grupo.nombre
        else:
            session.flash = T('Se han anotado los cambios...')
        redirect(URL(f='gestion_pedidos.html', vars=dict(grupo=igrupo)))
    elif form.errors:
        response.flash = C.MENSAJE_ERRORES

    return dict(edit_form = form,
                comentarios=comentarios,
                pedido=pedido,
                ppedido=pp or pedido,
                nombre_red=nombre_red,
                info_red_productor = info_red_productor,
                grupo=grupo)

#@requires_get_arguments(('grupo', db.grupo), ('unidad', 'integer'), ('fecha_reparto', 'date'))
def ver_peticion_unidad():
    from modelos.pedidos import toda_la_peticion
    from modelos.usuarios import representa_usuario_2
    auth.basic()
    MENSAJE_NO_PERMISO = T('''403: No tienes permiso para descargar este archivo.
Por favor repasa la url, o haz login con permisos suficientes.''')
    igrupo = int(request.vars.grupo)
    grupo  = db.grupo(igrupo)
    fecha_reparto = request.vars.fecha_reparto

    unidad = int(request.vars.unidad)
    if request.vars.persona:
        ipersona0 = int(request.vars.persona)
    elif auth.user and db.personaXgrupo(persona=auth.user.id, grupo=igrupo, unidad=unidad, activo=True):
        ipersona0 = auth.user.id
    else:
        ipersona0 = -1

    ipersonas, totales_personas, tablas, gran_total, total_personal = toda_la_peticion(
        igrupo, fecha_reparto, ipersona0, unidad=unidad
        )
    npersonas = len(ipersonas)

    aviso_email = db.aviso_email(grupo=igrupo, persona=ipersona0, fecha_reparto=fecha_reparto)
    auth_code   = aviso_email.random_string if aviso_email else ''
    if aviso_email and auth_code==request.vars.auth_code:
        pass
    elif not auth.user:
        session.flash = MENSAJE_NO_PERMISO
        redirect(URL(c='default', f='user.html',
            vars=dict(_next=URL(c=request.controller,f=request.function, vars=request.vars))))
    elif (auth.has_membership(role='admins_%d'%igrupo) or
          auth.has_membership(role='miembros_%d'%igrupo)):
        #Observese que cada user tiene permiso para descargar los informes de todos
        #sus compañeros de grupo.
        pass
    else:
        session.flash = MENSAJE_NO_PERMISO
        redirect(URL(c='default', f='user.html'))

    tablas_html = []
    for ipedido, data, total_productos, coste_extra, total in tablas:

        Ncols = (7+2*npersonas)
        w1    = 100/Ncols
        if request.extension=='pdf':
            kwds1 = {'_width': '%d%%'%w1}
            kwds2 = {'_width': '%d%%'%(w1*4)}
        else:
            kwds1 = kwds2 = {}
        ths = [TH(T('Producto'), **kwds2), TH(T('Precio unitario'), **kwds1) ]

        if npersonas>1:
            for ipersona in ipersonas:
                ths.append(TH(representa_usuario_2(ipersona), **kwds1))
                ths.append(TH(T('Coste'), **kwds1))
        else:
            ths.append(TH(T('Cantidad pedida'), **kwds1))
            ths.append(TH(T('Coste'), **kwds1))

        ths.append(TH(T('Cantidad final'), **kwds1))
        ths.append(TH(T('Coste final'), **kwds1))

        trs = []
        hay_incidencias = False
        for pdata in data:
            pnombre, preciou = pdata[0:2]
            tds =  [TD(pnombre), TD( preciou)]
            for j in range(2,len(pdata)-4,2):
                c, p = pdata[j:j+2]
                tds.append(TD(c))
                tds.append(TD(p))
            cantidad_p, precio_p = pdata[-4:-2]
            cantidad_i, precio_i = pdata[-2:]
            if cantidad_i!=cantidad_p:
                hay_incidencias = True
                if request.extension=="pdf":
                    tds.append(TD(TAG.FONT(cantidad_i + '!', _color='#660000')))
                    tds.append(TD(TAG.FONT(precio_i   + '!' , _color='#660000')))
                else:
                    tds.append(TD(cantidad_i, '! ',
                               SPAN( cantidad_p, _class='peticion_no_satisfecha')))
                    tds.append(TD( precio_i + '!' ))
            else:
                tds.append(TD( cantidad_i ))
                tds.append(TD( precio_i ))
            trs.append(TR(*tds))

        ttots = [TH('Totales',_colspan=2)]
        totales_personas_pedido = totales_personas[ipedido]
        for ipersona in ipersonas:
            ttots.append(TD(' ', **kwds1))
            ttots.append(TD(totales_personas_pedido[ipersona], **kwds1))
        ttots.append(TD(' ', **kwds1))

        if hay_incidencias:
            if request.extension=="pdf":
                ttots.append(TD(TAG.FONT(total_productos + '!', _color='#660000', **kwds1)))
            else:
                ttots.append(TD(total_productos, '! ', **kwds1))
        else:
            ttots.append(TD(total_productos, **kwds1))

        tabla_html = TABLE(THEAD(TR(*ths)), TBODY(*trs), TFOOT(TR(*ttots)),
                           _border="1", _align="center", _width="100%")
        tablas_html.append((ipedido, tabla_html, total_productos, coste_extra, total))

    if request.extension=="pdf":
        from fpdf import FPDF, HTMLMixin
        from fpdf.html import HTML2FPDF, hex2dec
        from math import floor

        # define our FPDF class (move to modules if it is reused  frequently)
        class MyHTML2FPDF(HTML2FPDF):
            def output_table_header(self):
                if self.theader:
                    b = self.b
                    x = self.pdf.x
                    self.pdf.set_x(self.table_offset)
                    self.set_style('B',True)
                    for k,border in [(0,'TLR'),(1,'BLR')]:
                        for cell, bgcolor in self.theader:
                            (w,h,txt,_,_,align) = cell
                            self.box_shadow(w,h, bgcolor)
                            if ' ' not in txt:
                                txt += ' '
                            txts = txt.split(' ',1)
                            self.pdf.cell(w,h,txts[k],border,_,align)
                        self.pdf.ln(self.theader[0][0][1])
                        self.pdf.set_x(self.table_offset)
                    self.set_style('B',b)
                    #self.pdf.set_x(x)
                self.theader_out = True

        class MyFPDF(FPDF, HTMLMixin):
            def header(self):
                pass
            def footer(self):
                pass
            def write_html(self, text, font_size):
                "Parse HTML and convert it to PDF"
                h2p = MyHTML2FPDF(self)
                h2p.set_font('',font_size)
                h2p.feed(text)

        pdf=MyFPDF()
        pdf.add_font('DejaVu', '', 'DejaVuSansCondensed.ttf', uni=True)
        pdf.set_font('DejaVu', '', 14)
        # first page:
        pdf.add_page()
        char_width = (15*npersonas + max(chain(
                       [40],
                       (len(pdata[0]) for _, data, _, _, _ in tablas for pdata in data)))
                     )
        font_size  = floor(600/char_width)
        for ipedido,table,total_productos,coste_extra,total in tablas_html:
            for element in [H4(db.pedido(ipedido).productor.nombre),
                            table,
                            P(('Coste extra del pedido:%s. '%(coste_extra or '0')) +
                              ('Total: %s'%total)),
                            TAG.br()]:
                pdf.write_html(str(XML(element, sanitize=False)),font_size )

        pdf.write_html(str(XML(P('Gran total de la unidad: %s'%gran_total), sanitize=False)),font_size )
        if ipersona0>0 and npersonas>1:
            pdf.write_html(str(XML(P('Gran total personal: %s'%total_personal), sanitize=False)),font_size )
        response.headers['Content-Type']='application/pdf'
        return pdf.output(dest='S')
    else:
        return dict(unidad=unidad,
                    tablas=tablas_html,
                    pedidos=[row[0] for row in tablas],
                    gran_total=gran_total,
                    auth_code=auth_code, grupo=igrupo,
                    fecha_reparto = fecha_reparto,
                    persona=ipersona0, total_personal=total_personal,
                    npersonas=npersonas,
                    host=myconf.take('global.dominio'),
                    port=request.env.server_port
                    )

@scope.set('miembro')
#@requires_get_arguments(('grupo', db.grupo), ('fecha_reparto', 'date'))
@auth.requires(auth.has_membership(role='admins_%s'%request.vars.grupo) or
               auth.has_membership(role='miembros_%s'%request.vars.grupo))
def cobro():
    from modelos.pedidos import pedidos_del_dia

    igrupo = int(request.vars.grupo)
    grupo  = db.grupo(igrupo)

    fecha_reparto = K.parse_fecha( request.vars.fecha_reparto)
    response.title = T('Cobro, día %s')%K.format_fecha(fecha_reparto)

    repartos, repartosc = pedidos_del_dia(fecha_reparto, igrupo)
    ipedidos = ','.join('%d'%row['id'] for row in (repartos.as_list()+repartosc.as_list()))

    us = db((db.personaXgrupo.grupo==igrupo) &
            (db.personaXgrupo.activo==True)) \
                .select(db.personaXgrupo.unidad, distinct=True,
                        orderby=db.personaXgrupo.unidad)
    unidades = [row.unidad for row in us]
    return dict(unidades=unidades, pedidos=ipedidos,
                 grupo=igrupo, fecha_reparto=fecha_reparto)



############# Seccion informes ##########################
#########################################################

@scope.set('miembro')
#@requires_get_arguments(('grupo', db.grupo), ('fecha_reparto', 'date'))
@auth.requires(auth.has_membership(role='admins_%s'%request.vars.grupo) or
               auth.has_membership(role='miembros_%s'%request.vars.grupo))
def ver_informes():
    from modelos.pedidos import pedidos_del_dia
    igrupo = int(request.vars.grupo)
    grupo  = db.grupo(igrupo)
    if grupo.red:
        tipos_informe = C.TIPOS_INFORME_RED
        tooltips_informe = C.TOOLTIPS_INFORME_RED
        informe_defecto = 3
    else:
        tipos_informe = C.TIPOS_INFORME_GRUPO
        tooltips_informe = C.TOOLTIPS_INFORME_GRUPO
        informe_defecto = 0

    fecha_reparto = K.parse_fecha( request.vars.fecha_reparto)

    # ver si hay parametros de informe:
    all_pedidos = True
    tallas = ['XS','S','M','L']
    if request.vars.tipo_informe:
        informe_defecto = int(request.vars.tipo_informe)
    if request.vars.talla:
        talla_defecto= request.vars.talla
    else:
        talla_defecto = 'M'
    if request.vars.apaisado:
        apaisado = request.vars.apaisado
        all_pedidos = False
    else:
        apaisado = 'True'

    pedidosOn = {}
    for k,v in request.vars.items():
        if k.startswith('pedido_') and v=='on':
            _,pedido = k.split('_')
            pedidosOn[int(pedido)]=1
            all_pedidos=False

    response.title = T('Informes de %s para el %s')%(db.grupo(igrupo).nombre,
                                                     K.format_fecha(fecha_reparto) )
    repartos, repartosc = pedidos_del_dia(fecha_reparto, igrupo)
    todos_pedidos = [p.id for p in chain(repartos, repartosc)]
    pedidos_con_peticion = set()
    for t in (db.peticion, db.item, db.item_grupo):
        q_hay_pedido = ((t.productoXpedido==db.productoXpedido.id) &
                        (t.cantidad>0) &
                        (db.productoXpedido.pedido.belongs(todos_pedidos)) &
                        (db.productoXpedido.pedido==db.pedido.id))
        if not grupo.red:
            q_hay_pedido &= (t.grupo==igrupo)
        for row in db(q_hay_pedido).select(
             db.pedido.id, groupby=db.pedido.id):
             pedidos_con_peticion.add(row.id)
    # fix 3.2.15
    q_coste_extra = ((db.coste_pedido.pedido.belongs(todos_pedidos)) &
                    (db.coste_pedido.coste>0))
    if not grupo.red:
        q_coste_extra &= (db.coste_pedido.grupo==igrupo)
    for row in db(q_coste_extra).select(
        db.coste_pedido.pedido, groupby=db.coste_pedido.pedido):
        pedidos_con_peticion.add(row.pedido)

    form = FORM(DIV(
                 DIV(LABEL(T('Tipo de informe: '),_for='tipo_informe')),
                 DIV(*[DIV(
                    INPUT(_value=k,value=informe_defecto,_name='tipo_informe',_type='radio'),
                    SPAN(_class='glyphicon glyphicon-ok'),
                    SPAN(' '),
                    SPAN(v),
                    _class='btn btn-default' +(' active' if k==informe_defecto else ''),
                    **{'_data-toggle':'tooltip', '_data-placement':'bottom','_title':tooltips_informe[k]}
                        )
                          for k,v in tipos_informe.items()],
                     **{'_class':'btn-group cool_radio', '_data-toggle':'buttons'}),
                    _class='form-group'),
                DIV(LABEL(T('Impresión'), _style='padding-right:4px'),
                        DIV(
                            DIV(
                                INPUT(_value='False', value=apaisado,_name='apaisado',_type='radio'),
                                SPAN(_class='glyphicon glyphicon-ok'),
                                SPAN(T('Vertical')),
                                _class='btn btn-default '+(' active' if apaisado=='False' else '')),
                            DIV(
                                INPUT(_value='True', value=apaisado,_name='apaisado',_type='radio'),
                                SPAN(_class='glyphicon glyphicon-ok'),
                                SPAN(' '),
                                SPAN(T('Apaisado')),
                                _class='btn btn-default'+(' active' if apaisado=='True' else '')),
                            **{'_class':'btn-group cool_radio', '_data-toggle':'buttons'}),
                    _class='form-group'),
                DIV(DIV(LABEL(T('Tamaño '),
                        TAG.i(**{'_class':'glyphicon glyphicon-info-sign',
                                 '_data-toggle':'tooltip',
                                 '_data-placement':'right',
                                 '_title':T('El informe L tiene el tipo de letra más grande, el XS tiene el tipo de letra más pequeño.'
                                            'Ajustaremos el número de columnas para aprovechar el espacio, así que algunos de los informes de distinto tamaño pueden ser iguales.')})),
                    DIV(*[DIV(
                       INPUT(_value=k,value=talla_defecto,_name='talla',_type='radio'),
                       SPAN(_class='glyphicon glyphicon-ok'),
                       SPAN(' '),
                       SPAN(k),
                       _class='btn btn-default' +(' active' if k==talla_defecto else '')
                       ) for k in tallas],
                       **{'_class':'btn-group cool_radio', '_data-toggle':'buttons'}),
                       _class='form-group')),
                DIV(DIV(LABEL(T('Productor')),
                        TAG.i(**{'_class':'glyphicon glyphicon-info-sign',
                                 '_data-toggle':'tooltip',
                                 '_data-placement':'right',
                                 '_title':T('Se muestran solo los productores con peticiones.'
                                            'Un * indica que el pedido sigue abierto.')}),
                        TABLE(
                               *[TR(TD(DIV(DIV(
                                       INPUT( _name='pedido_%d'%r.id, _type='checkbox', value=(True if all_pedidos or r.id in pedidosOn else False)),
                                       SPAN(_class='glyphicon glyphicon-ok'),
                                       SPAN(' '),
                                       SPAN(r.productor.nombre + ('  *' if r.abierto else '')),
                                       _class='btn btn-default '+(' active' if all_pedidos or r.id in pedidosOn else '')),
                                       **{'_class':'btn-group cool_checked', '_data-toggle':'buttons'}))) for r in chain(repartos, repartosc) if r.id in pedidos_con_peticion]
                                   ,_class='tabla_productor_cool')
                    ),_class='form-group'),
                DIV(
                    INPUT(_type='submit', _name='submit', value='Enviar', _class='btn btn-success'),
                    A(T('Volver a gestión de pedidos'),
                      _href=URL(c='gestion_pedidos', f='gestion_pedidos.html', vars=dict(grupo=igrupo)),
                      _class='btn btn-primary'),
                    _class='btn-group'
                     )
        )
    pedidos_sin_peticion = [r.productor.nombre for r in chain(repartos, repartosc)
                            if (r.id not in pedidos_con_peticion)]
    tabla_pedidos_sin_peticion = TABLE(
       *[TR(TD(DIV(DIV(
               SPAN(_class='glyphicon glyphicon-remove'),
               SPAN(' '),
               SPAN(nombre_productor),
               _class='btn btn-warning',
               _disabled='disabled',
             ), **{'_class':'btn-group cool_disabled', '_data-toggle':'buttons'})))
             for nombre_productor in pedidos_sin_peticion],
        _class='tabla_productor_cool'
    ) if pedidos_sin_peticion else ''

    if form.accepts(request.vars, session, keepvalues=True):
        ipedidos = []
        for k,v in form.vars.items():
#            if 'pedido_' in k:
            if k.startswith('pedido_') and v=='on':
                _,pedido = k.split('_')
                ipedidos.append(int(pedido))
        tipo_informe = int(form.vars.tipo_informe)
        apaisado = True if form.vars.apaisado=='True' else False
        #apaisado = form.vars.apaisado
        if ipedidos and tipo_informe>=0:
            response.flash= T('Generando informe para descargar.')
            response.js =  (';RouterPlugin.navTo("%s");'%
                URL(c='gestion_pedidos', f='informe', extension='pdf',
                     vars=dict(apaisado='1' if apaisado==True else '0',
                               pedidos=','.join(str(ipedido) for ipedido in ipedidos),
                               tipo=tipo_informe,talla=form.vars.talla,
                               fecha_reparto=fecha_reparto, grupo=igrupo)) )
        else:
            if tipo_informe==-1:
                form.errors.tipo_informe = T('Elige un tipo de informe.')
                response.flash = T('Elige un tipo de informe.')
            else:
                response.flash = T('Por favor selecciona al menos un pedido.')
#    elif form.errors:
#        #Como podria haber errores con un desplegable y varios chekboxes?
#        raise Exception, form.errors

    return dict(repartos=form, tabla_pedidos_sin_peticion=tabla_pedidos_sin_peticion)

#@requires_get_arguments(('grupo', db.grupo), ('fecha_reparto', 'date'),
#                         pedidos, apaisado, talla, tipo)
@auth.requires(auth.has_membership(role='admins_%s'%request.vars.grupo) or
               auth.has_membership(role='miembros_%s'%request.vars.grupo))
def informe():
    from modelos.pedidos import pedidos_del_dia
    igrupo = int(request.vars.grupo)
    ipedidos = [int(pedido) for pedido in request.vars.pedidos.split(',')]
    fecha_reparto = request.vars.fecha_reparto
    talla = request.vars.talla
    apaisado = (request.vars.apaisado=='1')
    if apaisado:
        #fix 3.2.18: ncolumnas = {'XS':40, 'S':30, 'M':20, 'L':15}[talla]
        ncolumnas = {'XS':27, 'S':24, 'M':20, 'L':15}[talla]
    else:
        #fix 3.2.18: ncolumnas = {'XS':20, 'S':18, 'M':14, 'L':10}[talla]
        ncolumnas = {'XS':17, 'S':15, 'M':14, 'L':10}[talla]
    tipo_informe = int(request.vars.tipo)
    #Comprobamos que los pedidos son del grupo o de una red a la que pertenece el grupo
    #Si selecciona pedidos de un grupo del que no es miembro o admin, no podra ver lo
    #que han pedido, pero podria ver algunos datos que no deberia ver
    repartos, repartosc = pedidos_del_dia(fecha_reparto, igrupo, devuelve_solo_ids=True)
    ipedidos_grupo_fecha = set(p.id for p in chain(repartos, repartosc))
    if (not ipedidos
        or (any(ipedido not in ipedidos_grupo_fecha for ipedido in ipedidos))
        ):
        #Ponemos todos los pedidos del grupo en esa fecha
        ipedidos = ipedidos_grupo_fecha
    from informes import generar_informe
    rutapdf, ruta_completa = generar_informe(
        tipo_informe, ipedidos, fecha_reparto, igrupo,
        apaisado=apaisado, columnas_por_tabla=ncolumnas
    )
    response.headers['Content-Type'] = 'application/x-pdf'
    return response.stream(ruta_completa,
            4000,
            attachment=True,
            filename=rutapdf
            )


def get_func_nombre_producto(obligatorias):
    if obligatorias:
        def func_nombre_producto(nombre, pesar, hay_discrepancia_red, hay_discrepancia_interna, cantidad_recibida):
            if hay_discrepancia_red:
                return '%s (** %s)'%(nombre, cantidad_recibida)
            if hay_discrepancia_interna:
                return '%s (** %s)'%(nombre, cantidad_recibida)
            if cantidad_recibida:
                return '%s (%s)'%(nombre, cantidad_recibida)
            return nombre
    else:
        def func_nombre_producto(nombre, pesar, hay_discrepancia_red, hay_discrepancia_interna, cantidad_recibida):
            if hay_discrepancia_red:
                asteriscos = '**'
            elif pesar:
                asteriscos = '*'
            else:
                asteriscos = ''
            formato = '%s'
            #se muestra entre paréntesis la cantidad recibida, excepto si
            #cantidad=cantidad_recibida=cantidad_red=0
            if cantidad_recibida or hay_discrepancia_red or hay_discrepancia_interna:
                nombre_producto = '%s (%s %s)'%(nombre, asteriscos, cantidad_recibida)
            else:
                nombre_producto = '%s %s'%(nombre, asteriscos)
            if hay_discrepancia_red or hay_discrepancia_interna or pesar:
                return XML('<strong>%s</strong>'%nombre_producto, sanitize=True)
            return nombre_producto
    return func_nombre_producto

@scope.set('admin-grupo')
#@requires_get_arguments(('pedido', db.pedido))
@auth.requires(Permisos.autoriza_pedido(request.vars))
def incidencias():
    from hotsheet import HOTSheet
    from modelos.pedidos import query_mask, suma_item_grupo, suma_item_grupo_red,\
                                recalcula_costes_extra_pedido, grupo_ha_consolidado, \
                                hay_discrepancia_interna, hay_discrepancia_descendiente

    tppp = db.productoXpedido
    tig = db.item_grupo

    ipedido = int(request.vars.pedido)
    pedido  = db.pedido(ipedido)
    grupo_productor = pedido.productor.grupo
    obligatorias = bool(request.vars.obligatorias)
    todas = bool(request.vars.todas)

    if pedido.abierto:
        session.flash = T('El pedido todavía no está cerrado, así que no puedes resolver incidencias, pero puedes editar las peticiones, cambiando el pedido de las personas del grupo.')
        redirect(URL(f='editar_peticiones.html', vars=request.vars))
    if 'grupo' in request.vars:
        igrupo = int(request.vars.grupo)
        grupo  = db.grupo[igrupo]
        coordinado = (grupo_productor.id !=igrupo)
    else:
        grupo  = grupo_productor
        igrupo = grupo.id
        coordinado = False
    grupoesred = grupo.red
    ppedido = db.proxy_pedido(pedido=ipedido, grupo=igrupo) if coordinado else pedido

    response.title = T('Resolviendo incidencias para el %s')%represent(db.pedido, pedido)

    #rows: productos
    q_productos = ((tppp.pedido==ipedido) &
    # si hay petición de un producto fuera de temporada, hay que mostrarla
    # caso de uso: marcar el producto como fuera de temporada cuando se acaba el stock, pero
    #              manteniendo la petición ya hecha.
#                   (tppp.temporada==True) &
#                   (tppp.activo==True) &
                   (db.categorias_productos.id==tppp.categoria)
    )
    if coordinado:
        q_productos &= query_mask(igrupo)

    if obligatorias:
        q_productos &= ((tig.productoXpedido==tppp.id) &
                        (tig.grupo==igrupo) &
                        ((tig.cantidad>0) |
                         (tig.cantidad_red>0) |
                         (tig.cantidad_recibida>0)) &
                        ((tppp.pesar == True) |
                         (tig.cantidad_red != tig.cantidad_recibida)))

        lefton = None
    elif todas:
        # Queremos mostrar todos los productos,
        # aunque no tengan entrada en db.item_grupo => hacemos un left join
        lefton = tig.on( (tig.grupo==igrupo) &
                         (tig.productoXpedido==tppp.id) )
    else:
        q_productos &= ((tig.productoXpedido==tppp.id) &
                        (tig.grupo==igrupo) &
                        ((tig.cantidad>0) |
                         (tig.cantidad_red>0) |
                         (tig.cantidad_recibida>0)) )
        lefton = None

    ls_productos = [(row.productoXpedido.nombre, row.productoXpedido.id, row.productoXpedido.pesar,
                    (row.item_grupo.cantidad_recibida != row.item_grupo.cantidad_red),
                    (row.item_grupo.cantidad_recibida != row.item_grupo.cantidad),
                    row.item_grupo.cantidad_recibida)
                    for row in db(q_productos).select(
                        tppp.id, tppp.nombre, tppp.pesar,
                        tig.cantidad_recibida, tig.cantidad_red, tig.cantidad,
                        left=lefton
                    )]

    #Es necesario mantener el orden y evitar repeticiones...
    #TODO: estaria bien usar el mismo orden de productos, primero por nombre de categoria y
    #luego por nombre de producto... lo marco para mas adelante
    d_productos = dict((nombre,(id,pesar,hay_discrepancia_red,hay_discrepancia_interna,cantidad_recibida) )
        for nombre,id,pesar,hay_discrepancia_red,hay_discrepancia_interna,cantidad_recibida in ls_productos)
    #Duda: ¿¿¿ ordenar por nombre o por categoría ??? supongo que si no muestro las categorías
    # es mejor por nombre
    ls_productos = sorted(
        (nombre,id,pesar,hay_discrepancia_red,hay_discrepancia_interna,cantidad_recibida)
        for (nombre,(id,pesar,hay_discrepancia_red,hay_discrepancia_interna,cantidad_recibida) )
        in d_productos.items()
    )

    row_ids = [id for _,id,_,_,_,_ in ls_productos]

    hay_que_pesar = (
        (not coordinado and pedido.pendiente_consolidacion) or
        (coordinado and db.proxy_pedido(grupo=igrupo, pedido=ipedido).pendiente_consolidacion)
    ) and any(pesar  for _,_,pesar,_,_,_ in ls_productos)
    hay_discrepancias = (
        any((hay_discrepancia_red or hay_discrepancia_interna) for _,_,_,hay_discrepancia_red,hay_discrepancia_interna,_ in ls_productos)
    )
    algunas_obligatorias = (
        (not obligatorias) and
        any(pesar for _,_,pesar,_,_,_ in ls_productos)
    )

    func_nombre_producto = get_func_nombre_producto(obligatorias)
    row_headers = [func_nombre_producto(
        nproducto, pesar, hay_discrepancia_red,hay_discrepancia_interna,cantidad_recibida
        ) for nproducto,_,pesar,hay_discrepancia_red,hay_discrepancia_interna,cantidad_recibida in ls_productos
    ]
    if grupoesred:
        #cols son grupos
        query_hijos = ((db.proxy_pedido.pedido==ipedido) &
                        (db.proxy_pedido.activo==True) &
                        (db.proxy_pedido.grupo==db.grupo.id) &
                        (db.grupoXred.activo==True) &
                        (db.grupoXred.grupo==db.grupo.id) &
                        (db.grupoXred.red==igrupo))
        ls_grupos = [(row.grupo.id, row.grupo.nombre, row.proxy_pedido.discrepancia_ascendiente)
                        for row in db(query_hijos)
                            .select(db.grupo.id, db.grupo.nombre,
                                    db.proxy_pedido.discrepancia_ascendiente,
                                    orderby=db.grupo.nombre)]
        col_ids = [id for id,_,_ in ls_grupos]
        col_headers = [ngrupo for _,ngrupo,_ in ls_grupos]
        #data: item_grupo
        query = ((db.item_grupo.productoXpedido==tppp.id) &
                 (db.item_grupo.grupo==db.grupoXred.grupo) &
                 (db.item_grupo.cantidad_red>0) &
                 (db.grupoXred.red==igrupo) &
                 (db.grupoXred.activo==True) &
                 (tppp.id.belongs(row_ids)) )
        tabla = db.item_grupo
        campo_cantidad = tabla.cantidad_red
        field_cols = tabla.grupo
        col_headers
    else:
        #cols: unidades
        unidades = [row.unidad for row in
                    db((db.personaXgrupo.grupo==igrupo) &
                       (db.personaXgrupo.activo==True)) \
                    .select(db.personaXgrupo.unidad,
                            groupby=db.personaXgrupo.unidad,
                            orderby=db.personaXgrupo.unidad)]
        col_ids = col_headers = unidades
        #data: item
        query = ((db.item.productoXpedido==tppp.id) &
                 (db.item.grupo==igrupo) &
                 (tppp.id.belongs(row_ids)) )
        tabla = db.item
        campo_cantidad = tabla.cantidad
        field_cols = tabla.unidad

    sheet = HOTSheet(query,
                    tabla.productoXpedido,
                    field_cols,
                    campo_cantidad,
                    col_ids=col_ids,
                    col_headers=col_headers,
                    row_ids=row_ids,
                    row_headers=row_headers,
                    #extra_values se rellena con el campo grupo si es un grupo,
                    #pero no si es una red, porque toma el valor de la columna
                    extra_values={} if grupoesred else {'grupo':igrupo},
                    col_totals=True,
                    submit_buttons = [
                       ('asumir', T('Enviar, pero mantener la cantidad recibida'), 'success'),
                       ('recalcular', T('Enviar y recalcular la cantidad recibida'), 'success')
                    ]
                    )

    if sheet.accepts(request.vars):
        # aunque no haya cambiado nada, si guarda, nos aseguramos de establecer los
        # chivatos de estado correctamente
        algun_cambio =  not sheet.empty_override
        #if sheet.empty_override:
        #    session.flash = T('¡No has hecho ningún cambio!')
        #elif not sheet.discard:
        if not sheet.discard:
            if algun_cambio:
                if not grupoesred:
                    suma_item_grupo([ipedido], igrupo)
                else:
                    suma_item_grupo_red([ipedido], igrupo)
            if sheet.button_submitted=='recalcular':
                # movemos cantidad a cantidad_recibida
                db((db.item_grupo.grupo==igrupo) &
                    db.item_grupo.productoXpedido.belongs(row_ids)) \
                .update(cantidad_recibida=db.item_grupo.cantidad)
            # fijar los chivatos discrepancia_interna, discrepancia_ascendiente etc
            (di, da, dd) = grupo_ha_consolidado(ipedido, igrupo, hacia_arriba=(sheet.button_submitted=='recalcular'))
            if algun_cambio:
                incidencias_coste_pedido = recalcula_costes_extra_pedido(ipedido, igrupo)
                if incidencias_coste_pedido:
                    session.flash=T('El pedido de %s ha sido actualizado, pero ha habido problemas al calcular los costes extra del pedido')%pedido.productor.nombre
                    redirect(URL(f='vista_pedido.html',vars=dict(pedido=ipedido, grupo=grupo.id, recalcula_costes_extra=1)))
                if da or di:
                    # - si no es (cantidad==cantidad_recibida==cantidad_red), reenviamos a
                    # incidencias_globales con un session.flash
                    session.flash = T('Hay diferencias entre la cantidad repartida y la recibida, por favor resolvedlas.')
                    redirect(URL(f='incidencias_globales.html',vars=dict(pedido=ipedido, grupo=grupo.id)))
                session.flash=T('El pedido de %s ha sido actualizado')%pedido.productor.nombre
        redirect(URL(f='vista_pedido.html',vars=dict(pedido=ipedido, grupo=grupo.id)))

    return dict(sheet=sheet,
                grupoesred=grupoesred,
                nombregrupo=grupo.nombre,
                solo_obligatorias=obligatorias,
                todas=todas,
                algunas_obligatorias=algunas_obligatorias,
                hay_que_pesar=hay_que_pesar,
                hay_discrepancias=hay_discrepancias)

@scope.set('admin-grupo')
#@requires_get_arguments(('pedido', db.pedido))
@auth.requires(Permisos.autoriza_pedido(request.vars))
def editar_peticiones():
    from hotsheet import HOTSheet
    from modelos.usuarios import representa_usuario_3
    from modelos.pedidos import peticion_a_item, query_mask
    tppp = db.productoXpedido

    ipedido = int(request.vars.pedido)
    pedido  = db.pedido(ipedido)
    grupo_productor = pedido.productor.grupo

    if 'grupo' in request.vars:
        igrupo = int(request.vars.grupo)
        grupo  = db.grupo[igrupo]
        coordinado = (grupo_productor.id !=igrupo)
    else:
        grupo  = grupo_productor
        igrupo = grupo.id
        coordinado = False
    if grupo.red:
        response.flash = DIV(T('%s es una red, no un grupo.')%grupo.nombre, TAG.br(),
                             T('No puedes editar los pedidos de las personas que están en los grupos que pertenecen a %s.')%grupo.nombre, TAG.br(),
                             T('¿Qué querías hacer?'), TAG.br(),
                             T('Si necesitas algo, por favor escribe a info@karakolas.org y explícanoslo.'))

    if not db.pedido[ipedido].abierto:
        response.flash = DIV(T('El pedido ya está cerrado.'),
                            T(' ¿estás seguro de que quieres modificar la petición de las personas en vez de resolver incidencias?'),
                            T('Entra en el menú "Gestión de pedidos y podrás acceder al link para resolver incidencias"'))
    response.title = T('Editando las peticiones para el %s')%represent(db.pedido, pedido)

    #rows
    query_productos = ((tppp.pedido==ipedido) &
                       (tppp.temporada==True) &
                       (tppp.activo==True) &
                       (db.categorias_productos.id==tppp.categoria))
    if coordinado:
        query_productos &= query_mask(igrupo)
    ls_productos = [(row.id, row.nombre)
                    for row in db(query_productos)
                        .select(tppp.id, tppp.nombre,
                                orderby=db.categorias_productos.nombre|tppp.nombre)]
    row_ids = [id for id,_ in ls_productos]
    row_headers = [nproducto for _,nproducto in ls_productos]

    #cols
    ls_miembros = [(row.persona, representa_usuario_3(row.persona, row.unidad, grupo=igrupo)) for row in
                 db((db.personaXgrupo.grupo==igrupo) &
                    (db.personaXgrupo.activo==True))\
                  .select(db.personaXgrupo.persona,
                          db.personaXgrupo.unidad,
                          orderby=db.personaXgrupo.unidad)]
    col_ids = [id for id,_ in ls_miembros]
    col_headers = [m for _,m in ls_miembros]

    #data
    query = ((db.peticion.productoXpedido==tppp.id) &
             (tppp.temporada==True) &
             (tppp.activo==True) &
             (tppp.pedido==ipedido) &
             (db.peticion.persona==db.auth_user.id) &
             (db.peticion.grupo==igrupo))
    if coordinado:
        query &= query_mask(igrupo)

    sheet = HOTSheet(query,
                     db.peticion.productoXpedido,
                     db.peticion.persona,
                     db.peticion.cantidad,
                     col_ids=col_ids,
                     col_headers=col_headers,
                     row_ids=row_ids,
                     row_headers=row_headers,
                     col_totals=True,
                     extra_values={'grupo':igrupo},
                     records_per_page=20
                    )
    if sheet.accepts(request.vars):
        if sheet.empty_override:
            session.flash = T('¡No has hecho ningún cambio!')
        elif not sheet.discard:
            peticion_a_item(ipedido, igrupo)
            session.flash=T('El pedido de %s ha sido actualizado')%db.pedido(ipedido).productor.nombre
        redirect(URL(f='vista_pedido.html',vars=dict(pedido=ipedido, grupo=grupo.id)))
    return dict(sheet=sheet)

#TODO: scope puede ser admin-grupo o admin-red
# si request.vars.desdered es vacío, entonces puede ser admin-grupo o admin-red
# si request.vars.desdered es no vacío, entonces tiene que ser admin-red
# no es especialmente importante
@scope.set('admin-grupo')
#@requires_get_arguments(('pedido', db.pedido))
@auth.requires_membership('admins_%s'%(request.vars.red if request.vars.desdered
                                       else request.vars.grupo))
def incidencias_globales():
    '''Anota las cantidades que un grupo/red declara haber recibido de la red superior.

    Las cantidades que una red declara haber repartido a la red inferior se pueden anotar en
    incidencias para todos los grupos a la vez, o desde aquí pasando el argumento desdered=1.

    Si el grupo es la red/grupo primigenia, sirve para anotar la cantidad recibida del productor,
    tanto lo que declara el productor como lo que declara la red/grupo

    - obligatorias=1 => solo las obligatorias (productos con pesar=True de los que se ha pedido)
    - todos=1 => incluir todos los productos en el pedido, aunque no se hayan pedido ni pollas
    - por defecto => todos los productos que se han pedido
    '''
    #TODO: redderedes indica el precio unitario del grupo y el de la red, que pueden ser totalmente distintos,
    # por ejemplo si se ha usado ajusta_precio_para_compensar_discrepancias
    from modelos.pedidos import recalcula_costes_extra_pedido, via_pedido_en_grupo, \
                                hay_discrepancia_ascendiente, hay_discrepancia_interna, \
                                comprueba_pedido_pendiente_consolidacion, grupo_ha_consolidado, \
                                suma_item_grupo_red

    tppp = db.productoXpedido
    tig  = db.item_grupo

    igrupo = int(request.vars.grupo)
    grupo  = db.grupo[igrupo]
    grupoesred = grupo.red
    desdered = request.vars.desdered

    ipedido = int(request.vars.pedido)
    pedido  = db.pedido(ipedido)
    productor = pedido.productor
    grupo_productor  = productor.grupo
    igrupo_productor = grupo_productor.id
    if desdered:
        via = int(request.vars.red)
    else:
        via = via_pedido_en_grupo(ipedido, igrupo)
    red_via = db.grupo(via)
    propio = (igrupo==igrupo_productor)
    ppedido = pedido if propio else db.proxy_pedido(pedido=ipedido, grupo=igrupo)

    nombre_grupo = grupo.nombre
    nombre_red = productor.nombre if propio else red_via.nombre

    obligatorias = bool(request.vars.obligatorias)
    todas = bool(request.vars.todas)

    if db.pedido[ipedido].abierto:
        session.flash = T('El pedido todavía no está cerrado, así que no puedes resolver las incidencias.')
        redirect(URL(c='gestion_pedidos', f='gestion_pedidos.html', vars=dict(grupo=igrupo)))

    response.title = T('Anotar las incidencias globales para el %s')%represent(db.pedido, pedido)

    q_productos = ((tppp.pedido==ipedido) &
    # si hay petición de un producto fuera de temporada, hay que mostrarla
    # caso de uso: marcar el producto como fuera de temporada cuando se acaba el stock, pero
    #              manteniendo la petición ya hecha.
#                   (tppp.temporada==True) &
#                   (tppp.activo==True) &
                   (db.categorias_productos.id==tppp.categoria))
    if propio:
        field_precio = tppp.precio_productor
    else:
        q_productos &= ((db.mascara_producto.productoXpedido == tppp.id) &
                        (db.mascara_producto.grupo==igrupo))
        field_precio = db.mascara_producto.precio_red

    if obligatorias:
        q_productos &= ((tig.grupo==igrupo) &
                        (tig.productoXpedido==tppp.id) &
                        ((tig.cantidad>0) |
                         (tig.cantidad_red>0) |
                         (tig.cantidad_recibida>0)) &
                        ((tppp.pesar == True) |
                         (tig.cantidad_red != tig.cantidad_recibida)))

        lefton = None
    elif todas:
        # Queremos mostrar todos los productos,
        # aunque no tengan entrada en db.item_grupo => hacemos un left join
        lefton = tig.on( (tig.grupo==igrupo) &
                         (tig.productoXpedido==tppp.id) )
    else:
        q_productos &= ((tig.grupo==igrupo) &
                        (tig.productoXpedido==tppp.id) &
                        ((tig.cantidad>0) |
                         (tig.cantidad_red>0) |
                         (tig.cantidad_recibida>0)) )
        lefton = None

    ls = [ (r[tppp.id],
            r[tppp.nombre],
            r[field_precio],
            r[tig.cantidad_red] or Decimal(),
            r[tig.precio_red] or Decimal(),
            r[tig.cantidad] or Decimal(),
            r[tig.precio_total] or Decimal(),
            r[tig.cantidad_recibida] or Decimal(),
            r[tppp.pesar])
           for r in db(q_productos) \
                    .select(tppp.id, tppp.nombre, tppp.pesar,
                            tig.cantidad_red, tig.cantidad_recibida, tig.cantidad,
                            tig.precio_red, tig.precio_total,
                            field_precio,
                            left=lefton,
                            orderby=db.categorias_productos.nombre|tppp.nombre)
        ]
    precio_acordado_total = sum(precio_red for _,_,_,_,precio_red,_,_,_,_ in ls)

    # Solo marca los productos a pesar con un asterisco si el pedido está pendiente de consolidación
    algunas_obligatorias = (
        (not obligatorias) and
        ppedido.pendiente_consolidacion and
        any(pesar for _,_,_,_,_,_,_,_,pesar in ls)
    )
    if algunas_obligatorias:
        nombre_producto = lambda pnombre, pesar: STRONG(pnombre + ' (*)') if pesar else pnombre
    else:
        nombre_producto = lambda pnombre, pesar: pnombre

    if ls:
        thead = TR(TH(T('Producto')),
                   TH(T('Precio unitario'), _class='precio_unitario'),
                   TH(T('Cantidad según ') + nombre_red),
                   TH(T('Precio según ') + nombre_red, _class='precio_producto_red'),
                   TH(T('Cantidad según ') + nombre_grupo),
                   TH(T('Precio según ') + nombre_grupo, _class='precio_producto_grupo'),
        )
        tbodyrows = []
        for pppid, pnombre, pprecio, cantidad_red, precio_red, \
              cantidad, preciot, cantidad_recibida, pesar in ls:
            row = TR(
               TD(nombre_producto(pnombre, pesar)),
               TD(pprecio, _id='precio_unitario_%d'%pppid, _class='precio_unitario'),
               TD(INPUT(_value=cantidad_red, _name='cantidad_red_%d'%pppid,
                        _id='cantidad_red_%d'%pppid, _class='cantidad decimal',
                        requires=db.item.cantidad.requires)) if propio or desdered
               else TD(cantidad_red, _id='cantidad_red_%d'%pppid),
               TD(precio_red, _id='precio_producto_red_%d'%pppid, _class='precio_producto_red'),
               TD(INPUT(_value=cantidad_recibida, _name='cantidad_grupo_%d'%pppid,
                        _id='cantidad_grupo_%d'%pppid, _class='cantidad decimal',
                        requires=db.item.cantidad.requires)) if not desdered
               else TD(cantidad_recibida, _id='cantidad_grupo_%d'%pppid),
               TD('...', _id='precio_producto_%d'%pppid, _class='precio_producto_grupo'),
                   )
            if not desdered:
                row.append(TD(cantidad, _id='cantidad_repartida_%d'%pppid)  )
                row.append(TD(preciot, _id='precio_repartido_%d'%pppid)  )
            tbodyrows.append(row)
        tfoot = TR(TH(T('Totales')), TD(), TD(),
                 TD(SPAN(precio_acordado_total, _id='precio_total_red', _class='destacado')),
                 TD(),
                 TD(SPAN('...', _id='precio_total'), _class='destacado') )
        if not desdered:
            thead.append(TH(T('Cantidad repartida')) )
            thead.append(TH(T('A ingresar de los grupos') if grupoesred else  T('A pagar por las unidades')) )
            tfoot.append(TD())
            tfoot.append(TD())

        form = FORM(
            DIV(TABLE(THEAD(thead), TBODY(*tbodyrows), TFOOT(tfoot)), _class="table-responsive"),
            DIV(
            INPUT(_type='submit', _value=T('Enviar'), _class='btn btn-primary', _id='submit'),
            INPUT(_type='submit', _value=T('Cancelar'), _class='btn btn-warning', _id='discard', _name='discard', _onclick="$('#comando').val('discard');"),
            _class='btn-group'),
            INPUT(_type='hidden', _id='ids_productos',
                  _value=','.join([str(pppid) for pppid,*rest in ls])),
            INPUT(_type='hidden', _id='comando', _name='comando', _value=''),
            _id='incidencias_globales'
        )
        vacio = False
    else:
        form = None
        vacio = True

    if form and form.process().accepted:
        if form.vars.comando=='discard':
            session.flash = T('Cambios descartados')
            redirect(URL(f='gestion_pedidos.html',vars=dict(grupo=via if desdered else igrupo)))

        # TODO: (eficiencia) podriamos procesar cantidad_recibida y cantidad_red de una pasada?
        # a lo mejor incluso hacer un delete unico y luego un unico insert ?
        # TODO: (eficiencia) si cantidad=cantidad_recibida=cantidad_red=0, es mejor borrar el item_grupo ?
        #       pero hay que pensar si es lo mejor, si queda petición... posiblemente no
        #       compensa el esfuerzo
        if not desdered:
            for k,v in form.vars.items():
                v = v or Decimal()
                if k.startswith('cantidad_grupo_'):
                    pppid = int(k[15:])
                    tig.update_or_insert(
                        (tig.grupo==igrupo) & (tig.productoXpedido==pppid),
                        grupo=igrupo, productoXpedido=pppid, cantidad_recibida=v)
        if propio or desdered:
            for k,v in form.vars.items():
                if k.startswith('cantidad_red_'):
                    pppid = int(k[13:])
                    tig.update_or_insert(
                        (tig.grupo==igrupo) & (tig.productoXpedido==pppid),
                        grupo=igrupo, productoXpedido=pppid, cantidad_red=v)
        if desdered:
            suma_item_grupo_red([ipedido], via)


        # discrepancia_ascendiente y discrepancia_interna se comprueban igual independientemente
        #  de si es desdered o no, y también del botón que hayan pulsado
        # discrepancia_interna_asumida lo fijan las funciones
        #  ajusta_precio_para_compensar_discrepancias o reparto_redondeo
        grupo_ha_consolidado(ipedido, igrupo, hacia_abajo=False, hacia_arriba=True)
        if desdered:
            # Si la red cambia cantidad_red de item_grupo(igrupo, pxp), entonces también
            #  cambia cantidad de item_grupo(via, pxp), lo que puede dar lugar a una discrepancia interna
            grupo_ha_consolidado(ipedido, via, hacia_abajo=False, hacia_arriba=False)
        comando = form.vars.comando
        if comando=='proporcional':
            from modelos.pedidos import ajusta_precio_para_compensar_discrepancias
            resuelto = ajusta_precio_para_compensar_discrepancias(ipedido, igrupo)
        elif comando=='redondeo':
            from modelos.pedidos import reparto_redondeo
            resuelto = reparto_redondeo(ipedido, igrupo)
        else:
            resuelto = True

        if desdered:
            # Si cambia cantidad_red y cantidad de la red via, puede cambiar el reparto de costes extra
            incidencias_coste_pedido = recalcula_costes_extra_pedido(ipedido, via)
            if incidencias_coste_pedido:
                session.flash=T('El pedido de %s ha sido actualizado, pero ha habido problemas al calcular los costes extra del pedido')%pedido.productor.nombre
                redirect(URL(f='vista_pedido.html',vars=dict(pedido=ipedido, grupo=grupo.id, recalcula_costes_extra=1)))

        from modelos.pedidos import anota_incidencias_contabilidad
        anota_incidencias_contabilidad(ipedido)

        if comando=='asumir':
            ppedido.update_record(discrepancia_interna_asumida=True)
            session.flash=T('El grupo asume la diferencia entre lo recolectado de las unidades y lo que se paga a %s')%nombre_red
            redirect(URL(f='gestion_pedidos.html',vars=dict(grupo=igrupo)))
        elif comando=='proporcional' and not resuelto:
            response.flash=T('No se puede resolver la discrepancia ajustando el precio porque de algún producto no se ha repartido nada. Resolvedlo manualmente, por favor. Por ejemplo, podéis "asumir la diferencia".')
            return
        elif comando=='redondeo' and not resuelto:
            session.flash=T('Algún producto no se puede repartir de forma automática. Resolvedlo manualmente, por favor.')
            redirect(URL(f='incidencias.html',vars=dict(pedido=ipedido, grupo=igrupo)))
        elif comando in ('redondeo', 'proporcional'):# and resuelto:
            incidencias_coste_pedido = recalcula_costes_extra_pedido(ipedido, igrupo)
            if incidencias_coste_pedido:
                session.flash=T('El pedido de %s ha sido actualizado, pero ha habido problemas al calcular los costes extra del pedido')%pedido.productor.nombre
                redirect(URL(f='vista_pedido.html',vars=dict(pedido=ipedido, grupo=via if desdered else igrupo, recalcula_costes_extra=1)))

        session.flash=T('El pedido de %s ha sido actualizado')%pedido.productor.nombre
        if comando in ('manual', 'redondeo'):
            redirect(URL(f='incidencias.html',vars=dict(pedido=ipedido, grupo=igrupo)))
        else:
            redirect(URL(f='vista_pedido.html',vars=dict(pedido=ipedido, grupo=via if desdered else igrupo)))

    response.files.append(URL('static', 'js/incidencias_globales.js'))

    return dict(form=form,
                solo_obligatorias=obligatorias,
                algunas_obligatorias=algunas_obligatorias,
                vacio=vacio,
                todas=todas,
                propio=propio,
                desdered=desdered,
                grupoesred=grupoesred,
                nombre_grupo=nombre_grupo,
                nombre_red=nombre_red,
                sintitulo=request.vars.sintitulo)

#@requires_get_arguments(('grupo', db.grupo))
def exportar_oferta():
    '''Exporta los productos de cada productor del grupo, para mostrar
    la oferta al publico, solo si el grupo ha marcado "exportar_oferta"
    '''
    igrupo  = int(request.vars.grupo)
    grupo   = db.grupo(igrupo)

    auth.basic()
    if not grupo.exportar_oferta:
        return T('La oferta de este grupo no es pública')

    if request.extension != 'ods':
        redirect(URL(c=request.controller, f=request.function, extension='ods',
                    vars=request.get_vars))

    from exportador import ods_exportar_oferta
    from uuid import uuid4

    filename = 'Oferta_%s.ods'%K.ascii_safe(grupo.nombre, filename_safe=True)

    tmpfilename=os.path.join(request.folder,'private','temp_files','deleteme_' + str(uuid4()))
    ods_exportar_oferta(igrupo, tmpfilename)
    return response.stream(tmpfilename,
            4000,
            attachment=True,
            filename=filename
            )

#@requires_get_arguments(('productor', db.productor))
#@auth.requires_membership(Permisos.grupo_del_productor(request.vars.productor))
#@auth.requires(Permisos.puede_leer_productos(request.vars.productor, request.vars.auth_code))
def exportar_productos():
    '''Exporta los productos, la vista exportar_productos.csv genera un archivo
    csv compatible con "importar_productor" definida en models/60_utils.py
    '''
    iproductor  = int(request.vars.productor)
    productor   = db.productor(iproductor)
    grupo       = productor.grupo
    if request.extension != 'csv':
        redirect(URL(c=request.controller, f=request.function, extension='csv',
                    vars=request.get_vars))

    auth.basic()
    if productor.random_string==request.vars.auth_code:
        pass
    elif not auth.login():
        return T('403: No tienes permiso para descargar este archivo. Por favor repasa la url, o haz login con permisos suficientes.')
    elif (auth.has_membership(role='admins_%s'%grupo) or
          auth.has_membership(role='miembros_%s'%grupo) or
          auth.has_membership(role='webadmins')):
         pass
    else:
        session.flash = T('No tienes permiso. Por favor repasa la url, o accede a la web con otro usuario.')
        redirect(URL(c='default', f='user.html'))

    from exportador import csv_exportar_productos
    response.headers['Content-Type'] = 'text/csv'
    response.headers['Content-Disposition'] = 'attachment; filename=productos_%s.csv'%productor.nombre.replace(' ','_')
    return dict(csv=csv_exportar_productos(productor))

#@requires_get_arguments(('grupo', db.grupo), ('pedido', db.pedido))
@auth.requires(auth.has_membership(role='admins_%s'%request.vars.grupo) or
               auth.has_membership(role='miembros_%s'%request.vars.grupo))
def exportar_pedido():
    if request.extension not in ('csv', 'ods'):
        redirect(URL(c=request.controller, f=request.function, extension='ods',
                    vars=request.get_vars))

    igrupo  = int(request.vars.grupo)
    ipedido = int(request.vars.pedido)
    pedido  = db.pedido(ipedido)

    incidencias = request.vars.incidencias or '3'
    if incidencias=='3':
        prefilename = T('peticiones')
    elif incidencias=='1':
        prefilename = T('recibido')
    else:
        prefilename = T('suministrado')

    filename = '%s_%s.%s'%(
               prefilename,
               pedido.productor.nombre,
               request.extension
        )
    if request.extension=='csv':
        response.headers['Content-Type'] = 'text/csv'
        response.headers['Content-Disposition'] = 'attachment; filename=' + filename
        from exportador import csv_exportar_pedido
        return dict(csv=csv_exportar_pedido(igrupo, ipedido, incidencias))
    else:
        from exportador import ods_exportar_pedido
        from uuid import uuid4

        tmpfilename=os.path.join(request.folder,'private','temp_files','deleteme_' + str(uuid4()))
        ods_exportar_pedido(igrupo, ipedido, incidencias, tmpfilename)
        return response.stream(tmpfilename,
                4000,
                attachment=True,
                filename=filename
                )

#@requires_get_arguments(('grupo', db.grupo), ('fecha_reparto', 'date'))
@auth.requires(auth.has_membership(role='admins_%s'%request.vars.grupo) or
               auth.has_membership(role='miembros_%s'%request.vars.grupo))
def exportar_pedidos_fecha():
    from modelos.pedidos import pedidos_del_dia

    igrupo  = int(request.vars.grupo)
    nombre_grupo = db.grupo(igrupo).nombre
    fecha_reparto = request.vars.fecha_reparto
    (repartos, repartosc) = pedidos_del_dia(fecha_reparto, igrupo)
    pedidos = [r.id for r in chain(repartos, repartosc)]

    incidencias = request.vars.incidencias or '3'
    if incidencias=='3':
        prefilename = T('peticiones')
    else:
        prefilename = T('recibido') if incidencias=='1' else T('suministrado')

    filename = '%s_%s_%s.%s'%(
               prefilename,
               K.nombre2slug(nombre_grupo),
               fecha_reparto,
               request.extension
        )

    if request.extension not in ('csv', 'ods'):
         redirect(URL(c=request.controller, f=request.function, extension='ods',
                     vars=request.get_vars))
    elif request.extension == 'csv':
        from exportador import csv_exportar_pedidos

        response.headers['Content-Type'] = 'text/csv'
        response.headers['Content-Disposition'] = 'attachment; filename='+filename
        return dict(csv=csv_exportar_pedidos(igrupo, pedidos, incidencias))
    else:
        from exportador import ods_exportar_pedidos
        from uuid import uuid4

        tmpfilename=os.path.join(request.folder,'private','temp_files','deleteme_' + str(uuid4()))
        ods_exportar_pedidos(igrupo, pedidos, incidencias, tmpfilename)
        return response.stream(tmpfilename,
                4000,
                attachment=True,
                filename=filename
                )

#@requires_get_arguments(('grupo', db.grupo), ('pedido', db.pedido))
@auth.requires(auth.has_membership(role='admins_%s'%request.vars.grupo) or
               auth.has_membership(role='miembros_%s'%request.vars.grupo))
def exportar_tabla_reparto():
    from modelos.pedidos import tipo_pedido, TiposPedido

    igrupo  = int(request.vars.grupo)
    ipedido = int(request.vars.pedido)
    pedido  = db.pedido(ipedido)
    if request.extension not in ('csv', 'ods'):
         redirect(URL(c=request.controller, f=request.function, extension='ods',
                     vars=request.get_vars))
    elif request.extension == 'csv':
        from exportador import csv_exportar_tabla_reparto

        return dict(csv=csv_exportar_tabla_reparto(
            igrupo,
            [ipedido],
            coordinados=(tipo_pedido(ipedido, igrupo)==TiposPedido.COORDINADO)
        ))
    else:
        from exportador import exportar_pedido_ods
        from uuid import uuid4

        tmpfilename=os.path.join(request.folder,'private','temp_files','deleteme_' + str(uuid4()))
        exportar_pedido_ods(igrupo, ipedido, tmpfilename)
        return response.stream(tmpfilename,
                4000,
                attachment=True,
                filename=('tabla_pedido_%s.ods'%pedido.productor.nombre).replace(' ','_')
                )

#@requires_get_arguments(('grupo', db.grupo), ('fecha_reparto', 'date'))
@auth.requires(auth.has_membership(role='admins_%s'%request.vars.grupo) or
               auth.has_membership(role='miembros_%s'%request.vars.grupo))
def exportar_tabla_reparto_fecha():
    from modelos.pedidos import pedidos_del_dia_por_redes

    fecha_reparto = request.vars.fecha_reparto
    if request.extension != 'ods':
        redirect(URL(c=request.controller, f=request.function, extension='ods',
                    vars=request.get_vars))

    from exportador import exportar_reparto_ods
    from uuid import uuid4

    igrupo  = int(request.vars.grupo)
    (repartos, repartos_por_redes) = pedidos_del_dia_por_redes(fecha_reparto, igrupo)

    tmpfilename=os.path.join(request.folder,'private','temp_files','deleteme_' + str(uuid4()))
    exportar_reparto_ods(igrupo, fecha_reparto, tmpfilename)
    return response.stream(tmpfilename,
            4000,
            attachment=True,
            filename='tabla_reparto_%s.ods'%fecha_reparto)

#@requires_get_arguments(('grupo', db.grupo), ('pedido', db.pedido))
@auth.requires(auth.has_membership(role='admins_%s'%request.vars.grupo) or
               auth.has_membership(role='miembros_%s'%request.vars.grupo))
def exportar_plantilla():
    igrupo = int(request.vars.grupo)
    ipedido = int(request.vars.pedido)
    pedido  = db.pedido(ipedido)
    peticion   = not request.vars.incidencias

    if not pedido.usa_plantilla:
        session.flash = T('Estos datos no se importaron de una hoja de cálculo y por lo tanto no se pueden exportar a este formato')
        redirect(URL(c='gestion_pedidos',f='vista_pedido.html',vars=request.get_vars))
    _, xls    = db.pedido.xls.retrieve(pedido.xls)
    from importar_tablas import exportar_plantilla
    return exportar_plantilla([ipedido], xls, pedido.xls_filename, peticion=peticion, igrupo=igrupo)

#@requires_get_arguments(('plantilla', db.plantilla_pedidos))
@auth.requires(Permisos.puede_gestionar_plantilla(request.vars.plantilla))
def exportar_plantilla_multiple():
    plantilla  = db.plantilla_pedidos(request.vars.plantilla)
    pedidos    = plantilla.pedidos
    peticion   = not request.vars.incidencias

    _, xls    = db.plantilla_pedidos.plantilla.retrieve(plantilla.plantilla)
    from importar_tablas import exportar_plantilla
    return exportar_plantilla(pedidos, xls, plantilla.plantilla_filename, peticion=peticion)

@auth.requires_membership('admins_%s'%request.vars.grupo)
def limpia_peticion():
    '''Elimina la peticion de este grupo en este pedido

    Sólo se permite para un pedido abierto

    29JUN21: Si este grupo es una red, elimina los pedidos de las subredes
    '''
    from modelos.grupos import grupos_y_subredes_de_la_red
    ipedido = int(request.vars.pedido)
    pedido  = db.pedido(ipedido)
    if not pedido.abierto:
        return 'web2py.flash("%s");'%T('No se puede limpiar la petición en un pedido cerrado.')
    igrupo  = int(request.vars.grupo)
    subgrupos = grupos_y_subredes_de_la_red(igrupo)
    subgrupos.add(igrupo)
    # TODO: ¿avisar a los grupos si la red elimina su pedido?
    # suponemos que lo harán igualmente...
    for t in (db.peticion, db.item, db.item_grupo):
        ids = [row.id for row in
            db((t.grupo.belongs(subgrupos)) &
               (t.productoXpedido==db.productoXpedido.id) &
               (db.productoXpedido.pedido==ipedido)).select(t.id)]
        db(t.id.belongs(ids)).delete()
    # 29JUN21 NO desactiva el pedido si es coordinado, hay otro botón para eso ...
    # if pedido.productor.grupo!=igrupo:
    #     db.proxy_pedido(pedido=ipedido, grupo=igrupo).update_record(activo=False)
    return 'web2py.flash("%s");'%T('Se ha limpiado la petición del grupo para este pedido.')


@scope.set('admin-grupo')
#@requires_get_arguments(('grupo', db.grupo), ('fecha_reparto', 'date'))
@auth.requires_membership('admins_%s'%request.vars.grupo)
def importar_tabla_reparto_fecha():
    igrupo = int(request.vars.grupo)
    grupo  = db.grupo(igrupo)
    fecha_reparto = request.vars.fecha_reparto

    response.title = T('Importa la tabla del reparto con las incidencias anotadas.')

    form_upload = FORM(DIV(
        LABEL(T('Selecciona la tabla de reparto con las incidencias anotadas.'), _for='tabla'),
        INPUT(_type='file', _name='tabla', _id='tabla',
              requires=IS_NOT_EMPTY(T('Elige un archivo'))),
        _class='form-group form-group-lg'),
             INPUT(_value=T('Enviar'), _type='submit', _class='btn btn-primary'),
             _class='form-horizontal'
    )

    errores     = []
    incidencias = []
    if form_upload.process(formname='importar_tabla_reparto').accepted:
        from importar_tabla_reparto import importa_tabla_reparto_fecha
        errores, incidencias = importa_tabla_reparto_fecha(
            igrupo, fecha_reparto, form_upload.vars.tabla
        )
        session.incidencias_tras_importar = incidencias
        if errores:
            response.flash = T('Hemos encontrado algunos errores al importar las hojas.')
        else:
            response.flash = T('Por favor comprueba si la hoja se ha importado correctamente.')
        if (not incidencias) and (not errores):
            errores.append(T('No hemos encontrado ninguna diferencia entre la hoja y la base de datos de karakolas.'))

    form_aceptar = FORM(
            LABEL(T('¿Quieres importar las incidencias detectadas?')),
            DIV(INPUT(_value=T('Enviar, pero mantener la cantidad recibida'),
                  _type='submit',
                  _class='button-submit btn btn-primary',
                  _id='aceptar'),
                INPUT(_value=T('Enviar y recalcular la cantidad recibida'),
                  _type='submit',
                  _class='button-submit btn btn-success',
                  _id='recalcular',
                  _name='recalcular',
                  _onclick="$('#comando').val('recalcular');"),
            _class='btn-group'),
            INPUT(_type='hidden', _id='comando', _name='comando', _value=''),
        _class='form-horizontal'
    )
    errores_coste_extra = []
    if form_aceptar.process(formname='aceptar_cambios').accepted:
        from importar_tabla_reparto import escribe_incidencias
        from modelos.pedidos import suma_item_grupo, recalcula_costes_extra_pedido,\
                                    grupo_ha_consolidado

        escribe_incidencias(igrupo, session.incidencias_tras_importar)
        ipedidos = []
        for _, incidencias_red,_ in session.incidencias_tras_importar.values():
            ipedidos.extend(ipedido for _,ipedido,incidencias_pedido in incidencias_red)
        suma_item_grupo(ipedidos, igrupo)
        if form_aceptar.vars.comando=='recalcular':
            row_ids = []
            # junta todas las redes
            for _, incidencias_red,_ in session.incidencias_tras_importar.values():
                # todos los pedidos de todas las redes
                for nproductor, ipedido, incidencias_pedido in incidencias_red:
                    # todos los productos de los pedidos
                    row_ids.extend(ippp for nproducto, ippp, _ in incidencias_pedido)
            # movemos cantidad a cantidad_recibida
            db((db.item_grupo.grupo==igrupo) &
                db.item_grupo.productoXpedido.belongs(row_ids)) \
            .update(cantidad_recibida=db.item_grupo.cantidad)

        for ipedido in ipedidos:
            grupo_ha_consolidado(ipedido, igrupo)
        errores_coste_extra = {}
        for ipedido in ipedidos:
            error_coste_extra = recalcula_costes_extra_pedido(ipedido, igrupo)
            if error_coste_extra:
                #No hacemos el apunte contable, que va a arrojar números incorrectos
                #El mensaje de error tiene un link a editar_pedido para que cambien la
                #formula, entonces anotaremos las incidencias en la contabilidad
                errores_coste_extra[ipedido] = error_coste_extra
            else:
                from modelos.pedidos import anota_incidencias_contabilidad
                anota_incidencias_contabilidad(ipedido)
        if not errores_coste_extra:
            session.flash = T('Se han importado las incidencias señaladas.')
            redirect(URL(c='gestion_pedidos', f='gestion_pedidos', vars=dict(grupo=igrupo)))
    return dict(form=(form_aceptar if (errores or incidencias) else form_upload),
                errores=errores, incidencias=incidencias, errores_coste_extra=errores_coste_extra,
                grupo=igrupo
    )
