# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

try:#web2py >=2.9.12
   from gluon.dal.objects import Rows
except:#web2py <=2.9.11
   from gluon.dal import Rows

@scope.set('admin-grupo')
#@requires_get_arguments(('grupo', db.grupo))
@auth.requires(auth.has_membership('admins_%s'%request.vars.grupo) or
               auth.has_membership('webadmins'))
def admin_grupo():
    igrupo    = int(request.vars.grupo)
    grupo     = db.grupo[igrupo]
    if grupo.red:
        redirect(URL(c='redes', f='admin_red.html', vars=dict(red=igrupo)))

    response.title = T('Administración del grupo %s')%grupo.nombre

    iadmins   = auth.id_group('admins_%d'%igrupo)
    imiembros = auth.id_group('miembros_%d'%igrupo)
    return dict(igrupo=igrupo, iadmins=iadmins, imiembros=imiembros)


#@requires_get_arguments(('grupo', db.grupo))
@auth.requires(auth.has_membership('admins_%s'%request.vars.grupo) or
               auth.has_membership('webadmins'))
def miembros_grupo():
    igrupo     = int(request.vars.grupo)
    imiembros  = (int(request.vars.miembros) if request.vars.miembros else
                  auth.id_group('miembros_%d'%igrupo))
    nuevo_miembro = FORM(FIELDSET(T('Nombre de usuaria de la persona que se une:'),
                                INPUT(_name='miembro_username', #_readonly='readonly',
                                      _id='miembro_username')),
                      FIELDSET(T('Unidad a la se incorpora: '),
                                INPUT(_name='miembro_unidad',
                                      _class='integer',
                                      requires=IS_INT_IN_RANGE(1,1000))),
                      INPUT(_type='submit', _name='incluir_miembro',
                             _value=T('Incluir miembro') , _class='btn btn-primary' ),
#                      INPUT(_type='submit', _name='sacar_usuario',
#                             _value=T('Sacar de la lista de espera')),
                             )

    if nuevo_miembro.process().accepted:
        usuario = db.auth_user(username=request.vars.miembro_username)
        if usuario:
            pxg = db.personaXgrupo(grupo=igrupo, persona=usuario.id)
            if pxg and pxg.activo:
                pxg.update_record(activo=True, unidad=request.vars.miembro_unidad)
                response.flash = T('el usuario ya estaba en el grupo. Hemos actualizado su número de unidad.')
            else:
                db.auth_user(usuario.id).update_record(registration_key='')
                db.auth_membership.insert(group_id=imiembros, user_id=usuario.id)
                if pxg:
                    pxg.update_record(activo=True, unidad=request.vars.miembro_unidad)
                else:
                    db.personaXgrupo.insert(grupo=igrupo, persona=usuario.id,
                                            activo=True, unidad=request.vars.miembro_unidad)
                asunto = T('Has entrado en el grupo %s')%db.grupo(igrupo).nombre
                cuerpo = '<html>%s</html>'%db.textos_grupo(grupo=igrupo,slug='email_incorporacion_grupo').texto
                if not mail.send(to=usuario.email,
                                  subject=asunto,
                                  message=cuerpo):
                    response.flash = T('El usuario %s es ahora miembro del grupo.'
                                       'Le hemos enviado un email para avisarle.')%request.vars.miembro_username
                else:
                    response.flash = T('El usuario %s es ahora miembro del grupo')%request.vars.miembro_username
            ple = db.personaXlistaespera(grupo=igrupo,persona=usuario.id)
            if ple:
                ple.update_record(activo=False)
            else:
                db.personaXlistaespera.insert(grupo=igrupo,
                                              persona=usuario.id,
                                              fecha_de_solicitud=request.now,
                                              activo=False)
        else:
            response.flash = T('Usuario desconocido.')
    lista_solicitantes = db((db.personaXlistaespera.grupo==igrupo) &
                            (db.personaXlistaespera.activo==True) &
                            (db.personaXlistaespera.persona==db.auth_user.id))\
                         .select(db.auth_user.ALL,
                                 db.personaXlistaespera.comentario,
                                 orderby=db.personaXlistaespera.fecha_de_solicitud)
    return dict(nuevo_miembro=nuevo_miembro,
                igrupo=igrupo, imiembros=imiembros,
                lista_solicitantes=lista_solicitantes,
                lista_de_espera=db.grupo[igrupo].lista_de_espera)

#@requires_get_arguments(('grupo', db.grupo), ('uid',db.auth_user))
@auth.requires(auth.has_membership('admins_%s'%request.vars.grupo) or
               auth.has_membership('webadmins'))
def sacar_de_lista_de_espera():
    ple = db.personaXlistaespera(persona=request.vars.uid, grupo=request.vars.grupo)
    if ple: ple.update_record(activo=False)
    return ""

#@requires_get_arguments(('grupo', db.grupo))
@auth.requires(auth.has_membership('admins_%s'%request.vars.grupo) or
               auth.has_membership('webadmins'))
def lista_miembros_en_grupo():
    igrupo    = int(request.vars.grupo)
    imiembros  = (int(request.vars.miembros) if request.vars.miembros else
                  auth.id_group('miembros_%d'%igrupo))
    for k,v in request.vars.items():
        if k.startswith('del_'):
            id = int(k[4:])
            db((db.auth_membership.group_id==imiembros) &
               (db.auth_membership.user_id==id)).delete()
            db.personaXgrupo(grupo=igrupo, persona=id).update_record(activo=False)
            ple = db.personaXlistaespera(grupo=igrupo,persona=id)
            if ple: ple.update_record(activo=False)

    miembros = db((db.personaXgrupo.grupo==igrupo) &
                  (db.personaXgrupo.persona==db.auth_user.id) &
                  (db.personaXgrupo.activo==True)) \
               .select(db.auth_user.ALL, db.personaXgrupo.unidad,
                       orderby=db.personaXgrupo.unidad)
    if db.grupo[igrupo].prueba:
        for record in miembros:
            record.auth_user.email='xxx'
            record.auth_user.telefono='xxx'

    f = FORM(DIV(TABLE(
            TR(TH(T('Unidad')), TH(T('username')),
               TH(T('Nombre')), TH(T('Teléfono')), TH(T('Email')),
               TH(T('Sacar del grupo'))),
            *[TR(TD(u.personaXgrupo.unidad),
                 TD(u.auth_user.username),
                 TD('%s %s'%(u.auth_user.first_name, u.auth_user.last_name)),
                 TD(u.auth_user.telefono or T('Desconocido')), TD(u.auth_user.email),
                 TD(INPUT(_value='Eliminar', _type='checkbox', _name='del_%s'%u.auth_user.id) ))
              for u in miembros]),
            _class="table-responsive"),
            INPUT(_type='submit', _name='submit',
                  _value='Sacar del grupo los usuarios marcados',
                  _class='btn btn-primary' )
    )

    return dict(grupos=f)

@scope.set('miembro')
#@requires_get_arguments(('grupo', db.grupo))
@auth.requires_membership('miembros_%s'%request.vars.grupo)
def info_personas():
    igrupo = int(request.vars.grupo)
    grupo  = db.grupo[igrupo]

    response.title = T('Integrantes del grupo %s')%grupo.nombre

    mensaje=None
    if grupo.discreto and not auth.has_membership('admins_%s'%igrupo):
        mensaje = T('Este grupo ha restringido el acceso a estos datos a los administradores del grupo')
        return dict(miembros=Rows(), grupo=grupo, mensaje=mensaje)
    elif grupo.prueba:
        mensaje = T('Se ocultan datos personales al tratarse de un grupo de prueba')

    if grupo.prueba:
        miembros = []
        for m in db((db.personaXgrupo.grupo==igrupo) &
                      (db.personaXgrupo.persona==db.auth_user.id) &
                      (db.personaXgrupo.activo==True)) \
                   .select(db.auth_user.username, db.personaXgrupo.unidad,
                           orderby=db.personaXgrupo.unidad|db.auth_user.username):
            d = Storage(auth_user=
                        Storage(first_name='Xxxx', last_name='Xxxx',
                                telefono='xxxx', email='xxxx'),
                        personaXgrupo=Storage())
            d.auth_user.usename=m.auth_user.username
            d.personaXgrupo.unidad=m.personaXgrupo.unidad
            miembros.append(d)
    else:
        miembros = db((db.personaXgrupo.grupo==igrupo) &
                      (db.personaXgrupo.persona==db.auth_user.id) &
                      (db.personaXgrupo.activo==True)) \
                   .select(db.auth_user.ALL, db.personaXgrupo.unidad,
                           orderby=db.personaXgrupo.unidad|db.auth_user.username)

    return dict(miembros=miembros, mensaje=mensaje)

@scope.set('admin-grupo')
#@requires_get_arguments(('grupo', db.grupo))
@auth.requires(auth.has_membership('admins_%s'%request.vars.grupo) or
               auth.has_membership('webadmins'))
def importar_lista_espera():
    from rsheet import ERTable

    igrupo = int(request.vars.grupo)
    grupo  = db.grupo(igrupo)

    response.title = T('Incorporar personas a la lista de espera de %s')%grupo.nombre

    try:
        t  = next((k for k in request.vars if k.startswith('_table_name_')))
        #ce: campos extra  tp: sheet principal
        id_t = t[12:]
    except:
        id_t = None
    sheet = ERTable.factory(
        Field('nombre'),
        Field('apellido'),
        Field('email', requires=IS_EMAIL()),
        Field('telefono'),
        Field('comentario', 'text'),
        master_field='email',
        _id=id_t
    )
    if sheet.accepts(request.vars):
        rv = sheet.process_request(request.vars, keep_order=True)
        import datetime
        f  = request.now
        d  = datetime.timedelta(seconds=1)
        errores_email = []
        for fields in list(rv.values()):
            record = db.auth_user(email=fields['email'])
            if record:
            #si el usuario ya esta en la web, probablemente sus datos sean mejores
            #que los de la tabla
#                record.update_record(**user_vals)
                uid = record.id
            else:
                uid = db.auth_user.insert(
                    username=fields['email'], email=fields['email'],
                    first_name=fields['nombre'], last_name=fields['apellido'],
                    password=''.join(chr(random.randrange(32,123)) for _ in range(20) ),
                    telefono=fields['telefono']
                    )
                gid = db.auth_group.insert(
                        description='Group uniquely assigned to user %d'%uid,
                        role='user_%d'%uid)
                db.auth_membership.insert(user_id=uid, group_id=gid)
                if not auth.email_reset_password(db.auth_user(uid)):
                    errores_email.append(fields['email'])

            if not grupo.prueba:
                asunto = T('Has entrado en la lista de espera del grupo %s')%db.grupo(igrupo).nombre
                cuerpo = '<html>%s</html>'%db.textos_grupo(grupo=igrupo,slug='email_incorporacion_lista_de_espera').texto
                if not mail.send(to=fields['email'],
                                 subject=asunto,
                                 message=cuerpo):
                    errores_email.append(fields['email'])
            #Lista de espera
            r1 = db.personaXgrupo(grupo=igrupo, persona=uid)
            r2 = db.personaXlistaespera(grupo=igrupo, persona=uid)
            if (r1 and r1.activo) or (r2 and r2.activo):
                pass
            else:
                if r2:
                    r2.update_record(activo=True, comentario=fields['comentario'])
                else:
                    db.personaXlistaespera.insert(grupo=igrupo, persona=uid,
                                                  fecha_de_solicitud=f + d*fields['order'],
                                                  comentario=fields['comentario'])
        if grupo.prueba:
            session.flash = T('Las personas mencionadas se han incorporado a la lista de espera con éxito. No les hemos enviado un email porque este es un grupo de prueba.')
        elif not errores_email:
            session.flash = T('Las personas mencionadas se han incorporado a la lista de espera con éxito. Les hemos enviado un email para avisarles.')
        else:
            #TODO: log errores_email
            session.flash = T('Las personas mencionadas se han incorporado a la lista de espera con éxito. Algunos emails no han podido enviarse. Por favor avísales tu.')
        redirect(URL(c='grupo', f='admin_grupo.html', vars=dict(grupo=igrupo)))
    elif sheet.errors:
        response.flash = C.MENSAJE_ERRORES
    return dict(sheet=sheet, grupo=igrupo)

@scope.set('admin-grupo')
#@requires_get_arguments(('grupo', db.grupo))
@auth.requires(auth.has_membership('admins_%s'%request.vars.grupo) or
               auth.has_membership('webadmins'))
def importar_al_grupo():
    from rsheet import ERTable

    igrupo = int(request.vars.grupo)
    grupo  = db.grupo(igrupo)

    response.title = T('Incorporar personas al grupo %s')%grupo.nombre

    try:
        t  = next((k for k in request.vars if k.startswith('_table_name_')))
        #ce: campos extra  tp: sheet principal
        id_t = t[12:]
    except:
        id_t = None
    sheet = ERTable.factory(
        Field('email', requires=IS_EMAIL()),
        Field('unidad', 'integer', requires=IS_INT_IN_RANGE(1,1000)),
        master_field='email',
        _id=id_t
    )
    if sheet.accepts(request.vars):
        rv = sheet.process_request(request.vars, keep_order=True)
        import datetime
        f  = request.now
        d  = datetime.timedelta(seconds=1)
        errores_email = []
        for fields in list(rv.values()):
            record = db.auth_user(email=fields['email'])
            if record:
            #si el usuario ya esta en la web, probablemente sus datos sean mejores
            #que los de la tabla
#                record.update_record(**user_vals)
                uid = record.id
            else:
                uid = db.auth_user.insert(
                    username=fields['email'], email=fields['email'],
                    password=''.join(chr(random.randrange(32,123)) for _ in range(20) )
                    )
                gid = db.auth_group.insert(
                        description='Group uniquely assigned to user %d'%uid,
                        role='user_%d'%uid)
                db.auth_membership.insert(user_id=uid, group_id=gid)
                if not auth.email_reset_password(db.auth_user(uid)):
                    errores_email.append(fields['email'])

            if not grupo.prueba:
                asunto = T('Has entrado en el grupo %s')%db.grupo(igrupo).nombre
                cuerpo = '<html>%s</html>'%db.textos_grupo(grupo=igrupo,slug='email_incorporacion_grupo').texto
                if not mail.send(to=fields['email'],
                                  subject=asunto,
                                  message=cuerpo):
                    errores_email.append(fields['email'])

            #Lista de espera
            r1 = db.personaXgrupo(grupo=igrupo, persona=uid)
            imiembros = auth.id_group('miembros_%d'%igrupo)
            if r1:
                r1.update_record(activo=True, unidad=fields['unidad'])
            else:
                db.personaXgrupo.insert(grupo=igrupo, persona=uid, unidad=fields['unidad'])
            db.auth_membership.update_or_insert(group_id=imiembros, user_id=uid)
            r2 = db.personaXlistaespera(grupo=igrupo, persona=uid)
            if r2:
                r2.update_record(activo=False)
            else:
                db.personaXlistaespera.insert(
                    grupo=igrupo, persona=uid, activo=False,
                    fecha_de_solicitud=f + d*fields['order'],
                    comentario=T('este usuario se incorporó directamente al grupo sin pasar por la lista de espera en %s')%f)
        if grupo.prueba:
            session.flash = T('Las personas mencionadas se han incorporado al grupo con éxito. No les hemos enviado un email porque este es un grupo de prueba.')
        elif not errores_email:
            session.flash = T('Las personas mencionadas se han incorporado al grupo con éxito. Les hemos enviado un email para avisarles.')
        else:
            #TODO: log errores_email
            session.flash = T('Las personas mencionadas se han incorporado al grupo con éxito. Algunos emails no han podido enviarse. Por favor avísales tu.')
        redirect(URL(c='grupo', f='admin_grupo.html', vars=dict(grupo=igrupo)))
    elif sheet.errors:
        response.flash = C.MENSAJE_ERRORES
    return dict(sheet=sheet, grupo=igrupo)

@scope.set(scope_grupo_o_red)
#@requires_get_arguments(('grupo', db.grupo))
@auth.requires(auth.has_membership('admins_%s'%request.vars.grupo) or
               auth.has_membership('webadmins'))
def editar_datos_grupo():
    igrupo    = int(request.vars.grupo)
    grupo     = db.grupo[igrupo]

    response.title = T('Configuración de %s')%grupo.nombre

    #Seccion 1 de la configuracion del grupo
    fields1 = ['direccion', 'telefono', 'descripcion',           #de texto
               'lista_de_espera', 'discreto', 'exportar_oferta', #booleanos
               'dias_pedido_reciente'] #entero
    if grupo.red:
        fields1.remove('lista_de_espera')
    form_grupo = SQLFORM(db.grupo, igrupo, fields=fields1, buttons=[], showid=False)
    form_components1 = form_grupo.components

    #Seccion 2 de la configuracion del grupo: contabilidad
    fields2 = [ 'opcion_ingresos', 'opcion_pedidos']
    form_grupo = SQLFORM(db.grupo, igrupo, fields=fields2, buttons=[], showid=False)
    form_components2 = form_grupo.components
    textos = [(r.slug, r.texto) for r in db(db.textos_grupo.grupo==igrupo) \
                             .select(db.textos_grupo.slug, db.textos_grupo.texto)
    ]
    #Monedas
    monedas = db(db.moneda.activa==True).select()
    monedas_del_grupo = set(row.moneda
        for row in db((db.grupoXmoneda.grupo==igrupo) &
                      (db.grupoXmoneda.activo==True)).select(db.grupoXmoneda.moneda))
    componente_monedas = DIV(
        *[DIV(INPUT(_type='checkbox', _name='moneda_%d'%moneda.id,
                    value=(moneda.id in monedas_del_grupo)),
              moneda.nombre,
              SPAN(XML(moneda.simbolo_html), _class='simbolo_moneda'),
              _class='col-md-2 col-sm-6') for moneda in monedas],
        _class='row')
    form_components2.append(componente_monedas)

    #Seccion 3 de la configuracion del grupo
    editar_textos = DIV(*[FIELDSET(LABEL(slug),
            TEXTAREA(texto, _class='texto_grupo ckeditor',
                     _name='texto_grupo_%s'%slug,
                     _id='texto_grupo_%s'%slug ))
                     for slug, texto in textos],
                         **dict(_id='div_textos_grupo')
    )
    textareas = ['texto_grupo_%s'%slug for slug,_ in textos]

    #Componemos el formulario en forma de acordeon
    def panel_acordeon(icono, descripcion, componentes):
        accordion_id = 'panel_acordeon_%s'%icono[10:]
        return DIV(
            DIV(SPAN(_class='glyphicon '+icono),
                SPAN(descripcion, _class='descripcion_grupo_config'),
                **{'_data-toggle':'collapse', '_data-parent':'#acordeon_opciones', '_role':'button',
                   '_href':'#'+accordion_id, '_class':'panel-heading'}),
            DIV(DIV(DIV(DIV(*componentes, _class='col-md-10 col-md-offset-1'), _class='row'),
                    _class='panel-body'),
                _class='panel-collapse collapse',
                _id=accordion_id),
            _class='panel panel-default')
    form_components = (
        panel_acordeon('glyphicon-list', T('Opciones generales'), form_components1),
        panel_acordeon('glyphicon-euro', T('Contabilidad'), form_components2),
        panel_acordeon('glyphicon-pencil', T('Textos'), [editar_textos]),
    )
    form = FORM(DIV(*form_components, _class='panel-group', _id='acordeon_opciones'),
                INPUT(_type='submit', _name='submit', _value=T('Enviar'), _class='btn btn-primary')
    )
    def valida_monedas(form):
        alguna_moneda = any((v=='on' and k.startswith('moneda_') )
            for k,v in form.vars.items())
        if not alguna_moneda:
            for k,v in form.vars.items():
                if k.startswith('moneda_'):
                    form.errors[k] = 'Selecciona al menos una moneda'

    if form.accepts(request.vars, keepvalues=True, onvalidation=valida_monedas):
        vars_textos = {}
        monedas_on = []
        for k,v in request.post_vars.items():
            if k.startswith('texto_grupo_'):
                db.textos_grupo(grupo=igrupo, slug=k[12:]).update_record(texto=v)
            if k.startswith('moneda_'):
                #Solo llegan las monedas que estaban marcadas
                imoneda = int(k[7:])
                if v=='on':
                    monedas_on.append(imoneda)
                    rmoneda = db.grupoXmoneda(grupo=igrupo, moneda=imoneda)
                    if not rmoneda:
                        db.grupoXmoneda.insert(grupo=igrupo, moneda=imoneda, activo=True)
                    elif not rmoneda.activo:
                        rmoneda.update_record(activo=True)
        for imoneda in monedas_del_grupo.difference(monedas_on):
            rmoneda = db.grupoXmoneda(grupo=igrupo, moneda=imoneda)
            rmoneda.update_record(activo=False)

        grupo.update_record(**dict(
            (f.name, request.vars[f.name]) for f in db.grupo
            if (f.name in form.vars and f.writable and f.name!='id'))
        )
        response.flash = T('Datos actualizados')
    elif form.errors:
        response.flash = C.MENSAJE_ERRORES
    return dict(form=form, textareas=textareas)

@scope.set('admin-grupo')
#@requires_get_arguments(('grupo', db.grupo))
@auth.requires(auth.has_membership('admins_%s'%request.vars.grupo) or
               auth.has_membership('webadmins'))
def redes():
    igrupo = int(request.vars.grupo)
    grupo  = db.grupo(igrupo)
    #Por lo pronto únicamente cargamos productores_coordinados
    response.title = T('Redes a las que pertenece el grupo %s')%grupo.nombre
    return dict(grupo=igrupo)
