# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.


#########################################################################

#@scope.set( TODO, a lo mejor no hace falta porque solo lo uso como componente )
@scope.set(scope_grupo_o_red)
#@requires_get_arguments(('tipo_operacion', 'integer'),
#                        ('param0', 'integer'))
def ingreso():
    auth.basic()
    ito = int(request.vars.tipo_operacion)
    to  = CONT.TIPOS_OPERACION[ito]
    params = [int(request.vars.param0)]
    if request.vars.param1:
        params.append(int(request.vars.param1))
    if request.vars.param2:
        params.append(int(request.vars.param2))

    if not (auth.user_id and to.tiene_permiso(auth.user_id, params)):
        session.flash = T('No puedes hacer ingresos en esta cuenta')
        redirect(request.vars.get_back or URL(c='default', f='home.html'))

    input_cantidad = INPUT(_value=Decimal(), _type='decimal',
                           _class="form-control decimal",
                           _name='valor', _id='valor',
                           requires=IS_DECIMAL_IN_RANGE(0 if to.positivo else None,None))

    imonedas = to.monedas(params)
    if not imonedas:
        session.flash = T('No hay ninguna moneda de uso común por ambas partes para hacer este ingreso')
        redirect(request.vars.get_back or URL(c='default', f='home.html'))
    elif len(imonedas)==1:
        imoneda = imonedas[0]
        if imoneda:
            simbolo_moneda = db.moneda(imoneda).simbolo_html
            input_monedas = INPUT(_type='hidden', value=imoneda, _name='moneda')
            input_cantidad = DIV(
                input_cantidad,
                DIV(XML(simbolo_moneda), _class='input-group-addon'),
                _class='input-group')
        else: #Si monedas es None, no hace falta hacer nada
            input_monedas = ''
    else:
        simbolos_monedas = dict((row.id, row.simbolo_html)
            for row in db(db.moneda.id.belongs(imonedas)).select(
                db.moneda.id, db.moneda.simbolo_html))
        input_monedas = SELECT(*[OPTION(XML(simbolo), _value=imoneda)
                                 for imoneda, simbolo in simbolos_monedas.items()],
                               _name='moneda' ,_class='form-control'
        )

    descripcion_operacion = to.descripcion((params))
    response.title = descripcion_operacion
    form_ingreso = FORM(
            DIV(LABEL(T('Cantidad'), ' ', _for='valor'),
                input_cantidad,
#                      requires=IS_DECIMAL_IN_RANGE(0,None,dot=',' if current.T.accepted_language.startswith('es') else '.')),
                input_monedas,
                _class='form-group form-inline'
            ),
            INPUT(_type='submit', _name='submit',
                  _value=T('Anotar operación'),  _class="btn btn-primary"),
            _class = 'form-horizontal'
    )
    if request.vars.comentario:
        form_ingreso.insert(1,
            DIV(LABEL(T('Concepto'), ' ', _for='comentario'),
                INPUT(_placeholder=request.vars.comentario,
                      _class='form-control',
                      _name='comentario', _id='comentario'),
                _class='form-group'
            ))
    if form_ingreso.process().accepted:
        moneda = int(form_ingreso.vars.moneda) if form_ingreso.vars.moneda else None
        valor = Decimal(form_ingreso.vars.valor)
        comentario = form_ingreso.vars.comentario or ''
        to.ejecuta(params, valor=valor, comentario=comentario, moneda=moneda)
        if request.vars.get_back:
            session.flash=T('El ingreso se ha anotado correctamente')
            redirect(request.vars.get_back)
        response.flash=T('El ingreso se ha anotado correctamente')
    elif form_ingreso.errors:
        response.flash=T('Por favor introduce un número usando el punto decimal')
    return dict(form=form_ingreso,
                descripcion_operacion=descripcion_operacion,
                ayuda=to.ayuda(),
                title=response.title)

def vista_operaciones(query, fields):
    form = SQLFORM.grid(
        query, fields,
        orderby=~db.operacion.id,
        paginate=10,
        maxtextlength=255,
        selectable=False, editable=False, deletable=False, details=False,
        searchable=False, create=False, sortable=False, csv=False
#        exportclasses=dict(xml=False, html=False, json=False,
#                           csv_with_hidden_cols=False, tsv_with_hidden_cols=False)
    )
    return form

#@requires_get_arguments(('cuenta', db.cuenta))
def vista_cuenta():
    icuenta = int(request.vars.cuenta)
    cuenta  = db.cuenta(icuenta)
    descripcion  = cuenta.descripcion
    saldo = cuenta.saldo
    form = vista_operaciones(
        (db.apunte.operacion==db.operacion.id) & (db.apunte.cuenta==icuenta),
        [db.operacion.created_on, db.operacion.fecha, db.apunte.cantidad,
         db.operacion.descripcion, db.apunte.saldo]
    )
    return dict(descripcion=descripcion, form=form, saldo=saldo)

#@requires_get_arguments(('grupo', db.grupo))
def vista_operaciones_grupo():
    igrupo = int(request.vars.grupo)
    grupo  =db.grupo(igrupo)
    if request.vars.tipo=='caja':
        cuenta_der = db.cuenta_derivada(tipo=CONT.TIPOS_CUENTA_DERIVADA.CAJA_GRUPO, owner=igrupo, param=request.vars.moneda)
    else:
        cuenta_der = db.cuenta_derivada(tipo=CONT.TIPOS_CUENTA_DERIVADA.SALDO_GRUPO, owner=igrupo)
    descripcion = cuenta_der.descripcion
    saldo = cuenta_der.saldo
    form = vista_operaciones(
        (db.saldo_cuenta_derivada.cuenta_derivada==cuenta_der.id) &
        (db.saldo_cuenta_derivada.operacion==db.operacion.id),
        [db.operacion.created_on, db.operacion.fecha, db.operacion.descripcion,
         db.saldo_cuenta_derivada.cantidad, db.saldo_cuenta_derivada.saldo]
    )
    return dict(descripcion=descripcion, form=form, saldo=saldo)

@scope.set(scope_miembro_o_red)
@auth.requires(auth.has_membership(role='admins_%s'%request.vars.grupo) or
               auth.has_membership(role='miembros_%s'%request.vars.grupo))
#@requires_get_arguments(('grupo', db.grupo))
def resumen_grupo():
    # Idea? acceso a las cuentas del grupo desde el login de usuario, o no,
    #dependiendo de la configuracion => #424
#    auth.basic()
#    if auth.user_id and ...:
#        pass
#    else:
#        session.flash = T('No puedes ver la contabilidad de este grupo')
#        redirect(URL(c='default', f='home.html'))
    igrupo = int(request.vars.grupo)
    grupo  =db.grupo(igrupo)
    if grupo.opcion_ingresos==CONT.OPCIONES_INGRESOS.NO_CONT:
        redirect(URL(c='grupo', f='editar_datos_grupo.html', vars=dict(grupo=igrupo)))
    response.title = T('Contabilidad de %s')%grupo.nombre
    dinero_en_caja = pagos_pendientes = cobros_pendientes = saldo_neto = Decimal()
    d_cuentas = {}
    for itc, tc in CONT.TIPOS_CUENTA.items():
        if ((tc.target == CONT.TipoCuenta.TARGET_SINK) or
            (tc.owner[0]!='grupo' and  tc.target[0]!='grupo')):
            continue
        if itc in (CONT.TIPOS_CUENTA.GRUPO_PRODUCTOR,
                   CONT.TIPOS_CUENTA.GRUPO,
                   CONT.TIPOS_CUENTA.GRUPO_RED):
            cuentas = db((db.cuenta.tipo==itc) &
                         (db.cuenta.owner==igrupo) &
                         (db.cuenta.activa==True)
                        ).select()
            saldo_cuentas = sum(c.saldo for c in cuentas)
            saldo_neto += saldo_cuentas
            if tc.target != CONT.TipoCuenta.TARGET_SELF:
                pagos_pendientes -= saldo_cuentas
            else:
                dinero_en_caja += saldo_cuentas
        if ((itc == CONT.TIPOS_CUENTA.UNIDAD_GRUPO) or
            (itc == CONT.TIPOS_CUENTA.GRUPO_RED) and grupo.red):
            cuentas = db((db.cuenta.tipo==itc) &
                         (db.cuenta.target==igrupo) &
                         (db.cuenta.activa==True)
                        ).select()
            saldo_cuentas = sum(c.saldo for c in cuentas)
            dinero_en_caja += saldo_cuentas
            cobros_pendientes -= saldo_cuentas
        d_cuentas[itc] = cuentas

    unidades_personas = defaultdict(list)
    for row in db((db.personaXgrupo.grupo==igrupo) &
                  (db.personaXgrupo.activo==True)
                 ).select():
        unidades_personas[row.unidad].append(
            (row.persona, K.represent(db.auth_user, db.auth_user(row.persona)))
        )

    nombres_productores = dict((row.id, row.nombre)
        for row in db(db.productor.grupo==igrupo).select(
            db.productor.id, db.productor.nombre)
    )

    nombres_redes = dict((row.id, row.nombre)
        for row in db((db.grupoXred.grupo==igrupo) &
                      (db.grupoXred.red==db.grupo.id)
                     ).select(db.grupo.id, db.grupo.nombre)
    )

    nombres_grupos = dict((row.id, row.nombre)
        for row in db((db.grupoXred.red==igrupo) &
                      (db.grupoXred.grupo==db.grupo.id)
                     ).select(db.grupo.id, db.grupo.nombre)
    )

    cajas_grupos = [(row.param, row.descripcion, row.saldo) for row in
        db((db.cuenta_derivada.tipo==CONT.TIPOS_CUENTA_DERIVADA.CAJA_GRUPO) &
           (db.cuenta_derivada.owner==igrupo) &
           (db.cuenta_derivada.activa==True)
          ).select(db.cuenta_derivada.descripcion,
                   db.cuenta_derivada.saldo,
                   db.cuenta_derivada.param)
    ]
    return dict(cuentas=d_cuentas,
                saldo_neto=saldo_neto,
                dinero_en_caja=dinero_en_caja,
                cajas_grupos=cajas_grupos,
                cobros_pendientes=cobros_pendientes,
                pagos_pendientes=pagos_pendientes,
                es_admin=auth.has_membership(role='admins_%s'%request.vars.grupo),
                unidades_personas=unidades_personas,
                nombres_productores=nombres_productores,
                nombres_redes=nombres_redes,
                nombres_grupos=nombres_grupos,
                igrupo=igrupo, grupo=grupo,
                title=response.title
               )

@auth.requires_login()
def resumen_persona():
    response.title = T('Contabilidad de %s')%auth.user.username

    cuentas = db((db.cuenta.tipo==CONT.TIPOS_CUENTA.UNIDAD_GRUPO) &
                 (db.cuenta.owner==db.personaXgrupo.unidad) &
                 (db.cuenta.target==db.personaXgrupo.grupo) &
                 (db.personaXgrupo.grupo==db.grupo.id) &
                 (db.personaXgrupo.persona==auth.user_id) &
                 (db.personaXgrupo.activo==True)
                ).select(db.cuenta.id, db.personaXgrupo.unidad,
                         db.grupo.id, db.grupo.nombre, db.grupo.opcion_ingresos
                )
    return dict(cuentas=cuentas,
                title=response.title)

@scope.set(scope_grupo_o_red)
@auth.requires_membership('admins_%s'%request.vars.grupo)
#@requires_get_arguments(('grupo', db.grupo), ('tipo_operacion', 'integer'))
def ingreso_multiple():
    from modelos.usuarios import representa_usuario_2
    from contabilidad import monedas_grupo

    ito = int(request.vars.tipo_operacion)
    to  = CONT.TIPOS_OPERACION[ito]
    igrupo = int(request.vars.grupo)
    grupo  = db.grupo(igrupo)
    ngrupo = grupo.nombre
    grupo_delante = ((ito == CONT.TIPOS_OPERACION.PAGO_PRODUCTOR) or
                     (ito == CONT.TIPOS_OPERACION.PAGO_RED and not grupo.red))

    if ito == CONT.TIPOS_OPERACION.INGRESO_GRUPO:
        itc = CONT.TIPOS_CUENTA.UNIDAD_GRUPO
        cuentas = [(row.cuenta.id, (row.personaXgrupo.persona, row.cuenta.owner),
                    '%d (%s)'%(
                        row.cuenta.owner,representa_usuario_2(row.personaXgrupo.persona)),
                    row.cuenta.saldo, False)
            for row in db((db.cuenta.tipo==itc) &
                          (db.cuenta.target==igrupo) &
                          (db.cuenta.activa==True) &
                          (db.personaXgrupo.grupo==igrupo) &
                          (db.personaXgrupo.unidad==db.cuenta.owner) &
                          (db.personaXgrupo.activo==True)
                         ).select(db.cuenta.id, db.cuenta.owner, db.cuenta.saldo,
                                  db.personaXgrupo.persona,
                                  orderby=db.cuenta.owner
                         )
        ]
        params2key = lambda p:tuple(p[:2])
        texto_cuenta = T('Unidad')
        cabecera = T('Anotar depósitos de las unidades de %s')%ngrupo
    elif ito == CONT.TIPOS_OPERACION.PAGO_UNIDAD:
        itc = CONT.TIPOS_CUENTA.UNIDAD_GRUPO
        cuentas = [(row.id, row.owner, str(row.owner),
                    row.saldo, False)
            for row in db((db.cuenta.tipo==itc) &
                          (db.cuenta.target==igrupo) &
                          (db.cuenta.activa==True)
                         ).select(db.cuenta.id, db.cuenta.owner, db.cuenta.saldo,
                                  orderby=db.cuenta.owner
                         )
        ]
        params2key = lambda p:p[0]
        texto_cuenta = T('Unidad')
        cabecera = T('Cobrar a las unidades de %s')%ngrupo
    elif ito == CONT.TIPOS_OPERACION.PAGO_PRODUCTOR:
        itc = CONT.TIPOS_CUENTA.GRUPO_PRODUCTOR
        productores = db((db.productor.grupo==igrupo) &
                         (db.productor.activo==True)
        ).select(
            db.productor.nombre, db.productor.id
        ).as_dict()
        cuentas = [(row.id, row.target, productores[row.target]['nombre'], row.saldo, False)
            for row in db((db.cuenta.tipo==itc) &
                          (db.cuenta.owner==igrupo) &
                          (db.cuenta.activa==True)
                         ).select(db.cuenta.id, db.cuenta.target, db.cuenta.saldo,
                                  orderby=db.cuenta.target
                         )
        ]
        params2key = lambda p:p[1]
        texto_cuenta = T('Productor')
        cabecera = T('Anotar depósitos en las cuentas de los productores de %s')%ngrupo
    elif ito == CONT.TIPOS_OPERACION.PAGO_RED and grupo.red:
        itc = CONT.TIPOS_CUENTA.GRUPO_RED
        grupos = db((db.grupoXred.red==igrupo) &
                    (db.grupoXred.grupo==db.grupo.id) &
                    (db.grupoXred.activo==True)
                  ).select(db.grupo.nombre, db.grupo.id
                  ).as_dict()
        cuentas = [(row.id, row.owner, grupos[row.owner]['nombre'], row.saldo, False)
            for row in db((db.cuenta.tipo==itc) &
                          (db.cuenta.target==igrupo) &
                          (db.cuenta.activa==True)
                         ).select(db.cuenta.id, db.cuenta.owner, db.cuenta.saldo)
        ]
        params2key = lambda p:p[0]
        texto_cuenta = T('Grupo')
        cabecera = T('Anotar depósitos de los grupos que pertenecen a %s')%ngrupo
    elif ito == CONT.TIPOS_OPERACION.PAGO_RED:
        itc = CONT.TIPOS_CUENTA.GRUPO_RED
        redes = db((db.grupoXred.grupo==igrupo) &
                   (db.grupoXred.red==db.grupo.id) &
                   (db.grupoXred.activo==True)
                  ).select(db.grupo.nombre, db.grupo.id, db.grupo.opcion_ingresos
                  ).as_dict()
        cuentas = [(row.id, row.target, redes[row.target]['nombre'], row.saldo,
                    redes[row.target]['opcion_ingresos']==CONT.OPCIONES_INGRESOS.SOLO_ADMIN)
            for row in db((db.cuenta.tipo==itc) &
                          (db.cuenta.owner==igrupo) &
                          (db.cuenta.activa==True)
                         ).select(db.cuenta.id, db.cuenta.target, db.cuenta.saldo,
                                  orderby=db.cuenta.target
                         )
        ]
        params2key = lambda p:p[1]
        texto_cuenta = T('Red')
        cabecera = T('Anotar depósitos en las cuentas de las redes a las que pertenece %s')%ngrupo

    ultima_operacion = defaultdict(lambda: ('',''))
    for row in db((db.operacion.tipo==ito) &
                  (db.operacion.parametros.like(('|%d|%%|' if grupo_delante else '%%|%d|')%igrupo))
                 ).select(db.operacion.created_on, db.operacion.valor, db.operacion.parametros):
        ultima_operacion[params2key(row.parametros)] = (row.valor, K.format_fecha(row.created_on.date()))

    fila_cabecera = TR(TH(texto_cuenta), TH(T('Saldo')), TH(T('Último movimiento')))
    filas_ingreso = [TR(TD(nobjeto), TD(saldo),
               TD('%s (%s)'%ultima_operacion[iobjeto])
              )
        for cid, iobjeto, nobjeto, saldo, disabled in cuentas
    ]
    if ito in [CONT.TIPOS_OPERACION.INGRESO_GRUPO,
               CONT.TIPOS_OPERACION.PAGO_PRODUCTOR,
               CONT.TIPOS_OPERACION.PAGO_RED]:
        dmonedas_grupo = monedas_grupo(igrupo)
    else:
        dmonedas_grupo = {0:''}
    for imoneda, simbolo in dmonedas_grupo.items():
        fila_cabecera.append(TH(T('Cantidad'), ' ', XML(simbolo) ))
    for fila,(cid, iobjeto, nobjeto, saldo, disabled) in zip(filas_ingreso, cuentas):
        if ito == CONT.TIPOS_OPERACION.PAGO_RED:
            monedas_aceptadas = monedas_grupo(iobjeto)
        else:
            monedas_aceptadas = dmonedas_grupo
        for imoneda, simbolo in dmonedas_grupo.items():
            nombre_input = ('ingreso_%d_'%imoneda) + (
                str(iobjeto) if isinstance(iobjeto,int)
                             else '_'.join(map(str,iobjeto)))

            fila.append(
               TD(INPUT(_name=nombre_input, _type='decimal', _class="form-control decimal",
                        **(dict(_disabled='disabled')
                           if (disabled or imoneda not in monedas_aceptadas) else {})))
            )

    form = FORM(TABLE(THEAD(fila_cabecera), TBODY(*filas_ingreso)),
                      INPUT(_type='submit', _class='btn btn-primary')
    )
    if form.process().accepted:
        to = CONT.TIPOS_OPERACION[ito]
        problemas_permisos = False
        for k,v in form.vars.items():
            if v and k.startswith('ingreso_'):
                k_parts = k.split('_')
                imoneda = int(k_parts[1]) or None
                iobjeto = list(map(int, k_parts[2:]))
                params = ([igrupo] + iobjeto) if grupo_delante else (iobjeto + [igrupo])
                if to.tiene_permiso(auth.user_id, params):
                    to.ejecuta(params, valor=Decimal(v), moneda=imoneda)
                else:
                    problemas_permisos = True
        if problemas_permisos:
            session.flash=T('No tienes permisos para realizar algunos de los ingresos')
        else:
            session.flash=T('Las operaciones se han anotado correctamente')
        redirect(URL(c='contabilidad', f='resumen_grupo.html', vars=dict(grupo=igrupo)))
    elif form.errors:
        response.flash = C.MENSAJE_ERRORES
    response.title = cabecera
    return dict(form=form,
                title=response.title)

@scope.set(scope_grupo_o_red)
#@requires_get_arguments(('pedidos', 'string'))
@auth.requires(request.vars.pedidos and
               auth.has_membership(Permisos.grupo_del_pedido(request.vars.pedidos.split(',',1)[0])))
def piensa_antes_de_cerrar():
    # solo para pedidos propios
    ipedidos = [int(v) for v in request.vars.pedidos.split(',')]
    pedido0  = db.pedido(ipedidos[0])
    igrupo   = pedido0.productor.grupo.id
    response.title = T('Unidades con saldo insuficiente al cerrar pedidos del %s')%pedido0.fecha_reparto

    excepciones, total_pedido, total_saldos = \
        session.unidades_con_saldo_insuficiente[request.vars.pedidos]
    repr_pedidos = [row.nombre
        for row in db( db.pedido.id.belongs(ipedidos) &
                      (db.pedido.productor==db.productor.id)
                   ).select(db.productor.nombre, orderby=db.productor.nombre)
    ]
    # TODO ? : lista las personas en cada unidad, ponlas en una columna extra o en un tooltip
    form = FORM(
        TABLE(
            THEAD(TR(TH(),TH(T('Pedido')),TH(T('Saldo')),TH())),
            TBODY(*[TR(TH(T('Unidad %d')%u),
                       TD(p), TD(s),
                       TD(INPUT(_type='checkbox', _name='clear_unidad_%d'%u)) )
            for u,p,s in excepciones])
        ),
        INPUT(_type='hidden', _name='cerrar_o_no_cerrar',
              _value='', _id='cerrar_o_no_cerrar'),
        INPUT(_type='submit', _class='btn btn-primary', _name='clear_and_send',
              _value=T('Eliminar excepciones y cerrar'),
              _onclick='$("#cerrar_o_no_cerrar").val("si");',
              **{'_data-toggle':'tooltip', '_data-placement':'bottom',
                 '_title':T('Elimina el pedido de las unidades seleccionadas y cierra el pedido.')}),
        INPUT(_type='submit', _class='btn btn-primary', _name='clear_and_return',
              _value=T('Eliminar excepciones'),
              **{'_data-toggle':'tooltip', '_data-placement':'bottom',
                 '_title':T('Elimina el pedido de las unidades seleccionadas pero no cierra el pedido todavía.')}),
        _id='excepciones',
        _action=URL('contabilidad', 'piensa_antes_de_cerrar',
                    vars=dict(pedidos=request.vars.pedidos))
    )
    if form.process().accepted:
        unidades_a_limpiar = [int(k[13:])
            for k,v in form.vars.items()
            if k.startswith('clear_unidad_') and v
        ]
        CONT.anula_peticion_unidades(unidades_a_limpiar, igrupo, ipedidos)
        if len(ipedidos)==1:
            redirect(URL(c='gestion_pedidos', f='cerrar_pedido.html', vars=dict(
                pedido=ipedidos[0],
                forzar_cierre=('1' if request.vars.cerrar_o_no_cerrar else '')
            )))
        else:
            redirect(URL(c='gestion_pedidos', f='cerrar_pedidos.html', vars=dict(
                plantilla=request.vars.plantilla,
                forzar_cierre=('1' if request.vars.cerrar_o_no_cerrar else '')
            )))
    return dict(form=form,
                total_pedido=total_pedido,
                total_saldos=total_saldos,
                repr_pedidos=repr_pedidos,
                title=response.title
           )
