# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.


#########################################################################
#@requires_get_arguments(('pedido', db.pedido), ('grupo', db.grupo))
@auth.requires_membership('miembros_%s'%request.vars.grupo)
def pedir_uno():
    from modelos.pedidos import query_mask
    tppp = db.productoXpedido

    ipedido  = int(request.vars.pedido)
    pedido   = db.pedido(ipedido)
    nombre_productor = pedido.productor.nombre

    igrupo       = int(request.vars.grupo)
    grupo_pedido = db.pedido(ipedido).productor.grupo.id
    if grupo_pedido!=igrupo:
        coordinado = True
    else:
        coordinado = False

    ipersona = auth.user.id
    unidad = db.personaXgrupo(persona=ipersona, grupo=igrupo).unidad

    query = ((tppp.pedido==ipedido)          &
             (tppp.categoria==db.categorias_productos.id) &
             (tppp.producto==db.producto.id) &
             (tppp.temporada==True) &
             (tppp.activo==True) &
             (db.producto.productor==db.productor.id))
    if coordinado:
        query &= query_mask(igrupo)
    productos = db(query) \
                  .select(tppp.ALL,
                          db.productor.nombre,
                          db.categorias_productos.nombre)

    from bloques_vista_pedir import PedirProductosController
    form = PedirProductosController(productos, ipersona, igrupo, pedidos={ipedido:coordinado})

    rec = db.comentarioApeticion(pedido=ipedido, persona=ipersona)
    comentario = rec.texto if rec else ''
    form.insert(1,
        DIV(H4('¿Tienes algo que añadir?'),
            TEXTAREA(_name='comentarioApeticion', _class='comentarioApeticion', value=comentario))
    )
    if not pedido.abierto:
        session.flash = T('El pedido de %s se ha cerrado recientemente')%nombre_productor
        redirect(URL(c='pedir', f='pedir', vars=dict(fecha_reparto=pedido.fecha_reparto, grupo=igrupo)))
    elif form.accepts(request.vars):
        response.js = "$('#carrito').trigger('actualiza_carrito');"
        comentario = request.vars.comentarioApeticion
        if comentario:
            db.comentarioApeticion.update_or_insert(
                ((db.comentarioApeticion.pedido==ipedido) &
                 (db.comentarioApeticion.persona==ipersona)),
                pedido=ipedido, persona=ipersona, texto=comentario)

        response.flash = form.feedback
    elif form.errors:
        response.flash = form.feedback

    return dict(elige_productos=form,
                nombre_productor=nombre_productor,
                descripcion_productor=XML(pedido.productor.descripcion),
                fecha_reparto=pedido.fecha_reparto,
                info_cierre=pedido.info_cierre)

#@requires_get_arguments(('grupo', db.grupo), ('fecha_reparto', 'date'))
@auth.requires_membership('miembros_%s'%request.vars.grupo)
def pedir_cat():
    from modelos.pedidos import pedidos_del_dia, query_mask
    tppp = db.productoXpedido

    fecha_reparto = K.parse_fecha(request.vars.fecha_reparto)
    igrupo        = int(request.vars.grupo)
    ipersona      = auth.user.id
    unidad        = db.personaXgrupo(persona=ipersona, grupo=igrupo).unidad
    if request.vars.categoria:
        icat       = int(request.vars.categoria)
        cat_nombre = db.categorias_productos(icat).nombre
    else:
        icat       = -1
        cat_nombre = "Todas las categorias"

    repartosp, repartosc = pedidos_del_dia(fecha_reparto, igrupo, solo_abiertos=True)
    pedidos_p = [row.id for row in repartosp]
    pedidos_c = [row.id for row in repartosc]

    query_p = ((tppp.pedido.belongs(pedidos_p))  &
               (tppp.producto==db.producto.id) &
               (tppp.temporada==True) &
               (tppp.activo==True) &
               (tppp.categoria==db.categorias_productos.id) &
               (db.producto.productor==db.productor.id))
    query_c = (query_mask(igrupo) &
               (tppp.pedido.belongs(pedidos_c))  &
               (tppp.producto==db.producto.id) &
               (tppp.temporada==True) &
               (tppp.activo==True) &
               (tppp.categoria==db.categorias_productos.id) &
               (db.producto.productor==db.productor.id))
    if icat > 0:
        query_p &= (tppp.categoria==icat)
        query_c &= (tppp.categoria==icat)

    productos_p = db(query_p) \
                  .select(tppp.ALL,
                          db.productor.nombre, db.categorias_productos.nombre)

    productos_c = db(query_c) \
                  .select(tppp.ALL,
                          db.productor.nombre, db.categorias_productos.nombre)

    from bloques_vista_pedir import PedirProductosController
    form = PedirProductosController(
        list(productos_p) + list(productos_c),
        ipersona, igrupo,
        pedidos = dict(chain(((pid, False) for pid in pedidos_p),
                             ((pid, True ) for pid in pedidos_c)) )
        )

    if form.accepts(request.vars):
        response.js = "$('#carrito').trigger('actualiza_carrito');"
        response.flash    = form.feedback
    elif form.errors:
        response.flash = form.feedback
    return dict(elige_productos=form,
                cat_nombre = cat_nombre)

@auth.requires(auth.has_membership(role='miembros_%s'%request.vars.grupo))
def carrito():
    from modelos.pedidos import toda_la_peticion
    igrupo = int(request.vars.grupo)
    grupo  = db.grupo(igrupo)
    ipersona = auth.user.id
    fecha_reparto = request.vars.fecha_reparto
    tablas, gran_total = toda_la_peticion(igrupo, fecha_reparto, ipersona)
    if grupo.opcion_ingresos != CONT.OPCIONES_INGRESOS.NO_CONT:
        unidad = db.personaXgrupo(persona=ipersona, grupo=igrupo).unidad
        saldo = db.cuenta(tipo=CONT.TIPOS_CUENTA.UNIDAD_GRUPO,
                          owner=unidad, target=igrupo,
                          activa=True
        ).get('saldo')
    else:
        saldo = None

    recibir_email = bool(db.aviso_email(persona=ipersona, grupo=igrupo, fecha_reparto=fecha_reparto))
    return dict(tablas=tablas,
                gran_total=gran_total,
                grupo=igrupo,
                pedidos=[row[0] for row in tablas],
                fecha_reparto=fecha_reparto,
                recibir_email=recibir_email,
                saldo=saldo)

@scope.set('miembro')
#@requires_get_arguments(('grupo', db.grupo), ('fecha_reparto', 'date'))
@auth.requires_membership('miembros_%s'%request.vars.grupo)
def pedir():
    fecha_reparto = K.parse_fecha(request.vars.fecha_reparto)
    igrupo     = int(request.vars.grupo)
    ipersona = auth.user.id
    unidad = db.personaXgrupo(persona=ipersona, grupo=igrupo).unidad

    response.title = T('Pedir para el %s de %s')%(fecha_reparto.day,C.MESES[fecha_reparto.month])

    response.files.append(URL('static', 'js/pedir.js'))

    pedidos_a = db((db.pedido.abierto==True) &
                   (db.pedido.productor==db.productor.id) &
                   (db.productor.grupo==igrupo) &
                   (db.pedido.fecha_reparto==fecha_reparto)) \
                .select(db.pedido.ALL, db.productor.nombre)

    pedidos_c = db((db.pedido.abierto==True) &
                   (db.proxy_pedido.pedido==db.pedido.id) &
                   (db.pedido.productor==db.productor.id) &
                   (db.proxy_pedido.activo==True) &
                   (db.proxy_pedido.grupo==igrupo) &
                   (db.proxy_pedido.fecha_reparto==fecha_reparto) &
                   (db.productor.grupo==db.grupo.id)) \
                  .select(db.pedido.ALL,
                          db.proxy_pedido.comentarios,
                          db.productor.nombre,
                          db.grupo.nombre,
                          orderby=db.pedido.fecha_reparto)
    pedidos = list(pedidos_a) + list(pedidos_c)

    cats_fecha = db( db.productoXpedido.pedido.belongs([row.pedido.id for row in pedidos]) &
                    (db.productoXpedido.categoria==db.categorias_productos.id))\
                 .select(db.categorias_productos.id, db.categorias_productos.nombre,
                         groupby=db.categorias_productos.id,
                         orderby=db.categorias_productos.nombre)
    return dict(fecha_reparto=fecha_reparto,
                 pedidos=pedidos,
                 categorias=cats_fecha,
                 grupo=igrupo,
                 unidad=unidad)

@scope.set('miembro')
#@requires_get_arguments(('grupo', db.grupo))
@auth.requires_membership('miembros_%s'%request.vars.grupo)
def vista_pedidos():
    igrupo    = int(request.vars.grupo)

    today = request.now.date()
    pedidos = defaultdict(list)

    #TODO: refactor usando K.dame_pedidos_por_fecha

    s = db((db.pedido.abierto==True) &
           (db.pedido.productor==db.productor.id) &
           (db.productor.grupo==igrupo)).select(db.pedido.ALL)

    cantidad_total = db.item.cantidad.sum()
    def has_pedido(pid):
        ct = db((db.peticion.productoXpedido == db.productoXpedido.id) &
                (db.productoXpedido.pedido==pid) &
                (db.peticion.persona==auth.user.id) &
                (db.peticion.grupo==igrupo) ) \
             .select(cantidad_total)
        return ct.first()[cantidad_total]

    for row in s:
        pedidos[row.fecha_reparto].append(
            (row.productor.nombre, row.id, has_pedido(row.id), False)
        )

    s2 = db((db.pedido.abierto==True) &
            (db.proxy_pedido.pedido==db.pedido.id) &
            (db.pedido.productor==db.productor.id) &
            (db.proxy_pedido.activo==True) &
            (db.proxy_pedido.grupo==igrupo))  \
          .select(db.pedido.id, db.proxy_pedido.fecha_reparto,
                  db.productor.nombre,
                  orderby=db.proxy_pedido.fecha_reparto)
    for row in s2:
        pedidos[row.proxy_pedido.fecha_reparto].append(
            (row.productor.nombre, row.pedido.id, has_pedido(row.pedido.id), True)
        )

    return dict(pedidos=pedidos, grupo=igrupo)

@scope.set('miembro')
#@requires_get_arguments(('grupo', db.grupo))
@auth.requires_membership('miembros_%s'%request.vars.grupo)
def pedidos_activos():
    igrupo = int(request.vars.grupo)
    grupo  = db.grupo(igrupo)
    response.title = T('Pedidos del grupo %s')%grupo.nombre
    return dict(grupo=igrupo)

@auth.requires(auth.has_membership(role='miembros_%s'%request.vars.grupo))
def eliminar_peticion():
    from modelos.pedidos import peticion_a_item
    igrupo = int(request.vars.grupo)
    grupo  = db.grupo(igrupo)
    ipedido = int(request.vars.pedido)
    ipersona = auth.user.id
    peticiones = [row.id for row in db(
       (db.peticion.grupo==igrupo) &
       (db.peticion.persona==ipersona) &
       (db.peticion.productoXpedido==db.productoXpedido.id) &
       (db.productoXpedido.pedido==ipedido)
    ).select(db.peticion.id)]
    db(db.peticion.id.belongs(peticiones)).delete()
    unidad = db.personaXgrupo(persona=ipersona, grupo=igrupo).unidad
    # incidente 21-9-21 Correhuela: hay que poner item e item_grupo a cero para evitar confusiones
    peticion_a_item(ipedido, igrupo, unidad)
    return 'web2py.flash("%s");'%T('Hemos eliminado tu petición.')

@scope.set('miembro')
#@requires_get_arguments(('grupo', db.grupo))
@auth.requires_membership('miembros_%s'%request.vars.grupo)
def informes():
    from modelos.pedidos import dame_pedidos_por_fecha

    igrupo = int(request.vars.grupo)
    grupo  = db.grupo(igrupo)

    response.title = T('Informes del grupo %s')%grupo.nombre
    #Separa abiertos, cerrados, historicos
    q = request.vars.tipo_pedidos
    if q not in dEstadosPedidoSingular:
        q = EstadosPedido.ABIERTOS
    if q == EstadosPedido.RECIENTES:
        d_pedidos = defaultdict(lambda:([],[]))
        for tipo in (EstadosPedido.ABIERTOS, EstadosPedido.PENDIENTES_RECIENTES,
                     EstadosPedido.CERRADOS):
            pedidos_tipo = dame_pedidos_por_fecha(igrupo, tipo,
                                           todos_los_coordinados=True)
            for fecha, (propios, coordinados) in pedidos_tipo:
                lista_propios, lista_coordinados = d_pedidos[fecha]
                lista_propios.extend(propios)
                lista_coordinados.extend(coordinados)
        fechas = sorted(d_pedidos.keys())
        pedidos = [(fecha, map(tuple, d_pedidos[fecha]))
            for fecha in fechas
        ]
    else:
        pedidos = dame_pedidos_por_fecha(igrupo, q)

    tp = dEstadosPedidoPlural[q]
    links = [(k,v) for k,v in dEstadosPedidoPlural.items()
                   if (k!=q and k!=EstadosPedido.ACTIVOS)]
    return dict(pedidos=pedidos,
                links=links, tp=tp, grupo=igrupo)

@scope.set('miembro')
#@requires_get_arguments(('grupo', db.grupo))
@auth.requires_membership(role='miembros_%s'%request.vars.grupo)
def consulta():
    igrupo = int(request.vars.grupo)
    grupo  = db.grupo(igrupo)
    unidad = db.personaXgrupo(persona=auth.user.id, grupo=igrupo).unidad

    response.title = T('Consulta tus próximos pedidos')

    from modelos.pedidos import fechas_pedidos
    fechas = [(K.format_fecha(f), f) for f in fechas_pedidos(igrupo)]
    return dict(unidad=unidad,
                grupo=igrupo,
                fechas=fechas)
