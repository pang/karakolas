# -*- coding: utf-8 -*-
# (C) Copyright (C) ?, Massimo di Piero
# (C) Copyright (C) 2012-23 Authors of karakolas


#########################################################################
## This is a samples controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
## - call exposes all registered services (none by default)
#########################################################################
 # This isn't required in Python 2.6
__metaclass__ = type
import os
import sys
import io
from cgi import FieldStorage
try:
    from PIL import Image
except ImportError:
    raise HTTP(200,"Requires the Python Imaging Library installed")
from gluon.contrib import simplejson as json

now=datetime.datetime.now()
split_path = os.path.split
split_ext = os.path.splitext
path_exists = os.path.exists
normalize_path = os.path.normpath
absolute_path = os.path.abspath
output=os.path.join(request.folder,'static','test.txt')

#tienePermisos = auth.has_membership('admins_%s'%request.vars.grupo)
tienePermisos = (auth.has_membership('admins_%s'%request.vars.grupo) or
                 auth.has_membership('miembros_%s'%request.vars.grupo))
tienePermisosLectura = (tienePermisos or
                        Permisos.admin_o_miembro_de_un_grupo_de_la_red(request.vars.grupo))

@scope.set('miembro')
#TODO: tipo de argumento "path"
##@requires_get_arguments(('grupo', db.grupo), ('path','path'))
#@requires_get_arguments(('grupo', db.grupo))
@auth.requires(tienePermisos)
def index():
    igrupo = int(request.vars.grupo)
    grupo  = db.grupo(igrupo)
    row = db((db.allfiles.filepath==request.vars.path) & (db.allfiles.grupo==igrupo)).select().first()
    if not row:
        if request.vars.path:
            response.flash= T('No se ha encontrado el fichero')
        path='/'
    else:
        path=request.vars.path

    response.title = T('Archivos de %s')%grupo.nombre

    appname=request.application

    response.files.append(URL('static', 'scripts/jquery_filetree/jqueryFileTree-new.css'))
    response.files.append(URL('static', 'scripts/jquery_contextmenu/jquery.contextMenu-1.01.css'))
    response.files.append(URL('static', 'styles/filemanager.css'))
    response.files.append(URL('static', 'scripts/jquery.splitter/jquery.splitter.css'))
    response.files.append(URL('static', 'jquery-browser-plugin/dist/jquery.browser.min.js'))
    response.files.append(URL('static', 'scripts/jquery.splitter/jquery.splitter-1.5.1.js'))
    response.files.append(URL('static', 'scripts/jquery_filetree/jqueryFileTree-new.js'))
    response.files.append(URL('static', 'scripts/jquery_contextmenu/jquery.contextMenu-1.01.js'))
    response.files.append(URL('static', 'scripts/jquery.impromptu-3.2.min.js'))
    response.files.append(URL('static', 'scripts/jquery.tablesorter.min.js'))
    response.files.append(URL('static', 'scripts/filemanager.config.js'))
    response.files.append(URL('static', 'scripts/filemanager.js', vars=dict(reload=1)))
    return dict(appname=appname, grupo=igrupo, path=path)

@scope.set('miembro')
#TODO: tipo de argumento "path"
##@requires_get_arguments(('grupo', db.grupo), ('path','path'))
#@requires_get_arguments(('grupo', db.grupo))
@auth.requires(tienePermisosLectura)
def vista():
    igrupo = int(request.vars.grupo)
    grupo  = db.grupo(igrupo)
    path   = request.vars.path or '/'

    row = db((db.allfiles.filepath==path) & (db.allfiles.grupo==igrupo)).select().first()
    if not row or row.filetype not in ('html', 'txt'):
        redirect(URL(c='ficheros', f='index.html', vars=dict(grupo=igrupo)))

    response.title = T('Archivo: %s')%path

    return dict(content=row.content,
                grupo=igrupo,
                path=path,
                filetype=row.filetype,
                tienePermisoEscritura=tienePermisos)

@auth.requires(tienePermisos)
@service.json
def getinfo(param=False, grupo=None, path=None):
    grupo = grupo or request.vars.grupo
    path = path or param
    igrupo=int(request.vars.grupo)

    row=db((db.allfiles.filepath==path)&(db.allfiles.grupo==igrupo)).select().first()
    abspath=request.folder+path
    iconfolder='static/images/fileicons/'
    absiconlocation=request.folder+iconfolder
    iconlocation='/'+request.application+'/'+iconfolder
    filename=row.filename
    filetype=row.filetype
    destacado=row.destacado
    datecreated=row.datecreated
    datemodified=row.datemodified
    filesize=row.filesize
    thefile = {
            'Filename' : filename,
            'FileType' : '',
            'Destacado' : destacado or '',
            'Preview' : iconlocation+'_Open.png' if filetype=='dir' else '' ,
            'FilePreview':'',
            'Path' : path,
            'Error' : '',
            'Code' : 0,
            'Properties' : {
                    'Date Created' : '',
                    'Date Modified' : '',
                    'Width' : '',
                    'Height' : '',
                    'Size' : ''
             }
            }

    imagetypes = set(['gif','jpg','jpeg','png','bmp'])

    if filetype=='dir':
        thefile['FileType'] = 'Directory'
    else:
        thefile['FileType'] = filetype
        if filetype in imagetypes:
            thefile['Preview']='/'+request.application+'/default/download/'+row.file
        elif filetype=="mp3" or filetype=="flv":
            thefile['FileType']='media'
            embedfile='/'+request.application+'/default/download/'+row.file
            thefile['FilePreview']=str(plugin_mediaplayer(embedfile,400,300))
            previewPath = iconlocation + filetype.lower() + '.png'
            abspreviewPath=absiconlocation+filetype.lower()+'.png'
            thefile['Preview'] = previewPath if os.path.exists(abspreviewPath) else iconlocation+'default.png'
        else:
            previewPath = iconlocation + filetype.lower() + '.png'
            abspreviewPath=absiconlocation+filetype.lower()+'.png'
            thefile['Preview'] = previewPath if os.path.exists(abspreviewPath) else iconlocation+'default.png'

    thefile['Properties']['Date Created'] = datecreated
    thefile['Properties']['Date Modified'] = datemodified
    thefile['Properties']['Size'] = filesize
    return thefile

#TODO: tipo de argumento "path"
##@requires_get_arguments(('grupo', db.grupo), ('path','path'))
@auth.requires(tienePermisos)
@service.json
def getdata(grupo, path):
    igrupo=int(grupo)
    data=db((db.allfiles.filepath==path)&(db.allfiles.grupo==igrupo)).select().first()
    textdata=data.content
    return textdata

#TODO: tipo de argumento "path"
##@requires_get_arguments(('grupo', db.grupo), ('path','path'))
@auth.requires(tienePermisos)
def updatedata():
    igrupo=int(request.vars.grupo)
    data=request.vars.senddata
    path=request.vars.getpath
    db((db.allfiles.filepath==path)&(db.allfiles.grupo==igrupo)).update(content=data)
    return T('El fichero ha sido actualizado.')

#TODO: tipo de argumento "path"
##@requires_get_arguments(('grupo', db.grupo), ('path','path'))
#@requires_get_arguments(('grupo', db.grupo))
@auth.requires(tienePermisos)
@service.json
def getfolder(grupo, path):
    igrupo=int(grupo)
    rows=db((db.allfiles.parentpath==path)&(db.allfiles.grupo==igrupo)).select()
    result = {}
    for row in rows:
        result[row.filepath]=getinfo(row.filepath)
    return result

def print_dict(d):
    if isinstance(d, str):
        return '"'+ d +'"'
    if isinstance(d, dict):
        return '{%s}'%','.join('"%s":%s'%(k,print_dict(v)) for k,v in d.items())
    return str(d)

#@requires_get_arguments(('grupo', db.grupo))
@auth.requires(tienePermisos)
def browsefiles():
    igrupo=int(request.vars.grupo)
    files=db((db.allfiles.grupo==igrupo)&(db.allfiles.filetype !='dir')).select()
    result={}
    filetypes= set(['gif','jpg','png','bmp', 'jpeg'])
    for rfile in files:
        if rfile.filepath[-3:] in filetypes:
            result[rfile.filepath]=getfileinfo(rfile.filepath)
    return dict(result=print_dict(result),cknum=request.vars.CKEditorFuncNum)

@auth.requires(tienePermisos)
def getfileinfo(path):
    igrupo=int(request.vars.grupo)
    row=db((db.allfiles.filepath==path)&(db.allfiles.grupo==igrupo)).select().first()
    abspath=request.folder+path
    iconfolder='static/images/fileicons/'
    absiconlocation=request.folder+iconfolder
    iconlocation='/'+request.application+'/'+iconfolder
    filename=row.filename
    filetype=row.filetype
    filesize=row.filesize
    thefile = {
            'Filename' : filename,
            'FileType' : '',
            'Preview' : iconlocation+'_Open.png' if filetype=='dir' else '' ,
            'FilePreview':'',
            'Path' : path,
            'Error' : '',
            'Code' : 0,
            'Properties' : {
                    'Width' : '',
                    'Height' : '',
                    'Size' : ''
             }
            }

    imagetypes = set(['gif','jpg','jpeg','png','bmp'])

    if filetype=='dir':
        thefile['FileType'] = 'Directory'
    else:
        thefile['FileType'] = filetype
        if filetype in imagetypes:
            thefile['Preview']='/'+request.application+'/default/download/'+row.file
        elif filetype=="mp3" or filetype=="flv":
            thefile['FileType']='media'
            embedfile='/'+request.application+'/default/download/'+row.file
            thefile['FilePreview']=str(plugin_mediaplayer(embedfile,400,300))
            previewPath = iconlocation + filetype.lower() + '.png'
            abspreviewPath=absiconlocation+filetype.lower()+'.png'
            thefile['Preview'] = previewPath if os.path.exists(abspreviewPath) else iconlocation+'default.png'
        else:
            previewPath = iconlocation + filetype.lower() + '.png'
            abspreviewPath=absiconlocation+filetype.lower()+'.png'
            thefile['Preview'] = previewPath if os.path.exists(abspreviewPath) else iconlocation+'default.png'
    thefile['Properties']['Size'] = filesize
    return thefile


#@requires_get_arguments(('grupo', db.grupo))
@auth.requires(tienePermisos)
def dirlist():
    igrupo=int(request.vars.grupo)
    import re
    r=['<ul class="jqueryFileTree" style="display: none;">']
    path=request.post_vars.dir or '/'
    rows=db((db.allfiles.parentpath==path)&(db.allfiles.grupo==igrupo)).select()
    for row in rows:
        if row.filetype=='dir':
            r.append('<li class="directory collapsed"><a href="#" rel="%s">%s</a></li>' % (row.filepath,row.filename))
    r.append('</ul>')
    return r

#TODO: tipo de argumento "path"
##@requires_get_arguments(('grupo', db.grupo), ('path','path'), ('name', 'string'))
@auth.requires(tienePermisos)
@service.json
def addfolder(param, grupo, name, path):
    igrupo=int(grupo)
    filename=name.replace(' ','_')
    parentpath=path
    filepath=parentpath+filename+'/'
    rows=db((db.allfiles.filepath==filepath)&(db.allfiles.grupo==igrupo)).select()
    recordexists=len(rows)
    if not recordexists:
        db.allfiles.insert(filename=filename,filepath=filepath,parentpath=parentpath,filetype='dir',datecreated=now,grupo=igrupo)
        result= {
                    'Parent' : parentpath,
                    'Name' : filename,
                    'Error' :'',
                    'Code' :0
                    }
    else:
        result = {
                    'Path' : parentpath,
                    'Name' : filename,
                    'Code' : 1,
                    'Error' : T('Esa carpeta ya existe')
                }
    return result

#TODO: tipo de argumento "path"
##@requires_get_arguments(('grupo', db.grupo), ('path','path'), ('name', 'string'))
@auth.requires(tienePermisos)
@service.json
def newfile(param, grupo, name, path):
    igrupo  = int(grupo)
    filename = name.replace(' ','_')
    parentpath = path
    filetype = os.path.splitext(filename)[1][1:]
    filepath = parentpath + filename
    recordexists=db((db.allfiles.filepath==filepath)&(db.allfiles.grupo==igrupo)).count()
    if not recordexists:
        template_file = os.path.join(request.folder,'private','blank_file.html')
        db.allfiles.insert(filename=filename,filepath=filepath,
                           parentpath=parentpath,filetype=filetype,
                           file=db.allfiles.file.store(open(template_file, 'rb'),filename),
                           datecreated=now,grupo=igrupo,
                           content=io.open(template_file, 'r', encoding='utf8').read())
        result= {
        'Parent' : parentpath,
        'Name' : filename,
        'Error' :'',
        'Code' :0
        }
    else:
        result = {
                    'Path' : parentpath,
                    'Name' : filename,
                    'Code' : 1,
                    'Error' : T('Ya existe un fichero con ese nombre'),
                }
    return result

#TODO: tipo de argumento "path"
##@requires_get_arguments(('grupo', db.grupo), ('file','path'))
@auth.requires(tienePermisos)
@service.json
def destacar(param, grupo, file):
    igrupo=int(grupo)
    filepath=file
    file_record=db.allfiles(filepath=filepath, grupo=igrupo)
    if file_record:
        destacado= not file_record.destacado
        file_record.update_record(destacado=destacado)
    return {
            'Name' : filepath,
            'Destacado' : destacado or '',
            'Error' :'',
            'Code' :0
            }

#TODO: tipo de argumento "path"
##@requires_get_arguments(('grupo', db.grupo), ('file','path'))
@auth.requires(tienePermisos)
@service.json
def unzip(param):
    igrupo=int(request.vars.grupo)
    filepath=request.vars.file
    parentpath,filename=os.path.split(filepath)
    foldername,_=os.path.splitext(filename)
    folderpath,_=os.path.splitext(filepath)
    folderpath=folderpath+'/'
    parentpath=parentpath+'/'

    rows=db((db.allfiles.filepath==folderpath)&(db.allfiles.grupo==igrupo)).select()
    recordexists=len(rows)
    ziprecord=db.allfiles(filepath=filepath, grupo=igrupo)
    if ziprecord and not recordexists:
        import zipfile
        targetfile = db.allfiles.file.retrieve(ziprecord.file)[1]
        zipf = zipfile.ZipFile(targetfile,'r')
        if (any(fn.startswith('/') for fn in zipf.namelist()) or
            any('..' in fn for fn in zipf.namelist())):
            result = {
                        'Path' : parentpath,
                        'Name' : filename,
                        'Code' : 1,
                        'Error' : T('Problema al descomprimir el archivo zip')
                    }
        else:
            db.allfiles.insert(filename=foldername, filepath=folderpath,
                               parentpath=parentpath, filetype='dir',
                               datecreated=now, grupo=igrupo)
            zipparent = targetfile.name + '_content'
            zipf.extractall(zipparent)
            parentpath=parentpath+foldername+'/'
            for fn in zipf.namelist():
                if fn.endswith('/'):
                    newpath = parentpath+fn[:-1]
                    newppath, newname = os.path.split(newpath)
                    db.allfiles.insert(filename=newname, filepath=newpath+'/',
                                       parentpath=newppath+'/', filetype='dir',
                                       datecreated=now, grupo=igrupo)
                else:
                    newpath   = parentpath+fn
                    newppath, newname = os.path.split(newpath)
                    _, newext = os.path.splitext(newname)
                    newext    = newext[1:]
                    newname   = str.encode(str(newname),'utf8')
                    stream    = open(os.path.join(zipparent,fn), 'rb')
                    if newext=='txt' or newext=='html':
                        dbcontent=open(os.path.join(zipparent,fn)).read()
                    else:
                        dbcontent = None
                    db.allfiles.insert(filename=newname, filepath=newpath,
                                       parentpath=newppath+'/', filetype=newext,
                                       file=db.allfiles.file.store(stream,newname),
                                       datecreated=now, content=dbcontent, grupo=igrupo)
            result= {
                        'Parent' : parentpath,
                        'Name' : filename,
                        'Error' :'',
                        'Code' :0
                        }
    else:
        result = {
                    'Path' : parentpath,
                    'Name' : filename,
                    'Code' : 1,
                    'Error' : T('Esa carpeta ya existe')
                }
    return result

#TODO: tipo de argumento "path"
##@requires_get_arguments(('grupo', db.grupo), ('path','path'))
#@auth.requires(auth.has_membership('admins_%s'%request.vars.grupo) or
#               auth.has_membership('miembros_%s'%request.vars.grupo))
@service.json
def delete(param, grupo, path):
    igrupo = int(grupo)

    auth.basic()
    if auth.user and auth.has_membership(role='admins_%d'%igrupo):
        pass
    elif auth.user and auth.has_membership(role='miembros_%d'%igrupo):
        return {
            'Error':T('No tienes permiso para borrar este fichero. Solo las administradoras pueden borrar documentos.'),
            'Code':1,
            'Path': path
           }
    else:
        return {
            'Error':T('No tienes permiso para borrar este fichero.'),
            'Code':1,
            'Path': path
           }

    filepaths=remove(path,pathset=[])
    for filepath in filepaths:
        if not filepath[-1]=='/':
            row=db((db.allfiles.filepath==filepath )&(db.allfiles.grupo==igrupo)).select().first()
            filename=row.file
            os.remove(os.path.join(request.folder,'uploads','allfiles.file', filename.split('.',2)[-1][:2],filename))
        db((db.allfiles.filepath==filepath )&(db.allfiles.grupo==igrupo)).delete()
    result={
            'Error':'No Error',
            'Code':0,
            'Path': path
           }
    return result

@auth.requires(tienePermisos)
@service.json
def rename(param, grupo, old, new):
    igrupo = int(grupo)
    oldpath = old
    newname = new.replace(' ','_')
    result={
            'type':'',
            'Old Path':'',
            'New Path':'',
            'Old Name':'',
            'New eName':'',
            'parent':'',
            'Code':0,
            'Error':''
            }
    if oldpath[-1]=='/':
        oldname=split_path(oldpath[:-1])[-1]
        parentpath=split_path(oldpath[:-1])[0]
        if not parentpath=='/':
            parentpath=parentpath+'/'
        row=db((db.allfiles.parentpath==parentpath)&(db.allfiles.filetype=='dir') \
        &(db.allfiles.filename==newname)&(db.allfiles.grupo==igrupo)).select()
        if len(row)==1:
            result['Code']=1
            result['Error']=T('Esa carpeta ya existe')
            return result
        newpath=parentpath+newname+'/'
        result['type']='dir'
        db((db.allfiles.filepath==oldpath)&(db.allfiles.grupo==igrupo)).update(filepath=newpath,filename=newname,datemodified=now)
        renamedir(oldpath,oldname,newname)
    else:
        oldname = split_path(oldpath)[-1]
        parentpath = split_path(oldpath)[0]
        if not parentpath=='/':
            parentpath=parentpath+'/'
        newpath = parentpath + newname
        row=db((db.allfiles.parentpath==parentpath)&(db.allfiles.filepath==newpath)&(db.allfiles.grupo==igrupo)).select()
        if len(row)==1:
            result['Code']=1
            result['Error']=T('Ya existe un fichero con ese nombre')
            return result
        result['type']='file'
        db((db.allfiles.filepath==oldpath)&(db.allfiles.grupo==igrupo)).update(filepath=newpath,filename=newname,datemodified=now)
    result['parent']=parentpath
    result['Old Path']= oldpath
    result['Old Name']=oldname
    result['New Path']=newpath
    result['New eName']=newname
    return result

@auth.requires(tienePermisos)
def renamedir(oldpath,oldname,newname):
    igrupo=int(request.vars.grupo)
    newpath=oldpath.replace(oldname,newname)
    rows=db((db.allfiles.parentpath==oldpath)&(db.allfiles.grupo==igrupo)).select()
    for row in rows:
        if row.filepath[-1]=='/':
            db((db.allfiles.parentpath==oldpath) \
            &(db.allfiles.grupo==igrupo)).update(parentpath=row.parentpath.replace(oldname,newname),datemodified=now)
            renamedir(row.filepath,oldname,newname)
            db((db.allfiles.filepath==row.filepath) \
            &(db.allfiles.grupo==igrupo)).update(filepath=row.filepath.replace(oldname,newname),datemodified=now)
        else:
            db((db.allfiles.filepath==row.filepath)&(db.allfiles.grupo==igrupo)). \
            update(filepath=row.filepath.replace(oldname,newname),parentpath=row.parentpath.replace(oldname,newname),datemodified=now)

#TODO: tipo de argumento "path" y "file?"
##@requires_get_arguments(('grupo', db.grupo), ('currentpath','path'), ('file','file'))
@auth.requires(tienePermisos)
def add():
    igrupo     = int(request.vars.grupo)
    parentpath = request.vars.currentpath
    file       = request.vars.file
    if (file is None) or (not isinstance(file, FieldStorage)):
        result = {
                'Path' : '/',
                'Name' : '',
                'Error' : T('Por favor elige un fichero'),
                'Code':1
            }
        return '<textarea>'+str(result)+'</textarea>'
    filename = file if isinstance(file, str) else file.filename
    filename = filename.replace(' ','_')

    filepath = parentpath+filename
    filetype = os.path.splitext(filename)[1][1:]
    row=db((db.allfiles.parentpath==parentpath) &
           (db.allfiles.filepath==filepath) &
           (db.allfiles.filename==filename) &
           (db.allfiles.grupo==igrupo)).select()
    if not len(row):
        db.allfiles.insert(
            filename=filename, filepath=filepath, parentpath=parentpath,
            filetype=filetype, file=db.allfiles.file.store(file.file,filename),
            datecreated=now, grupo=igrupo
            )
        result = {
                'Path' : parentpath,
                'Name' : filename,
                'Error' :'',
                'Code':0
            }
    else:
        result = {
                'Path' : parentpath,
                'Name' : filename,
                'Error' : T('Ya existe un fichero con ese nombre'),
                'Code':1
            }
        return '<textarea>'+str(result)+'</textarea>'
    row = db((db.allfiles.filename==filename) &
             (db.allfiles.grupo==igrupo)
            ).select().first()
    insertedfile = row.file
    outfile = open(output,'wb')
    try:
        _, filepath = db.allfiles.file.retrieve(insertedfile, nameonly=True)
        filesize    = os.path.getsize(filepath)
        if filepath[-3:]=='txt' or filepath[-4:]=='html':
            filepath = os.path.normpath(filepath)
            filein = open(filepath,'r')
            dbcontent = filein.read()
            filein.close()
            db((db.allfiles.filename==filename) &
               (db.allfiles.grupo==igrupo)
              ).update(filesize=filesize,content=dbcontent)
        else:
            db((db.allfiles.filename==filename) &
               (db.allfiles.grupo==igrupo)
              ).update(filesize=filesize)
        outfile.close()
    except:
        outfile.write(str(sys.exc_info()[1]))
        outfile.close()

    return '<textarea>'+str(result)+'</textarea>'

#TODO: tipo de argumento "path"
##@requires_get_arguments(('grupo', db.grupo), ('filename','path'))
@auth.requires(tienePermisos)
def downloadurl():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    igrupo=int(request.vars.grupo)
    filename=request.vars.filename
    if filename.endswith('html'):
        return filename
    file = db((db.allfiles.filename==filename)&(db.allfiles.grupo==igrupo)) \
           .select(db.allfiles.file).first()
    return file.file if file else ''

def download():
    igrupo=int(request.vars.grupo)
    if not request.args:
        return 'No hemos encontrado el contenido del fichero.'
    filename = request.args[0]
    if filename.endswith('html'):
        file = db((db.allfiles.filename==filename)&(db.allfiles.grupo==igrupo)) \
               .select(db.allfiles.content).first()
        response.headers['Content-Disposition'] = 'attachment; filename='+filename
        response.headers['Content-Type'] = 'text/html'
        return file.content if file else 'No hemos encontrado el contenido del fichero.'
    return response.download(request,db)

def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    session.forget()
    return service()

@auth.requires(tienePermisos)
def remove(path,pathset):
    igrupo=int(request.vars.grupo)
    if path[-1]=='/':
        pathset.append(path)
        rows=db((db.allfiles.parentpath==path)&(db.allfiles.grupo==igrupo)).select()
        for row in rows:
            if row.filetype=='dir':
                remove(row.filepath,pathset=pathset)
            else:
                pathset.append(row.filepath)
    else:
        pathset.append(path)
    return pathset
