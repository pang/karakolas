# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.


#########################################################################

def default_action(g):
    s,k = g.role.split('_')
    id = int(k)
    if s == 'admins':
        return URL(c='gestion_pedidos', f='gestion_pedidos.html', vars=dict(grupo=id))
    if s == 'red':
        return URL(c='redes', f='admin_red.html', vars=dict(grupo=id))
    if s == 'miembros':
        return URL(c='pedir', f='vista_pedidos.html', vars=dict(grupo=id))

@scope.set('no-logueado')
#@requires_get_arguments()
def index():
    slug = request.vars.slug
    if (not slug) and auth.user:
        redirect(URL(f='home.html'))
    elif not slug:
        slug = 'welcome'
    try:
        idioma_por_defecto = myconf.take('global.default_language')
    except BaseException:
        idioma_por_defecto = None
    p = (db.paginas_especiales(slug=slug, language=T.accepted_language)
         or db.paginas_especiales(slug=slug, language=idioma_por_defecto))
    response.title = (p.title if p else slug)
    form_edita_pagina = FORM(
        LABEL(T('Idioma'), _for='language'),
        INPUT(_name='language',  _id='language', _value=T.accepted_language),
        INPUT(_type='submit', _name='submit', _value='Editar')
    )
    if form_edita_pagina.process().accepted:
        redirect(URL(c='default', f='edita.html', vars=dict(slug=slug, language=form_edita_pagina.vars.language)))
    return dict(title=response.title, page=p, slug=slug, form_edita_pagina=form_edita_pagina)

@scope.set('admin-red')
#@requires_get_arguments(('slug', 'slug'))
@auth.requires_membership('webadmins')
def edita():
    slug = request.vars.slug
    language = request.vars.language
    record = db.paginas_especiales(slug=slug, language=language)
    if not record:
        rid = db.paginas_especiales.insert(slug=slug, language=language)
        record = db.paginas_especiales(rid)

    response.title = T('Editando la página %s')%slug

    form = SQLFORM(db.paginas_especiales, record)
    if form.process().accepted:
        response.flash = T('Texto de %s actualizado')%slug
    elif form.errors:
        response.flash = C.MENSAJE_ERRORES
    return dict(title=response.title, form = form, record=record)

@auth.requires_login()
def none():
    '''Necesario para que funcione el borrado de filas en las tablas
    '''
    return dict()

def version():
    return dict(version=SNEW_VERSION,
#                static_path=URL('static','.'),
                static_path=('/' + request.application +
                             ('/static' if DESARROLLO else '/static/_'+SNEW_VERSION)),
                wiki_path=(WIKI_EXTERNA or '')
    )

@scope.set('logueado')
#@requires_get_arguments()
@auth.requires_login()
def home():
    response.title = T('Vista resumen de las acciones habituales.')
    id = auth.user.id
    user = auth.user
    if not user.first_name:
        session.flash = T('¡Es muy importante que rellenes los datos de tu perfil antes de continuar!')
        redirect(URL(c='default', f='user.html', args=['profile']))

    grupos = db((db.auth_membership.user_id==id) &
                (db.auth_membership.group_id==db.auth_group.id)) \
            .select(db.auth_group.ALL)
    gruposa = [g for g in grupos if (g.role.startswith('admins') or
                                 g.role.startswith('red') or
                                 g.role.startswith('miembros'))]
    if not gruposa:
        redirect(URL(c='default', f='listas_de_espera.html'))
    elif len(gruposa)==1:
        redirect(default_action(gruposa[0]))
    opciones = []
    if len(grupos)==1:
        redirect(default_action(g))
    from menu import group_menu
    for g in grupos:
        if g.role == 'webadmins':
            continue
        s,k = g.role.split('_')
        id = int(k)
        if s not in ('admins', 'red', 'miembros'):
            continue
        opciones.append((g.description, group_menu(g.role)[-1], s, id))
    return dict(title=response.title, opciones=opciones)

def user_external():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    form = SQLFORM.factory(
        Field('username'),
        Field('password', 'password')
    )
    #session=None no es peligroso aqui, no tiene sentido un ataque CSRF si conocen user+pass
    if form.accepts(request.vars, session=None):
        success = auth.login_bare(form.vars.username, form.vars.password)
        if success:
            redirect(request.vars._next or URL(c='default', f='home.html'))
        else:
            #Si el user+pass no es correcto, redirigimos para que aparezcan todos los
            #menus, los errores se vean bonitos etc.
            redirect(URL(c='default', f='user.html', args=['login'], vars=dict(
                errors='1', username_login_error=form.vars.username)
            ))
    return dict(form=form)

@scope.set('logueado')
def user():
    if request.extension not in ('html', 'load'):
        redirect(URL(c='default', f='user', extension='html',
                     args=request.args,
                     vars=request.get_vars))
    if not request.args:
        if not auth.user:
            request.args = ['login']
        else:
            redirect(URL(c='default', f='index.html'))
    action = request.args[0]
    if request.args:
        response.title = T(request.args[0].replace('_',' ').capitalize())
    if action == 'login':
        form = SQLFORM.factory(
            Field('username'),
            Field('password', 'password')
        )
        for el in form.elements('input'):
            el.add_class('vete_sin_problema')
        form.vars.username = request.vars.username_login_error
        if form.accepts(request.vars, session):
            success = auth.login_bare(form.vars.username, form.vars.password)
            if request.extension == 'load':
                if success:
                    response.js = ";Loader.reload_menus();"
                    redirect(request.vars._next or '/default/home.load')
                else:
                    form.errors.username = T('Usuaria o password incorrectos')
                    form.errors.password = T('Usuaria o password incorrectos')
                    return dict(form=form)
            return dict(error=0 if success else 1, form=form)
        elif request.vars.errors:
            form.errors.username = T('Usuaria o password incorrectos')
            form.errors.password = T('Usuaria o password incorrectos')
        return dict(form=form)
    if action == 'logout':
        auth.logout(next=None)
        if request.extension == 'load':
            response.js = ";Loader.reload_menus();Loader.load_link('/default/index.load');"
        return dict(error=0, form=None)
    response.title = T(request.args[0].replace('_',' ').capitalize())
    if action=='reset_password':
        #fix: hacemos aqui el redirect de prevent_password_reset_attacks que tuvimos que
        #deshabilitar pq en ese código hacen un redirect sin atender a auth.setting.client_side
        #admito que no entiendo a qué propósito sirve esta redirección, no está documentado
        #en el código, ni el commit, ni en la lista web2py-dev (pang 06-12-15)
        key = request.vars.key
        if key:
            session._reset_password_key = key
            redirect(URL(f='user.html', args=['reset_password']))
        else:
            request.vars.key = session._reset_password_key
        #después de un reset_password exitoso, hace login automáticamente, y queremos
        #recargar los menús. Tengo que usar una técnica indirecta porque hay un redirect
        #inevitable en gluon.tools
        auth_form = auth.reset_password(next=URL(c='default', f='reload_menus.html'))
    elif action=='register':
        #después de un register exitoso, hace login automáticamente, y queremos
        #recargar los menús. Tengo que usar una técnica indirecta porque hay un redirect
        #inevitable en gluon.tools
        auth_form = auth.register(next=URL(c='default', f='reload_menus.html'))
    else:
        auth_form = auth()
    return dict(error=0, form=auth_form)

def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request,db)

def reload_menus():
    response.js = ";Loader.reload_menus();Loader.load_link('/default/index.load');"
    return dict()

#########################################################################

@scope.set('logueado')
def wrong_parameters():
    response.title = T('Error en los argumentos')

    if request.get_vars:
        c,f = request.args
        n,v = next(iter(request.get_vars.items()))
    else:
        c,f,n = request.args
        v = None
    return dict(title=response.title, c=c, f=f, n=n, v=v,
                full_url=request.env.http_referer)

#########################################################################

@scope.set('logueado')
@auth.requires_login()
def borrar_datos_personales():
    from modelos.usuarios import borrar_datos_personales

    response.title = T('Borrar tus datos personales')
    f = FORM(FIELDSET(T('Confirma que quieres eliminar tus datos personales y desactivar tu cuenta: '),
                      INPUT(_type='checkbox', _name='confirma_borrado')),
             INPUT(_type='submit'))
    if f.process().accepted:
        if f.vars.confirma_borrado:
            borrar_datos_personales(auth.user.id)
            session.flash = T('Tus datos han sido borrados. Te deseamos buena suerte.')
            auth.logout()
        else:
            response.flash = T('Tienes que marcar la casilla, compañera')
    return dict(title=response.title, form=f)

#@requires_get_arguments(('group_id', db.auth_group))
@auth.requires(auth.has_membership(group_id=request.vars.group_id) or
               auth.has_membership('webadmins'))
def lista_miembros():
    gid = int(request.vars.group_id)
    for k,v in request.vars.items():
        if k.startswith('del_'):
            id = int(k[4:])
            db((db.auth_membership.group_id==gid) &
               (db.auth_membership.user_id==id)).delete()
    miembros = db(db.auth_membership.group_id==gid) \
               .select(db.auth_membership.user_id)
    usuarios = [db.auth_user[row.user_id] for row in miembros]
    f = FORM(TABLE(TR(TH('usuario'), TH('Nombre'),
                      TH('Eliminar del grupo')),
                   *[TR(TD(user.username),
                        TD('%s %s'%(user.first_name,user.last_name)),
                        TD(INPUT(_value='Eliminar', _type='checkbox', _name='del_%s'%user.id) ))
                     for user in usuarios]),
            INPUT(_type='submit', _name='submit', _value='Sacar del grupo a los miembros seleccionados', _class='btn btn-primary'  ))
    return dict(lista_miembros=f, hay_miembros=bool(miembros))

#@requires_get_arguments(('group_id', db.auth_group))
@auth.requires(auth.has_membership(group_id=request.vars.group_id) or
               auth.has_membership('webadmins'))
def crud_auth_group():
    igroup = int(request.vars.group_id)
    role   = db.auth_group(igroup).role
    description   = db.auth_group(igroup).description
    if not (role.startswith('admin') or role.startswith('red')):
        redirect(auth.settings.on_failed_authorization)
    new_member = FORM(FIELDSET(
               T('Nombre de usuaria: '),
               A(IMG(_src=URL('static', 'images/info.png'), _alt="info"),
               ' ',
                 SPAN('Nombre de usuaria (username) de la persona que se va a incorporar a "%s"'%description),
                 _href='#', _class='my_tooltip'),
                               INPUT(_name='miembro_username')),
                       INPUT(_type='submit', _name='incluir_miembro',
                             _value=T('Incluir miembro')))

    if new_member.accepts(request.vars):
        usuario = db.auth_user(username=request.vars.miembro_username)
        if usuario:
            if db((db.auth_membership.group_id==igroup)&
                  (db.auth_membership.user_id==usuario.id)).count()>0:
                response.flash = 'el usuario ya está en el grupo'
            else:
                db.auth_membership.insert(**dict(group_id=igroup, user_id=usuario.id))
                response.flash = 'ok'
        else:
            response.flash = 'Usuario desconocido'
    return dict(new_member=new_member, group=igroup, description=description)

@scope.set('logueado')
#@requires_get_arguments()
@auth.requires_login()
def listas_de_espera():
    response.title = T('Grupos con lista de espera')

    lespera = db((db.grupo.red==False) & (db.grupo.lista_de_espera==True)).select(db.grupo.id)
    ls = DIV(*[LOAD('default', 'info_grupo.load', ajax=True, vars=dict(grupo=row.id))
                for row in lespera])
    return dict(title=response.title,
                lista=ls)

#@requires_get_arguments(('grupo', db.grupo))
@auth.requires_login()
def info_grupo():
    igrupo = int(request.vars.grupo)
    grupo  = db.grupo[igrupo]
    def info_form():
        r1 = db.personaXgrupo(grupo=igrupo, persona=auth.user_id)
        r2 = db.personaXlistaespera(grupo=igrupo, persona=auth.user_id)
        if r1 and r1.activo:
            status = T('Estás en el grupo %s')%grupo.nombre
            accion = T('Irme del grupo')
        elif r2 and r2.activo:
            status = T('Estás en la lista de espera del grupo %s')%grupo.nombre
            accion = T('Quitarme de la lista de espera')
        elif grupo.prueba:
            status = T('No estás en el grupo de prueba: %s')%grupo.nombre
            accion = T('Incorporarme al grupo')
        else:
            status = T('No estás en la lista de espera del grupo %s')%grupo.nombre
            accion = T('Incorporarme a la lista de espera')
        return FORM(H5(grupo.nombre),P(grupo.direccion), P(grupo.descripcion),
                     P(EM(status)),
                     INPUT(_value=accion,
                           _type='submit', _name='entrar'))
    t=info_form()
    if t.accepts(request.vars):
        r1 = db.personaXgrupo(grupo=igrupo, persona=auth.user_id)
        r2 = db.personaXlistaespera(grupo=igrupo, persona=auth.user_id)
        if r1 and r1.activo:
            r1.update_record(activo=False)
            r2.update_record(activo=False)
            imiembros = auth.id_group('miembros_%d'%igrupo)
            db((db.auth_membership.group_id==imiembros) &
               (db.auth_membership.user_id==auth.user.id)).delete()
            if grupo.prueba:
                iadmins = auth.id_group('admins_%d'%igrupo)
                db((db.auth_membership.group_id==iadmins) &
                   (db.auth_membership.user_id==auth.user.id)).delete()
            response.flash = 'Ya no estás en el grupo %s'%grupo.nombre
        elif r2 and r2.activo:
            r2.update_record(activo=False)
            response.flash = XML(T('Te hemos quitado de la lista de espera de %s')%grupo.nombre)
        else:
            if grupo.prueba:
                if r1:
                    r1.update_record(activo=True)
                else:
                    from random import randint
                    db.personaXgrupo.insert(grupo=igrupo, persona=auth.user_id,
                            unidad=randint(1,10))
                imiembros = auth.id_group('miembros_%d'%igrupo)
                iadmins = auth.id_group('admins_%d'%igrupo)
                db.auth_membership.update_or_insert(group_id=imiembros, user_id=auth.user_id)
                db.auth_membership.update_or_insert(group_id=iadmins, user_id=auth.user_id)
                if not r2:
                    db.personaXlistaespera.insert(grupo=igrupo, persona=auth.user_id,
                                                  fecha_de_solicitud=request.now,
                                                  activo=False)
                response.flash = 'Te hemos incorporado al grupo de prueba, como miembro y como admin: %s'%grupo.nombre
            else:
                if r2:
                    r2.update_record(activo=True)
                else:
                    db.personaXlistaespera.insert(grupo=igrupo, persona=auth.user_id,
                                                  fecha_de_solicitud=request.now)
                response.flash = 'Te hemos incorporado a la lista de espera de %s'%grupo.nombre
        t=info_form()
        response.js = ";Loader.reload_menus();"
    return dict(form=t)

def menu():
    if request.extension == 'json':
        #session.forget()
        from menu import build_menu
        return dict(menu = build_menu())
    response.js = "web2py.activate_menus();"
    return CAT(MENU(response.menu_usuario,
                    _class='nav navbar-nav navbar-right',
                    li_class='dropdown',
                    ul_class='dropdown-menu'),
               MENU(response.menu,
                    _class='nav navbar-nav',
                    li_class='dropdown',
                    ul_class='dropdown-menu')
              )
