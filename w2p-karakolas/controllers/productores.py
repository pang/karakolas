# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#
#########################################################################

@scope.set(scope_grupo_o_red)
#@requires_get_arguments(('grupo', db.grupo))
@auth.requires_membership('admins_%s'%request.vars.grupo)
def index():
    igrupo = int(request.vars.grupo)
    grupo  = db.grupo(igrupo)
    if (grupo.red):
        response.title = T('Productores de la red %s')%grupo.nombre
    else:
        response.title = T('Productores del grupo %s')%grupo.nombre
    return dict(grupo=request.vars.grupo)

@scope.set(scope_grupo_o_red)
#@requires_get_arguments(('grupo', db.grupo))
@auth.requires_membership('admins_%s'%request.vars.grupo)
def vista_productores():
    igrupo = int(request.vars.grupo)
    lista_productores = db(db.productor.grupo==igrupo).select(db.productor.ALL,
            orderby=~db.productor.activo | db.productor.nombre
    )
    return dict(productores=lista_productores)

@scope.set(scope_grupo_o_red)
#@requires_get_arguments(('grupo', db.grupo))
@auth.requires_membership('admins_%s'%request.vars.grupo)
def nuevo_productor():
    from modelos.productores import nuevo_productor

    igrupo = int(request.vars.grupo)
    grupo  = db.grupo(igrupo)
#    response.title = T('Nuevo productor para el grupo %s')%grupo.nombre

    form_nuevo_productor  = SQLFORM(
        db.productor,
        fields=['nombre', 'email', 'telefono', 'direccion', 'descripcion', 'info_pedido',
                'comentarios_internos', 'formula_coste_extra_pedido'],
        formstyle='bootstrap3_stacked',
        _id='form_nuevo_productor'
    )

    if form_nuevo_productor.process(
            dbio=False, formname='form_nuevo_productor'
        ).accepted:
        form_nuevo_productor.vars.id = nuevo_productor(igrupo, form_nuevo_productor.vars)
        response.flash = T('Se ha creado un nuevo productor: %s.')%form_nuevo_productor.vars.nombre
        response.js = '$(document).trigger("nuevo_productor")';
    elif form_nuevo_productor.errors:
        response.flash = T('El formulario de nuevo productor tiene errores. Por favor revisalos.')

    nuevo_productor_url = FORM(
        INPUT(_type='string', _name='link_to_csv', requires=IS_URL()),
        INPUT(_type='submit', _class='btn btn-primary')
    )

    if nuevo_productor_url.process(formname='form_importar_productor').accepted:
        import urllib.request, urllib.parse, urllib.error
        u = urllib.request.urlopen(nuevo_productor_url.vars.link_to_csv)
        t = u.read().decode('utf8')
        try:
            from modelos.productores import importar_productor
            from modelos.grupos import grupos_y_subredes_de_la_red
            iproductor = importar_productor(igrupo,t)
            response.flash = T('Se ha incorporado el nuevo productor.')
            response.js = '$(document).trigger("nuevo_productor")';
        except SyntaxError:
            response.flash = T('No se ha podido incorporar al nuevo productor. Por favor comprueba que esa es la url correcta.')
    elif nuevo_productor_url.errors:
        response.flash = T('La url tiene errores. Por favor revisalos.')
    return dict(nuevo_productor=form_nuevo_productor,
                nuevo_productor_url=nuevo_productor_url,
                grupo=igrupo
                )

@scope.set('miembro')
#@requires_get_arguments(('grupo', db.grupo))
@auth.requires_membership('miembros_%s'%request.vars.grupo)
def info_productores():
    igrupo = int(request.vars.grupo)
    grupo  = db.grupo(igrupo)

    response.title = T('Productores del grupo %s')%grupo.nombre
    propios = db(db.productor.grupo==igrupo).select(db.productor.ALL)
    lista_pc = []
    for row in db(db.grupoXred.grupo==igrupo).select(db.grupoXred.red):
        lista_pc.append((row.red,
                         db(db.productor.grupo==row.red) \
                                .select(db.productor.ALL)))
    return dict(productores=propios,
                 lista_pc=lista_pc,
                 igrupo=igrupo, nombre_grupo=db.grupo[igrupo].nombre)

@scope.set(scope_grupo_o_red)
#@requires_get_arguments(('productor', db.productor))
@auth.requires_membership(Permisos.grupo_del_productor(request.vars.productor))
def edita_productor():
    from evalua_coste_extra import chequea_coste_extra_en_form

    iproductor = int(request.vars.productor)
    productor  = db.productor[iproductor]
    igrupo     = productor.grupo
    grupo      = db.grupo(igrupo)
    es_red = grupo.red

    response.title = T('Editando los datos del productor %s')%productor.nombre

    contabilidad = ( grupo.opcion_ingresos != CONT.OPCIONES_INGRESOS.NO_CONT )

    fields = ['nombre', 'email', 'telefono', 'direccion', 'descripcion', 'info_pedido', 'comentarios_internos',
              'formula_coste_extra_pedido', 'activo'
    ]
    # if es_red:
        # fields.insert(6, 'comentarios_internos')
    if contabilidad:
        fields.insert(-2,'formula_coste_extra_productor')

    form = SQLFORM(db.productor, iproductor, showid=False, deletable=False,
                   fields=fields,
                   _id='form_editar_productor'
           )
    if form.process(onvalidation=chequea_coste_extra_en_form).accepted:
        #Si cambia el nombre del productor hay que cambiar la descripcion de sus cuentas
        cuenta_gp = db.cuenta(tipo=CONT.TIPOS_CUENTA.GRUPO_PRODUCTOR, owner=igrupo, target=iproductor)
        cuenta_p  = db.cuenta(tipo=CONT.TIPOS_CUENTA.PRODUCTOR, owner=iproductor)
        for cuenta in (cuenta_gp, cuenta_p):
            cuenta.update_record(descripcion=crea_descripcion_cuenta(cuenta))

        redirect(URL(c='productores', f='index.html', vars={'grupo':igrupo}))
    elif form.errors:
        response.flash = C.MENSAJE_ERRORES

    return dict(edita_productor=form,
                nombre_grupo=grupo.nombre,
                es_red=es_red,
                productor=productor)

@scope.set(scope_grupo_o_red)
#@requires_get_arguments(('grupo', db.grupo))
@auth.requires_membership('admins_%s'%request.vars.grupo)
def productores_coordinados():
    from modelos.grupos import redes_del_grupo, arbol_redes_del_grupo
    igrupo = int(request.vars.grupo)
    grupo  = db.grupo(igrupo)
    iredes = redes_del_grupo(igrupo)
    arbol = arbol_redes_del_grupo(igrupo)
    via = {}
    def recorre_arbol_redes(ar, cadena):
        ls = []
        for isubred, (nombre_red, d) in ar.items():
            ls.append(isubred)
            via[isubred] = cadena
            ls.extend(recorre_arbol_redes(d, cadena + [isubred]))
        return ls
    redes_ordenadas = recorre_arbol_redes(arbol, [])
    lista_pc = {}
    info_redes = {}
    for row in db(db.grupo.id.belongs(iredes)).select(
            db.grupo.id, db.grupo.nombre, db.grupo.descripcion
        ):
        ired = row.id
        info_redes[ired] = (row.nombre, row.descripcion, via[ired])
        lista_pc[ired] = db((db.productor.grupo==ired) &
                            (db.productor.id==db.proxy_productor.productor) &
                            (db.proxy_productor.grupo==igrupo)) \
                            .select(db.productor.ALL, db.proxy_productor.estado,
                                    orderby=~db.productor.activo | db.productor.nombre
                            )
    return dict(lista_pc=lista_pc,
                info_redes=info_redes,
                grupo=igrupo,
                nombre_grupo=grupo.nombre)

@scope.set('admin-grupo')
#@requires_get_arguments(('grupo', db.grupo),
                    #     ('productor', db.productor),
                    #     _consistency= ( (db.grupo.id==db.grupoXred.grupo) &
                    #                     (db.productor.grupo==db.grupoXred.red) )
                    #    )
@auth.requires_membership('admins_%s'%request.vars.grupo)
def edita_productor_coordinado():
    from modelos.pedidos import abre_pedidos_en_grupos_de_la_red
    from evalua_coste_extra import chequea_coste_extra_en_form

    igrupo = int(request.vars.grupo)
    grupo  = db.grupo(igrupo)
    es_red = grupo.red
    iproductor = int(request.vars.productor)
    productor  = db.productor[iproductor]

    response.title = T('Editando los comentarios de %s sobre %s')%(grupo.nombre, productor.nombre)

    pp = db.proxy_productor(grupo=igrupo, productor=iproductor)
    if not pp:
        pp = db.proxy_productor.insert(grupo=igrupo, productor=iproductor)

    fields = ['estado', 'comentarios', 'formula_coste_extra_pedido']
    if es_red:
        fields.insert(2, 'comentarios_subgrupos')
    form = SQLFORM(db.proxy_productor, pp.id, fields=fields)

    if form.process(onvalidation=chequea_coste_extra_en_form).accepted:
        #si el estado cambia de bloqueado a auto o avisar, hay que crear los
        #proxy_pedido para los pedidos abiertos
        if form.vars.estado!='bloqueado':
            pedidos = [row.id for row in
                db((db.pedido.productor == iproductor) &
                   (db.pedido.abierto   == True)) \
                .select(db.pedido.id)
                ]
            if pedidos:
                abre_pedidos_en_grupos_de_la_red(pedidos, [igrupo])
        response.flash = 'form accepted'
    elif form.errors:
        response.flash = C.MENSAJE_ERRORES

    return dict(nombre_grupo=grupo.nombre,
                nombre_red=productor.grupo.nombre,
                es_red=es_red,
                edit_form = form)

@scope.set('miembro')
#@requires_get_arguments(('productor', db.productor))
@auth.requires(Permisos.tiene_acceso_al_grupo(request.vars.productor))
def vista_productos():
    # TODO: mostrar los precios de la red que trae el pedido, no de la red madre
    from modelos.pedidos import pedido_fantasma
    iproductor = int(request.vars.productor)
    productor  = db.productor[iproductor]
    ipedidof   = pedido_fantasma(iproductor).id
    tppp = db.productoXpedido

    response.title = T('Todos los productos de %s')%productor.nombre

    productos = db((tppp.pedido==ipedidof) &
                   (tppp.activo==True)  &
                   (tppp.categoria==db.categorias_productos.id)
                  ).select(tppp.ALL, db.categorias_productos.nombre,
                           orderby=~tppp.temporada |
                                   db.categorias_productos.nombre |
                                   tppp.nombre)
    return dict(grupo=Permisos.grupo_del_productor(iproductor),
                productos=productos
    )

script_productos = '''<script type="text/javascript">
aplicar_formula = function(){
    _aplicar_formula('#formula_precio_final', 'precio_final');
    if($('#formula_precio_productor').length>0){
        _aplicar_formula('#formula_precio_productor', 'precio_productor');
    }
}
_aplicar_formula = function(formula_field, target){
    var formula = $(formula_field).val();
    var vals={}, globs={};
    var preciof;

    if(formula=='precio_base'){
        $('#ertable_%(id)s_th_'+target).hide();
        $('textarea.ertable_field_'+target).parent().hide();
    }else{
        $('#ertable_%(id)s_th_'+target).show();
        $('textarea.ertable_field_'+target).parent().show();
    }
    var fields = $('#_ertable_%(id)s_fields').val().split(',');

    //campos globales
    var campos = $('tr[id^=ertable_%(id2)s_row_]');
    for(var k=0;k<campos.length;k++){
        var nombre = campos[k].children[1].children[0].value;
        var v = campos[k].children[2].children[0].value;
        globs[nombre] = parseFloat(v);
    }

    var trs = $('#ertable_%(id)s tr');
    for(var j=1; j<trs.length; j++){
        var rid = trs[j].id.split('_')[3];
        vals={};
        jQuery.extend(vals,globs);

        //columnas
        for(var k=0;k<fields.length;k++){
            var v = $('[id^="ertable_%(id)s_cell_'+fields[k]+'_'+rid+'"]').val();
            v = v?v.replace(',','.'):0;
            if(v=='on'){
                if ($('[id^="ertable_%(id)s_cell_'+fields[k]+'_'+rid+'"]').attr('checked')){
                    v=1;
                }else{
                    v=0;
                }
            }
            vals[fields[k]] = parseFloat(v);
        }

        try {
            preciof = eval_arit(formula, vals);
            //redondea:
            preciof = Math.round(preciof*100)/100;
        } catch (e) {
            alert(e.name + ": " + e.message);
            return true;
        }
        $('#ertable_%(id)s_cell_'+target+'_'+rid).val( preciof);
    }
}
$(document).one('scripts_loaded', function() {
    $('textarea[id^="ertable_%(id)s_cell_precio_final_"]').attr('readonly', 'readonly');
    $('textarea[id^="ertable_%(id)s_cell_precio_productor_"]').attr('readonly', 'readonly');
    $('table.ertable').on('change', 'textarea,input', aplicar_formula);
    $('#formula_precio_final').on('change', aplicar_formula);
    $('#formula_precio_productor').on('change', aplicar_formula);
    aplicar_formula();
})
</script>
'''

FORMULA_NO_VARS = re.compile('[\>\<= \+,\*\-\?\/\(\):0-9\.]+')
FORMULA_FUNCTIONS = ('IF', 'MIN', 'MAX')
@scope.set(scope_grupo_o_red)
#@requires_get_arguments(('productor', db.productor))
@auth.requires_membership(Permisos.grupo_del_productor(request.vars.productor))
def productos():
    from merge_forms import MERGED_FORM
    from rsheet import ERTable
    from kutils import parse_decimal, nombre2slug, reduce_espacios_nombre
    from modelos.pedidos import mueve_productos_al_pedido_fantasma, \
                                activa_productos_coordinados, \
                                actualiza_precios_peticion, elimina_item_fuera_de_temporada
    from modelos.productores import crea_productos_si_nec, compara_con_productos_inactivos
    from eval_arit import EvalArit

    iproductor = int(request.vars.productor)
    productor  = db.productor[iproductor]
    grupo      = productor.grupo
    igrupo     = grupo.id
    contabilidad = ( grupo.opcion_ingresos!=CONT.OPCIONES_INGRESOS.NO_CONT )
    pagina = int(request.vars.page or 0)

    pedido_sinconfirmar = False
    if 'pedido' in request.vars:
        ipedido = int(request.vars.pedido)
        pedido  = db.pedido[ipedido]
        assert pedido, 'El pedido no existe'
        assert pedido.productor==iproductor, 'El pedido no corresponde al productor'
        pedido_fantasma = False
        if pedido.usa_plantilla:
            response.flash = T('Atención: este pedido se importó desde una hoja de cálculo. Si introduces productos nuevos y después intentas exportar el pedido en el formato original, los productos nuevos no aparecerán. Sin embargo, puedes eliminar productos o modificar productos existentes, siempre que conserves el nombre original.')
            if db((db.pedido.productor==iproductor) &
                  (db.pedido.fecha_reparto>pedido.fecha_reparto)
                  ).count() == 0:
                pedido_sinconfirmar = pedido.sin_confirmar
    else:
        pedido = db.pedido(productor=iproductor,fecha_reparto=C.FECHA_FANTASMA)
        ipedido = pedido.id
        pedido_fantasma = True

    if not pedido_fantasma:
        response.title = T('Editar los productos del pedido de %s para el %s')%(
            productor.nombre, pedido.fecha_reparto
        )
    else:
        response.title = T('Editando los productos de %s')%productor.nombre

    response.files.append(URL('static', 'js/esprima.js'))
    response.files.append(URL('static', 'js/eval_arit.js'))

    try:
        t1, t2 = [k for k in request.vars if k.startswith('_table_name_')]
        #ce: campos extra  tp: tabla principal
        id_ce = (t1[12:] if request.vars[t1]=='campo_extra_pedido'
                         else t2[12:] )
        id_tp = (t1[12:] if request.vars[t1]=='productoXpedido'
                         else t2[12:] )
    except ValueError:
        id_ce = id_tp = None

    columnas = ['nombre', 'precio_base', 'descripcion', 'categoria', 'granel', 'pesar',
                'destacado', 'temporada', 'precio_final'
               ]
    if contabilidad:
        columnas.append('precio_productor')
    my_parsers = {'decimal(9,2)':parse_decimal, 'string':reduce_espacios_nombre}
    #TODO: sin prisa: a lo mejor se puede usar db.productoXpedido.slug en vez de recalcularlo
    #dentro de ERTable, pero en cualquier caso es necesario recalcularlo con los valores
    #que nos envian por el formulario
    tabla  = ERTable(db.productoXpedido,
                     columnas,
                     ((db.productoXpedido.pedido==ipedido) &
                      (db.productoXpedido.activo==True)),
                     master_field='nombre',
                     simplify_master_field=nombre2slug,
                     values = {'pedido':ipedido, 'activo':True},
                     extra_columns_table=db.columna_extra_pedido,
                     extra_columns_values={'pedido':ipedido},
                     extra_columns_names=('nombre', 'tipo'),
                     extra_data=TABLAS_VALORES_EXTRA_PEDIDO,
                     data_types_names=C.NOMBRES_TIPOS_COLUMNA,
                     extra_data_fields=('productoXpedido', 'columna', 'valor'),
                     add_controls=False,
                     add_new_columns=True,
                     pagination=20,
                     page=pagina,
                     parsers=my_parsers,
                     _id=id_tp,
                     _class='table_productos'
                     )
    formula_field = FIELDSET(T('Formula para el precio final: '),
                             TEXTAREA(_name='formula_precio_final',
                                      _id='formula_precio_final',
                                      value=pedido.formula_precio_final,
                                      _cols="60", _rows="2"))
    if contabilidad:
        formula_productor_field = FIELDSET(
            T('Formula para el precio del productor: '),
            TEXTAREA(_name='formula_precio_productor',
                     _id='formula_precio_productor',
                     value=pedido.formula_precio_productor,
                     _cols="60", _rows="2")
        )
        formula_field = CAT(formula_field,
                            formula_productor_field)
    cs = db(db.campo_extra_pedido.pedido==ipedido).select()
    campos_extra = ERTable(db.campo_extra_pedido,
                          ['nombre', 'valor'],
                          master_field='nombre',
                          values = {'pedido':ipedido},
                          parsers=my_parsers,
                          add_controls=False,
                          add_n_rows_at_a_time=(1,),
                          _id=id_ce)
    botones_submit = FORM(INPUT(_type='submit', _id='submit_button',
                                _value=T('Grabar los cambios'),
                                _class='btn btn-primary'),
                          INPUT(_type='hidden', _id='submit',
                                _name='submit', _value=''),
                          INPUT(_type='submit', _id='submit_and_go_button',
                                _value=T('Grabar los cambios y volver'),
                                _class='btn btn-primary'),
                          INPUT(_type='hidden', _id='submit_and_go',
                                _name='submit_and_go', _value='')
                          )
    if db(db.campo_extra_pedido.pedido==ipedido).count():
        botones_avanzados = DIV(formula_field,
                                _id='div_controles_avanzados')

        form = MERGED_FORM(campos_extra, botones_avanzados, tabla, botones_submit)
        div_form = DIV(form, XML(script_productos%dict(id=tabla._id, id2=campos_extra._id)))
    else:
        botones_avanzados = DIV(campos_extra,
                                formula_field,
                                _id='div_controles_avanzados')

        form = MERGED_FORM(botones_avanzados, tabla, botones_submit)
        div_form = DIV(form, XML(script_productos%dict(id=tabla._id, id2=campos_extra._id)))

    muestra_campos_avanzados = False
    #Si envían el formulario, comprobamos los datos envíados...
    if request.vars.submit or request.vars.submit_and_go:
        #Separamos las variables de una y otra ERTable
        vars_ce = dict((k,v) for (k,v) in request.vars.items() if id_ce in k)
        vars_tp = dict((k,v) for (k,v) in request.vars.items() if id_tp in k)

        #Campos Extra
        ce_a = campos_extra.accepts(vars_ce, dbio = False)

        #Formula precio final y precio productor
        nombres_vars = set(v for (k,v) in vars_ce.items() if '_cell_nombre_' in k)
        nombres_vars.update(''.join(k.split('_')[4:])
                      for (k,v) in vars_tp.items() if '_nueva_columna_' in k)
        nombres_vars.update(('precio_base', 'granel'))

        error_formula_refs = T('Esta fórmula hace referencia a campos que no están presentes.')
        error_formula = T('Esta formula no da resultados válidos. Por favor corrígela.')
        formula_ok = formula_refs = True
        errores_formula = {}
        formula_precio_final = request.vars.formula_precio_final or 'precio_base'
        formula_precio_productor = request.vars.formula_precio_productor or 'precio_base'
        for nombre_formula, formula in (('formula_precio_final', formula_precio_final),
                                        ('formula_precio_productor', formula_precio_productor)):
            has_formula = formula and (formula != 'precio_base')
            formula_element = formula_field.element('#'+nombre_formula)
            if formula_element:
                formula_element['value'] = formula
            nombres = [w.strip() for w in FORMULA_NO_VARS.split(formula)
                                 if (w and
                                     not w.isspace() and
                                     not w in FORMULA_FUNCTIONS)]
            formula_refs0 = all((nombre in nombres_vars) for nombre in nombres)
            formula_refs = formula_refs and formula_refs0
            #Observacion: en un navegador sin javascript habria que rellenar precio_final a mano
            #Podriamos cambiar esto en un futuro, ahora que hemos unificado la sintaxis para poder
            #parsear formulas en python y javascript
            try:
                #Comprueba unicamente que la sintaxis es correcta, no la aplica a cada fila
                EvalArit(formula, {})
                formula_ok0 = True
            except SyntaxError:
                formula_ok0 = False
            formula_ok = formula_ok and formula_ok0
            if not formula_refs0:
                errores_formula[nombre_formula] = error_formula_refs
            elif not formula_ok0:
                errores_formula[nombre_formula] = error_formula
            if formula_element:
                formula_element.errors = errores_formula
                formula_element._postprocessing()

        #Tabla principal
        tp_a = tabla.accepts(vars_tp,
                             dbio=(ce_a and formula_ok and formula_refs))

        #Si el formulario es finalmente validado...
        muestra_campos_avanzados = not (formula_ok and formula_refs)
        if ce_a and formula_ok and formula_refs and tp_a:
            if pedido_fantasma:
                compara_con_productos_inactivos(ipedido)
            crea_productos_si_nec(ipedido)

            vars_globales_pre = dict((row.nombre, row.valor)
                for row in db(db.campo_extra_pedido.pedido==ipedido) \
                          .select(db.campo_extra_pedido.ALL) )
            campos_extra.accepts(vars_ce)
            vars_globales_pos = dict((row.nombre, row.valor)
                for row in db(db.campo_extra_pedido.pedido==ipedido) \
                          .select(db.campo_extra_pedido.ALL) )
            #Si no usa la contabilidad, no ha enviado el campo precio_productor, pero
            #es necesario rellenarlo con el precio_final
            #Alternativamente, podriamos hacer
            #actualiza_precios_productos(ipedido, set([('precio_productor', 'precio_final')]) )
            if not contabilidad:
                db(db.productoXpedido.pedido==ipedido).update(
                    precio_productor=db.productoXpedido.precio_final
                )
            #Si ha cambiado alguna formula, o alguna variable global, y hay más de una
            #pagina, tenemos que recalcular los precios de todos los productos
            #TODO Si ha cambiado alguna formula, **o alguna variable global**
            formulas_que_han_cambiado = set()
            if tabla.pagination:
                if (formula_precio_final!=pedido.formula_precio_final):
                    formulas_que_han_cambiado.add(('precio_final', formula_precio_final))
                if (formula_precio_productor!=pedido.formula_precio_productor):
                    formulas_que_han_cambiado.add(('precio_productor', formula_precio_productor))
                if any((v!=vars_globales_pre.get(k)) for k,v in vars_globales_pos.items()):
                    formulas_que_han_cambiado.update(
                        (('precio_final', formula_precio_final),
                         ('precio_productor', formula_precio_productor))
                    )
                if formulas_que_han_cambiado:
                    from modelos.pedidos import actualiza_precios_productos
                    #TODO: try-except etcetera
                    actualiza_precios_productos(ipedido, formulas_que_han_cambiado)

            db.pedido[ipedido].update_record(
                formula_precio_final=formula_precio_final,
                formula_precio_productor=formula_precio_productor
            )
            if grupo.red:
                # Recalculamos los precios que bajan por el arbol de redes hasta los grupos.
                activa_productos_coordinados(ipedido)

            pedido_mas_reciente = db((db.pedido.productor==iproductor) &
                                     (db.pedido.fecha_reparto>pedido.fecha_reparto)
            ).count() == 0
            if pedido_fantasma or pedido_mas_reciente:
                db.productor[iproductor].update_record(
                    formula_precio_final=formula_precio_final,
                    formula_precio_productor=formula_precio_productor
                )
            if not pedido_fantasma:
                if pedido_mas_reciente:
                    mueve_productos_al_pedido_fantasma(ipedido)
                actualiza_precios_peticion(ipedido)
                #Si marcas un producto como fuera de temporada, elimina toda la petición y el item
                # No hacemos la llamada, pq hay un caso de uso: marcar el producto como fuera de
                #  temporada cuando se acaba el stock
                elimina_item_fuera_de_temporada(ipedido, solo_inactivos=True)
                if not pedido.abierto:
                    from modelos.pedidos import anota_incidencias_contabilidad
                    anota_incidencias_contabilidad(ipedido)
            if request.vars.submit_and_go:
                if pedido_fantasma:
                    session.flash = T('Los productos de %s han sido actualizados.\nLa modificación será efectiva para el siguiente pedido.\nNo se han actualizado los productos de ningún pedido existente.')%productor.nombre
                    redirect(URL(c='productores', f='vista_productores.html',
                                 vars=dict(grupo=igrupo)))
                else:
                    session.flash = T('Los productos del pedido de %s \n para el %s han sido actualizados')%(
                        productor.nombre, pedido.fecha_reparto)
                    redirect(URL(c='gestion_pedidos', f='gestion_pedidos.html',
                        vars=dict(grupo=igrupo)))
            else:
                    session.flash = T('Se han guardado los cambios. Estás en la página %d')%(
                        int(request.vars.page or 0) + 1
                    )
                    redirect(URL(c='productores', f='productos.html',
                                 vars=request.get_vars))
        else:
            #Eliminamos los errores en los campos precio_final y precio_productor, ya que no
            #pueden corregir en esos campos, sino indirectamente en la fórmula
            errores = [(k, k.split('_',3)[-1]) for k in tabla.errors]
            errores_precio_final = [k for k,error in errores
                                       if (error.startswith('precio_final') or
                                           error.startswith('precio_productor'))]
            if errores_precio_final:
                muestra_campos_avanzados = True
                formula_ok = False
            for k in errores_precio_final:
                tabla.errors.pop(k)
            flash_messages = []
            if tabla.errors:
                flash_messages.append( T('La tabla de productos tiene errores. Por favor completa los campos marcados en rojo. '))
            if not ce_a:
                flash_messages.append( T('El formulario de campos globales tiene errores. '))
            if not formula_refs:
                flash_messages.append( T('La fórmula para el precio final hace referencia a campos que no están presentes.\nPor favor comprueba los campos avanzados. ') )
            elif not formula_ok:
                flash_messages.append( T('La fórmula para el precio final no da resultados válidos. Repasa la aritmética y los paréntesis, y que todos los campos mencionados en la fórmula tienen un valor numérico válido.'))
            response.flash = '\n'.join(m.decode() for m in flash_messages)
    return dict(productos=div_form,
                productor=productor,
                pedido_fantasma=pedido_fantasma,
                pedido=ipedido,
                tabla_id=tabla._id,
                muestra_campos_avanzados=muestra_campos_avanzados,
                pedido_sin_confirmar=pedido_sinconfirmar
                )

@scope.set('miembro')
#@requires_get_arguments(('productor', db.productor))
@auth.requires(
    auth.has_membership(role='admins_%s'%request.vars.grupo) if request.vars.grupo else
    Permisos.tiene_acceso_al_grupo(request.vars.productor)
)
def ver_info_pedido():
    iproductor = int(request.vars.productor)
    productor  = db.productor[iproductor]
    info = XML(productor.info_pedido)
    if request.vars.grupo:
        igrupo = int(request.vars.grupo)
        nombre_grupo  = db.grupo(igrupo).nombre
        proxy_productor = db.proxy_productor(productor=iproductor, grupo=igrupo)
        info_local = XML(proxy_productor.comentarios or '')
    else:
        nombre_grupo = info_local = ''
    response.title = T('Info para gestionar los pedidos de %s')%productor.nombre

    return dict(info=info,
                info_local=info_local,
                productor=productor,
                nombre_grupo=nombre_grupo)

#@requires_get_arguments(('productor', db.productor))
@auth.requires_membership(Permisos.grupo_del_productor(request.vars.productor))
def actualizar_productos():
    iproductor = int(request.vars.productor)
    productor  = db.productor(iproductor)
    t = request.post_vars.csv_data
    from modelos.productores import actualizar_productos_del_productor
    actualizar_productos_del_productor(iproductor,t)
    return dict(status='ok')
