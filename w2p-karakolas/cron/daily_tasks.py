#!/usr/bin/python
# -*- coding: utf-8 -*-
# Tareas que karakolas puede aplazar hasta la noche, cuando la carga del servidor es menor
# Tambien da tiempo para que se agrupen tareas similares

import logging
import os.path
import datetime

from modelos.pedidos import pedidos_del_dia
from modelos.usuarios import email_blacklist

from gluon.contrib.appconfig import AppConfig
conf = AppConfig()

ASUNTO_ERRORES = T('Errores al enviar algunos emails')
MENSAJE_ERRORES = T('''
Hola:

Este es un mensaje automatico generado por karakolas.

Varias personas del grupo %(nombre_grupo)s pidieron que les enviaramos un email con su
pedido para el %(fecha_reparto)s.

Sin embargo, los siguiente emails no se han podido enviar:

%(emails_problematicos)s
''')

def envia_avisos_email(logger):
    '''Envía los emails con el recordatorio del pedido, en teoría
    la noche después de haber cerrado el último pedido para esa fecha.
    '''
    # Los emails que han sido rebotados. Esa lista se puebla manualmente
    blacklist = email_blacklist()

    logger.info(T("### Comenzamos a enviar los emails pendientes..."))
    webadmins_gid  = current.auth.id_group('webadmins')

    today = datetime.date.today()
    para_enviar = set()
    #1: busca las fechas y grupos con aviso_email que no esta "enviado"
    #   agrupa los repartos, que se envían en bloque
    for row in db((db.aviso_email.enviado==False) &
                  (db.aviso_email.fecha_reparto<today)) \
        .select(db.aviso_email.fecha_reparto, db.aviso_email.grupo,
                groupby=db.aviso_email.fecha_reparto|db.aviso_email.grupo):
        para_enviar.add( (row.grupo, row.fecha_reparto) )

    for grupo, fecha_reparto in para_enviar:
        #2: para este grupo y esta fecha, mira si estan cerrados todos los pedidos
        pedidos = chain(*pedidos_del_dia(fecha_reparto,grupo))
        if all(not(r.abierto) for r in pedidos ):
            #3: si es asi, envia todos los emails para esa fecha y grupo, y marcalos como "enviado"
            logger.info(T('## Enviando emails para el grupo %s de la fecha %s.'),
                         grupo, fecha_reparto )
            errores = []
            #Escribimos la query en dos partes, porque esta primera parte la usamos mas adelante
            q = ((db.aviso_email.grupo==grupo) &
                 (db.aviso_email.fecha_reparto==fecha_reparto) &
                 (db.aviso_email.enviado==False) )
            for row2 in db(q &
                           (db.personaXgrupo.grupo==grupo) &
                           (db.personaXgrupo.persona==db.aviso_email.persona) &
                           (db.auth_user.id==db.personaXgrupo.persona)) \
                         .select(db.aviso_email.persona,
                                 db.aviso_email.random_string,
                                 db.personaXgrupo.unidad,
                                 db.auth_user.email):
                c = LOAD('gestion_pedidos', 'ver_peticion_unidad.load',
                         vars=dict(grupo=grupo, fecha_reparto=fecha_reparto,
                                   unidad=row2.personaXgrupo.unidad, persona=row2.aviso_email.persona,
                                   auth_code=row2.aviso_email.random_string) )
                s = mail.send(
                  to=row2.auth_user.email,
                  subject='Resumen de los pedidos de tu unidad para el %s'%fecha_reparto,
                  message='<html>%s</html>'%c.xml()
                ) if row2.auth_user.email not in blacklist else 'BLACK'
                if s=='BLACK':
                    logger.warning(
                        T('El email a %s con sus pedidos para el %s no ha sido enviado porque está en la lista negra.'),
                          row2.auth_user.email, fecha_reparto
                        )

                elif s:
                    logger.info(
                        T('El email a %s con sus pedidos para el %s ha sido enviado.'),
                          row2.auth_user.email, fecha_reparto
                        )
                else:
                    logger.warning(
                        T('El email a %s con sus pedidos para el %s *no* se pudo enviar.'),
                          row2.auth_user.email, fecha_reparto
                        )
                    errores.append(row2.auth_user.email)
            #Ya lo hemos intentado, no tiene sentido seguir intentándolo
            db(q).update(enviado=True)
            #pero si no se ha podido enviar algún email, avisamos a los admins del grupo
            #y a los webadmins
            if errores:
                admin_group_id = current.auth.id_group('admins_%d'%grupo)
                emails_admins = [admin.email for admin in db(
                        (db.auth_user.id == db.auth_membership.user_id) &
                        (db.auth_membership.group_id.belongs((admin_group_id, webadmins_gid)))
                    ).select(db.auth_user.email)]
                s = mail.send(to=[email for email in emails_admins if email not in blacklist],
                              subject=ASUNTO_ERRORES,
                              message=MENSAJE_ERRORES%dict(
                                        emails_problematicos=', '.join(errores),
                                        nombre_grupo=db.grupo(grupo).nombre,
                                        fecha_reparto=fecha_reparto)
                    ) if any((email not in blacklist) for email in email_admins) else None
                if not s:
                    logger.warning(
                        T('El email a %s con el mensaje de error *no* se pudo enviar.'),
                          ', '.join(emails_admins)
                        )

def resetea_y_datos_test(logger):
    '''Resetea los datos de la base de datos si en appconfig aparece
    reload_test_data_daily = true
    '''
#    from populate_database import cleandata, importdata
    logger.warning('new: programa en cron directamente el script karakolas/testdata/import_database.py')

if __name__=='__main__':
    send_daily_emails = conf['global'].get('send_daily_emails')
    logger = logging.getLogger('Tareas diarias')
    logger.setLevel(logging.INFO)
    fh = logging.FileHandler("logs/daily_tasks.log")
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    logger.addHandler(fh)
    # Cuidado, AppConfig no devuelve booleanos sino str :-O !
    # if send_daily_emails=='True':
    if False:
        # JUN21: Está resultando problemático, los emails llegan tarde, se
        # envían más de una vez... y pensamos que nos puede causar problemas
        # de reputación, que podrían afectar a los emails de recuperar
        # contraseña, que son críticos. Así que desactivamos los avisos por
        # email hasta nueva orden.
        envia_avisos_email(logger)
    # Resetea los datos de la base de datos si es necesario
    # JUL23: Este sistema ya no funciona ni está mantenido, y es más fácil 
    # programar en cron directamente el script karakolas/testdata/import_database.py
#    reload_test_data_daily = conf['global'].get('reload_test_data_daily')
#    if reload_test_data_daily == 'True':
#        resetea_y_datos_test(logger)
