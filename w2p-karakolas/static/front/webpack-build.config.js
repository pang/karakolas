var webpack = require('webpack');
var path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const Assets = require('./assets');
const TerserPlugin = require("terser-webpack-plugin");

module.exports =  {
      mode: "production", // "production" | "development" | "none"
      // Chosen mode tells webpack to use its built-in optimizations accordingly.
      entry: "./src/js/app.js", // string | object | array
      // defaults to ./src
      // Here the application starts executing
      // and webpack starts bundling
      output: {
        // options related to how webpack emits results
        path: path.resolve(__dirname, 'src'), // string
        // the target directory for all output files
        // must be an absolute path (use the Node.js path module)
        filename: 'app.bundle.js',
        // the filename template for entry chunks
        publicPath: "/static/front/", // string
        // the url to the output directory resolved relative to the HTML page
        library: "karakolas_app", // string,
        // the name of the exported library
        libraryTarget: "umd", // universal module definition
        // the type of the exported library
      },
      module: {
        // configuration regarding modules
        rules: [
          // rules for modules (configure loaders, parser options, etc.)
              // {
              //   // Exposes jQuery for use outside Webpack build
              //   test: /jquery.+\.js$/,
              //   use: [{
              //       loader: 'expose-loader',
              //       options: 'jQuery'
              //   },{
              //       loader: 'expose-loader',
              //       options: '$'
              //   }]
              // },
              {
                  test: /\.js$/,
                  exclude: /node_modules/,
    //Descomentar esta línea es todo lo que necesitamos para poder usar ES6 en vez de javascript
    //Quizá podríamos usar esta técnica, para las traducciones del front
    //Las cadenas usarían una sintaxis [['Texto a traducir']]
    //el loader de webpack simplemente eliminaría las secuencias [[' y ']]
    //para producción, podríamos usar una tarea gulp para reemplazar las cadenas por sus
    //traducciones, y crear app.bundle.es.js, app.bundle.en.js, etc
    //                    loader: 'babel!imports-loader?define=>false'
//                  loader: 'imports-loader?define=>false',
/** https://www.howtobuildsoftware.com/index.php/how-do/InC/javascript-jquery-amd-webpack-managing-jquery-plugin-dependency-in-webpack */
		  use: [
		      {loader: "imports-loader?this=>window"}
		  ]
              },

              { test: /\.html$/, 
		 use:  [{loader: 'raw-loader'}]
	      },
              { test: /\.css$/, 
		use: [{loader: "style-loader"},
		      {loader: "css-loader"}]
	      },
              { test: /\.png$/, 
		 use: [{loader: "url-loader",
		        options:{limit:100000}
		       }] 
	      },
              { test: /\.jpg$/, 
		 use: [{loader: "file-loader"}] 
	      },
              { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/, 
		 use: [{loader: "url-loader",
		        options:{
			    limit: 10000,
			    mimetype: "image/svg+xml"}
		       }]
	      }

          ]
      },
      resolve: {
        // options for resolving module requests
        // (does not apply to resolving to loaders)
          alias: {
              "jquery": path.resolve(__dirname, "src/vendor/jquery/src/jquery.js"),
              "jqueryform": path.resolve(__dirname,"src/vendor/jquery-form/src/jquery.form.js"),
              "bootstrap": path.resolve(__dirname, "src/vendor/bootstrap/dist/js/bootstrap.js"),
              "web2py": path.resolve(__dirname,"src/js/web2py.js"),
              "calendar": path.resolve(__dirname,"src/js/calendar.js"),
          },
          extensions: [".js", ".css", ".html"],
          modules: [
            path.resolve(__dirname, "node_modules")
//            "src/vendor"
          ],
      },
      devtool: "source-map", // enum
      // enhance debugging by adding meta info for the browser devtools
      // source-map most detailed at the expense of build speed.
      context: __dirname, // string (absolute path!)
      // the home directory for webpack
      // the entry and module.rules.loader option
      //   is resolved relative to this directory
      target: "web", // enum
      // the environment in which the bundle should run
      // changes chunk loading behavior and available modules
//      externals: ["jquery", "jQuery"],
      // // Don't follow/bundle these modules, but request them at runtime from the environment
      // serve: { //object
      //   port: 1337,
      //   content: './dist',
      //   // ...
      // },
      // lets you provide options for webpack-serve
      stats: "errors-only",
      // lets you precisely control what bundle information gets displayed
      // devServer: {
      //   proxy: { // proxy URLs to backend development server
      //     '/api': 'http://localhost:3000'
      //   },
      //   contentBase: path.join(__dirname, 'public'), // boolean | string | array, static file location
      //   compress: true, // enable gzip compression
      //   historyApiFallback: true, // true for index.html upon 404, object for multiple paths
      //   hot: true, // hot module replacement. Depends on HotModuleReplacementPlugin
      //   https: false, // true for self-signed, object for cert authority
      //   noInfo: true, // only errors & warns on hot reload
      //   // ...
      // },

      plugins: [
        new CopyWebpackPlugin(
          Assets.map(function(asset) {
            return {
              from: path.resolve(__dirname, `./node_modules/${asset}`),
              to: path.resolve(__dirname, `./src/vendor/${asset}`)
            };
          })
        ),
        // new webpack.ProvidePlugin({
        //     $: "jquery",
        //     jQuery: "jquery",
        //     "window.jQuery": "jquery",
        //     "root.jQuery": "jquery"
        // })
      ],
      optimization: {
	    minimize: true,
	    minimizer: [new TerserPlugin()],
      }

        //config['optimization'] = {
        //     minimize: true,
        //     minimizer: [new UglifyJsPlugin()],
        //     // runtimeChunk: true,
        //     // splitChunks: {
        //     //     chunks: "async",
        //     //     minSize: 1000,
        //     //     minChunks: 2,
        //     //     maxAsyncRequests: 5,
        //     //     maxInitialRequests: 3,
        //     //     name: true,
        //     //     cacheGroups: {
        //     //         default: {
        //     //             minChunks: 1,
        //     //             priority: -20,
        //     //             reuseExistingChunk: true,
        //     //         },
        //     //         vendors: {
        //     //             test: /[\\/]node_modules[\\/]/,
        //     //             priority: -10
        //     //         }
        //     //     }
        //     // }
        // };
        // performance: {
        //   hints: "warning", // enum
        //   maxAssetSize: 200000, // int (in bytes),
        //   maxEntrypointSize: 400000, // int (in bytes)
        //   assetFilter: function(assetFilename) {
        //     // Function predicate that provides asset filenames
        //     return assetFilename.endsWith('.css') || assetFilename.endsWith('.js');
        //   }
        // },

//         config.plugins.push(
//         //   new webpack.DefinePlugin({
//         //     'process.env': {
//         //       'NODE_ENV': JSON.stringify('production')
//         //     }
//         //   }),
// //          new webpack.optimize.DedupePlugin(),
// //          new webpack.optimize.UglifyJsPlugin(),
// //          new webpack.NoErrorsPlugin()
//         )
}
