﻿/*
Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/
CKEDITOR.editorConfig = function( config ) {
	config.language = 'es';
	config.uiColor = '#babdb6';
	config.height = 300;

	config.skin = 'moono';
	// %REMOVE_END%

	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	config.toolbar = [
	    ['Cut','Copy','Paste', '-','Undo', 'Redo'],
	    ['Table', 'Image', 'SpecialChar'],
	    ['Link', 'Unlink', 'Anchor'],
	    ['Source'],
	    '/',
	    ['Format', 'Bold', 'Italic', 'Underline', 'Strike', '-', 'NumberedList', 'BulletedList'],
	    ['Font', 'FontSize', 'ColorDialog'],
	    ['TextColor','BGColor']
	];
	config.removePlugins = 'about,floatingspace,a11yhelp,horizontalrule,magicline,maximize,pastetext,pastefromword,showborders,stylescombo,tab,wsc,dialogadvtab,TextDirection,templates,div,find,flash,forms,iframe,indentblock,smiley,language,newpage,pagebreak,preview,print,save,showblocks,SCAYT';
	config.extraPlugins = 'tableresize,colorbutton,colordialog';
//	config.plugins = 'basicstyles,dialog,button,format,toolbar,enterkey,entities,fakeobjects,link,list,undo';

};
