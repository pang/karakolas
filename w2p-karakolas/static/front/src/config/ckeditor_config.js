﻿/*
Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/
CKEDITOR.editorConfig = function( config ) {
	config.language = 'es';
	config.uiColor = '#babdb6';
	config.height = 300;

	config.skin = 'moono';
	// %REMOVE_END%

	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	config.toolbar = [
	    ['Undo', 'Redo', '-', 'Format', 'Bold', 'Italic', 'Underline', 'Strike', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Entities']
	];
	config.removePlugins = 'about,floatingspace,a11yhelp,blockquote,elementspath,filebrowser,horizontalrule,htmlwriter,image,magicline,maximize,pastetext,pastefromword,removeformat,showborders,sourcearea,specialchar,SCAYT,stylescombo,tab,wsc,dialogadvtab,TextDirection,colorbutton,colordialog,templates,div,find,flash,font,forms,iframe,indentblock,smiley,justify,language,liststyle,newpage,pagebreak,preview,print,save,selectall,showblocks';
//	config.plugins = 'basicstyles,dialog,button,format,toolbar,enterkey,entities,fakeobjects,link,list,undo';

};
