karakolas-front
===============

Prospective front-end for karakolas. [Learn more](http://karakolas.org)

This frontend starts with a very humble purpose: to hold the karakolas menu, and to load the different pages from the web2py backend as components.
The next goal will be to improve navigation.
Then it will be possible to gradually replace views from karakolas into karakolas-front and move as much logic as possible to the client, while the backend requires only minimal changes.


build
-----

1. Install nodejs and npm
2. Install gulp and bower `sudo npm install -g gulp bower`
3. Clone karakolas-front
4. `$ npm install`
5. `$ bower install`
6. `$ gulp build`
