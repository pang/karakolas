﻿/*
Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/
CKEDITOR.editorConfig = function( config )
{

    config.removePlugins = 'forms,smiley,flash';
	// Define changes to default configuration here. For example:
	config.removeButtons = 'Underline,Strike,Subscript,Superscript';
	config.language = 'es';
	config.plugins = "about,a11yhelp,basicstyles,bidi,blockquote,button,clipboard,colorbutton,colordialog,contextmenu,dialogadvtab,div,elementspath,enterkey,entities,filebrowser,find,flash,font,format,forms,horizontalrule,htmldataprocessor,image,indent,justify,keystrokes,link,list,liststyle,maximize,newpage,pagebreak,pastefromword,pastetext,popup,preview,print,removeformat,resize,save,scayt,smiley,showblocks,showborders,sourcearea,stylescombo,table,tabletools,specialchar,tab,templates,toolbar,undo,wysiwygarea,wsc"
//	config.plugins = 'showblocks,styles,div,image,dialog,liststyle,pagebreak';

	// config.uiColor = '#AADC6E';
};
