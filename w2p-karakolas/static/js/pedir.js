/**
 (C) Copyright (C) 2012, 2013 Pablo Angulo
 This file is part of karakolas <karakolas.org>.

 karakolas is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 karakolas is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Affero General Public
 License along with karakolas.  If not, see
 <http://www.gnu.org/licenses/>.
*/


actualiza_precios = function(){
    var ids, element_cantidad, cantidad, preciou, preciof, i, preciot, ids_productos;
    ids_productos = $('#ids_productos').val();
    ids = (ids_productos?ids_productos.split(','):[]);
    preciot = 0;
    for(i=0; i<ids.length; i++){
        preciou  = $('#precio_unitario_'+ids[i]).html();
        preciou = preciou?parseFloat(preciou):0;
        element_cantidad = $('#cantidad_'+ids[i]);
        cantidad = element_cantidad.val().replace(',','.');
        cantidad = cantidad?parseFloat(cantidad):0;
        if($('#granel_'+ids[i]).val()=="True"){
            cantidad = Math.round(100*cantidad)/100;
        }else{
            cantidad = Math.round(cantidad);
        }
        element_cantidad.val(cantidad);
        preciof  = Math.round(100*preciou*cantidad)/100;
        $('#precio_producto_'+ids[i]).html(preciof);
        if(isNaN(cantidad)){
            element_cantidad.parents('tr').css('background','#FBB');
            element_cantidad.parents('div.collapsibleContainerContent').show()
        }else{
            element_cantidad.parents('tr').css('background','');
        }
        preciot += preciof;
    }
    $('#precio_total').html(Math.round(100*preciot)/100);
}

muestra_info_extendida = function(e){
    var modal_info_producto = $('#modal_producto');
    modal_info_producto.find('.modal-body').html(this.getAttribute('data-original-title'));
    modal_info_producto.find('.modal-title').html(this.parentElement.parentElement.parentElement.firstChild.innerHTML);
    modal_info_producto.modal('show');
}

$(document).ready(function() {
    $('#productos_inner').on('change', 'input.cantidad', actualiza_precios);
    $('#productos_inner').on('click', '.info-producto', muestra_info_extendida);
    $('#productos_inner').on('click', 'input', function(e){
        this.select();
    });
    $('#productos_inner').on('click', '#expandir_categorias', function(){
        $('.body-categoria-productos.collapse').collapse('show');
    })
    $('#productos_inner').on('click', '#contraer_categorias', function(){
        $('.body-categoria-productos.collapse').collapse('hide');
    });
    $('#productos_inner').on('keydown', 'input', function(e){
        var next_row, next_input, fieldname, myrow;
        if(e.keyCode==40 || e.keyCode==38){//down arrow or up arrow
            try{
                myrow = $(this).parent().parent();
                if(e.keyCode==40){
                    next_row = myrow.next();
                }else if(e.keyCode==38){
                    next_row = myrow.prev();
                }
                next_input = next_row.find('input');
                next_input.focus();
                next_input.select();
            }catch(_){}
            e.preventDefault();
        }
    });
});
