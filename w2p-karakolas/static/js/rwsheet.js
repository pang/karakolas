/**
 (C) Copyright (C) 2012, 2013 Pablo Angulo
 This file is part of karakolas <karakolas.org>.

 karakolas is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 karakolas is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Affero General Public
 License along with karakolas.  If not, see
 <http://www.gnu.org/licenses/>.
*/

/********************** controls *********************************/

focus_on_cell = function(rid, cid){
    $('textarea[name^=cell_]').parent().css('background','#FFF');
    $('th[id^=row_header_]').css('background','#FFF');
    $('span[id^=total_]').parent().css('background','#FFF');

    $('textarea[name^=cell_'+rid+']').parent().css('background','#CDF');
    $('textarea[name$=_'+cid+']').parent().css('background','#CFD');
    $('textarea#cell_'+rid+'_'+cid).parent().css('background','#CDD');
    $('th#row_header_'+rid).css('background','#CDF');
    $('span#total_'+rid).parent().css('background','#CDF');
    $('div#active_product p').html($('th#row_header_'+rid).html());

    $('#cell_'+rid+'_'+cid).focus();
    $('#cell_'+rid+'_'+cid).select();
}

prepare_cells = function() {
    $(".rwsheet textarea").on('click', function(e) {
        var n = this.name;
        var ls = n.split('_');
        if (ls[0]!='cell'){
            return true;
        }
        focus_on_cell(ls[1], ls[2]);
    });
    $(".rwsheet textarea").on('keydown', function(e) {
        var n = this.name;
        var ls = n.split('_');
        if (ls[0]!='cell'){
            return true;
        }
        if(e.keyCode==13 || e.keyCode==40 || e.keyCode==38){//enter, down arrow or up arrow
            var prow = parseInt(ls[1]);

            var row_ids  = $('#_row_ids').val().split(',');
            var pfila = 0;
            while((pfila<row_ids.length) & (row_ids[pfila]!=prow)){
                pfila++;
            }
            if(e.keyCode==38 && pfila>0){//up
                focus_on_cell(row_ids[pfila-1],ls[2]);
            }else if((e.keyCode==13 || e.keyCode==40) && pfila<row_ids.length-1){
                focus_on_cell(row_ids[pfila+1],ls[2]);
            }
            e.preventDefault();
        }else if(e.keyCode==39 || e.keyCode==37){//right or left arrow
            var pcol = parseInt(ls[2]);

            var col_ids  = $('#_col_ids').val().split(',');
            var pcolu = 0;
            while((pcolu<col_ids.length) & (col_ids[pcolu]!=pcol)){
                pcolu++;
            }
            if(e.keyCode==37 && pcolu>0){//left
                focus_on_cell(ls[1], col_ids[pcolu-1]);
            }else if(e.keyCode==39 && pcolu<col_ids.length-1){
                focus_on_cell(ls[1], col_ids[pcolu+1]);
            }
            e.preventDefault();

        }else if(e.keyCode==36 || e.keyCode==35){//home or end
            var col_ids  = $('#_col_ids').val().split(',');
            if(e.keyCode==36){//home
                focus_on_cell(ls[1], col_ids[0]);
            }else{
                focus_on_cell(ls[1], col_ids[col_ids.length-1]);
            }

        }
    });
    /**
    more info on http://www.tablefixedheader.com/fullpagedemo/
    MIT license
    */
    $('#rwsheet').fixheadertable({
        height      : 400,
        minWidthAuto   : true,
    });

    $('.head colgroup col:first').css('width','200px');
    $('#rwsheet colgroup col:first').css('width','200px');
}
$(document).ready(prepare_cells);

/*************** Paste from spreadsheet and csv *******************/

prepare_paste_behaviour = function() {
    $(".rwsheet textarea").on('paste', function(e) {
        var n = this.name;
        $('#'+n).val('');
        $('#paste_into_cell').val(n);
        // Short pause to wait for paste to complete
        setTimeout( function() {
            var target = $('#paste_into_cell').val();
            var text   = $('#'+target).val();
            var row_ids  = $('#_row_ids').val().split(',');
            var col_ids  = $('#_col_ids').val().split(',');
            var ls = target.split('_');
                       
            var prow = parseInt(ls[1]);
            var pfila = 0;
            while((pfila<row_ids.length) & (row_ids[pfila]!=prow)){
                pfila++;
            }
            var ufila = parseInt(row_ids.length);
           
            var pcol = parseInt(ls[2]);
            var pcolu = 0;
            while((pcolu<col_ids.length) & (col_ids[pcolu]!=pcol)){
                pcolu++;
            }
            var ucolu = parseInt(col_ids.length);

            var filas  = (text+'\n').split(/\r\n|\r|\n/);
            var i = 0;
            var ifila  = pfila;
            while((ifila<ufila) && (i<filas.length-1)){
                var fila   = filas[i];
                if(!fila.trim()){
                    i+=1;
                    continue;
                }
//                    var celdas = fila.split(/[\t,]/);
                var celdas;
                if(fila.match(/\t/)){
                    celdas = fila.split(/\t/);
                }else if(fila.match(/"/)){ 
                    //Vaya mierda: no se si se puede a golpe de regex, asi que parseo.
                    fila = fila+',';
                    celdas = [];
                    var f1 = -1;
                    var f2 = -1;
                    var last = 0;
                    for(var f=0;f<fila.length;f++){
                    if(fila[f]==='"'){
                        if(f1<0){
                            f1=f;
                        }
                        else if(f2<0){
                            f2=f;
                        }
                        else{
                            continue;//ERROR
                        }
                    }
                    else if(fila[f]===','){
                        if(f1<0 && f2<0){
                            celdas.push(fila.substring(last,f));
                            last=f+1;
                        }
                        else if(f1>=0 && f2>0){
                            celdas.push(fila.substring(last+1,f-1));
                            last=f+1;
                            f1=-1;
                            f2=-1;
                        }

                      }
                    }
                }else{
                    celdas = fila.split(/,/);
                }
                var j = 0;
                var icolu = pcolu;
                while((icolu<ucolu) && (j<celdas.length)){
                    var celda = celdas[j]?celdas[j].trim():''
                    $('#cell_'+row_ids[ifila]+'_'+col_ids[icolu]).val(celda);
                    j++;
                    icolu++;
                }
                ifila+=1;           
                i+=1;
            }
        compute_all_totals();
        }, 100);
    });
}
$(document).ready(prepare_paste_behaviour);




