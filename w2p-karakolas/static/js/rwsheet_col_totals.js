/**
 (C) Copyright (C) 2012, 2013 Pablo Angulo
 This file is part of karakolas <karakolas.org>.

 karakolas is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 karakolas is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Affero General Public
 License along with karakolas.  If not, see
 <http://www.gnu.org/licenses/>.
*/

compute_row_totals = function(rid){
    var sum = 0.0;
    var cells = $('*[id*=cell_'+rid+'_]');
    for(var i=0;i<cells.length;i++){
        if(cells[i].value){
            sum += parseFloat(cells[i].value.replace(',','.'));
        }
    }
    if(!sum){
        sum='';
    }
    $('#total_'+rid).html(sum);
}
compute_all_totals = function(){
    var rows = $('span[id*=total]');
    for(var k=0;k<rows.length;k++){
        var ls = rows[k].id.split('_');
        var rid = ls[1];
        compute_row_totals(rid);
    }
}
prepare_rows_behaviour = function() {
    $(".rwsheet input, .rwsheet textarea").bind('blur', function(e) {
        var ls   = this.name.split('_');
        var rid = ls[1];
        compute_row_totals(rid);
    });
    compute_all_totals();
}
$(document).ready(prepare_rows_behaviour);

