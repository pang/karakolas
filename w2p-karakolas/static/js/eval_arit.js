/**
 (C) Copyright (C) 2014 Pablo Angulo
 This file is part of karakolas <karakolas.org>.

 karakolas is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 karakolas is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Affero General Public
 License along with karakolas.  If not, see
 <http://www.gnu.org/licenses/>.
*/

eval_arit_functions = {
    'min': Math.min,
    'max': Math.max
}

eval_arit = function(s, vals){
    var args, func_name, val;
    var ast = esprima.parse(s);
    if(ast.body.length!=1){
        throw {
            name: 'TypeError',
            message: 'only one expression'
        }
    }
    eval_node = function(node){
        if(node.type=='BinaryExpression'){
                 if(node.operator=='+') return eval_node(node.left) + eval_node(node.right);
            else if(node.operator=='-') return eval_node(node.left) - eval_node(node.right);
            else if(node.operator=='*') return eval_node(node.left) * eval_node(node.right);
            else if(node.operator=='/') return eval_node(node.left) / eval_node(node.right);
            else if(node.operator=='<') return eval_node(node.left) < eval_node(node.right);
            else if(node.operator=='>') return eval_node(node.left) > eval_node(node.right);
            else if(node.operator=='<=') return eval_node(node.left) <= eval_node(node.right);
            else if(node.operator=='>=') return eval_node(node.left) >= eval_node(node.right);
            else if(node.operator=='==') return eval_node(node.left) == eval_node(node.right);
            else if(node.operator=='!=') return eval_node(node.left) != eval_node(node.right);
        }else if(node.type=='LogicalExpression'){
                 if(node.operator=='&&') return eval_node(node.left) && eval_node(node.right);
            else if(node.operator=='||') return eval_node(node.left) || eval_node(node.right);
        }else if(node.type=='UnaryExpression'){
                 if(node.operator=='!') return !eval_node(node.argument);
            else if(node.operator=='-') return -eval_node(node.argument);
            else if(node.operator=='+') return eval_node(node.argument);
//        }else if(node.type=='ConditionalExpression'){
//            if(eval_node(node.test)) {
//                return eval_node(node.consequent);
//            }else{
//                return eval_node(node.alternate);
//            }
        }else if(node.type=='CallExpression'){
            func_name = node.callee.name;
            if((node.callee.type=='Identifier') & (func_name=='IF')){
                args = node.arguments;
                if(eval_node(args[0])) {
                    return eval_node(args[1]);
                }else{
                    return eval_node(args[2]);
                }
            }
            return eval_arit_functions[func_name.toLowerCase()];
        }else if(node.type=='ExpressionStatement'){
            return eval_node(node.expression);
        }else if(node.type=='Literal'){
            return node.value;
        }else if(node.type=='Identifier'){
            val = vals[node.name];
            if(val==undefined){throw {
                name: 'EvalFormulaError',
                message: 'variable unknown: ' + node.name
            }}
            return val;
        }else{
            throw {
                name: 'EvalFormulaError',
                message: 'node type not allowed: ' + node.type
            }
        }
    }
    return eval_node(ast.body[0]);
}
