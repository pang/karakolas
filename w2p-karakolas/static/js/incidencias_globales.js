/**
 (C) Copyright (C) 2012-15 Pablo Angulo
 This file is part of karakolas <karakolas.org>.

 karakolas is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as
 published by the Free Software Foundation, either version 3 of the
 License, or (at your option) any later version.

 karakolas is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU Affero General Public
 License along with karakolas.  If not, see
 <http://www.gnu.org/licenses/>.
*/

var lee_cantidad = function(id_cantidad){
  /**Tiene en cuenta que el campo de cantidad puede ser editable (input) o no (td)
  */
  elemento_cantidad = $('#' + id_cantidad);
  cantidad = elemento_cantidad.val();
  cantidad = cantidad?parseFloat(cantidad.replace(',','.')):parseFloat(elemento_cantidad.html());
  return cantidad?cantidad:0;
}

var actualiza_precios_producto = function(iproducto){
  var element_cantidad, cantidad, cantidad_red, preciou, preciof;
  preciou  = $('#precio_unitario_'+iproducto).html();
  preciou = preciou?parseFloat(preciou):0;
  //Dificultad, puede ser input o no, dependiendo de si es pedido propio o no
  cantidad_grupo = lee_cantidad('cantidad_grupo_'+iproducto);
//        if($('#granel_'+ids[i]).val()=="True"){
//            cantidad = Math.round(100*cantidad)/100;
//        }else{
//            cantidad = Math.round(cantidad);
//        }
//element_cantidad.val(cantidad);
  preciof  = Math.round(100*preciou*cantidad_grupo)/100;
  $('#precio_producto_'+iproducto).html(preciof);
  // if(isNaN(cantidad)){
  //     element_cantidad.parents('tr').addClass('destacado');
  // }else{
  //     element_cantidad.parents('tr').removeClass('destacado');
  // }
  //Dificultad, puede ser input o no, dependiendo de si es pedido propio o no
  cantidad_red = lee_cantidad('cantidad_red_'+iproducto);
  preciored  = Math.round(100*preciou*cantidad_red)/100;
  $('#precio_producto_red_'+iproducto).html(preciored);

  cantidad_repartida = $('#cantidad_repartida_'+iproducto).html();
  cantidad_repartida = cantidad_repartida?parseFloat(cantidad_repartida):0;

  if(cantidad_grupo!=cantidad_red){
    $('#cantidad_red_'+iproducto).addClass('destacado');
    $('#precio_producto_'+iproducto).addClass('destacado');
    $('#precio_producto_red_'+iproducto).addClass('destacado');
  }else{
    $('#cantidad_red_'+iproducto).removeClass('destacado');
    $('#precio_producto_'+iproducto).removeClass('destacado');
    $('#precio_producto_red_'+iproducto).removeClass('destacado');
  }
  if(cantidad_grupo!=cantidad_repartida){
    $('#cantidad_repartida_'+iproducto).addClass('destacado');
    //No destaco los precios, porque el precio unitario es distinto...
//    $('#precio_repartido_'+iproducto).addClass('destacado');
  }else{
    $('#cantidad_repartida_'+iproducto).removeClass('destacado');
//    $('#precio_repartido_'+iproducto).removeClass('destacado');
  }
  return [preciof, preciored];
}
var actualiza_precios = function(){
    var id_str, ids, preciof, i, preciored;
    var preciotgrupo=0, preciotred=0;
    id_str = $('#ids_productos').val();
    ids = id_str?id_str.split(','):[];
    preciot = 0;
    for(i=0; i<ids.length; i++){
        precios = actualiza_precios_producto(ids[i]);
        preciof = precios[0];
        preciored = precios[1];
        preciotgrupo += preciof;
        preciotred += preciored;
    }
    $('#precio_total').html(Math.round(100*preciotgrupo)/100);
    $('#precio_total_red').html(Math.round(100*preciotred)/100);
    if(preciotred!=preciotgrupo){
      $('#precio_total').parent('td').addClass('destacado');
      $('#precio_total_red').parent('td').addClass('destacado');
    }else{
      $('#precio_total').parent('td').removeClass('destacado');
      $('#precio_total_red').parent('td').removeClass('destacado');
    }
}

var activa_controles_incidencias_globales = function() {
    $('#incidencias_globales').on('keydown', 'input', function(e){
        var next_row, next_input, fieldname, myrow;
        if(e.keyCode==40 || e.keyCode==38){//down arrow or up arrow
            try{
                myrow = $(this).parent().parent();
                if(e.keyCode==40){
                    next_row = myrow.next();
                }else{
                    next_row = myrow.prev();
                }
                next_input = next_row.find('input');
                next_input.focus();
                next_input.select();
            }catch(_){}
            e.preventDefault();
        }
    });
    $('#incidencias_globales').on('click', 'input', function(e){
        this.select();
        });
    $('#incidencias_globales').on('blur', 'input', function(e){
        actualiza_precios();
        });
};

var vuelca_cantidad = function(id_stub_1, id_stub_2){
  var id_str, ids, preciof, i, preciored;
  var preciotgrupo=0, preciotred=0;
  id_str = $('#ids_productos').val();
  ids = id_str?id_str.split(','):[];
  preciot = 0;
  for(i=0; i<ids.length; i++){
      iproducto = ids[i];
      cantidad1 = lee_cantidad(id_stub_1 + iproducto);
      elem = $('#' + id_stub_2 + iproducto);
      if(elem[0].tagName=='INPUT'){
        elem.val(cantidad1);
      }else{
        elem.html(cantidad1);
      }
  }
  actualiza_precios();
}

var vuelca_red_grupo = function(){
  vuelca_cantidad('cantidad_red_', 'cantidad_grupo_');
}

var vuelca_repartido_recibido = function(){
  vuelca_cantidad('cantidad_repartida_', 'cantidad_grupo_');
}

var vuelca_grupo_red = function(){
  vuelca_cantidad('cantidad_grupo_', 'cantidad_red_');
}

var aviso_antes_de_repartir = function(variante){
  $('#modal_repartir').modal();
  $('#comando').val(variante);
  $('div.modal-body div.explicaciones').hide();
  $('#'+variante+'_explicaciones').show();
}
