# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################
from gluon.sqlhtml import OptionsWidget
from gluon.html import DIV, UL, LI, INPUT, LABEL, SPAN, IMG, URL, A, P

############### Distintos tipos de contabilidad ###############

class TweakedRadioWidget(OptionsWidget):

    @classmethod
    def widget(cls, field, value, **attributes):
        """
        Generates a TABLE tag, including INPUT radios (only 1 option allowed)

        see also: `FormWidget.widget`
        """

        if isinstance(value, (list, tuple)):
            value = str(value[0])
        else:
            value = str(value)

        attr = cls._attributes(field, {}, **attributes)
        attr['_class'] = 'tweakedradiowidget'

        requires = field.requires
        if not isinstance(requires, (list, tuple)):
            requires = [requires]
        if requires:
            if hasattr(requires[0], 'options'):
                options = requires[0].options()
            else:
                raise SyntaxError('widget cannot determine options of %s'
                                  % field)
        options = [(k, v) for k, v in options if str(v)]
        opts = []
        for k,v in options:
            short1, long1, short2, long2 = v
            sd, ld = (short2, long2) if attributes.get('red') else (short1, long1)
            opts.append(
                LI(INPUT(_type='radio',
                         _id='%s%s' % (field.name, k),
                         _name=field.name,
                         _value=k,
                         value=value,
                         **{'_checked': 'checked'} if k == value else {}),
                   LABEL(sd, _for='%s%s' % (field.name, k)),
                   A(' ', IMG(_src=URL('static', 'images/info.png')), ' ',
                     SPAN(ld),
                     _href='#', _class='my_tooltip')
                                 )
            )
        return DIV(UL(*opts), **attr)


