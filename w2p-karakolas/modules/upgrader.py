# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################
import re
from datetime import date
from decimal import Decimal
from collections import defaultdict

from gluon import current
from gluon.html import URL

from .version import less

def get_upgrader():
    '''No es la solucion mas limpia, pero al mover el codigo de upgrade desde
    models/62_install_o_upgrade hasta modules/upgrader, se me ocurrio que al meter este
    codigo dentro de la funcion do_upgrade, era más fácil recrear el mismo entorno que
    tenía cada script de actualización (recuerda que sólo se puede usar current dentro de
    una función o método).
    Importante porque tras un refactor más serio no habría sido fácil comprobar que cada
    posible actualización funciona, y realmente no hay tantas instalaciones de karakolas
    hay fuera como para justificar ese esfuerzo (06-01-16).
    '''
    db = current.db
    T = current.T
    C = current.C
    class Upgrader(object):

        @staticmethod
        def upgrade(old_version, snew_version):
            upgrade_tasks = [
                ((0,0,2), Upgrader.create_mariadb_indices_run_once),
                ((0,0,2), Upgrader.prepopulate_tables_run_once),
                ((1,0,0), Upgrader.ajustes_para_upload_separate),
                ((1,0,1), Upgrader.welcome_text),
                ((1,1,0), Upgrader.textos_grupo_por_defecto_todos),
                ((1,1,1), Upgrader.crea_pedidos_fantasma),
                ((2,1,1), Upgrader.completa_campo_estado),
                ((2,3,2), Upgrader.valor_por_defecto_para_formulas_en_pedidos),
                ((2,3,3), Upgrader.valor_por_defecto_para_formulas_en_proxy_pedidos),
                ((2,3,6), Upgrader.valor_por_defecto_para_formulas_en_proxy_productor),
                ((2,4,0), Upgrader.valor_por_defecto_para_formula_sobreprecio),
                ((2,4,1), Upgrader.valor_por_defecto_para_precio_en_mascara_producto),
                ((2,4,5), Upgrader.update_compute_precio_total_red),
                ((2,4,6), Upgrader.wiki_externa),
                ((2,5,1), Upgrader.update_contabilidad),
                ((2,5,2), Upgrader.valor_por_defecto_para_cuenta_activa),
                ((2,5,2), Upgrader.valor_por_defecto_para_el_campo_activo),
                ((2,5,4), Upgrader.valor_por_defecto_para_formula_precio_productor),
                ((2,5,5), Upgrader.valor_por_defecto_para_precio_productor),
                ((2,5,6), Upgrader.valor_por_defecto_para_formula_coste_extra_productor),
                ((2,5,7), Upgrader.valor_por_defecto_para_formula_precio_productor),
                ((2,5,8), Upgrader.valor_por_defecto_para_coste_extra_productor),
#                ((3,0,0), Upgrader.rellena_tabla_producto),
                ((3,1,0), Upgrader.regenera_indices),
                ((3,1,0), Upgrader.elimina_productos_inactivos),
                ((3,1,7), Upgrader.valor_defecto_consolidado_y_problemas_calculo_coste_extra),
                ((3,1,8), Upgrader.simplifica_espacios_en_blanco_en_nombres_de_producto),
                ((3,1,11), Upgrader.completa_productoXpedido_slug),
                ((3,1,13), Upgrader.valor_defecto_pendiente_incidencias_unidades),
                ((3,2,4), Upgrader.crea_cuentas_saldo_grupo),
                ((3,2,4), Upgrader.crea_moneda_euro),
                ((3,2,21), Upgrader.valor_por_defecto_para_chivatos_pedido_y_proxy_pedido),
                ((3,3,3), Upgrader.valor_defecto_incidencias_declaradas),
#                ((3,3,5), Upgrader.rellenar_item_red)
                ((3,4,3), Upgrader.completa_operacion_fecha),
                ((3,5,2), Upgrader.completa_paginas_especiales_language),
                ((3,6,3), Upgrader.rellena_proxy_pedido_via),
                ((3,6,4), Upgrader.rellena_item_grupo_cantidad_red),
                ((3,6,5), Upgrader.rellena_item_grupo_cantidad_recibida),
                ((3,6,6), Upgrader.rellena_mascara_producto_item_red),
                ((4,0,2), Upgrader.rellena_nuevos_chivatos_pedido_proxy_pedido),
                ((4,0,6), Upgrader.rellena_proxy_productor_via),
                ((4,0,11), Upgrader.rellena_item_grupo_para_redes),
                ((4,0,12), Upgrader.rellena_item_cantidad_pedida),
                ((4,0,14), Upgrader.actualiza_variables_costes_extra),
                ((4,1,3), Upgrader.rellena_grupo_dias_pedido_reciente),
            ]

            for version, action in upgrade_tasks:
                if less(old_version, version):
                    action()
                    db.commit()
            f = open('applications/%s/private/DONE_UPGRADE'%current.request.application, mode="w")
            f.write(snew_version)
            f.close()

        @staticmethod
        def limpia_mariadb_index(t):
            duplicated_index = re.compile('[a-zA-Z_]+_\d+')
            for index in db.executesql('SHOW INDEX FROM %s;'%t):
                index_name = index[2]
                if (duplicated_index.match(index_name) and
                    index_name.rsplit('_',1)[0] in t):
                    db.executesql('DROP INDEX `%s` on %s;'%(index_name, t))
                    #Al eliminar un indice pueden desaparecer varios indices,
                    #lo que vuelve loco al bucle for, asi que repetimos por si quedan
                    #indices sueltos, pero abandonamos este bucle
                    Upgrader.limpia_mariadb_index(t)
                    return

        @staticmethod
        def create_mariadb_index(t):
            try:
                if (db._adapter.dbengine in ('mysql', 'mariadb') and
                    hasattr(t,'_mariadb_index_command')):
                    comandos = (t._mariadb_index_command
                               if isinstance(t._mariadb_index_command, (list, tuple))
                               else (t._mariadb_index_command,)
                    )
                    for comando in comandos:
                        db.executesql(comando)
                    Upgrader.limpia_mariadb_index(t)
                    db.commit()
            except Exception as e:
                print('Error creating index on table ', t)
                print(e.args)
                db.rollback()

        @staticmethod
        def create_mariadb_indices_run_once():
            if db._dbname=='mysql' or db._dbname=='mariadb':
                for t in db:
                    Upgrader.create_mariadb_index(t)

        @staticmethod
        def prepopulate_tables_run_once():
            #dudo: crear un user admin?
        #    u = db.auth_user(username='admin')
            r = db.auth_group(role='webadmins')
            if not r:
                db.auth_group.insert(role='webadmins',description=T('Administradores de este sitio web'))

            r = db.categorias_productos(codigo=50000000)
            if not r:
                db.categorias_productos.insert(codigo=50000000, nombre=T("Alimentos"))

            r = db.categorias_productos(codigo=-1)
            if not r:
                db.categorias_productos.insert(codigo=-1, nombre="_Ninguna de las anteriores")

        @staticmethod
        def ajustes_para_upload_separate():
            from shutil import move
            import os.path
            join = os.path.join
            uploads_path = join('applications', current.request.application, 'uploads')
            upload_fields = ['allfiles.file', 'plugin_wiki_attachment.filename', 'pedido.xls']
            def ujoin(*args):
                return join(uploads_path,*args)
            for p in upload_fields:
                if not os.path.exists(ujoin(p)):
                    os.makedirs(ujoin(p))
            for f in os.listdir(uploads_path):
                try:
                    table,c1,c2,ext = f.rsplit('.',3)
                    if table in upload_fields:
                        ddir = ujoin(table,c1[:2])
                        if not os.path.exists(ddir):
                            os.mkdir(ddir)
                        move(ujoin(f), join(ddir,f))
                except ValueError:
                    continue

        @staticmethod
        def welcome_text():
            r = db.paginas_especiales(slug='welcome')
            if not r:
                WELCOME_TEXT=T('''<p>Esta es la página de inicio de tu instalacion de karakolas.</p>
    <p>Te recomendamos que sigas los siguientes pasos:</p>
    <ol>
        <li>Registrar un usuario.</li>
        <li>Añadir ese usuario al grupo 'webadmins' usando el <a href="%(url_auth_membership)s">interfaz de administracion</a> (<a href="%(url_auth_membership_https)s">con el https</a> si no conectas desde localhost)</li>
        <li>Aparecerá un menú 'Wedadmin' que te permitirá crear un grupo nuevo.</li>
        <li>Después tienes que añadir un usuario como "administradora" del grupo que has creado.</li>
        <li>Al refrescar karakolas (o al entrar como el nuevo usuario admin del grupo), aparecerá un menú ’Admin XXXXX’, donde ’XXXXX’ es el grupo que has creado.</li>
        <li>Entra en el menú "Personas", y usa el link para "incorporacion en bloque al grupo" como una forma rápida de incorporar gente (pueden ser emails falsos si sólo quieres probar la aplicación).</li>
        <li>Los emails con los resúmenes de los pedidos se envían por la noche. Para que estos emails se envíen correctamente, es necesario configurar algún tipo de tarea periódica, usando cron o similar. Puedes configurar cualquier servidor o pc para que llame a la url %(url_envia_avisos_email)s usando curl, wget, o cualquier otro mecanismo. También puedes configurar cualquiera de los <a href="http://web2py.com/books/default/chapter/29/04/the-core#Cron" target="_blank">sistemas cron de web2py</a>. Lo más sencillo es que añadas esta linea al archivo crontab de cualquier usuario del servidor donde has instalado karakolas (cambia la ruta a tu instalación de web2py):</li>
        <code>0 0 * * * /path/to/web2py/applications/karakolas/cron/daily_tasks.py</code>
        <li>Si esta instalación es pública, cambia este texto en la página de <a href="%(url_edicion)s">edicion</a></li>
    </ol>
                '''%{'url_edicion':URL(c='default', f='edita.html',vars=dict(slug='welcome')),
                     'url_auth_membership':URL(c='appadmin', f='insert.html', args=['db','auth_membership']),
                     'url_auth_membership_https':URL(c='appadmin', f='insert.html', args=['db','auth_membership'], scheme='https'),
                     'url_envia_avisos_email':URL(c='pedir',f='envia_avisos_email.html',host=True)})

                db.paginas_especiales.insert(slug='welcome', title=T('Bienvenida a Karakolas'), body=WELCOME_TEXT)

        @staticmethod
        def textos_grupo_por_defecto_todos(host=None):
            from .modelos.grupos import textos_grupo_por_defecto
            for r in db(db.grupo.red==False).select(db.grupo.id, db.grupo.nombre):
                textos_grupo_por_defecto(r.id,r.nombre,host)

        @staticmethod
        def crea_pedidos_fantasma():
            from .modelos.pedidos import crea_pedido_fantasma
            for row in db(db.productor).select(db.productor.id):
                #crea_pedido_fantasma no crea el pedido fantasma si ya existe
                crea_pedido_fantasma(row.id)

        @staticmethod
        def completa_campo_estado():
            for r in db(db.proxy_productor.estado==None).select():
                if r.aceptado:
                    r.update_record(estado='auto')
                else:
                    r.update_record(estado='bloqueado')

        @staticmethod
        def valor_por_defecto_para_formulas_en_pedidos():
            db(db.productor.formula_coste_extra_pedido==None).update(formula_coste_extra_pedido='0')
            db(db.productor.formula_precio_final==None).update(formula_precio_final='precio_base')
            db(db.pedido.formula_coste_extra_pedido==None).update(formula_coste_extra_pedido='0')
        #    db(db.pedido.formula_precio_final==None).update(formula_precio_final='precio_final')
            for row in db((db.pedido.formula_precio_final==None) &
                          (db.pedido.productor==db.productor.id))\
                     .select(db.productor.formula_precio_final, db.pedido.id):
                db.pedido[row.pedido.id].update_record(formula_precio_final=row.productor.formula_precio_final)

        @staticmethod
        def valor_por_defecto_para_formulas_en_proxy_pedidos():
            db(db.proxy_pedido.formula_coste_extra_pedido==None).update(formula_coste_extra_pedido='COSTE_PEDIDO_RED/UNIDADES_EN_PEDIDO')

        @staticmethod
        def valor_por_defecto_para_formulas_en_proxy_productor():
            db(db.proxy_productor.formula_coste_extra_pedido==None) \
             .update(formula_coste_extra_pedido='COSTE_PEDIDO_RED*COSTE_UNIDAD_EN_RED/COSTE_GRUPO_EN_RED')

        @staticmethod
        def valor_por_defecto_para_formula_sobreprecio():
            db(db.proxy_pedido.formula_sobreprecio==None) \
             .update(formula_sobreprecio=C.FORMULA_SOBREPRECIO_PEDIDO_COORDINADO_POR_DEFECTO)
            db(db.proxy_productor.formula_sobreprecio==None) \
             .update(formula_sobreprecio=C.FORMULA_SOBREPRECIO_PEDIDO_COORDINADO_POR_DEFECTO)

        @staticmethod
        def valor_por_defecto_para_precio_en_mascara_producto():
            for row in db((db.mascara_producto.precio==None) &
                          (db.mascara_producto.productoXpedido==db.productoXpedido.id)) \
                       .select(db.productoXpedido.precio_final,
                               db.mascara_producto.id):
                db.mascara_producto(row.mascara_producto.id).update_record(precio=row.productoXpedido.precio_final)

        @staticmethod
        def update_compute_precio_total_red():
            for tabla in (db.peticion, db.item, db.item_grupo):
                for r in db((tabla.productoXpedido==db.productoXpedido.id) &
                            (tabla.precio_red==None)) \
                         .select(db.productoXpedido.precio_final,
                                 tabla.cantidad,
                                 tabla.id):
                    tabla[r[tabla.id]].update_record(
                        precio_red=r.productoXpedido.precio_final*r[tabla.cantidad]
                    )

        @staticmethod
        def wiki_externa():
            'Obsoleto tras karakolas v3'
            pass
#             if 'wiki_externa' not in globals():
#                 global wiki_externa
#                 wiki_externa = None
#                 f = open('applications/%s/models/10_connection_string.py'%current.request.application, mode="a")
#                 f.write('''
# ### Insertado automaticamente en una actualizacion de karakolas
# #Usar una wiki externa para la ayuda
# #wiki_externa = 'http://www.otra_instalacion_de_karakolas.net'
# wiki_externa = None
#     ''')

        @staticmethod
        def update_contabilidad():
            from .contabilidad import TiposCuenta
            TIPOS_CUENTA = TiposCuenta()
            if db._dbname=='mysql' or db._dbname=='mariadb':
                for t in (db.cuenta, db.operacion, db.apunte):
                    Upgrader.create_mariadb_index(t)
            #Establece las opciones por defecto para la contabilidad de los grupos
            for f in (db.grupo.opcion_ingresos, db.grupo.opcion_pedidos):
                db(f==None).update(**{f.name:f.default})

            #Crea todas las cuentas, si no existen ya
            for tc in TIPOS_CUENTA.values():
                cc = tc.creador_cuentas()
                for row in db(tc.table).select():
                    cc(row, row.id)

        @staticmethod
        def valor_por_defecto_para_cuenta_activa():
            db(db.cuenta.activa==None).update(activa=True)

        @staticmethod
        def valor_por_defecto_para_el_campo_activo():
            for t in (db.grupo, db.productor, db.grupoXred):
                db(t.activo==None).update(activo=True)

        @staticmethod
        def valor_por_defecto_para_formula_precio_productor():
            for t in (db.productor, db.pedido):
                db(t.formula_precio_productor==None) \
                 .update(formula_precio_productor='precio_base')

        @staticmethod
        def valor_por_defecto_para_precio_productor():
            for t in (db.producto, db.productoXpedido):
                db(t.precio_productor==None) \
                 .update(precio_productor=t.precio_final)

            for t in (db.item, db.peticion, db.item_grupo):
                db(t.precio_productor==None) \
                 .update(precio_productor=t.precio_total)

        @staticmethod
        def valor_por_defecto_para_formula_coste_extra_productor():
            for t in (db.productor, db.pedido):
                db(t.formula_coste_extra_productor==None) \
                 .update(formula_coste_extra_productor='0')

        @staticmethod
        def valor_por_defecto_para_coste_extra_productor():
            db(db.pedido.coste_extra_productor==None) \
             .update(coste_extra_productor='0')

        @staticmethod
        def rellena_tabla_producto():
            if 'slug' in db.producto: #db.producto.slug finalmente fue eliminado
                fs = [f for f in db.productoXpedido.fields if f not in ['id', 'pedido', 'producto']]
                if db._adapter.dbengine in ('mysql', 'mariadb', 'postgresql'):
                    sql = 'UPDATE producto AS p, productoXpedido AS pp SET %s WHERE pp.producto=p.id'%(
                        ', '.join('pp.%s=p.%s'%(f,f) for f in fs))
                    db.executesql(sql)

        @staticmethod
        def regenera_indices():
            if db._dbname=='mysql' or db._dbname=='mariadb':
#                from pymysql.err import InternalError, OperationalError

                dtablas = {db.producto:'productor',
                           db.valor_extra_string_pedido:'columna',
                           db.valor_extra_boolean_pedido:'columna',
                           db.valor_extra_decimal_pedido:'columna'   }
                #elimina algunos indices obsoletos y vuelve a crearlos
                for tabla, index in dtablas.items():
                    print(tabla, index)
                    try:
                        sql = "ALTER TABLE %s DROP INDEX %s;"%(tabla.sql_shortref, index)
                        db.executesql(sql)
                        print('ok')
                        print()
                    #except (OperationalError, InternalError) as e:
                    except Exception as e:
                        print('No se ha podido eliminar un indice')
                        print(e.args)
                        print('Continuamos, probablemente no es grave')
                        print('-'*30)

                for tabla in dtablas:
                    Upgrader.create_mariadb_index(tabla)

        @staticmethod
        def elimina_productos_inactivos():
            #Elimina los productoXpedido inactivos en pedidos que no sean el fantasma
            pppids = [row.id for row in  db((db.productoXpedido.activo==False) &
                                            (db.productoXpedido.pedido==db.pedido.id) &
                                            (db.pedido.fecha_reparto>date(1980,1,1))
                                    ).select(db.productoXpedido.id)]
            db(db.productoXpedido.id.belongs(pppids)).delete()

        @staticmethod
        def valor_defecto_consolidado_y_problemas_calculo_coste_extra():
            for t in (db.pedido, db.proxy_pedido):
                db(t.pendiente_consolidacion==None).update(pendiente_consolidacion=False)
                db(t.problemas_calculo_coste_extra==None).update(
                    problemas_calculo_coste_extra=False
                )

        @staticmethod
        def simplifica_espacios_en_blanco_en_nombres_de_producto(dbio=True):
            '''Atención, puede tardar mucho!
            '''
            from .kutils import parse_decimal, nombre2slug, reduce_espacios_nombre

            tppp = db.productoXpedido
            WHITESPACE = re.compile('\s+')
            # for row in db(tppp).select(tppp.id, tppp.nombre):
            #     tppp(row.id).update_record(nombre=WHITESPACE.sub(' ', row.nombre))
            # db.commit()
            #Ahora miramos si el cambio ha idenfificado algunos nombres de producto
            for row_productor in db(db.productor).select(
                db.productor.id, db.productor.nombre, db.productor.grupo,
                orderby=db.productor.grupo):
                print()
                print('#'*30)
                print(row_productor.id, ':', row_productor.nombre)
                #pedido a pedido no permitimos dos productos con el mismo nombre
                for row_pedido in db(db.pedido.productor).select(db.pedido.id):
                    ipedido = row_pedido.id
                    todelete = set()
                    prods_con_peticion = set(row.id
                        for row in db((tppp.pedido==ipedido) &
                                      (db.peticion.cantidad>0) &
                                      (db.peticion.productoXpedido==tppp.id)).select(
                                      tppp.id, groupby = tppp.id
                                      ))
                    #Por si acaso, miramos también los productos con item_grupo
                    prods_con_peticion.update(row.id
                        for row in db((tppp.pedido==ipedido) &
                                      (db.item_grupo.cantidad>0) &
                                      (db.item_grupo.productoXpedido==tppp.id)).select(
                                      tppp.id, groupby = tppp.id
                                      ))
                    dproductos = {}
                    for row_ppp in db((tppp.pedido==ipedido)).select(
                                  tppp.id, tppp.nombre, tppp.producto
                                 ):
                        slug = nombre2slug(row_ppp.nombre)
                        otro_pppid, otro_prod = dproductos.get(slug, (None, None))
                        if not otro_pppid:
                            dproductos[slug] = row_ppp.id, row_ppp.producto
                        elif row_ppp.id not in prods_con_peticion:
                            todelete.add(row_ppp.id)
                        elif otro_pppid not in prods_con_peticion:
                            todelete.add(otro_pppid)
                            dproductos[slug] = row_ppp.id, row_ppp.producto
                        else:#ambos ppps tienen peticiones
                            if otro_prod == row_ppp.producto:
                                #si el producto es el mismo, los identificamos
                                if dbio:
                                    for t in (db.peticion, db.item_grupo, db.item_rd):
                                        db(t.productoXpedido==tppp.id).update_record(
                                            productoXpedido=otro_pppid)
                                        todelete.add(row_ppp.id)
                                else:
                                    print('> ppps %d y %d han sido identificados (%s)'%(
                                        row_ppp.id, otro_pppid,slug))
                            else:
                                #si no, buscamos un nombre nuevo para el nuevo producto
                                k = 2
                                nuevo_slug = '%s_%d'%(slug, k)
                                while nuevo_slug in dproductos:
                                    k += 1
                                    nuevo_slug = '%s_%d'%(slug, k)
                                dproductos[nuevo_slug] = row_ppp.id
                                if dbio:
                                    tppp(row_ppp.id).update_record(
                                        nombre='%s_%d'%(row_ppp.nombre, k)
                                    )
                                else:
                                    print('> nombre de ppp %d cambiado %s => %s'%(
                                        row_ppp.id, slug, nuevo_slug))
                    if todelete:
                        print('%d ppps eliminados en el pedido %d'%(len(todelete),ipedido))
                    if dbio:
                        db(tppp.id.belongs(todelete)).delete()
                        #Positivamos los cambios por si hay que parar y volver a empezar porque
                        #tarde demasiado...
                        db.commit()

                print('#'*30)
                print('Ahora comparamos nombres de productos entre pedidos del mismo productor')
                slug2producto = {}
                total_redirecciones = 0
                for row_ppp in db((tppp.pedido==db.pedido.id) &
                                  (db.pedido.productor==row_productor.id)).select(
                                  tppp.nombre, tppp.id, tppp.producto,
                                  orderby = tppp.pedido
                                  ):
                    slug = nombre2slug(row_ppp.nombre)
                    otro_producto = slug2producto.get(slug)
                    if otro_producto and otro_producto!=row_ppp.producto:
                        total_redirecciones += 1
                        if dbio:
                            tppp(row_ppp.id).update_record(producto = otro_producto)
                        else:
                            print('> redireccion %s, %s: producto=%d'%(
                                slug, row_ppp.id, otro_producto
                            ))
                    else:
                        slug2producto[slug] = row_ppp.producto
                print('Se han redirigido %d ppps'%total_redirecciones)

            #Eliminamos las entradas de db.producto que han quedado desiertas
            productos_en_uso = set(row.producto for row in db(tppp).select(
                tppp.producto, groupby=tppp.producto
            ))
            s = db(~db.producto.id.belongs(productos_en_uso))
            if dbio:
                total_productos_borrados = s.delete()
            else:
                total_productos_borrados = s.count()
            print('#'*30)
            print('Se han borrado %d productos'%total_productos_borrados)
            if dbio:
                #Positivamos los cambios por si hay que parar y volver a empezar porque
                #tarde demasiado...
                db.commit()

        @staticmethod
        def completa_productoXpedido_slug(dbio=True):
            '''Atención, puede tardar mucho!
            '''
            from .kutils import nombre2slug
            tppp = db.productoXpedido
            for row in db(tppp.slug==None).select(tppp.id, tppp.nombre):
                tppp(row.id).update_record(slug=nombre2slug(row.nombre))

        @staticmethod
        def valor_defecto_pendiente_incidencias_unidades():
            for t in (db.pedido, db.proxy_pedido):
                db(t.pendiente_incidencias_unidades==None).update(
                    pendiente_incidencias_unidades=False)

        @staticmethod
        def crea_cuentas_saldo_grupo():
            from .cuentas_derivadas import TiposCuentaDerivada, SaldoGrupo
            for row in db(db.grupo).select(db.grupo.id, db.grupo.activo):
                rcuenta = db.cuenta_derivada(tipo=TiposCuentaDerivada.SALDO_GRUPO,
                                             owner=row.id)
                if not rcuenta:
                    db.cuenta_derivada.insert(tipo=TiposCuentaDerivada.SALDO_GRUPO,
                                              owner=row.id,
                                              activa=row.activo)
                    SaldoGrupo().recalcula_saldo_cuenta_derivada(row.id)

        @staticmethod
        def crea_moneda_euro():
            from .cuentas_derivadas import TiposCuentaDerivada, CajaGrupo
            moneda = db(db.moneda).select().first()
            if moneda:
                imoneda = moneda.id
            else:
                imoneda = db.moneda.insert(nombre='euro',
                                           simbolo_html='&euro;',
                                           simbolo_tex='\\geneuro')
                db(db.operacion).update(moneda=imoneda)
            for row in db(db.grupo).select(db.grupo.id, db.grupo.activo):
                rgxm = db.grupoXmoneda(grupo=row.id)
                if not rgxm:
                    db.grupoXmoneda.insert(grupo=row.id,
                                           moneda=imoneda,
                                           activo=row.activo)
                    CajaGrupo().recalcula_saldo_cuenta_derivada(row.id, imoneda)

        @staticmethod
        def valor_por_defecto_para_chivatos_pedido_y_proxy_pedido():
            for t in (db.pedido, db.proxy_pedido):
                db(t.discrepancia_asumida==None).update(discrepancia_asumida=0)

        @staticmethod
        def valor_defecto_incidencias_declaradas():
            for t in (db.pedido, db.proxy_pedido):
                db(t.incidencias_declaradas==None).update(incidencias_declaradas=False)

        @staticmethod
        def rellenar_item_red():
            '''
            Se trata de completar item_red con lo mismo que hay en item_grupo,
            excepto si ya hay un item_red para ese item_grupo
            '''
            ids_excluidos = set(row.id   for row in db(
                (db.item_grupo.productoXpedido == db.item_red.productoXpedido) &
                (db.item_grupo.grupo == db.item_red.grupo)
                ).select(db.item_grupo.id))
            if ids_excluidos:
                campos = 'productoXpedido,grupo,cantidad,precio_total,precio_red,precio_productor'
                #https://mariadb.com/kb/en/mariadb/insert-select/
                #aviso: no lo hemos probado en postgresql, aunque posiblemente funcione
                #http://www.postgresql.org/docs/current/static/sql-insert.html
                sql = 'INSERT INTO item_red (%(campos)s) SELECT %(campos)s'\
                      ' FROM item_grupo WHERE (item_grupo.id NOT IN (%(ids_excluidos)s));'%dict(
                    campos=campos,
                    ids_excluidos=','.join('%d'%idex for idex in sorted(ids_excluidos))
                )
                db.executesql(sql)

        @staticmethod
        def completa_operacion_fecha():
            compute_fecha = db.operacion.fecha.compute
            for r in db(db.operacion).select():
                r.update_record(fecha=compute_fecha(r))

        @staticmethod
        def completa_paginas_especiales_language():
            db(db.paginas_especiales.language==None).update(language='es')

        @staticmethod
        def rellena_proxy_pedido_via():
            for row in db((db.proxy_pedido.pedido==db.pedido.id) &
                          (db.pedido.productor==db.productor.id)
                          ).select(db.proxy_pedido.id, db.productor.grupo):
                db.proxy_pedido(row.proxy_pedido.id).update_record(via=row.productor.grupo)

        @staticmethod
        def rellena_item_grupo_cantidad_red():
            db(db.item_grupo).update(cantidad_red=db.item_grupo.cantidad)

        @staticmethod
        def rellena_item_grupo_cantidad_recibida():
            db(db.item_grupo).update(cantidad_recibida=db.item_grupo.cantidad_red)

        @staticmethod
        def rellena_mascara_producto_item_red():
            '''ADVERTENCIA: puede ser lento'''
            from modelos.pedidos import activa_productos_coordinados
            #TODO: lo podríamos hacer más rápido de otro modo...
            for row in db(db.pedido).select(db.pedido.id):
                activa_productos_coordinados(row.id)

        @staticmethod
        def rellena_nuevos_chivatos_pedido_proxy_pedido():
            '''Actualiza los nuevos chivatos de estados de pedido en función de los antiguos

            v4.0.11 usamos esta versión de rellena_nuevos_chivatos_pedido_proxy_pedido en vez de
            rellena_nuevos_chivatos_pedido_proxy_pedidov2, para mantener como pendientes los
            mismos pedidos que ya estaban pendientes, causar la mínima disrupción, y a partir
            de ese momento, actualizar los nuevos chivatos usando las herramientas de v4
            '''
            db(db.pedido.discrepancia_productor==None).update(discrepancia_productor=False)
            db(db.pedido.discrepancia_interna==None).update(
                discrepancia_interna=db.pedido.pendiente_incidencias_unidades)
            db(db.pedido.discrepancia_interna_asumida==None).update(
                discrepancia_interna_asumida=db.pedido.discrepancia_asumida>0)
            pedidos_coords_a_rellenar =  [row.id for row in
                db((db.pedido.discrepancia_descendiente==None) &
                   (db.pedido.productor==db.productor.id) &
                   (db.productor.grupo==db.grupo.id) &
                   (db.grupo.red==True)).select(db.pedido.id)]
            db((db.pedido.id.belongs(pedidos_coords_a_rellenar))).update(
                discrepancia_descendiente=db.pedido.pendiente_incidencias_unidades)

            db(db.proxy_pedido.discrepancia_ascendiente==None).update(
                discrepancia_ascendiente=db.proxy_pedido.incidencias_declaradas)
            db(db.proxy_pedido.discrepancia_interna==None).update(
                discrepancia_interna=db.proxy_pedido.pendiente_incidencias_unidades)
            db(db.proxy_pedido.discrepancia_interna_asumida==None).update(
                discrepancia_interna_asumida=db.proxy_pedido.discrepancia_asumida>0)
            pedidos_coords_a_rellenar =  [row.id for row in
                db((db.proxy_pedido.discrepancia_descendiente==None) &
                   (db.proxy_pedido.grupo==db.grupo.id) &
                   (db.grupo.red==True)).select(db.proxy_pedido.id)]
            db((db.proxy_pedido.id.belongs(pedidos_coords_a_rellenar))).update(
                discrepancia_descendiente=db.proxy_pedido.pendiente_incidencias_unidades)

        @staticmethod
        def rellena_nuevos_chivatos_pedido_proxy_pedidov2():
            '''
            v4.0.11 opto por rellena_nuevos_chivatos_pedido_proxy_pedido porque
            las funciones hay_discrepancia_descendiente dependen de cantidad_recibida etc
            que se han rellenado copiando o sumando cantidad etc
            luego no reflejan la realidad.

            Mejor mantener como pendientes los mismos pedidos que ya estaban pendientes, y a partir
            de ese momento, actualizar los nuevos chivatos usando las herramientas de v4

            Sin embargo, conservo esta función que podría servir para regenerar todos los chivatos :-/
            '''
            from modelos.pedidos import hay_discrepancia_descendiente, \
                                        hay_discrepancia_interna, \
                                        hay_discrepancia_ascendiente
            for row in db(db.pedido).select():
                ipedido = row.id
                igrupo = db.pedido(ipedido).productor.grupo
                row.update_record(
                    discrepancia_productor=hay_discrepancia_ascendiente(ipedido, igrupo),
                    discrepancia_interna=hay_discrepancia_interna(ipedido, igrupo),
                    discrepancia_descendiente=hay_discrepancia_descendiente(ipedido, igrupo)
                )
            for row in db(db.proxy_pedido).select():
                igrupo = row.grupo
                ipedido = row.pedido
                row.update_record(
                    discrepancia_ascendiente=hay_discrepancia_ascendiente(ipedido, igrupo),
                    discrepancia_interna=hay_discrepancia_interna(ipedido, igrupo),
                    discrepancia_descendiente=hay_discrepancia_descendiente(ipedido, igrupo)
                )
            #La discrepancia asumida o no no se puede deducir del estado de item_grupo:
            db(db.pedido.discrepancia_interna_asumida==None).update(
                discrepancia_interna_asumida=db.pedido.discrepancia_asumida>0)
            db(db.proxy_pedido.discrepancia_interna_asumida==None).update(
                discrepancia_interna_asumida=db.proxy_pedido.discrepancia_asumida>0)

        @staticmethod
        def rellena_item_grupo_para_redes():
            '''Cuidado: puede ser muuuy largo.'''
            from modelos.pedidos import suma_item_grupo_red
            from modelos.grupos import niveles_pedido
            ipedidos = [(r.pedido.id, r.productor.grupo) for r in
                        db((db.pedido.productor==db.productor.id))
                        .select(db.pedido.id, db.productor.grupo)]
            for ipedido, igrupo in ipedidos:
                #TODO: redderedes v4 suma_item_grupo...
                niveles = niveles_pedido(igrupo, ipedido)
                # si es un grupo no hacemos, nada, por eso empezamos en el índice 1
                for nivel, igrupos in enumerate(niveles[1:]):
                    for isubgrupo in igrupos:
                        suma_item_grupo_red([ipedido], isubgrupo, actualizar_cantidad_red=True)
                print('rellena_item_grupo_para_redes', ipedido)
                db.commit()

        @staticmethod
        def rellena_item_cantidad_pedida():
            '''Cuidado: puede ser muuuy largo.

            Hacemos db.commit() de vez en cuando para que, aunque la ejecución se interrumpa, por lo
            menos el trabajo haya avanzado'''
            from modelos.grupos import esquema_grupos_pedido
            for rpedido in db((db.pedido.productor==db.productor.id)
                             ).select(db.pedido.id, db.productor.grupo):
                # Identifica todos los grupos y redes involucrados en este pedido
                esquema = esquema_grupos_pedido(rpedido.productor.grupo, ipedido=rpedido.pedido.id)
                # suma peticiones de unidades a item
                cantidad_peticion_por_unidades = defaultdict(Decimal)
                precio_peticion_por_unidades = defaultdict(Decimal)
                # suma las mismas peticiones a item_grupo, para los grupos y la red que lanza el
                # pedido y todas las subredes involucradas
                cantidad_peticion_por_grupos = defaultdict(Decimal)
                precio_peticion_por_grupos = defaultdict(Decimal)
                for row in db(
                    (db.peticion.productoXpedido==db.productoXpedido.id) &
                    (db.productoXpedido.pedido==rpedido.pedido.id) &
                    (db.peticion.persona==db.personaXgrupo.persona) &
                    (db.peticion.grupo==db.personaXgrupo.grupo)
                    ).select(db.personaXgrupo.unidad,
                             db.peticion.grupo,
                             db.peticion.productoXpedido,
                             db.peticion.cantidad,
                             db.peticion.precio_total):
                    quien = (row.personaXgrupo.unidad, row.peticion.grupo, row.peticion.productoXpedido)
                    _,grupo,pxp = quien
                    cantidad = row.peticion.cantidad
                    precio = row.peticion.precio_total
                    cantidad_peticion_por_unidades[quien] += cantidad
                    precio_peticion_por_unidades[quien] += precio
                    cantidad_peticion_por_grupos[(grupo, pxp)] += cantidad
                    precio_peticion_por_grupos[(grupo, pxp)] += precio
                    # en una instalación antigua y muy trabajada, puede hacer una peticion para un grupo
                    #  que no esté en el esquema ¿un pedido que se desactivó después de abierto?
                    #  => ignoramos esa peticion y continuamos
                    redes = esquema.get(grupo, [])
                    for red in redes:
                        cantidad_peticion_por_grupos[(red, pxp)] += cantidad
                        precio_peticion_por_grupos[(red, pxp)] += precio

                for quien in cantidad_peticion_por_unidades:
                    unidad, grupo, pxp = quien
                    item = db.item(grupo=grupo, unidad=unidad, productoXpedido=pxp)
                    if item:
                        item.update_record(
                        cantidad_pedida = cantidad_peticion_por_unidades[quien],
                        precio_pedido = precio_peticion_por_unidades[quien]
                    )
                for quegrupo in cantidad_peticion_por_grupos:
                    grupo, pxp = quegrupo
                    ig = db.item_grupo(grupo=grupo, productoXpedido=pxp)
                    if ig:
                        ig.update_record(
                        cantidad_pedida = cantidad_peticion_por_grupos[quegrupo],
                        precio_pedido = precio_peticion_por_grupos[quegrupo]
                    )
                print('rellena_item_cantidad_pedida', rpedido.pedido.id)
                db.commit()

            db(db.item_grupo).update(cantidad_red=db.item_grupo.cantidad)

        @staticmethod
        def rellena_proxy_productor_via():
            # No es suficiente si ya hay redes anidadas, pero todavía no hemos terminado la
            #  rama, podemos asumir que solo hay grupos y redes de primer nivel
            for row in db((db.proxy_productor.productor==db.productor.id)
                          ).select(db.proxy_productor.id, db.productor.grupo):
                db.proxy_productor(row.proxy_productor.id).update_record(via=row.productor.grupo)

        @staticmethod
        def actualiza_variables_costes_extra():
            def replace_vars(s):
                for old,new in [
                    ('HA_PEDIDO_EN_RED', 'HA_PEDIDO_A_LA_RED'),
                    ('COSTE_UNIDAD_EN_RED', 'COSTE_UNIDAD_EN_PEDIDOS_RED'),
                    ('UNIDADES_EN_PEDIDO_EN_RED', 'UNIDADES_EN_PEDIDOS_DE_LA_RED'),
                    ('COSTE_GRUPO_EN_RED', 'COSTE_GRUPO_EN_PEDIDOS_RED'),
                    ('HA_PEDIDO_EN_FECHA', 'HA_PEDIDO_A_LA_RED'),
                    ('COSTE_UNIDAD_FECHA', 'COSTE_UNIDAD_EN_PEDIDOS_RED'),
                    ('UNIDADES_EN_PEDIDO_FECHA', 'UNIDADES_EN_PEDIDOS_DE_LA_RED'),
                    ('UNIDADES_EN_PEDIDOS_RED', 'UNIDADES_EN_PEDIDOS_DE_LA_RED'),
                    ('COSTE_GRUPO_FECHA', 'COSTE_GRUPO_EN_PEDIDOS_RED'),
                    ('GRUPO_HA_PEDIDO_EN_FECHA', 'GRUPO_HA_PEDIDO_A_LA_RED'),
                    ('COSTE_RED_FECHA', 'COSTE_RED_EN_PEDIDOS_RED'),
                    ('CANTIDAD_RED_FECHA', 'CANTIDAD_RED_EN_PEDIDOS_RED'),
                    ('GRUPOS_EN_FECHA', 'GRUPOS_EN_PEDIDOS_RED')
                    ]:
                    s=s.replace(old, new)
                return s
            # No es necesario con formula_coste_extra_productor, allí no se admiten estas variables
            for tabla in (db.pedido, db.proxy_pedido, db.productor, db.proxy_productor):
                for row in db(tabla.formula_coste_extra_pedido.contains('_EN_RED') |
                              tabla.formula_coste_extra_pedido.contains('_EN_PEDIDOS_RED') |
                              tabla.formula_coste_extra_pedido.contains('_FECHA'))\
                           .select(tabla.id, tabla.formula_coste_extra_pedido):
                    tabla[row.id].update_record(formula_coste_extra_pedido=
                        replace_vars(row.formula_coste_extra_pedido)
                    )

        @staticmethod
        def rellena_grupo_dias_pedido_reciente():
            db(db.grupo).update(dias_pedido_reciente=db.grupo.dias_pedido_reciente.default)


    return Upgrader
