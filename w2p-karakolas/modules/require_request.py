# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################

from gluon import current
from gluon.http import redirect
from gluon.html import URL
from gluon.validators import IS_DATE, IS_INT_IN_RANGE, IS_FLOAT_IN_RANGE, IS_SLUG
from functools import reduce
try:#web2py >=2.9.12
   from gluon.dal.objects import Table
except:#web2py <=2.9.11
   from gluon.dal import Table

TYPES = {'string': lambda x:(x,None),
         'slug':   IS_SLUG(check=True),
         'date':   IS_DATE(),
         'integer':IS_INT_IN_RANGE(-1e12, +1e12),
         'float':  IS_FLOAT_IN_RANGE()}

def requires_get_arguments(*get_args, **kwds):
    def check_accepts(action):
        if not get_args:
            return action
        def f(*args_f, **kwds_f):
            vals = current.request.get_vars
            for (n,t) in get_args:
                #Daba un error desconocido y aleatorio en urls que tuvieran un argumento
                #get de tipo date
#                if t=='date':
#                    continue
                v = vals.get(n)
                a, c, f = current.request.application, current.request.controller, current.request.function
                if not v:
                    redirect(URL(a=a, c='default', f='wrong_parameters',
                                 args=[c, f, n]),
                             client_side=True)
                if isinstance(t, str):
                    val,error = TYPES[t](v)
                    if error:
                        redirect(URL(a=a, c='default', f='wrong_parameters',
                                     args=[c,f], vars={n:v}),
                                 client_side=True)
                    else:
                        continue
                if not t(v):
                    redirect(URL(a=a, c='default', f='wrong_parameters',
                                 args=[c,f], vars={n:v}),
                             client_side=True)
                db = t._db

            condition = kwds.get('_consistency', None)
            def check_condition(c):
                return db(c &
                          reduce(lambda a,b:(a & b) ,
                                 (t.id==vals[n] for (n,t) in get_args
                                                if isinstance(t, Table)))
                        ).count() > 0
            if isinstance(condition, (list, tuple)):
                is_consistent = any(check_condition(c) for c in condition)
            elif condition:
                is_consistent = check_condition(condition)
            else:
                is_consistent = True
            if not is_consistent:
                redirect(URL(a=a, c='default', f='wrong_parameters.html',
                             args=[c,f], vars=dict(_consistency=condition)),
                         client_side=True)
            return action(*args_f, **kwds_f)
        f.__doc__ = action.__doc__
        f.__name__ = action.__name__
        f.__dict__.update(action.__dict__)
        return f
    return check_accepts

requires_get_arguments.MENSAJE_PROBLEMA_URL = current.T('Por favor si has llegado a esta página haciendo click en un menú o un link de esta web, informa a la administradora de tu grupo para que tome las medidas oportunas, indicando qué menú o qué link has seguido para llegar a esta página. Si has modificado la URL a mano: compa, te lo has buscado.')
