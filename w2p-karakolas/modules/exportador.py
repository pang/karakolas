# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

from decimal import Decimal
from collections import defaultdict
from itertools import chain

from gluon import current
from functools import reduce
try:#web2py >=2.9.12
   from gluon.dal.objects import Expression
except:#web2py <=2.9.11
   from gluon.dal import Expression

from .kutils import represent, pprint_c, pprint_p
from .modelos.pedidos import tipo_pedido, TiposPedido, pedidos_del_dia_por_redes

class SafeDecimal(Decimal):
    def __new__(cls, val):
        return Decimal.__new__(cls, val or 0)

class Numero(SafeDecimal):
    pass

class Cantidad(SafeDecimal):
    pass

class Precio(SafeDecimal):
    pass

from odswriter import Formula

class MatrixFormula(str):
    pass

def ref(row, col, dolar = 0):
    '''Devuelve la referencia en formato hoja de cálculo para (fila, columna). Ej (2,3)->B4'''
    # TODO: Solo llega hasta la Z, podría ser AA1 AZ1, etc...
    s = chr(65+col%26)
    col = col//26
    while col>0:
        s = chr(64+col%26) + s
        col = col//26
    dolar_row, dolar_col = dolar&ref.ROW, dolar&ref.COL
    return '%s%s%s%d'%('$' if dolar_col else '', s, '$' if dolar_row else '', row+1)

ref.ROW = 1
ref.COL = 2
ref.ROWCOL = 3

################################
# Utils
################################

def _alias_columnas_extra(db, columnas_extra):
    return [(row.id,
             db['valor_extra_%s_pedido'%row.tipo.split('(')[0]] \
               .with_alias('_' + row.nombre.replace('-','_')))
            for row in columnas_extra]

def pretty_record(table, r):
    ls = []
    for tf in table:
        t,f = str(tf).split('.')
        field = current.db[t][f]
        type  = field.type
        if type=='id' or not field.readable:
            continue
        elif type.startswith('reference'):
            ls.append( (f, represent(current.db[type.split(' ')[-1]], r[t+'.'+f])))
        elif type.startswith('list:reference'):
#                line += str(r[t+'.'+f])
            reft = current.db[type.split(' ')[-1]]
            ls.append( [f] +  [represent(reft, ir) for ir in r[t+'.'+f] ])
        else:
            ls.append( (f, str(r[t+'.'+f] or '') ))
    return ls

def pretty_csv_record(table, r, locale=None):
    from .csv_utils import csv_dump_sheet
    return csv_dump_sheet(pretty_record(table, r), locale=locale)

def pretty_results(results, fields):
    ls = []
    ls.append([str(f) for f in fields])
    for r in results:
        line=[]
        for tf in fields:
            if isinstance(tf, str):
                #si tf no es una columna de una tabla, es porque queremos mostrar esa columna
                #en el output, aunque no sea una columna extra de este pedido,
                #y en ese caso, las dejamos en blanco
                line.append('')
                continue
            if type(tf) == Expression:
                line.append( Numero(r[tf]))
                continue
            t,f = str(tf.table), tf.name
            ftype = tf.type
            if ftype.startswith('reference'):
                line.append( represent(current.db[ftype.split(' ')[-1]], r[tf]) )
            elif ftype.startswith('list:reference'):
                line.append( ','.join(represent(current.db[ftype.split(' ')[-1]], ir) for ir in r[tf]) )
            elif 'cantidad' in f:
                line.append( Cantidad(r[tf]))
            elif 'precio' in f:
                line.append( Precio(r[tf]))
            elif ftype.startswith('decimal') or (ftype in ('float', 'integer')):
                line.append( Numero(r[tf]) )
            else:
                line.append( str(r[tf] or '') )
        ls.append(line)
    return ls

def pretty_csv(results, fields, locale=None):
    from .csv_utils import csv_dump_sheet
    return csv_dump_sheet(pretty_results(results, fields), locale=locale)

def pretty_ods(results, fields, nombre_fichero):
    from .ods_utils import ods_write_sheet
    ods_write_sheet(pretty_results(results, fields), nombre_fichero)

################################
# Pedido
################################

def exportar_pedido(igrupo, ipedido, incidencias, columnas=None, desglosar_por_grupos=True):
    '''Exporta las cantidades de cada producto para un producto y un grupo

    incidencias puede ser:
        - 1: cantidad_recibida
        - 2: cantidad_red
        - 3: peticion
        - 4: cantidad (repartida)

    Si desglosar_por_grupos=False, no se separa la cantidad de cada grupo en un pedido de red
    No tiene efecto en un pedido propio

    Devuelve:
        - un objeto Rows con la cantidad de cada producto
        - una lista de columnas

    Si igrupo es el índice de una red, el resultado se agrupa para las redes del nivel inferior
    '''
    db = current.db
    grupo   = db.grupo(igrupo)
    tig = db.item_grupo

    tipo = tipo_pedido(ipedido, igrupo)
    desglosar_por_grupos = desglosar_por_grupos and (tipo==TiposPedido.RED or tipo==TiposPedido.SUBRED)

    if incidencias=='1':
        campo_cantidad = tig.cantidad_recibida
        campo_precio = tig.precio_recibido
    elif incidencias=='2':
        campo_cantidad = tig.cantidad_red
        campo_precio = tig.precio_red
    elif incidencias=='3':
        campo_cantidad = tig.cantidad_pedida
        campo_precio = tig.precio_pedido
    else: # if incidencias=='4':
        campo_cantidad = tig.cantidad
        campo_precio = tig.precio_total

    fields  = [db.productoXpedido.nombre, db.productoXpedido.descripcion, db.productoXpedido.categoria]
    q = ((tig.productoXpedido==db.productoXpedido.id) &
         (db.productoXpedido.pedido==ipedido) &
         (campo_cantidad>0))

    # En pedidos de tipo RED o SUBRED agrupamos el pedido en el nivel -1
    # y mostramos una columna para indicar qué grupo ha pedido
    if desglosar_por_grupos:
        q &= ( (tig.grupo==db.grupo.id) &
               (db.grupoXred.grupo==db.grupo.id) &
               (db.grupoXred.red==igrupo) &
               (db.grupoXred.activo==True))
        fields.append(db.grupo.nombre)
    else: # if tipo==TiposPedido.COORDINADO or tipo==TiposPedido.PROPIO:
        # si igrupo no es una red, simplemente tomamos los item_grupo de ese igrupo
        q &= (tig.grupo==igrupo)

    # Elegimos el campo adecuado para mostrar el precio en cada caso: puede estar en
    # productoXpedido o mascara_producto
    if desglosar_por_grupos:
        if incidencias in ('3', '4'):
            fields.append(db.productoXpedido.precio_final)
        else:
            fields.append(db.productoXpedido.precio_productor)
    elif tipo==TiposPedido.COORDINADO or tipo==TiposPedido.SUBRED:
        # En pedidos que bajan de una red, no usamos productoXpedido.precio_final
        # sino mascara_producto.precio, el precio que pone el grupo al producto
        # y si no hay mascara_producto, no aparece
        q &= ((db.mascara_producto.grupo==igrupo) &
              (db.mascara_producto.productoXpedido==db.productoXpedido.id))
        if incidencias in ('3', '4'):
            fields.append(db.mascara_producto.precio)
        else:
            fields.append(db.mascara_producto.precio_red)

    fields.append(campo_cantidad.sum())

    if columnas:
        columnas_extra = db((db.columna_extra_pedido.pedido==ipedido) &
                            (db.columna_extra_pedido.nombre.belongs(columnas))) \
                        .select(db.columna_extra_pedido.ALL)
    else:
        columnas_extra = db((db.columna_extra_pedido.pedido==ipedido)) \
                        .select(db.columna_extra_pedido.ALL)
        columnas = [row.nombre for row in columnas_extra]

    if columnas:
        aliases = _alias_columnas_extra(db, columnas_extra)
        left_join = [calias.on((calias.productoXpedido==db.productoXpedido.id) &
                               (calias.columna==cid))
                    for cid, calias in aliases] or None
        dcolumnas_extra = dict(aliases)
        dcolumnas = dict((row.nombre, row.id) for row in columnas_extra)
        fields_extra = [
            dcolumnas_extra[dcolumnas[columna]].valor if columna in dcolumnas else columna
            for columna in columnas]
        #all_fields se usa para la query a la base de datos
        #columnas_exportar son las columnas que queremos que aparezcan, que pueden corresponder a
        #una columna extra de este pedido, o no, y en ese caso, las dejaremos en blanco
        all_fields = fields + [calias.valor for cid, calias in aliases]
        columnas_exportar = fields + fields_extra
    else:
        all_fields = columnas_exportar = fields
        left_join=None

    if desglosar_por_grupos:
        results = db(q).select(*all_fields,
                               groupby=tig.grupo|db.productoXpedido.producto,
                               orderby=tig.grupo|db.productoXpedido.producto,
                               left=left_join)
    else:
        results = db(q).select(*all_fields, groupby=db.productoXpedido.producto,
                               left=left_join)
    return results, columnas_exportar

def exportar_pedidos(
        igrupo, 
        pedidos, 
        incidencias, 
        desglosar_por_grupos=True):
    db = current.db
    ls = []
    #recopilamos las columnas_extra que aparecen en cualquiera de los pedidos,
    #usamos esa lista de columnas para todos los pedidos, tanto si la columna
    #está presente en ese pedido como si no
    columnas_exportar = sorted([row.nombre for row in
        db(db.columna_extra_pedido.pedido.belongs(pedidos)) \
        .select(db.columna_extra_pedido.nombre,
                groupby=db.columna_extra_pedido.nombre)])

    for ipedido in pedidos:
        ls.append((represent(db.productor, db.pedido(ipedido).productor),))
        results, fields = exportar_pedido(
                igrupo, ipedido, incidencias, columnas_exportar, desglosar_por_grupos=desglosar_por_grupos
        )
        ls.extend(pretty_results(results, fields))
        ls.append([])
    return ls

def csv_exportar_pedido(igrupo, ipedido, incidencias):
    results, fields = exportar_pedido(igrupo, ipedido, incidencias)
    return pretty_csv(results, fields)

def csv_exportar_pedidos(igrupo, ipedidos, incidencias):
    from .csv_utils import csv_dump_sheet
    ls = exportar_pedidos(igrupo, ipedidos, incidencias)
    return csv_dump_sheet(ls)

def ods_exportar_pedido(igrupo, ipedido, incidencias, nombre_fichero):
    results, fields = exportar_pedido(igrupo, ipedido, incidencias)
    pretty_ods(results, fields, nombre_fichero)

def ods_exportar_pedidos(igrupo, ipedidos, incidencias, nombre_fichero):
    from .ods_utils import ods_write_sheet
    ls = exportar_pedidos(igrupo, ipedidos, incidencias)
    ods_write_sheet(ls, nombre_fichero)

################################
# Productos de un productor
################################

def csv_exportar_productos(productor):
    from .modelos.pedidos import pedido_fantasma
    db = current.db
    tppp = db.productoXpedido
    t_colep = db.columna_extra_pedido
    t_campoep = db.campo_extra_pedido
    iproductor = productor.id
    ipedidof   = pedido_fantasma(iproductor).id

    fields  = [tppp.nombre, tppp.precio_base, tppp.precio_final, tppp.precio_productor,
               tppp.descripcion, tppp.granel,
               tppp.destacado, tppp.temporada,
               db.categorias_productos.codigo]
    q = ((tppp.pedido==ipedidof) &
         (tppp.activo==True) &
         (tppp.categoria==db.categorias_productos.id))

    columnas_extra = db(t_colep.pedido==ipedidof) \
                    .select(t_colep.id, t_colep.tipo,
                            t_colep.nombre)
    campos_extra = db(db.campo_extra_pedido.pedido==ipedidof) \
                    .select(t_campoep.valor,
                            t_campoep.nombre)
    if columnas_extra:
        aliases = _alias_columnas_extra(db, columnas_extra)
        q &= reduce(lambda q1,q2:q1&q2,
                    [((calias.producto==db.productoXpedido.producto) &
                      (calias.columna==cid))
                     for cid, calias in aliases] )
        fields.extend(calias.valor for cid, calias in aliases)

    results = db(q).select(*fields, groupby=db.productoXpedido.producto)
    csv ='''PRODUCTOR
%(productor)s

CAMPOS EXTRA
%(campos_extra)s

COLUMNAS EXTRA
%(columnas_extra)s

PRODUCTOS
%(productos)s
'''%dict(productor      = pretty_csv_record(db.productor, productor, locale='en_US'),
         campos_extra   = pretty_csv(campos_extra,
                                     [t_campoep.nombre, t_campoep.valor],
                                     locale='en_US'),
         columnas_extra = pretty_csv(columnas_extra,
                                     [t_colep.nombre, t_colep.tipo],
                                     locale='en_US'),
         productos      = pretty_csv(results, fields, locale='en_US'))
    return csv

#####################################
# Exportar oferta de un grupo o red
#(por ahora solo una red, porque no exporta los productores coordinados)
#####################################

def ods_exportar_oferta(igrupo, nombre_fichero):
    from .ods_utils import ods_write
    from .modelos.pedidos import pedido_fantasma
    db = current.db
    tppp = db.productoXpedido
    tcep = db.columna_extra_pedido
    T  = current.T
    grupo = db.grupo[igrupo]

    oferta = {}
    #No hago una sola query para todos los productos porque las columnas extra
    #pueden variar de un productor a otro...
    for row in db((db.productor.grupo==igrupo) &
                  (db.productor.activo==True) &
                  (db.pedido.productor==db.productor.id) &
                  (db.pedido.fecha_reparto==current.C.FECHA_FANTASMA))\
               .select(db.productor.id, db.productor.nombre, db.pedido.id):
        iproductor = row.productor.id
        nproductor = row.productor.nombre
        ipedidof   = row.pedido.id
        fields  = [tppp.nombre,
                   tppp.precio_final,
                   tppp.descripcion,
                   tppp.temporada,
                   db.categorias_productos.nombre
                  ]
        q = ((db.producto.productor==iproductor) &
             (tppp.producto==db.producto.id) &
             (tppp.pedido==db.pedido.id) &
             (db.pedido.fecha_reparto==current.C.FECHA_FANTASMA) &
             (tppp.activo==True) &
             (tppp.categoria==db.categorias_productos.id)
            )
        cabeceras = [T('Producto'), T('Categoría'), T('Descripción'), T('precio'), T('En temporada')]

        columnas_extra = db(tcep.pedido==ipedidof) \
                        .select(tcep.id, tcep.tipo, tcep.nombre)
        if columnas_extra:
            aliases = _alias_columnas_extra(db, columnas_extra)
            q &= reduce(lambda q1,q2:q1&q2,
                        [((calias.productoXpedido==tppp.id) &
                          (calias.columna==cid))
                         for cid, calias in aliases] )
            fields.extend(calias.valor for cid, calias in aliases)
            cabeceras.extend(row.nombre for row in columnas_extra)
        else:
            aliases = []

        results = db(q).select(
            *fields,
            orderby=db.categorias_productos.nombre|tppp.nombre
        )
        ls_productor = [cabeceras]
        ls_productor.extend(
            [row.productoXpedido.nombre,
             row.categorias_productos.nombre,
             row.productoXpedido.descripcion,
             row.productoXpedido.precio_final,
             row.productoXpedido.temporada] +
            [row[calias.valor] for cid, calias in aliases]
            for row in results
        )
        oferta[nproductor] = ls_productor

    ods_write(oferta, nombre_fichero)
    return

################################
# Tabla para el reparto
################################

def tabla_reparto(igrupo, ipedidos, coordinados=False, locale='en_US'):
    '''Prepara la tabla para el reparto como una lista de filas, cada fila es una lista de celdas

    Cada celda puede ser Formula, MatrixFormula, Numero... o un tipo básico python

    ATENCIÓN: Cambios en esta funcion deben ir acompañados de cambios en
    importa_tabla_reparto_fecha, que importa las tablas modificadas durante el reparto
    '''
    db = current.db
    T  = current.T
    tppp = db.productoXpedido
    grupo   = db.grupo(igrupo)

    q1 = ((db.item.grupo==igrupo) &
          (db.item.productoXpedido==tppp.id) &
          (tppp.pedido.belongs(ipedidos)) )
    q3 = ((db.item_grupo.grupo==igrupo) &
          (db.item_grupo.productoXpedido==tppp.id) &
          (tppp.pedido.belongs(ipedidos)) )
    if coordinados:
        q_extra = ((db.mascara_producto.productoXpedido == tppp.id) &
                   (db.mascara_producto.grupo==igrupo))
        q1, q3 = q1&q_extra, q3&q_extra
        field_precio = db.mascara_producto.precio
    else:
        field_precio = tppp.precio_final
    fields1 = [tppp.producto, tppp.pedido, db.item.cantidad, db.item.unidad]
    res1 = db(q1).select(*fields1)
    unidades_en_pedido = set(row.item.unidad for row in res1)
    fields3 = [tppp.producto, tppp.pedido, db.item_grupo.cantidad, field_precio]
    res3 = db(q3).select(*fields3)

    prods = set(row.productoXpedido.producto for row in chain(res1, res3))
    nombres_prods = [(row.producto, row.nombre, row.pesar)
                     for row in db((tppp.producto.belongs(prods) &
                                   (tppp.pedido.belongs(ipedidos)) &
                                   (tppp.categoria==db.categorias_productos.id)))\
                        .select(tppp.producto, tppp.nombre, tppp.pesar,
                                orderby=db.categorias_productos.nombre|tppp.nombre) ]
    prods_d = dict((k,j) for j,(k,_,_) in enumerate(nombres_prods))

    precios_d = dict((row.productoXpedido.producto, row[field_precio])
                     for row in chain(res3))

    nunidades_en_grupo  = db((db.personaXgrupo.grupo==igrupo) &
                             (db.personaXgrupo.activo==True) ) \
                          .count(db.personaXgrupo.unidad)
    unidades_en_pedido.update(row.unidad
        for row in db( db.coste_pedido.pedido.belongs(ipedidos) &
                      (db.coste_pedido.grupo==igrupo) &
                      (db.coste_pedido.coste>0))
                   .select(db.coste_pedido.unidad))

    unidades   = list(unidades_en_pedido)
    unidades.sort()
    unidades_d = dict((unidad,k) for k,unidad in enumerate(unidades))

    productores = db( db.pedido.id.belongs(ipedidos) &
                     (db.pedido.productor==db.productor.id)) \
                  .select(db.productor.nombre, db.pedido.id,
                          orderby=db.productor.nombre)
    nombres_productores_d = dict((row.pedido.id,row.productor.nombre)
                         for row in productores)

    items = dict((row.pedido.id,
                  [[Decimal()]*len(unidades) for _ in range(len(prods))])
                 for row in productores)
    #Primero rellenamos con las peticiones, después item puede sobreescribir las
    #cantidades pedidas
    for row in res1:
        items[row.productoXpedido.pedido] \
             [prods_d[row.productoXpedido.producto]] \
             [unidades_d[row.item.unidad]] = row.item.cantidad

    items_grupo = dict((row.pedido.id, [Decimal() for _ in range(len(prods))])
                       for row in productores)
    for row in res3:
        items_grupo[row.productoXpedido.pedido] \
                   [prods_d[row.productoXpedido.producto]] = row.item_grupo.cantidad

    ls = []
    fila_base = 0
    cota_sup_num_filas = (db(tppp.pedido.belongs(list(items.keys()))).count() +
                          db(db.campo_extra_pedido.pedido.belongs(list(items.keys()))).count() +
                          len(items)*8)
    primera_col_unidad = 4
    ultima_col_unidad  = primera_col_unidad + len(unidades) - 1
    for ipedido, items_pedido in items.items():
        pedido  = db.pedido(ipedido)
        items_grupo_pedido = items_grupo[ipedido]
        if pedido.productor.grupo == igrupo:
            formula = pedido.formula_coste_extra_pedido
            coste_pedido_red = None
        else:
            proxy_pedido = db.proxy_pedido(pedido=ipedido, grupo=igrupo)
            formula      = proxy_pedido.formula_coste_extra_pedido
            coste_pedido_red = proxy_pedido.coste_red

        columnas_extra = db(db.columna_extra_pedido.pedido==ipedido)\
                         .select(db.columna_extra_pedido.ALL,
                                 orderby=db.columna_extra_pedido.nombre)
        tipo_columna = {'decimal(9,2)':Numero,
                        'string': str,
                        'boolean':bool
                        }
        tablas_columnas_extra = [
            (row.id, db['valor_extra_%s_pedido'%(row.tipo.split('(')[0])])
            for row in columnas_extra]
        valores_columnas_extra = dict((
            cid,
            defaultdict((lambda:t.valor.default),
                 ((row2.producto, row2.valor)
                 for row2 in db((t.columna==cid))
                             .select(t.valor, t.producto)) ))
            for cid, t in tablas_columnas_extra)
        ps = [[''], [nombres_productores_d[ipedido]]]
        ps.extend((row.nombre, Cantidad(row.valor))
            for row in db(db.campo_extra_pedido.pedido==ipedido)
                       .select(db.campo_extra_pedido.ALL) )
        if coste_pedido_red!=None:
            ps.append(['COSTE_PEDIDO_RED', Precio(coste_pedido_red)])

        ps.append([T('PRODUCTO'),T('PRECIO UNITARIO'),T('TOTAL'),T('TOTAL REPARTIDO')] +
              [T('Ud:')+'%d'%unidad for unidad in unidades]+
              [row.nombre for row in columnas_extra])

        parcial_fila_base = len(ps)
        fila_base += parcial_fila_base
        csv_row = 0
        for  prod_row, (pid, pname, pesar), item_grupo in \
            zip(items_pedido, nombres_prods, items_grupo_pedido):
            if any(prod_row):
                ps.append(
                  [('*%s*' if pesar else '%s')%pname,
                   Precio(precios_d[pid]),
                   Cantidad(item_grupo),
                   Formula('SUM(%s:%s)'%(
                           ref(csv_row+fila_base,primera_col_unidad),
                           ref(csv_row+fila_base,ultima_col_unidad)
                           ))
                  ] +
                  [Cantidad(c) for csv_col,c in enumerate(prod_row)] +
                  [tipo_columna[row.tipo]( valores_columnas_extra[row.id][pid])
                    for row in columnas_extra] )
                csv_row += 1

        #Totales por unidad
        if csv_row:
            ps.append([T('TOTAL UNIDAD'), '','','',] +
                   [Formula('SUMPRODUCT(%s:%s;%s:%s)'%(ref(fila_base, 1, dolar=ref.COL),
                                                 ref(csv_row+fila_base-1, 1, dolar=ref.COL),
                                                 ref(fila_base, primera_col_unidad+j),
                                                 ref(csv_row+fila_base-1, primera_col_unidad+j)))
                    for j in range(len(unidades)) ]
                )
        else:
            ps.append( [T('TOTAL UNIDAD'), '','','',] +
                       ['0' for j in range(len(unidades)) ] )

        #coste extra del pedido
        from .evalua_coste_extra import TraductorFormula, CostesExtraException
        traductor = TraductorFormula(formula,
                                     fila_base, csv_row+fila_base-1,
                                     csv_row+fila_base+2, 2,
                                     fila_base-parcial_fila_base +2,fila_base-2,
                                     dict((row.nombre.upper(), ultima_col_unidad+j+1)
                                          for j,row in enumerate(columnas_extra)),
                                     unidades,
                                     cota_sup_num_filas,
                                     locale=locale,
                                     UNIDADES_EN_GRUPO=nunidades_en_grupo)
        try:
            ps.append([T('COSTE EXTRA DEL PEDIDO'), 1, formula, ''] +
                      [Formula(traductor.formula_traducida(unidad)) for unidad in unidades])
        except Exception as e:
            if isinstance(e, (CostesExtraException, SyntaxError, ZeroDivisionError, TypeError)):
                ps.append( [T('COSTE EXTRA DEL PEDIDO'), 1, 'ERROR EN LA FORMULA: "%s"'%formula, ''] +
                           ['ERROR']*len(unidades) )
            else:
                raise e

        if csv_row:
            ps.append([T('TOTAL PRODUCTOS'),'',
                       Formula('SUMPRODUCT(%s:%s;%s:%s)'%(
                         ref(fila_base,1),ref(csv_row+fila_base-1,1),
                         ref(fila_base,3),ref(csv_row+fila_base-1,3) )
                      ) ])
            ps.append([T('TOTAL PEDIDO'), '',
                       Formula('ROUND(%s + SUM(%s:%s);2)'%(
                        ref(csv_row+fila_base+2,2),
                        ref(csv_row+fila_base+1,primera_col_unidad),
                        ref(csv_row+fila_base+1,ultima_col_unidad) )
                    ) ])
        else:
            ps.append([T('TOTALES'), '', 0] )
            ps.append([''])
        ls.extend(ps)
        fila_base += len(ps) - parcial_fila_base

    nfilas = len(ls)
    ls.extend([[''],
                #Total de los productos de cada unidad para todos los pedidos
               [T('GRAN TOTAL PRODUCTOS'), '','',
                Formula('SUM(%s:%s)'%(ref(nfilas+1, primera_col_unidad),
                                      ref(nfilas+1,ultima_col_unidad)))] +
               [Formula('ROUND(SUMIF(%s:%s;"%s";%s:%s);2)'%(
                    ref(2, 0, dolar=ref.ROWCOL),
                    ref(cota_sup_num_filas, 0, dolar=ref.ROWCOL),
                    T('TOTAL UNIDAD'),
                    ref(2, primera_col_unidad + j, dolar=ref.ROW),
                    ref(cota_sup_num_filas, primera_col_unidad + j, dolar=ref.ROW)
                )) for j in range(len(unidades))
               ],
                #Unidades que han pedido algun producto
               [T('UNIDADES QUE PARTICIPAN'),
                Formula('COUNTIF(%s:%s;">0")'%(
                    ref(nfilas+1, primera_col_unidad), ref(nfilas+1, ultima_col_unidad)
                ))],
                #Totales por unidad de todos los pedidos
               [T('GRAN TOTAL'), '','',
                Formula('SUM(%s:%s)'%(ref(nfilas+3, primera_col_unidad), ref(nfilas+3,ultima_col_unidad)))] +
               [Formula('ROUND(SUMPRODUCT(%s:%s;%s:%s);2)'%(
                    ref(2, 1, dolar=ref.COL),ref(fila_base-2, 1, dolar=ref.COL),
                    ref(2, primera_col_unidad + j), ref(fila_base-2, primera_col_unidad + j)
                 )) for j in range(len(unidades))
               ]
            ])
    return ls

def csv_exportar_tabla_reparto(igrupo, ipedidos, coordinados=False):
    '''Exporta en una hoja la cantidad de item a repartir por unidad,
    para un conjunto de pedidos del mismo tipo:
    - Pedidos propios del mismo grupo
    - Pedidos coordinados del mismo grupo, lanzados desde la misma red
    '''
    from .csv_utils import csv_dump_sheet
    data = tabla_reparto(igrupo, ipedidos, coordinados=coordinados, locale=current.T.accepted_language)
    return csv_dump_sheet(data)

def exportar_reparto_ods(igrupo, fecha_reparto, nombre_fichero):
    from .ods_utils import ods_write
    db = current.db
    grupo = db.grupo(igrupo)
    ipedidosp, pedidos_por_redes = pedidos_del_dia_por_redes(fecha_reparto, igrupo)
    dpedidos = {grupo.nombre: tabla_reparto(igrupo, ipedidosp)}
    for nombre_red, ipedidos in pedidos_por_redes.items():
        dpedidos[nombre_red] = tabla_reparto(igrupo, ipedidos, coordinados=True)
    ods_write(dpedidos, nombre_fichero)

def exportar_pedido_ods(igrupo, ipedido, nombre_fichero):
    from .ods_utils import ods_write
    from .modelos.pedidos import tipo_pedido, TiposPedido
    db = current.db
    tp = tipo_pedido(ipedido, igrupo)
    if tp in (TiposPedido.RED, TiposPedido.SUBRED):
        raise AttributeError('exportar_pedido_ods es solo para grupos')
    #Puede ser pedido propio o coordinado
    nombre_grupo_o_red = db.pedido(ipedido).productor.grupo.nombre
    dpedidos = {nombre_grupo_o_red:
        tabla_reparto(igrupo, [ipedido], coordinados=(tp==TiposPedido.COORDINADO))
    }
    ods_write(dpedidos, nombre_fichero)

#######################################################
### Tabla con desajustes para pedidos desde una red ###
#######################################################

def tabla_reparto_redes(ipedido):
    from .modelos.grupos import nodos_en_la_red

    db = current.db

    ired  = db.pedido(ipedido).productor.grupo
    red   = db.grupo(ired)
    assert red.red

    #cisam: filtrar solo los grupos que han aceptado el pedido
    # solo los del nivel inferior
    ngrupos = nodos_en_la_red(ired,[ipedido])
    def data(tabla):
        cantidad_total = tabla.cantidad.sum()
        precio_red   = tabla.precio_red.sum()
        rows = db((tabla.grupo==db.grupo.id) &
                   db.grupo.id.belongs(list(ngrupos.keys())) &
                  (tabla.productoXpedido==db.productoXpedido.id) &
                  (db.productoXpedido.pedido==ipedido))\
               .select(db.productoXpedido.producto,
                       db.productoXpedido.pesar,
                       cantidad_total, precio_red,
                       db.grupo.id,
                       groupby=db.grupo.id|db.productoXpedido.id)
        return (rows,
                dict(((row.grupo.id, row.productoXpedido.producto),
                      (row[cantidad_total], row.productoXpedido.pesar)) for row in rows))

    rows_itemsred, prods_itemsred = data(db.item_red)
    rows_items, prods_items = data(db.item_grupo)
    rows_pets, prods_pets   = data(db.peticion)
    #prods = set(rid for (gid, rid),(q,pesar) in prods_items.iteritems()
    #             if (q!=prods_pets.get((gid,rid), (Decimal(),))[0] or pesar))
    #prods.update(rid for (gid, rid), (q, _) in prods_items.iteritems()
    #                 if (q!=prods_pets.get((gid,rid), (Decimal(),))[0] or pesar))
    prods = set(rid for (gid, rid),(q,pesar) in prods_itemsred.items()
                 if (q!=prods_pets.get((gid,rid), (Decimal(),))[0] or pesar))
    prods.update(rid for (gid, rid), (q, pesar) in prods_items.items()
                     if (q!=prods_itemsred.get((gid,rid), (Decimal(),))[0] or pesar))
    prods.update(rid for (gid, rid), (q, _) in prods_pets.items()
                     if (q>0 and ((gid,rid) not in prods_itemsred)))

    nombres_prods = [(row.producto, row.nombre + ('!' if row.pesar else ''))
                     for row in db((db.productoXpedido.producto.belongs(prods) &
                                   (db.productoXpedido.pedido==ipedido) &
                                   (db.productoXpedido.categoria==db.categorias_productos.id)))\
                        .select(db.productoXpedido.producto,db.productoXpedido.nombre,
                                db.productoXpedido.pesar,
                                orderby=db.categorias_productos.nombre|db.productoXpedido.nombre) ]

    precios_prods = dict((row.producto, row.precio_final)
                     for row in db(db.productoXpedido.producto.belongs(prods) &
                                   (db.productoXpedido.pedido==ipedido))\
                        .select(db.productoXpedido.producto, db.productoXpedido.precio_final) )
    prods_d = dict((k,j) for j,(k,_) in enumerate(nombres_prods))

    grupos = list(ngrupos.items())
    grupos_d = dict((k,j) for j,(k,_) in enumerate(grupos))

    peticiones = [[Decimal()]*len(grupos_d) for _ in range(len(prods))]
    peticiones_precios = [[Decimal()]*len(grupos_d) for _ in range(len(prods))]
    for row in rows_pets:
        pppid = row.productoXpedido.producto
        if pppid in prods:
            peticiones[prods_d [pppid]]\
                      [grupos_d[row.grupo.id]] = row[db.peticion.cantidad.sum()] or Decimal()
            peticiones_precios[prods_d [row.productoXpedido.producto]]\
                              [grupos_d[row.grupo.id]] = row[db.peticion.precio_red.sum()] or Decimal()
    items = [[Decimal()]*len(grupos_d) for _ in range(len(prods))]
    items_precios = [[Decimal()]*len(grupos_d) for _ in range(len(prods))]
    for row in rows_items:
        pppid = row.productoXpedido.producto
        if pppid in prods:
            items[prods_d [pppid]]\
                 [grupos_d[row.grupo.id]] = row[db.item_grupo.cantidad.sum()] or Decimal()
            items_precios[prods_d [pppid]]\
                         [grupos_d[row.grupo.id]] = row[db.item_grupo.precio_red.sum()] or Decimal()

    itemsred = [[Decimal()]*len(grupos_d) for _ in range(len(prods))]
    itemsred_precios = [[Decimal()]*len(grupos_d) for _ in range(len(prods))]
    for row in rows_itemsred:
        pppid = row.productoXpedido.producto
        if pppid in prods:
            itemsred[prods_d [pppid]]\
                 [grupos_d[row.grupo.id]] = row[db.item_red.cantidad.sum()] or Decimal()
            itemsred_precios[prods_d [pppid]]\
                         [grupos_d[row.grupo.id]] = row[db.item_red.precio_red.sum()] or Decimal()


    ls = [['', ''] + sum(([grupo, '','','',''] for _,grupo in grupos), [])]
    ls.append(['', ''] + sum((["Pedido","Recibido","Suministrado","Desajuste","Discrepancias"] for _ in grupos), []))
    for pets_row,items_row,itemsred_row,pets_precios_row,items_precios_row,itemsred_precios_row,(pid, pname) in zip(peticiones, items, itemsred, peticiones_precios, items_precios, itemsred_precios, nombres_prods):
        ls.append([pname,
                   Precio(precios_prods[pid])] +
                   sum(([Cantidad(c1), Cantidad(c2), Cantidad(c3), Precio((p1-p3)), Precio((p2-p3))] for c1,c2,c3,p1,p2,p3 in
                   zip(pets_row,items_row,itemsred_row,pets_precios_row,items_precios_row, itemsred_precios_row)), []))
    return ls

def exportar_pedidos_redes(pedidos):
    db = current.db
    ls = []
    for ipedido in pedidos:
        ls.append((represent(db.productor, db.pedido(ipedido).productor),))
        ls.extend(tabla_reparto_redes(ipedido))
        ls.append([])
    return ls

def csv_exportar_pedido_redes(ipedido):
    from .csv_utils import csv_dump_sheet
    data = tabla_reparto_redes(ipedido)
    return csv_dump_sheet(data)

def csv_exportar_pedidos_redes(ipedidos):
    from .csv_utils import csv_dump_sheet
    ls = exportar_pedidos_redes(ipedidos)
    return csv_dump_sheet(ls)

def ods_exportar_pedido_redes(ipedido, nombre_fichero):
    from .ods_utils import ods_write_sheet
    ls = tabla_reparto_redes(ipedido)
    ods_write_sheet(ls, nombre_fichero)

def ods_exportar_pedidos_redes(ipedidos, nombre_fichero):
    from .ods_utils import ods_write_sheet
    ls = exportar_pedidos_redes(ipedidos)
    ods_write_sheet(ls, nombre_fichero)

#################################
# Exportar desglosado por grupos
#################################

def ods_exportar_pedido_por_grupos(ired, ipedidos, incidencias, nombre_fichero):
    from .ods_utils import ods_write
    from .modelos.pedidos import tipo_pedido, TiposPedido
    from .modelos.grupos import nodos_en_la_red
    db = current.db
    dpedidos = {nombre_grupo_o_red:
        exportar_pedidos(igrupo, ipedidos, incidencias, desglosar_por_grupos=False)
        for igrupo,nombre_grupo_o_red in nodos_en_la_red(ired, ipedidos).items()
    }
    ods_write(dpedidos, nombre_fichero)

