# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################
import os.path

from gluon import current

def get_version(*localpath):
    filepath = os.path.join('applications', current.request.application, *localpath)
    f = open(filepath, mode='r')
    s = f.read().strip()
    f.close()
    return s,tuple(int(n) for n in s.split('.'))

def less(t1, t2):
    for v1,v2 in zip(t1,t2):
        if v1 > v2:
            return False
        if v1 < v2:
            return True
    # Si todos los números son iguales, la versión t1 no es menor que la versión t2
    return False
