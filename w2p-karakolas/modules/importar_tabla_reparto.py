# -*- coding: utf-8 -*-

# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################

"""
Importa las mismas tablas para el reparto que exportamos en modules.exportador
"""
import pyexcel
import os
from collections import defaultdict
# import ast
# from math import floor
from decimal import Decimal#, InvalidOperation
# from itertools import chain

from gluon import current

from .modelos.pedidos import pedidos_del_dia_por_redes
# from rsheet import _update_or_insert
# from kutils import ascii_safe
# from modelos.productores import categorias
# from utils.db import my_bulk_insert

PRECISION = Decimal('0.01')

def importa_tabla_reparto_fecha(igrupo, fecha_reparto, tabla):
    '''
    '''
    db = current.db
    tppp = db.productoXpedido
    nombre_grupo = db.grupo(igrupo).nombre

    filename = tabla.filename
    _, ext = os.path.splitext(filename)
    if ext not in ('.ods', '.xlsx', '.xls'):
        raise NotImplementedError(current.T('Este tipo de archivo no está soportado'))

    sheets = pyexcel.get_book_dict(file_content=tabla.file.read(), file_type=ext[1:])

    ipedidosp, pedidos_por_redes = pedidos_del_dia_por_redes(fecha_reparto, igrupo)
    pedidos_por_redes[nombre_grupo] = ipedidosp

    #Una lista de mensajes con errores como nombres no identificados
    errores = []
    #Un dict que a cada ipedido le asigna un dict (productoXpedido,unidad) -> cantidad
    incidencias = {}
    for ngrupo_o_red, tabla in sheets.items():
        ipedidos = pedidos_por_redes.get(ngrupo_o_red, [])
        dproductores = {}
        for rpedido in db( db.pedido.id.belongs(ipedidos) &
                      (db.pedido.productor==db.productor.id)
            ).select(db.pedido.id, db.productor.nombre):
            ipedido = rpedido.pedido.id
            dproductos = dict((row.nombre, row.id) for row in
                db((tppp.pedido==ipedido)
    # a estas alturas, si hay petición de un producto fuera de temporada, mejor procesarla
    #  y si todo ha ido bien y no se ha pedido nada de los productos fuera de temporada, es tontería
    #  engordar la query
#                   (tppp.temporada==True) &
#                   (tppp.activo==True) &
                ).select(db.productoXpedido.nombre, db.productoXpedido.id)
            )
            dproductores[rpedido.productor.nombre] = ipedido, dproductos

        if ipedidos:
            coordinados = (nombre_grupo != ngrupo_o_red)
            errores_red, unidades_red, incidencias_red, pedidos_sin_cambios = _importa_hoja(
                igrupo, fecha_reparto, tabla, dproductores, coordinados
            )
            errores.extend(errores_red)
            if pedidos_sin_cambios or incidencias_red:
                incidencias[ngrupo_o_red] = unidades_red, incidencias_red, pedidos_sin_cambios

    return errores, incidencias

def _importa_hoja(igrupo, fecha_reparto, tabla, dproductores, coordinados):
    from .modelos.pedidos import recoge_y_ordena_item, peticiones_por_unidades
    from .modelos.grupos import unidades_en_grupo

    ipedidos = [ipedido for ipedido,_ in list(dproductores.values())]
    errores, unidades_hoja, items, pedidos_tabla = _lee_hoja(fecha_reparto, tabla, dproductores)
    d_unidades_hoja = dict((u,j) for j,u in enumerate(unidades_hoja))
    lunidades_db, _, cantidades_db = recoge_y_ordena_item(ipedidos, igrupo)
    unidades_db = set(lunidades_db)
    #interseca con las unidades reales del grupo
    unidades_grupo = unidades_en_grupo(igrupo)
    sunidades_hoja = set(unidades_hoja)
    unidades_desconocidas = sunidades_hoja.difference(unidades_grupo)
    if unidades_desconocidas:
        errores.append(current.T(
        '''En la tabla hay columnas para las siguientes unidades que no existen en el grupo: %s'''
                      )%','.join('%d'%u for u in sorted(unidades_desconocidas)) )
    todas_unidades = list(sorted(sunidades_hoja.union(unidades_db).intersection(unidades_grupo) ))
    incidencias, pedidos_sin_cambios = [], []
    for nproductor,(ipedido, dproductos) in dproductores.items():
        if nproductor in pedidos_tabla:
            cpedido_db = cantidades_db[ipedido]
            incidencias_pedido = []
            for nproducto, ippp in dproductos.items():
                dcants_db = cpedido_db[ippp]
                lcants_db = [dcants_db.get(u, Decimal()) for u in todas_unidades]
                cants_hoja = items.get(ippp, defaultdict(Decimal))
                lcants_hoja = [Decimal(cants_hoja[d_unidades_hoja[u]] if u in d_unidades_hoja else 0)
                               for u in todas_unidades]
                if lcants_db!=lcants_hoja:
                    incidencias_pedido.append((nproducto, ippp, list(zip(lcants_db, lcants_hoja))) )
            if incidencias_pedido:
                incidencias.append( (nproductor, ipedido, incidencias_pedido))
            else:
                pedidos_sin_cambios.append(nproductor)
    return errores, todas_unidades, incidencias, pedidos_sin_cambios

def _lee_hoja(fecha_reparto, tabla, dproductores):
    db = current.db
    T  = current.T
    tppp = db.productoXpedido
    texto_unidad = T('Ud:').encode()
    texto_producto = T('PRODUCTO').encode()
    cabeceras_validas = (T('TOTAL UNIDAD').encode(),
                         T('COSTE EXTRA DEL PEDIDO').encode(),
                         T('GRAN TOTAL PRODUCTOS').encode(),
                         T('GRAN TOTAL').encode(),
                         T('PRODUCTO').encode(),
    )
    len_ud = len(T('Ud:'))
    col_primera_unidad = 4
    unidades, errores, incidencias, pedidos = [], [], [], []
    dproductos, items = {}, {}
    for k,ls in enumerate(tabla):
        primera_celda = ls[0]
        if primera_celda.startswith('*'):
            primera_celda = primera_celda[1:-1]
        #Fila productor
        if not any(cell for cell in ls[1:]) and primera_celda in dproductores:
            ipedido, dproductos = dproductores[primera_celda]
            pedidos.append(primera_celda)
        #Fila cabecera
        elif primera_celda==texto_producto and not unidades:
            try:
                for k in range(col_primera_unidad, len(ls)):
                    cell = ls[k]
                    if not cell.startswith(texto_unidad):
                        k -= 1
                        break
                    unidades.append(int(cell[len_ud:]))
            except ValueError:
                errores.append(T('No se pudo leer la lista de unidades en la fila %d'%(k+1)))
            nunidades = k - 2
        #Fila producto
        elif primera_celda in dproductos:
            ippp = dproductos[primera_celda]
            items[ippp] = [Decimal(q or 0).quantize(PRECISION) for q in
                           ls[col_primera_unidad:nunidades+col_primera_unidad-1]]
        #Identifica las otras posibles filas válidas
        elif ((primera_celda in cabeceras_validas) or
              all((not cell) for cell in ls[col_primera_unidad:])):
            pass
        #anota un error si una fila no encaja
        else:
            errores.append(T('No hemos entendido una fila con un posible producto: ')+primera_celda)

    return errores, unidades, items, pedidos

def escribe_incidencias(igrupo, incidencias):
    # from utils.db import my_bulk_insert
    db = current.db
    rs = []
    for unidades_red, incidencias_red, _ in list(incidencias.values()):
        for _,ipedido,incidencias_pedido in incidencias_red:
            #No tocamos los db.item de los productos que no estan en incidencias
            ippps = [ippp for _,ippp,_  in incidencias_pedido]
            db(db.item.productoXpedido.belongs(ippps) & (db.item.grupo==igrupo)).delete()
            for _,ippp,lcants  in incidencias_pedido:
                rs.extend(dict(grupo=igrupo, unidad=u, productoXpedido=ippp, cantidad=q)
                          for u,(_,q) in zip(unidades_red, lcants))

    db.item.bulk_insert(rs)
