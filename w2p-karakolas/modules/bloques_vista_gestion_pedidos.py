# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

from collections import defaultdict
from random import randint
from datetime import date

from gluon.html import URL, A, IMG, SPAN, TAG, DIV, CAT, H3, H4, H5, P, EM, TABLE, THEAD, TBODY, TR, TH, TD, UL, LI, INPUT, FORM, SCRIPT, BUTTON, XML
from gluon import current

from .kutils import format_fecha
from .modelos.pedidos import EstadosPedido, str_estados_pedido, str_discrepancias
from .modelos.grupos import grupos_y_subredes_de_la_red

class PrinterPedidos(object):
    def __init__(self, pedidos, nombres_pedidos, via_pedidos, bundles, grupo, tp):
        self.pedidos = pedidos
        self.grupo = grupo
        self.igrupo = grupo.id
        self.red = grupo.red
        self.tp = tp
        self.bundles = bundles
        self.nombres_pedidos = nombres_pedidos
        self.via_pedidos = via_pedidos
        self.dEstadosPedidoPlural = str_estados_pedido(plural=True)
        self.dEstadosPedidoSingular = str_estados_pedido(plural=False)
        self.textos_discrepancias = str_discrepancias()
        self.today = date.today()

    def cabecera_fecha(self, f):
        hoy = current.request.now.date()
        return (H3(format_fecha(f)+'!!', _class='fecha_pasada')
                   if (self.tp!=EstadosPedido.HISTORICOS and f<hoy) else
                  H3(format_fecha(f)))

    def tooltips(self, tp):
        C = current.C
        T = current.T
        #Pongo el texto correspondiente a pedidos cerrados, luego matizo (si es nec) para pedidos abiertos o históricos (aunque no veo porque deberia ser distinto entre cerrados e históricos)
        textos_tooltip = {
            'activo':T('Desactivar el pedido descuelga el grupo de este pedido coordinado. Desactivar un pedido coordinado no es equivalente a cerrar un pedido propio del grupo. De hecho, desde la interfaz del grupo no se puede cerrar un pedido coordinado, sólo el admin de la red puede hacerlo.'),
            'aceptar_rechazar':T('Puedes editar los precios del pedido coordinado y/o bloquear los productos que no interesan a tu grupo.'),
            'editar':T('Aquí puedes abrir o cerrar el pedido, cambiar la fecha de reparto, y el mensaje que informa sobre cuándo se cerrará el pedido. También puedes cancelar el pedido.'),
            'editar_coordinado':T('Aquí puedes unirte a, o descolgarte de, este pedido coordinado. También puedes cambiar la fecha de reparto: la fecha en la que las integrantes del grupo de llevan el pedido a casa. Puede ser distinto de la fecha de reparto del pedido coordinado, que es la fecha en que llega el pedido al local.'),
            'incidencias': T('Anota lo que realmente se llevó cada unidad de cada producto. Esta hoja también está pensada para el día del reparto: si tienes un ordenador conectado a internet en el local, puedes resolver las incidencias según vayan surgiendo y después recalcular los informes para el cobro (o usar la hoja de cobro)'),
            'totales'    : T('Consulta la cantidad que ha pedido el grupo de cada producto. El pedido está cerrado, así que puedes enviar esta info al productor.'),
            'totales_red'    : T('Consulta la cantidad que han pedido todos los grupos de cada producto. El pedido está cerrado, así que puedes enviar esta info al productor.'),
            'grupos'  : T('Elige qué grupos se unen a este pedido. Solo puedes abrir el pedido a los grupos que han puesto al productor en "automatico". Puedes cancelar el pedido para algunos grupos o subredes aunque ya hayan pedido.'),
            'productos'  : '',
            'info': T('Consulta la información sobre este productor que habéis recopilado en tu grupo (info para pedir, recoger el pedido, pagar...).'),
            'cerrar_pedido': '',
            'incidencias_globales':T('Anota las cantidades que realmente llegaron de cada producto, a nivel de grupo.'),
            'incidencias_red':T('Anota las cantidades que realmente llegaron de cada producto, a cada uno de los grupos de la red') + T(' (formato compacto)'),
            'incidencias_desdered':T('Anota las cantidades que realmente llegaron de cada producto, a cada uno de los grupos de la red') + T(' (formato detallado)'),
            'tabla_reparto':T('Exporta una hoja de cálculo en formato OpenDocument (ods) que incluye fórmulas para calcular los totales y precios. Especialmente pensada para resolver incidencias durante el reparto, como alternativa offline a la página de incidencias.'),
        }
        if tp==EstadosPedido.ABIERTOS:
            textos_tooltip.update({
            'incidencias'  : T('Modifica lo que ha pedido cada persona de cada producto.'),
            'hay_pedido'  : T('Indica si alguna persona del grupo ha pedido algún producto de este pedido.'),
            'limpia_peticion'  : T('Elimina toda la petición del grupo hasta el momento.'),
            'totales'      : T('Consulta la cantidad que ha pedido el grupo de cada producto ...hasta el momento'),
            'totales_red'  : T('Consulta la cantidad que han pedido todos los grupos de cada producto ...hasta el momento'),
            'productos'    : T('Aquí puedes añadir o quitar productos y modificar sus características. Ten cuidado de no cambiarlos de nombre, porque si alguien ya ha pedido un producto con un nombre, se mantendrá el nombre antiguo.'),
            'cerrar_pedido': T('Después de cerrar un pedido abierto no puede pedir nadie. También te muestra un resumen del pedido y la información necesaria para hacer llegar la información al productor.')
            })
    #    elif self.tp==EstadosPedido.HISTORICOS:
    #        textos_tooltip.update({
    #        })

        return dict((k, CAT(' ', SPAN(_class='glyphicon glyphicon-question-sign', _title=v,
                             **{'_data-toggle':'tooltip', '_data-placement':'bottom'}))
                                 if v else '')
                     for (k,v) in textos_tooltip.items())

    def tabla_pedidos(self, ps, tp, pasado=False):
        C = current.C
        T = current.T
        tppp = current.db.productoXpedido
        tooltip = self.tooltips(tp)

        cabeceras = [TH(),
                     TH(T('Productor')),
                     TH(T('Info'), tooltip['info']),
                     TH(T('Editar'), tooltip['editar']),
                     TH(T('Editar los productos'), tooltip['productos']) ]
        filas = [TR(TD(SPAN(_class='glyphicon ' + (
                     'glyphicon-warning-sign icon-warning' if (tp==EstadosPedido.PENDIENTES) else
                     'glyphicon-folder-open icon-warning' if ((tp==EstadosPedido.ABIERTOS) and pasado) else
                     'glyphicon-folder-open icon-ok' if (tp==EstadosPedido.ABIERTOS) else
                     'glyphicon-folder-close icon-ok'),
                            _title=self.dEstadosPedidoSingular[tp]
                                   + (', ' + T('pasado de fecha') if pasado else ''),
                            **{'_data-toggle':'tooltip', '_data-placement':'bottom'})
                    ),
                    TD(p.productor.nombre),
                    TD(A(SPAN(_class='glyphicon glyphicon-info-sign',
                              _title=T('Info'),
                              **{'_data-toggle':'tooltip', '_data-placement':'bottom'}),
                        _href=URL(c='productores', f='ver_info_pedido.html',
                                  vars={'productor':p.pedido.productor.id}), _target='blank')),
                    TD(A(SPAN(_class='glyphicon glyphicon-pencil' +
                              (' icon-warning' if p.pedido.problemas_calculo_coste_extra else ''),
                              _title=(T('La formula del coste extra ha causado problemas al intentar cerrar el pedido.')
                              if p.pedido.problemas_calculo_coste_extra else T('Editar')),
                              **{'_data-toggle':'tooltip', '_data-placement':'bottom'}),
                        _href=URL(c='gestion_pedidos', f='edita_pedido.html',
                                  vars=dict(pedido=p.pedido.id)))),
                    TD(A(SPAN(_class='glyphicon glyphicon-list',
                              _title=T('Editar los productos'),
                              **{'_data-toggle':'tooltip', '_data-placement':'bottom'}),
                        _href=URL(c='productores', f='productos.html',
                                  vars=dict(pedido=p.pedido.id, productor=p.productor.id))))
                    )
                        for p in ps]
        if tp==EstadosPedido.ABIERTOS:
            cabeceras.insert(2,TH(T('Hay pedido'), tooltip['hay_pedido'] ) )
            if not self.red:
                cabeceras.append(TH(T('Editar peticiones'), tooltip['incidencias']) )
            cabeceras.append(TH(T('Cerrar el pedido'), tooltip['cerrar_pedido']) )
            for f,p in zip(filas,ps):
                ipedido = p.pedido.id
                f.insert(2,TD(CAT(TAG.strong(T('Si ')),
                                SPAN(_class='glyphicon glyphicon-remove',
                                     _title=T('Elimina el pedido del grupo'),
                                     _onclick='muestra_js_limpia_pedido(%d);'%ipedido,
                                     **{'_data-toggle':'tooltip', '_data-placement':'bottom'}))
                             if self.hay_pedido(ipedido) else T('No'),
                            _id='pedido_%d'%ipedido))
                if not self.red:
                    f.append(TD(A(SPAN(_class='glyphicon glyphicon-th',
                                       _title=T('Editar peticiones'),
                                       **{'_data-toggle':'tooltip', '_data-placement':'bottom'}),
                                 _href=URL(c='gestion_pedidos', f='editar_peticiones.html',
                                           vars=dict(pedido=ipedido)))))
                f.append(TD(A(SPAN(_class='glyphicon glyphicon-step-forward',
                                   _title=T('Cerrar el pedido'),
                                   **{'_data-toggle':'tooltip', '_data-placement':'bottom'}),
                             _href=URL(c='gestion_pedidos', f='cerrar_pedido.html',
                                       vars=dict(pedido=ipedido, grupo=self.igrupo)))) )
        else:
            #tanto grupos como redes usan incidencias_globales, pero para contrastar la cantidad
            # recibida con la declarada en el albarán de entrega, sea de la superred que ofrece el
            # pedido o del productor
            cabeceras.append(TH(T('Incidencias globales'), tooltip['incidencias_globales']))
            for f,p in zip(filas,ps):
                pendiente_consolidacion = p.pedido.pendiente_consolidacion
                discrepancia_productor = p.pedido.discrepancia_productor
                discrepancia_interna = p.pedido.discrepancia_interna
                discrepancia_descendiente = p.pedido.discrepancia_descendiente
                discrepancia_interna_asumida = p.pedido.discrepancia_interna_asumida
                discrepancias = (discrepancia_productor or discrepancia_descendiente or
                                 (discrepancia_interna and not discrepancia_interna_asumida))
                f.append(
                    TD(A(SPAN(_class='glyphicon glyphicon-list-alt' +
                            (' icon-warning' if
                            (pendiente_consolidacion or discrepancia_productor or
                             (discrepancia_interna and not discrepancia_interna_asumida))
                            else ''),
                              _title=T('Incidencias globales'),
                              **{'_data-toggle':'tooltip', '_data-placement':'bottom'}),
                              _href=URL(c='gestion_pedidos', f='incidencias_globales.html',
                                        vars=dict(pedido=p.pedido.id, grupo=self.igrupo))),
                       A(SPAN(_class='glyphicon glyphicon-bell icon-warning',
                              _title=T('Reportar incidencias obligatorias globalmente'),
                              **{'_data-toggle':'tooltip', '_data-placement':'bottom'}),
                              _href=URL(c='gestion_pedidos', f='incidencias_globales.html',
                                        vars=dict(obligatorias='1', pedido=p.pedido.id, grupo=self.igrupo)))
                       if pendiente_consolidacion else '' )
                )

            if self.red:
                cabeceras.append(TH(T('Incidencias'), tooltip['incidencias_red'] ))
                cabeceras.append(TH(T('Incidencias detalladas'), tooltip['incidencias_desdered'] ))
                for f,p in zip(filas,ps):
                    pendiente_consolidacion = p.pedido.pendiente_consolidacion
                    discrepancia_productor = p.pedido.discrepancia_productor
                    discrepancia_interna = p.pedido.discrepancia_interna
                    discrepancia_descendiente = p.pedido.discrepancia_descendiente
                    discrepancia_interna_asumida = p.pedido.discrepancia_interna_asumida
                    discrepancias = (discrepancia_productor or discrepancia_descendiente or
                                     (discrepancia_interna and not discrepancia_interna_asumida))
                    f.append(TD(A(SPAN(_class='glyphicon glyphicon-th'  +
                                              (' icon-warning' if (discrepancia_descendiente and
                                                                  (discrepancia_interna and
                                                                   not discrepancia_interna_asumida))
                                               else ' icon-asume-red' if (discrepancia_productor or
                                                                          pendiente_consolidacion)
                                               else ''),
                                       _title=(T('Hay discrepancias con las cantidades declaradas por algún grupo')
                                               if discrepancias else T('Incidencias')),
                                       **{'_data-toggle':'tooltip', '_data-placement':'bottom'}),
                                  _href=URL(c='gestion_pedidos', f='incidencias.html',
                                            vars=dict(pedido=p.pedido.id, grupo=self.igrupo))),
                                A(SPAN(_class='glyphicon glyphicon-bell' +
                                              (' icon-incidencias-grupo' if (discrepancia_descendiente and
                                                                             (discrepancia_interna and
                                                                              not discrepancia_interna_asumida))
                                                else ' icon-incidencias-red' if discrepancia_productor
                                                else ''),
                                       _title=T('Incidencias o productos a pesar'),
                                       **{'_data-toggle':'tooltip', '_data-placement':'bottom'}),
                                  _href=URL(c='gestion_pedidos', f='incidencias.html',
                                            vars=dict(obligatorias='1',pedido=p.pedido.id, grupo=self.igrupo)))
                                            if (discrepancias or pendiente_consolidacion) else ''
                                ) )
                    f.append(TD(A(SPAN(_class='glyphicon glyphicon-scale'  +
                                              (' icon-warning' if (discrepancia_descendiente or
                                                                   (discrepancia_interna and not
                                                                    discrepancia_interna_asumida))
                                               else ''),
                                       _title=T('Incidencias detalladas por grupo o subred'),
                                       **{'_data-toggle':'tooltip', '_data-placement':'bottom'}),
                                  _href=URL(c='redes', f='incidencias_desdered.html',
                                            vars=dict(pedido=p.pedido.id, red=self.igrupo)))
                                ) )
            else:
                cabeceras.append(TH(T('Incidencias'), tooltip['incidencias']))
                cabeceras.append(TH(T('Tabla para reparto'), tooltip['tabla_reparto']))
                for f,p in zip(filas,ps):
                    f.append(
                        TD(A(SPAN(_class='glyphicon glyphicon-th',
                                  _title=(T('Hay productos que es necesario pesar')
                                          if (pendiente_consolidacion or
                                              (discrepancia_interna and not discrepancia_interna_asumida))
                                          else T('Incidencias')),
                                  **{'_data-toggle':'tooltip', '_data-placement':'bottom'}),
                                  _href=URL(c='gestion_pedidos', f='incidencias.html',
                                            vars=dict(pedido=p.pedido.id, grupo=self.igrupo))),
                           A(SPAN(_class='glyphicon glyphicon-bell',
                                  _title=T('Productos a pesar'),
                                  **{'_data-toggle':'tooltip', '_data-placement':'bottom'}),
                                  _href=URL(c='gestion_pedidos', f='incidencias.html',
                                            vars=dict(obligatorias='1', pedido=p.pedido.id, grupo=self.igrupo)))
                            if (pendiente_consolidacion or
                                (discrepancia_interna and not discrepancia_interna_asumida))
                            else '' )
                    )
                    f.append(
                        TD(A(SPAN(_class='glyphicon glyphicon-export',
                                 _title=T('Tabla para reparto'),
                                 **{'_data-toggle':'tooltip', '_data-placement':'bottom'}),
                                 _href=URL(c='gestion_pedidos', f='exportar_tabla_reparto.ods',
                                           vars=dict(pedido=p.pedido.id, grupo=self.igrupo))) )
                    )

        if self.red:
            cabeceras.append(TH(T('Totales'), tooltip['totales_red']) )
            cabeceras.append(TH(T('Grupos'), tooltip['grupos']) )
        else:
            cabeceras.append(TH(T('Totales del grupo'), tooltip['totales']) )
        for f,p in zip(filas,ps):
            if self.red:
                discrepancia_productor = p.pedido.discrepancia_productor
                discrepancia_interna = p.pedido.discrepancia_interna
                discrepancia_descendiente = p.pedido.discrepancia_descendiente
                discrepancia_interna_asumida = p.pedido.discrepancia_interna_asumida
                discrepancia_cerrada = p.pedido.discrepancia_interna_asumida
                discrepancias = ( discrepancia_productor or discrepancia_descendiente or
                                 (discrepancia_interna and not discrepancia_interna_asumida))

                f.append( TD(A(SPAN(_class='glyphicon glyphicon-plus',
                                    _title=T('Totales'),
                                    **{'_data-toggle':'tooltip', '_data-placement':'bottom'}),
                               _href=URL(c='redes', f='vista_pedido_red.html',
                                         vars=dict(pedido=p.pedido.id, grupo=self.igrupo)))) )
                f.append( TD(A(SPAN(_class='glyphicon glyphicon-tasks',
                                    _title=T('Grupos'),
                                    **{'_data-toggle':'tooltip', '_data-placement':'bottom'}),
                               _href=URL(c='redes', f='elegir_grupos_pedido.html',
                                         vars=dict(pedido=p.pedido.id, red=self.igrupo)))) )
            else:
                f.append( TD(A(SPAN(_class='glyphicon glyphicon-plus',
                                    _title=T('Totales'),
                                    **{'_data-toggle':'tooltip', '_data-placement':'bottom'}),
                             _href=URL(c='gestion_pedidos', f='vista_pedido.html',
                                       vars=dict(pedido=p.pedido.id, grupo=self.igrupo)))) )
        return DIV(TABLE(THEAD(TR( *cabeceras )),
                         TBODY(*filas) ) if ps else '',
                   _class='pedidos table-responsive')

    def hay_pedido(self, ipedido):
        db = current.db
        subgrupos = grupos_y_subredes_de_la_red(self.igrupo)
        subgrupos.add(self.igrupo)
        return not db((db.item_grupo.cantidad_pedida>0) &
                      (db.item_grupo.grupo.belongs(subgrupos)) &
                      (db.item_grupo.productoXpedido==db.productoXpedido.id) &
                      (db.productoXpedido.pedido==ipedido)).isempty()

    def tabla_pedidos_coordinados(self, ps, tp, pasado=False):
        C = current.C
        T = current.T
        tppp = current.db.productoXpedido
        tooltip = self.tooltips(tp)

        cabeceras = [
              TH(),
              TH(T('Productor')),
              TH(T('Activo'), tooltip['activo'] ),
              TH(T('Editar'), tooltip['editar_coordinado'] ),
              # v4: ahora Aceptar/Rechazar productos es también para pedidos cerrados (con otro texto)
              #     el caso de uso es cambiar los precios a posteriori para cuadrar cuentas, como
              #     se hace con ajusta_precio_para_compensar_discrepancias
              TH((T('Aceptar/Rechazar productos') if tp==EstadosPedido.ABIERTOS else
                  T('Actualizar precios')),
                  tooltip['aceptar_rechazar'] )
        ]
        pedidos = [(p.productor.nombre, p.pedido.id, p.proxy_pedido.activo,
                    p.proxy_pedido.pendiente_consolidacion,
                    p.proxy_pedido.discrepancia_interna,
                    p.proxy_pedido.discrepancia_interna_asumida,
                    p.proxy_pedido.discrepancia_descendiente,
                    p.proxy_pedido.discrepancia_ascendiente,
                    p.proxy_pedido.problemas_calculo_coste_extra)
                    for p in ps]

        filas = [TR(
            TD(SPAN(_class='glyphicon' + (
                ' glyphicon-warning-sign' if (tp==EstadosPedido.PENDIENTES) else
                ' glyphicon-folder-close' if (tp==EstadosPedido.CERRADOS) else
                ' glyphicon-folder-open'
                ) + (' icon-warning' if (discrepancia_descendiente or
                                        (discrepancia_interna and not discrepancia_interna_asumida))
                  else ' icon-pendiente' if pendiente_consolidacion
                  else ' icon-asume-red' if discrepancia_ascendiente
                  else ' icon-asume-grupo' if discrepancia_interna_asumida
                  else ' icon-warning' if (tp==EstadosPedido.ABIERTOS and pasado)
                  else ' icon-ok'),
                _title=(
                self.dEstadosPedidoSingular[tp]
                + (', ' + T('pasado de fecha') if pasado else '')
                + ' (via %s). '%self.via_pedidos[ipedido])
                + (self.textos_discrepancias.discrepancia_descendiente if discrepancia_descendiente else '')
                + (self.textos_discrepancias.discrepancia_interna_asumida%(self.via_pedidos[ipedido])
                    if discrepancia_interna_asumida else
                   self.textos_discrepancias.discrepancia_interna%(self.via_pedidos[ipedido])
                    if discrepancia_interna else '')
                + (self.textos_discrepancias.discrepancia_ascendiente
                    %(self.via_pedidos[ipedido], self.grupo.nombre)
                    if discrepancia_ascendiente else ''),
                **{'_data-toggle':'tooltip', '_data-placement':'bottom'})),
            TD(nombre_productor),
            TD(
                SPAN(_class='glyphicon glyphicon-check' if activo else
                            'glyphicon glyphicon-unchecked',
                     _title=T('Pincha para activar o desactivar el pedido'),
                     _onclick='js_activa_pedido(%d);'%ipedido,
                     **{'_data-toggle':'tooltip', '_data-placement':'bottom'}),
                INPUT(_type='hidden', _id='pedido_activo_%d'%ipedido,
                      _value='1' if activo else '0'),
                _id = 'td_pedido_activo_%d'%ipedido
            ),
            TD(A(SPAN(_class='glyphicon glyphicon-pencil' +
                      (' icon-warning' if problemas_calculo_coste_extra else ''),
                      _title=(T('La formula del coste extra ha causado problemas al intentar cerrar el pedido.')
                              if problemas_calculo_coste_extra else T('Editar')),
                      **{'_data-toggle':'tooltip', '_data-placement':'bottom'}),
                 _href=URL(c='gestion_pedidos', f='edita_pedido_coordinado.html',
                           vars=dict(pedido=ipedido, grupo=self.igrupo)))),
            TD(A(SPAN(_class='glyphicon glyphicon-list',
                      _title=(T('Aceptar/Rechazar productos') if tp==EstadosPedido.ABIERTOS else
                              T('Actualizar precios')),
                      **{'_data-toggle':'tooltip', '_data-placement':'bottom'}),
                      _href=URL(c='gestion_pedidos', f='aceptar_productos_coordinados.html',
                                vars=dict(pedido=ipedido, grupo=self.igrupo)),
                      _disabled=(not activo)))
            )
            for (nombre_productor, ipedido, activo,
                 pendiente_consolidacion, discrepancia_interna, discrepancia_interna_asumida,
                 discrepancia_descendiente, discrepancia_ascendiente,
                 problemas_calculo_coste_extra) in pedidos
        ]

        if tp==EstadosPedido.ABIERTOS:
            cabeceras.append(TH(T('Hay pedido'), tooltip['hay_pedido'] ) )
            if not self.red:
                cabeceras.append(TH(T('Editar peticiones'), tooltip['incidencias'] ) )
            cabeceras.append(TH(T('Totales del grupo'), tooltip['totales']))
            for f,(_, ipedido, activo, _, _, _, _, _,_) in zip(filas, pedidos):
                f.append(TD(CAT(TAG.strong(T('Si ')),
                                SPAN(_class='glyphicon glyphicon-remove',
                                     _title=T('Elimina el pedido del grupo'),
                                     _onclick='muestra_js_limpia_pedido(%d);'%ipedido,
                                     **{'_data-toggle':'tooltip', '_data-placement':'bottom'}))
                             if self.hay_pedido(ipedido) else T('No'),
                            _id='pedido_%d'%ipedido))
                if not self.red:
                    f.append(TD(A(SPAN(_class='glyphicon glyphicon-th',
                                       _title=T('Editar peticiones'),
                                       **{'_data-toggle':'tooltip', '_data-placement':'bottom'}),
                                _href=URL(c='gestion_pedidos', f='editar_peticiones.html',
                                        vars=dict(pedido=ipedido, grupo=self.igrupo)))) )
                f.append( TD(A(SPAN(_class='glyphicon glyphicon-plus',
                                    _title=T('Totales'),
                                    **{'_data-toggle':'tooltip', '_data-placement':'bottom'}),
                               _href=URL(c='gestion_pedidos', f='vista_pedido.html',
                                         vars=dict(pedido=ipedido, grupo=self.igrupo)))) )
        else:
            cabeceras.append(TH(T('Incidencias globales'), tooltip['incidencias_globales']))
            cabeceras.append(TH(T('Incidencias'), tooltip['incidencias'] ) )
            if self.red:
                cabeceras.append(TH(T('Incidencias detalladas'), tooltip['incidencias_desdered'] ))
            cabeceras.append(TH(T('Tabla para reparto'), tooltip['tabla_reparto'] ) )
            cabeceras.append(TH(T('Totales del grupo'), tooltip['totales']))

            for f,(_,ipedido,_,pendiente_consolidacion,
                   discrepancia_interna, discrepancia_interna_asumida,
                   discrepancia_descendiente, discrepancia_ascendiente,
                   _) in zip(filas,pedidos):
                discrepancias = (discrepancia_ascendiente or discrepancia_descendiente or
                                 (discrepancia_interna and not discrepancia_interna_asumida))
                # No mostramos alerta si la discrepancia es descendiente, porque no se puede resolver
                # desde incidencias_globales
                f.append(TD(A(SPAN(_class='glyphicon glyphicon-list-alt' +
                                          (' icon-warning'
                                           if (pendiente_consolidacion
                                               or discrepancia_ascendiente
                                               or (discrepancia_interna and not discrepancia_interna_asumida))
                                           else ''),
                                   _title=T('Incidencias globales'),
                                   **{'_data-toggle':'tooltip', '_data-placement':'bottom'}),
                                   _href=URL(c='gestion_pedidos', f='incidencias_globales.html',
                                             vars=dict(pedido=ipedido, grupo=self.igrupo))),
                            A(SPAN(_class='glyphicon glyphicon-bell icon-warning',
                                   _title=T('Reportar incidencias obligatorias globalmente'),
                                   **{'_data-toggle':'tooltip', '_data-placement':'bottom'}),
                                   _href=URL(c='gestion_pedidos', f='incidencias_globales.html',
                                             vars=dict(obligatorias='1', pedido=ipedido,  grupo=self.igrupo)))
                            if (pendiente_consolidacion
                                or discrepancia_ascendiente
                                or (discrepancia_interna and not discrepancia_interna_asumida))
                            else '' ))
                f.append(TD(A(SPAN(_class='glyphicon glyphicon-th'  +
                                          (" icon-warning" if (discrepancia_descendiente
                                                               or (discrepancia_interna and
                                                                   not discrepancia_interna_asumida))
                                            else " icon-asume-red" if (discrepancia_ascendiente or
                                                                       pendiente_consolidacion)
                                            else ""),
                                   _title=(T('Es necesario resolver incidencias')
                                           if discrepancias else T('Incidencias')),
                                   **{'_data-toggle':'tooltip', '_data-placement':'bottom'}),
                              _href=URL(c='gestion_pedidos', f='incidencias.html',
                                        vars=dict(pedido=ipedido, grupo=self.igrupo))
                                    if self.red else
                                    URL(c='gestion_pedidos', f='incidencias.html',
                                        vars=dict(pedido=ipedido, grupo=self.igrupo)),
                              _id = 'td_pedido_incidencias_%d'%ipedido ),
                            A(SPAN(_class='glyphicon glyphicon-bell',
                                   _title=T('Incidencias o pesar productos'),
                                   **{'_data-toggle':'tooltip', '_data-placement':'bottom'}),
                              _href=URL(c='gestion_pedidos', f='incidencias.html',
                                        vars=dict(obligatorias='1', pedido=ipedido, grupo=self.igrupo)))
                            if (pendiente_consolidacion or discrepancias)
                            else '' ) )
                if self.red:
                    f.append(TD(A(SPAN(_class='glyphicon glyphicon-scale'  +
                                              (' icon-warning' if (discrepancia_interna and not
                                                                   discrepancia_interna_asumida)
                                               else ' icon-asume-red' if discrepancia_descendiente
                                               else ''),
                                       _title=T('Incidencias detalladas por grupo o subred'),
                                       **{'_data-toggle':'tooltip', '_data-placement':'bottom'}),
                                  _href=URL(c='redes', f='incidencias_desdered.html',
                                            vars=dict(pedido=ipedido, red=self.igrupo)))
                                ) )

                f.append(TD(A(SPAN(_class='glyphicon glyphicon-export',
                                   _title=T('Tabla para reparto'),
                                   **{'_data-toggle':'tooltip', '_data-placement':'bottom'}),
                              _href=URL(c='gestion_pedidos', f='exportar_tabla_reparto.ods',
                                        vars=dict(pedido=ipedido, grupo=self.igrupo))) ) )
                f.append( TD(A(SPAN(_class='glyphicon glyphicon-plus',
                                    _title=T('Totales'),
                                    **{'_data-toggle':'tooltip', '_data-placement':'bottom'}),
                               _href=URL(c='gestion_pedidos', f='vista_pedido.html',
                                         vars=dict(pedido=ipedido, grupo=self.igrupo)))) )

        #for f,(_,ipedido,_,_,_,_,_) in zip(filas,pedidos):
        #    f.append( TD(A(SPAN(_class='glyphicon glyphicon-plus',
        #                        _title=T('Totales'),
        #                        **{'_data-toggle':'tooltip', '_data-placement':'bottom'}),
        #                   _href=URL(c='gestion_pedidos', f='vista_pedido.html',
        #                             vars=dict(pedido=ipedido, grupo=self.igrupo)))) )
        if self.red:
            cabeceras.append(TH(T('Grupos'), tooltip['grupos']) )
            for f,p in zip(filas,ps):
                discrepancia_productor = p.pedido.discrepancia_productor
                discrepancia_interna = p.pedido.discrepancia_interna
                discrepancia_descendiente = p.pedido.discrepancia_descendiente
                discrepancia_interna_asumida = p.pedido.discrepancia_interna_asumida
                discrepancia_cerrada = p.pedido.discrepancia_interna_asumida
                discrepancias = ( discrepancia_productor or discrepancia_descendiente or
                                 (discrepancia_interna and not discrepancia_interna_asumida))

                f.append( TD(A(SPAN(_class='glyphicon glyphicon-tasks',
                        _title=T('Grupos'),
                        **{'_data-toggle':'tooltip', '_data-placement':'bottom'}),
                   _href=URL(c='redes', f='elegir_grupos_pedido.html',
                             vars=dict(pedido=p.pedido.id, red=self.igrupo)))) )


        return DIV(TABLE(THEAD(TR( *cabeceras )),
                        *filas, _class='table' ), _class='pedidos_coordinados table-responsive') if ps else ''

    def acciones_por_fecha_pedido(self, f):
        T = current.T
        botones_acciones = [A(T('Informes'),
                              _href=URL(c='gestion_pedidos', f='ver_informes.html',
                                        vars={'fecha_reparto':f, 'grupo':self.igrupo}),
                             _class='btn btn-default')]
        if not self.red:
            botones_acciones.append(
                A(T('Cobrar'),
                  _href=URL(c='gestion_pedidos', f='cobro.html', vars={'fecha_reparto':f, 'grupo':self.igrupo}),
                  _class='btn btn-default')
            )
        acciones_descargas = [
            LI(A(T('Peticiones (ods)'),
              _href=URL(c='gestion_pedidos', f='exportar_pedidos_fecha.ods',
                        vars=dict(grupo=self.igrupo, fecha_reparto=f)))),
           LI(A(T('Incidencias (ods)'),
              _href=URL(c='gestion_pedidos', f='exportar_pedidos_fecha.ods',
                        vars=dict(grupo=self.igrupo, fecha_reparto=f, incidencias=1))))
        ]
        if not self.red:
            acciones_descargas.append(
                LI(A(T('Tabla para reparto (ods)'),
                    _href=URL(c='gestion_pedidos', f='exportar_tabla_reparto_fecha.ods',
                    vars=dict(grupo=self.igrupo, fecha_reparto=f))))
            )
        else:
            acciones_descargas.extend([
                LI(A(T('Suministrado (ods)'),
                     _href=URL(c='gestion_pedidos', f='exportar_pedidos_fecha.ods',
                               vars=dict(grupo=self.igrupo, fecha_reparto=f, incidencias=2)))),
                LI(A(T('Discrepancias (ods)'),
                     _href=URL(c='redes', f='exportar_pedidos_redes_fecha.ods',
                               vars=dict(red=self.igrupo, fecha_reparto=f)))),
                LI(A(T('Pediciones desglosadas (ods)'),
                     _href=URL(c='redes', f='exportar_pedidos_red_por_grupos_fecha.ods',
                               vars=dict(red=self.igrupo, fecha_reparto=f)))),
                LI(A(T('Incidencias desglosadas (ods)'),
                     _href=URL(c='redes', f='exportar_pedidos_red_por_grupos_fecha.ods',
                               vars=dict(red=self.igrupo, fecha_reparto=f, incidencias=1)))),
                LI(A(T('Suministrado desglosado (ods)'),
                     _href=URL(c='redes', f='exportar_pedidos_red_por_grupos_fecha.ods',
                               vars=dict(red=self.igrupo, fecha_reparto=f, incidencias=2))))
            ])


        botones_acciones.append(
            DIV(A(T('Descargar hoja de cálculo'),
                  SPAN(' ', _class='caret'),
                  **{'_class':"btn btn-default dropdown-toggle",
                     '_data-toggle':"dropdown", '_href':'#'}),
                UL(acciones_descargas, _class='dropdown-menu'),
                _class='btn-group', _role='group')
            )
        if not self.red:
            botones_acciones.append(
                A(T('Cargar tabla del reparto'),
                  _href=URL(c='gestion_pedidos', f='importar_tabla_reparto_fecha.html',
                            vars={'fecha_reparto':f, 'grupo':self.igrupo}),
                  _class='btn btn-default')
                )
        return DIV( DIV(*botones_acciones, _class='btn-group', _role='group'))

    def form_limpiar_peticion(self):
        T = current.T
        return DIV(
            INPUT(_type='hidden', _id='grupo', _name='grupo', _value=self.igrupo),
            INPUT(_type='hidden', _id='pedido', _name='pedido', _value=''),
            DIV(DIV(DIV(
                # header
                DIV(BUTTON(SPAN(XML("&times;"), **{"_aria-hidden":"true"}),
                           **{"_type":"button", "_class":"close",
                              "_data-dismiss":"modal", "_aria-label":"Close"}),
                    H4(T('Elimina el pedido del grupo'), **{"_class":"modal-title"}),
                    _class='modal-header'),
                # body
                DIV(T('Elimina todas las peticiones de los grupos de esta red.')
                    if self.red else
                    T('Elimina toda la petición del grupo hasta el momento.'),
                    **{'_class':'modal-body'}),
                # footer
                DIV(
                    BUTTON(T('Cancelar'),
                           **{"_type":"button", "_class":"btn btn-default",
                              "_data-dismiss":"modal", "_aria-label":T('Cancelar')}),
                    BUTTON(T('Eliminar peticiones'),
                           **{"_type":"button", "_class":"btn btn-primary",
                              "_onclick":"js_limpia_pedido()",
                              "_data-dismiss":"modal", "_aria-label":T('Eliminar')}),
#        <button type="button" class="btn btn-primary" id="continuar" href='#' onclick="$('#modal_repartir').modal('hide');$('#incidencias_globales').submit()">{{ =T('Aceptar')}}</button>
                    **{'_class':'modal-footer'}),
                    _class="modal-content"),
                _class="modal-dialog modal-lg"),
            _class='modal', _id='modal_limpiar_peticion', _tabindex="-1", _role="dialog"),
            SCRIPT('''
var muestra_js_limpia_pedido = function(ipedido){
    $('#pedido').val(ipedido);
    $('#modal_limpiar_peticion').modal();
}
var js_limpia_pedido = function(){
    var td_pedido;
    ipedido = $('#pedido').val();
    web2py.ajax('/gestion_pedidos/limpia_peticion', ['grupo', 'pedido'], ':eval');
    td_pedido = $('#pedido_'+ipedido);
    td_pedido.hide().empty().append('No').fadeIn();
};
var reactiva_links = function(ipedido){
    $('#td_pedido_activo_'+ipedido).parent().find('a').off('click.disable');
};
var desactiva_links = function(ipedido){
    $('#td_pedido_activo_'+ipedido).parent().find('a').on('click.disable', function(e){
        e.preventDefault();
        web2py.flash('%(mensaje_link_desactivado)s');
    });
};
var js_activa_pedido = function(ipedido){
    var boton_activar_pedido = $('#td_pedido_activo_'+ipedido).find('span');
    boton_activar_pedido.attr('disabled', true);
    boton_activar_pedido.removeClass().addClass('glyphicon glyphicon-transfer');
    activar = 1 - $('#pedido_activo_'+ipedido).val();
    $.getJSON('/gestion_pedidos/activa_pedido_coordinado.json', {
        grupo:$('#grupo').val(),
        pedido:ipedido,
        activar:activar
    }, function(respuesta){
        if(respuesta.status=='ok'){
            if (respuesta.activo==1){
                web2py.flash('%(mensaje_activar)s');
            }else{
                web2py.flash('%(mensaje_desactivar)s');
            }
        }else{
            web2py.flash(respuesta.mensaje);
        }
        if (respuesta.activo==1){
            boton_activar_pedido.removeClass().addClass('glyphicon glyphicon-check');
            reactiva_links(ipedido);
        }else{
            boton_activar_pedido.removeClass().addClass('glyphicon glyphicon-unchecked');
            desactiva_links(ipedido);
        }
        $('#pedido_activo_'+ipedido).val(respuesta.activo);
        boton_activar_pedido.attr('disabled', false);
    });
};
$(function(){
    $('input[id^="pedido_activo_"][value=0]').each(function(){
        var eid = this.id;
        desactiva_links( parseInt(eid.substr(14)));
    });
});
'''%dict(mensaje_desactivar = T('Se ha desactivado el pedido.'),
         mensaje_activar = T('Se ha activado el pedido con todos sus productos. Puedes rechazar productos individuales usando otra acción.'),
         mensaje_link_desactivado=T('No puedes seguir ese link para un pedido desactivado'),
         mensaje_discrepancia_deshecha=T('Se ha deshecho la discrepancia asumida. El pedido queda pendiente.'),
         mensaje_discrepancia_cerrada_grupo=T('Se ha cerrado la discrepancia. El grupo asume lo que dice la red, respetando lo repartido a las unidades')),
            _type='text/javascript')
        )

    def print_pedidos(self):
        C = current.C
        T = current.T
        tp = self.tp

        if self.tp in (EstadosPedido.ACTIVOS, EstadosPedido.RECIENTES):
            return self.print_pedidos_activos(tp)
        pedidos = self.pedidos

        return DIV(
           self.form_limpiar_peticion(),
           *[DIV(self.cabecera_fecha(f),
                 self.acciones_por_fecha_pedido(f),
                 H4(T('Pedidos del grupo')) if pcs else '',
                 self.tabla_pedidos(ps,tp,pasado=(f<self.today)) if ps else
                    P(T('No hay %s propios para esta fecha')%self.dEstadosPedidoPlural[tp].lower()),
                 self.bloque_bundles(self.bundles[f]),
                 H4(T('Pedidos Coordinados')) if pcs else '',
                 self.tabla_pedidos_coordinados(pcs,tp,pasado=(f<self.today)),
                )
             for f,(ps, pcs) in pedidos ]
            ) if pedidos else P(T('No hay pedidos %s'%tp))

    def bloque_bundles(self, xs):
        T = current.T
        if not xs: return ''
        return DIV(*[DIV(DIV(H5(T('Pedidos de %s')%', '.join(self.nombres_pedidos[pid] for pid in pedidos)),
                               A(T('Exportar en el formato original'),
                                 _href=URL(c='gestion_pedidos', f='exportar_plantilla_multiple.xls',
                                           vars=dict(plantilla=iplantilla, grupo=self.igrupo)),
                                 _class='btn btn-default'),
                               A(T('Cerrar este bloque de pedidos'),
                                 _href=URL(c='gestion_pedidos', f='cerrar_pedidos.html',
                                           vars=dict(plantilla=iplantilla, grupo=self.igrupo)),
                                 _class='btn btn-default'),
                               A(T('Exportar incidencias'),
                                 _href=URL(c='gestion_pedidos', f='exportar_plantilla_multiple.xls',
                                           vars=dict(plantilla=iplantilla,
                                                     incidencias='true',
                                                     grupo=self.igrupo)),
                                 _class='btn btn-default')),
                                _class='col-lg-4 col-md-6 bloque')
                        for iplantilla, pedidos in xs],
                      _class='row')

    def print_pedidos_activos(self, tp=EstadosPedido.ACTIVOS):
        C = current.C
        T = current.T
        pedidos = self.pedidos
        return DIV(
             self.form_limpiar_peticion(),
            *[DIV(
                 self.cabecera_fecha(f),
                 self.acciones_por_fecha_pedido(f),
                 H4(T('Pedidos del grupo')) if (pacs or pccs) else '',
                 P(T('No hay pedidos propios del grupo para esta fecha')) if not (paps or pcps or ppps) else '',
                 self.tabla_pedidos(paps,EstadosPedido.ABIERTOS,pasado=(f<self.today)) if paps else '',
                 self.tabla_pedidos(ppps,EstadosPedido.PENDIENTES) if ppps else '',
                 self.tabla_pedidos(pcps,EstadosPedido.CERRADOS) if pcps else '',
                 self.bloque_bundles(self.bundles[f]),
                 H4(T('Pedidos Coordinados')) if (pacs or pccs) else '',
                 self.tabla_pedidos_coordinados(pacs,EstadosPedido.ABIERTOS,pasado=(f<self.today)) if pacs else '',
                 self.tabla_pedidos_coordinados(ppcs,EstadosPedido.PENDIENTES) if ppcs else '',
                 self.tabla_pedidos_coordinados(pccs,EstadosPedido.CERRADOS) if pccs else '',
                    )
              for f,(paps, pacs), (pcps, pccs), (ppps, ppcs) in pedidos ])
