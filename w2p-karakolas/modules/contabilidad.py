# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################
from decimal import Decimal
from collections import defaultdict

from gluon.html import P
from gluon import current
try:#web2py >=2.9.12
   from gluon.dal.objects import Table
except:#web2py <=2.9.11
   from gluon.dal import Table

from .kutils import represent
from .utils.db import field_starts
from .modelos.usuarios import es_admin_del_grupo, es_admin_o_miembro_del_grupo

############### Distintos tipos de contabilidad ###############
class OpcionIngresos(dict):
    NO_CONT = 0
    DISTRIBUIDO = 1
    SOLO_ADMIN  = 2
    #VERIFICADO_POR_ADMIN = 3
    def __init__(self):
        #Tengo que crear estos tipos de cuenta dentro de un método para poder usar T
        T = current.T
        self.update({
        #Grupos
            self.NO_CONT: (
                T('Sin contabilidad'),
                T('El grupo no usa karakolas para su contabilidad. No se pueden hacer ingresos, y no se muestra en ningún momento los saldos de las cuentas, aunque internamente se hacen apuntes en las cuentas del grupo si el grupo participa en pedidos coordinados para que las redes puedan hacer su contabilidad (no se hace ningún apunte para los pedidos propios). El grupo puede decidir activar la contabilidad más adelante.'),
                T('Sin contabilidad'),
                T('La red no usa karakolas para su contabilidad. No se pueden hacer ingresos, y no se muestra en ningún momento los saldos de las cuentas, aunque internamente se hacen apuntes en las cuentas de la red si alguno de los grupos de la red usa karakolas para su contabilidad. La red puede decidir activar la contabilidad más adelante.')
            ),
            self.DISTRIBUIDO: (
                T('Gestionados por las unidades'),
                T('Las personas anotan ingresos en sus cuentas directamente. Las administradoras pueden visualizar los ingresos de todas las personas del grupo y modificarlos.'),
                T('Gestionados por los grupos'),
                T('Los grupos anotan sus ingresos directamente y se añaden al saldo del grupo en la red automáticamente. Las administradoras de la red pueden visualizar los ingresos de todos los grupos y modificarlos.')
            ),
            self.SOLO_ADMIN: (
                T('Gestionados por las administradoras'),
                T('Las administradoras del grupo anotan todos los ingresos. Las personas sólo pueden consultar su saldo.'),
                T('Gestionados por la red'),
                T('Las administradoras de la red anotan todos los ingresos. Los grupos sólo pueden consultar su saldo en la red.')
            ),
#            self.VERIFICADO_POR_ADMIN : (
#                T('Verificados por las administradoras'),
#                T('Las personas anotan sus ingresos, pero quedan sin verificar. Las administradoras tienen una página especial para verificar los ingresos de las personas, y tb pueden hacer anotaciones en nombre de cualquier persona del grupo.'),
#                T('Verificados por la red'),
#                T('Los grupos anotan sus ingresos, pero quedan sin verificar. Las administradoras de la red tienen una página especial para verificar los ingresos de los grupos, y tb pueden anotar los ingresos de cualquier grupo.')
#            )
        })

class OpcionPedidos(dict):
    NO_CONT = 0
    NO_BLOQUEO = 1
    PARA_Y_PIENSA = 2
    #NO_SIN_SALDO = 3 #TODO, sin prisa
    def __init__(self):
        #Tengo que crear estos tipos de cuenta dentro de un método para poder usar T
        T = current.T
        self.update({
        #Grupos
            self.NO_CONT : (
                T('Sin contabilidad'),
                T('El grupo no usa karakolas para su contabilidad. Los pedidos se cierran con normalidad.'),
                T('Sin contabilidad'),
                T('La red no usa karakolas para su contabilidad. Los pedidos se cierran con normalidad.')
            ),
            self.NO_BLOQUEO : (
                T('No bloquear pedidos'),
                T('Al cerrar el pedido desciende el saldo de las unidades que han pedido, pero siempre se sigue adelante con el pedido, aunque los saldos de las unidades queden en negativo. Las unidades deberán corregir ese saldo negativo, típicamente el mismo día del reparto.'),
                T('No bloquear pedidos'),
                T('Al cerrar el pedido desciende el saldo de los grupos que han pedido, pero siempre se sigue adelante con el pedido, aunque los saldos de los grupos queden en negativo. Los grupos deberán corregir ese saldo negativo, en el momento y forma acordados.'),
            ),
            self.PARA_Y_PIENSA : (
                T('Parar si no hay saldo'),
                T('Si alguna unidad tiene saldo menor al total de lo que ha pedido, se detiene el pedido. Se muestra un interfaz que muestra todas las unidades en excepción, y hace fácil visualizar si el grupo, de forma conjunta, tiene saldo suficiente, y borrar el pedido de las unidades con saldo insuficiente, a discreción de quien cierra el pedido. En los pedidos coordinados, desgraciadamente, no es posible, así que se elimina el pedido de las unidades con saldo insuficiente.'),
                T('Parar si no hay saldo'),
                T('Si algún grupo tiene saldo menor al total de lo que ha pedido, se detiene el pedido. Se muestra un interfaz que muestra todos los grupos en esta situación, y hace fácil visualizar si la red, de forma conjunta, tiene saldo suficiente, o borrar el pedido de los grupos con saldo insuficiente, a discreción de quien cierra el pedido.')
            )
        })

########################## Monedas ######################################

def monedas_grupo(igrupo):
    db = current.db
    return dict((row.id, row.simbolo_html)
        for row in db((db.grupoXmoneda.grupo==igrupo) &
                      (db.grupoXmoneda.activo==True) &
                      (db.grupoXmoneda.moneda==db.moneda.id)).select(
                      db.moneda.id, db.moneda.simbolo_html)
    )

########################## Cuentas ######################################
class TipoCuenta(object):
    TARGET_SINK = ('sink', 1)
    TARGET_SELF = ('self', 2)

    def __init__(self, plantilla_descripcion, owner, target, table,
                 id_tipo_cuenta, ayuda=''):
        self.plantilla_descripcion = plantilla_descripcion
        self.owner  = owner
        self.target = target
        self.table  = table
        self.id_tipo_cuenta = id_tipo_cuenta
        self.ayuda = ayuda

    def identificadores_cuenta(self):
        nowner, towner = self.owner
        if self.table==towner:
            oid = (lambda f,id:id)
        else:
            ofieldname = self.table[nowner].name
            oid = (lambda f,id: f[ofieldname])
        ntarget, ttarget = self.target
        if self.table==ttarget:
            tid = (lambda f,id:id)
        elif self.target in [self.TARGET_SELF, self.TARGET_SINK]:
            tid = lambda f,id:None
        else:
            tfieldname = self.table[ntarget].name
            tid = (lambda f,id: f[tfieldname])
        return oid, tid

    def creador_cuentas(self):
        oid, tid = self.identificadores_cuenta()
        dbc = current.db.cuenta
        def func(f, id):
            iowner, itarget = oid(f,id), tid(f,id)
            dbc.update_or_insert(
                (dbc.tipo==self.id_tipo_cuenta) &
                (dbc.owner == iowner) &
                (dbc.target == itarget),
                tipo=self.id_tipo_cuenta, owner=iowner, target=itarget, activa=True
            )
        return func

    def desactivador_cuentas(self):
        '''Desactiva la cuenta si se desactiva el registro responsable
        de la creacion de esta cuenta
        '''
        oid, tid = self.identificadores_cuenta()
        def func(s,f):
            if 'activo' in f:
                val = f['activo']
                for row in s.select():
                    current.db.cuenta(**dict(
                        tipo=self.id_tipo_cuenta,
                        owner = oid(row,row.id),
                        target = tid(row,row.id)
                    )).update_record(activa=val)
        return func

    def desactiva_cuenta_si_se_desactiva_owner(self):
        db = current.db
        _, towner = self.owner
        tc = self.id_tipo_cuenta
        def func(s,f):
            if 'activo' in f:
                val = f['activo']
                db((db.cuenta.tipo==tc) &
                    db.cuenta.owner.belongs(s._select(towner.id))
                ).update(activa=val)
        return func

    def desactiva_cuenta_si_se_desactiva_target(self):
        db = current.db
        _, ttarget = self.target
        tc = self.id_tipo_cuenta
        def func(s,f):
            if 'activo' in f:
                val = f['activo']
                db((db.cuenta.tipo==tc) &
                    db.cuenta.target.belongs(s._select(ttarget.id))
                ).update(activa=val)
        return func

    def register_callbacks(self):
        table = self.table
        table._after_insert.append(self.creador_cuentas())
        #al desactivar el registro que pario la cuenta, es necesario desactivar las
        #cuentas con otro callback
        table._after_update.append(self.desactivador_cuentas())
        #y tb si se esfuman el owner o el target.
        nowner, towner = self.owner
        if isinstance(towner, Table) and towner!=table:
            towner._after_update.append(self.desactiva_cuenta_si_se_desactiva_owner())
        ntarget, ttarget = self.target
        if isinstance(ttarget, Table) and ttarget!=table:
            ttarget._after_update.append(self.desactiva_cuenta_si_se_desactiva_target())

    def tiene_permiso(self, uid, iowner, itarget):
        '''Devuelve true si el usuario tiene permiso de lectura en esta cuenta
        '''
        db = current.db
        towner  = self.owner[1]
        ttarget = self.target[1]
        #TODO: los miembros podrian no tener permisos de lectura en alguna cuenta,
        #pero empecemos por la transparencia...
        if towner == db.auth_user:
            return uid==iowner
        elif db.grupo in (towner, ttarget):
            return any(es_admin_o_miembro_del_grupo(uid, igrupo)
                       for igrupo,table in zip((iowner, itarget), (towner, ttarget))
                       if table==db.grupo)
        elif towner==db.productor:
            p = db.productor(iowner)
            return p and es_admin_o_miembro_del_grupo(uid, p.grupo)

    def descripcion(self, iowner, itarget):
        db = current.db
        owner_name, owner_type = self.owner
        # No funciona, y no se por que:
        #owner_value = (represent(owner_type, owner_type(iowner))
        owner_value = (represent(owner_type, db[owner_type](iowner))
                       if isinstance(owner_type, Table) else
                       iowner)
        target_name, target_type = self.target
        target_value = (represent(target_type, db[target_type](itarget))
                        if isinstance(target_type, Table) else
                        itarget or '')
        return self.plantilla_descripcion%{
            owner_name:owner_value,
            target_name:target_value
        }

class CuentaUnidadGrupo(TipoCuenta):
    def __init__(self):
        db = current.db
        TipoCuenta.__init__(self,
            current.T('Cuenta de la unidad %(unidad)s en el grupo %(grupo)s'),
            ('unidad', 'integer'),
            ('grupo', db.grupo),
            db.personaXgrupo,
            TiposCuenta.UNIDAD_GRUPO
        )

    def desactivador_cuentas(self):
        '''solo se desactiva la cuenta UNIDAD_GRUPO si no quedan personas en esa unidad
        '''
        db = current.db
        oid, tid = self.identificadores_cuenta()
        def func(s,f):
            row0 = s.select(limitby=(0,1)).first()
            if not row0:
                return
            igrupo = row0.grupo
            if 'unidad' in f:
                db.cuenta.update_or_insert(
                    tipo=self.id_tipo_cuenta, owner = f['unidad'], target = igrupo)
            if 'activo' in f:
                if f['activo']:
                    for row in s.select():
                        db.cuenta(**dict(
                            tipo=self.id_tipo_cuenta,
                            owner=oid(row,row.id),
                            target=tid(row,row.id)
                        )).update_record(activa=True)
            #Si una persona cambia de unidad, la unidad que deja puede quedar vacia
            #y en este punto no sabemos el numero de unidad...
            unidades_no_vacias = [row.unidad for row in
                db((db.personaXgrupo.grupo==igrupo) &
                   (db.personaXgrupo.activo==True)).
                select(db.personaXgrupo.unidad, groupby=db.personaXgrupo.unidad)]
            #desactiva cuentas de unidades vacias
            db((db.cuenta.tipo==self.id_tipo_cuenta) &
               (db.cuenta.activa==True) &
               (~db.cuenta.owner.belongs(unidades_no_vacias)) &
               (db.cuenta.target==igrupo)
            ).update(activa=False)
        return func

class TiposCuenta(dict):
    PERSONA         = 1
    UNIDAD_GRUPO    = 2
    GRUPO           = 3
    GRUPO_GASTOS    = 4
    GRUPO_RED       = 5
    GRUPO_PRODUCTOR = 6
    PRODUCTOR       = 7

    def __init__(self):
        #Tengo que crear estos tipos de cuenta dentro de un método para poder usar T
        T = current.T
        db = current.db
        self.update({
            self.PERSONA: TipoCuenta(
                T('Cuenta de la usuaria %(persona)s'),
                ('persona', db.auth_user),
                TipoCuenta.TARGET_SELF,
                db.auth_user,
                self.PERSONA
            ),
            self.UNIDAD_GRUPO: CuentaUnidadGrupo(),
            self.GRUPO: TipoCuenta(
                T('Cuenta del grupo %(grupo)s'),
                ('grupo', db.grupo),
                TipoCuenta.TARGET_SELF,
                db.grupo,
                self.GRUPO,
                T('Registra los ingresos y gastos que modifican el saldo del grupo, pero no los adelantos ni las deudas de las unidades, porque es dinero que el grupo custodia pero del que no puede disponer. Al cerrar un pedido, el saldo del grupo aumenta, y disminuyen los saldos de las unidades, pero el grupo debe entregar el coste del pedido al productor, en un movimiento posterior que disminuirá el saldo del grupo.')
            ),
            self.GRUPO_GASTOS: TipoCuenta(
                T('Cuenta de gastos del grupo %(grupo)s'),
                ('grupo', db.grupo),
                TipoCuenta.TARGET_SINK,
                db.grupo,
                self.GRUPO_GASTOS
            ),
            self.GRUPO_RED: TipoCuenta(
                T('Cuenta del grupo %(grupo)s en la red %(red)s'),
                ('grupo', db.grupo),
                ('red', db.grupo),
                db.grupoXred,
                self.GRUPO_RED
            ),
            self.GRUPO_PRODUCTOR: TipoCuenta(
                T('Saldo del grupo %(grupo)s con el productor %(productor)s'),
                ('grupo', db.grupo),
                ('productor', db.productor),
                db.productor,
                self.GRUPO_PRODUCTOR
            ),
            self.PRODUCTOR: TipoCuenta(
                T('Ingresos totales del productor %(productor)s'),
                ('productor', db.productor),
                TipoCuenta.TARGET_SINK,
                db.productor,
                self.PRODUCTOR
            )
        })

    def register_callbacks(self):
        '''Callbacks para crear cuentas siempre que se cree un registro
        que necesita una o más cuentas, y para desactivar las cuentas
        cuando el registro desaparece o se desactiva'''
        for tc in self.values():
            tc.register_callbacks()

########################## Operaciones ####################################

class TipoOperacion (object):
    def __init__(self, plantilla_descripcion, parametros, id_tipo_operacion):
        self.plantilla_descripcion = plantilla_descripcion
        self.parametros = parametros
        self.id_tipo_operacion = id_tipo_operacion
        #Por ahora se usa solo para ingresos, indica si se admite un valor negativo
        self.positivo = True

    def ayuda(self, html=True):
        texto = self.__init__.__doc__
        texto = current.T(texto) if texto else ''
        return P(texto) if html else texto

    def tiene_permiso(self, uid, params):
        '''Devuelve True si el usuario tiene permiso para anotar esta operacion

        Son permisos de escritura, por tanto mas estrictos que para leer las cuentas
        '''
        db = current.db
        pname, ptype = self.parametros[0]
        if ptype == db.grupo:
            igrupo = params[0]
            return es_admin_del_grupo(uid, igrupo)
        if ptype == db.pedido:
            ipedido = params[0]
            try:
                igrupo = db.pedido(ipedido).productor.grupo
                return es_admin_del_grupo(uid, igrupo)
            except AttributeError:
                return False

    def ejecuta(self, params, valor=Decimal(), **kwds):
        raise NotImplementedError

    def _print_vals(self, params):
        def param_value(datatype, value):
            return (represent(datatype, datatype[value])
                    if isinstance(datatype, Table) else
                    value)
        return dict(
            (name, param_value(datatype, val))
            for (name, datatype), val in zip(self.parametros, params)
        )

    def descripcion(self, params):
        vals = self._print_vals(params)
        return self.plantilla_descripcion%vals

class Ingreso(TipoOperacion):
    def ejecuta(self, params, valor=Decimal(), moneda=None, **kwds):
        if not valor:
            return
        db = current.db
        cuenta_origen  = self.cuenta_origen(params)
        cuenta_destino = self.cuenta_destino(params)
        ioperacion = db.operacion.insert(
            tipo=self.id_tipo_operacion,
            parametros=params,
            valor=valor,
            verificada=kwds.get('verificada', True),
            comentario=kwds.get('comentario', ''),
            moneda=moneda
        )
        db.apunte.insert(
            operacion=ioperacion,
            cuenta=cuenta_origen.id,
            cantidad=-valor,
        )
        db.apunte.insert(
            operacion=ioperacion,
            cuenta=cuenta_destino.id,
            cantidad=valor,
        )

    def monedas(self, params):
        '''Las operaciones que no afectan a la caja no tienen moneda, es decir, se hacen
        en la moneda "None". Si devolvieramos una lista vacia, nos comunicaria que no se
        puede hacer la operacion por no haber moneda en común.
        '''
        return [None]

    def _monedas_del_grupo(self, igrupo):
        '''Devuelve las monedas que usa el grupo.
        '''
        db = current.db
        return [row.moneda for row in db(
            (db.grupoXmoneda.grupo==igrupo) &
            (db.grupoXmoneda.activo==True)
            ).select(db.grupoXmoneda.moneda)]

class IngresoGrupo(Ingreso):
    def __init__(self):
        '''Anota un ingreso de una persona en la cuenta de su unidad en el grupo.
Aumenta el saldo de la unidad.
Aumenta el dinero en caja del grupo pero no el saldo del grupo.
El dinero se considera propiedad de la unidad, y meramente custodiado por el grupo,
hasta que se cierre algún pedido, y el dinero pase a ser del grupo.
        '''
        db = current.db
        TipoOperacion.__init__(self,
            current.T('Ingreso de %(persona)s a la unidad %(unidad)s de %(grupo)s'),
            [('persona', db.auth_user), ('unidad', 'integer'), ('grupo', db.grupo)],
            TiposOperacion.INGRESO_GRUPO
        )

    def tiene_permiso(self, uid, params):
        db = current.db
        ipersona, unidad, igrupo = params
        grupo = db.grupo(igrupo)
        pxg = db.personaXgrupo(persona=ipersona, unidad=unidad,  grupo=igrupo, activo=True)
        if ((grupo.opcion_ingresos==OpcionIngresos.DISTRIBUIDO) and
            pxg and (ipersona==uid)
            ):
            return True
        return es_admin_del_grupo(uid, igrupo)

    def cuenta_origen(self, params):
        return current.db.cuenta(tipo=TiposCuenta.PERSONA, owner=params[0])

    def cuenta_destino(self, params):
        return current.db.cuenta(tipo=TiposCuenta.UNIDAD_GRUPO, owner=params[1], target=params[2])

    def monedas(self, params):
        return self._monedas_del_grupo(params[2])

class PagoUnidad(Ingreso):
    def __init__(self):
        '''El grupo cobra una cantidad a una unidad, por ejemplo una cuota,
        o reintegra una cantidad por el motivo que sea.

        Una cantidad positiva aumenta el saldo del grupo y disminuye el de la unidad.
        La caja del grupo no resulta modificada.
        '''
        db = current.db
        TipoOperacion.__init__(self,
            current.T('Pago de la unidad %(unidad)s a %(grupo)s'),
            [('unidad', 'integer'), ('grupo', db.grupo)],
            TiposOperacion.PAGO_UNIDAD
        )
        self.positivo = False

    def tiene_permiso(self, uid, params):
        db = current.db
        unidad, igrupo = params
        return es_admin_del_grupo(uid, igrupo)

    def cuenta_origen(self, params):
        return current.db.cuenta(tipo=TiposCuenta.UNIDAD_GRUPO, owner=params[0], target=params[1])

    def cuenta_destino(self, params):
        return current.db.cuenta(tipo=TiposCuenta.GRUPO, owner=params[1])

class PagoProductor(Ingreso):
    def __init__(self):
        '''El grupo deposita dinero en la cuenta del grupo con el productor,
        aumentando así su saldo con el productor.
        Puede ser un pago por un pedido ya hecho, o un adelanto para pedidos futuros
        '''
        db = current.db
        TipoOperacion.__init__(self,
            current.T('Ingreso de %(grupo)s en la cuenta de %(productor)s'),
            [('grupo', db.grupo), ('productor', db.productor)],
            TiposOperacion.PAGO_PRODUCTOR
        )

    def ayuda(self, html=True):
        texto = current.T('''
El grupo deposita dinero en la cuenta del grupo con el productor,
aumentando así su saldo con el productor, a costa de disminuir el saldo del grupo.
Al cerrar un pedido se registra el pago de forma automática.
Esto puede hacer que el saldo del grupo con el productor se vuelva negativo.
Al efectuar un depósito en la cuenta, se superan los números rojos.
También se puede depositar dinero como adelanto para pedidos futuros.''')
        return P(texto) if html else texto

    def tiene_permiso(self, uid, params):
        db = current.db
        igrupo, iproductor = params
        productor = db.productor(iproductor)
        return ( productor and
                (productor.grupo==igrupo) and
                 es_admin_del_grupo(uid, igrupo)
        )

    def cuenta_origen(self, params):
        return current.db.cuenta(tipo=TiposCuenta.GRUPO, owner=params[0])

    def cuenta_destino(self, params):
        return current.db.cuenta(tipo=TiposCuenta.GRUPO_PRODUCTOR, owner=params[0], target=params[1])

    def monedas(self, params):
        '''Devuelve las monedas que usa el grupo.
        Por ahora vemos innecesario declarar las monedas de cada productor, el grupo lo sabrá.
        '''
        return self._monedas_del_grupo(params[0])

class AjusteProductor(Ingreso):
    def __init__(self):
        '''El grupo hace un ajuste a su saldo con el productor,
        pero no se produce movimiento de caja.

        Es decir, se usa PagoProductor para pagar la deuda con el productor,
        y AjusteProductor para cancelar o aumentar esa deuda.
        '''
        db = current.db
        TipoOperacion.__init__(self,
            current.T('Pago de %(grupo)s a %(productor)s'),
            [('grupo', db.grupo), ('productor', db.productor)],
            TiposOperacion.AJUSTE_PRODUCTOR
        )
        self.positivo = False

    def ayuda(self, html=True):
        texto = current.T('''
El grupo ajusta su saldo con el productor, por cualquier concepto no relacionado con
pedidos. No se produce movimiento de caja.
Si la cuenta del grupo con el productor está en números rojos tras cerrar un pedido, la
deuda ya está registrada, y no se debe registrar un ajuste, sino un depósito en la cuenta
del productor.
Si hay algún desajuste entre lo pedido y lo recibido, se pueden declarar
incidencias, o incidencias globales.
''')
        return P(texto) if html else texto

    def tiene_permiso(self, uid, params):
        db = current.db
        igrupo, iproductor = params
        productor = db.productor(iproductor)
        return ( productor and
                (productor.grupo==igrupo) and
                 es_admin_del_grupo(uid, igrupo)
        )

    def cuenta_origen(self, params):
        return current.db.cuenta(tipo=TiposCuenta.GRUPO_PRODUCTOR, owner=params[0], target=params[1])

    def cuenta_destino(self, params):
        return current.db.cuenta(tipo=TiposCuenta.PRODUCTOR, owner=params[1])

class PagoRed(Ingreso):
    def __init__(self):
        '''El grupo deposita dinero en la cuenta del grupo con la red,
        aumentando así su saldo con la red.
        Puede ser un pago por un pedido ya hecho, o un adelanto para pedidos futuros
        '''
        db = current.db
        TipoOperacion.__init__(self,
            current.T('Ingreso de %(grupo)s en la cuenta de %(red)s'),
            [('grupo', db.grupo), ('red', db.grupo)],
            TiposOperacion.PAGO_RED
        )

    def ayuda(self, html=True):
        texto = current.T('''
El grupo deposita dinero en la cuenta del grupo con la red,
aumentando así su saldo con la red, a costa de disminuir el saldo del grupo.
Al cerrar un pedido se registra el pago de forma automática:
esto puede hacer que el saldo del grupo con la red se vuelva negativo.
Al efectuar un depósito en la cuenta, se superan los números rojos.
También se puede depositar dinero como adelanto para pedidos futuros.''')
        return P(texto) if html else texto

    def tiene_permiso(self, uid, params):
        db = current.db
        igrupo, ired = params
        red = db.grupo(ired)
        gxr = db.grupoXred(grupo=igrupo, red=ired)
        if ((red.opcion_ingresos!=OpcionIngresos.SOLO_ADMIN) and
            gxr and es_admin_del_grupo(uid, igrupo)
            ):
            return True
        return es_admin_del_grupo(uid, ired)

    def cuenta_origen(self, params):
        return current.db.cuenta(tipo=TiposCuenta.GRUPO, owner=params[0])

    def cuenta_destino(self, params):
        return current.db.cuenta(tipo=TiposCuenta.GRUPO_RED, owner=params[0], target=params[1])

    def monedas(self, params):
        '''Devuelve las monedas comunes al grupo y a la red.
        '''
        monedas_grupo = self._monedas_del_grupo(params[0])
        monedas_red   = self._monedas_del_grupo(params[1])
        return [imoneda for imoneda in monedas_grupo if imoneda in monedas_red]

class AjusteRed(Ingreso):
    def __init__(self):
        '''El grupo hace un ajuste a su saldo con la red,
        pero la contrapartida es la cuenta de la red, no la del grupo.

        Es decir, se usa PagoRed para pagar la deuda con el productor,
        y AjusteRed para cancelar o aumentar esa deuda.
        '''
        db = current.db
        TipoOperacion.__init__(self,
            current.T('Pago de %(grupo)s a %(red)s'),
            [('grupo', db.grupo), ('red', db.grupo)],
            TiposOperacion.AJUSTE_RED
        )
        self.positivo = False

    def ayuda(self, html=True):
        texto = current.T('''
El grupo paga a la red, por cualquier concepto no relacionado con pedidos,
por ejemplo, una cuota que pagan los grupos miembros de la red.
Si el grupo está en números rojos tras cerrar un pedido, el pago por el pedido ya está registrado.
Cuando se entregue el dinero, se anotará un depósito en la cuenta de la red.
Si hay desajustes entre lo pedido y lo recibido, se pueden declarar
incidencias, o incidencias globales.
''')
        return P(texto) if html else texto

    def tiene_permiso(self, uid, params):
        db = current.db
        igrupo, ired = params
        red = db.grupo(ired)
        gxr = db.grupoXred(grupo=igrupo, red=ired)
        if ((red.opcion_ingresos==OpcionIngresos.DISTRIBUIDO) and
            gxr and es_admin_del_grupo(uid, igrupo)
            ):
            return True
        return es_admin_del_grupo(uid, ired)

    def cuenta_origen(self, params):
        return current.db.cuenta(tipo=TiposCuenta.GRUPO_RED, owner=params[0], target=params[1])

    def cuenta_destino(self, params):
        return current.db.cuenta(tipo=TiposCuenta.GRUPO, owner=params[1])

class PagoTrabajadora(Ingreso):
    def __init__(self):
        db = current.db
        TipoOperacion.__init__(self,
            current.T('Pago de %(grupo)s a %(persona)s'),
            [('grupo', db.grupo), ('persona', db.auth_user)],
            TiposOperacion.PAGO_TRABAJADORA
        )

    def cuenta_origen(self, params):
        return current.db.cuenta(tipo=TiposCuenta.GRUPO, owner=params[0])

    def cuenta_destino(self, params):
        return current.db.cuenta(tipo=TiposCuenta.PERSONA, owner=params[1])

    def monedas(self, params):
        return self._monedas_del_grupo(params[0])

class GastoGrupo(Ingreso):
    def __init__(self):
        '''Un gasto del grupo disminuye su saldo y su dinero en caja.

        Usa este botón para gastos no relacionados con karakolas:
        alquiler, electricidad, etc, pero salvo causa justificada, no para unidades,
        productores o redes representados en karakolas.

        Una cantidad negativa es un ingreso extraordinario, un regalo al grupo, etc.
        '''
        db = current.db
        TipoOperacion.__init__(self,
            current.T('Gasto de %(grupo)s'),
            [('grupo', db.grupo)],
            TiposOperacion.GASTO_GRUPO
        )
        self.positivo = False

    def cuenta_origen(self, params):
        return current.db.cuenta(tipo=TiposCuenta.GRUPO, owner=params[0])

    def cuenta_destino(self, params):
        return current.db.cuenta(tipo=TiposCuenta.GRUPO_GASTOS, owner=params[0])

    def monedas(self, params):
        return self._monedas_del_grupo(params[0])

class CierrePedido(TipoOperacion):
    def __init__(self):
        db = current.db
        TipoOperacion.__init__(self,
            current.T('Cierre de %(pedido)s'),
            [('pedido', db.pedido)],
            TiposOperacion.CERRAR_PEDIDO
        )

    def ejecuta(self, params, valor=Decimal(), incidencia=False, **kwds):
        db = current.db
        ipedido    = params[0]
        pedido     = db.pedido(ipedido)
        productor  = pedido.productor
        self.ipedido = ipedido
        self.pedido = pedido
        self.iproductor = productor.id
        self.grupo = productor.grupo
        self.igrupo = self.grupo.id
        if self.grupo.red:
            #Llamamos a CancelarPedido para cancelar los CierreParcialPedido
            CancelarPedido().ejecuta(params)
            self._cierra_pedido_coordinado(incidencia=incidencia,**kwds)
        else:
            self._cierra_pedido_propio(**kwds)

    def _valor_extra_red(self, ipedido):
        db = current.db
        total_extra_red = db.proxy_pedido.coste_red.sum()
        q_extra_red = ((db.proxy_pedido.pedido==ipedido) &
                       (db.proxy_pedido.activo==True) )
        return db(q_extra_red).select(total_extra_red).first()[total_extra_red] or Decimal()

    def _cierra_pedido_coordinado(self, incidencia=False, **kwds):
        db = current.db
        ired = self.igrupo
        ipedido = self.ipedido
        tig = db.item_grupo

        # ¿Es pedido vacío?
        q_productos = ((tig.productoXpedido==db.productoXpedido.id) &
                       (db.productoXpedido.pedido==ipedido) &
                       (tig.precio_total>0) &
                       (tig.grupo==self.igrupo))
        q_extra = ((db.coste_pedido.pedido==ipedido) &
                   (db.coste_pedido.coste>0))
        # Si el pedido es vacío, no anotamos nada
        # Tiene en cuenta la posibilidad de un pedido sin productos pero con coste extra,
        #  que es parte del funcionamiento de algunos grupos
        if db(q_productos).isempty() and db(q_extra).isempty():
            return

        # Todos los grupos y subred involucrados
        # Incluye la red que lanza el pedido, subredes y grupos finales
        grupos_en_pedido = [(row.grupo.id,
                             row.grupo.red,
                             row.grupo.opcion_pedidos,
                             row.proxy_pedido.via,
                             row.proxy_pedido.coste_red)
            for row in  db((db.proxy_pedido.pedido==ipedido) &
                           (db.proxy_pedido.activo==True) &
                           (db.proxy_pedido.grupo==db.grupo.id))
                        .select(db.grupo.opcion_pedidos, db.grupo.id, db.grupo.red,
                                db.proxy_pedido.coste_red, db.proxy_pedido.via)
        ]
        # los grupos PARA_Y_PIENSA eliminan los pedidos
        #  de las unidades con saldo insuficiente de forma automática
        # Solo eliminamos el pedido de unidades sin saldo en el momento de cerrar el pedido, nunca al
        #  declarar incidencias o asumir discrepancias
        if not incidencia:
            cambios = False
            for igrupo, es_red, opcion_pedidos, _, _ in grupos_en_pedido:
                if not es_red and (opcion_pedidos==OpcionPedidos.PARA_Y_PIENSA):
                    excepciones, _, _ = pedido_tiene_saldo_suficiente([ipedido], igrupo)
                    if excepciones:
                        cambios = True
                    lunidades = [u for u,_,_ in excepciones]
                    anula_peticion_unidades(lunidades, igrupo, [ipedido])
            if cambios:
                from .modelos.pedidos import recalcula_costes_extra_pedido
                recalcula_costes_extra_pedido(ipedido)
                # Si el pedido es ahora vacío, no anotamos nada
                if db(q_productos).isempty() and db(q_extra).isempty():
                    return

        # El valor realmente no es importante, se guarda como referencia, un indicador de la magnitud
        #  de la operación, pero no es nada fácil de definir para un cierre de pedido coordinado.
        #  El valor es diferente entre productor y red, y en cada nivel de subred, hasta llegar a los
        #  grupos. Creo que solo tienen sentido el nivel productor <-> red y quizá el último nivel, de
        #  consumidor final, pero es raro porque cada grupo final puede haber aplicado sobreprecios
        #  según criterios muy diferentes :-/
        # Ante la duda, me quedo con la cantidad del pedido al productor pq es más fácil de calcular
        #  y más fácil de definir y entender.
        fields_operacion = dict(tipo=self.id_tipo_operacion, parametros=[ipedido])
        if 'descripcion' in kwds:
            fields_operacion['descripcion'] = kwds['descripcion']
        ioperacion = db.operacion.insert(**fields_operacion)

        #1: red y productor
        valor_productor = self._ajusta_productor(ioperacion)
        db.operacion(ioperacion).update_record(valor=valor_productor)

        #2: grupos y red
        self._ajusta_grupo_y_red(grupos_en_pedido, ioperacion)

        #3: unidades y grupos
        self._ajusta_unidades(
            [igrupo for igrupo, es_red, _, _, _ in grupos_en_pedido if not es_red],
            ioperacion
        )

    def _cierra_pedido_propio(self, **kwds):
        # Adaptada a karakolas v4
        db = current.db
        igrupo = self.igrupo
        pedido = self.pedido
        ipedido = self.ipedido

        # ¿Es pedido vacío?
        #Coste extra total del pedido (podria ser positivo aunque no haya pedidos)
        s_extra = db((db.coste_pedido.pedido==ipedido) &
                     (db.coste_pedido.coste>0))
        s_productos  = db((db.item_grupo.productoXpedido==db.productoXpedido.id) &
                          (db.productoXpedido.pedido==ipedido) &
                          (db.item_grupo.precio_total>0))
        #Si no hay productos ni costes extra, no anotamos nada
        if s_productos.isempty() and s_extra.isempty():
            return

        # Entrada en db.operacion
        fields_operacion = dict(tipo=self.id_tipo_operacion, parametros=[ipedido])
        if 'descripcion' in kwds:
            fields_operacion['descripcion'] = kwds['descripcion']
        ioperacion = db.operacion.insert(**fields_operacion)

        #1: grupo y productor
        valor_productor = self._ajusta_productor(ioperacion)

        #2: unidades y grupo
        self._ajusta_unidades([igrupo], ioperacion)

        # v4: el valor de la operación se fija igual al total del pedido al productor
        #  ver discusión en _cierra_pedido_coordinado
        db.operacion(ioperacion).update_record(valor=valor_productor)

    def _ajusta_productor(self, ioperacion):
        '''Anota la transaccion entre grupo o red primigenia y productor
        '''
        db = current.db
        ipedido = self.ipedido
        igrupo  = self.igrupo
        s_productos  = db((db.item_grupo.productoXpedido==db.productoXpedido.id) &
                          (db.productoXpedido.pedido==ipedido) &
                          (db.item_grupo.precio_recibido>0) &
                          (db.item_grupo.grupo==igrupo))
        total_productor = db.item_grupo.precio_recibido.sum()
        valor_productos_productor = s_productos\
            .select(total_productor).first()[total_productor] or Decimal()
        valor_extra_productor = self.pedido.coste_extra_productor or Decimal()

        valor_productor = valor_productos_productor + valor_extra_productor

        if valor_productor:
            cuenta_gp = db.cuenta(tipo=TiposCuenta.GRUPO_PRODUCTOR, owner=self.igrupo, target=self.iproductor)
            cuenta_p  = db.cuenta(tipo=TiposCuenta.PRODUCTOR, owner=self.iproductor)

            db.apunte.insert(operacion=ioperacion, cuenta=cuenta_gp.id, cantidad=-valor_productor)
            db.apunte.insert(operacion=ioperacion, cuenta=cuenta_p.id,  cantidad=valor_productor)

        return valor_productor

    def _ajusta_unidades(self, igrupos, ioperacion):
        db = current.db
        ipedido = self.ipedido
        for igrupo in igrupos:
            total = db.item.precio_total.sum()
            s_unidades  = db((db.item.productoXpedido==db.productoXpedido.id) &
                             (db.productoXpedido.pedido==ipedido) &
                             (db.item.grupo==igrupo))
            rows_productos = s_unidades.select(db.item.unidad, total, groupby=db.item.unidad)
            d_coste = defaultdict(Decimal, ((row.item.unidad, row[total]) for row in rows_productos))
            # Coste extra
            s_extra = db((db.coste_pedido.pedido==ipedido) &
                         (db.coste_pedido.grupo==igrupo))
            rows_extra = s_extra.select(db.coste_pedido.unidad, db.coste_pedido.coste)
            for row in rows_extra:
                d_coste[row.unidad] += row.coste

            for unidad, coste_unidad in d_coste.items():
                if coste_unidad:
                    #TODO: Mejora performance si sacas un dict unidad -> cuenta_u.id al principio
                    cuenta_u = db.cuenta(tipo=TiposCuenta.UNIDAD_GRUPO, owner=unidad, target=igrupo)
                    db.apunte.insert(operacion=ioperacion,
                                     cuenta=cuenta_u.id,
                                     cantidad=-coste_unidad)

            valor_g = sum(d_coste.values())
            if valor_g:
                cuenta_g  = db.cuenta(tipo=TiposCuenta.GRUPO, owner=igrupo)
                db.apunte.insert(operacion=ioperacion, cuenta=cuenta_g.id, cantidad=valor_g)

    def _ajusta_grupo_y_red(self, grupos_en_pedido, ioperacion):
        # Se hace un poco más complicado porque lo que una red ingresa no es precio_total,
        #  que es la suma de cantidad_red, sino la suma de cantidad_recibida
        db = current.db
        pp = db.proxy_pedido
        ipedido = self.ipedido
        # red primigenia que lanza el pedido
        ired = self.igrupo
        igrupos_en_pedido = [igrupo for igrupo,_,_,_,_ in grupos_en_pedido]

        totales_ingresos_grupos = defaultdict(Decimal)
        totales_pagos_grupos = defaultdict(Decimal)

        d_via = {}
        for igrupo, es_red, _, via, coste_red in grupos_en_pedido:
            d_via[igrupo] = via
            totales_ingresos_grupos[via] += coste_red
            totales_pagos_grupos[igrupo] += coste_red

        total_campo_precio = db.item_grupo.precio_recibido.sum()
        s_productos  = db((db.item_grupo.productoXpedido==db.productoXpedido.id) &
                           db.item_grupo.grupo.belongs(igrupos_en_pedido) &
                          (db.productoXpedido.pedido==ipedido))
        for row in s_productos.select(db.item_grupo.grupo, total_campo_precio, groupby=db.item_grupo.grupo):
            igrupo = row.item_grupo.grupo
            # La red primigenia no paga a ninguna otra red, ya ha echado sus cuentas con el productor
            if igrupo!=ired:
                totales_ingresos_grupos[d_via[igrupo]] += row[total_campo_precio]
                totales_pagos_grupos[igrupo] += row[total_campo_precio]

        for igrupo, _, _, via, _ in grupos_en_pedido:
            paga = totales_pagos_grupos[igrupo]
            if paga:
                cuenta_gr = db.cuenta(tipo=TiposCuenta.GRUPO_RED, owner=igrupo, target=via)
                db.apunte.insert(operacion=ioperacion, cuenta=cuenta_gr.id, cantidad=-paga)
            ingresa = totales_ingresos_grupos[igrupo]
            if ingresa:
                cuenta_r  = db.cuenta(tipo=TiposCuenta.GRUPO, owner=igrupo)
                db.apunte.insert(operacion=ioperacion, cuenta=cuenta_r.id,  cantidad=ingresa)
        # Falta la cuenta de la red primigenia, que no está en grupos_en_pedido
        #  no tiene que pagar pero seguramente tiene ingresos
        igrupo = self.igrupo
        ingresa = totales_ingresos_grupos[igrupo]
        if ingresa:
            cuenta_r  = db.cuenta(tipo=TiposCuenta.GRUPO, owner=igrupo)
            db.apunte.insert(operacion=ioperacion, cuenta=cuenta_r.id,  cantidad=ingresa)

class CancelarPedido(TipoOperacion):
    def __init__(self):
        db = current.db
        TipoOperacion.__init__(self,
            current.T('Cancelación de %(pedido)s'),
            [('pedido', db.pedido)],
            TiposOperacion.CANCELAR_PEDIDO
        )

    def ejecuta(self, params, valor=Decimal(), **kwds):
        db = current.db
        ipedido = params[0]

        #cancelar los CERRAR_PEDIDO_PARCIAL que no han sido cancelados antes
        set_cierre_parcial = db(
            (db.operacion.tipo == TiposOperacion.CERRAR_PEDIDO_PARCIAL) &
            field_starts(db.operacion.parametros, ipedido) &
            (db.operacion.cancelada==False)
        )
        operaciones_cierre_parcial = set_cierre_parcial.select(db.operacion.ALL)
        for row in operaciones_cierre_parcial:
            valor_cierre_parcial = row.valor

            ioperacion_cancelacion_parcial = db.operacion.insert(
                tipo=TiposOperacion.CANCELAR_PEDIDO_PARCIAL,
                parametros=row.parametros,
                valor=valor_cierre_parcial
            )

            q = ((db.apunte.operacion==row.id) &
                 (db.apunte.cuenta==db.cuenta.id))
            apuntes = db(q).select(db.apunte.ALL, db.cuenta.tipo)
            for row in apuntes:
                db.apunte.insert(
                    operacion=ioperacion_cancelacion_parcial,
                    cuenta=row.apunte.cuenta,
                    cantidad=-row.apunte.cantidad
                    )
        set_cierre_parcial.update(cancelada=True)

        #doble comprobación, por compatibilidad con version <v3.5:
        # * usamos el booleano 'cancelada' para asegurarnos de que
        #   cada CierrePedido solo se cancela una vez
        # * la operación que se cancela es la última de cierre
        #   - un pedido no se puede cancelar dos veces
        #   - tras una incidencia siempre queda un cierre de pedido posterior al cierre
        operacion_pedido = db(
            (db.operacion.tipo == TiposOperacion.CERRAR_PEDIDO) &
            (db.operacion.parametros==params) &
            (db.operacion.cancelada==False)
        ).select(db.operacion.ALL, orderby=~db.operacion.id).first()
        if operacion_pedido:
            valor_pedido = operacion_pedido.valor

            ioperacion = db.operacion.insert(
                tipo=self.id_tipo_operacion,
                parametros=params,
                valor=valor_pedido
            )

            q = ((db.apunte.operacion==operacion_pedido.id) &
                 (db.apunte.cuenta==db.cuenta.id))
            apuntes = db(q).select(db.apunte.ALL, db.cuenta.tipo)
            for row in apuntes:
                db.apunte.insert(
                    operacion=ioperacion,
                    cuenta=row.apunte.cuenta,
                    cantidad=-row.apunte.cantidad
                    )
            operacion_pedido.update_record(cancelada=True)

class Incidencia(TipoOperacion):
    def __init__(self):
        '''No es exactamente un tipo de operacion, sino una composicion de dos...

        es decir, que en la base de datos nunca se escribe
        db.operacion.tipo == TiposOperacion.INCIDENCIA

        pero podríamos cambiar este comportamiento, y en vez de hacer una
        operación de CANCELAR_PEDIDO y otra de CERRAR_PEDIDO, hacer sólo una
        de INCIDENCIA con el delta de la operación...
        '''
        db = current.db
        TipoOperacion.__init__(self,
            current.T('Cierre de %(pedido)s después de incidencias'),
            [('pedido', db.pedido)],
            TiposOperacion.INCIDENCIA
        )

    def ejecuta(self, params, valor=Decimal(), **kwds):
        db = current.db
        ipedido = params[0]
        pedido  = db.pedido(ipedido)

        #TODO: Si no ha cambiado nada, no hacemos ningún apunte.
        #Hay que comprobar todos los apuntes...
        CancelarPedido().ejecuta(params)
        CierrePedido().ejecuta(
            params,
            descripcion=self.descripcion(params),
            incidencia=True
        )

class IncidenciaGlobal(TipoOperacion):
    def __init__(self):
        '''Las incidencias globales no permiten ajustar lo que debe cada unidad.
        El desajuste va a cuenta del grupo.

        Deprecated: ya no se aceptan incidencias globales
        '''
        return

    def ejecuta(self, params, valor=Decimal(), **kwds):
        raise DeprecationWarning("Deprecated: ya no se aceptan incidencias globales")

#################### Cierre parcial de pedidos ####################
# El objetivo es consolidar la contabilidad de forma provisional entre un grupo
# y una red sin esperar a que se resuelvan las discrepancias de todos los demás grupos
class CierreParcialPedido(TipoOperacion):
    def __init__(self):
        db = current.db
        TipoOperacion.__init__(self,
            current.T('Cierre parcial de %(pedido)s para %(grupo)s'),
            [('pedido', db.pedido), ('grupo', db.grupo)],
            TiposOperacion.CERRAR_PEDIDO_PARCIAL
        )

    def ejecuta(self, params, valor=Decimal(), **kwds):
        # TODO: redderedes, lo termino a la vuelta si puedo, podemos pasar sin ello.
        #       creo que es mejor usar CierrePedido.ejecuta con un argumento opcional parcial=True,
        #       que mire globalmente todo el pedido, y si por ejemplo la transacción entre un grupo y
        #       una red no tiene discrepancia_ascendiente ni pendiente_consolidacion, se hace, pero
        #       si después un grupo tiene una discrepancia_interna no asumida, no se hace el cobro
        #       a las unidades. El grupo quedaría temporalmente en números rojos... :-/, lo que podría
        #       ser otro incentivo más para resolver las discrepancias.
        return
        db = current.db
        ipedido, igrupo    = params
        pedido     = db.pedido(ipedido)
        productor  = pedido.productor
        iproductor = productor.id
        red      = productor.grupo
        if not red.red:
            raise AttributeError("Sólo se puede hacer cierre parcial de un pedido coordinado")
        ired = red.id
        ppedido = db.proxy_pedido(pedido=ipedido, grupo=igrupo)
        discrepancia_asumida = ppedido.discrepancia_asumida

        q_grupo, q_red = [
            ((t.productoXpedido==db.productoXpedido.id) &
             (db.productoXpedido.pedido==ipedido) &
             (t.cantidad>0) &
             (t.grupo==igrupo))
            for t in (db.item_grupo, db.item_red)]

        #el coste extra de la red y el del grupo son distintos
        #El de la red se obtiene de proxy_pedido
        coste_red = ppedido.coste_red
        # el coste extra del grupo se obtiene de coste_pedido
        # y está desglosado por unidades
        total_extra = db.coste_pedido.coste.sum()
        q_extra_final = ((db.coste_pedido.pedido==ipedido) & (db.coste_pedido.grupo==igrupo))

        #Tiene en cuenta la posibilidad de un pedido sin productos pero con coste extra
        #que es parte del funcionamiento de algunos grupos
        if db(q_grupo).isempty() and db(q_red).isempty() and (not coste_red):
            return

        #No calculamos el valor_total ahora, lo calculamos sumando los ingresos del grupo a la red,
        #que se calculan a veces con item_grupo, a veces con item_red
        valor_red = Decimal()
        fields_operacion = dict(tipo=self.id_tipo_operacion, parametros=[ipedido, igrupo], valor=valor_red)
        if kwds.get('descripcion'):
            fields_operacion['descripcion'] = kwds['descripcion']
        ioperacion = db.operacion.insert(**fields_operacion)

        #1: red y productor
        total_productor = db.item_red.precio_productor.sum()
        valor_productos_productor = db(q_red) \
            .select(total_productor).first()[total_productor] or Decimal()
        valor_extra_productor = pedido.coste_extra_productor or Decimal()
        valor_productor = valor_productos_productor + valor_extra_productor

        if valor_productor:
            cuenta_rp = db.cuenta(tipo=TiposCuenta.GRUPO_PRODUCTOR, owner=ired, target=iproductor)
            cuenta_p  = db.cuenta(tipo=TiposCuenta.PRODUCTOR, owner=iproductor)

            db.apunte.insert(operacion=ioperacion, cuenta=cuenta_rp.id, cantidad=-valor_productor)
            db.apunte.insert(operacion=ioperacion, cuenta=cuenta_p.id,  cantidad=valor_productor)

        #2: grupo y red
        tabla_item_total = db.item_red if discrepancia_asumida==1 else db.item_grupo
        total_red = tabla_item_total.precio_red.sum()
        q_productos = q_red if discrepancia_asumida==1 else q_grupo
        valor_productos_gr = db(q_productos) \
                             .select(total_red).first()[total_red] or Decimal()
        valor_gr = valor_productos_gr + coste_red
        valor_red += valor_gr

        if valor_gr:
            cuenta_gr = db.cuenta(tipo=TiposCuenta.GRUPO_RED, owner=igrupo, target=ired)
            db.apunte.insert(operacion=ioperacion, cuenta=cuenta_gr.id, cantidad=-valor_gr)

        if valor_red:
            cuenta_r  = db.cuenta(tipo=TiposCuenta.GRUPO, owner=ired)
            db.apunte.insert(operacion=ioperacion, cuenta=cuenta_r.id,  cantidad=valor_red)
            db.operacion(ioperacion).update_record(valor=valor_red)

        #TODO HERE
        #3: unidades y grupos
        total_final = db.item_grupo.precio_total.sum()
        total = db.item.precio_total.sum()
        valor_productos_g = db(q_grupo) \
                           .select(total_final).first()[total_final] or Decimal()
        valor_extra_g = db(q_extra_final) \
                        .select(total_extra).first()[total_extra] or Decimal()
        valor_g = valor_productos_g + valor_extra_g

        if valor_g:
            cuenta_g  = db.cuenta(tipo=TiposCuenta.GRUPO, owner=igrupo)
            db.apunte.insert(operacion=ioperacion,
                             cuenta=cuenta_g.id,
                             cantidad=valor_g)

        q_unidades  = ((db.item.productoXpedido==db.productoXpedido.id) &
                       (db.productoXpedido.pedido==ipedido) &
                       (db.item.grupo==igrupo))
        rows = db(q_unidades) \
               .select(db.item.unidad, total, groupby=db.item.unidad)
        rows_extra = db(q_extra_final) \
                     .select(db.coste_pedido.unidad, db.coste_pedido.coste)
        d_coste = defaultdict(Decimal, ((row.item.unidad, row[total]) for row in rows))
        for row in rows_extra:
            d_coste[row.unidad] += row.coste

        for unidad, coste_unidad in d_coste.items():
            if coste_unidad:
                #TODO: Mejora performance si sacas un dict unidad -> cuenta_u.id al principio
                cuenta_u = db.cuenta(tipo=TiposCuenta.UNIDAD_GRUPO, owner=unidad, target=igrupo)
                db.apunte.insert(operacion=ioperacion,
                                 cuenta=cuenta_u.id,
                                 cantidad=-coste_unidad)

class CancelarCierreParcial(TipoOperacion):
    def __init__(self):
        '''Este tipo de operación nunca debería ir sola, pero puede aparecer
        en tres situaciones distintas:

        1. en una INCIDENCIA_PARCIAL, primero se cancela el CERRAR_PEDIDO_PARCIAL,
           y luego se hace uno nuevo.
        2. en una INCIDENCIA, se cancela el CERRAR_PEDIDO y todos los
           CERRAR_PEDIDO_PARCIAL, luego se hace únicamente un CERRAR_PEDIDO
        3. si se cancela un pedido, también se cancelan todos los
           CERRAR_PEDIDO_PARCIAL
        '''
        db = current.db
        TipoOperacion.__init__(self,
            current.T('Cancelación del cierre parcial de %(pedido)s para %(grupo)s'),
            [('pedido', db.pedido), ('grupo', db.grupo)],
            TiposOperacion.CANCELAR_PEDIDO_PARCIAL
        )

    def ejecuta(self, params, valor=Decimal(), **kwds):
        db = current.db

        operacion_pedido = db(
             db.operacion.tipo.belongs((TiposOperacion.CERRAR_PEDIDO_PARCIAL,
                                        TiposOperacion.INCIDENCIA_PARCIAL)) &
            (db.operacion.parametros==params) &
            (db.operacion.cancelada==False)
        ).select(db.operacion.ALL, orderby=~db.operacion.id).first()
        if not operacion_pedido:
            return False
        valor_pedido = operacion_pedido.valor

        ioperacion = db.operacion.insert(
            tipo=self.id_tipo_operacion,
            parametros=params,
            valor=valor_pedido
        )

        q = ((db.apunte.operacion==operacion_pedido.id) &
             (db.apunte.cuenta==db.cuenta.id))
        apuntes = db(q).select(db.apunte.ALL, db.cuenta.tipo)
        for row in apuntes:
            db.apunte.insert(
                operacion=ioperacion,
                cuenta=row.apunte.cuenta,
                cantidad=-row.apunte.cantidad
                )
        operacion_pedido.update_record(cancelada=True)
        return True

class IncidenciaParcial(TipoOperacion):
    def __init__(self):
        '''No es exactamente un tipo de operacion, sino una composicion de dos...

        Por ahora no se contempla escribir una incidencia parcial a un pedido cerrado, sino
        únicamente a un cierre parcial.
        '''
        db = current.db
        TipoOperacion.__init__(self,
            current.T('Cierre parcial de %(pedido)s para %(grupo)s después de incidencias'),
            [('pedido', db.pedido), ('grupo', db.grupo)],
            TiposOperacion.INCIDENCIA_PARCIAL
        )

    def ejecuta(self, params, valor=Decimal(), **kwds):
        #TODO: Si no ha cambiado nada, no hacemos ningún apunte.
        #Hay que comprobar todos los apuntes...
        cancelado = CancelarCierreParcial().ejecuta(params)
        descripcion = self.descripcion(params) if cancelado else None
        CierreParcialPedido().ejecuta(params, descripcion=descripcion)

class TiposOperacion(dict):
    INGRESO_GRUPO           = 1
    PAGO_UNIDAD             = 10
    CERRAR_PEDIDO           = 2
    CERRAR_PEDIDO_PARCIAL   = 13
    CANCELAR_PEDIDO         = 8
    CANCELAR_PEDIDO_PARCIAL = 14
    INCIDENCIA              = 3
    INCIDENCIA_GLOBAL       = 9
    INCIDENCIA_PARCIAL      = 15
    PAGO_PRODUCTOR          = 4
    AJUSTE_PRODUCTOR        = 11
    PAGO_RED                = 5
    AJUSTE_RED              = 12
    PAGO_TRABAJADORA        = 6
    GASTO_GRUPO             = 7
    #No tengo claro si necesitare los siguientes:
    #CERRAR_PEDIDO_COORDINADO     =
    #INCIDENCIA_PEDIDO_COORDINADO =
    def __init__(self):
        self.update({
            self.INGRESO_GRUPO: IngresoGrupo(),
            self.PAGO_UNIDAD: PagoUnidad(),
            self.CERRAR_PEDIDO: CierrePedido(),
            self.CERRAR_PEDIDO_PARCIAL: CierreParcialPedido(),
            self.INCIDENCIA: Incidencia(),
            self.INCIDENCIA_GLOBAL: IncidenciaGlobal(),
            self.INCIDENCIA_PARCIAL: IncidenciaParcial(),
            self.CANCELAR_PEDIDO: CancelarPedido(),
            self.CANCELAR_PEDIDO_PARCIAL: CancelarCierreParcial(),
            self.PAGO_PRODUCTOR: PagoProductor(),
            self.AJUSTE_PRODUCTOR: AjusteProductor(),
            self.PAGO_RED: PagoRed(),
            self.AJUSTE_RED: AjusteRed(),
            self.PAGO_TRABAJADORA: PagoTrabajadora(),
            self.GASTO_GRUPO: GastoGrupo(),
        })

    def register_callbacks(self):
        '''Callbacks para que al cerrar o eliminar pedido se hagan los ajustes en las cuentas
        Atencion: hay que anotar la operacion despues de haber calculado los costes extra!
        '''
        db = current.db
        def si_el_pedido_se_abre(s,f):
            nuevo_estado = f.get('abierto', None)
            if nuevo_estado==None:
                return
            for r in s.select(db.pedido.id, db.pedido.abierto):
                if r.abierto==False and nuevo_estado==True:
                    self[self.CANCELAR_PEDIDO].ejecuta([r.id])

        def si_el_pedido_se_elimina(s):
            for r in s.select(db.pedido.id, db.pedido.abierto):
                if r.abierto==False:
                    self[self.CANCELAR_PEDIDO].ejecuta([r.id])

        db.pedido._before_update.append(si_el_pedido_se_abre)
        db.pedido._before_delete.append(si_el_pedido_se_elimina)

##################### Recalcular saldos ##############################

def recalcular_saldos(cuentas =None):
    '''Recalcula los saldos de las cuentas indicadas
    Inevitable si eliminamos operaciones antiguas no consecutivas
    '''
    #TODO: recalcula tambien los saldos de las cuentas derivadas...
    db = current.db
    if cuentas:
        qc = db.cuenta.id.belongs(cuentas)
        qa = db.apunte.cuenta.belongs(cuentas)
    else:
        qc = db.cuenta.id>0
        qa = db.apunte.id>0

    db(qc).update(saldo=0)
    for row in db(qa).select(orderby=db.apunte.id):
        cuenta = db.cuenta(row.cuenta)
        nuevo_saldo = cuenta.saldo + row.cantidad
        cuenta.update_record(saldo=nuevo_saldo)
        row.update_record(saldo=nuevo_saldo)

def pedido_tiene_saldo_suficiente(ipedidos, igrupo=None):
    '''Comprueba si las unidades de un grupo tienen saldo suficiente como para hacer un pedido

    Solo a nivel de grupo por ahora
    ?? TODO ??: idem para redes, donde se mira el saldo de los grupos
        (no creo que ninguna red actual lo use, y lo puede hacer a mano)
    '''
    db = current.db
    pedido0 = db.pedido(ipedidos[0])
    if igrupo:
        grupo = db.grupo(igrupo)
    else:
        grupo  = pedido0.productor.grupo
        igrupo = grupo.id
    if grupo.red:
        raise NotImplementedError
    saldos = dict(
        (row.owner, row.saldo)
        for row in db((db.cuenta.tipo==TiposCuenta.UNIDAD_GRUPO) &
                      (db.cuenta.target==igrupo)
                   ).select(db.cuenta.owner, db.cuenta.saldo)
    )
    costes_extra = defaultdict(Decimal,(
        (row.unidad, row.coste)
        for row in db((db.coste_pedido.grupo==igrupo) &
                      (db.coste_pedido.pedido.belongs(ipedidos))
                   ).select(db.coste_pedido.unidad, db.coste_pedido.coste)
    ))
    # TODO: v4 ahora se puede hacer con item_grupo.precio_pedido en vez de peticion
    total = db.peticion.precio_total.sum()
    pedidos = db(
        (db.peticion.grupo==igrupo) &
        (db.peticion.productoXpedido==db.productoXpedido.id) &
        (db.productoXpedido.pedido.belongs(ipedidos)) &
        (db.peticion.persona==db.personaXgrupo.persona) &
        (db.personaXgrupo.grupo==igrupo) &
        (db.personaXgrupo.activo==True)
    ).select(total, db.personaXgrupo.unidad,
             groupby=db.personaXgrupo.unidad)
    datos_por_unidad = [(row.personaXgrupo.unidad,
                         row[total] + costes_extra[row.personaXgrupo.unidad],
                         saldos[row.personaXgrupo.unidad])
                        for row in pedidos]
    excepciones = [(u,p,s) for (u,p,s) in datos_por_unidad if p>s]
    total_pedido = sum(p for u,p,s in datos_por_unidad)
    total_saldos = sum(s for u,p,s in datos_por_unidad)
    return excepciones, total_pedido, total_saldos

def anula_peticion_unidades(unidades, igrupo, ipedidos):
    if not unidades: return
    from .modelos.pedidos import prepara_item_grupo
    db = current.db
    peticiones_a_borrar = db(
       (db.peticion.productoXpedido==db.productoXpedido.id) &
       (db.productoXpedido.pedido.belongs(ipedidos)) &
       (db.peticion.persona==db.personaXgrupo.persona) &
       (db.peticion.grupo==igrupo) &
       (db.personaXgrupo.grupo==igrupo) &
       (db.personaXgrupo.activo==True) &
       db.personaXgrupo.unidad.belongs(unidades)
    ).select(db.peticion.id)
    db(db.peticion.id.belongs(row.id for row in peticiones_a_borrar)).delete()
    items_a_borrar = db(
       (db.item.productoXpedido==db.productoXpedido.id) &
       (db.productoXpedido.pedido.belongs(ipedidos)) &
       (db.item.grupo==igrupo) &
       db.item.unidad.belongs(unidades)
    ).select(db.item.id)
    db(db.item.id.belongs(row.id for row in items_a_borrar)).delete()
    prepara_item_grupo(ipedidos, igrupo)
    envia_email_anulacion(unidades, igrupo, ipedidos)

def envia_email_anulacion(unidades, igrupo, ipedidos):
    db = current.db
    T  = current.T
    mail = current.mail
    pedidos = [db.pedido(ipedido) for ipedido in ipedidos]
    grupo = db.grupo(igrupo)
    personas = [row.email for row in db(
        (db.personaXgrupo.grupo==igrupo) &
        (db.personaXgrupo.activo==True) &
        (db.personaXgrupo.unidad.belongs(unidades)) &
        (db.personaXgrupo.persona==db.auth_user.id)
        ).select(db.auth_user.email)
    ]
    opcion_para_y_piensa = OpcionPedidos()[grupo.opcion_pedidos]
    if personas:
        mail.send(to = personas,
                  subject = T('Se ha anulado vuestra petición en algunos pedidos'),
                  message = T('''
Se ha anulado vuestra petición de los pedidos de "%(pedidos)s" por no tener suficiente saldo.

El grupo %(grupo)s ha elegido la opción de contabilidad "%(opcion_contabilidad)s":

%(opcion_contabilidad_descripcion)s
''')%dict(grupo=grupo.nombre,
          pedidos=', '.join(represent(db.pedido, db.pedido(ipedido)) for ipedido in ipedidos),
          opcion_contabilidad=opcion_para_y_piensa[0],
          opcion_contabilidad_descripcion=opcion_para_y_piensa[1]))

def resetea_grupos(igrupos):
    """Elimina todas las operaciones que actuan sobre las cuentas de los grupos, sus
    productores, las unidades del grupo, etc, y a cierres de pedidos que solo involucran
    a los grupos de la lista.
    En los cierres e incidencias de pedidos coordinados que involucran a grupos que no
    estan en la lista igrupos, se borran algunos apuntes, pero no todos, así que la página
    de contabilidad podria no quedar completamente limpia.
    """
    from .cuentas_derivadas import TiposCuentaDerivada
    TIPOS_CUENTA_DERIVADA = TiposCuentaDerivada()

    db = current.db
    dbc = db.cuenta
    dbcd = db.cuenta_derivada
    grupos = [db.grupo(igrupo) for igrupo in igrupos]
    iredes = [grupo.id for grupo in grupos if grupo.red]
    inodos = [grupo.id for grupo in grupos if not grupo.red]
    cuentas_redes = [ row.id for row in db(
            (dbc.tipo==TiposCuenta.GRUPO) &
            (dbc.owner.belongs(iredes))
        ).select(dbc.id) ]
    cuentas_nodos = [row.id for row in db(
            (dbc.tipo==TiposCuenta.GRUPO) &
            (dbc.owner.belongs(inodos))
        ).select(dbc.id) ]
    cuentas_nodos.extend(row.id for row in db(
            (dbc.tipo==TiposCuenta.UNIDAD_GRUPO) &
            (dbc.target.belongs(inodos))
        ).select(dbc.id))
    #el resto de las cuentas no necesito distinguirlas...
    cuentas = cuentas_redes + cuentas_nodos
    cuentas.extend(row.id for row in db(
             (dbc.tipo.belongs((
                TiposCuenta.GRUPO_PRODUCTOR,
                TiposCuenta.GRUPO_GASTOS))) &
            (dbc.owner.belongs(igrupos))
        ).select(dbc.id))
    cuentas.extend(row.id for row in db(
            (dbc.tipo==TiposCuenta.PRODUCTOR) &
            (dbc.owner==db.productor.id) &
            (db.productor.grupo.belongs(igrupos))
        ).select(dbc.id))
    cuentas.extend(row.id for row in db(
            (dbc.tipo==TiposCuenta.GRUPO_RED) &
            (dbc.owner.belongs(inodos)) &
            (dbc.target.belongs(iredes))
        ).select(dbc.id))
    #Toma las operaciones que involucran alguna de las cuentas anteriores
    operaciones = set(row.operacion for row in db(
        db.apunte.cuenta.belongs(cuentas)
        ).select(db.apunte.operacion))
    operaciones_coordinadas = set(row.operacion for row in db(
        (db.apunte.operacion.belongs(list(operaciones))) &
        ~db.apunte.cuenta.belongs(cuentas) &
        (db.apunte.cuenta==dbc.id) &
        (dbc.tipo!=TiposCuenta.PERSONA)
        ).select(db.apunte.operacion))
    #Elimina las operaciones que involucran alguna cuenta que no esta en esa lista anteriores
    operaciones_internas = operaciones.difference(operaciones_coordinadas)
    db(db.operacion.id.belongs(operaciones_internas)).delete()
    ######### Operaciones coordinadas ##################
    #Para las operaciones de pedido coordinado, incidencias en un pedido coordinado, etc
    #Mantenemos la contabilidad del grupo con la red, pero eliminamos los detalles de las
    #unidades
    #Eliminamos los apuntes de cobro a las unidades por el grupo
    apuntes_a_eliminar = [row.id for row in db(
        (db.operacion.id.belongs(operaciones_coordinadas)) &
        (db.operacion.tipo.belongs((TiposOperacion.CERRAR_PEDIDO,
                                    TiposOperacion.CANCELAR_PEDIDO,
                                    TiposOperacion.INCIDENCIA))) &
        (db.apunte.operacion==db.operacion.id) &
        (db.apunte.cuenta.belongs(cuentas_nodos))
    ).select(db.apunte.id)]
    #Eliminamos los apuntes de pago al productor
    apuntes_a_eliminar.extend(row.id for row in db(
        (db.operacion.id.belongs(operaciones_coordinadas)) &
        (db.operacion.tipo.belongs((TiposOperacion.CERRAR_PEDIDO,
                                    TiposOperacion.CANCELAR_PEDIDO,
                                    TiposOperacion.INCIDENCIA))) &
        (db.apunte.operacion==db.operacion.id) &
        (db.apunte.cuenta.belongs(cuentas)) &
        (db.apunte.cuenta==dbc.id) &
        (dbc.tipo.belongs((TiposCuenta.PRODUCTOR,
                                 TiposCuenta.GRUPO_PRODUCTOR)))
    ).select(db.apunte.id) )

    #Las operaciones de cruce entre la red y el grupo solo se eliminan si todos los
    #grupos estan en la lista igrupos, en cuyo caso se habra eliminado ya la operacion
    #si solo estan un grupo y la red, se podria eliminar el apunte del grupo y restar
    #esa cantidad del apunte de la red. Se puede hacer, pero no creo que se usara...

    db(db.apunte.id.belongs(apuntes_a_eliminar)).delete()
    recalcular_saldos(cuentas)

    #Limpia y recalcula los saldos de las cuentas derivadas
    cuentasder = [(row.id,row.tipo,row.owner,row.param) for row in
        db(dbcd.owner.belongs(igrupos)).select(
            dbcd.id, dbcd.tipo,
            dbcd.owner, dbcd.param
            )]
    icuentasder = [icuenta_der for icuenta_der,_,_,_ in cuentasder]
    db(db.saldo_cuenta_derivada.cuenta_derivada.belongs(icuentasder)).delete()
    for icuenta_der, tipo, iowner, iparam in cuentasder:
        tipocuentader = TIPOS_CUENTA_DERIVADA[tipo]
        tipocuentader.recalcula_saldo_cuenta_derivada(iowner,iparam)
