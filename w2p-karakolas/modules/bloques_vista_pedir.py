# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

from gluon.html import FORM, INPUT, URL, A, IMG, SPAN, TAG, DIV, CAT, H3, H4, H5, P, EM, TABLE, THEAD, TBODY, TR, TH, TD, UL, LI, XML, BUTTON
from gluon import current
from gluon.storage import Storage
from decimal import Decimal
from collections import defaultdict

from . import kutils as K
from .modelos.pedidos import peticion_a_item, dame_valores_extra

class PedirProductosController(FORM):
    def __init__(self, productos, ipersona, igrupo, pedidos=None, **attributes):
        '''recibe un objeto Rows (o una lista de objetos "Row") de web2py con los productos y ...
        '''
        FORM.__init__(self, **attributes)
        db = current.db
        T  = current.T

        self.ppps     = [row.productoXpedido.id for row in productos]
        self.set_ppps = set(self.ppps)

        self.igrupo   = igrupo
        self.ipersona = ipersona
        self.unidad   = db.personaXgrupo(persona=ipersona, grupo=igrupo).unidad

        if pedidos:
            self.pedidos  = pedidos
        else:
            spedidos  = set()
            for row in productos:
                spedidos.add(row.productoXpedido.pedido)
            self.pedidos = {}
            for ipedido in spedidos:
                #coordinado, o no
                self.pedidos[ipedido] = (igrupo != db.pedido(ipedido).productor.grupo.id)

        # Mensaje de feedback tras enviar el formulario
        self.feedback = ''

        precio_mascara = db( db.mascara_producto.productoXpedido.belongs(self.ppps) &
                            (db.mascara_producto.grupo==igrupo)) \
                           .select(db.mascara_producto.productoXpedido, db.mascara_producto.precio) \
                       .as_dict(key='productoXpedido')

        data_cat = defaultdict(list)
        for row in productos:
            rppp = row.productoXpedido
            ipedido = rppp.pedido
            preciof = ( precio_mascara[rppp.id]['precio']
                        if self.pedidos[ipedido]
                        else rppp.precio_final)
            data_cat[row.categorias_productos.nombre, rppp.categoria].append((
                 rppp.nombre,
                 rppp.descripcion or '',
                #añado el campo pesar para mostrar al usuario que se pide por unidades pero se paga por kilos
                 rppp.granel,
                 rppp.precio_base,preciof,
                 rppp.id,
                 rppp.destacado,
                 row.productor.nombre,
		         rppp.pesar
         ))

        for data in data_cat.values():
            #Al mezclar productos de varios pedidos, puede que en un pedido los productos
            #esten en mayusculas y en otro no
            data.sort(key=lambda k:k[0].lower())

        p2extra = defaultdict(dict)
        for ipedido in self.pedidos:
            p2extra.update( dame_valores_extra(ipedido) )
        extra   = dict((row.productoXpedido.id, p2extra[row.productoXpedido.producto]) for row in productos)

        bang_cols = [row.nombre
            for row in db( db.columna_extra_pedido.pedido.belongs(self.pedidos) &
                           db.columna_extra_pedido.nombre.contains('_IMP'))
                       .select(db.columna_extra_pedido.nombre,
                               groupby=db.columna_extra_pedido.nombre)]

        cantidades = ','.join("'cantidad_%d'"%row.productoXpedido.id
                              for row in productos)

        def productos_cat(cat_id, cat_nombre, data):
            headers = [TH(T('Producto')), TH('')]
            if len(self.pedidos)>1:
                headers.append(TH(T('Productor')))
            headers.extend(TH(col[:-4]) for col in bang_cols)
            headers.extend([TH(T('Precio unitario')), TH(T('Cantidad')), TH(T('Precio total'))])
            trows = []
            for (nombre_producto, descripcion, granel, preciob,
                 preciof, pid, destacado, nombre_productor, pesar)   in data:
                extra_cols = extra[pid]
                tds = [TD(nombre_producto),
                       TD(A(SPAN(_class=("info-producto glyphicon glyphicon-info-sign" +
                                         (" icon-warning" if destacado else "")),
                                 _title=(XML(descripcion, sanitize=True) +
                                        '<br/> Precio base: %s<br/>'%preciob +
                                        '<br/>'.join(('%s: %s'%(k,XML(v, sanitize=True))
                                            for k,v in extra_cols.items()))
                                        ),
                                 **{'_data-toggle':'tooltip', '_data-placement':'bottom',
                                 '_data-html':'true'})),
                          A(SPAN(_class="glyphicon glyphicon-bell",
                                 _title=T('Se pide por unidades se paga por kilos'),
                                 **{'_data-toggle':'tooltip', '_data-placement':'bottom'}))
                          if (pesar and not granel) else '',
                         )]
                if len(self.pedidos)>1:
                    tds.append( TD(nombre_productor))
                tds.extend(TD(extra_cols.get(col, '')) for col in bang_cols)
                tds.extend([TD(SPAN(preciof, _id='precio_unitario_%d'%pid), XML('&euro;'),
                               INPUT(_type='hidden', _name='granel_%d'%pid,
                                     _id='granel_%d'%pid, _value='%s'%bool(granel)),
                               _class='precio'),
                            TD(INPUT(_name='cantidad_%d'%pid,
                                     _id  ='cantidad_%d'%pid,
                                     _class='cantidad decimal',
                                     _type='decimal',
                                     _autocomplete='off')),
                            TD(SPAN('', _id='precio_producto_%d'%pid), XML('&euro;'),
                               _class='precio') ])
                trows.append(TR(tds))
            return DIV(DIV('%s (%d)'%(cat_nombre, len(data)),
                             **{'_role':'button',
                                '_data-toggle':'collapse',
                                '_href':'#div_cat_%s'%cat_id,
                                'aria-expanded':'false',
                                'aria-controls':'div_cat_%s'%cat_id,
                                '_class':'panel-heading'}),
                       DIV(TABLE(THEAD(TR(*headers)),
                                 TBODY(*trows),
                                 **dict(_id='table_cat_%s'%cat_id)),
                           _class='panel panel-body collapse body-categoria-productos',
                           _id='div_cat_%s'%cat_id),
                       _class='panel panel-default panel-categoria-productos')

        self.components = [
                    DIV(*[productos_cat(cat_id, cat_nombre, data)
                          for (cat_nombre, cat_id), data in sorted(data_cat.items())]),
                    DIV(
                        INPUT(_type='submit', _name='submit', _id='submit',
                              _value='Actualizar cesta de la compra', _class='btn btn-success'),
                        BUTTON(SPAN(_title=T('Expandir'),
                                    _class='glyphicon glyphicon-collapse-down'),
                               _id='expandir_categorias',
                               _class='btn btn-default', _type='button'),
                        BUTTON(SPAN(_title=T('Contraer'),
                                    _class='glyphicon glyphicon-collapse-up'),
                               _id='contraer_categorias',
                               _class='btn btn-default', _type='button'),
                        _class='btn-group'),
                    INPUT(_type='hidden', _id='cambios', _value='0'),
                    INPUT(_type='hidden', _id='ids_productos', _value=','.join(
                            ['%d'%d[5] for _,data in data_cat.items() for d in data]))
                    ]

        peticiones = db((db.peticion.persona==ipersona) &
                        (db.peticion.grupo==igrupo) &
                        (db.peticion.productoXpedido.belongs(self.ppps)) &
                        (db.peticion.productoXpedido==db.productoXpedido.id))\
                     .select(db.productoXpedido.id, db.peticion.cantidad, db.productoXpedido.granel)

        for row in peticiones:
            q = row.peticion.cantidad or 0
            self.vars['cantidad_%d'%row.productoXpedido.id] = q if row.productoXpedido.granel else int(q)

    def accepts(
        self,
        request_vars,
        session=None,
        ):
        db = current.db
        T  = current.T
        if request_vars.__class__.__name__ == 'Request':
            request_vars=request_vars.post_vars
        self.errors.clear()
        self.request_vars = Storage()
        self.request_vars.update(request_vars)
        ret = FORM.accepts(
            self,
            request_vars,
            session,
            keepvalues=True
            )
        if not ret:
            self.cambios_productos = False
            return False

        self.cambios_productos = True

        ps_granel = []
        for k,v in request_vars.items():
            if k.startswith('cantidad_'):
                try:
                    # TODO ¿es mejor if int(v)!=v ? No creo, el javascript ya ha hecho su trabajo,
                    # aquí no debería llegar 3.0
                    # esta es una comprobación adicional, por seguridad
                    if ('.' in v) or (',' in v):
                        ippp = int(k.split('_')[1])
                        ps_granel.append(ippp)
                    if v: v=Decimal(v)
                except:
                    self.errors[k] = T('Por favor escribe un número válido')
        s = db((db.productoXpedido.id.belongs(ps_granel)) &
               (db.productoXpedido.granel==False)).select()
        if s:
            for row in s:
                name = 'cantidad_%d'%row.id
                self.errors[name] = T('Este producto no se puede pedir a granel')

        if self.errors:
            self.feedback = T('Ha habido problemas al procesar tu petición')
        else:
            # Limpia la peticion de los productos de los pedidos abiertos
            db( db.peticion.productoXpedido.belongs(self.ppps) &
               (db.peticion.grupo==self.igrupo) &
               (db.peticion.persona ==self.ipersona)  ).delete()

            # Registra la nueva peticion
            pide_de_pedido_abierto = False
            pide_de_pedido_cerrado = False
            for k,v in request_vars.items():
                #Es posible para un usuario "malicioso" pedir productos coordinados
                #que su grupo ha decidido no pedir
                #Aquí podría evitarlo, pero es tontería...
                if k.startswith('cantidad_'):
                    _, ppp = k.split('_')
                    ippp = int(ppp)
                    v = Decimal(v.replace(',','.') if v else Decimal())
                    if v and (ippp in self.set_ppps):
                        pide_de_pedido_abierto = True
                        db.peticion.insert(productoXpedido=ippp, grupo=self.igrupo,
                                           persona=self.ipersona, cantidad=v)
                    # Ha intentado pedir de algún producto de un pedido que se ha cerrado
                    # después de enviar la página de pedir
                    elif v:
                        pide_de_pedido_cerrado = True
            if pide_de_pedido_cerrado and not pide_de_pedido_abierto:
                self.feedback = T('Lo sentimos, pero el pedido se cerró poco antes de que enviaras'\
                                  ' tu petición y tu pedido no se ha registrado')
            elif pide_de_pedido_cerrado:
                # feedback si hay pedidos_cerrados (pero no todos)
                self.feedback =  'Atención, algunos pedidos se cerraron justo antes de que enviaras tu petición,' \
                ' y tu pedido no se ha registrado. Tus peticiones de los pedidos abiertos sí se han registrado'
            else:
                if len(self.pedidos)==1:
                    #Un solo
                    self.feedback =  (T('Pedido de %s registrado correctamente')%
                    db.pedido(tuple(self.pedidos)[0]).productor.nombre)
                else:
                    self.feedback =  T('Pedido registrado correctamente')


            for ipedido in self.pedidos:
                peticion_a_item(ipedido, self.igrupo, self.unidad)
        return True
