# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################
from decimal import Decimal
from collections import defaultdict

from gluon.html import P
from gluon import current
try:#web2py >=2.9.12
   from gluon.dal.objects import Table
except:#web2py <=2.9.11
   from gluon.dal import Table

from .kutils import represent
from .modelos.usuarios import es_admin_del_grupo, es_admin_o_miembro_del_grupo

from .contabilidad import TiposCuenta, TiposOperacion

############### Distintos tipos de contabilidad ###############
class TipoCuentaDerivada(object):
    '''Una cuenta derivada es una suma y/o resta de otras cuentas auténticas.

    Es ajena al sistema de doble entrada: la suma de las cuentas derivadas no tiene
    por que ser cero.
    '''
    def __init__(self, plantilla_descripcion, owner, table,
                 id_tipo_cuenta_der, param=None, ayuda=''):
        self.plantilla_descripcion = plantilla_descripcion
        self.owner = owner
        self.table = table
        self.param = param
        self.id_tipo_cuenta_der = id_tipo_cuenta_der
        self.ayuda = ayuda

    def identificadores_cuenta(self):
        nowner, towner = self.owner
        if self.table==towner:
            oid = (lambda f,id:id)
        else:
            ofield = self.table[nowner]
            oid = (lambda f,id: f[ofield.name])
        if self.param:
            nparam, tparam = self.param
            if self.table==tparam:
                pid = (lambda f,id:id)
            else:
                tfield = self.table[nparam]
                pid = (lambda f,id: f[tfield.name])
        else:
            pid = lambda f,id:None
        return oid, pid

    def id_cuenta_derivada(self, iowner, iparam):
        raise NotImplementedError

    def cuentas_a_seguir(self, iowner):
        raise NotImplementedError

    def recalcula_saldo_cuenta_derivada(self, iowner, iparam=None):
        'crea todos los apuntes necesarios en db.saldo_cuenta_derivada'
        db = current.db
        icuenta_der = self.id_cuenta_derivada(iowner, iparam)
        cuentas_a_seguir = self.cuentas_a_seguir(iowner)
        saldo = cantidad_operacion = Decimal()
        ultima_operacion = None
        for row in db( db.apunte.cuenta.belongs(cuentas_a_seguir) &
                      (db.apunte.operacion==db.operacion.id)
                        ).select(db.apunte.ALL, db.operacion.moneda, orderby=db.apunte.operacion):
            ioperacion = row.apunte.operacion
            cantidad = row.apunte.cantidad
            if row.operacion.moneda!=iparam:
                pass
            elif ioperacion != ultima_operacion:
                if cantidad_operacion and ultima_operacion:
                    saldo += cantidad_operacion
                    db.saldo_cuenta_derivada.insert(
                        operacion=ultima_operacion, cuenta_derivada=icuenta_der,
                        cantidad=cantidad_operacion, saldo=saldo
                    )
                ultima_operacion = ioperacion
                cantidad_operacion = cantidad
            else:
                cantidad_operacion += cantidad
        #Falta anotar el  ultimo valor del saldo
        if cantidad_operacion:
            saldo += cantidad_operacion
            db.saldo_cuenta_derivada.insert(
                operacion=ultima_operacion, cuenta_derivada=icuenta_der,
                cantidad=cantidad_operacion, saldo=saldo
            )
        db(db.cuenta_derivada.id==icuenta_der).update(saldo=saldo)

    def creador_cuentas(self):
        oid, pid = self.identificadores_cuenta()
        dbc = current.db.cuenta_derivada
        def func(f, id):
            iowner, iparam = oid(f,id), pid(f,id)
            dbc.update_or_insert(
                (dbc.tipo==self.id_tipo_cuenta_der) &
                (dbc.owner == iowner) &
                (dbc.param == iparam),
                tipo=self.id_tipo_cuenta_der, owner=iowner, param=iparam, activa=True
            )
            self.recalcula_saldo_cuenta_derivada(iowner, iparam)
        return func

    def desactivador_cuentas(self):
        '''Desactiva la cuenta si se desactiva el registro responsable
        de la creacion de esta cuenta
        '''
        oid, pid = self.identificadores_cuenta()
        def func(s,f):
            if 'activo' in f:
                val = f['activo']
                for row in s.select():
                    current.db.cuenta_derivada(**dict(
                        tipo=self.id_tipo_cuenta_der,
                        owner = oid(row,row.id),
                        param = pid(row,row.id)
                    )).update_record(activa=val)
                #TODO: si la cuenta se reactiva, crea todos los apuntes que falten
                #en db.saldo_cuenta_derivada
        return func

    def desactiva_cuenta_si_se_desactiva_owner(self):
        db = current.db
        _, towner = self.owner
        tc = self.id_tipo_cuenta_der
        def func(s,f):
            if 'activo' in f:
                val = f['activo']
                db((db.cuenta_derivada.tipo==tc) &
                    db.cuenta_derivada.owner.belongs(s._select(towner.id))
                ).update(activa=val)
                #TODO: si la cuenta se reactiva, crea todos los apuntes que falten
                #en db.saldo_cuenta_derivada
        return func

    def desactiva_cuenta_si_se_desactiva_param(self):
        db = current.db
        _, tparam = self.param
        tc = self.id_tipo_cuenta_der
        def func(s,f):
            if 'activo' in f:
                val = f['activo']
                cambio = db((db.cuenta_derivada.tipo==tc) &
                             db.cuenta_derivada.param.belongs(s._select(tparam.id))
                ).update(activa=val)
                if cambio:
                    pass
                    #TODO: si la cuenta se reactiva, crea todos los apuntes que falten
                    #en db.saldo_cuenta_derivada
        return func

    def ajustador_saldo(self):
        db = current.db
        dbapunte = db.apunte
        cuenta_derivada_para_apunte = self.cuenta_derivada_para_apunte
        def func(f, iapunte):
            #TODO: refactor con un join
            apunte = dbapunte(iapunte)
            ioperacion = apunte.operacion
            cantidad   = apunte.cantidad
            cuenta_der = cuenta_derivada_para_apunte(apunte)
            if cuenta_der:
                rsaldo = db.saldo_cuenta_derivada(operacion=ioperacion,
                                                  cuenta_derivada=cuenta_der.id)
                if rsaldo:
                    nueva_cantidad = rsaldo.cantidad + cantidad
                    nuevo_saldo = rsaldo.saldo + cantidad
                    if nueva_cantidad:
                        rsaldo.update_record(cantidad=nueva_cantidad, saldo=nuevo_saldo)
                    else:
                        rsaldo.delete_record()
                else:
                    nuevo_saldo = cuenta_der.saldo + cantidad
                    db.saldo_cuenta_derivada.insert(
                        operacion=ioperacion, cuenta_derivada=cuenta_der.id,
                        cantidad=cantidad, saldo=nuevo_saldo
                    )
                cuenta_der.update_record(saldo=nuevo_saldo)
        return func

    def register_callbacks(self):
        table = self.table
        table._after_insert.append(self.creador_cuentas())
        #al desactivar el registro que pario la cuenta, es necesario desactivar las
        #cuentas con otro callback
        table._after_update.append(self.desactivador_cuentas())
        #y tb si se esfuman el owner o el param.
        nowner, towner = self.owner
        if isinstance(towner, Table) and towner!=table:
            towner._after_update.append(self.desactiva_cuenta_si_se_desactiva_owner())
        if self.param:
            nparam, tparam = self.param
            tparam._after_update.append(self.desactiva_cuenta_si_se_desactiva_param())
        #Escucha a cada apunte en db.apunte, si es una de las cuentas que componen la
        #cuenta derivada, hacemos el apunte en db.saldo_cuenta_derivada
        current.db.apunte._after_insert.append(self.ajustador_saldo())

    def descripcion(self, iowner, iparam):
        db = current.db
        owner_name, owner_type = self.owner
        owner_value = (represent(owner_type, db[owner_type](iowner))
                       if isinstance(owner_type, Table) else
                       iowner)
        d = {owner_name: owner_value}
        if self.param:
            param_name, param_type = self.param
            param_value = (represent(param_type, db[param_type](iparam))
                            if isinstance(param_type, Table) else
                            iparam or '')
            d[param_name] = param_value
        return self.plantilla_descripcion%d

class CajaGrupo(TipoCuentaDerivada):
    '''La caja de un grupo en una moneda concreta: es la suma de la cuenta GRUPO
    mas las cuentas UNIDAD_GRUPO con el dinero que las unidades adelantaron (o el dinero que deben)

    Para una red, la caja es la suma de la cuenta GRUPO mas las cuentas GRUPO_RED con el
    dinero que los grupos adelantaron (o el dinero que deben)
    '''
    def __init__(self):
        db = current.db
        T  = current.T
        TipoCuentaDerivada.__init__(self,
            T('Caja de %(grupo)s en %(moneda)s'),
            ('grupo', db.grupo),
            db.grupoXmoneda,
            TiposCuentaDerivada.CAJA_GRUPO,
            param = ('moneda', db.moneda)
        )

    def id_cuenta_derivada(self, igrupo, imoneda):
        return current.db.cuenta_derivada(tipo=self.id_tipo_cuenta_der,
                                          owner=igrupo,
                                          param=imoneda).id

    def cuentas_a_seguir(self, igrupo):
        db = current.db
        tipo_cuenta_depositos = (TiposCuenta.GRUPO_RED if db.grupo(igrupo).red else
                                 TiposCuenta.UNIDAD_GRUPO)
        return [r.id for r in
            db(((db.cuenta.tipo==tipo_cuenta_depositos) &
                (db.cuenta.target==igrupo)) |
               ((db.cuenta.tipo==TiposCuenta.GRUPO) &
                   (db.cuenta.owner==igrupo) )
              ).select(db.cuenta.id)]

    def cuenta_derivada_para_apunte(self, apunte):
        db = current.db
        dbcuenta_der = db.cuenta_derivada
        icuenta = apunte.cuenta
        cuenta  = db.cuenta(icuenta)
        param  = db.operacion(apunte.operacion).moneda
        tipo = cuenta.tipo
        if tipo in [TiposCuenta.UNIDAD_GRUPO, TiposCuenta.GRUPO_RED]:
            igrupo = cuenta.target
            red = db.grupo(igrupo).red
            if ((red and tipo==TiposCuenta.UNIDAD_GRUPO) or
                (not red and tipo==TiposCuenta.GRUPO_RED)):
                return
        elif tipo==TiposCuenta.GRUPO:
            igrupo = cuenta.owner
        else:
            return
        #Si la cuenta_derivada no está activa, no hacemos apuntes
        return dbcuenta_der(tipo=self.id_tipo_cuenta_der,
                            owner=igrupo,
                            param=param,
                            activa=True)

class SaldoGrupo(TipoCuentaDerivada):
    '''El saldo del grupo no esta desglosado por moneda: no sabemos en que moneda las
    unidades pagaran las deudas, ni en que moneda pagaremos a productores.

    Es la suma de la cuenta GRUPO mas las cuentas GRUPO_PRODUCTOR (lo que el grupo ha
    adelantado, o lo que debe a los productores) y las cuentas GRUPO_RED (lo que el grupo
    ha adelantado, o lo que debe a los productores)

    Si el grupo es una red, todo funciona igual, salvo que no se suman las cuentas
    GRUPO_RED, porque una red no pertenece a otras redes (todavía: 19-03-16)
    '''
    def __init__(self):
        db = current.db
        T  = current.T
        TipoCuentaDerivada.__init__(self,
            T('Saldo de %(grupo)s'),
            ('grupo', db.grupo),
            db.grupo,
            TiposCuentaDerivada.SALDO_GRUPO
        )

    def cuenta_derivada_para_apunte(self, apunte):
        db = current.db
        icuenta = apunte.cuenta
        cuenta  = db.cuenta(icuenta)
        tipo    = cuenta.tipo
        igrupo  = cuenta.owner
        if ( (tipo not in [TiposCuenta.GRUPO, TiposCuenta.GRUPO_PRODUCTOR, TiposCuenta.GRUPO_RED]) or
             ((tipo==TiposCuenta.GRUPO_RED) and db.grupo(igrupo).red  ) ):
             return
        #Si la cuenta_derivada no está activa, no hacemos apuntes
        return db.cuenta_derivada(tipo=self.id_tipo_cuenta_der,
                                  owner=igrupo,
                                  activa=True)

    def id_cuenta_derivada(self, iowner, iparam):
        return current.db.cuenta_derivada(tipo=self.id_tipo_cuenta_der,
                                          owner=iowner).id

    def cuentas_a_seguir(self, igrupo):
        db = current.db
        lista_tipos_cuenta = [TiposCuenta.GRUPO, TiposCuenta.GRUPO_PRODUCTOR]
        if not db.grupo(igrupo).red:
            lista_tipos_cuenta.append(TiposCuenta.GRUPO_RED)
        return [r.id for r in
            db((db.cuenta.owner==igrupo) &
                db.cuenta.tipo.belongs(lista_tipos_cuenta)
              ).select(db.cuenta.id)]

class TiposCuentaDerivada(dict):
    CAJA_GRUPO      = 1
    SALDO_GRUPO     = 2

    def __init__(self):
        self.update({
            self.CAJA_GRUPO: CajaGrupo(),
            self.SALDO_GRUPO: SaldoGrupo(),
        })

    def register_callbacks(self):
        '''Callbacks para crear cuentas siempre que se cree un registro
        que necesita una o más cuentas, y para desactivar las cuentas
        cuando el registro desaparece o se desactiva'''
        for tc in self.values():
            tc.register_callbacks()
