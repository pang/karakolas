# -*- coding: utf-8 -*-
from gluon import current
import shutil
from os import path
import os
import zipfile
from gluon.contrib.appconfig import AppConfig

UPLOADS_PATH = path.join('applications', current.request.application, 'uploads')


def cleandata():
    db = current.db
    tablas = list(db.tables)
    for tabla in tablas:
        db(db[tabla]).delete()
    db.commit()
    if path.exists(UPLOADS_PATH):
        shutil.rmtree(UPLOADS_PATH)
    bayes_pickle_path = os.path.join(current.request.folder,'private','bayes.pickle')
    if path.exists(bayes_pickle_path):
        os.remove(bayes_pickle_path)


def post_import_sql_data():
    '''Pequeños ajustes después de importar una base de datos de un fichero .sql
    '''
    db = current.db
    myconf = AppConfig()

    # corrección de la fecha de reparto para evitar pedidos abandonados
    # se avanzan los pedidos abiertos, tantos días contando desde hoy como pasaron
    # desde la apertura hasta el cierre del pedido
    # Los pedidos cerrados no se modifican
    db.executesql('update pedido set '
                  'fecha_reparto=ADDDATE(CURRENT_DATE(),INTERVAL datediff(fecha_reparto, fecha_activacion) DAY) '
                  'where abierto=\'T\''
                  ' and NOW()>fecha_reparto')
    db.executesql('update pedido p join proxy_pedido pp ON pp.pedido =p.id  set pp.fecha_reparto=p.fecha_reparto')
    #Adela y Jordi usan sql, pang usa pydal ;-)
    # el password de los usuarios se lee desde appconfig.ini
    db(db.auth_user).update(
        password=str(db.auth_user.password.validate(
            myconf.take('global.user_password_import_data')
        )[0])
    )
    # el password de los usuarios admin tb se lee desde appconfig.ini, y es distinto del anterior
    admin_ids = [row.id for row in
            db((db.auth_user.id==db.auth_membership.user_id) &
               (db.auth_membership.group_id==db.auth_group.id) &
               (db.auth_group.role=='webadmins')
               ).select(db.auth_user.id)]
    db(db.auth_user.id.belongs(admin_ids)).update(
        password=str(db.auth_user.password.validate(
            myconf.take('global.admin_password_import_data')
        )[0])
    )
    # Borramos el bayes.pickle anterior porque ahora no funcionará
    #  - si hay un bayes.pickle en esa carpeta, lo importamos
    #  - si no lo hay, se regenerará
    bayes_pickle_path = path.join(
        current.request.global_settings.gluon_parent,
        'applications', current.request.application, 'private', 'bayes.pickle')
    bayes_pickle_testdata_path = path.join(
        current.request.global_settings.gluon_parent,
        'applications', current.request.application, 'private', 'testdata', 'bayes.pickle')
    if path.exists(bayes_pickle_path):
        os.remove(bayes_pickle_path)
    if bayes_pickle_testdata_path and path.exists(bayes_pickle_testdata_path):
        from adivinar_categorias import Bayes_Filter
        bayes = Bayes_Filter()
        # foreign=True es necesario porque este bayes.pickle puede venir de una base de datos distinta
        # con otros ids, incluso con otras categorías, que se identifican por el código.
        bayes.pickle_load(bayes_pickle_testdata_path, foreign=True)
        bayes.pickle_dump()

    db.commit()

# Si el módulo se llama como un script, es parte del proceso de importación de un sql,
# adjuntos y ajustes posteriores.
if __name__=='__main__':
    post_import_sql_data()
