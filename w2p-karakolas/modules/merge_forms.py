# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

from gluon.html import FORM, DIV

class MERGED_FORM(FORM):
    form_keys = ['_action', '_enctype', '_method']

    def __init__(self, *forms,
                 **attributes):
        FORM.__init__(self, *forms, **attributes)
        #TODO: admite prefixes como argumentos, para separar las ids de los campos que puedan tener igual nombre
#        self.components = []
#        for f in forms:
#            try:
#                self.components.append(DIV(*f.components, **f.attributes))
#            except AttributeError:
#                self.components.append(f)
        cleaned = self.clean(self)
        self.components = cleaned.components
        self.attributes = cleaned.attributes

    def clean(self, elm):
        if hasattr(elm, 'tag') and elm.tag=='form':
            new_components = [self.clean(c) for c in elm.components]
            attrs = dict((k,v) for k,v in elm.attributes.items()
                         if k not in MERGED_FORM.form_keys )
            return DIV(*new_components, **attrs)
        try:
            elm.components = [self.clean(c) for c in elm.components]
            return elm
        except:
            return elm

    def validate(self,**kwargs):
        raise NotImplementedError

    def accepts(
        self,
        request_vars,
        session=None,
        formname='default',
        keepvalues=False,
        onvalidation=None,
        hideerror=False,
        **kwargs
        ):
        raise NotImplementedError

