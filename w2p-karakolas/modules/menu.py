# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

from gluon import current
from gluon.html import URL, A

from .contabilidad import OpcionIngresos
from .modelos.grupos import redes_del_grupo

def group_menu(role):
    db = current.db
    T = current.T
    if role == 'webadmins':
        opts = {} if current.request.is_local else dict(scheme='https', host=True)
        return (T('Webadmin'), False,
                URL(c='webadmin', f='index.html', **opts), [
               ('', False, A(
                    T('Categorías'),
                    _href=URL(c='webadmin', f='grid_all_categorias.html', **opts),
                    _target='_blank'),
                    []),
               ('', False, A(
                    T('Grupos'),
                    _href=URL(c='webadmin', f='index.html', **opts),
                    _target='_blank'),
                    []),
               ('', False, A(
                    T('Nuevo grupo'),
                    _href=URL(c='webadmin', f='add_group.html', **opts),
                    _target='_blank'),
                    []),
               ('', False, A(
                    T('Productoras/es'),
                    _href=URL(c='webadmin', f='grid_all_productores.html', **opts),
                    _target='_blank'),
                    []) ,
                ] )

    s,k = role.split('_')
    id = int(k)
    if s == 'user':
        return ()
    ngrupo = db.grupo(id).nombre
    es_red = db.grupo(id).red
    if es_red and s == 'admins':
        ls = [ (T('Administración'), False,
                URL(c='redes', f='admin_red', vars=dict(red=id))),
               (T('Configuración'), False,
                         URL('grupo','editar_datos_grupo.html', vars=dict(grupo=id))),
               (T('Productoras/es'), False,
                     URL('productores','index.html', vars=dict(grupo=id))),
               (T('Gestión de pedidos'), False,
                URL(c='gestion_pedidos', f='gestion_pedidos.html', vars=dict(grupo=id))),
               (T('Estadisticas'), False,
                URL(c='estadisticas',f='stat_red.html', vars=dict(red=id))),
               (T('Archivos de la red'), False,
                URL(c='ficheros',f='index.html', vars=dict(grupo=id)))
             ]
        if db.grupo(id).opcion_ingresos>0:
            ls.insert(-1, (T('Contabilidad'), False,
                URL(c='contabilidad', f='resumen_grupo.html', vars=dict(grupo=id)))
            )
        return (ngrupo, False,
                URL(c='redes', f='admin_red', vars=dict(red=id)), ls)
    elif s == 'admins':
        menu_datos    = (T('Configuración del grupo'), False,
                         URL('grupo','editar_datos_grupo.html', vars=dict(grupo=id)))
        menu_personas = (T('Personas'), False,
                         URL('grupo','admin_grupo.html', vars=dict(grupo=id)))
        menu_prods    = (T('Productoras/es'), False,
            URL('productores','index.html', vars=dict(grupo=id)))
        menu_pedidos  = (T('Gestión de pedidos'), False,
            URL(c='gestion_pedidos', f='gestion_pedidos.html', vars=dict(grupo=id)))
        menu_stats    = (T('Estadisticas'), False,
            URL(c='estadisticas',f='stat_grupo.html', vars=dict(grupo=id)))
        menu_ficheros = (T('Archivos del grupo'), False,
            URL(c='ficheros',f='index.html', vars=dict(grupo=id)))


        ls = [menu_datos, menu_personas, menu_prods, menu_pedidos, menu_ficheros, menu_stats]
        if db((db.grupoXred.grupo==id) & (db.grupoXred.activo==True)).count():
            ls.insert(3, (T('Archivos y productores en red del grupo'), False,
                URL(c='grupo', f='redes.html', vars=dict(grupo=id)))
            )
        if db.grupo(id).opcion_ingresos>0:
            ls.insert(-1, (T('Contabilidad'), False,
                URL(c='contabilidad', f='resumen_grupo.html', vars=dict(grupo=id)))
            )
        return ('Admin %s'%ngrupo, False, '', ls)
    elif s == 'miembros':
        ls = [(T('Personas'), False,
                  URL(c='grupo', f='info_personas.html', vars=dict(grupo=id))),
                 (T('Productoras/es'), False,
                  URL(c='productores', f='info_productores.html', vars=dict(grupo=id))),
                 (T('Pedir'), False,
                  URL(c='pedir', f='pedidos_activos.html', vars=dict(grupo=id))),
                 (T('Consulta tus pedidos'), False,
                  URL(c='pedir', f='consulta.html', vars=dict(grupo=id))),
                 (T('Consultar los pedidos del grupo'), False,
                  URL(c='pedir', f='informes.html', vars=dict(tipo_pedidos='cerrados', grupo=id, ))),
                 (T('Archivos del grupo'), False,
                  URL(c='ficheros', f='index.html', vars=dict(grupo=id))),
                 (T('Estadisticas'), False,
                  URL(c='estadisticas', f='index.html', vars=dict(grupo=id) )),
                 ]
        if db.grupo(id).opcion_ingresos != OpcionIngresos.NO_CONT:
            ls.insert(-1, (T('Contabilidad'), False,
                URL(c='contabilidad', f='resumen_grupo.html', vars=dict(grupo=id)))
            )
        return (ngrupo, False, '', ls)
    else:
        return []

def build_menu():
    db = current.db
    request = current.request
    auth = current.auth
    T = current.T

    if not auth.user:
        return [(T('Entra en karakolas'), False, URL('default','user.html',args=['login'])),
    ]

    opciones = [(T('Home'), False, '',
                 [(T('Resumen'), False, URL('default','home.html')),
                  (T('Listas de espera'), False, URL('default','listas_de_espera.html'))]
                )]
    if not db((db.personaXgrupo.persona==auth.user_id) &
              (db.personaXgrupo.activo==True) &
              (db.personaXgrupo.grupo==db.grupo.id) &
              (db.grupo.opcion_ingresos>0)
             ).isempty():
        opciones[0][-1].append((T('Contabilidad'), False,
                                URL(c='contabilidad', f='resumen_persona.html'))
        )

    id = auth.user.id
    #si ordenamos por db.auth_group.description, aparecerían primero las entradas de Admin,
    #y luego los grupos, y si el user es miembro de un grupo que va antes que admin por orden
    #cronológico se colaría el menu de miembro de ese grupo antes que las entradas de Admin
    grupos = db((db.auth_membership.user_id==id) &
                (db.auth_membership.group_id==db.auth_group.id)) \
            .select(db.auth_group.ALL, orderby=db.auth_group.id)

    if not any((g.role.startswith('admins') or
                 g.role.startswith('red') or
                 g.role.startswith('miembros') or
                 g.role == 'webadmins')
                 for g in grupos):
        return opciones
    tus_grupos_de_consumo = set()
    for g in grupos:
        gmenu = group_menu(g.role)
        if gmenu:
            opciones.append(gmenu)
            if g.role.startswith('miembros_') or g.role.startswith('admins_'):
                s,k = g.role.split('_')
                id = int(k)
                if s == 'user':
                    return []
                ngrupo = db.grupo(id).nombre
                tus_grupos_de_consumo.add((id, ngrupo))
    #Podemos ordenar los menús de documentos por orden alfabetico del nombre del grupo:
    #sorted(tus_grupos_de_consumo, key=lambda s:s[1])
    #pero dejo el orden cronologico
    for gid, gnombre in sorted(tus_grupos_de_consumo):
        s = db((db.allfiles.grupo==gid) &
               (db.allfiles.destacado==True)).select()
        if s:
            opciones.append((T('Documentos %s')%gnombre,False, '', [
                (row.filename, False,
                URL(c='ficheros',f='vista.html',vars=dict(grupo=gid,path=row.filepath)),
                [] )
                for row in s]
            ))
    tus_redes_ids = set()
    for gid,_ in tus_grupos_de_consumo:
        tus_redes_ids.update(redes_del_grupo(gid))
    #No añadimos las redes de las que el user es admin porque tienen su propio menú dedicado
    tus_redes_ids.difference_update(gid for gid,_ in tus_grupos_de_consumo)
    tus_redes = [(row.id, row.nombre) for row in
        db(db.grupo.id.belongs(tus_redes_ids)).select(db.grupo.id, db.grupo.nombre)
    ]
    documentos_redes = {}
    for rid, rnombre in sorted(tus_redes):
        s = db((db.allfiles.grupo==rid) &
               (db.allfiles.destacado==True)).select()
        if s:
            documentos_redes[rnombre] = [
                (row.filename, False,
                 URL(c='ficheros',f='vista.html',vars=dict(grupo=rid,path=row.filepath)))
                for row in s ]
    if len(documentos_redes)==1:
        rnombre, documentos = list(documentos_redes.items())[0]
        opciones.append((T('Documentos %s')%rnombre, False, '', documentos))
    elif documentos_redes:
        entradas_menu = []
        for rnombre, documentos in documentos_redes.items():
            entradas_menu.append((rnombre, False,
                dict(_role="presentation", _class="dropdown-header")))
            entradas_menu.extend(documentos)
        opciones.append((T('Documentos de tus redes'), False, '', entradas_menu))
    opciones.append(('Ayuda', False, '',
               [ ('', False, A(
                    T('Ayuda de esta página'),
                    _href='',
                    _onclick="App.ayuda.show();return false;") ),
                 ('', False, A(
                    T('Tutoriales básicos'),
                    _href='http://karakolas.org/tutoriales/usuarias/',
                    _target='_blank') ),
                 ('', False, A(
                    T('Tutoriales avanzados'),
                    _href='http://karakolas.org/tutoriales/usuarias-avanzado/',
                    _target='_blank') )
               ]
           ))

    return opciones

def build_user_menu(uid):
    T = current.T
    if not uid:
        return [(T('Usuaria'), False, '', [
        (T('Entra en karakolas'), False, URL('default','user.html',args=['login'])),
        (T('Sign up'), False, URL('default','user.html',args=['register'])),
        (T('Lost password?'), False, URL('default','user.html',args=['request_reset_password'])),
        (T('Forgot username?'), False, URL('default','user.html',args=['retrieve_username'])),
            ])]
    else:
        return [(T('Usuaria'), False, '', [
        (T('Log Out'), False, URL('default','user.html',args=['logout'])),
        (T('Profile'), False, URL('default','user.html',args=['profile'])),
        (T('Password'), False, URL('default','user.html',args=['change_password'])),
            ])]
