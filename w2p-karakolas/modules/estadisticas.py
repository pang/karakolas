# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

from gluon.html import URL, FORM, A, SPAN, CAT, P
from gluon import current
from collections import defaultdict

from .kutils import random_string
from .jqplot import JQPlot
from .rsheet import Tabular, ROSheet

def multistat_ods(stats_list, filename):
    import os.path
    from uuid import uuid4
    from .ods_utils import ods_write

    tmpfilename = os.path.join(current.request.folder,'private','temp_files','deleteme_' + str(uuid4()))

    ods_write(dict((estadistica.descripcion.decode('utf8'), estadistica.tabla())
        for estadistica in stats_list
        ), tmpfilename)

    return current.response.stream(tmpfilename, 4000, attachment=True, filename=filename)


class Estadistica():
    WITH_JQPLOT = 1
    WITH_ODS    = 2
    WITH_TABLE  = 4

    def __init__(self, data,
                 descripcion='',
                 options=WITH_JQPLOT|WITH_ODS|WITH_TABLE,
                 rosheet_options=None,
                 jqplot_options=None,
                 ods_options=None):
        '''Distintas formas de visualizar una relacion (row,col) -> valor
        '''
        self.data = data
        self.descripcion = descripcion
        self.isTrivial = data.isTrivial
        self.options = options
        if options & self.WITH_JQPLOT:
            self.jqplot = JQPlot(data, **(
                jqplot_options or
                dict(_id=('jqplot_'+random_string()))
            ) )
        if options & self.WITH_TABLE:
            self.rosheet = ROSheet(data, **(rosheet_options or dict()))
        if options & self.WITH_ODS:
            self.ods_options = ods_options or {}

    @staticmethod
    def add_static_files(options=WITH_JQPLOT|WITH_ODS|WITH_TABLE):
        if options & Estadistica.WITH_JQPLOT:
            JQPlot.add_static_files()
        if options & Estadistica.WITH_TABLE:
            ROSheet.add_static_files()

    def tabla(self):
        from .exportador import Formula, MatrixFormula, ref

        data = self.data
        if self.ods_options.get('no_totales'):
            tabla = [[''] + data.col_names]
            tabla.extend( [[rname] +
            [data.table_data[r,c] for c in data.col_ids]
            for r, rname in zip(data.row_ids, data.row_names)])
        else:
            tabla = [['', 'TOTALES'] + data.col_names]
            tabla.extend( [[rname] +
                           [Formula('SUM(%s:%s)'%(
                                   ref(j+1,2), ref(j+1,len(data.col_ids)+1)
                                   ))] +
                           [data.table_data[r,c] for c in data.col_ids]
                          for j, (r, rname) in enumerate(zip(data.row_ids, data.row_names))])
        return tabla

    def ods(self, filename):
        import os.path
        from uuid import uuid4
        from .ods_utils import ods_write_sheet

        tabla = self.tabla()

        tmpfilename = os.path.join(current.request.folder,'private','temp_files','deleteme_' + str(uuid4()))
        ods_write_sheet(tabla, tmpfilename)
        return current.response.stream(tmpfilename, 4000, attachment=True, filename=filename)

    def ods_link(self, **kwds):
        request = current.request
        d = self.ods_options or {}
        d.update(request.vars)
        d.update(kwds)
        return URL(c=request.controller, f=request.function,
                   vars=d, extension='ods')

    def xml(self):
        if self.isTrivial:
            return P(current.T('No hay datos que mostrar')).xml()
        ls = []
        if self.options & self.WITH_JQPLOT:
            ls.append(self.jqplot)
        if self.options & self.WITH_TABLE:
            ls.append(self.rosheet)
        if self.options & self.WITH_ODS:
            ls.append(A(
                SPAN(**{'_class':'glyphicon glyphicon-download', '_aria-hidden':'true'}),
                current.T('Descargar'),
                _href=self.ods_link(),
                _class='btn btn-info'
            ))
        return CAT(*ls).xml()
