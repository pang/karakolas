# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################

from gluon import current
from gluon.html import LINK, SCRIPT, CAT, URL

def add_libs(files):
    #Podría ocurrir que se llame a esta funcion varias veces con response.files como
    #argumento, así que limpiamos la lista para no repetirnos
    return SCRIPT('Loader.getScripts([[%s]])'%','.join(
        '"%s"'%f for f in files
    ))

