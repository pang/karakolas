from gluon import current
import shutil
from os import path
import os
import zipfile

UPLOADS_PATH = path.join('applications', current.request.application, 'uploads')

def exportdata(folder, filename):
    db = current.db
    db.export_to_csv_file(open(path.join(folder, filename + '.csv'), 'w', encoding='utf-8', newline=''))
    shutil.make_archive(
        path.join(folder, filename + '_attachments'),
        'zip',
        UPLOADS_PATH)

def importdata(folder, filename):
    from gluon.contrib.appconfig import AppConfig
    myconf = AppConfig()
    db = current.db
    for tabla in db.tables:
        db[tabla]._after_insert.clear()

    db.import_from_csv_file(open(path.join(folder, filename + '.csv'), 'r', encoding='utf-8', newline=''))
    db.commit()
    # corrección description con los nuevos ids autogenerados
    db.executesql('update auth_group ag join  grupo g  on  ag.description like concat(\'%\',g.nombre ) '
                  'set role =concat(substring(ag.role,1, LOCATE(\'_\', ag.role) ), g.id)')
    # corrección de la fecha de reparto para evitar pedidos abandonados
    # se avanzan los pedidos abiertos, tantos días contando desde hoy como pasaron
    # desde la apertura hasta el cierre del pedido
    # Los pedidos cerrados no se modifican
    db.executesql('update pedido set '
                  'fecha_reparto=ADDDATE(CURRENT_DATE(),INTERVAL datediff(fecha_reparto, fecha_activacion) DAY) '
                  'where abierto=\'T\''
                  ' and NOW()>fecha_reparto')
    db.executesql('update pedido p join proxy_pedido pp ON pp.pedido =p.id  set pp.fecha_reparto=p.fecha_reparto')
    # corrección de owners y target de cuenta y cuentaderivada con los nuevos ids autogenerados de grupo y productor
    db.executesql('update cuenta c join auth_user u on c.descripcion like concat(\'Cuenta de la usuaria \',u.username ) '
                  'set owner = u.id')
    db.executesql('update cuenta c join grupo g on c.descripcion like concat(\'Cuenta del grupo \',g.nombre,\'%\' ) '
                  'set owner = g.id')
    db.executesql('update cuenta c join grupo g on c.descripcion like concat(\'Cuenta de gastos del grupo \',g.nombre ) '
                  'set owner = g.id')
    db.executesql('update cuenta c join grupo g on c.descripcion like concat(\'Cuenta del grupo % en la red \',g.nombre ) '
                  'set target = g.id')
    db.executesql('update cuenta c join grupo g on c.descripcion like concat(\'Cuenta de la unidad % en el grupo \',g.nombre ) '
                  'set target = g.id')
    db.executesql('update cuenta c join grupo g on c.descripcion like concat(\'Saldo del grupo \',g.nombre,\'%\' ) '
                  'set owner = g.id')
    # buscar productor cruzando también con grupo porque puede haber el nombre del productor dos veces ya que puede estar en diferentes grupos
    db.executesql('update cuenta c join productor p on c.descripcion like concat(\'Saldo del grupo % con el productor \',p.nombre ) '
                  'set target = p.id '
                  'where p.grupo = c.owner')
    # Para las cuentas de ingresos totales del productor, coger el id del productor de la cuenta anterior ya que no podemos cruzarlo con productor con el nombre pq puede estar repetido
    db.executesql('update cuenta c join cuenta c1 on c1.id = c.id-1 '
                  'set c.owner = c1.target '
                  'where c.descripcion like \'Ingresos totales del productor %\'')

    db.executesql('update cuenta_derivada c join grupo g on c.descripcion like concat(\'Caja de \',g.nombre,\'%\' ) '
              'set owner = g.id')
    db.executesql('update cuenta_derivada c join grupo g on c.descripcion like concat(\'Saldo de \',g.nombre ) '
              'set owner = g.id')

    # corrección de los parametros de operación, que son el pedido, con los nuevos ids autogenerados de pedido
    db.executesql('update operacion o join pedido p on o.descripcion like concat(\'Cierre de Pedido de % del \',day(p.fecha_reparto),\'-\',month(p.fecha_reparto),\'-\',year(p.fecha_reparto) ) '
                  'set parametros = concat(\'|\',p.id,\'|\')')
    #Adela y Jordi usan sql, pang usa pydal
    # el password de los usuarios se lee desde appconfig.ini
    db(db.auth_user).update(
        password=str(db.auth_user.password.validate(
            myconf.take('global.user_password_import_data')
        )[0])
    )
    # el password de los usuarios admin tb se lee desde appconfig.ini, y es distinto del anterior
    admin_ids = [row.id for row in
            db((db.auth_user.id==db.auth_membership.user_id) &
               (db.auth_membership.group_id==db.auth_group.id) &
               (db.auth_group.role=='webadmins')
               ).select(db.auth_user.id)]
    db(db.auth_user.id.belongs(admin_ids)).update(
        password=str(db.auth_user.password.validate(
            myconf.take('global.admin_password_import_data')
        )[0])
    )
    # todos los archivos adjuntos, que aparecen como campos de tipo "file", están
    # en la carpeta uploads
    with zipfile.ZipFile(path.join(folder, filename + '_attachments.zip'), 'r') as zip_ref:
        zip_ref.extractall(UPLOADS_PATH)
    # Borramos el bayes.pickle anterior porque ahora no funcionará
    #  - si hay un bayes.pickle en esa carpeta, lo importamos
    #  - si no lo hay, se regenerará
    bayes_pickle_testdata_path = path.join(folder, 'bayes.pickle')
    bayes_pickle_path = path.join(
        'applications', current.request.application, 'private', 'bayes.pickle')
    if path.exists(bayes_pickle_path):
        os.remove(bayes_pickle_path)
    if path.exists(bayes_pickle_testdata_path):
        from adivinar_categorias import Bayes_Filter
        bayes = Bayes_Filter()
        bayes.pickle_load('applications/karakolas/private/testdata/bayes.pickle', foreign=True)
        bayes.pickle_dump()

    # ... y recuperamos las páginas especiales
    db(db.paginas_especiales).delete()
    db.import_from_csv_file(open(path.join(folder, 'paginas_especiales' + '.csv'), 'r', encoding='utf-8', newline=''))
    db.commit()

def cleandata():
    db = current.db
    for tabla in db.tables:
        db(db[tabla]).delete()
    db.commit()
    if path.exists(UPLOADS_PATH):
        shutil.rmtree(UPLOADS_PATH)
    bayes_pickle_path = os.path.join(current.request.folder,'private','bayes.pickle')
    if path.exists(bayes_pickle_path):
        os.remove(bayes_pickle_path)
