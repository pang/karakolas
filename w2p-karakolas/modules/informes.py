# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################

from encodings import codecs
import subprocess
from decimal import Decimal
from datetime import datetime
from time import time
from collections import defaultdict
import os
import re

from gluon import current

from .kutils import pprint_c, pprint_p, format_fecha
from .modelos.pedidos import coste_extra_pedido, recoge_y_ordena_item, recoge_y_ordena_peticion, \
                             recoge_y_ordena_item_por_grupos, explicaciones_discrepancias, TiposDiscrepancia
from .modelos.grupos import grupos_de_la_red, mapa_grupo
from .modelos.productores import ordena_productos

TEMPLATE_VERTICAL = r'''\documentclass[a4paper]{article}
\usepackage[scaled=FONT_SCALE]{helvet}
\renewcommand*\familydefault{phv}
\usepackage[utf8]{inputenc}
%\usepackage[top=1cm,bottom=1cm,left=1cm,right=1cm]{geometry}
\usepackage[margin=1cm]{geometry}
\usepackage{longtable}
\usepackage{eurosym}
\usepackage[table]{xcolor}
\usepackage{soul}
\usepackage{hhline}

\begin{document}
\rowcolors{1}{lightgray}{white}

tabla_latex
\end{document}
'''

TEMPLATE_APAISADO = r'''\documentclass[landscape, a4paper]{article}
\usepackage[scaled=FONT_SCALE]{helvet}
\renewcommand*\familydefault{phv}
\usepackage[utf8]{inputenc}
\usepackage[top=1cm,bottom=1cm,left=1cm,right=1cm]{geometry}
\usepackage{longtable}
\usepackage{eurosym}
\usepackage[table]{xcolor}
\usepackage{soul}
\usepackage{hhline}

%\setlength{\oddsidemargin}{0in}        % default=0in
%\setlength{\textwidth}{9in}        % default=9in
\addtolength{\textwidth}{40pt}        % default=9in
\addtolength{\topmargin}{-20pt}        % default=0.20in
\addtolength{\textheight}{40pt}        % default=5.15in
\addtolength{\leftmargin}{-20pt}        % default=???

\begin{document}
\rowcolors{1}{lightgray}{white}

tabla_latex
\end{document}
'''
# Excepto por los dos primeros, no podria explicar el valor del resto
# de las constantes; las he puesto tras ensayo y error :-/
PAGE_WIDTH       = 21.0
PAGE_HEIGHT      = 29.7
PROD_WIDTH       = 2.0
CELL_WIDTH_LARGE = 0.8
CELL_WIDTH       = 0.5
CELL_WIDTH_SHORT = 0.3
MULTIPLIER       = 1.8
#MARGIN           = 1.0
# Dificil acertar, se quejaron de que el tamaño era muy grande
# (con FONT_SCALE_BASE  = 0.6)  => commit 46ca5d20217a
# Ahora de que es pequeño (con FONT_SCALE_BASE  = 0.4)
# Antes hacía pruebas con pocos users y no apreciaba bien el efecto...
FONT_SCALE_BASE  = 0.55
FONT_SCALE_BIS   = 0.35
LINE_STRETCH_V   = '1.5'
LINE_STRETCH_A   = '1.6'

def ajusta_columnas(max_columnas_page,total_columnas):
    # ntablas      = int(ceil(float(len(unidades))/unidades_por_tabla))
    if total_columnas==0:
        return 1,0
    ntablas = total_columnas//max_columnas_page + (1 if total_columnas%max_columnas_page else 0)
    return total_columnas//ntablas, (ntablas+((1 if total_columnas%ntablas else 0)))

def reparte(ncolgroups, columnas_por_tabla):
    k = 0
    paginas = 1
    columnas_pagina_actual = 0
    while k < len(ncolgroups):
        columnas_pagina_actual += ncolgroups[k]
        if columnas_pagina_actual > columnas_por_tabla:
             paginas += 1
             columnas_pagina_actual = ncolgroups[k]
        k += 1
    return paginas

def ajusta_columnas_lista(ncolgroups, columnas_por_tabla):
    if not ncolgroups:
        return 1,0
    if columnas_por_tabla <= max(ncolgroups):
        columnas_por_tabla = max(ncolgroups)
        ntablas = reparte(ncolgroups, columnas_por_tabla)
        return columnas_por_tabla, ntablas
    ntablas = reparte(ncolgroups, columnas_por_tabla)
    ncolumnas = columnas_por_tabla-1
    while reparte(ncolgroups, ncolumnas)==ntablas:
        ncolumnas -= 1
    return ncolumnas+1, ntablas

#https://stackoverflow.com/questions/16259923/how-can-i-escape-latex-special-characters-inside-django-templates
def latex_escape(text):
    """
        :param text: a plain text message
        :return: the message escaped to appear correctly in LaTeX
    """
    conv = {
        '&': r'\&',
        '%': r'\%',
        '$': r'\$',
        '#': r'\#',
        '_': r'\_',
        '{': r'\{',
        '}': r'\}',
        '~': r'\textasciitilde{}',
        '^': r'\^{}',
        '\\': r'\textbackslash{}',
        '<': r'\textless{}',
        '>': r'\textgreater{}',
        '«': '"',
        '»': '"'
}
    regex = re.compile('|'.join(re.escape(key)
        for key in sorted(conv.keys(), key = lambda item: - len(item))))
    text_wo_latex_special = regex.sub(lambda match: conv[match.group()], text)
    return text_wo_latex_special.replace('\xc2\xa0','').replace('€','\\geneuro')

def tabla_totales(ipedidos, igrupo, apaisado, es_red=False):
    '''Recibe una lista de pedidos y un grupo, devuelve un str con latex para una tabla
       con dos partes:

    - La primera tiene una línea por cada productor propio, y una última de "Totales"
    - La segunda tiene una línea por cada red, agrupando todos los productores
    de la red.

    Al final se muestra una leyenda si es necesario con los símbolos que indican discrepancias.

    Esta tabla se usa como referencia para saber cuánto se debe pagar al productor, así
    que se usa siempre total_recibido, pero se indica que hay discrepancias.

    No se incluye discrepancia_descendiente porque no es relevante en una tabla de totales

    OLD: No se muestran cantidades alternativas, es tontería porque estamos agregando y no se podrían
    resolver las discrepancias, así que sólo se muestra un mensaje de advertencia.

    Febrero 2022: Incluímos también total repartido y total según la red, a petición de Ecomarca,
    porque muchos grupos ignoran las advertencias y las alertas de incidencias globales y pagan el
    total recibido, que es incorrecto.
    '''
    db = current.db
    tp = db.pedido
    tpp = db.proxy_pedido
    tig = db.item_grupo
    T  = current.T
    nombre_grupo = db.grupo(igrupo).nombre

    pedidos_recibido  = dict((ipedido,coste_extra_pedido(ipedido, igrupo, es_red))
                             for ipedido in ipedidos)
    pedidos_repartido = pedidos_recibido.copy()
    pedidos_red       = pedidos_recibido.copy()
    total_recibido  = tig.precio_recibido.sum()
    total_repartido = tig.precio_total.sum()
    total_red       = tig.precio_red.sum()
    q_productos = ((tig.grupo==igrupo) &
                   (tig.productoXpedido==db.productoXpedido.id) &
                    db.productoXpedido.pedido.belongs(ipedidos)
    )
    totales = db(q_productos).select(
        total_recibido, total_repartido, total_red,
        db.productoXpedido.pedido,
        groupby=db.productoXpedido.pedido
    )
    for row in totales:
        pedidos_repartido[row.productoXpedido.pedido] += row[total_repartido]
        pedidos_recibido[row.productoXpedido.pedido] += row[total_recibido]
        pedidos_red[row.productoXpedido.pedido] += row[total_red]

    # ¿Es necesario comprobar si un pedido no está en recibido, pero aún así está en
    #  repartido o en red? En la práctica, no lo creo ...
    ipedidos_no_vacios = [
        ipedido for ipedido, total in pedidos_recibido.items() if total>0
    ]
    # Los productores que son de alguna red no aparecen en la tabla de totales, se paga
    # todo junto a la red
    datos_productores_propios = db(
        tp.id.belongs(ipedidos_no_vacios) &
        (tp.productor == db.productor.id) &
        (db.productor.grupo==igrupo)
    ).select(tp.id, tp.discrepancia_interna,
             tp.discrepancia_interna_asumida, tp.discrepancia_productor,
             db.productor.nombre, db.productor.telefono, db.productor.grupo,
             orderby=db.productor.nombre)

    # Aquí los datos de los pedidos coordinados
    # Tienen que constar como de la red + 1, no de la red primigenia
    productores_redes = db(
        tpp.pedido.belongs(ipedidos_no_vacios) &
        (tpp.grupo == igrupo) &
        (tpp.via==db.grupo.id)
    ).select(tpp.pedido, tpp.discrepancia_interna,
             tpp.discrepancia_interna_asumida, tpp.discrepancia_ascendiente,
             db.grupo.id, db.grupo.nombre, db.grupo.telefono,
             orderby=db.grupo.nombre)

    if (not datos_productores_propios) and (not productores_redes):
        return ''

    # Ya tenemos los datos, ahora componemos la tabla latex.
    # dimensiones, comunes a las dos tablas
    width = PAGE_HEIGHT if apaisado else PAGE_WIDTH
    ncols = 7
    cell_width = width/(ncols + 1)
    LINE_STRETCH = LINE_STRETCH_A if apaisado else LINE_STRETCH_V
    #ls acumula las filas del texto latex
    #la primera fila es la cabecera
    ls = [
        r'{\renewcommand{\arraystretch}{'
        + LINE_STRETCH
        + r'}\begin{longtable}{|p{%.2fcm}|p{%.2fcm}|p{%.2fcm}|p{%.2fcm}|p{%.2fcm}|}'
          %(2*cell_width, cell_width, cell_width, cell_width, cell_width),
        r'\hline',
        r'&%s&%s&%s&%s\\'%(T('Teléfono'), T('Repartido'), T('Recibido'), T('Albarán'))
    ]

    d_explicaciones_discrepancias = explicaciones_discrepancias()
    simbolos_alerta = {
        TiposDiscrepancia.INTERNA :r'\dag',
        TiposDiscrepancia.INTERNA_ASUMIDA: r'\ddag',
        TiposDiscrepancia.PRODUCTOR: r'\spadesuit',
        TiposDiscrepancia.ASCENDIENTE: r'\clubsuit'
    }
    # tipos de discrepancia que pueden aparecer en un pedido propio
    # no se incluye discrepancia_descendiente porque no es relevante en una tabla de totales
    tablas_alerta = {
        TiposDiscrepancia.INTERNA: tp.discrepancia_interna,
        TiposDiscrepancia.INTERNA_ASUMIDA: tp.discrepancia_interna_asumida,
        TiposDiscrepancia.PRODUCTOR: tp.discrepancia_productor
    }
    total_fecha_repartido = Decimal()
    total_fecha_recibido  = Decimal()
    total_fecha_red       = Decimal()
    # Almacenamos las alertas de cada red
    alertas_redes = defaultdict(lambda : defaultdict(bool))
    if datos_productores_propios:
        total_propios_repartido = Decimal()
        total_propios_recibido = Decimal()
        total_propios_red = Decimal()
        for row in datos_productores_propios:
            total_pedido_repartido = pedidos_repartido[row.pedido.id]
            total_pedido_recibido  = pedidos_recibido[row.pedido.id]
            total_pedido_red       = pedidos_red[row.pedido.id]
            total_propios_repartido += total_pedido_repartido
            total_propios_recibido  += total_pedido_recibido
            total_propios_red       += total_pedido_red
            alertas_redes[0] = {
                tipo_discrepancia:row[field] for tipo_discrepancia, field in tablas_alerta.items()
            }
            alertas = ''.join(
                simbolos_alerta[tipo_discrepancia] if hay_alerta else ''
                for tipo_discrepancia, hay_alerta in alertas_redes[0].items()
            )
            ls.append(r'\hline %s & %s & %s & %s & %s \\'%(
                latex_escape(row.productor.nombre) + ('$^{%s}$'%alertas if alertas else ''),
                latex_escape(str(row.productor.telefono or '')),
                total_pedido_repartido,
                total_pedido_recibido,
                total_pedido_red))

        ls.append(r'\hhline{|=|=|=|=|=|} %s & %s & %s & %s & %s \\'%(
            T('Total de los pedidos propios'), '',
            total_propios_repartido, total_propios_recibido, total_propios_red)
        )
        total_fecha_repartido += total_propios_repartido
        total_fecha_recibido  += total_propios_recibido
        total_fecha_red       += total_propios_red

    # tipos de discrepancia que pueden aparecer en un pedido que viene desde una red
    tablas_alerta = {
        TiposDiscrepancia.INTERNA: tpp.discrepancia_interna,
        TiposDiscrepancia.INTERNA_ASUMIDA: tpp.discrepancia_interna_asumida,
        TiposDiscrepancia.ASCENDIENTE: tpp.discrepancia_ascendiente
    }
    #Agrupamos los productores de la red, mostramos solo el total de la red
    if productores_redes:
        nombres_redes = defaultdict(str)
        redes_repartido = defaultdict(Decimal)
        redes_recibido = defaultdict(Decimal)
        redes_red = defaultdict(Decimal)
        telefonos = defaultdict(str)
        for row in productores_redes:
            nombres_redes[row.grupo.id] = row.grupo.nombre
            redes_repartido[row.grupo.id] += pedidos_repartido[row.proxy_pedido.pedido]
            redes_recibido[row.grupo.id]  += pedidos_recibido[row.proxy_pedido.pedido]
            redes_red[row.grupo.id]       += pedidos_red[row.proxy_pedido.pedido]
            telefonos[row.grupo.id] = row.grupo.telefono or ''
            for tipo_discrepancia, field in tablas_alerta.items():
                alertas_redes[row.grupo.id][tipo_discrepancia] += row[field]

        for ired, nombre_red in nombres_redes.items():
            s_alertas = ''.join(
                simbolos_alerta[tipo_discrepancia] if hay_alerta else ''
                for tipo_discrepancia, hay_alerta in alertas_redes[ired].items()
            )
            ls.append(r'\hline %s & %s & %s & %s & %s \\'%(
                latex_escape(nombre_red) + ('$^{%s}$'%s_alertas if s_alertas else ''),
                telefonos[ired],
                redes_repartido[ired],
                redes_recibido[ired],
                redes_red[ired]))

        total_fecha_repartido += sum(redes_repartido.values())
        total_fecha_recibido  += sum(redes_recibido.values())
        total_fecha_red       += sum(redes_red.values())
        ls.append(r'\hhline{|=|=|=|=|=|} %s & %s & %s & %s & %s \\'%(
            T('Gran Total'), '',
            total_fecha_repartido, total_fecha_recibido, total_fecha_red)
        )

    ls.append(r'\hline\end{longtable}}')
    #
    alertas_presentes = {
        tipo_discrepancia: any(d.get(tipo_discrepancia) for i, d in alertas_redes.items())
        for tipo_discrepancia, simbolo in simbolos_alerta.items()
    }
    if any(alertas_presentes.values()):
        ls.append(r'\begin{tabular}{rl}')
        for tipo_discrepancia, hay_alerta in alertas_presentes.items():
            if hay_alerta:
                ls.append(r'$%s$ & %s \\'%
                            (simbolos_alerta[tipo_discrepancia],
                             latex_escape(d_explicaciones_discrepancias[tipo_discrepancia].decode()
                                %nombre_grupo))
                )
        ls.append(r'\end{tabular}')

    return '\n'.join(ls)

####
def tabla_discrepancias_red(ipedidos, ired, apaisado, forzar=False):
    from .modelos.grupos import nodos_en_la_red

    ls = []
    hay_datos=False
    # cisam: permitir Informe Discrepancias con sólo un grupo.
    # para ello puede llegar en el campo ired el id de un grupo q no es red.
    gruposenlared = nodos_en_la_red(ired)
    if(not gruposenlared):
        ipedido = ipedidos[0] if ipedidos else 0
        gruposenlared={}
        if(ipedido):
            db = current.db
            rowp = db.grupo(ired)
            if( rowp ):
                gruposenlared[rowp.id] = rowp.nombre
                ired = db.pedido(ipedido).productor.grupo.id

    for igrupo, nombre_grupo in gruposenlared.items():
        #ls.append( tabla_discrepancias(ipedidos, igrupo, apaisado, nombre_grupo))
        cuerpo = tabla_discrepancias(ipedidos, igrupo, apaisado, nombre_grupo)
        if (len(cuerpo)>0):
            hay_datos = True
            ls.append( cuerpo )

    #informe vacio, indicar al menos la cabezera
    T = current.T
    if (not hay_datos) and forzar:
        width = PAGE_HEIGHT if apaisado else PAGE_WIDTH
        partes = 8
        cell_width = width/partes
        LINE_STRETCH = LINE_STRETCH_A if apaisado else LINE_STRETCH_V
        ls = [r'{\renewcommand{\arraystretch}{'+LINE_STRETCH+r'}\begin{longtable}{|p{%.2fcm}|p{%.2fcm}|p{%.2fcm}|p{%.2fcm}|p{%.2fcm}|}'%(
                2*cell_width, cell_width, cell_width, cell_width, cell_width
            ),
            r'\hline\multicolumn{5}{|c|}{%s}\\'%(T('Sin Discrepancias') ) ]
        ls.append(r'\hline\end{longtable}}')

    # #indicar explicacion de discrepancia cerrada,asumida grupo o red al final del informe
    # if hay_datos:
    #     ls.append(r'     {\small (1) {%s}}'%(T('Discrepancia asumida por el grupo')))
    #     ls.append('\n')
    #     ls.append(r'     {\small (2) {%s}}'%(T('Discrepancia asumida por la red')))

    return '\n'.join(ls)


def tabla_discrepancias(ipedidos, igrupo, apaisado, cabecera=''):
    from .modelos.pedidos import productos_discrepancia
    T = current.T
    pedidos_con_discrepancias, nombres_productores = productos_discrepancia(ipedidos, igrupo)
    if pedidos_con_discrepancias:
        width = PAGE_HEIGHT if apaisado else PAGE_WIDTH
        ncols = len(ipedidos) + 1
        partes = 10
        cell_width = width/partes
        LINE_STRETCH = LINE_STRETCH_A if apaisado else LINE_STRETCH_V
        tex_red = r'}\begin{longtable}{|p{%.2fcm}|p{%.2fcm}|p{%.2fcm}|p{%.2fcm}|p{%.2fcm}|p{%.2fcm}|p{%.2fcm}|}'%(
            2*cell_width, cell_width, cell_width, cell_width, cell_width, cell_width, cell_width
        )
        ls = [
            r'{\renewcommand{\arraystretch}{'
            + LINE_STRETCH
            + tex_red,
            r'\hline\multicolumn{7}{|c|}{%s}\\'%(T('Discrepancias') + ' ' + cabecera) ]
        tex_productor = (
            r'''\hline \multicolumn{1}{|c|}{%%s} & \multicolumn{2}{p{%.2fcm}|}{%%s} & \multicolumn{2}{p{%.2fcm}|}{%%s}\\
            '''%(2*cell_width,2*cell_width)
            if cabecera else
            r'''\hline \multicolumn{1}{|c|}{%%s} & \multicolumn{2}{p{%.2fcm}|}{%%s}  & \multicolumn{2}{p{%.2fcm}|}{%%s} & \multicolumn{2}{p{%.2fcm}|}{%%s}\\
            '''%(2*cell_width,2*cell_width,2*cell_width)
        )
        tex_producto = (
            r'\hline %s & %s & %s \geneuro & %s & %s \geneuro \\'
            if cabecera else
            r'\hline %s & %s & %s \geneuro & %s & %s \geneuro & %s & %s \geneuro \\'
        )
        for ipedido, productos  in pedidos_con_discrepancias.items():
            nombre_productor, nombre_red, propio, disc_i, disc_ia, disc_a = nombres_productores[ipedido]
            textos_productor = (
                nombre_productor,
                T('Total según %s')%(nombre_productor if propio else nombre_red),
                T('Total según Grupo') if propio else (T('Total según %s')%cabecera)
            )
            if not cabecera:
                textos_productor += (T('Total repartido'),)
            ls.append( tex_productor%textos_productor )

            for (cantidad_repartida, cantidad_recibida, cantidad_red,
                 precio_repartido, precio_recibido, precio_red, nombre_producto) in productos:
                datos_producto = (
                    latex_escape(nombre_producto),
                    cantidad_red, precio_red,
                    cantidad_recibida, precio_recibido,
                )
                if not cabecera:
                    datos_producto += (cantidad_repartida, precio_repartido)

                ls.append(tex_producto%datos_producto)
        # TODO: ¿¿ Incluir TOTALES de discrepancias agrupados por red ??
        # ya está en tabla_totales
        ls.append(r'\hline\end{longtable}}')
        #si el informe lo solicita el grupo, añadir las notas a pie de pagina
        return '\n'.join(ls)
    else:
        return ''


def crear_informe_completo(ipedidos, igrupo,
                           columnas_por_tabla=20,
                           apaisado=True,
                           **kwds):
    db = current.db
    T  = current.T
    tppp = db.productoXpedido

    ###Adquisicion de datos
    #TODO: deberia recoger tb los precio_total para no tener que hacer cuentas aqui dentro
    #TODO: se puede recoger todo de un tiron, y quizá tb reusar codigo de otras partes
    mapag   = mapa_grupo(igrupo)
    unidades, productos_i, cantidades_i = recoge_y_ordena_item(ipedidos, igrupo)
    personas, productos_p, cantidades_p = recoge_y_ordena_peticion(ipedidos, igrupo)

    ###Merge de los items por unidades y las peticiones por personas
    #Podria ocurrir que se asignase pedido a una unidad que no lo pidio
    #O que una persona pidiese algo pero no le llegara nada
    spersonas = set(p for (_,p,_) in personas)
    us = set(unidades)
    us.update(u for u,_,_ in personas)
    us.update(row.unidad
        for row in db( db.coste_pedido.pedido.belongs(ipedidos) &
                      (db.coste_pedido.grupo==igrupo) &
                      (db.coste_pedido.coste>0))
                   .select(db.coste_pedido.unidad))

    unidades = list(us)
    unidades.sort()
    u2ps = dict((u, [p for p in mapag[u] if p in spersonas]) for u in unidades)
    username = dict((id,un) for (_,id,un) in personas)

    #Puede que un producto que se pidio no llegue, o que llegue uno que se pidio
    iproductos = set(productos_i.keys()).union(productos_p.keys() )

    ordered_prods = db( tppp.id.belongs(iproductos) &
                       (db.categorias_productos.id==tppp.categoria)) \
                    .select(tppp.id,
                            orderby=db.categorias_productos.nombre|tppp.nombre)
    lproductos = [(row.id, productos_i.get(row.id) or
                           productos_p.get(row.id))
                  for row in ordered_prods]

    productos = dict(lproductos)
    productos_ids = list(productos.keys())

    ###Preparar las columnas, como si no fuera nec cortar la tabla en partes
    columnas  = []
#    for unidad,miembros in u2ps.iteritems():
    for unidad in unidades:
        miembros = u2ps[unidad]
        colgroup = []
        for p in miembros:
            colgroup.append(latex_escape(username[p]))
        colgroup.append(current.T('Total').encode())
        colgroup.append(current.T('Precio').encode())
        columnas.append((unidad, current.T('Unidad %s').encode()%unidad, colgroup))
    ncolumnas = sum(len(colgroup) for colgroup in columnas)

    ncolgroups = [len(cg[2]) for cg in columnas]
    columnas_por_tabla, ntablas = ajusta_columnas_lista(ncolgroups, columnas_por_tabla)

    coste_pedidos = dict((ipedido, defaultdict(Decimal)) for ipedido in ipedidos)
    for row in db((db.coste_pedido.grupo==igrupo) &
                  (db.coste_pedido.pedido.belongs(ipedidos))) \
               .select(db.coste_pedido.ALL):
        coste_pedidos[row.pedido][row.unidad] = row.coste

    datos   = {}
    totales = {}
    gran_total = dict((unidad,Decimal()) for unidad in unidades)
    for ipedido in ipedidos:
        tabla = []
        coste_pedido = coste_pedidos[ipedido]
        totales_pedido = dict((unidad,coste_pedido[unidad]) for unidad in unidades)
        ci_pedido = cantidades_i[ipedido]
        cp_pedido = cantidades_p[ipedido]
        prods = set(list(ci_pedido.keys()) + list(cp_pedido.keys()))
        prods = ordena_productos(prods)

        for pid in prods:
            _, _, _, precio_f = productos[pid]
            fila = []
            ci_prod = ci_pedido[pid]
            cp_prod = cp_pedido[pid]
#            for unidad,miembros in u2ps.iteritems():
            for unidad in unidades:
                miembros = u2ps[unidad]
                cantidad_ps = Decimal()
                for p in miembros:
                    cantidad = cp_prod[p]
                    cantidad_ps += cantidad
                    fila.append('%s'%pprint_c(cantidad) if cantidad else '')
                cantidad_i = ci_prod[unidad]
                precio     = cantidad_i*precio_f
                if cantidad_i or cantidad_ps:
                    fila.append('%s'%pprint_c(cantidad_i)
                                if (cantidad_ps==cantidad_i) else
                                (r'\st{' + '%s' + '} ' + '%s')%(pprint_c(cantidad_ps), pprint_c(cantidad_i)))
                else:
                    fila.append('')
                fila.append('%s'%pprint_p(precio) if precio else '')
                totales_pedido[unidad] += precio
            tabla.append((pid, fila))

        datos[ipedido] = tabla
        totales[ipedido] = totales_pedido
        for unidad in u2ps:
            gran_total[unidad] += totales_pedido[unidad]

    ###Preparamos la tabla final
    width = PAGE_HEIGHT if apaisado else PAGE_WIDTH
    LINE_STRETCH = LINE_STRETCH_A if apaisado else LINE_STRETCH_V
    total_width = PROD_WIDTH + CELL_WIDTH*(columnas_por_tabla+1)
    scale       = width / (total_width*MULTIPLIER)
    widths =dict(product_width='%.2f'%(PROD_WIDTH*scale),
                 normal_cell_width='%.2f'%(CELL_WIDTH*scale),
                )
    ls = []
    icol_start = 0
    icol_end   = 0
    while columnas:
        hay_que_pesar = False
        cols = [columnas.pop(0)]
        lcols = len(cols[0][2])
        while columnas and (lcols + len(columnas[0][2]) <= columnas_por_tabla):
            cols.append(columnas.pop(0))
            lcols += len(cols[-1][2])
        icol_end += lcols
        #Tabla con los grupos que estan en cols
#        ls.append(r'\begin{longtable}{|p{3cm}' + '||p{.5cm}|p{1cm}'*lcols + '||}\n')
        ls.append((r'{\renewcommand{\arraystretch}{'+LINE_STRETCH+r'}\begin{longtable}{|p{%(product_width)scm}|p{%(normal_cell_width)scm}||' +
                   '||'.join('|'.join('p{%(normal_cell_width)scm}' for _ in range(len(colgroup)))
#                   '%s p{.7cm}|p{1cm}'%('p{0.7cm}|'*(len(colgroup) - 2) )
                            for _,_,colgroup in cols) +
                   '||}\n')%widths)
        cabecera1   = r'\hiderowcolors\hline & & %s\\'% \
                       ' & '.join(r'\multicolumn{%d}{c||}{%s}'%
                                 (len(colgroup), unidad)
                                  for _,unidad,colgroup in cols)
        cabecera2   = r'\hline & precio unitario & %s\\\showrowcolors '% \
                       ' & '.join(' & '.join(colgroup)
                                  for _,_,colgroup in cols)
        for ipedido in ipedidos:
            pedido = db.pedido[ipedido]
            totales_pedido = totales[ipedido]
            coste_pedido   = coste_pedidos[ipedido]
            ls.append(r'\hline\multicolumn{%d}{|c|}{\textbf{%s}}\\'%
                      (lcols+2, latex_escape(pedido.productor.nombre)))
            ls.append(cabecera1)
            ls.append(cabecera2)

            for pid,fila in datos[ipedido]:
                pnombre,_,pesar,pprecio = productos[pid]
                hay_que_pesar = hay_que_pesar or pesar
                ls.append(r'\hline %s & %s & %s \\'%(
                    ('\\textbf{%s} (*)' if pesar else '%s')%latex_escape(pnombre),
                    pprint_p(pprecio),
                    ' & '.join(fila[icol_start:icol_end])) )
            if sum(coste_pedido.values()):
                ls.append(r'\hiderowcolors\hline %s & & %s\\'% \
                            (T('Coste del pedido'),
                             ' & '.join(r'\multicolumn{%d}{c||}{%s}'%
                                        (len(colgroup),
                                        pprint_p(coste_pedido[iunidad]))
                                        for iunidad,_,colgroup in cols))
                        )
            ls.append(r'\hiderowcolors\hline %s %s & & %s\\'% \
                       (T('Total'), T('Unidad'),
                        ' & '.join(r'\multicolumn{%d}{c||}{%s}'%
                                 (len(colgroup), pprint_p(totales_pedido[iunidad]))
                                  for iunidad,_,colgroup in cols))
                    )

        ls.append(r'\hiderowcolors\hline %s %s & & %s\\'% \
                   (T('Gran Total'), T('Unidad'),
                    ' & '.join(r'\multicolumn{%d}{c||}{%s}'%
                             (len(colgroup), pprint_p(gran_total[iunidad]))
                              for iunidad,_,colgroup in cols))
                )
        ls.append(r'\hline\end{longtable}}')
        if hay_que_pesar:
            ls.append('{\\small \\textbf{(*)}: recuerda que tienes que pesar estos productos, y posiblemente declararlos en las incidencias.}')
        icol_start = icol_end
    tabla_latex         = '\n'.join(ls)
    tabla_latex_totales = tabla_totales(ipedidos, igrupo, apaisado) + tabla_discrepancias(ipedidos, igrupo, apaisado)

    if apaisado:
        latex = TEMPLATE_APAISADO
    else:
        latex = TEMPLATE_VERTICAL
    return latex.replace('FONT_SCALE', '%.2f'%(scale*FONT_SCALE_BIS)) \
                .replace('tabla_latex','%s'%tabla_latex + tabla_latex_totales)

def crear_informe_agrupado(ipedidos, igrupo,
                           columnas_por_tabla=20,
                           apaisado=True,
                           **kwds):
    db = current.db
    T  = current.T

    pedidos = [db.pedido[id] for id in ipedidos]
    unidades, productos, cantidades = recoge_y_ordena_item(ipedidos, igrupo)
    sunidades = set(unidades)
    sunidades.update(row.unidad
        for row in db( db.coste_pedido.pedido.belongs(ipedidos) &
                      (db.coste_pedido.grupo==igrupo) &
                      (db.coste_pedido.coste>0))
                   .select(db.coste_pedido.unidad))
    unidades = list(sorted(sunidades))

    unidades_por_tabla = columnas_por_tabla//2
    unidades_por_tabla, ntablas = ajusta_columnas( unidades_por_tabla, len(unidades))
    columnas_por_tabla = unidades_por_tabla*2

    productos_ids = list(productos.keys())
    productos_ids.sort()

    coste_pedidos = dict((ipedido, defaultdict(Decimal)) for ipedido in ipedidos)
    for row in db((db.coste_pedido.grupo==igrupo) &
                  (db.coste_pedido.pedido.belongs(ipedidos))) \
               .select(db.coste_pedido.ALL):
        coste_pedidos[row.pedido][row.unidad] = row.coste

    width = PAGE_HEIGHT if apaisado else PAGE_WIDTH
    LINE_STRETCH = LINE_STRETCH_A if apaisado else LINE_STRETCH_V
    total_width = (PROD_WIDTH +
                   (CELL_WIDTH + CELL_WIDTH_SHORT)*unidades_por_tabla)
    scale       = width / (total_width*MULTIPLIER)
    widths =dict(product_width='%.2f'%(PROD_WIDTH*scale),
                 normal_cell_width='%.2f'%(CELL_WIDTH*scale),
                 short_cell_width='%.2f'%(CELL_WIDTH_SHORT*scale)
                )

    ls           = []
    for k in range(ntablas):
        hay_que_pesar = False
        lunidades    = unidades[k*unidades_por_tabla:(k+1)*unidades_por_tabla]
        nunidades    = len(lunidades)
        gran_total  = dict(zip(lunidades, [Decimal()]*nunidades))

        row_unidades = r'\hiderowcolors\hline & %s\\\showrowcolors '% \
                       ' & '.join(r'\multicolumn{2}{c||}{%d}'%unidad
                                  for unidad in lunidades)
        ls.append((r'{\renewcommand{\arraystretch}{'+LINE_STRETCH+r'}\begin{longtable}{|p{%(product_width)scm}' +
                    '||p{%(short_cell_width)scm}|p{%(normal_cell_width)scm}'*nunidades +
                    '||}\n')%widths)

        for pedido in pedidos:
            cants           = cantidades[pedido.id]
            coste_pedido    = coste_pedidos[pedido.id]
            precios_totales = dict((unidad,coste_pedido[unidad]) for unidad in lunidades)
            ls.append(r'\hline\multicolumn{%d}{|c|}{\textbf{%s}}\\'%
                      (2*nunidades + 1, latex_escape(pedido.productor.nombre)) )
            ls.append(row_unidades)

            prods = ordena_productos(list(cants.keys()))

#            for pid,peticiones in cants.iteritems():
            for pid in prods:
                peticiones = cants[pid]
                pnombre, _, pesar, pprecio = productos[pid]
                hay_que_pesar = hay_que_pesar or pesar
                pnombre = ('\\textbf{%s} (*)' if pesar else '%s')%latex_escape(pnombre)
                pprecio = pprecio

#                if granel:
                cs = [peticiones.get(unidad, Decimal()) for unidad in lunidades]
                ls.append(r'\hline %s & '%pnombre +
                          ' & '.join('%s & %s\\geneuro'%( pprint_c(c), pprint_p(pprecio*c) )
                                     if c>0 else ' & '
                                     for c in cs) +
                          r' \\')
#                else:
#                    cs = [peticiones.get(unidad, Decimal()) for unidad in lunidades]
#                    ls.append(r'\hline %s & '%pnombre +
#                              ' & '.join('%d & %.2f\\geneuro'%(c, pprecio*c)
#                                         if c>0 else ' & '
#                                         for c in cs) +
#                              r' \\')
                for unidad,c in zip(lunidades, cs):
                    precios_totales[unidad] += c*pprecio

            for unidad in lunidades:
                gran_total[unidad] += precios_totales[unidad]

            #Totales
            ls.append(r'\hline %s & '%T('Coste del pedido')+
                          ' & '.join('  & %s\\geneuro'%pprint_p(coste_pedido[unidad])
                                     for unidad in lunidades) +
                          r' \\')
            #Totales
            ls.append(r'\hline %s & '%T('Total')+
                          ' & '.join('  & %s\\geneuro'%pprint_p(precios_totales[unidad])
                                     for unidad in lunidades) +
                          r' \\')
        #Grand Totales
        if len(ipedidos)>1:
            ls.append(r'\hline\multicolumn{%d}{|c|}{\textbf{%s}}\\'%(2*nunidades + 1, T('Gran Total')))
            ls.append(r'\hline %s & '%T('Gran Total')+
                          ' & '.join('  & %s\\geneuro'%pprint_p(gran_total[unidad])
                                     for unidad in lunidades) +
                          r' \\')
        ls.append(r'\hline\end{longtable}}')
        if hay_que_pesar:
            ls.append('{\\small \\textbf{(*)}: recuerda que tienes que pesar estos productos, y posiblemente declararlos en las incidencias.}')

#        ls.append(r'\newpage')
    tabla_latex = '\n'.join(ls)
    tabla_latex_totales = tabla_totales(ipedidos, igrupo, apaisado) + tabla_discrepancias (ipedidos, igrupo, apaisado)

    if apaisado:
        latex = TEMPLATE_APAISADO
    else:
        latex = TEMPLATE_VERTICAL
    return latex.replace('FONT_SCALE', '%.2f'%(scale*FONT_SCALE_BIS)) \
                .replace('tabla_latex','%s'%tabla_latex + tabla_latex_totales)

def crear_informe_reparto(ipedidos, igrupo,
                          columnas_por_tabla=20,
                          apaisado=True,
                          **kwds):
    db = current.db

    pedidos = [db.pedido[id] for id in ipedidos]
    unidades, productos, cantidades = recoge_y_ordena_item(ipedidos, igrupo)

    total_columnas = len(unidades)
    columnas_por_tabla, ntablas = ajusta_columnas( columnas_por_tabla, total_columnas)

    productos_ids = list(productos.keys())
    productos_ids.sort()

    width = PAGE_HEIGHT if apaisado else PAGE_WIDTH
    LINE_STRETCH = LINE_STRETCH_A if apaisado else LINE_STRETCH_V
    total_width = (PROD_WIDTH +
                   CELL_WIDTH_SHORT*columnas_por_tabla)
    scale       = width / (total_width*MULTIPLIER)
    widths =dict(product_width='%.2f'%(PROD_WIDTH*scale),
                 short_cell_width='%.2f'%(CELL_WIDTH_SHORT*scale)
                )

    ls           = []
    for k in range(ntablas):
        hay_que_pesar = False
        lunidades    = unidades[k*columnas_por_tabla:(k+1)*columnas_por_tabla]
        nunidades    = len(lunidades)

        row_unidades = r'\hiderowcolors\hline & %s\\ \showrowcolors '% \
                       ' & '.join('%d'%unidad for unidad in lunidades)
        ls.append((r'{\renewcommand{\arraystretch}{'+LINE_STRETCH+r'}\begin{longtable}{|p{%(product_width)scm}' +
                  '|p{%(short_cell_width)scm}'*nunidades + '|}\n')%widths)

        for pedido in pedidos:
            cants = cantidades[pedido.id]
            ls.append(r'\hline\multicolumn{%d}{|c|}{\textbf{%s}}\\'%
                      (nunidades+1, latex_escape(pedido.productor.nombre)) )
            ls.append(row_unidades)

            prods = ordena_productos(list(cants.keys()))

            for pid in prods:
                peticiones = cants[pid]
                pnombre, _, pesar, _ = productos[pid]
                hay_que_pesar = hay_que_pesar or pesar
                pnombre = ('\\textbf{%s} (*)' if pesar else '%s')%latex_escape(pnombre)

                cs = [peticiones.get(unidad, Decimal()) for unidad in lunidades]
                ls.append(r'\hline %s & '%pnombre +
                          ' & '.join('%s'%pprint_c(c)
                                     if c>0 else ' '
                                     for c in cs) +
                          r' \\')

        ls.append(r'\hline\end{longtable}}')
        if hay_que_pesar:
            ls.append('{\\small \\textbf{(*)}: %s.}'%current.T(
            'recuerda que tienes que pesar estos productos, y posiblemente declararlos en las incidencias'
            ))
#        ls.append(r'\newpage')
    tabla_latex = '\n'.join(ls)
    tabla_latex = tabla_latex or r'\center{%s}'%current.T('No hay productos que repartir')
    if apaisado:
        latex = TEMPLATE_APAISADO
    else:
        latex = TEMPLATE_VERTICAL
    return latex.replace('FONT_SCALE', '%.2f'%(scale*FONT_SCALE_BIS)) \
                 .replace('tabla_latex','%s'%tabla_latex)

def crear_informe_por_grupos(ipedidos, ired,
                             columnas_por_tabla=20,
                             apaisado=True,
                             **kwds):
    db = current.db
    T  = current.T

    pedidos = [db.pedido[int(p)] for p in ipedidos]
    grupos_id, productos, cantidades = recoge_y_ordena_item_por_grupos(ipedidos, ired)

    grupos    = [db.grupo(gid) for gid in grupos_id]

    #fix 3.2.20 (talla XS y S en apaisado deben tener menos columnas)
    if (columnas_por_tabla>20):
        columnas_por_tabla = 20
    grupos_por_tabla = columnas_por_tabla//2
    grupos_por_tabla, ntablas = ajusta_columnas( grupos_por_tabla, len(grupos))
    columnas_por_tabla = grupos_por_tabla*2

    productos_ids = list(productos.keys())
    productos_ids.sort()

    width = PAGE_HEIGHT if apaisado else PAGE_WIDTH
    LINE_STRETCH = LINE_STRETCH_A if apaisado else LINE_STRETCH_V
    total_width = (PROD_WIDTH +
                   (CELL_WIDTH + CELL_WIDTH_LARGE)*grupos_por_tabla)
    scale       = width / (total_width*MULTIPLIER)
    widths = dict(product_width='%.2f'%(PROD_WIDTH*scale),
                  normal_cell_width='%.2f'%(CELL_WIDTH*scale),
                  large_cell_width='%.2f'%(CELL_WIDTH_LARGE*scale) )

    ls           = []
#    ls.append('%s, %s!!'%(ntablas, grupos ))
    for k in range(ntablas):
        lgrupos    = grupos[k*grupos_por_tabla:(k+1)*grupos_por_tabla]
        lgrupos_id = [row.id for row in lgrupos]
        ngrupos    = len(lgrupos)
        #cuidado al truncar, que podemos partir un carácter unicode por la mitad!
        #https://stackoverflow.com/questions/13665001/python-truncating-international-string
        #mira la respuesta "truncate_unicode_to_byte_limit"
        nombres16 = [row.nombre[0:16]
                     for row in lgrupos]
        row_grupos = r'\hline & %s\\'% \
                       ' & '.join(r'\multicolumn{2}{c||}{%s}'%latex_escape(nombre16)
                      for nombre16 in nombres16)
        ls.append((r'{\renewcommand{\arraystretch}{'+LINE_STRETCH+r'}\begin{longtable}{|p{%(product_width)scm}' +
                    '||p{%(large_cell_width)scm}|p{%(large_cell_width)scm}'*ngrupos +
                    '||}\n')%widths)
        gran_total = dict(zip(lgrupos_id, [Decimal()]*ngrupos))
        for pedido in pedidos:
            precios_totales = dict(zip(lgrupos_id, [Decimal()]*ngrupos))
            cants = cantidades[pedido.id]
            ls.append(r'\hline\multicolumn{%d}{|c|}{\textbf{%s}}\\'%
                      (2*ngrupos + 1, latex_escape(pedido.productor.nombre)) )
            ls.append(row_grupos)

            prods = ordena_productos(list(cants.keys()))

            for pid in prods:
                peticiones = cants[pid]
                pnombre, _, pesar, pprecio = productos[pid]
                pnombre = ('\\textbf{%s} (*)' if pesar else '%s')%latex_escape(pnombre)
                cs = [peticiones.get(gid, Decimal()) for gid in lgrupos_id]
                for gid, cantidad in zip(lgrupos_id, cs):
                    precios_totales[gid] += cantidad*pprecio

                ls.append(r'\hline %s & '%pnombre +
                          ' & '.join('%s & %s\\geneuro'%(pprint_c(c), pprint_p(pprecio*c) )
                                     if c>0 else ' & '
                                     for c in cs) +
                          r' \\')

            # fix 3.2.15
            # incluir coste extra pedido/productor
            coste_pedidos = dict((gid, Decimal()) for gid in lgrupos_id)
            coste_extrap= db.coste_pedido.coste.sum()
            q_coste_extra = ((db.coste_pedido.pedido==pedido.id) &
                             (db.coste_pedido.coste > 0) &
                             (db.coste_pedido.grupo.belongs(lgrupos_id)) )   #fix 3.2.20
            t_coste_extra = 0
            for row in db(q_coste_extra).select(db.coste_pedido.pedido,
                    db.coste_pedido.grupo, coste_extrap, groupby=db.coste_pedido.grupo):
                coste_pedidos[row.coste_pedido.grupo] = row[coste_extrap]
                precios_totales[row.coste_pedido.grupo] += row[coste_extrap]
                t_coste_extra += row[coste_extrap]
            #coste del pedido si lo hay
            if t_coste_extra>0:
                ls.append(r'\hline %s & '%T('Coste del pedido')+
                          ' & '.join('  & %s\\geneuro'%pprint_p(coste_pedidos[gid])
                                     for gid in lgrupos_id) +
                          r' \\')

            #Totales
            ls.append(r'\hline %s & '%T('Total')+
                          ' & '.join('  & %s\\geneuro'%pprint_p(precios_totales[gid])
                                     for gid in lgrupos_id) +
                          r' \\')
            #Actualiza gran total
            for gid, total in precios_totales.items():
                gran_total[gid] += total

        #Gran Total
        ls.append(r'\hline\multicolumn{%d}{|c|}{\textbf{%s}}\\'%
                  (2*ngrupos + 1, T('Total para este reparto')) )
        ls.append(row_grupos)
        ls.append(r'\hline \hline \textbf{%s} & '%T('Gran Total')+
                      ' & '.join(r'  & %s\geneuro'%pprint_p(gran_total[gid])
                                 for gid in lgrupos_id) +
                      r' \\')
        ls.append(r'\hline\end{longtable}}')
#        ls.append(r'\newpage')
    tabla_latex = '\n'.join(ls)
    tabla_latex_totales = tabla_totales(ipedidos, ired, apaisado, es_red=True) + tabla_discrepancias_red(ipedidos, ired, apaisado)

    if apaisado:
        latex = TEMPLATE_APAISADO
    else:
        latex = TEMPLATE_VERTICAL
    return latex.replace('FONT_SCALE', '%.2f'%(scale*FONT_SCALE_BASE)) \
                 .replace('tabla_latex','%s'%tabla_latex + tabla_latex_totales)

def crear_informe_discrepancias(ipedidos, ired,
                                columnas_por_tabla=20,
                                apaisado=True,
                                **kwds):
    tabla_latex_totales = tabla_discrepancias_red(ipedidos, ired, apaisado, True)

    if apaisado:
        latex = TEMPLATE_APAISADO
    else:
        latex = TEMPLATE_VERTICAL
    return latex.replace('FONT_SCALE', '%.2f'%(2*FONT_SCALE_BASE)) \
                 .replace('tabla_latex','%s'%tabla_latex_totales)

#Lo dejo aparcado
def crear_informe_totales(ipedidos, igrupo, personas_por_columna=6):
    db = current.db

    pedidos = [db.pedido[id] for id in ipedidos]
    cantidad_total = db.item.cantidad.sum()
    def ha_pedido(persona):
        return  db((db.item.productoXpedido==db.productoXpedido.id) &
                    (db.productoXpedido.pedido.belongs(pedidos))  &
                    (db.item.persona==persona) ).count()
    personas = [row for row in db((db.personaXgrupo.grupo==igrupo) &
                                  (db.personaXgrupo.persona==db.auth_user.id)) \
                                   .select(db.auth_user.ALL, db.personaXgrupo.unidad,
                                   orderby=db.personaXgrupo.unidad|db.auth_user.username)
                          if ha_pedido(row.auth_user.id)]
    personas_id  = [row.auth_user.id for row in personas]
    from math import ceil
    ntablas      = int(ceil(float(len(personas))/personas_por_columna))
    ls           = []
    # LINE_STRETCH = LINE_STRETCH_A if apaisado else LINE_STRETCH_V
    for k in range(ntablas):
        lpersonas     = personas[k*personas_por_columna:(k+1)*personas_por_columna]
        lpersonas_id  = [row.auth_user.id for row in lpersonas]
        npersonas     = len(lpersonas)
        row_unidades = r'\hline & %s\\'% \
                       ' & '.join(r'\multicolumn{2}{l||}{%d}'%row.personaXgrupo.unidad
                                  for row in lpersonas)
        row_personas = r'\hline & %s\\'% \
                       ' & '.join(r'\multicolumn{2}{l||}{%s}'%latex_escape(row.auth_user.username)
                                  for row in lpersonas)
        row_vacia    = r'\hline & %s\\'% \
                       ' & '.join(r'\multicolumn{2}{l||}{}' for row in lpersonas)
        ls.append(r'{\renewcommand{\arraystretch}{'+LINE_STRETCH_A+r'}\begin{longtable}{|p{3cm}' + '||p{.7cm}|p{1cm}'*npersonas + '||}\n\hiderowcolors\n')
        ls.append(row_unidades)
        ls.append(row_personas)
        gran_total = dict(zip(lpersonas_id, [0.0]*npersonas))
        for pedido in pedidos:
            precios_totales = dict(zip(lpersonas_id, [0.0]*npersonas))
            ls.append(r'\hline\multicolumn{%d}{|c|}{\textbf{%s}}\\'%
                      (2*npersonas + 1, latex_escape(pedido.productor.nombre)) )

            for row in db(db.productoXpedido.pedido==pedido.id) \
                       .select(db.productoXpedido.ALL):
                #Si nadie ha pedido el producto, no se pone la linea
                ct = db((db.item.productoXpedido==db.productoXpedido.id) &
                        (db.productoXpedido.pedido==pedido)         &
                        (db.productoXpedido.producto==row.producto) ) \
                     .select(cantidad_total,
                             groupby=db.item.productoXpedido)
                if not ct or not ct.first()[cantidad_total]:
                    continue
                #Seguimos
                producto = db.producto[row.producto]
                pprecio  = float(row.precio)
                pnombre  = latex_escape(producto.nombre)
                cantidades = {}
                for pid in lpersonas_id:
                    q = db.item(productoXpedido=row.id, persona=pid)
                    cantidad = float(q.cantidad or 0) if q else 0.0
                    cantidades[pid]      =  cantidad
                    precios_totales[pid] += cantidad*pprecio

            for pid in lpersonas_id:
                gran_total[pid] += precios_totales[pid]
            ls.append(r'\hline %s & '%T('Total')+
                          ' & '.join('  & %.2f\\geneuro'%precios_totales[pid]
                                     for pid in lpersonas_id) +
                          r' \\')
            ls.append(row_vacia)
            ls.append(row_vacia)

        ls.append(r'\hline %s & '%T('Gran total')+
                      ' & '.join('  & %.2f\\geneuro'%gran_total[pid]
                                 for pid in lpersonas_id) +
                      r' \\')
        ls.append(row_vacia)
        ls.append(row_vacia)

        ls.append(r'\hline\end{longtable}}')
#        ls.append(r'\newpage')
    return '\n'.join(ls)


def generar_informe(tipo_informe, ipedidos, fecha_reparto, igrupo, **kwds):
    db = current.db
    C  = current.C

    prefijo_informe = {
        C.COBRO_DESGLOSADO:        'Cobro_personas',
        C.COBRO_AGRUPADO:          'Cobro_unidades',
        C.REPARTO_FACIL:           'Reparto_facil',
    #    COBRO_TOTALES:           crear_informe_totales,
        C.DESGLOSADO_POR_GRUPOS:   'Cobro_grupos',
        C.DISCREPANCIAS:   'Discrepancias'
    }
    CREAR_INFORME = {
        C.COBRO_DESGLOSADO:        crear_informe_completo,
        C.COBRO_AGRUPADO:          crear_informe_agrupado,
        C.REPARTO_FACIL:           crear_informe_reparto,
    #    COBRO_TOTALES:           crear_informe_totales,
        C.DESGLOSADO_POR_GRUPOS:   crear_informe_por_grupos,
        C.DISCREPANCIAS:           crear_informe_discrepancias
    }

    pedidos = [db.pedido[id] for id in ipedidos]
    table_func = CREAR_INFORME[tipo_informe]

    latex_content = table_func(ipedidos, igrupo, **kwds)

    #Write tex file
    str_fecha = format_fecha(fecha_reparto)

    #A raiz de los problemas con informes de multiples pedidos (>40),
    #se sustituye la relacion de pedidos por un timestamp, y se indica el tipo de informe
    ts = time()
    str_fecha2 = datetime.fromtimestamp(ts).strftime('%Y-%m-%d-%H-%M-%S-%f')
    rutatex = '%s_%s_%s.tex' %(prefijo_informe[tipo_informe],
                                  fecha_reparto,
                                  str_fecha2)
#    outfile = codecs.EncodedFile(open(rutatex, 'w'), 'utf-8', 'utf-8')
#    outfile.write(latex_content.encode('utf-8'))

    outfile = open(rutatex, 'wb')
    outfile.write(latex_content.encode('utf-8'))
    outfile.close()

    wd = os.path.join(os.getcwd(), 'applications', current.request.application, 'private', 'temp_files')
    command = ['pdflatex', '-halt-on-error', '-output-directory', wd, rutatex]
    process = subprocess.Popen(command, bufsize=-1,
                               stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdoutdata, stderrdata = process.communicate()
    exit_code = process.returncode
    if exit_code:
        raise Exception(latex_content)
    base, _ = os.path.splitext(rutatex)
    rutapdf = base + '.pdf'
    rutaaux = base + '.aux'
    rutalog = base + '.log'
    os.remove(rutatex)
    os.remove(os.path.join(wd, rutaaux))
    os.remove(os.path.join(wd, rutalog))
    ruta_completa = os.path.join(wd, rutapdf)
    return rutapdf, ruta_completa
