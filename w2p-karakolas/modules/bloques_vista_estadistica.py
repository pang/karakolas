# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

from decimal import Decimal
from collections import defaultdict
from datetime import date, timedelta

from gluon.html import FORM, INPUT, LABEL, SELECT, OPTION, URL, DIV, BUTTON
from gluon import current
from gluon.storage import Storage
from gluon.http import redirect

from .kutils import parse_fecha

def filter_vars(d):
    return dict((k,v) for k,v in d.items() if not k.startswith('_'))

class SelectorRangoFechas(FORM):

    def __init__(self, **kwds):
        T = current.T
        request_vars = current.request.vars
        try:
            fecha_fin = parse_fecha(request_vars.fecha_fin)
        except (ValueError, AttributeError) as e:
            undia = timedelta(days=1)
            fecha_fin = date.today() - undia
        try:
            fecha_ini = parse_fecha(request_vars.fecha_ini)
        except (ValueError, AttributeError) as e:
            if (fecha_fin.month, fecha_fin.day) == (2,29):
                fecha_ini = date(fecha_fin.year - 1, 2, 28)
            else:
                #Esta formula falla los años bisiestos
                fecha_ini = date(fecha_fin.year - 1, fecha_fin.month, fecha_fin.day)

        if fecha_ini.year == fecha_fin.year:
            def representa_fecha(col):
                return '%s-%s'%(col.day, col.month)
        else:
            def representa_fecha(col):
                return '%s-%s-%s'%(col.day, col.month, col.year%100)

        if '_id' not in kwds:
            kwds['_id'] = 'form_rango_fechas'
        if '_class' not in kwds:
            kwds['_class'] = 'form-inline'
        args=[
            DIV(
                LABEL(T('Inicio'), _for='fecha_ini'),
                INPUT(_value=fecha_ini, _type='date',
                      _class="form-control",
                      _name='fecha_ini', _id='fecha_ini'),
                _class='form-group'
            ),
            DIV(
                LABEL(T('Fin'), _for='fecha_fin'),
                INPUT(_value=fecha_fin, _type='date',
                      _class="form-control",
                      _name='fecha_fin', _id='fecha_fin'),
                _class='form-group'
            ),
            INPUT(_type='submit', _name='submit',
                  _value=T('Cambiar rango'),  _class="btn btn-primary")
        ]

        FORM.__init__(self, *args, **kwds)

        self._id = kwds['_id']
        self.rango_fechas = fecha_ini, fecha_fin
        self.representa_fecha = representa_fecha

    def redirect_if_hit(self):
        request = current.request
        if FORM.process(self, formname=self._id, onsuccess=None).accepted:
            rvars = filter_vars(request.vars)
            rvars['fecha_ini'] = self.vars.fecha_ini
            rvars['fecha_fin'] = self.vars.fecha_fin
            redirect(URL(c=request.controller, f=request.function,
                         vars=rvars),
                     client_side=True
            )


class SelectorDetalle(FORM):
    def __init__(self, ls, link, name, label, **kwds):
        '''Por ejemplo:

        form_stat_productor = SelectorDetalle(
            productores, 'productor', T('Elige un productor')
        )
        form_stat_productor.redirect_if_hit()
        '''
        T = current.T
        if '_id' not in kwds:
            kwds['_id'] = 'selector_%s'%name
        if '_class' not in kwds:
            kwds['_class'] = 'form-inline'
        args=[
            DIV(LABEL(T('Ver estadisticas detalladas de: '),
                      _for=name),
                SELECT(OPTION(label, _value=-1),
                       *[OPTION(rname, _value=rid)
                         for rid, rname in ls],
                       **dict(_name=name, _id=name)),
                _class='form-group'),
            INPUT(_type='submit', _name='submit',
                  _value=T('Ver estadísticas'),  _class="btn btn-primary")
            ]

        FORM.__init__(self, *args, **kwds)
        self._id = kwds['_id']
        self.link = link

    def redirect_if_hit(self):
        request = current.request
        if FORM.process(self, formname=self._id, onsuccess=None).accepted:
            redirect(URL(c=request.controller, f=self.link,
                         vars=filter_vars(request.vars)),
                     client_side=True
            )
