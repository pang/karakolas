# -*- coding: utf-8 -*-

# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################

#########################################################################
# Este archivo intenta evaluar expresiones aritmeticas exactamente igual que
# lo haría eval_arit.js. Como en realidad el problema es sencillo, no
# necesitamos hacer cosas sofisticadas como traducir de python a javascript
# ni llamar a un proceso javascript.
#########################################################################


import ast
import operator as op
from decimal import Decimal, InvalidOperation
from bisect import bisect_left

from gluon import current

class EvalAritException(Exception):
    pass

class EvalArit(object):
    # supported operators (Uso tanto ** como ^ para la potencia)
    operators = {ast.Add: op.add, ast.Sub: op.sub, ast.Mult: op.mul,
                 ast.Div: op.truediv, ast.Pow: op.pow, ast.BitXor: op.pow,
                 ast.USub: op.neg}
    functions = {'max':max,'min':min}

    def __init__(self, formula, global_vars, precision=Decimal('0.01')):
        '''Evalua la formula en el diccionario local_vars

        Puede lanzar SyntaxError
        '''
        self.global_vars = global_vars
        self.precision = precision
        self.tree = ast.parse(formula, mode='eval').body
        #TODO: compila el arbol en un objeto tipo code usando compile,
        #para ganar 100x en velocidad al evaluar.
        #Es necesario hacer alguna transformacion al arbol, por ej el IF(,,)
        #TODO: hazlo tb con la formula para el coste extra del pedido

    def get_var(self, name):
        try:
            return self.global_vars.get(name) or self.local_vars[name]
        except KeyError:
            raise EvalAritException(current.T('Has usado una variable que no reconocemos: %s')%name)

    def _eval(self, node):
        #TODO: refactor con CalculadoraCostesExtra, tienen demasiado en comun
        if isinstance(node, ast.Num): # <number>
            #Uso Decimal sobre la cadena porque es una representacion mas fiel de la formula
            return Decimal(str(node.n))
        elif isinstance(node, ast.BinOp): # <left> <operator> <right>
            try:
                return self.operators[type(node.op)](self._eval(node.left), self._eval(node.right))
            #En este contexto, no es raro obtener 0/0, y típicamente,
            #el resultado de la operación es 0 (ej: coste 0 a repartir entre 0 personas)
            except InvalidOperation:
                return Decimal()
        elif isinstance(node, ast.UnaryOp):
            return self.operators[type(node.op)](self._eval(node.operand))
        elif isinstance(node, ast.Name): # <variable name>
            return self.get_var(node.id)
        elif isinstance(node, ast.Call):
            try:
                func_name = node.func.id.lower()
            except AttributeError:
                raise EvalAritException(current.T('Problema al evaluar la formula: %s')%ast.dump(node))
            if func_name=='if':
                if len(node.args)!=3:
                    raise EvalAritException(T('Un condicional "IF" debe tener siempre tres argumentos: el primero es la condición, el segundo es una expresión que se ejecutará si la condición es cierta, y el tercero es una expresión que se ejecutará si la condición es falsa.'))
                condicion, exp1, exp2 = node.args
                if self._eval(condicion):
                    return self._eval(exp1)
                else:
                    return self._eval(exp2)
            try:
                return self.functions[func_name](*[self._eval(a) for a in node.args])
            except KeyError:
                raise EvalAritException(current.T('Has usado una función que no reconocemos: %s')%func_name)
        elif isinstance(node, ast.Compare):
            assert len(node.ops) == 1
            assert len(node.comparators) == 1
            op = node.ops[0]
            comp = self._eval(node.comparators[0])
            left = self._eval(node.left)
            if isinstance(op, ast.Eq):
                return left == comp
            elif isinstance(op, ast.NotEq):
                return left != comp
            elif isinstance(op, ast.Lt):
                return left < comp
            elif isinstance(op, ast.LtE):
                return left <= comp
            elif isinstance(op, ast.Gt):
                return left > comp
            elif isinstance(op, ast.GtE):
                return left >= comp

    def evalua(self, local_vars):
        '''Puede lanzar EvalAritException
        '''
        self.local_vars = local_vars
        return self._eval(self.tree).quantize(current.C.PRECISION_DECIMAL)

