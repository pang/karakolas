# -*- coding: utf-8 -*-

# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################

#_evalexpr obtenido (y modificado) de una de las respuestas a:
#http://stackoverflow.com/questions/2371436/evaluating-a-mathematical-expression-in-a-string
import ast
import operator as op
from decimal import Decimal, InvalidOperation
from bisect import bisect_left

from gluon import current
from gluon.html import A, URL, MARKMIN

from .kutils import pprint_c
from .modelos.grupos import unidades_en_grupo
from .exportador import ref

class CostesExtraException(Exception):
    pass

class CalculadoraCostesExtra(object):
    # supported operators (Uso tanto ** como ^ para la potencia)
    operators = {ast.Add: op.add, ast.Sub: op.sub, ast.Mult: op.mul,
                 ast.Div: op.truediv, ast.Pow: op.pow, ast.BitXor: op.pow,
                 ast.USub: op.neg}
    functions = {'max':max,'min':min}

    def __init__(self, ipedido, igrupo=None):
        '''Evalua la formula para calcular los costes extra asociados a un pedido.

        Se puede usar esta clase para un pedido de un grupo normal o de una red.
        Si se trata de un grupo normal, calcula el coste por cada unidad
        Si se trata de una red, calcula el coste a pagar por cada grupo, y
        crea recursivamente clases para repartir el coste de cada grupo entre las unidades
        '''
        db = current.db
        tproxy = db.proxy_pedido
        self.ipedido  = ipedido
        pedido = self.pedido = db.pedido(ipedido)
        grupo_productor = pedido.productor.grupo
        igrupo_productor = grupo_productor.id
        # La variable igrupo puede cambiar al aplicar la fórmula a distintos grupos
        igrupo = self.igrupo = igrupo or igrupo_productor
        # Este es el grupo, o red, que escribió la fórmula
        self.grupo_formula = db.grupo(igrupo)
        self.coordinado = (igrupo!=igrupo_productor)
        self.es_red = self.grupo_formula.red
        self.formula_productor = pedido.formula_coste_extra_productor
        tig = db.item_grupo

        self.vars_globales = dict(('%s'%row.nombre.upper(), row.valor)
            for row in db(db.campo_extra_pedido.pedido==ipedido) \
                       .select(db.campo_extra_pedido.nombre, db.campo_extra_pedido.valor))

        if self.coordinado:
            proxy_pedido = tproxy(pedido=ipedido, grupo=igrupo)
            fecha_reparto = proxy_pedido.fecha_reparto
        else:
            fecha_reparto = pedido.fecha_reparto
        if self.es_red:
            self.pedidos_red = [row.id
                    for row in db((db.pedido.productor==db.productor.id) &
                                  (db.productor.grupo==igrupo_productor) &
                                  (db.pedido.fecha_reparto==fecha_reparto))
                               .select(db.pedido.id) ]
        elif self.coordinado:
            self.pedidos_red = [row.pedido
                    for row in db((tproxy.grupo==self.igrupo)   &
                                  (tproxy.activo==True)   &
                                  # Nos ofrece el pedido la misma red, pero puede mezclar pedidos
                                  #  propios y de otras redes primigenias
                                  (tproxy.via==proxy_pedido.via) &
                                  (tproxy.fecha_reparto==fecha_reparto))
                               .select(tproxy.pedido) ]
        else:
            self.pedidos_red = []

        if self.coordinado:
            self.formula = proxy_pedido.formula_coste_extra_pedido
            self.vars_globales['COSTE_PEDIDO_RED'] = proxy_pedido.coste_red
        else:
            self.formula = pedido.formula_coste_extra_pedido

        if self.es_red:
            #Pedido desde una red
            self.proxys = [(row.grupo, row.id)
                      for row in db((tproxy.pedido==self.ipedido) &
                                    (tproxy.activo==True) &
                                    #Solo los proxys del nivel inmediatamente inferior
                                    (tproxy.via==igrupo)) \
                       .select(tproxy.grupo, tproxy.id)]
            self.grupos_en_pedido = [g for g,_ in self.proxys]

            self.grupos_que_han_pedido = [row.grupo
                      for row in db( tproxy.id.belongs([pid for _,pid in self.proxys]) &
                                    (tig.grupo==tproxy.grupo) &
                                    (tig.productoXpedido==db.productoXpedido.id) &
                                    (db.productoXpedido.pedido==self.ipedido) &
                                    (tig.cantidad_recibida>0)) \
                       .select(tproxy.grupo,
                               groupby=tproxy.id)]
        else:
            if self.coordinado:
                self.unidades_en_pedido_en_red = set(row.unidad
                    for row in db(
                    (db.item.grupo==self.igrupo) &
                    (db.item.cantidad>0) &
                    (db.item.productoXpedido==db.productoXpedido.id) &
                    (db.productoXpedido.pedido.belongs(self.pedidos_red))) \
                   .select(db.item.unidad, distinct=True))

            self.unidades_en_pedido = [ row.unidad for row in
                 db((db.item.grupo==self.igrupo) &
                    (db.item.cantidad>0) &
                    (db.item.productoXpedido==db.productoXpedido.id) &
                    (db.productoXpedido.pedido==self.ipedido))
                   .select(db.item.unidad, groupby=db.item.unidad) ]
            self.unidades_en_grupo = unidades_en_grupo(self.igrupo)

        columnas_extra = db((db.columna_extra_pedido.pedido==ipedido) &
                            (db.columna_extra_pedido.tipo=='decimal(9,2)')) \
                         .select(db.columna_extra_pedido.ALL)
        self.cols_unidad = dict(('%s_UNIDAD'%row.nombre.upper(), row.id)
            for row in columnas_extra)
        self.cols_grupo = dict(('%s_GRUPO'%row.nombre.upper(), row.id)
            for row in columnas_extra)
        self.cols_red = dict(('%s_RED'%row.nombre.upper(), row.id)
            for row in columnas_extra)

        self.vars_grupo = {}
        self.vars_red   = {}

    def get_var(self, name):
        '''Busca las variables en la formula y clasifica las globales y las locales.
        '''
        db = current.db
        tig = db.item_grupo
        name = name.upper()
        for d in [self.vars_globales, self.vars_grupo, self.vars_red]:
            if name in d:
                return d[name]
        #Comenzamos con las variables de ambito el grupo: su cache es vars_grupo
        v = None
        if name=='COSTE_GRUPO':
            #Si estamos calculando desde una red hacia abajo, usamos precio_recibido
            #Si estamos calculando desde un grupo, COSTE_GRUPO debe ser el agregado de las unidades
            coste_total = tig.precio_recibido.sum() if self.es_red else tig.precio_total.sum()
            row = db(
                (tig.grupo==self.igrupo) &
                (tig.productoXpedido==db.productoXpedido.id) &
                (db.productoXpedido.pedido==self.ipedido)) \
               .select(coste_total, groupby=tig.grupo).first()
            v = (row[coste_total] or Decimal()) if row else Decimal()
        elif name=='COSTE_GRUPO_EN_PEDIDOS_RED':
            #Tiene sentido en pedidos de una red y en pedidos coordinados
            if self.es_red or self.coordinado:
                coste_total = tig.precio_recibido.sum() if self.es_red else tig.precio_total.sum()
                row = db(
                    (tig.grupo==self.igrupo) &
                    (tig.productoXpedido==db.productoXpedido.id) &
                    (db.productoXpedido.pedido.belongs(self.pedidos_red))) \
                   .select(coste_total, groupby=tig.grupo).first()
                v = (row[coste_total] or Decimal()) if row else Decimal()
        elif name=='UNIDADES_EN_PEDIDOS_DE_LA_RED':
            #Tiene sentido en pedidos coordinados
            if (not self.es_red) and self.coordinado:
                v = len(self.unidades_en_pedido_en_red)
        elif name=='CANTIDAD_GRUPO':
            cantidad_total = tig.cantidad_recibida.sum() if self.es_red else tig.cantidad.sum()
            row = db(
                (tig.grupo==self.igrupo) &
                (tig.productoXpedido==db.productoXpedido.id) &
                (db.productoXpedido.pedido==self.ipedido)) \
               .select(cantidad_total, groupby=tig.grupo).first()
            v = (row[cantidad_total] or Decimal()) if row else Decimal()
        elif name=='UNIDADES_EN_PEDIDO':
            v = len(self.unidades_en_pedido)
        elif name=='UNIDADES_EN_GRUPO':
            v = len(self.unidades_en_grupo)
        elif name=='GRUPO_HA_PEDIDO':
            v = int(self.igrupo in self.grupos_que_han_pedido)
        elif name=='GRUPO_HA_PEDIDO_A_LA_RED':
            v = 0 if db((tig.grupo==self.igrupo) &
                        (tig.productoXpedido==db.productoXpedido.id) &
                        (db.productoXpedido.pedido==db.pedido.id) &
                        (db.proxy_pedido.pedido==db.pedido.id) &
                        (db.proxy_pedido.grupo==self.igrupo) &
                        (db.proxy_pedido.fecha_reparto==self.pedido.fecha_reparto) &
                        (tig.cantidad>0)
                 ).isempty() else 1
        elif name in self.cols_grupo:
            col_id = self.cols_grupo[name]

            item_cantidad_total = tig.cantidad_recibida.sum() if self.es_red else tig.cantidad.sum()
            v = sum(
                  r[item_cantidad_total]*r.valor_extra_decimal_pedido.valor
                  for r in db((tig.grupo==self.igrupo) &
                              (tig.productoXpedido==db.productoXpedido.id) &
                              (db.productoXpedido.pedido==self.ipedido) &
                              (db.valor_extra_decimal_pedido.columna==col_id) &
                              (db.valor_extra_decimal_pedido.producto==db.productoXpedido.producto)) \
                           .select(item_cantidad_total, db.valor_extra_decimal_pedido.valor,
                                   groupby=db.productoXpedido.producto)
            )
        if v!=None:
            self.vars_grupo[name] = v
            return v
        #Ahora las variables de ambito la red: su cache es vars_red
        if self.es_red:
            if name in ('COSTE_RED', 'CANTIDAD_RED', 'COSTE_RED_EN_PEDIDOS_RED', 'CANTIDAD_RED_EN_PEDIDOS_RED'):
                v = 0
                # al calcular COSTE_RED y CANTIDAD_RED,
                # usa item_grupo.precio_recibido e item_grupo.cantidad_recibida
                # porque el grupo paga en función de lo que dice haber recibido.
                # Mientras no se resuelva la diferencia entre cantidad_recibida
                # y cantidad_red, la red no estará contenta, pero eso es normal,
                # y cantidad_recibida es el lugar adecuado para que el grupo acepte
                # la versión de la red sin alterar sus cuentas internas.
                if name in ('COSTE_RED', 'COSTE_RED_EN_PEDIDOS_RED'):
                    total = tig.precio_recibido.sum()
                else:
                    total = tig.cantidad_recibida.sum()
                q = (tig.grupo.belongs(self.grupos_en_pedido) &
                    (tig.productoXpedido==db.productoXpedido.id))
                if name in ('COSTE_RED', 'CANTIDAD_RED'):
                    q &= (db.productoXpedido.pedido==self.ipedido)
                else:
                    q &= db.productoXpedido.pedido.belongs(self.pedidos_red)
                row = db(q).select(total, groupby=db.productoXpedido.pedido).first()
                v += row.get(total, Decimal()) if row else Decimal()
            elif name=='GRUPOS_EN_PEDIDO':
                v = len(self.grupos_que_han_pedido)
            elif name=='GRUPOS_EN_PEDIDOS_RED':
                v = db((tig.grupo.belongs([igr for igr,_ in self.proxys])) &
                       (tig.productoXpedido==db.productoXpedido.id) &
                       (db.productoXpedido.pedido.belongs(self.pedidos_red)) &
                       (tig.cantidad_recibida>0)
                ).select(tig.grupo, groupby=tig.grupo)
            elif name in self.cols_red:
                col_id = self.cols_red[name]

                item_cantidad_total = tig.cantidad_recibida.sum()
                v = sum(
                      r[item_cantidad_total]*r.valor_extra_decimal_pedido.valor
                      for r in db((tig.grupo.belongs(self.grupos_en_pedido)) &
                                  (tig.productoXpedido==db.productoXpedido.id) &
                                  (db.productoXpedido.pedido==db.pedido.id) &
                                  (db.valor_extra_decimal_pedido.columna==col_id) &
                                  (db.valor_extra_decimal_pedido.producto==db.productoXpedido.producto)) \
                               .select(item_cantidad_total, db.valor_extra_decimal_pedido.valor,
                                       groupby=db.productoXpedido.producto)
                )
            if v!=None:
                self.vars_red[name] = v
                return v
        #Ahora las variables de ambito la unidad, que no se cachean
        else:
            if name=='COSTE_UNIDAD':
                coste_total = db.item.precio_total.sum()
                row = db(
                    (db.item.unidad==self.unidad) &
                    (db.item.grupo==self.igrupo) &
                    (db.item.productoXpedido==db.productoXpedido.id) &
                    (db.productoXpedido.pedido==self.ipedido)) \
                   .select(coste_total, groupby=db.item.unidad).first()
                return (row[coste_total] or Decimal()) if row else Decimal()
            if name=='COSTE_UNIDAD_EN_PEDIDOS_RED':
                if (not self.es_red) and self.coordinado:
                    coste_total = db.item.precio_total.sum()
                    row = db(
                        (db.item.unidad==self.unidad) &
                        (db.item.grupo==self.igrupo) &
                        (db.item.productoXpedido==db.productoXpedido.id) &
                        (db.productoXpedido.pedido.belongs(self.pedidos_red))) \
                       .select(coste_total, groupby=db.item.unidad).first()
                    return (row[coste_total] or Decimal()) if row else Decimal()
            if name=='CANTIDAD_UNIDAD':
                cantidad_total = db.item.cantidad.sum()
                row = db(
                    (db.item.unidad==self.unidad) &
                    (db.item.grupo==self.igrupo) &
                    (db.item.productoXpedido==db.productoXpedido.id) &
                    (db.productoXpedido.pedido==self.ipedido)) \
                   .select(cantidad_total, groupby=db.item.unidad).first()
                return (row[cantidad_total] or Decimal()) if row else Decimal()
            if name=='HA_PEDIDO':
                return int(self.unidad in self.unidades_en_pedido)
            if name=='HA_PEDIDO_A_LA_RED':
                if (not self.es_red) and self.coordinado:
                    return int(self.unidad in self.unidades_en_pedido_en_red)
            if name in self.cols_unidad:
                col_id = self.cols_unidad[name]
                return sum(r.item.cantidad*r.valor_extra_decimal_pedido.valor for r in
                         db((db.item.unidad==self.unidad) &
                            (db.item.grupo==self.igrupo) &
                            (db.item.productoXpedido==db.productoXpedido.id) &
                            (db.productoXpedido.pedido==self.ipedido) &
                            (db.valor_extra_decimal_pedido.columna==col_id) &
                            (db.valor_extra_decimal_pedido.producto==db.productoXpedido.producto)) \
                         .select(db.item.cantidad, db.valor_extra_decimal_pedido.valor) )
        raise CostesExtraException(current.T('Has usado una variable que no reconocemos: %s')%name)

    def _eval(self, node):
        if isinstance(node, ast.Num): # <number>
            return Decimal(node.n)
        elif isinstance(node, ast.BinOp): # <left> <operator> <right>
            try:
                left, right = self._eval(node.left), self._eval(node.right)
                return self.operators[type(node.op)](left, right)
            except InvalidOperation as e:
                #Si no pide nadie, es tipico obtener 0/0, y en este caso,
                if left == right == 0 :
                    return Decimal()     #el resultado de la operación es 0
                raise e
        elif isinstance(node, ast.UnaryOp): # <operator> <operand> e.g., -1
            return self.operators[type(node.op)](self._eval(node.operand))
        elif isinstance(node, ast.Name): # <variable name>
            return self.get_var(node.id)
        elif isinstance(node, ast.Call):
            try:
                func_name = node.func.id.lower()
            except AttributeError:
                raise CostesExtraException(current.T('Problema al evaluar la formula: %s')%ast.dump(node))
            if func_name=='if':
                if len(node.args)!=3:
                    raise CostesExtraException(T('Un condicional "IF" debe tener siempre tres argumentos: el primero es la condición, el segundo es una expresión que se ejecutará si la condición es cierta, y el tercero es una expresión que se ejecutará si la condición es falsa.'))
                condicion, exp1, exp2 = node.args
                if self._eval(condicion):
                    return self._eval(exp1)
                else:
                    return self._eval(exp2)
            try:
                return self.functions[func_name](*[self._eval(a) for a in node.args])
            except (KeyError, AttributeError):
                raise CostesExtraException(CalculadoraCostesExtra.mensaje_funcion_no_permitida())
        elif isinstance(node, ast.Compare):
            assert len(node.ops) == 1
            assert len(node.comparators) == 1
            op = node.ops[0]
            comp = self._eval(node.comparators[0])
            left = self._eval(node.left)
            if isinstance(op, ast.Eq):
                return left == comp
            elif isinstance(op, ast.NotEq):
                return left != comp
            elif isinstance(op, ast.Lt):
                return left < comp
            elif isinstance(op, ast.LtE):
                return left <= comp
            elif isinstance(op, ast.Gt):
                return left > comp
            elif isinstance(op, ast.GtE):
                return left >= comp
            elif isinstance(op, ast.In):
                assert isinstance(comp, str)
                assert isinstance(left, str)
                return left.lower() in comp.lower()
            elif isinstance(op, ast.NotIn):
                assert isinstance(comp, str)
                assert isinstance(left, str)
                return left.lower() not in comp.lower()

    @staticmethod
    def mensaje_funcion_no_permitida():
        return (current.T('Sólo se permiten operaciones aritméticas, y las funciones: ')
        + ','.join(CalculadoraCostesExtra.functions)
        )

    def cambia_grupo(self, igrupo):
        db = current.db
        self.igrupo = igrupo
        self.vars_grupo = {}

    def coste_extra_grupo(self, grupo):
        return Decimal(self._eval(ast.parse(self.formula, mode='eval').body))

    def recalcula_costes_extra(self):
        db = current.db
        tig = db.item_grupo
        T  = current.T
        incidencias = []
        if not self.coordinado:
            try:
                #paga siempre al productor según los apuntes en item_grupo.precio_recibido
                # y item_grupo.cantidad_recibido
                tree_productor = ast.parse(self.formula_productor, mode='eval').body
                coste_productor = Decimal(self._eval(tree_productor))
                self.pedido.update_record(coste_extra_productor=coste_productor)
            except Exception as e:
                # TODO: redderedes comprueba que redirige al edita_pedido correcto
                #       ¿deberia ir a edita_pedido_coordinado?
                if isinstance(e, (CostesExtraException, SyntaxError, ZeroDivisionError, TypeError)):
                    if isinstance(e.args[0], str):
                        error_message = T(e.args[0])
                    elif hasattr(e.args[0], 'encode'):
                        error_message = e.args[0].encode()
                    else:
                        error_message = str(e.args)
                    incidencias.append((MARKMIN(T('''
### Error al calcular el coste correspondiente al productor para el pedido de %(nombre_productor)s.

Por favor repasa la fórmula de costes del pedido: ``%(formula)s``

Se ha encontrado un error ``%(error_type)s`` con el siguiente mensaje:

``%(error_message)s``
''',dict(formula=self.formula_productor,
          nombre_productor=self.pedido.productor.nombre,
          error_type=e.__class__, error_message=error_message)).xml()),
                        A(T('Cambia la fórmula'),
                            _href = URL(c='gestion_pedidos',
                                        f='edita_pedido.html',
                                        vars=dict(pedido=self.ipedido)),
                            _target='_blank'))
                        )
                    self.pedido.update_record(problemas_calculo_coste_extra=True)
                else:
                    raise e
        #Ahora parseamos la fórmula, pero sin evaluarla
        # Si hay errores, no continuamos
        try:
            tree = ast.parse(self.formula, mode='eval').body
        except SyntaxError as e:
            incidencias.append((MARKMIN(T('''
### Error al calcular el coste extra para el pedido de %(nombre_productor)s.

La fórmula de costes del pedido no es una fórmula correcta: ``%(formula)s``

Se ha encontrado un error ``%(error_type)s`` con el siguiente mensaje:

``%(error_message)s``
''',dict(formula=self.formula,
  nombre_productor=self.pedido.productor.nombre,
  error_type=e.__class__, error_message=T(e.msg))).xml()),
                A(T('Cambia la fórmula'),
                    _href = URL(c='gestion_pedidos',
                                f='edita_pedido.html',
                                vars=dict(pedido=self.ipedido)),
                    _target='_blank'))
                )
            self.pedido.update_record(problemas_calculo_coste_extra=True)
            return incidencias

        if self.es_red:
            for igrupo, iproxy in self.proxys:
                self.cambia_grupo(igrupo)
                proxy = db.proxy_pedido[iproxy]
                #Mostramos todos los grupos que dan problemas, porque cada uno tiene su
                #formula, mejor avisarles en paralelo y no en serie
                try:
                    coste_grupo = Decimal(self._eval(tree))
                    #Anotamos coste_grupo en proxy_pedido antes de calcular los costes del grupo pq
                    #para ese calculo se lee coste_grupo en proxy_pedido
                    proxy.update_record(coste_red=coste_grupo)

                    Calc = CalculadoraCostesExtra(self.ipedido, igrupo)
                    incidencias_unidades = Calc.recalcula_costes_extra()
                    incidencias.extend(incidencias_unidades)
                    proxy.update_record(problemas_calculo_coste_extra=bool(incidencias_unidades))
                except Exception as e:
                    proxy.update_record(problemas_calculo_coste_extra=True)
                    if isinstance(e, (CostesExtraException, ZeroDivisionError, TypeError)):
                        if isinstance(e.args[0], str):
                            error_message = T(e.args[0])
                        elif hasattr(e.args[0], 'encode'):
                            error_message = e.args[0].encode()
                        else:
                            error_message = str(e.args)
                        incidencias.append((MARKMIN(T('''
    ### Error al calcular el coste asignado al grupo %(grupo)s en el pedido de %(nombre_productor)s.

    Por favor repasa la fórmula de costes del pedido: ``%(formula)s``

    Se ha encontrado un error ``%(error_type)s`` con el siguiente mensaje:

    ``%(error_message)s``
    ''',dict(grupo=db.grupo(self.igrupo).nombre, formula=self.formula,
              nombre_productor=self.pedido.productor.nombre,
              error_type=e.__class__, error_message=error_message)).xml()),
                            A(T('Cambia la fórmula'),
                                _href = URL(c='gestion_pedidos',
                                            f='edita_pedido_coordinado.html',
                                            vars=dict(pedido=self.ipedido, grupo=self.grupo_formula.id)),
                                _target='_blank'))
                            )
                    else:
                        raise e

        else:
            try:
                #Solo mostramos la primera unidad que da problemas, porque todas salen de
                #la misma formula
                db((db.coste_pedido.pedido==self.ipedido) &
                   (db.coste_pedido.grupo==self.igrupo)).delete()
                for unidad in self.unidades_en_grupo:
                    self.unidad = unidad
                    #self.tabla_item_total es db.item_grupo, no es necesario cambiarla
                    coste_unidad = Decimal(self._eval(tree))

                    db.coste_pedido.insert(coste=coste_unidad, pedido=self.ipedido,
                                           grupo=self.igrupo, unidad=unidad)
            except Exception as e:
                if isinstance(e, (CostesExtraException, ZeroDivisionError, TypeError)):
                    if isinstance(e.args[0], str):
                        error_message = T(e.args[0])
                    elif hasattr(e.args[0], 'encode'):
                        error_message = e.args[0].encode()
                    else:
                        error_message = str(e.args)
                    incidencias.append((MARKMIN(T('''
### Error al calcular el coste asignado a la unidad %(unidad)s del grupo %(grupo)s en el pedido de %(nombre_productor)s.

Por favor repasa la fórmula de costes del pedido: ``%(formula)s``

Se ha encontrado un error ``%(error_type)s`` con el siguiente mensaje:

``%(error_message)s``
''',dict(unidad=unidad, grupo=db.grupo(self.igrupo).nombre, formula=self.formula,
          nombre_productor=self.pedido.productor.nombre,
          error_type=e.__class__, error_message=error_message)).xml()),
                        A(T('Cambia la fórmula'),
                          _href = URL(
                             c='gestion_pedidos',
                             f=('edita_pedido_coordinado.html' if self.coordinado else
                                'edita_pedido.html'),
                             vars=dict(pedido=self.ipedido, grupo=self.igrupo)),
                          _target='_blank')))
                else:
                    raise e
        # problemas_calculo_coste_extra se ajusta solo desde el grupo si es un pedido propio,
        #  o la red primigenia para un pedido coordinado, después de haber recopilado todas las
        #  incidencias de las subredes y subgrupos
        if not self.coordinado:
            self.pedido.update_record(problemas_calculo_coste_extra=bool(incidencias))
        return incidencias

class TraductorFormula(object):
    operators = {ast.Add: '+', ast.Sub: '-', ast.Mult: '*',
                 ast.Div: '/', ast.Pow: '^', ast.BitXor: '^',
                 ast.USub: '-'}
    comparators = {ast.Eq: '=', ast.NotEq: '<>', ast.Lt: '<',
                   ast.LtE: '<=', ast.Gt: '>', ast.GtE: '>='}

    COL_UNIDAD_1 = 4

    def __init__(self, formula,
                 row_min, row_max,
                 row_coste_total, col_coste_total,
                 row_campos_pedido_min, row_campos_pedido_max,
                 dict_cols_extra,
                 unidades,
                 cota_sup_num_filas=5,
                 locale='es_ES',
                 **attributes):
        self.row_min = row_min
        self.row_max = row_max
        self.row_coste_total = row_coste_total
        self.col_coste_total = col_coste_total
        self.row_campos_pedido_min = row_campos_pedido_min
        self.row_campos_pedido_max = row_campos_pedido_max
        self.dict_cols_extra = dict_cols_extra
        self.cota_sup_num_filas = cota_sup_num_filas
        self.unidades  = unidades
        self.tree = ast.parse(formula, mode='eval').body
        if locale.startswith('es'):
            self.print_nums = lambda n: pprint_c(Decimal(str(n))).replace('.',',')
        else:
            self.print_nums = lambda n: pprint_c(Decimal(str(n)))
        self.var_cache = attributes

    def _eval(self, node):
        if isinstance(node, ast.Num): # <number>
            return self.print_nums(node.n)
        elif isinstance(node, ast.BinOp): # <left> <operator> <right>
            left  = self._eval(node.left)
            right = self._eval(node.right)
            if type(node.op)==ast.Div:
                return 'IF(%s=0;0;%s/%s)'%(left,left,right)
            return '(%s%s%s)'%(left, self.operators[type(node.op)], right)
        elif isinstance(node, ast.UnaryOp): # <operator> <operand> e.g., -1
            return self.operators[type(node.op)] + self._eval(node.operand)
        elif isinstance(node, ast.Name): # <variable name>
            return self.get_var(node.id)
        elif isinstance(node,ast.Call):
            func_name = node.func.id.upper()
            if func_name=='IF':
                if len(node.args)!=3:
                    raise CostesExtraException(T(
'''Un condicional "IF" debe tener siempre tres argumentos: el primero es la condición,
el segundo es una expresión que se ejecutará si la condición es cierta,
y el tercero es una expresión que se ejecutará si la condición es falsa.'''
                ))
                return 'IF(%s;%s;%s)'%tuple(map(self._eval, node.args))
            return '%s(%s)'%(func_name,
                             ';'.join(self._eval(a) for a in node.args))
        elif isinstance(node, ast.Compare):
            assert len(node.ops) == 1
            assert len(node.comparators) == 1
            op = node.ops[0]
            comp = self._eval(node.comparators[0])
            left = self._eval(node.left)
            return '(%s%s%s)'%(left, self.comparators[type(op)], comp)
        else:
            raise CostesExtraException(current.T('Sólo se permiten operaciones aritméticas'))

    def get_var(self, name):
        T = current.T
        if name.endswith('_GRUPO') and (name in self.var_cache):
            return self.var_cache[name]
        if name=='COSTE_GRUPO':
            v = ref(self.row_coste_total, self.col_coste_total, dolar=ref.ROWCOL)
        elif name=='COSTE_GRUPO_EN_PEDIDOS_RED':
            v = 'SUMIF(%s:%s;"TOTAL PRODUCTOS";%s:%s)'%(
                ref(2, 0, dolar=ref.ROWCOL),
                ref(self.cota_sup_num_filas, 0, dolar=ref.ROWCOL),
                ref(2, 2, dolar=ref.ROWCOL),
                ref(self.cota_sup_num_filas, 2, dolar=ref.ROWCOL))
        elif name=='UNIDADES_EN_PEDIDOS_DE_LA_RED':
            v = 'VLOOKUP("%s";%s:%s;2;0)'%(
                T('UNIDADES QUE PARTICIPAN'),
                ref(0, 0, dolar=ref.ROWCOL),
                ref(self.cota_sup_num_filas, 1, dolar=ref.ROWCOL)
            )
        elif name=='COSTE_UNIDAD':
            v = ref(self.row_max + 1, self.col)
        elif name=='COSTE_UNIDAD_EN_PEDIDOS_RED':
            v = 'SUMIF(%s:%s;"%s";%s:%s)'%(
                ref(2, 0, dolar=ref.ROWCOL),
                ref(self.cota_sup_num_filas, 0, dolar=ref.ROWCOL),
                T('TOTAL UNIDAD'),
                ref(2, self.col, dolar=ref.ROW),
                ref(self.cota_sup_num_filas, self.col, dolar=ref.ROW))
        elif name=='HA_PEDIDO_A_LA_RED':
            v = 'IF(%s>0;1;0)'%self.get_var('COSTE_UNIDAD_EN_PEDIDOS_RED')
        elif name=='CANTIDAD_GRUPO':
            v = 'SUM(%s:%s)'%(ref(self.row_min, self.COL_UNIDAD_1),
                              ref(self.row_max, self.COL_UNIDAD_1+len(self.unidades)-1))
        elif name=='CANTIDAD_UNIDAD':
            v = 'SUM(%s:%s)'%(ref(self.row_min, self.col), ref(self.row_max, self.col))
        elif name=='UNIDADES_EN_PEDIDO':
            v = 'COUNTIF(%s:%s;">0")'%(ref(self.row_max + 1, self.COL_UNIDAD_1, dolar=ref.ROWCOL),
                                       ref(self.row_max + 1, self.COL_UNIDAD_1+len(self.unidades)-1,
                                           dolar=ref.ROWCOL))
        elif name=='HA_PEDIDO':
            v = 'IF(%s>0;1;0)'%ref(self.row_max + 1, self.col)
        elif name.endswith('_GRUPO'):
            var = name.rpartition('_')[0]
            v = '''SUMPRODUCT(%(row_cantidad_min)s:%(row_cantidad_max)s;%(row_var_min)s:%(row_var_max)s)'''%dict(
                row_cantidad_min=ref(self.row_min, 2),
                row_cantidad_max=ref(self.row_max, 2),
                row_var_min=ref(self.row_min, self.dict_cols_extra[var]),
                row_var_max=ref(self.row_max, self.dict_cols_extra[var])
            )
        elif name.endswith('_UNIDAD'):
            var = name.rpartition('_')[0]
            v = '''SUMPRODUCT(%(row_cantidad_min)s:%(row_cantidad_max)s;%(row_var_min)s:%(row_var_max)s)'''%dict(
                row_cantidad_min=ref(self.row_min, self.col),
                row_cantidad_max=ref(self.row_max, self.col),
                row_var_min=ref(self.row_min, self.dict_cols_extra[var]),
                row_var_max=ref(self.row_max, self.dict_cols_extra[var])
            )
        else:
            v = 'VLOOKUP("%s";%s:%s;2;0)'%(name,
                ref(self.row_campos_pedido_min, 0, dolar=ref.ROWCOL),
                ref(self.row_campos_pedido_max, 1, dolar=ref.ROWCOL) )

        self.var_cache[name] = v
        return v

    def formula_traducida(self, unidad):
        self.col = self.COL_UNIDAD_1 + bisect_left(self.unidades, unidad)
        return self._eval(self.tree)

def chequea_coste_extra_en_form(form):
    for f in ('formula_coste_extra_pedido', 'formula_coste_extra_productor'):
        if f in form.fields:
            formula = form.vars[f]
            if not formula:
                form.vars[f] = '0'
            else:
                error = _chequea_formula(formula)
                if error:
                    form.errors[f] = error

def _chequea_formula(formula):
    T = current.T
    try:
        tree = ast.parse(formula, mode='eval').body
    except SyntaxError as e:
        return ' (%s)'%','.join(str(arg) for arg in e.args)
    lvalid_nodes = list(TraductorFormula.operators.keys())
    lvalid_nodes.extend(iter(TraductorFormula.comparators.keys()))
    lvalid_nodes.extend([ast.Num, ast.Compare, ast.BinOp, ast.UnaryOp, ast.Name, ast.Load])
    tvalid_nodes = tuple(lvalid_nodes)
    for node in ast.walk(tree):
        if isinstance(node, ast.Call):
            try:
                func_name = node.func.id.lower()
            except AttributeError:
                return T('Problema al evaluar la formula: %s')%ast.dump(node)
            if func_name=='if':
                if len(node.args)!=3:
                    return T('Los condicionales deben escribirse IF(condicion, si_es_cierto, si_es_falso)')
            elif func_name not in CalculadoraCostesExtra.functions:
                return CalculadoraCostesExtra.mensaje_funcion_no_permitida()
        elif isinstance(node, ast.Tuple):
            return T('Problema al evaluar la formula. Recuerda usar el punto decimal en vez de la coma.')
        elif not isinstance(node, tvalid_nodes):
            return T('Operacion no permitida en la fórmula: ')+type(node)
