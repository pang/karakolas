# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################
#
#TODO: Este modulo no da errores, pero necesita algún ajuste. Más info en taiga
#
#Objetivo: adivinar la categoria a partir del nombre, usando las clasificaciones
#previas que ha hecho el usuario. Usamos el metodo Naive Bayes, pero con algun
#apaño y cuidando la distribucion a priori
#La documentacion de scikits.learn es muy clara:
#http://scikit-learn.org/stable/modules/naive_bayes.html
#pero a la hora de usarlo, me parecio mas facil escribir el codigo yo, y
#controlar mejor la prior despues de un rato probando y pensando, me quede con
#la multinomial
#http://scikit-learn.org/stable/modules/naive_bayes.html#multinomial-naive-bayes

import numpy
import pickle
import os.path
import re

from gluon import current

TAKE_MODE_THRESHOLD   = 1.25
RECOMPUTE_ON_NEW_DATA = 1.10
LIDSTONE_ALPHA = 0.5

NOT_A_LETTER = re.compile('[^a-zA-Z]+')

class Bayes_Filter(object):
    def __init__(self):
        self.CAT_NINGUNA = current.db.categorias_productos(codigo=-1).id
        try:
            self.pickle_load()
            #Mejor recalcular solo de noche...
#            self.recalcula_si_nec()
        except IOError as EOFError:
            self.restart()
            self.pickle_dump()

    def pickle_load(self, picklepath=None, foreign=False):
        if not picklepath:
            picklepath = os.path.join(current.request.folder,'private','bayes.pickle')
        pickload = pickle.load(open(picklepath ,'rb'))
        if len(pickload)==6:
            self.M, self.prior, self.dns, self.dcats, self.dcatcodigos, self.data_size = pickload
        else:
            self.M, self.prior, self.dns, self.dcats, self.data_size = pickload
            self.dcatcodigos = {}
        if foreign and self.dcatcodigos:
            '''Importa bayes.pickle de otra instalación de karakolas:
              - puede tener más categorías => si el clasificador adivina una categoría
                que no existe en esta instalación, devolvemos CAT_NINGUNA
              - puede que en el pickle externo no estén no todas las categorías locales
                => las ignoramos, nunca serán las categoría adivinada hasta que hagamos
                   un restart
              - los ids de la categorías son distintos => hay que mapear
            '''
            db = current.db
            dict_codigos_local = {
                row.codigo:row.id for row in db(db.categorias_productos).select()
            }
            # Los códigos del pickle externo que no están en la instalación local
            # los mapeamos a ids negativos, que luego serán ignorados en adivina_categorias
            self.dcats = {
                dict_codigos_local.get(codigo,-j):fila
                for j,(fila,codigo) in enumerate(self.dcatcodigos.items())
            }

    def pickle_dump(self, picklepath=None):
        if not picklepath:
            picklepath = os.path.join(current.request.folder,'private','bayes.pickle')
        pickle.dump(
            (self.M, self.prior, self.dns, self.dcats, self.dcatcodigos, self.data_size) ,
            open(picklepath ,'wb'),
            protocol=4
        )

    def _query_productos_en_pedidos_fantasma(self):
        db = current.db
        tppp = db.productoXpedido
        return ((tppp.categoria!=self.CAT_NINGUNA) &
                (tppp.pedido==db.pedido.id) &
                (db.pedido.fecha_reparto==current.C.FECHA_FANTASMA) &
                (db.pedido.productor==db.productor.id) &
                (db.productor.reglas==''))

    def restart(self):
        db = current.db
        tppp = db.productoXpedido
        catrecords = db(db.categorias_productos.codigo>0).select()
        dcatcodigos = {j:r.codigo for j,r in enumerate(catrecords)}
        dcats = {r.id:j for j,r in enumerate(catrecords)}
        ncats = len(dcats)
        prior = [1.0]*ncats

        rows = db(self._query_productos_en_pedidos_fantasma()) \
              .select(tppp.nombre, tppp.categoria)
        self.data_size = len(rows)
        ns = set()
        for r in rows:
            for w in NOT_A_LETTER.split(r.nombre):
                if len(w)>2:
                    ns.add(w.lower())
            prior[dcats[r.categoria]] += 1
        total = sum(prior)
        self.prior = numpy.array(prior)/total

        #Alternativa mas rapida para prior, pero hace mas probables las categorias con productos cuyo nombre tiene mas palabras
        #prior = [sum(M[j,:]) for j in xrange(ncats)]

        dns = dict((w,j) for j,w in enumerate(ns) if len(w)>2)
        nns = len(dns)

        M = LIDSTONE_ALPHA*numpy.ones((ncats,nns), dtype=numpy.float64)
        for r in rows:
            for w in NOT_A_LETTER.split(r.nombre):
                if len(w)>2:
                    M[dcats[r.categoria],dns[w.lower()]] += 1

        self.dcats = dcats
        self.dcatcodigos = dcatcodigos
        self.dns   = dns
        self.M     = M

    def recalcula_si_nec(self):
        db = current.db
        N  = db(self._query_productos_en_pedidos_fantasma()).count()

        if self.data_size*RECOMPUTE_ON_NEW_DATA < N:
            self.restart()
            self.pickle_dump()

    def posterior(self, xs, y, first_guess=None):
        if first_guess:
            pass #TODO
        p = self.prior[y]/sum(self.M[y,:])**(len(xs))
        for x in xs:
            p *= self.M[y,x]
        return p

    def adivina_categorias(self,nombre_producto, first_guess=None):
        #fix si solo hay una categoria (por ej al arrancar)
        if not self.dcats:
            return self.CAT_NINGUNA
        elif len(self.dcats)==1:
            return next(iter(self.dcats))
        words = [w.lower() for w in NOT_A_LETTER.split(nombre_producto)]
        wcols = [self.dns.get(w,-1) for w in words if len(w)>2]
        wcols = [wcol for wcol in wcols if wcol>=0]
        ls = [(self.posterior(wcols,crow),cid) for cid,crow in self.dcats.items()]
        ls.sort(reverse=True)
        first_prob, first_cat = ls[0]
        second_prob, _        = ls[1]
        if first_prob>TAKE_MODE_THRESHOLD*second_prob and first_cat>0:
            return first_cat
        else:
            return self.CAT_NINGUNA
