# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

from gluon.html import URL, FORM, CAT, DIV, UL, LI, A, SCRIPT, XML
from gluon import current
from collections import defaultdict

from .kutils import random_string

class JQPlot(FORM):
    def __init__(self, data, rows_per_plot=10, **attributes):
        '''Muestra una gráfica ligeramente interactiva para visualizar
        una relacion que representa una funcion (ref1,ref2) -> valor
        '''
        FORM.__init__(self, **attributes)
        self.add_static_files()
        self.tab_list = data.split_by_row(rows_per_plot)
        self.rows_per_plot = rows_per_plot
        self.nrows = len(data.row_ids)

    @staticmethod
    def add_static_files():
        current.response.files.append(URL('static', 'jqplot/jquery.jqplot.min.js'))
        current.response.files.append(URL('static', 'jqplot/jquery.jqplot.min.css'))
        current.response.files.append(URL('static', 'jqplot/plugins/jqplot.barRenderer.min.js'))
        current.response.files.append(URL('static', 'jqplot/plugins/jqplot.categoryAxisRenderer.min.js'))
        current.response.files.append(URL('static', 'jqplot/plugins/jqplot.canvasTextRenderer.js'))
        current.response.files.append(URL('static', 'jqplot/plugins/jqplot.canvasAxisTickRenderer.js'))
        current.response.files.append(URL('static', 'jqplot/plugins/jqplot.pointLabels.min.js'))
        current.response.files.append(URL('static', '/jquery-ui/themes/base/jquery-ui.css'))
        current.response.files.append(URL('static', '/jquery-ui/ui/jquery-ui.js'))

    def xml(self):
        if self.nrows==0:
            return ''
        if len(self.tab_list)<=1:
            return self.xml_single(self.tab_list[0])
        code = random_string(len=12)
        return CAT(DIV(
            UL(*[LI(A('%d:%d'%(j*self.rows_per_plot+1,
                               min(self.nrows, (j+1)*self.rows_per_plot)),
                      _href='#%s_%d'%(code,j)))
                 for j in range(len(self.tab_list))]),
            CAT(*[DIV(XML(self.xml_single(data)), _id='%s_%d'%(code,j))
                  for j,data in enumerate(self.tab_list)]),
            _id = 'tab_%s'%code
            ), SCRIPT('''$(document).one('scripts_loaded', function() {$( "#tab_%s" ).tabs();});'''%code)
        ).xml()

    def xml_single(self, data):
        d = defaultdict(float)

        table = [[float(data.table_data[r,c]) for c in data.col_ids] for r in data.row_ids]
        series_labels = ','.join("{label:'%s'}"%r for r in data.row_names)
        ticks = '[%s]'%','.join("'%s'"%k for k in data.col_names)

        d = dict(data=table, ticks=ticks, series_labels=series_labels,
                 chart_code=random_string(len=12))
        return current.response.render('jqplot.html', d)
