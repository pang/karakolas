# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

import csv
import io
from decimal import Decimal
import datetime

from gluon import current

from .exportador import Formula, MatrixFormula, Precio, Cantidad, Numero
from .kutils import pprint_c, pprint_p, represent

class UnicodeWriter:
    """
    A CSV writer to a string, without unicode errors
    """

    def __init__(self, locale='es_ES', **kwds):
        # Redirect output to a queue
        self.queue = io.StringIO()
        self.writer = csv.writer(self.queue, **kwds)
        self.locale = locale
        if locale.startswith('es'):
            local_print_precio = (lambda s:pprint_p(s).replace('.',',') )
            local_print_cantidad = (lambda s:pprint_c(s).replace('.',',') )
            local_print_nums = (lambda s:local_print_cantidad(Decimal(s)).replace('.',',') )
        else:
            local_print_precio = pprint_p
            local_print_cantidad = pprint_c
            local_print_nums = (lambda s:local_print_cantidad(Decimal(s)) )
        self.local_print_funcs = {
            Precio:local_print_precio,
            Cantidad:local_print_cantidad,
            Numero:local_print_nums
            }

    def dump_cell(self, cell_data):
        if isinstance(cell_data, (datetime.date, datetime.datetime)):
            return cell_data.isoformat()
        elif isinstance(cell_data, datetime.time):
            return cell_data.strftime("PT%HH%MM%SS")
        elif isinstance(cell_data, bool):
            # Bool condition must be checked before numeric because:
            # isinstance(True, int): True
            # isinstance(True, bool): True
            return T("VERDADERO") if cell_data else T("FALSO")
        elif isinstance(cell_data, (Precio, Cantidad, Numero)):
            return self.local_print_funcs[type(cell_data)](cell_data)
        elif isinstance(cell_data, (float, int, Decimal)):
            return Decimal(str(cell_data))
        elif isinstance(cell_data, (Formula, MatrixFormula)):
            return '=' + str(cell_data)
        elif cell_data is None:
            return ''  # Empty element
        else:
            return cell_data

    def writerow(self, row):
        self.writer.writerow([self.dump_cell(s) for s in row])

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)

    def dump(self):
        return self.queue.getvalue()

def csv_dump_sheet(data, locale=None):
    '''Toma una lista de listas y devuelve el texto csv
    '''
    locale = locale or current.T.accepted_language
    csv_writer = UnicodeWriter(locale=locale, delimiter=',', quotechar='"')
    csv_writer.writerows(data)
    return csv_writer.dump()
