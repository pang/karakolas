# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

from collections import defaultdict
import re
import random
from decimal import Decimal

import gluon.sqlhtml
from gluon.storage import Storage
from gluon.html import URL, P, A, IMG, XML, DIV, SPAN, TAG, \
                       TABLE, THEAD, TBODY, TH, TR, TD, UL, LI, \
                       FORM, FIELDSET, LABEL, INPUT, TEXTAREA, SELECT, OPTION, \
                       SCRIPT, CAT, xmlescape
from gluon.dal import DAL
from gluon import current
from functools import reduce

################### TODO #############################
######################################################
#
# - Permite seleccionar rangos, y luego poner el texto en el portapapeles,
#adecuadamente tabulados. Este trabajo esta empezado en ROSheet, en el script js4rosheet
#
################### Widgets ##########################
######################################################

#Repito código de gluon.sqlhtml, pero cambiando el INPUT por un TEXTAREA
#Necesario para que funcione pegar rangos
#Pero por otro lado, se puede simplificar, porque sobreescribo el valor de class
#que diferencia por ej MyIntegerWidget de MyStringWidget

class DefaultStorage (Storage):
    def __getitem__(self, item):
        return Storage.get(self, item, None)

class MyStringWidget(gluon.sqlhtml.FormWidget):
    _class = 'string'

    @classmethod
    def widget(cls, field, value, **attributes):
        """
        generates an INPUT text tag.

        see also: :meth:`FormWidget.widget`
        """

        default = dict(
            _type = 'text',
            value = (not value is None and str(value)) or '',
            _cols=20, _rows=1,
            )
        attr = cls._attributes(field, default, **attributes)

        return TEXTAREA(**attr)


class MyIntegerWidget(MyStringWidget):
    _class = 'integer'


class MyDoubleWidget(MyStringWidget):
    _class = 'double'


class MyDecimalWidget(MyStringWidget):
    _class = 'decimal'


class MyTimeWidget(MyStringWidget):
    _class = 'time'


class MyDateWidget(MyStringWidget):
    _class = 'date'


class MyDatetimeWidget(MyStringWidget):
    _class = 'datetime'

##################### smart update #################
def _update_or_insert(table, keys, values):
    record = table(**keys)
    if record:
        record.update_record(**values)
        newid = record.id  #aqui diverjo de update_or_insert
    else:
        newid = table.insert(**values)
    return newid

def smart_update(table, query, rows, key,
                 action=lambda s:s.delete(),
                 params = None):
    '''If a row (from rows) is already present in db(query), it is updated.
    If it is not, it is inserted.
    If a record is in db(query), but not in rows, it is deleted.

    rows is a list of dict's, as in table.insert(**row)
    '''
    if not query:
        return {}
    _db = table[table.fields[0]].db
    keys2id = {}
    for row in rows:
        key0 = row[key]
        keys0d = {key:key0}
        if params: keys0d.update(params)
        id = _update_or_insert(table, keys0d, row)
        keys2id[key0] = id

    ids_query = set(row.id for row in _db(query).select(table.id))
    removed   = ids_query.difference(list(keys2id.values()))
    if removed:
        action( _db(query & table.id.belongs(removed)) )
    return keys2id

def merge_dicts(d1,d2):
    d = dict(d1)
    d.update(d2)
    return d

#################### ERTable #######################
####################################################

ert_script_add_rows = '''<script type="text/javascript">
add_rows_%(id)s = function(rows){
    var table  = $('#ertable_%(id)s').find('tbody');
    var ufila  = $('#_ultima_fila_%(id)s').val();
    var fila0  = $('#_sample_row_%(id)s').val();
    for(var i=0;i<rows;i++){
        var fila = fila0.replace(/(cell_[A-Za-z_0-9\-]*_)0/g, '$1'+ufila).replace('ertable_%(id)s_row_0','ertable_%(id)s_row_'+ufila);
        table.append(fila);
        ufila++;
    }
    $('#_ultima_fila_%(id)s').val(ufila);
    prepare_paste_behaviour_%(id)s();
    prepare_navigation_%(id)s();
//    jQuery('[multiple]').multiselect(); //we no longer support the multiselect plugin
    $('#ertable_%(id)s').trigger('nueva_fila');
}
</script>
'''

ert_script_add_columns = '''<script type="text/javascript">
//TODO: replacements = {' ':'_', 'á':'a', 'é:'e', 'í':'i', 'ó':'o', 'ú':'u', 'ñ','n'};
delete_col_%(id)s = function(col_name){
    $('*[id^="ertable_%(id)s_cell_' + col_name + '"]').parent().detach();
    $('[id^="ertable_%(id)s_th_' + col_name+ '"]').detach();
}

nueva_columna_%(id)s = function(){
    var trs = $('#ertable_%(id)s tr');
    var nombre = $('#ertable_%(id)s_nombre_nueva_columna').val();
    //ya uso la barra baja para los ids de las celdas
    //asi que uso el guion para separar palabras en los nombres de columna extra
    //da problemas con el sql al usar los nombres de columna extra como alias en los left join
    //pero se pueden resolver
    //12-03-18 pang: para curarnos en salud, solo dejo letras y numeros, sin acentos ni eñes
    var nombre_n = nombre.replace(/[^a-zA-Z0-9]/g,'-').toLowerCase();
    var tipo_largo = $('#ertable_%(id)s_tipo_nueva_columna').val();
    var tipo;
    if (tipo_largo.substring(0,7)=='decimal'){
        tipo='decimal';
    }else{
        tipo=tipo_largo;
    }

    if(!nombre_n){
        alert('Por favor escribe un nombre de columna y un tipo de datos');
        return true;
    }
    var fields = $('#_ertable_%(id)s_fields').val();
    var field_array = fields.split(',');
    for(var k=0;k<field_array.length;k++){
        if(nombre_n===field_array[k]){
            alert('Este nombre de columna ya está en uso');
            return true;
        }
    }
    var sample_td = $('#_ertable_%(id)s_sample_td_'+tipo).val();
    var my_td = sample_td.replace(/new_field_name_0/g,nombre_n+'_XXX');

    var sample_th = $('#_ertable_%(id)s_sample_th').val();
    trs[0].innerHTML += sample_th.replace(/new_field_name/g,nombre_n).replace(/new_field_type/g,tipo_largo);
    for(var j=1; j<trs.length; j++){
        var rid = trs[j].id.split('_')[3];
//no puedo hacer trs[j].innerHTML += my_td.replace(/XXX/g,rid);
//porque innerHTML no se actualiza al introducir valores en los campos
//asi que los campos vuelven a sus valores por defecto
        $('#ertable_%(id)s tbody tr:nth-child('+j+')').append(my_td.replace(/XXX/g,rid) );


    }
    var fila0  = $('#_sample_row_%(id)s').val();
    var new_sample_row = fila0.slice(0,-5) + sample_td.replace(/new_field_name_0/g,nombre_n+'_0'); +'</tr>';
    $('#_sample_row_%(id)s').val(new_sample_row);

    $('#_ertable_%(id)s_fields').val(fields + ',' + nombre_n);
    prepare_paste_behaviour_%(id)s();
    prepare_navigation_%(id)s();
    $('.delete_col_button').show();
    $('#ertable_%(id)s').trigger('nueva_columna');
}
</script>
'''

ert_script_paste_rtable = r'''<script type="text/javascript">
strippable = '" \t\n' + "'";
strip = function(s){
    var j, startj=0, endj=s.length-1;
    for(j=0; j<s.length;j++){
        if(strippable.indexOf(s[j])<0){
            startj=j;
            break;
        }
    }
    for(j=s.length-1;j>=0;j--){
        if(strippable.indexOf(s[j])<0){
            endj=j;
            return s.substring(startj, endj+1);
        }
    }

}

text2array = function(text){
            if (!text.match(/[\r\n\t]/)){
                return [[text]];
            }

            //Eliminar retornos de linea entrecomillados
            //Se puede mejorar
            var lastj = 0;
            var in_commas = false;
            var ftext ='';
            for(var j = 0; j<text.length; j++){
                if(text[j]=='"'){
                    if(in_commas){
                        ftext += text.substring(lastj, j).replace(/(\r\n|\n|\r)/gm,'');
                    }
                    else{
                        ftext += text.substring(lastj, j);
                    }
                    in_commas = !in_commas;
                    lastj=j;
                }
            }
            ftext += text.substring(lastj);
            text=ftext;

            var filas  = (text+'\n').split(/\r\n|\r|\n/);
            var result = [];
            var i = 0;
            for(var i=0;i<filas.length;i++){
                var fila   = filas[i];
                if(!fila.trim()){
                    continue;
                }
//                    var celdas = fila.split(/[\t,]/);
                var celdas = [];
                if(fila.match(/\t/)){
                    var preceldas = fila.split(/\t/);
                    for (var g=0;g<preceldas.length;g++){
                        celdas.push(strip(preceldas[g]))
                    }
           
                }else if(fila.match(/"/)){ 
                    //Vaya mierda: no se si se puede a golpe de regex, asi que parseo.
                    fila = fila+',';
                    var f1 = -1;
                    var f2 = -1;
                    var last = 0;
                    for(var f=0;f<fila.length;f++){
                    if(fila[f]=='"'){
                        if(f1<0){
                            f1=f;
                        }
                        else if(f2<0){
                            f2=f;
                        }
                        else{
                            continue;//ERROR
                        }
                    }
                    else if(fila[f]===','){
                        if(f1<0 && f2<0){
                            celdas.push(strip(fila.substring(last,f)));
                            last=f+1;
                        }
                        else if(f1>=0 && f2>0){
                            celdas.push(strip(fila.substring(last+1,f-1)));
                            last=f+1;
                            f1=-1;
                            f2=-1;
                        }
                      }
                    }
                }else{
                    celdas = fila.split(/,/);
                }
                result.push(celdas);
            }
           
        return result;
}

prepare_paste_behaviour_%(id)s = function() {
    $("#ertable_%(id)s textarea").bind('paste', function(e) {
        var n = this.name;
        $('#'+n).val('');
        $('#_ertable_%(id)s_paste_into_cell').val(n);
        // Short pause to wait for paste to complete
        setTimeout( function() {
            var target = $('#_ertable_%(id)s_paste_into_cell').val();
            var text   = $('#'+target).val();
            var fields  = $('#_ertable_%(id)s_fields').val().split(',');
            var ls = target.split('_');
                       
            var pfila = parseInt(ls[ls.length-1]);
            var ufila  = $('#_ultima_fila_%(id)s').val();
           
            var pcol = ls.slice(3,-1).join('_');
            var pcolu = 0;
            while((pcolu<fields.length) & (fields[pcolu]!=pcol)){
                pcolu++;
            }
            var ucolu = parseInt(fields.length);

            var lines = text2array(text);
           
            var ifila  = pfila;
            for(var i = 0; i<lines.length; i++){
                var celdas = lines[i];
                var j = 0;
                var icolu = pcolu;
                while((icolu<ucolu) && (j<celdas.length)){
                    var ctag  = $('#ertable_%(id)s_cell_'+fields[icolu]+'_'+ifila);
                    var celda = celdas[j]?celdas[j].trim():'';
                    //http://stackoverflow.com/questions/149573/check-if-option-is-selected-with-jquery-if-not-select-a-default
                    if(ctag[0].tagName=='SELECT'){
                        $('#ertable_%(id)s_cell_'+fields[icolu]+'_'+ifila+' option:contains("'+celda+'")').attr('selected', 'selected');
                    }else if(ctag[0].type=='checkbox'){
                        if(celda){
                            $('#ertable_%(id)s_cell_'+fields[icolu]+'_'+ifila).attr('checked', 'checked');
                        }
                    }else{
                        $('#ertable_%(id)s_cell_'+fields[icolu]+'_'+ifila).val(celda);
                    }
                    j++;
                    icolu++;
                }
                var next_row = $('#ertable_%(id)s_row_'+ifila).next()[0];
                if(!next_row){
                    break;
                }
                ifila = next_row.id.split('_')[3];
            }
        }, 100);
    });
}
$(document).ready(prepare_paste_behaviour_%(id)s);
</script>
'''

#TODO: Al cambiar de tamaño un textarea de un RTable, se cambia toda la fila o columna (usa el evento resize: http://api.jquery.com/resize/)
#Despues recicla para un RWSheet, si puedes (fixheadertable sera un estorbo, o quiza una inspiracion, por su uso de colgroup y col)
ert_script_resize_rtable = r'''

'''

#Keyboard navigation
ert_script_navigation = r'''<script type="text/javascript">
prepare_navigation_%(id)s = function(){
    $("#ertable_%(id)s textarea, #ertable_%(id)s input").bind('keydown', function(e) {
        var next_row, next_cell, next_row_id, fieldname, myrow;
        if(e.keyCode==13 || e.keyCode==40 || e.keyCode==38){//enter, down arrow or up arrow
            try{
                fieldname = this.id.split('_').slice(3,-1).join('_');
                myrow = $(this).parent().parent();
                if(e.keyCode==13 || e.keyCode==40){
                    next_row = myrow.next();
                }else if(e.keyCode==38){
                    next_row = myrow.prev();
                }
                next_row_id = next_row[0].id.split('_')[3]
                next_cell = next_row.find('#ertable_%(id)s_cell_' + fieldname + '_' + next_row_id);
                next_cell.focus();
                next_cell.select();
            }catch(_){}
            e.preventDefault();
        }else if(e.keyCode==39 || e.keyCode==37){//right or left arrow
            //Forget this, TAB is good enough, arrows are needed to move within the textarea and this can be confusing
        }else if(e.keyCode==9){
            setTimeout(function(){
                $( document.activeElement ).select();
            },100);
        }
    });
$("#ertable_%(id)s input[type='checkbox']").bind('click',function(e){
    this.focus();
});
}
$(document).ready(prepare_navigation_%(id)s);
</script>
'''

#Bootstrap tooltip
ert_script_tooltip = r'''<script type="text/javascript">
$(function() {
    $('[data-toggle="tooltip"]').tooltip();
})
</script>
'''

#TODO: Busca el equivalente a readonly=readonly para select/option en html
ert_fix_master_field = r'''<script type="text/javascript">
$(document).ready(function(){
    $(".ertable_master_field").attr('readonly', 'readonly');
});
</script>
'''

class ERTable(FORM):
    #TODO: custom widget?
    widgets = Storage(dict(
        string = MyStringWidget,
        text = gluon.sqlhtml.TextWidget,
        integer = MyIntegerWidget,
        double = MyDoubleWidget,
        decimal = MyDecimalWidget,
        time = MyTimeWidget,
        date = MyDateWidget,
        datetime = MyDatetimeWidget,
        boolean = gluon.sqlhtml.BooleanWidget,
        options = gluon.sqlhtml.OptionsWidget,
        multiple = gluon.sqlhtml.MultipleOptionsWidget,
        password=gluon.sqlhtml.PasswordWidget,
#        radio = gluon.sqlhtml.RadioWidget,
#        checkboxes = gluon.sqlhtml.CheckboxesWidget,
#        autocomplete = gluon.sqlhtml.AutocompleteWidget,
#        list = gluon.sqlhtml.ListWidget,
        ))

    def __init__(self, table,
                 fields=None,
                 query=None,
                 orderby=None,
                 extra_columns_table=None,
                 extra_columns_values=None,
                 extra_columns_names=None,
                 extra_data=None,
                 data_types_names=None,
                 extra_data_fields=None,
                 url='.',
                 next=None,
                 parsers=None,
                 values=None,
                 add_controls=True,
                 add_new_columns=False,
                 master_field=None,
                 simplify_master_field=None,
                 fix_master_field=False,
                 add_n_rows_at_a_time=(5,25),
                 pagination=0,
                 page=0,
                 extension='html',
                 **attributes):
        '''ERTable builds a spreadsheet-like view that allows to edit a whole table,
        or a slice of it, at once
        '''
        FORM.__init__(self, **attributes)
        self.table  = table
        self.db     = table['id'].db
        self.url    = url
        self.values = values or {}
        self.fields = fields or [f.name for f in table if
                (f.type!='id' and (not f.compute) and f.name not in self.values) ]
        if query:
            self.query = query
        else:
            if values:
                self.query = reduce(lambda a,b:(a & b) ,
                                    [table[t]==v
                                     for t,v in self.values.items() ])
            else:
                #Use without database, ERTable.factory( ... )
                self.query = None
        self._id     = attributes.get('_id','') or str(random.random())[2:]
        self._class  = attributes.get('_class','ertable')
        if self._class != 'ertable': self._class+=' ertable'
        self.next    = next
        self.add_controls = add_controls
        self.add_new_columns = add_new_columns
        self.add_n_rows_at_a_time = add_n_rows_at_a_time
        self.fix_master_field = fix_master_field and master_field
        if not master_field:
            try:
                master_field = next((f.name for f in self.table if f.unique))
            except StopIteration:
                raise NotImplementedError("For this table, you need to specify the master field explicitely")
        self.master_field_name = master_field
        self.master_field = self.table[self.master_field_name]
        if orderby:
            self.orderby = orderby
        else:
            self.orderby =  self.master_field
        if (not (extra_columns_table and extra_columns_values and
                  extra_data and extra_data_fields)
            or self.fix_master_field):
            self.has_extra = self.add_new_columns = False
        else:
            if not self.query:
                raise NotImplementedError("Can't use ERTable with extra fields, but query = None")
            self.has_extra = True
            self.extra_columns_table  = extra_columns_table
            self.extra_columns_values = extra_columns_values
            self.extra_columns_name_field = self.extra_columns_type_field = None

            #Lo que sigue no rula porque no veo una buena forma de distinguir
            #el campo "nombre" del campo "tipo"
#            for f in self.extra_columns_table.fields:
#                t = self.extra_columns_table[f]
#                if f in self.extra_columns_values or f=='id':
#                    pass
#                elif t.type=='string' and not self.extra_columns_name_field:
#                    self.extra_columns_name_field = f
#                elif t.type=='string' and not self.extra_columns_type_field:
#                    self.extra_columns_type_field = f
#                else:
#                    raise Exception, "extra_columns_table must be a set of tuples "\
#                                      "(column_name, column_type)"
            (self.extra_columns_name_field,
             self.extra_columns_type_field) = extra_columns_names

            self.extra_data = extra_data
            (self.extra_data_row_field,
             self.extra_data_column_field,
             self.extra_data_value_field) = extra_data_fields
            self.data_types_names = data_types_names or dict((k,k.split('(')[0]) for k in extra_data)
            #TODO: We might check here that extra_data is consistent

        self._parsers = parsers or {}
        self.simplify_master_field = simplify_master_field or (lambda s:s)

        self.pagination = pagination
        self.page = page
        self.extension = extension
        #If query is None, there is no need for pagination
        if pagination and self.query:
            mf = self.master_field
            self.raw_mfvs = raw_mfvs = [row(mf)
                for row in self.db(self.query).select(mf, orderby=self.orderby)
            ]
            #Simplified values are used for comparison, to identify lowercase and uppercase,
            #or eliminate whitespace issues, for example
            self.all_mfs = all_mfs = [self.simplify_master_field(mfv) for mfv in raw_mfvs]
            nrecs = len(self.all_mfs)
            if nrecs > pagination:
                self.visible_rows = all_mfs[pagination*page:pagination*(page + 1)]
                self.hidden_rows  = all_mfs[:pagination*page]+ all_mfs[pagination*(page + 1):]
                self.hidden_rows_dict = dict(
                    (all_mfs[k], k//pagination)
                    for k in (list(range(min(nrecs,pagination*page))) + list(range(pagination*(page + 1), nrecs)))
                )
                self.paging_query = ((mf>=raw_mfvs[pagination*page]) &
                                     (mf<=raw_mfvs[min(pagination*(page + 1)-1, len(raw_mfvs)-1)]) )
                self.query_page = self.query & self.paging_query
            else:
                self.pagination=0
                self.paging_query = None
                self.query_page = self.query
        else:
            self.query_page = self.query

        self.prepare_components()

    def navigation(self):
        '''Buttons for pagination
        '''
        nrows = len(self.all_mfs)
        r = current.request
        T = current.T
        q = nrows//self.pagination
        number_of_pages = q + (0 if q*self.pagination==nrows else 1)
        page_links = []
        navbar = TAG.nav(UL(
            *[LI(A(k+1, **{
                '_data-toggle':'tooltip', '_data-placement':'bottom',
                '_data-html':'true',
                '_title':xmlescape(T('From %s up to %s')%(
                    self.all_mfs[k*self.pagination],
                    self.all_mfs[min((k+1)*self.pagination, nrows)-1])),
                '_href':URL(c=r.controller, f=r.function,
                            extension=self.extension or r.extension,
                            vars=merge_dicts(r.get_vars, dict(page=k)))
                          }),
                 _class=('active' if k==self.page else ''))
              for k in range(number_of_pages)],
            _class='pagination'
        ))
        return navbar

    #Los ids de estos controles no llevan id porque nunca deberia haber mas de uno.
    def ertable_controls(self):
        return DIV(*[INPUT(_type='submit',
                           _name='save_rtable_data',
                           _value=current.T('Guardar cambios'),
                           _class='btn btn-primary'),
                    ],
                  **dict(_class='rtable_controls'))

    def prepare_extra(self):
        self.query_extra = reduce(lambda a,b:(a & b) ,
                                  [self.extra_columns_table[t]==v
                                   for t,v in self.extra_columns_values.items() ])
        s = self.db(self.query_extra).select()
        self.extra_columns = [(row.id,
                               row[self.extra_columns_name_field],
                               row[self.extra_columns_type_field]) for row in s]

    def widget_of_type(self, field_type):
        if field_type.startswith('decimal'):
            field_type = 'decimal'
        if field_type.startswith('reference'):
            field_type = 'options'
        if field_type.startswith('list:reference'):
            field_type = 'multiple'
        try:
            return self.widgets[field_type].widget
        except AttributeError:
            raise AttributeError('ERTable not implemented for fields of type %s'%field_type)

    def field_widget(self, f):
        field = self.table[f]
        if gluon.sqlhtml.OptionsWidget.has_options(field):
            if  hasattr(field.requires, 'multiple') and field.requires.multiple:
                return gluon.sqlhtml.MultipleOptionsWidget.widget
            return gluon.sqlhtml.OptionsWidget.widget
        field_type = self.table[f].type
        return self.widget_of_type(field_type)

    def new_td(self, widget, content, field, fieldname, typename=None, row_number=-1):
        row_number = row_number if row_number>=0 else self.row_count
        class_type = (self.table[fieldname].type.replace(' ','_').split('(')[0]
                        if hasattr(self.table, fieldname)
                        else typename)
        return TD(
           widget(field, content,
                  _class='ertable_field_%s %s%s'%(
                     typename or fieldname, class_type,
                     ' ertable_master_field' if fieldname==self.master_field_name else '' ),
                  _id='ertable_%s_cell_%s_%d'%(self._id, fieldname, row_number),
                 _name='ertable_%s_cell_%s_%d'%(self._id,  fieldname, row_number))
        )

    def new_tr(self, row=None, row_extra=None, increase_counter=1):
        row = row or dict((f,self.table[f].default) for f in self.fields)
        row_extra = row_extra or defaultdict(str)

        tds = [self.new_td(w, row[f], self.table[f], f)
                for f,w in zip(self.fields, self.field_widgets)]
        if self.has_extra:
            tds += [self.new_td(w, row_extra[f],
                                self.extra_data[t][self.extra_data_value_field],
                                f, t.split('(')[0])
                    for (_,f,t),w in zip(self.extra_columns,
                                    self.extra_field_widgets)]

        mytr = TR(TD(A(IMG(_src=URL('static','images/delete.png'),
                           _width="16", _class='delete_row_button',
                           _alt="delete"),
                       callback=URL(c='default', f='none'), delete='tr' )),
                  *tds,
                  **dict(_id='ertable_%s_row_%d'%(self._id, self.row_count)))
        self.row_count+=increase_counter
        return mytr

    def new_extra_th(self, field, type):
        return TH(INPUT(_type='hidden',
                         _name='ertable_%s_nueva_columna_%s'%(self._id, field),
                         _value=type),
                   field, XML('&nbsp;'),
                   SPAN(XML('&nbsp;x&nbsp;'),
                       _onclick='delete_col_%s("%s")'%(self._id, field),
                       _class='delete_col_button'),
                   XML('&nbsp;'),
                   _class='extra_th',
                   _id='ertable_%s_th_%s'%(self._id,field))

    def prepare_components(self):
        if self.has_extra:
            self.prepare_extra()

        self.row_count = 0
#        cols = COLGROUP(*[COL(_id='ertable_%s_row_%s'%(self._id, f))
#                            for f in self.fields] +
#                         [COL(_id='ertable_%s_row_%s'%(self._id, f))
#                            for _,f,_ in self.extra_columns])

        ths = [TH(f, _id='ertable_%s_th_%s'%(self._id,f))
                                for f in self.fields]
        if self.has_extra:
            ths += [self.new_extra_th(f,t)
                    for _,f,t in self.extra_columns]
        headers = THEAD(TR(TH(), *ths))

        self.field_widgets = [self.field_widget(f) for f in self.fields]
        std_rows = (self.db(self.query_page).select(self.table.id,
                                              *[self.table[f] for f in self.fields],
                                              **dict(orderby=self.orderby))
                    if self.query_page else [])
        vs_extra = dict((row.id,defaultdict(str)) for row in std_rows)
        if self.has_extra and self.query_page:
            self.extra_field_widgets = [self.widget_of_type(t) for _,_,t in self.extra_columns]
            for cid,f,t in self.extra_columns:
                val_table = self.extra_data[t]
                s = self.db((val_table[self.extra_data_column_field] == cid) &
                            (val_table[self.extra_data_row_field] == self.table.id) &
                            self.query_page)\
                    .select(val_table[self.extra_data_value_field],
                            val_table[self.extra_data_row_field])
                for row in s:
                    #The KeyError can happen if some extra data refers to a row
                    #that is not part of self.query
                    try:
                        vs_extra[row[self.extra_data_row_field]][f]=row[self.extra_data_value_field]
                    except KeyError:
                        continue

        sample_row = self.new_tr(increase_counter=0).xml()
        rows     = [self.new_tr(row, row_extra=vs_extra[row.id])
                    for row in std_rows]
        if self.fix_master_field or self.pagination:
            new_rows = []
        else:
            new_rows = [self.new_tr() for _ in range(min(self.add_n_rows_at_a_time))]

        #TODO: incluye colgroup y cols (ver mas arriba un boceto)
        self.html_table = TABLE(headers, *(rows+new_rows),
                                **dict(_class=self._class, _id='ertable_%s'%self._id))
        table = DIV(self.html_table, _class="table-responsive")
        add_rows_buttons = [DIV(*[SPAN(current.T('Añadir %d filas más'%k),
                                  _onclick="add_rows_%s(%d);"%(self._id, k),
                                  _class='btn btn-default')
                               for k in self.add_n_rows_at_a_time],
                               _class='btn-group')]
        c = [INPUT(_type='hidden',
                   _id='_ertable_%s_fields'%self._id,
                   _name='_ertable_%s_fields'%self._id,
                   _value=','.join(self.fields +
                                   [f for _,f,_ in self.extra_columns]
                                   if self.has_extra else self.fields)),
             INPUT(_type='hidden',
                   _id='_ultima_fila_%s'%self._id,
                   _name='_ultima_fila_%s'%self._id,
                   _value=self.row_count),
             INPUT(_type='hidden',
                   _id='_table_name_%s'%self._id,
                   _name='_table_name_%s'%self._id,
                   _value=str(self.table)),
             INPUT(_type='hidden',
                   _id='_ertable_%s_paste_into_cell'%self._id,
                   _name='_ertable_%s_paste_into_cell'%self._id,
                   _value='nombre_0'),
             INPUT(_type='hidden',
                   _id='_sample_row_%s'%self._id,
                   _name='_sample_row_%s'%self._id,
                   _value=sample_row)]
        hidden_fields   = DIV(c, _class="hidden")
        js_paste        = XML(ert_script_paste_rtable%dict(id=self._id))
        js_add_rows     = XML(ert_script_add_rows%dict(id=self._id))
        js_navigation   = XML(ert_script_navigation%dict(id=self._id))
        js_tooltip      = XML(ert_script_tooltip)
        js_fix_mf       = XML(ert_fix_master_field)
        if self.fix_master_field:
            self.components = [table, hidden_fields, js_paste, js_navigation, js_fix_mf]
        else:
            self.components = ([table] +
                               add_rows_buttons +
                               [hidden_fields, js_add_rows, js_paste, js_navigation]
                               )
        if self.pagination:
            self.components.insert(0, self.navigation())
            self.components.append(js_tooltip)
        if self.add_new_columns:
            extra_tds = [(self.new_td(self.widget_of_type(t),
                                      '',
                                      self.extra_data[t][self.extra_data_value_field],
                                      'new_field_name',
                                      t.split('(')[0],
                                      row_number = 0 ) ,
                          t.split('(')[0])
                          for t in self.extra_data]
            sample_tds = [INPUT(_type='hidden',
                                _id='_ertable_%s_sample_td_%s'%(self._id, t),
                                _name='_ertable_%s_sample_td_%s'%(self._id, t),
                                _value=td.xml())
                           for td,t in extra_tds]
            sample_th = INPUT(_type='hidden',
                              _id='_ertable_%s_sample_th'%self._id,
                              _name='_ertable_%s_sample_th'%self._id,
                              _value=self.new_extra_th('new_field_name','new_field_type').xml())
            hidden_data_new_columns = DIV(sample_th, *sample_tds, **dict(_class="hidden"))

            js_add_columns = XML(ert_script_add_columns%dict(id=self._id))

            nombre_nueva_columna = INPUT(_id='ertable_%s_nombre_nueva_columna'%self._id)
            tipo_nueva_columna   = SELECT(*[OPTION(nombre_tipo, _value=tipo)
                                          for (tipo, nombre_tipo) in
                                           sorted(self.data_types_names.items())],
                                  **dict(_id='ertable_%s_tipo_nueva_columna'%self._id))

            boton_nueva_columna  = DIV('Nueva columna', _class='btn btn-default',
                                      _onclick='nueva_columna_%s();'%self._id)
            div_nueva_columna = DIV(FIELDSET(nombre_nueva_columna,
                                             tipo_nueva_columna,
                                             boton_nueva_columna,
                                             hidden_data_new_columns,
                                             js_add_columns),
                                    _id='ertable_%s_div_nueva_columna'%self._id,
                                    _class="nueva_columna")

            self.components.insert(0, div_nueva_columna)

        if self.add_controls:
            self.components.append(self.ertable_controls())
        current.response.files.append(URL('static', 'css/rsheet.css'))
        current.response.headers['X-XSS-Protection'] = '0'

    def parser(self, type, default=None):
        parser = self._parsers.get(type)
        return parser or (lambda v:v)

    def field_parser(self, f):
        return self.parser( f.type )

    def accepts(
        self,
        request_vars,
        formname=None,
#        keepvalues=True,
        onvalidation=None,
        dbio=True,
#        hideerror=False,
#        detect_record_change=False,
        on_delete=lambda s:s.delete()
        ):
        if request_vars.__class__.__name__ == 'Request':
            request_vars=request_vars.post_vars
        request_vars = DefaultStorage(request_vars)
        request_vars_get = request_vars.get

        self.errors.clear()
        self.request_vars = Storage()
        self.request_vars.update(request_vars)
        self.formname = formname or self._id
        # self.keepvalues = keepvalues
        #TODO? deal with formname and keepvalues

        saved = ((request_vars.save_rtable_data) or
                 (request_vars._formname == self.formname))
        if self.add_controls and (not saved):
            return False

        ufila = int(request_vars['_ultima_fila_%s'%self._id])
        #nos aseguramos de que hay suficientes filas extra
        if ufila > self.row_count:
            new_rows = [self.new_tr() for _ in range(ufila-self.row_count)]
            self.html_table.components.extend(new_rows)
            elemento_uf = self.element('#' + '_ultima_fila_%s'%self._id)
            elemento_uf['value'] = self.row_count
            elemento_uf._postprocessing()

        self.good_rows = {}
        self.good_row_raw = {}
        rows = []
        repeated_row_errors = {}
        if self.pagination:
            keys = dict(self.hidden_rows_dict)
        else:
            keys = {}
        mf = self.master_field_name
        parser_mf = self.field_parser(self.master_field)
        table_fields = [self.table[fname] for fname in self.fields]
        parsers = [self.field_parser(f) for f in table_fields]
        pre_validators = [f.requires for f in table_fields]
        validators = [(requires if isinstance(requires, (list, tuple)) else [requires])
                      for requires in pre_validators]
        #Only non-trivial validators:
        validators = [[validator for validator in requires if validator]
                      for requires in validators]
        for i in range(ufila):
            k  = 'ertable_%s_cell_%s_%d'%(self._id, mf, i)
            mfv = request_vars[k]
            key = self.simplify_master_field(mfv or '')
            if key: #non-empty row
                if key in keys:
                    repeated_on_page = keys[key]
                    if repeated_on_page==self.page:
                        errors = current.T('The value for this field was used in a different row')
                    else:
                        errors = current.T('The value for this field is used in page %d')%(repeated_on_page+1)
                    self.errors[k] = errors

                keys[key] = self.page

                #Comprobamos los valores de cada campo
                #codigo adaptado de tools/html.py/FORM._validate
                vs = {}
                is_good_row = True
                for fname, parser, requires in zip(self.fields, parsers, validators):
                    varname = 'ertable_%s_cell_%s_%d'%(self._id, fname, i)
                    #Ejecutamos el parser, que actua sobre el input crudo, por ejemplo
                    #para cambiar una coma por un punto decimal
                    value = parser(request_vars_get(varname, ''))
                    for validator in requires:
                        try:
                            (value, errors) = validator(value)
                        except:
                            msg = "Validation error, field:%s %s" % (fname, validator)
                            raise Exception(msg)
                        if not errors is None:
                            self.errors[varname] = errors
                            is_good_row = False
                            break
                    vs[fname] = value
                    self.vars[varname] = value

                if is_good_row:
                    vs.update(self.values)
                    rows.append(vs)
                    self.good_rows[i] = key
                    self.good_row_raw[i] = parser_mf(mfv)

        ret = (not self.errors)
        if ret and dbio:
            mf2id = smart_update(self.table,
                                 self.query_page,
                                 rows,
                                 self.master_field_name,
                                 action=on_delete,
                                 params=self.values)

            #Columnas extra
            if self.has_extra:
                extra_columns_request = [(v.split('_',4)[-1],t)
                    for (v,t) in request_vars.items()
                    if v.startswith('ertable_%s_nueva_columna_'%self._id)]
                new_column_names = dict((n,t) for n,t in extra_columns_request)
                columns = dict()
                columns_to_delete = []
                for cid,n,t in self.extra_columns:
                    #Si existia otra columna, pero de otro tipo, borramos la columna
                    #Si la columna ha desaparecido, la borramos también
                    if new_column_names.get(n)!=t:
                        columns_to_delete.append(cid)
                    else:
                        columns[n,t] = cid
                self.db(self.extra_columns_table.id.belongs(columns_to_delete)).delete()
                for n,t in extra_columns_request:
                    #Creamos las columnas que no existian
                    if (n,t) not in columns:
                        vs = {self.extra_columns_name_field:n,
                              self.extra_columns_type_field:t}
                        vs.update(self.extra_columns_values)
                        cid = self.extra_columns_table.insert(**vs)
                        columns[n,t] = cid
                        #valor por defecto para nuevas columnas extra si hay paginas no visibles
                        #los valores de la pagina actual seran borrados poco despues, pero
                        #es mas sencillo así
                        if self.paging_query:
                            self.valor_defecto_columna_extra(cid, t)

                for (n,t),cid in columns.items():
                    parser = self.parser(t)
                    extra_table = self.extra_data[t]
                    col_field = extra_table[self.extra_data_column_field]
                    row_field = extra_table[self.extra_data_row_field]
                    q = (col_field==cid) & (row_field==self.table.id)
                    if self.paging_query:
                        q &= self.paging_query
                    extra_values_ids = [row.id for row in self.db(q).select(extra_table.id)]
                    self.db(extra_table.id.belongs(extra_values_ids)).delete()

                    for i,mfv in self.good_row_raw.items():
                        v = parser(request_vars['ertable_%s_cell_%s_%d'%(self._id,n,i)])
                        vs = {self.extra_data_row_field:mf2id[mfv],
                              self.extra_data_column_field:cid,
                              self.extra_data_value_field:v}
                        extra_table.insert(**vs)
        elif self.errors:
            #Si hay errores, queremos mostrar los mensajes de error en su posicion
            #habitual, se hace distinto en TEXTAREA, SELECT, etc: dejamos que lo haga web2py
            def qtraverse(element):
                if hasattr(element, 'components'):
                    for subel in element.components:
                        qtraverse(subel)
                if hasattr(element, 'attributes'):
                    element_id = element.attributes.get('_id')
                    value = self.vars.get(element_id)
                    if value or self.errors.get(element_id):
                        element['value'] = value
                        element.errors = self.errors
                        element._postprocessing()
            qtraverse(self.html_table)
        return ret

    def valor_defecto_columna_extra(self, cid, type):
        represent = current.db._adapter.represent
        extra_table = self.extra_data[type]
        extra_table_name = extra_table.sql_shortref

        default = represent(extra_table[self.extra_data_value_field].default,
                            extra_table[self.extra_data_value_field].type)

        #https://mariadb.com/kb/en/mariadb/insert-select/
        #aviso: no lo hemos probado en postgresql, aunque posiblemente funcione
        #http://www.postgresql.org/docs/current/static/sql-insert.html
        sql = "INSERT INTO %s (%s, %s, %s) SELECT %s, %s, %s FROM %s WHERE (%s);"%(
            extra_table.sql_shortref,
            extra_table[self.extra_data_row_field]._rname,
            extra_table[self.extra_data_column_field]._rname,
            extra_table[self.extra_data_value_field]._rname,
            self.table.id._rname,
            cid,
            default,
            self.table.sql_shortref,
            ' AND '.join('%s=%s'%(self.table[t]._rname, represent(v, self.table[t].type))
                     for t,v in self.values.items())
        )
        current.db.executesql(sql)

    def process_request(self, request_vars, keep_order=True):
        '''call after accepts returns true
        '''
        rows = {}
        for i,t in self.good_rows.items():
            rows[t] = dict((f,request_vars['ertable_%s_cell_%s_%d'%(self._id, f, i) ] )
                           for f in self.fields)
            if keep_order:
                rows[t]['order'] = i
        return rows

    @staticmethod
    def factory(*fields, **attributes):
        """
        generates a ERTable for the given fields.

        Internally will build a non-database based data model
        to hold the fields.
        """
        # Define a table name, this way it can be logical to our CSS.
        # And if you switch from using ERTable to ERTable.factory
        # your same css definitions will still apply.

        table_name = attributes.get('table_name', 'no_table')

        # So it won't interfear with SQLDB.define_table
        if 'table_name' in attributes:
            del attributes['table_name']

        return ERTable(DAL(None).define_table(table_name, *fields),
                       **attributes)

######################### Tabular ############################
##############################################################

class Tabular(object):
    def __init__(self, query=None, row_field=None, col_field=None, data_field=None,
                 row_ids=None, col_ids=None,
                 row_represent=None, col_represent=None, data_represent=None,
                 col_totals=False,
                 orderby=None
                 ):
        '''Tabla con datos a partir de una query indexada por un campo fila y otro columna

        row_ids es una lista de ids de registros a usar en las filas, si el campo
            row_field es de tipo reference
        lo mismo para col_ids
        '''
        if not query:
            #empty Tabular object for use in self.transpose
            return
        if not isinstance(query, list):
            query = [query]
        if not isinstance(data_field, list):
            data_field = [data_field]*len(query)
        #comprueba que todos los data_field tienen el mismo type
        if len(set(df.type for df in data_field)) > 1:
            raise ValueError

        self.row_represent = row_represent or row_field.represent or (lambda x:x)
        self.col_represent = col_represent or col_field.represent or (lambda x:x)
        try:
            self.data_represent = data_represent or data_field[0].represent or (lambda x:x)
        except AttributeError:
            #A f.sum() or f.count() doesn't have a represent attribute
            self.data_represent = (lambda x:x)

        if data_field[0].type.startswith('decimal'):
            dec = re.compile('decimal\((\d+),(\d+)\)')
            ((_,prec),) = dec.findall(data_field[0].type)
            self.data_type = Decimal
        elif data_field[0].type=='float':
            self.data_type = float
        elif data_field[0].type=='integer':
            self.data_type = int
        else:
            raise TypeError('Non numeric data type')
        default_value = self.data_type()

        db = row_field.db

        self.row_ids = row_ids or sorted(set(r[row_field]
            for q in query
            for r in db(q).select(row_field, groupby=row_field, orderby=orderby)
        ))
        self.row_names = [self.row_represent(rid) for rid in self.row_ids]
        self.col_ids = col_ids or sorted(set(r[col_field]
            for q in query
            for r in db(q).select(col_field, groupby=col_field, orderby=orderby)
        ))
        self.col_names = [self.col_represent(cid) for cid in self.col_ids]

        self.table_data = defaultdict(self.data_type)
        isTrivial = True
        for q,df in zip(query,data_field):
            recs = db(q).select(row_field, col_field, df,
                                    groupby=row_field | col_field,
                                    orderby=orderby)
            isTrivial = isTrivial and not bool(recs)
            for q_row in recs:
                r,c,d = q_row[row_field], q_row[col_field], q_row[df]
                self.table_data[r,c] += d or default_value

        self.isTrivial = isTrivial

    def transpose(self):
        t_data = Tabular()
        t_data.row_represent  = self.col_represent
        t_data.col_represent  = self.row_represent
        t_data.data_represent = self.data_represent
        t_data.data_type = self.data_type
        t_data.row_ids = self.col_ids
        t_data.col_ids = self.row_ids
        t_data.row_names = self.col_names
        t_data.col_names = self.row_names
        t_data.isTrivial = self.isTrivial
        table_data = defaultdict(self.data_type)
        for (r,c),v in self.table_data.items():
            table_data[c,r] = v
        t_data.table_data = table_data
        return t_data

    def split_by_row(self, L):
        '''returns a list of Tabular objects of at most L rows
         that, when combined, contain the same info as self
        '''
        from itertools import repeat
        from math import ceil
        tab_list = []
        row_dict = {}
        for q in range(len(self.row_ids)//L + bool(len(self.row_ids)%L)):
            t = Tabular()
            t.row_represent  = self.row_represent
            t.col_represent  = self.col_represent
            t.data_represent = self.data_represent
            t.data_type = self.data_type
            t.col_ids = self.col_ids
            t.col_names = self.col_names
            t.isTrivial = False
            t.table_data = defaultdict(self.data_type)
            t.row_ids = self.row_ids[q*L:(q+1)*L]
            t.row_names = self.row_names[q*L:(q+1)*L]
            row_dict.update(zip(t.row_ids, repeat(q)))
            tab_list.append(t)
        for (r,c),v in self.table_data.items():
            t = tab_list[ row_dict[r] ]
            t.table_data[r,c] = v
        return tab_list

######################### ROSheet ############################
##############################################################

class ROSheet(DIV):
    fixheadertable_script = r'''
$(document).one('scripts_loaded', function(){
    var parent_width = $('#%(id)s').parent().width();
    var col_width = Math.max(parseInt(parent_width/Math.min(6, %(ncols)s)), 80);
    $('#%(id)s').fixheadertable({
        colratio      : Array.apply(null, new Array(%(ncols)s)).map(Number.prototype.valueOf, col_width)
    })
});
'''

    def __init__(self, data,
                 col_totals=False, row_totals=False,
                 col_average=False, row_average=False,
                 col_average_non_null=False, row_average_non_null=False,
                 transpose=False, **kwds):
        if '_id' not in kwds:
            from uuid import uuid4
            kwds['_id'] = 'rosheet_' + str(uuid4())
        if '_class' in kwds:
            kwds['_class'] += ' rosheet'
        else:
            kwds['_class'] = 'rosheet'

        DIV.__init__(self, **kwds)
        self.add_static_files()

        if transpose:
            data = data.transpose()
        repr_data = data.data_represent

        col_headers = TR(TH(''),*[TH(n) for n in data.col_names])
        trows = [TR(TH(data.row_represent(r)),
                    *[TD(repr_data(data.table_data[r, c]),
                         _id='cell_%d_%d'%(j,k))
                      for k,c in enumerate(data.col_ids)])
                 for j,r in enumerate(data.row_ids) ]

        if col_totals or col_average or col_average_non_null:
            col_totals_vals = defaultdict(data.data_type)
            non_null_cols_in_row = defaultdict(int)
            for (r,c),v in data.table_data.items():
                if v:
                    col_totals_vals[r] += v
                    non_null_cols_in_row[r] += 1
            if col_average_non_null:
                col_headers.insert(1, TH(current.T('Media no nulos')))
                for r,tr in zip(data.row_ids, trows):
                    tr.insert(1,
                        TD(repr_data(col_totals_vals[r]/non_null_cols_in_row[r]),
                           _class='total'))
            elif col_average:
                col_headers.insert(1, TH(current.T('Media')))
                ncols = len(data.col_ids)
                for r,tr in zip(data.row_ids, trows):
                    tr.insert(1,TD(repr_data(col_totals_vals[r]/ncols),_class='total'))
            if col_totals:
                col_headers.insert(1, TH(current.T('Total')))
                for r,tr in zip(data.row_ids, trows):
                    tr.insert(1,TD(repr_data(col_totals_vals[r]),_class='total'))

        if row_totals or row_average or row_average_non_null:
            row_totals_vals = defaultdict(data.data_type)
            non_null_rows_in_col = defaultdict(int)
            for (r,c),v in data.table_data.items():
                row_totals_vals[c] += v
                non_null_rows_in_col[c] += 1
            if row_average_non_null:
                nrows = len(data.row_ids)
                tr_averages = TR(TH(current.T('Medias no nulos')),
                                 *[TD(repr_data(row_totals_vals[c]/non_null_rows_in_col[c]))
                                   for c in data.col_ids])
                if col_totals or col_average:
                    grand_average = sum(row_totals_vals.values())/nrows
                if col_average_non_null:
                    tr_averages.insert(1, TD())
                elif col_average:
                    tr_averages.insert(1, TD())
                if col_totals:
                    tr_averages.insert(1, TD(repr_data(grand_average)))
                trows.append(tr_averages)
            elif row_average:
                nrows = len(data.row_ids)
                tr_averages = TR(TH(current.T('Medias')),
                                 *[TD(repr_data(row_totals_vals[c]/nrows)) for c in data.col_ids])
                if col_totals or col_average:
                    grand_average = sum(row_totals_vals.values())/nrows
                if col_average_non_null:
                    tr_averages.insert(1, TD())
                elif col_average:
                    tr_averages.insert(1, TD(repr_data(grand_average/ncols)))
                if col_totals:
                    tr_averages.insert(1, TD(repr_data(grand_average)))
                trows.append(tr_averages)
            if row_totals:
                tr_totals = TR(TH(current.T('Totales')),
                               *[TD(repr_data(row_totals_vals[c]),_class='total') for c in data.col_ids])
                if col_totals or col_average_non_null:
                    grand_total = sum(row_totals_vals.values())
                if col_average_non_null:
                    tr_totals.insert(1, TD(repr_data(grand_total/ncols),_class='total'))
                if col_totals:
                    tr_totals.insert(1, TD(repr_data(grand_total),_class='total'))
                trows.append(tr_totals)

        init_script = SCRIPT(self.fixheadertable_script%dict(
            ncols=len(trows[0]) if trows else 0,
            id = kwds['_id']
            )
        )
        self.components = [#css4rosheet, #js4rosheet, selected, shift_selected,
            TABLE(THEAD(col_headers), TBODY(*trows), **kwds),
            init_script
        ]

    @staticmethod
    def add_static_files():
        current.response.files.append(URL('static', 'css/rsheet.css'))
        current.response.files.append(URL('static', 'jquery-browser-plugin/dist/jquery.browser.min.js'))
        current.response.files.append(URL('static', 'js/jquery.fixheadertable.js'))
        current.response.files.append(URL('static', 'css/fixheadertable-base.css'))

######################### RWSheet ############################
##############################################################

class RWSheet(FORM):
    #TODO: Podria ser util tb un conjunto de booleanos?
    #TODO: custom widget? widget no uniforme?
    #Cualquiera de estas dos podria ser util para poder usar un decimal si es
    #a granel o un entero si no lo es.
    widgets = Storage(dict(
        string = MyStringWidget,
        integer = MyIntegerWidget,
        double = MyDoubleWidget,
        decimal = MyDecimalWidget,
        time = MyTimeWidget,
        date = MyDateWidget,
        datetime = MyDatetimeWidget,
        boolean = gluon.sqlhtml.BooleanWidget,
        ))

    def __init__(self, query,
                 row_field, col_field, data_field,
                 #query=None,
                 row_ids=None, col_ids=None,
                 row_represent=None, col_represent=None,
                 data_represent=None, data_parse=None,
                 row_totals=False, col_totals=False,
                 extra_values=None,
                 next=None,
                 **attributes):
        '''Muestra una hoja de calculo para editar una relacion que representa
        una funcion (ref1,ref2) -> valor
        '''
        FORM.__init__(self, **attributes)
        if not (row_field.table == col_field.table == data_field.table):
            raise AttributeError
        self.table = row_field.table
        self.db    = row_field.db
        self.query = query or table
        self._id_field  = self.table.id
        self.row_field  = row_field
        self.col_field  = col_field
        self.data_field = data_field
#        self.row_totals = row_totals
        if row_totals:
            raise NotImplementedError
        self.col_totals = col_totals
        self.next = next
        if not row_represent:
            row_represent = row_field.represent or (lambda x:x)
        self.row_represent = row_represent
        if not col_represent:
            col_represent = col_field.represent or (lambda x:x)
        self.col_represent = col_represent
        if not data_represent:
            data_represent = data_field.represent or (lambda x:x)
        self.data_represent = data_represent
        if not data_parse:
            data_parse = lambda x:x #or self.data_field.default
        self.data_parse = data_parse

        if not extra_values:
            extra_values = {}
        self.extra_values = extra_values

        #En RWSheet, row_ids es necesariamente una lista de enteros
        if row_ids and not row_field.type.startswith('reference'):
            raise AttributeError('row_ids specified for row field not of referenfe type')
        if row_ids==None:
            row_ids   = [r[row_field] for r in self.db(query).select(row_field, groupby=row_field)]
        self.row_ids = row_ids
        self.row_names = [row_represent(rid) for rid in row_ids]

#        if col_ids and not col_field.type.startswith('reference'):
#            raise AttributeError('col_ids specified for col field not of reference type')
        if col_ids==None:
            col_ids   = [r[col_field] for r in self.db(query).select(col_field, groupby=col_field)]
        self.col_ids = col_ids
        self.col_names = [col_represent(cid) for cid in col_ids]

        self.prepare_components()

    def prepare_components(self):
        table_data = defaultdict(str)
        for q_row in self.db(self.query).select(self.row_field, self.col_field, self.data_field):
            r,c,d = q_row[self.row_field], q_row[self.col_field], q_row[self.data_field]
            table_data[(r,c)] = self.data_represent(d) or ''
        hs = [TH(n) for n in self.col_names]
        if self.col_totals:
            col_headers = THEAD(TR(TH(''),TH(TAG.strong(current.T('Totales'))),*hs))
        else:
            col_headers = THEAD(TR(TH(''),*hs))

        td_widget = self.td_widget()
        validator = self.data_field.requires

        def ts(rid):
            return [TD(td_widget(self.data_field,
                                  table_data[(rid, cid)],
                                  _id='cell_%d_%d'%(rid,cid),
                                  _name='cell_%d_%d'%(rid,cid),
                                  requires=validator))
                      for cid in self.col_ids]
        if self.col_totals:
            trows = [TR(TH(rname, _id='row_header_%d'%rid),
                        TD(SPAN('',_id = 'total_%d'%rid)),
                        *ts(rid))
                     for rid,rname in zip(self.row_ids,self.row_names) ]
        else:
            trows = [TR(TH(rname, _id='row_header_%d'%rid), *ts(rid))
                     for rid,rname in zip(self.row_ids,self.row_names) ]

        table = TABLE(col_headers, TBODY(*trows), **{'_class':'rwsheet', '_id':'rwsheet'})
        c = [INPUT(_type='hidden', _id='_row_ids',
                   _name='_row_ids', _value=','.join(str(i) for i in self.row_ids)),
             INPUT(_type='hidden', _id='_col_ids',
                   _name='_col_ids', _value=','.join(str(i) for i in self.col_ids)),
             INPUT(_type='hidden', _id='paste_into_cell',
                   _name='paste_into_cell', _value='nombre_0')]
        paste_helper = DIV(c, _class="hidden")

        self.components = [#css4rwsheet,
                           #js4rwsheet,#, selected, shift_selected,
                           table,
                           self.show_product_area(), self.rwsheet_controls(),
                           paste_helper, #js_paste
                           ]
        current.response.files.append(URL('static', 'css/rsheet.css'))
        current.response.files.append(URL('static', 'jquery-browser-plugin/dist/jquery.browser.min.js'))
        current.response.files.append(URL('static', 'js/jquery.fixheadertable.js'))
        current.response.files.append(URL('static', 'css/fixheadertable-base.css'))
        current.response.files.append(URL('static', 'css/rsheet.css'))
#        current.response.files.append(URL('static', 'jquery-ui/css/smoothness/jquery-ui-1.8.4.custom.css'))
        current.response.files.append(URL('static', 'js/rwsheet.js'))
        if self.col_totals:
            current.response.files.append(URL('static', 'js/rwsheet_col_totals.js'))

    def rwsheet_controls(self):
        return DIV(INPUT(_type='submit',
                         _name='save',
                         _value=current.T('Guardar cambios'),
                         _class='btn btn-primary'),
                   INPUT(_type='submit',
                         _name='discard',
                         _value=current.T('Descartar cambios'),
                         _class='btn btn-warning'),
                   **dict(_class='rwsheet_controls btn-group'))

    def show_product_area(self):
        return DIV(P(''),_id='active_product')

    def td_widget(self):
        field_type = self.data_field.type
        if field_type.startswith('decimal'):
            field_type = 'decimal'
        try:
            return self.widgets[field_type].widget
        except KeyError:
            raise AttributeError('RWSheet not implemented for fields of type %s'%field_type)

    def accepts(
        self,
        request_vars,
        session=None,
        formname=None,
        keepvalues=False,
        onvalidation=None,
        dbio=True,
        hideerror=False,
        detect_record_change=False,
        ):
        if request_vars.__class__.__name__ == 'Request':
            request_vars=request_vars.post_vars
        #Ojo: si han pulsado en discard changes, no escribes nada (y recargas la hoja)
        if request_vars.discard and self.__next__:
            redirect(self.__next__, client_side=True)
        if request_vars.discard or not request_vars.save:
            return False
        self.errors.clear()
        self.request_vars = Storage()
        self.request_vars.update(request_vars)
        self.session = session
        self.formname = formname
        self.keepvalues = keepvalues

        ret = FORM.accepts(
            self,
            request_vars,
            session,
            formname,
            keepvalues,
            onvalidation,
            hideerror=hideerror,
            )

        if ret and dbio:
            #self.db(self.query).delete() no es adecuado, self.query
            #puede mencionar mas de una tabla
            rids = self.db(self.query).select(self._id_field)
            self.db(self.table.id.belongs(rids)).delete()
            qs = []
            for k,v in request_vars.items():
                if not k.startswith('cell'):
                    continue
                data = self.data_parse(v)
                if data:
                    _, rids, cids = k.split('_')
                    rid = int(rids)
                    cid = int(cids)
                    d = dict(self.extra_values)
                    d.update({self.row_field.name:rid,
                              self.col_field.name:cid,
                              self.data_field.name:data})
                    qs.append(d)
            self.table.bulk_insert(qs)
            #TODO: Se podria hacer mas eficiente actualizando los valores de las celdas
            #En vez de recrear todo el formulario, pero en fin, sera mas adelante
            self.prepare_components()

        return ret
