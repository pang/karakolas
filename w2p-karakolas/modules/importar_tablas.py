# -*- coding: utf-8 -*-

# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################

"""
Our MiniPython grammar, the AST classes contain those arguments as properties.

module MiniPython
{
    mod = Module(stmt* body)
        | Expression(expr body)

    stmt = Assign(expr* targets, expr value)
          | If(expr test, stmt* body, stmt* orelse)
          | Expr(expr value)

    expr = BoolOp(boolop op, expr* values)
         | BinOp(expr left, operator op, expr right)
         | UnaryOp(unaryop op, expr operand)
         | IfExp(expr test, expr body, expr orelse)
         | Compare(expr left, cmpop* ops, expr* comparators)
         | Call(expr func, expr* args, keyword* keywords, expr? starargs, expr? kwargs)
         | Num(object n)
         | Str(string s)

         | Subscript(expr value, slice slice, expr_context ctx)
         | Name(identifier id, expr_context ctx)

    slice = Ellipsis
          | Slice(expr? lower, expr? upper, expr? step)
          | ExtSlice(slice* dims)
          | Index(expr value)

    expr_context = Load | Store | Del | AugLoad | AugStore | Param

    boolop = And | Or

    operator = Add | Sub | Mult | Div

    unaryop = Not | UAdd | USub

    cmpop = Eq | NotEq | Lt | LtE | Gt | GtE | In | NotIn

    arguments = (expr* args, identifier? vararg, identifier? kwarg, expr* defaults)
    keyword = (identifier arg, expr value)

    alias = (identifier name, identifier? asname)
}
"""
import ast
from math import floor
from decimal import Decimal, InvalidOperation
from itertools import chain
from collections import defaultdict
import os

import xlrd

from gluon import current

from .rsheet import _update_or_insert
from .adivinar_categorias import Bayes_Filter
from .kutils import ascii_safe, nombre2slug, reduce_espacios_nombre
from .modelos.pedidos import pedido_fantasma, nuevo_pedido
from .modelos.productores import categorias
from .utils.db import my_bulk_insert
from functools import reduce

PRECISION = Decimal('0.01')

class RuleParsingError(Exception):
    pass

class DataProcessingError(Exception):
    pass

def quantize(x):
    #La cautela siguiente es necesaria si la hoja contiene numeros pero la
    #celda esta declarada como texto, y es inocuo en otro caso.
    if isinstance(x, str):
        x = x.replace(',','.')
    return Decimal(x).quantize(PRECISION)

def col2index(col):
    inds = [ord(c)-64 for c in col]
    return sum(d*26**k-1 for k,d in enumerate(reversed(inds)))

def get_ratio(a,b):
    try:
        a,b = float(a), float(b)
        return round(100*(b-a)/a)
    except:
        return 0

class RuleParser(ast.NodeVisitor):
    builtin = {
        'max':max,
        'min':min,
        'round':round,
        'floor':floor,
        'str':str,
        'lower':lambda s:s.lower(),
        'float':Decimal,
        'int':int,
        'bool':bool,
        'col':col2index,
        'get_ratio':get_ratio,
        'len':len
    }
    def parser_for_type(self, dtype):
        if dtype == 'boolean':
            return bool
        elif dtype == 'integer':
            return int
        elif dtype == 'string':
            return str
        elif dtype == 'float' or dtype.startswith('decimal'):
            return quantize
        return lambda s:s

    def __init__(self, rules, fields, defaults = None):
        '''Puede lanzar SyntaxError
        '''
        self.tree    = ast.parse(rules)
        self.fields  = fields
        self.skel    = defaults or {}
        self.context = ''
        #Global instructions
        self.extra_cols = dict((node.args[0].s, node.args[1].s)
                           for node in ast.walk(self.tree)
                           if type(node)==ast.Call and node.func.id=='define_extra_col')
#        self.extra_cols = {}
#        for node in ast.walk(self.tree):
#            if type(node)==ast.Call:
#                if node.func.id=='define_extra_cols':
#                    self.extra_cols[node.args[0].s] = node.args[1].s
#                elif node.func.id=='set_context_col':
#                    self.context_col = col2index(node.args[0].s)
        self.parsers = {}
        for k,v in chain(iter(fields.items()), iter(self.extra_cols.items())):
            if v not in self.parsers:
                self.parsers[v] = self.parser_for_type(v)

    def process(self, sheet, row, data, local_vars=None):
        self.data   = data
        self.locals = local_vars or {}
        self.locals.update( {'True':True,'False':False} )

        try:
            self.visit(self.tree)
            prod_data  = defaultdict(str,self.skel)
            extra_data = {}
            for k,v in self.locals.items():
                if k in self.fields:
                    parse = self.parsers[self.fields[k]]
                    prod_data[k] = parse(v)
                elif (k in self.extra_cols):
                    parse = self.parsers[self.extra_cols[k]]
                    extra_data[k] = parse(v)
            return prod_data, extra_data
        except (TypeError, InvalidOperation, DataProcessingError) as e:
            try:
                self.context = str(data[self.context_col])
                return {},{}
            except:
                raise DataProcessingError(e)

    def name(self, node):
        return node.__class__.__name__

    def visit(self, node):
        if isinstance(node, list):
            return [self.visit(n) for n in node]
        try:
            visitor = getattr(self, 'visit_' + self.name(node))
        except AttributeError:
            raise RuleParsingError("syntax not supported (%s)" % self.name(node))

        return visitor(node)

    def visit_Module(self, node):
        for stmt in node.body:
            self.visit(stmt)

    def visit_Assign(self, node):
        val = self.visit(node.value)
        target = node.targets[0]
        if isinstance(val, (tuple, list)):
            self.locals.update(list(zip([elt.id for elt in target.elts],val)))
        else:
            self.locals[target.id] = val

    def visit_Expr(self, node):
        self.visit(node.value)

    def visit_Name(self, node):
        try:
            val   = self.locals[node.id]
            dtype = self.fields.get(node.id) or self.extra_cols.get(node.id)
            if dtype:
                try:
                    val = self.parsers[dtype](val)
                except (TypeError, InvalidOperation, ValueError):
                    raise DataProcessingError(current.T("Variable %s was supposed to be of type %s, but it's not")%(node.id, dtype))
            return val
        except KeyError:
            raise RuleParsingError(current.T('Variable %s was referenced before it was defined')%node.id)

    def visit_Num(self, node):
        return Decimal(node.n)

    def visit_NameConstant(self, node):
        return node.value

    def visit_Str(self, node):
        #Podria eliminar caracteres innecesarios, pero en realidad no veo peligro
        #solo hablamos de literales...
#        return node.s.translate(None, '&%$etc')
        return node.s

    def visit_Call(self, node):
        if node.func.id == 'cols':
            return self.data
        elif node.func.id == 'context':
            return self.context
        elif node.func.id == 'guess':
            return 'guess'
        elif node.func.id=='set_context_col':
            self.context_col = col2index(node.args[0].s)
        elif node.func.id in self.builtin:
            return self.builtin[node.func.id](*[self.visit(arg) for arg in node.args])

    def visit_BoolOp(self, node):
        op = type(node.op)
        if op==ast.Or:
            f = lambda a,b:a or b
        if op==ast.And:
            f = lambda a,b:a and b
        return reduce(f, (self.visit(v) for v in node.values))

    def visit_UnaryOp(self, node):
        if isinstance(node.op, ast.UAdd):
            return self.visit(node.operand)
        if isinstance(node.op, ast.USub):
            return - self.visit(node.operand)
        if isinstance(node.op, ast.Not):
            return not self.visit(node.operand)

    def visit_BinOp(self, node):
        left = self.visit(node.left)
        right = self.visit(node.right)
        if isinstance(node.op, ast.Add):
            return left + right
        if isinstance(node.op, ast.Sub):
            return left - right
        if isinstance(node.op, ast.Mult):
            return left * right
        if isinstance(node.op, ast.Div):
            return left / right

    def visit_Compare(self, node):
        assert len(node.ops) == 1
        assert len(node.comparators) == 1
        op = node.ops[0]
        comp = self.visit(node.comparators[0])
        left = self.visit(node.left)
        if isinstance(op, ast.Eq):
            return left == comp
        elif isinstance(op, ast.NotEq):
            return left != comp
        elif isinstance(op, ast.Lt):
            return left < comp
        elif isinstance(op, ast.LtE):
            return left <= comp
        elif isinstance(op, ast.Gt):
            return left > comp
        elif isinstance(op, ast.GtE):
            return left >= comp
        elif isinstance(op, ast.In):
            assert isinstance(comp, str)
            assert isinstance(left, str)
            return left.lower() in comp.lower()
        elif isinstance(op, ast.NotIn):
            assert isinstance(comp, str)
            assert isinstance(left, str)
            return left.lower() not in comp.lower()

    def visit_IfExp(self, node):
        if self.visit(node.test):
            return self.visit(node.body)
        else:
            return self.visit(node.orelse)

    def visit_If(self, node):
        if self.visit(node.test):
            self.visit(node.body)
        else:
            self.visit(node.orelse)

    def visit_Subscript(self,node):
        return self.visit(node.value)[self.visit(node.slice)]

    def visit_Index(self,node):
        return self.visit(node.value)

    def visit_Slice(self,node):
        return slice(int(self.visit(node.lower) if node.lower else 0), int(self.visit(node.upper)),
            int(self.visit(node.step)) if node.step else None)

def importa_plantilla(ipedido):
    '''Puede lanzar SyntaxError
    '''
    db = current.db
    pedido     = db.pedido(ipedido)
    iproductor = pedido.productor
    igrupo     = db.productor(iproductor).grupo

    _, xls    = db.pedido.xls.retrieve(pedido.xls)
    return importa_plantilla_multiple(
            pedido.reglas, xls.name,
            igrupo=None, fecha_reparto=None,
            iproductor=iproductor, ipedido=ipedido
    )

def importa_plantilla_multiple(reglas,
                               plantilla,
                               igrupo,
                               fecha_reparto=None,
                               iproductor=None,
                               ipedido=None):
    '''
    Importa una plantilla siguiendo unas reglas. Si iproductor es None, asigna los productos
    a pedidos de distintos productores según el valor del campo productor.

    Puede lanzar HayPedidoParaEsaFecha
    '''
    db = current.db
    bayes = Bayes_Filter()
    icat_none  = db.categorias_productos(codigo=-1).id

    if hasattr(plantilla, 'file'):
        path = os.path.join(os.getcwd(), 'applications', current.request.application, 'private',
                            '%s.xls'%current.response.session_id)
        tmp = open(path, 'w')
        tmp.write(plantilla.file.read())
        tmp.close()

        p = xlrd.open_workbook(path, formatting_info=1)
        os.remove(path)
    else:
        p = xlrd.open_workbook(plantilla, formatting_info=1)

    defaults = {'categoria':db.categorias_productos(codigo=-1).id,
                'temporada':True,
                'activo':True }
    fields = dict((f.name,f.type) for f in db.productoXpedido)
    fields['productor'] = 'string'
    rp = RuleParser(reglas, fields, defaults)

    cat_por_codigo, cat_por_nombre = categorias()

    def prepara_productor(iproductor, ipedido):
        d_columnas_extra = {}
        for w,tipo in rp.extra_cols.items():
            if (tipo=='float' or tipo=='decimal'): tipo='decimal(9,2)'
            cid = db.columna_extra_pedido.insert(pedido=ipedido, nombre=w, tipo=tipo)
            d_columnas_extra[w] = (cid,tipo)
        return d_columnas_extra

    productos       = {}
    slugs_productos = {}
    pedidos         = {}
    extracols_dict  = {}

    if iproductor:
#        assert db.pedido(ipedido).productor.grupo==igrupo
        productor_abierto = False
        extracols_dict[iproductor] = prepara_productor(iproductor, ipedido)
        pedidos[iproductor] = ipedido
        productos[iproductor] = defaultdict(int)
    else:
        productor_abierto = True
        nombres_productores = dict((nombre2slug(row.nombre), row.id)
            for row in db(db.productor.grupo==igrupo).select(
                db.productor.nombre, db.productor.id
        ))

    total1=0
    total2=0
    total3=0
    descolgados = []
    ppp_ls = []
    vesp_ls = defaultdict(lambda : defaultdict(list))
    for k,s in enumerate(p.sheets()):
        for j in range(s.nrows):
            rowinfo = s.rowinfo_map.get(j)
            if rowinfo and rowinfo.hidden:
                continue
            try:
                d_prod, d_extra_cols = rp.process(k,j,
                    [c.value for c in s.row(j)],
                    {'fila':j, 'hoja':k, 'temporada':True})
            except DataProcessingError as e:
                #TODO: registra los errores y da info de debug:
#                errors.append(e)
                continue
            #precio positivo y nombre no vacio
            if (not d_prod.get('precio_base') or
                not d_prod.get('nombre') or
                #o no es un precio valido:
                db.productoXpedido.precio_base.validate(d_prod.get('precio_base'))[1]
            ):
                continue
            #TODO ? no estoy usando el ctype de xlrd (antes lo usaba, pero es nec?)
#            cell_precio = d_cells['precio_base']
#            if (cell_precio.ctype!=xlrd.XL_CELL_NUMBER
#                or not cell_precio.value
#                or not d_cells['nombre'].value.strip()) :
#                continue

            #productor
            if productor_abierto:
                #Codificamos los nombres para convertirlos en str, ya que
                #xlrd usa unicode, y queremos comparar los valores leidos con los almacenados
                iproductor = nombres_productores.get(
                    nombre2slug(d_prod['productor'].encode('utf8')), -1)
                if iproductor==-1:
                    descolgados.append((j, k, d_prod['nombre'], d_prod['productor']))
                    continue

                if iproductor not in pedidos:
                    #Puede lanzar HayPedidoParaEsaFecha
                    ipedido = nuevo_pedido(iproductor, fecha_reparto)
                    extracols_dict[iproductor] = prepara_productor(iproductor, ipedido)
                    pedidos[iproductor] = ipedido
                    productos[iproductor] = defaultdict(int)

            d_prod['productor'] = iproductor
            slugs_nuevos     = productos[iproductor]

            #nombre
            nombre = reduce_espacios_nombre(d_prod['nombre'])
            slug = nombre2slug(nombre)
            if slug in slugs_nuevos:
                extra = '_' + str(slugs_nuevos[slug]+1)
                d_prod['nombre'] = nombre + extra
                slugs_nuevos[slug] += 1
                slug += extra
            else:
                d_prod['nombre'] = nombre
                slugs_nuevos[slug] += 1

            #TODO ?
            #procesar otros campos tipo 'guess'
            #for k,v ... if v=='guess':...

            #categoria
            cat = d_prod['categoria']
            if cat=='guess':
                d_prod['categoria'] = bayes.adivina_categorias(nombre)
                if d_prod['categoria'] == icat_none:
                    d_prod['categoria'] = bayes.adivina_categorias(rp.context)
            else:
                #si es un string, buscamos la categoria con ese nombre, si es un codigo...
                try:
                    codigo = int(cat)
                    icategoria = cat_por_codigo.get(codigo)
                except ValueError:
                    icategoria = cat_por_nombre.get(cat)
                d_prod['categoria'] = icategoria or  icat_none

            #precio_final
            if 'precio_final' not in d_prod:
                d_prod['precio_final'] = d_prod['precio_base']

            #precio_productor
            if 'precio_productor' not in d_prod:
                d_prod['precio_productor'] = d_prod['precio_base']

            #producto
            iproducto = d_prod['producto'] = db.producto.insert(productor=iproductor)

            #pedido
            ipedido = pedidos[iproductor]
            d_prod['pedido'] = ipedido
            del d_prod['productor']
            ppp_ls.append(d_prod)
#            pppid = db.productoXpedido.insert(**d_prod)
            for col, data in d_extra_cols.items():
                columna, tipo = extracols_dict[iproductor][col]
                vesp_ls[tipo][ipedido].append(dict(
                    columna=columna,
                    valor=data,
                    producto=iproducto
                ))
#                db.valor_extra_string_pedido.insert(
#                    columna=extracols_dict[iproductor][col],
#                    valor=data,
#                    productoXpedido=pppid)
    my_bulk_insert(db.productoXpedido,
        [f for f in db.productoXpedido.fields if not f=='id'],
        ppp_ls)
    #sql a pelo para poner el valor correcto de tabla.productoXpedido!
    for tipo, vesp_ls_tipo in vesp_ls.items():
        tabla = db.TABLAS_VALORES_EXTRA_PEDIDO[tipo]
        for ipedido, ls in vesp_ls_tipo.items():
            my_bulk_insert( tabla, ['columna', 'valor', 'producto'],ls)
            sql = '''UPDATE %(nombre_tabla_valor)s AS tv,
                            productoXpedido AS ppp,
                            %(nombre_tabla_columna)s AS tc
                        SET tv.productoXpedido=ppp.id WHERE
                            (tv.columna=tc.id) AND
                            (tc.pedido=%(ipedido)d) AND
                            (ppp.producto=tv.producto) AND
                            (ppp.pedido=%(ipedido)d);'''%dict(
                nombre_tabla_valor=tabla.sql_shortref,
                nombre_tabla_columna='columna_extra_pedido',
                ipedido=ipedido
            )
            db.executesql(sql)

    return pedidos, descolgados

def exportar_plantilla(pedidos, archivo, nombre_fichero='', igrupo=None, peticion=True):
    import xlrd
    import xlutils.copy
    import tempfile
    db = current.db
    tig = db.item_grupo

    pedido0 = db.pedido(pedidos[0])
    fecha   = pedido0.fecha_reparto
    igrupo  = igrupo or pedido0.productor.grupo.id
    es_red  = db.grupo(igrupo).red

    cantidad_total = (tig.cantidad_pedida.sum()
        if peticion else db.item_grupo.cantidad.sum()
    )
    q = (db.pedido.id.belongs(pedidos) &
         (db.productoXpedido.pedido==db.pedido.id) &
         (tig.productoXpedido==db.productoXpedido.id))
    if es_red:#resumen de red
        #solo grupos hijos directos que tengan el proxy_pedido activo
        q = q & ((tig.grupo==db.grupoXred.grupo) &
                 (db.grupoXred.red==igrupo) &
                 (db.grupoXred.activo==True) &
                 (db.proxy_pedido.pedido==db.pedido.id) &
                 (db.proxy_pedido.activo==True) &
                 (db.proxy_pedido.grupo==db.grupoXred.grupo)
                 )

        igrupos = [r[tig.grupo] for r in db(q).select(tig.grupo, groupby=tig.grupo)]
        ngrupo    = {}
        col_grupo = {}
        col_offset = 0
        for r in db(db.grupo.id.belongs(igrupos))\
                 .select(db.grupo.id, db.grupo.nombre):
            ngrupo[r.id]    = r.nombre
            col_grupo[r.id] = col_offset
            col_offset += 1

        rb = xlrd.open_workbook(archivo.name, formatting_info=True)
        wb = xlutils.copy.copy(rb)

        #escribe los nombres de los grupos en la fila anterior a los productos...
        min_fila = db.productoXpedido.fila.min()
        #podria ocurrir que la columna variara dentro de la misma hoja, pero seria muy raro
        #no creo que merezca la pena comprobar que es así...
        min_col  = db.productoXpedido.columna_peticion.min()
        for row in db(db.productoXpedido.pedido.belongs(pedidos)) \
                .select(min_fila, min_col, db.productoXpedido.hoja, groupby=db.productoXpedido.hoja):
            fila = row[min_fila]
            col0 = row[min_col]
            for gid, col in col_grupo.items():
                nombre_grupo = ngrupo[gid]
                wb.get_sheet(row[db.productoXpedido.hoja]).write(
                    fila-1, col0+col, ascii_safe(nombre_grupo)
                )

        rows = db(q).select(cantidad_total, db.productoXpedido.ALL,
                            tig.grupo,
                            groupby=db.productoXpedido.id|tig.grupo)
        for r in rows:
            wb.get_sheet(r.productoXpedido.hoja).write(
                r.productoXpedido.fila,
                r.productoXpedido.columna_peticion + col_grupo[ r[tig.grupo] ],
                r[cantidad_total]
            )
    else: #pedido propio
        rows = db(q).select(cantidad_total, db.productoXpedido.ALL,
                            groupby=db.productoXpedido.id)

        rb = xlrd.open_workbook(archivo.name, formatting_info=True)
        wb = xlutils.copy.copy(rb)
        for r in rows:
            wb.get_sheet(r.productoXpedido.hoja).write(r.productoXpedido.fila,
                                                       r.productoXpedido.columna_peticion,
                                                       r[cantidad_total])
    temp = tempfile.NamedTemporaryFile()
    wb.save(temp.name)
    if nombre_fichero:
        base, ext = os.path.splitext(nombre_fichero)
        nombre_fichero_out = base + '_out' + ext
    else:
        nombre_fichero_out = 'pedidos_%s.xls'%fecha
    return current.response.stream(temp,40000, attachment=True,
            filename=nombre_fichero_out)
