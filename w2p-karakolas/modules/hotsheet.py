# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

from collections import defaultdict
import re
import random
from decimal import Decimal

import gluon.sqlhtml
from gluon.storage import Storage
from gluon.html import URL, P, A, IMG, XML, DIV, SPAN, TAG, \
                       TABLE, THEAD, TBODY, TH, TR, TD, UL, LI, \
                       FORM, FIELDSET, LABEL, INPUT, TEXTAREA, SELECT, OPTION, \
                       SCRIPT, CAT, xmlescape
from gluon.dal import DAL
from gluon import current
from functools import reduce

######################### RWSheet ############################
##############################################################

class HOTSheet(FORM):

    def __init__(self,
                 query,
                 row_field, col_field, data_field,
                 row_ids=None, col_ids=None,
                 row_headers=None, col_headers=None,
                 row_totals=False, col_totals=False,
                 extra_values=None,
                 records_per_page=20,
                 submit_buttons = None,
                 **attributes):
        '''Muestra una hoja de calculo para editar una relacion que representa
        una funcion (ref1,ref2) -> valor

        submit_buttons is a list [(button_name, button_text, button_type)]
            - 'discard' is a special button_name, that will cause the form not to be processed
        '''
        db = current.db
        FORM.__init__(self, **attributes)
        self.attributes = attributes
        if not (row_field.table == col_field.table == data_field.table):
            raise AttributeError("Only one table per HOTSheet")
        self.table = row_field.table
        self.db    = row_field.db
        self.query = query or table
        self._id_field  = self.table.id
        self.row_field  = row_field
        self.col_field  = col_field
        self.data_field = data_field
        self.records_per_page = records_per_page
        if row_totals:
            raise NotImplementedError
        self.col_totals = col_totals
        self.extra_values = extra_values or {}

        #En HOTSheet, row_ids es necesariamente una lista de enteros
        if row_ids and not row_field.type.startswith('reference'):
            raise AttributeError('row_ids specified for row field not of reference type')
        if row_ids==None:
            row_ids   = [r[row_field] for r in self.db(query).select(row_field, groupby=row_field)]
        self.row_ids = row_ids
        self.row_headers = row_headers or row_ids

        if col_ids==None:
            col_ids   = [r[col_field] for r in self.db(query).select(col_field, groupby=col_field)]
        self.col_ids = col_ids
        self.col_headers = col_headers or col_ids

        data = dict((rid, {}) for rid in self.row_ids)
        for r in db(query).select(row_field, col_field, data_field):
            data[r[row_field]][r[col_field]] = r[data_field]
        self.data = data
        # a list [(button_name, button_text, button_type)]
        # 'discard' is a special button_name, that will cause the form not to be processed
        self.submit_buttons = submit_buttons or [
           ('submit', current.T('Submit'), 'success')
        ]
        self.submit_buttons.append( ('discard', current.T('Discard'), 'warning') )
        self.prepare_components()

    def escape(self, xs, data_text = '"%s"'):
        return '[%s]'%','.join(data_text%xmlescape(x).decode('utf8') for x in xs)

    def prepare_components(self):
        if self.col_totals:
            pass

        current.response.files.append(URL('static', 'handsontable/dist/handsontable.full.js'))
        current.response.files.append(URL('static', 'handsontable/dist/handsontable.full.css'))
        current.response.files.append(URL('static', 'handsontable/plugins/bootstrap/handsontable.bootstrap.css'))

    def xml(self):
        d = dict(col_headers = self.escape(self.col_headers),
                 row_headers = self.escape(self.row_headers),
                 row_ids = self.escape(self.row_ids),
                 columns = self.escape(
                    self.col_ids,
                    data_text='{data: "%s", type: "numeric", format: "0.00"}'
                 ),
                 data = ('[%s]'%','.join(
                    '{%s}'%(
                        '"fila":%s,'%rid +
                        ','.join('%s:%s'%(colid,val) for colid,val in self.data[rid].items())
                    ) for rid in self.row_ids)),
                 records_per_page = self.records_per_page,
                 nrecords = len(self.row_ids),
                 submit_buttons = self.submit_buttons
                )
        hotsheet = XML(current.response.render('hotsheet.html', d))
        self.components.append(hotsheet)
        return FORM.xml(self)

    def accepts(
        self,
        request_vars,
        formname='default'
        ):
        '''
        La funcion no acepta todos los parametros habituales de FORM.accepts porque
        la validacion es 100% javascript, luego llegados a este punto no puede haber errores
        '''
        import json
        db = current.db
        data = self.data
        self.formname = formname
        if (formname != request_vars._formname):
            return False

        self.discard = (request_vars.button_submitted=='discard')
        self.button_submitted = request_vars.button_submitted
        self.empty_override = (request_vars.override=='{}')
        if self.discard or self.empty_override:
            return True

        override = json.loads(request_vars.override)
        q = (reduce(lambda a,b:a&b,
                    ((self.table[k]==v) for k,v in self.extra_values.items()))
             if self.extra_values else None)
        for k,v in override.items():
            rid, cid = list(map(int, k.split(',')))
            val = Decimal(v or 0)
            if cid in data[rid]:
                qrc = (self.row_field==rid) & (self.col_field==cid)
                if q: qrc &=q
                db(qrc).update(**{self.data_field.name:val})
            elif val:
                d = {self.row_field.name:rid,
                     self.col_field.name:cid}
                d.update(self.extra_values)
                #if record already exists
                r = self.table(**d)
                if r:
                    r.update_record(**{self.data_field.name:val})
                else:
                    d[self.data_field.name] = val
                    self.table.insert(**d)
            data[rid][cid] = val

        return True
