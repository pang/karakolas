# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################

import re

from gluon import current

from modelos.pedidos import crea_pedido_fantasma, pedido_fantasma
from kutils import separa_lineas, separa_por_coma

def categorias():
    db = current.db
    cat_por_codigo = {}
    cat_por_nombre = {}
    for row in db(db.categorias_productos).select():
        cat_por_codigo[row.codigo] = row.id
        cat_por_nombre[row.nombre] = row.id
    return cat_por_codigo, cat_por_nombre

#def ordena_categorias(cats):
#    '''Ordena las categorias por codigo
#    '''
#    db = current.db
#    #¿se puede hacer mas eficiente?
#    rows = db(db.categoria.id.belongs(cats)) \
#            .select(db.categoria.id,
#                    orderby=db.categoria.codigo)
#    return [row.id for row in rows]

def nuevo_productor(igrupo_productor, data):
    db = current.db
    iproductor = db.productor.insert(grupo=igrupo_productor, **data)
    crea_pedido_fantasma(iproductor)
    crea_proxy_productor_subgrupos(igrupo_productor, iproductor)
    return iproductor

def crea_proxy_productor_subgrupos(igrupo_base, iproductor):
    from modelos.grupos import arbol_grupos_de_la_red
    db = current.db
    def _crea_proxy_productor_subgrupos(d, via=None):
        for igrupo, (_, subgrupos) in d.items():
            if via:
                ppid = db.proxy_productor.insert(grupo=igrupo, productor=iproductor, via=via)
            _crea_proxy_productor_subgrupos(subgrupos, via=igrupo)
    _crea_proxy_productor_subgrupos(arbol_grupos_de_la_red(igrupo_base))

def compara_con_productos_inactivos(ipedidof):
    '''Si borras productos de la vista controllers/productos, para el pedido fantasma,
    los productos no desaparecen de la base de datos, para poder seguir la pista a los
    pedidos históricos, sino que se marcan con activo=False.

    Si posteriormente se introducen productos con el mismo nombre, podemos incluir productos
    con el mismo slug pero activo. Este problema esta resuelto en pedidos normales llamando
    a crea_productos_si_nec, pero no esta resuelto para el pedido fantasma

    2019-08-20: Mantengo esta función por cautela, pero podríamos eliminar los productos
    con activo=False y dejar de llamar a esta función :-/
    '''
    db = current.db
    tppp  = db.productoXpedido
    tppp2 = db.productoXpedido.with_alias('tppp2')

    for row in db( (tppp.pedido==ipedidof) &
                   (tppp2.pedido==ipedidof) &
                   (tppp.slug==tppp2.slug) &
                   (tppp.activo==False) &
                   (tppp2.activo==True)
        ).select(tppp.id, tppp.producto, tppp2.id):
        #TODO: Si modifico el indice (pedido, producto) de productoXpedido, podria guardar
        #los valores a eliminar en una lista y eliminarlos con una sola sentencia sql
        db(tppp.id==row[tppp.id]).delete()
        db(tppp.id==row[tppp2.id]).update(producto=row[tppp.producto])

def crea_productos_si_nec(ipedido):
    from modelos.pedidos import pedido_fantasma
    db = current.db
    tppp = db.productoXpedido

    iproductor = db.pedido(ipedido).productor
    ipedidof   = pedido_fantasma(iproductor).id

    ppps_sin_producto = [(row.id, row.slug)
        for row in db((tppp.pedido==ipedido) &
                      (tppp.producto==None)) \
                   .select(tppp.id, tppp.slug)]
    slugs_ppps = [slug for _,slug in ppps_sin_producto]

    #Comparamos los slugs de los productos nuevos con los del pedido fantasma del productor
    if ipedido!=ipedidof:
        productos_mismo_slug = dict((row.slug, row.producto)
            for row in db((tppp.pedido==ipedidof) &
                          (tppp.slug.belongs(slugs_ppps))) \
                       .select(tppp.slug, tppp.producto))
    #Excepto si se trata del pedido fantasma, entonces no es necesario
    else:
        productos_mismo_slug = dict()

    for pppid, slug in ppps_sin_producto:
        iproducto = productos_mismo_slug.get(slug)
        if not iproducto:
            iproducto = db.producto.insert(productor=iproductor)
        tppp(pppid).update_record(producto=iproducto)

    #Ahora rellenamos el campo producto de valor_extra_XXX_pedido
    for row in db(db.columna_extra_pedido.pedido==ipedido).select():
        tabla = db.TABLAS_VALORES_EXTRA_PEDIDO[row.tipo]
        sql = '''UPDATE %(nombre_tabla)s AS t,
                        productoXpedido AS ppp
                    SET t.producto=ppp.producto WHERE
                        (t.columna=%(id_columna)d) AND
                        (t.productoXpedido=ppp.id);'''%dict(
            nombre_tabla=tabla._tablename,
            id_columna=row.id
        )
        db.executesql(sql)

def ordena_productos(prods):
    '''Ordena los productos por el orden canonico, que ahora es categoria y
    luego nombre por orden alfabetico
    '''
    db = current.db
    tppp = db.productoXpedido
    #¿se puede hacer mas eficiente?
    rows = db(tppp.id.belongs(prods) &
             (tppp.categoria==db.categorias_productos.id)) \
            .select(tppp.id,
                    orderby=db.categorias_productos.nombre|tppp.nombre)
    return [row.id for row in rows]

AMBITOS = re.compile('^PRODUCTOR$|^CAMPOS EXTRA$|^COLUMNAS EXTRA$|^PRODUCTOS$', flags=re.MULTILINE)
N_COLUMNAS_PRODUCTOS = 8
def importar_productor(igrupo, csv_data):
    '''Importa productor con todos sus productos en el grupo indicado
    el formato del archivo csv es el generado por gestion_pedidos/exportar_productos.csv

    04-10-21: Crea también los proxy_productor para redes y subredes
    '''
    from io import StringIO
    from csv import reader as csv_reader
    try:
        _, dproductor, dcampos_extra, dcolumnas_extra, dproductos = AMBITOS.split(csv_data)
    except ValueError:
        raise SyntaxError
    sio = StringIO(dproductor.strip())
    reader = csv_reader(sio, delimiter=',')
    d = dict(reader)
    d['grupo'] = igrupo
    iproductor = current.db.productor.insert(**d)
    ipedidof = crea_pedido_fantasma(iproductor)
    _productos_para_productor(iproductor, ipedidof, dcampos_extra, dcolumnas_extra, dproductos)
    crea_proxy_productor_subgrupos(igrupo, iproductor)
    return iproductor

AMBITOS_SIN_PRODUCTOR = re.compile('^CAMPOS EXTRA$|^COLUMNAS EXTRA$|^PRODUCTOS$', flags=re.MULTILINE)
def actualizar_productos_del_productor(iproductor, csv_data):
    '''Similar a la anterior, pero solo actualiza los productos de un productor existente

    Se usa como web service, por ejemplo fixidixi tiene un script para actualizar su oferta
    '''
    db = current.db
    _, dcampos_extra, dcolumnas_extra, dproductos = AMBITOS_SIN_PRODUCTOR.split(csv_data)
    ipedidof = pedido_fantasma(iproductor).id
    db(db.campo_extra_pedido.pedido==ipedidof).delete()
    #Al borrar las columna_extra_pedido se borran los valores en CASCADE
    db(db.columna_extra_pedido.pedido==ipedidof).delete()
    #Al borrar los productos se borran los productoXpedido en CASCADE
    #Aviso: estropea las estadisticas!
    #Mejor ponerlos fuera de temporada ...
    db(db.producto.productor==iproductor).delete()
    _productos_para_productor(iproductor, ipedidof, dcampos_extra, dcolumnas_extra, dproductos)

def _productos_para_productor(iproductor, ipedidof, dcampos_extra, dcolumnas_extra, dproductos):
    '''Asumimos que el productor no tiene productos, campos extra, etc
    '''
    db = current.db
    for row in dcampos_extra.strip().splitlines()[1:]:
        nombre,valor = row.split(',')
        db.campo_extra_pedido.insert(nombre=nombre,valor=valor,pedido=ipedidof)
    columnas_extra = {}
    for row in dcolumnas_extra.strip().splitlines()[1:]:
        nombre,tipo = separa_por_coma(row)
        icolumna = db.columna_extra_pedido.insert(nombre=nombre,tipo=tipo,pedido=ipedidof)
        columnas_extra[nombre] = (tipo, icolumna)
    ls = separa_lineas(dproductos.strip())
    fs = ls[0].split(',')

    producto_campos = [fs[i].split('.')[1] for i in range(N_COLUMNAS_PRODUCTOS)]
    for row in ls[1:]:
        cs = separa_por_coma(row)
        d = dict(list(zip(producto_campos, cs[:N_COLUMNAS_PRODUCTOS])))
        codigo = int(cs[N_COLUMNAS_PRODUCTOS])
        cid = db.categorias_productos(codigo=codigo)
        if not cid:
            #TODO: busca la categoria existente mas aproximada!
            if 50000000<codigo<60000000:
                cid = db.categorias_productos(codigo=50000000)
            else:
                cid = db.categorias_productos(codigo=-1)
        d['categoria'] = cid
        d['pedido'] = ipedidof
        d['granel']    = True if d['granel']=='True' else False
        iproducto = db.producto.insert(productor=iproductor)
        d['producto'] = iproducto
        pppid = db.productoXpedido.insert(**d)
        for i in range(N_COLUMNAS_PRODUCTOS+1,len(fs)):
            nombre = fs[i].split('.')[0]
            tipo, icolumna = columnas_extra[nombre[1:]]
            tipo = tipo.split('(')[0]
            db['valor_extra_%s_pedido'%tipo].insert(
                productoXpedido=pppid,columna=icolumna,valor=cs[i]
            )
