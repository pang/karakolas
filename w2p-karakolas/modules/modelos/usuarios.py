# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################

from gluon import current
from gluon.html import XML

from kutils import random_string

### Usuario

def borrar_datos_personales(uid):
    db = current.db
    user = db.auth_user(uid)
    username = random_string(len=12)
    user.update_record(email='%s@karakolas.org'%random_string(len=12),
                       first_name='borrado_%s'%username[:6],
                       last_name=username[6:],
                       username='borrado_%s'%username,
                       telefono='0',
                       direccion='',
                       password=random_string()
                       )
    db(db.auth_membership.user_id==uid).delete()
    db(db.personaXgrupo.persona==uid).update(activo=False)
    db(db.personaXlistaespera.persona==uid).delete()

def representa_usuario_2(uid):
    usuario = current.db.auth_user[uid]
    return ('%s %s'%(usuario.first_name,
                     usuario.last_name[0] if usuario.last_name else '')
    )

def representa_usuario_3(uid, unidad=None, grupo=None):
    usuario = current.db.auth_user[uid]
    if not unidad:
        unidad  = current.db.personaXgrupo(persona=uid, grupo=grupo).unidad
    return XML(('<strong>%d</strong>: %s %s'%(
        unidad, usuario.first_name,
        usuario.last_name[0] if usuario.last_name else '')
    ))

def es_admin_del_grupo(uid, igrupo):
    db = current.db
    return not db(
       (db.auth_membership.user_id==uid) &
        db.auth_membership.group_id.belongs(db.auth_group.role=='admins_%d'%igrupo)
    ).isempty()

def es_admin_o_miembro_del_grupo(uid, igrupo):
    db = current.db
    auth_groups = ('admins_%d'%igrupo, 'miembros_%d'%igrupo)
    return not db(
       (db.auth_membership.user_id==uid) &
        db.auth_membership.group_id.belongs(db.auth_group.role.belongs(auth_groups))
    ).isempty()

def email_blacklist():
    '''Devuelve una lista de emails leídos directamente de 'applications/karakolas/private/blacklist'

    Son emails que nos han devuelto, porque no existe
    '''
    import os
    RUTA_BLACKLIST = 'applications/karakolas/private/blacklist'
    if os.path.exists(RUTA_BLACKLIST):
        return set(l.strip() for l in open(RUTA_BLACKLIST).read().splitlines())
    else:
        return set()

def emails_de_todos_los_admins():
    db = current.db
    webadmins_gid  = current.auth.id_group('webadmins')
    return [admin.email for admin in db(
                        (db.auth_user.id==db.auth_membership.user_id) &
                        (db.auth_membership.group_id==webadmins_gid)
                    ).select(db.auth_user.email)]
