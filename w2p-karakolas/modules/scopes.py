# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

#########################################################################
from gluon import current
from gluon.html import URL

class Scope (object):

    def __init__(self,):
        pass

    def set_scopes(self, d):
        self._scopes = d

    def set(self, scope):
        if callable(scope):
            scope = scope()
        def decorador(action):
            def f(*args_f, **kwds_f):
                current.response.js = ((current.response.js or '') + 
                    ';window.App.set_scope("%s", "%s");'%(scope, current.request.cid) )
#                current.response.files.append(URL('static', 'css/%s.css'%scope))
                return action(*args_f, **kwds_f)
            return f
        return decorador

