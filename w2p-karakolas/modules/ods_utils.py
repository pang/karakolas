# -*- coding: utf-8 -*-
# (C) Copyright (C) 2012-23 Authors of karakolas
# This file is part of karakolas <karakolas.org>.

# karakolas is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# karakolas is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU Affero General Public
# License along with karakolas.  If not, see
# <http://www.gnu.org/licenses/>.

import odswriter as ods

def ods_write_sheet(data, nombre_fichero):
    '''Toma una lista de listas y devuelve un documento ods
    '''
    with ods.writer(open(nombre_fichero, 'wb')) as odsfile:
        odsfile.writerows(data)

def ods_write(hojas, nombre_fichero):
    '''Toma un diccionario de hojas, cada hoja
    es una lista de listas y devuelve un documento ods
    '''
    with ods.writer(open(nombre_fichero, 'wb')) as odsfile:
        for nombre in sorted(hojas.keys()):
            data = hojas[nombre]
            sheet = odsfile.new_sheet(nombre)
            sheet.writerows(data)
