#!/bin/bash

if [ ! -f devops/.env ]; then
    echo "file 'devops/.env' doesn't exist, creating default..."
    cp devops/.env.sample devops/.env
    USER_ID=$(echo $UID)
    sed -i "s/USER_ID=1000/USER_ID=$USER_ID/" devops/.env
    echo "default .env created"
fi
set -o allexport; . devops/.env; set +o allexport; \
if [ ! -f w2p-karakolas/private/appconfig.ini ]; then \
    echo "file 'w2p-karakolas/private/appconfig.ini' doesn't exist, creating default..."
    cp w2p-karakolas/private/appconfig_sample.ini w2p-karakolas/private/appconfig.ini
    sed -i "s,uri       = sqlite://storage.sqlite,;uri       = sqlite://storage.sqlite," w2p-karakolas/private/appconfig.ini
    sed -i "s,;uri    = mysql://username:password@localhost/your_database,uri    = mysql://$MARIADB_USER:$MARIADB_PASSWORD@karakolasdb:3306/$MARIADB_DATABASE," w2p-karakolas/private/appconfig.ini
    echo "default w2p-karakolas/private/appconfig.ini created"
fi

# Create auth.key if not existing
if [ ! -f w2p-karakolas/private/auth.key ]; then \
    echo "file 'w2p-karakolas/private/auth.key' doesn't exist, creating default..."
    echo "sha256:$(uuidgen)" > w2p-karakolas/private/auth.key
    echo "default w2p-karakolas/private/auth.key created"
fi

# Pack front
if [ ! -d w2p-karakolas/static/front/node_modules ]; then \
    # cd /home/web2pyuser/w2p-karakolas/applications/karakolas/static/front && \
    npm install --prefix w2p-karakolas/static/front && \
    npm run pack --prefix w2p-karakolas/static/front || true && \
    npm run pack --prefix w2p-karakolas/static/front
fi
