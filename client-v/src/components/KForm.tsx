import { IonContent, IonPage, IonText, IonInput, IonButton, IonCheckbox, IonItem, IonLabel } from "@ionic/react";
import React from "react";
import { useForm } from "react-hook-form";

// Custom components
import KInput, { KInputProps } from "../components/KInput";

const KForm: React.FC = ({
  form_title,
  kfields,
  given_values={}, // JSON data object
  submitFn,
  submitEnabled,
  submitBtnText,
}) => {
  
  
  const { register, handleSubmit, errors } = useForm({
      // validationSchema,
  });
  
  // console.log(`submitEnabled: ${submitEnabled}`)
  
  // Customize onChange event:
  const on_change_fn = (e) => {
      console.log(`Custom on change!: ${e.target.type} : ${e.target.name} : ${e.target.value}`)
      // console.log(e.target)
  }
  
  return (
    <IonContent>
        <div className="ion-padding">
          <IonText color="muted">
            <h2>{form_title ?? 'Create new Record'}</h2>
          </IonText>
          <form onSubmit={handleSubmit(submitFn)}>
            
            {/* Map fields from given prop kfields array */}
            
            {kfields.map((field, index) => {
              // console.log(`Adding [${index}] : ${field.name}`) // debug
              
              // Pass current values if given - TODO - use hook defaults
              const field_val = given_values[field.name]
              if (field_val) {
                console.debug(`Found value for : ${field.name} : ${field_val}`) // debug
                field.given_value=field_val
              } 
              
              return <KInput 
                {...field} 
                register={register} 
                on_change_fn={on_change_fn}
                key={index} 
              />
            })}
            
            {/*
            <IonItem>
              <IonLabel>I agree to the terms of service</IonLabel>
              <IonCheckbox slot="start" />
            </IonItem>
            */}
            <IonButton disabled={!submitEnabled} expand="block" type="submit" className="ion-margin-top">
                {submitBtnText ?? 'Crear'}
            </IonButton>
          </form>
          
        </div>
    </IonContent>
  );
};

/* After IonText
      <form onSubmit={handleSubmit(registerUser)}>
            
          </form>
    
*/

export default KForm;