import './KToast.css';

import { useState } from 'react';
import { IonButton, IonToast } from '@ionic/react';

interface KToastProps {
  open: boolean;
  message: string;
  dismissHandler: () => void;
}

const KToast: React.FC<KToastProps> = ({ open, message, dismissHandler }) => {
  
  const [err, setErr] = useState(false); // boolean to indicate API error
  
  const sayBye = () => {
    console.log(`byebye`)
  }
  
  return (
    <div>
      
      <IonToast 
        isOpen={open}
        message={message} 
        duration={3000}
        onDidDismiss = { dismissHandler }
        ></IonToast>
  
    </div>
  );
};

export default KToast;
