import React, { FC } from "react";
import { IonItem, IonLabel, IonInput } from "@ionic/react";
import { Controller, Control } from "react-hook-form";

export interface CInputProps {
  name: string;
  control?: Control;
  label?: string;
  component?: JSX.Element;
}

const CInput: FC<CInputProps> = ({
  name,
  control,
  component,
  label,
}) => {
  
  return (
    <>
      <IonItem>
        {label && (
          <IonLabel position="floating">{label}</IonLabel>
        )}
        <Controller
          name={name}
          control={control}
          onChangeName="onIonChange"
          render={ (name) => ( component ?? <IonInput/> ) }
        />
      </IonItem>
    </>
  );
};

export default CInput;
