import React, { ReactNode } from "react";
import {
  IonPage,
} from "@ionic/react";
import Tabs from "./Tabs.tsx";
import Header from "./Header.tsx";

interface LayoutProps {
  children: ReactNode;
}

const Layout: React.FC<LayoutProps> = ({ children }) => {
  return (
    <IonPage>
      <Header />
      {/* Renderiza el contenido recibido como children */}
      {children}
      <Tabs />
    </IonPage>
  );
};

export default Layout;
