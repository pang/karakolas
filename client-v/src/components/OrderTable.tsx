import { FunctionComponent, useState } from "react";
import "./OrderTable.css";
import { IonIcon, IonGrid, IonRow, IonCol } from "@ionic/react";
import { addCircle, removeCircle, trash } from "ionicons/icons";

interface OrderTableProps {
  //Como no tenemos id de momento hacemos así
  order: Array<{
    product: string;
    number: string;
    observations: string;
    price: string;
    units: string;
  }>;
}

const OrderTable: FunctionComponent<OrderTableProps> = ({ order }) => {
  const [productQuantities, setProductQuantities] = useState<number[]>(
    order.map((product) => parseInt(product.number, 10))
  );
  //Suma una unidad de un producto
  const handleIncrement = (index: number) => {
    setProductQuantities((prev) =>
      prev.map((qty, i) => (i === index ? qty + 1 : qty))
    );
  };
  //Quita una unidad de un producto
  const handleDecrement = (index: number) => {
    setProductQuantities((prev) =>
      prev.map((qty, i) => (i === index ? Math.max(qty - 1, 0) : qty))
    );
  };
  //Quita todas la unidades de un producto
  const handleReset = (index: number) => {
    setProductQuantities((prev) =>
      prev.map((qty, i) => (i === index ? 0 : qty))
    );
  };
  //Calcula el total
  const total = order.reduce(
    (sum, product, i) => sum + parseFloat(product.price) * productQuantities[i],
    0
  );
  //Vacia la cesta
  const handleResetAll = () => {
    setProductQuantities(order.map(() => 0));
  };

  return (
    <IonGrid>
<IonRow className="order_table_header_row">
  <IonCol size="3">Producto</IonCol>
  <IonCol size="2">Unidades</IonCol>
  <IonCol size="3">Observaciones</IonCol>
  <IonCol size="4">Coste</IonCol>
</IonRow>
      {order.map(
        (product, index) =>
          productQuantities[index] > 0 && ( // Solo pintamos si hay unidades de ese producto
            <IonRow className="order_table_data_row" key={index}>
              <IonCol sizeXs="12" sizeSm="3">
                <IonRow className="order_table_product_text">
                  {product.product}
                </IonRow>
                <IonRow className="order_table_small_text gray_666">
                  Precio por {product.units}: {product.price}€
                </IonRow>
              </IonCol>
              <IonCol sizeXs="12" sizeSm="2">
                <IonRow className="order_table_controls">
                  <div className="order_table_buttons">
                    <div className="order_table_unit_number">
                      {productQuantities[index]}
                    </div>
                    <div
                      className="icon_inline azul_primary font_25"
                      onClick={() => handleDecrement(index)}
                    >
                      <IonIcon icon={removeCircle} />
                    </div>
                    <div
                      className="icon_inline azul_primary font_25"
                      onClick={() => handleIncrement(index)}
                    >
                      <IonIcon icon={addCircle} />
                    </div>
                  </div>
                </IonRow>
              </IonCol>
              <IonCol
                className="order_table_small_text gray_666"
                sizeXs="12"
                sizeSm="3"
              >
                {product.observations}
              </IonCol>
              <IonCol sizeXs="12" sizeSm="2">
                <div>
                  {(
                    parseFloat(product.price) * productQuantities[index]
                  ).toFixed(2)}
                  €
                </div>
              </IonCol>
              <IonCol sizeXs="12" sizeSm="2">
                <div
                  className="order_table_trash icon_inline gray_666"
                  onClick={() => handleReset(index)}
                >
                  <IonIcon icon={trash} />
                </div>{" "}
              </IonCol>
            </IonRow>
          )
      )}

      <IonRow className="order_table_line_total">
        <IonCol size="8" className="order_table_total">
          Total
        </IonCol>
        <IonCol size="2">{total.toFixed(2)}€</IonCol>
        <IonCol
          size="2"
          className="order_table_trash gray_666"
          onClick={handleResetAll}
        >
          <div className="icon_inline">
            <IonIcon icon={trash} />
          </div>
        </IonCol>
      </IonRow>
    </IonGrid>
  );
};

export default OrderTable;
