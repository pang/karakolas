import React, { useState } from "react";

import {
  IonButton,
  IonCard,
  IonCardTitle,
  IonCardSubtitle,
  IonIcon,
  IonText,
} from "@ionic/react";

import { trash, addCircle, removeCircle } from "ionicons/icons";

import "./ProductCard.css";

interface ProductCardProps {
  product: Record<string, any>;
}

const ProductCard: React.FC<ProductCardProps> = ({ product }) => {
  const [quantity, setQuantity] = useState(0);

  const handleAddToCart = () => {
    setQuantity(1); // Inicia con una unidad
    console.log(
      `Pulsado añadir a cesta. TODO: Añadir la primera unidad en cesta de ${product.name}`
    );
  };

  const handleIncrement = () => {
    setQuantity((prev) => prev + 1);
    console.log(
      `Pulsado icono +. TODO: Añadir otra unidad en cesta de ${product.name}`
    );
  };

  const handleDecrement = () => {
    setQuantity((prev) => (prev > 1 ? prev - 1 : 1));
    console.log(
      `Pulsado icono -. TODO: Reducir una unidad en cesta de ${product.name}`
    );
  };

  const handleReset = () => {
    setQuantity(0);
    console.log(
      `Pulsado icono papelera. TODO: Quitar todas las unidades en cesta de ${product.name}`
    );
  };

  return (
    <IonCard className="product_card_main flex_column">
      <img className="product_card_img" src={product.img} />
      <IonCardTitle className="product_card_title">{product.name}</IonCardTitle>
      <IonCardSubtitle className="product_card_info">
        {product.additionalInfo && product.additionalInfo}
      </IonCardSubtitle>

      <div className="product_card_price_div send_to_bottom">
        <IonText className="product_card_price_text">{product.price}€</IonText>{" "}
        <IonText className="product_card_unit_type">
          / precio por {product.units}
        </IonText>
      </div>
      <div>
        {quantity === 0 ? (
          <IonButton
            className="product_card_add_basket send_to_bottom"
            expand="block"
            onClick={handleAddToCart}
          >
            Añadir a la cesta
          </IonButton>
        ) : (
          <div className="product_card_controls send_to_bottom">
            <div>
              <IonText className="product_card_units">
                {quantity === 1 ? "1 ud." : `${quantity} uds.`}
              </IonText>
            </div>
            <div className="product_card_buttons">
              <div className="icon_inline product_card_icon" onClick={handleDecrement}>
                <IonIcon icon={removeCircle} />
              </div>
              <div className="icon_inline product_card_icon" onClick={handleIncrement}>
                <IonIcon icon={addCircle} />
              </div>
              <div
                className="icon_inline product_card_icon product_card_trash"
                onClick={handleReset}
              >
                <IonIcon icon={trash} />
              </div>
            </div>
          </div>
        )}
      </div>
    </IonCard>
  );
};

export default ProductCard;
