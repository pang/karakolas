import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import {
  IonHeader,
  IonItem,
  IonToolbar,
  IonButtons,
  IonLabel,
  IonButton,
  IonIcon,
  IonImg,
  IonPopover,
  IonList,
} from "@ionic/react";
import {
  personCircleOutline,
  logInOutline,
  helpCircleOutline,
  logOutOutline,
} from "ionicons/icons";
import { useAuth } from "../context/AuthContext";
import { chevronDownOutline } from "ionicons/icons";
import { menu as menuIcon } from "ionicons/icons";
import "./Header.css";

const Header: React.FC = () => {
  const { user, group, logout } = useAuth();
  const history = useHistory();

  const handleLogout = async () => {
    try {
      await logout();
      history.push("/");
    } catch (error) {
      console.log(error);
    }
  };

  const [showPopover, setShowPopover] = useState(false);
  const [popoverEvent, setPopoverEvent] = useState(null);

  //Para capturar el evento y poder desplegar el popover en la esquina
  const openPopover = (e) => {
    setPopoverEvent(e);
    setShowPopover(true);
  };

  {
    /* <IonButton fill="clear" color="dark" id="context-menu-trigger">
                                {group.name}      <IonIcon icon={chevronDownOutline} slot="end" />
                            </IonButton>
                            <IonPopover trigger="context-menu-trigger" triggerAction="click">
                                <IonContent><
                                    <IonList>
                                        <IonItem href={`pedidos/${group.id}`}>Pedir</IonItem>
                                        <IonItem href={`productores/${group.id}`}>Productores</IonItem>
                                    </IonList>
                                </IonContent>
                            </IonPopover> */
  }

  return (
    <IonHeader>
      <IonToolbar>
        {/* Logo karakolas al inicio-izquierda siempre*/}
        <IonImg
          className="header_karakolas_img"
          slot="start"
          src="/assets/logo_completo.svg"
        />

        {/* Botones para pantallas grandes si existe*/}
        {user ? (
          <>
            <IonButtons
              className="desktop_buttons header_right_buttons"
              slot="primary"
            >
              <IonItem button className="header_item_desktop hide_on_mobile" lines="none">
                Pedidos de grupo
              </IonItem>
              <IonItem button className="header_item_desktop hide_on_mobile" lines="none">
                Contabilidad
              </IonItem>
              <IonItem
                button
                className="header_item_desktop hide_on_mobile"
                lines="none"
                href={`productores/${group.id}`}
              >
                Productores
              </IonItem>
              <IonItem button className="header_item_desktop hide_on_mobile" lines="none">
                {group && group.nombre}{" "}
                <IonIcon icon={chevronDownOutline} slot="end" />
              </IonItem>
              <IonItem
                button
                onClick={() => history.push("/order/dashboard")}
                className="header_item_radius hide_on_mobile"
                color="primary"
              >
                PEDIR
              </IonItem>
              <div className="header_right_div hide_on_medium_res">
                <IonButton>
                  <IonIcon icon={personCircleOutline} />
                </IonButton>
                <IonButton>
                  <IonIcon icon={helpCircleOutline} />
                </IonButton>
                <IonButton onClick={handleLogout}>
                  <IonIcon icon={logOutOutline} />
                  <IonLabel>Log out</IonLabel>
                </IonButton>
              </div>
            </IonButtons>
          </>
        ) : (
          //Si no hay usuario logueado mostrar botón de login a la derecha
          <IonButtons slot="end" className="header_right_buttons">
            <IonButton href="/login">
              <IonIcon icon={logInOutline} />
              <IonLabel>Log in</IonLabel>
            </IonButton>
          </IonButtons>
        )}

        {/* Si existe usuario y la resolucion es baja mostrar hamburguesa/popover */}
        {user && (
          <>
            {/* Botón hamburguesa para pantallas intermedias */}
            <IonButtons className="mobile_menu" slot="end">
            <IonItem button className="header_item_desktop hide_on_mobile" lines="none">
                Pedidos de grupo
              </IonItem>
              <IonItem button className="header_item_desktop hide_on_mobile" lines="none">
                Contabilidad
              </IonItem>
              <IonItem
                button
                className="header_item_desktop hide_on_mobile"
                lines="none"
                href={`productores/${group.id}`}
              >
                Productores
              </IonItem>
              <IonItem button className="header_item_desktop hide_on_mobile" lines="none">
                {group && group.nombre}{" "}
                <IonIcon icon={chevronDownOutline} slot="end" />
              </IonItem>
              <IonItem
                button
                onClick={() => history.push("/order/dashboard")}
                className="header_item_radius hide_on_mobile"
                color="primary"
              >
                PEDIR
              </IonItem>
              <IonButton onClick={openPopover}>
                <IonIcon icon={menuIcon} />
              </IonButton>
            </IonButtons>

            <IonPopover
              isOpen={showPopover}
              event={popoverEvent}
              onDidDismiss={() => setShowPopover(false)}
              cssClass="header_popover"
            >
              <IonList>
          {/*    <IonItem button onClick={() => setShowPopover(false)}>
                  <IonLabel slot="end">Pedidos de grupo</IonLabel>
                </IonItem>
                <IonItem button onClick={() => setShowPopover(false)}>
                  <IonLabel slot="end">Contabilidad</IonLabel>
                </IonItem>
                <IonItem
                  button
                  href={`productores/${group.id}`}
                  onClick={() => setShowPopover(false)}
                >
                  <IonLabel slot="end">Productores</IonLabel>
                </IonItem>
                <IonItem button onClick={() => setShowPopover(false)}>
                  <IonLabel slot="end">
                    {group && group.nombre}{" "}
                    <IonIcon icon={chevronDownOutline} slot="end" />
                  </IonLabel>
                </IonItem>

                <IonItem
                  button
                  className="pedir_button_popover"
                  color="primary"
                  onClick={() => {
                    setShowPopover(false);
                    history.push("/order/dashboard");
                  }}
                >
                  <IonLabel slot="end">PEDIR</IonLabel>
                </IonItem>*/}   
{/* SE MUEVE DE POPOVER A TABS */}
                <IonItem button lines="none" onClick={() => setShowPopover(false)}>
                  <IonIcon slot="end" icon={personCircleOutline} />
                </IonItem>
                <IonItem button lines="none" onClick={() => setShowPopover(false)}>
                  <IonIcon slot="end" icon={helpCircleOutline} />
                </IonItem>
                <IonItem button lines="none" onClick={() => setShowPopover(false)}>
                  
                  <IonButtons slot="end">
                    <IonButton onClick={handleLogout}>
                      <IonIcon icon={logOutOutline} />
                      <IonLabel>Log out</IonLabel>
                    </IonButton>
                  </IonButtons>
                </IonItem>
                
              </IonList>
            </IonPopover>
          </>
        )}
      </IonToolbar>
    </IonHeader>
  );
};

export default Header;
