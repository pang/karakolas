import React from "react";
import { IonBadge } from "@ionic/react";
import "./Badge.css";

interface BadgeProps {
  status: "Abierto" | "Cerrado" | "En reparto" | "En revisión" | "Consolidado";
  type?: "Esquina" | "Linea";
}

const Badge: React.FC<BadgeProps> = ({ status,type }) => {
  const getBadgeClass = () => {
    switch (status) {
      case "Abierto":
        return "badge_abierto";
      case "Cerrado":
        return "badge_cerrado";
      case "En reparto":
        return "badge_reparto";
      case "En revisión":
        return "badge_revision";
      case "Consolidado":
        return "badge_consolidado";
      default:
        return "";
    }
  };

  const getTypeClass = () => {
    switch (type) {
      case "Esquina":
        return "badge_esquina";
      case "Linea":
        return "badge_linea";
      default:
        return "";
    }
  };

  return (
    <IonBadge className={`custom_badge ${getBadgeClass()} ${getTypeClass()}`} >{status}</IonBadge>
  );
};

export default Badge;
