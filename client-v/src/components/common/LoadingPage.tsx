import React from 'react';
import { IonContent, IonItem } from '@ionic/react';

const LoadingPage: React.FC = () => {
    
    return (
        <IonContent fullscreen>
            <IonItem>Loading...</IonItem>
        </IonContent>    
    )
}

export default LoadingPage;