import React from 'react';
import "./Circle.css";

interface CircleProps {
    color: string;
}

const Circle: React.FC<CircleProps> =({color})=> {
    
    // console.log (`Rendering circle: ${color}`)
    
    return (
        <div className={`circle_style ${color}`} />
    )
}

export default Circle;