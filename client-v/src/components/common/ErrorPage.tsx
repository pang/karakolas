import React, { useEffect, useState } from 'react';
import { IonContent, IonItem } from '@ionic/react';
import { useAuth } from '../../context/AuthContext';

const ErrorPage: React.FC = ({error_message}) => {
    
    const [logError, setLogError] = useState<boolean>(false)
    const [loading, setLoading] = useState<boolean>(true)
    const { get_user_with_token } = useAuth()

    useEffect(() => {
        const check_user = async() => {
            const user = await get_user_with_token()
            if (!user){
                setLogError(true)
            }
            setLoading(false)
        }
        check_user()
    }, [])

    let error_div
    
    if(error_message) {
        error_div = <IonItem> Msg: {error_message} </IonItem>
    }

    return (
        <IonContent fullscreen>
            <IonItem>Error</IonItem>
            {!loading && 
                logError ? (
                    <IonItem>User expired, try to log in again:  <a href='/login'>login</a></IonItem>
                ) : error_div
            }
        </IonContent>    
    )
}

export default ErrorPage;