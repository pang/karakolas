import React, { useState } from "react";
import { IonButton, IonIcon } from "@ionic/react";
import {
  closeOutline,
  informationCircle,
  checkmarkCircle,
  warning,
  closeCircle,
  cube,
} from "ionicons/icons";
import "./Alert.css";

type AlertType = "success" | "alert" | "warning" | "reparto" | "info";

interface AlertProps {
  title?: string;
  message: React.ReactNode;
  showClose?: boolean;
  actionButtonText?: string;
  downloadButtonText?: React.ReactNode;
  href?: string;
  onAction?: () => void;
  onDownload?: () => void;
  alertType: AlertType;
}

const getAlertIcon = (alertType: string) => {
  switch (alertType) {
    case "success":
      return checkmarkCircle;
    case "warning":
      return warning;
    case "alert":
      return closeCircle;
    case "reparto":
      return cube;
    case "info":
      return informationCircle;
    default:
      return informationCircle;
  }
};

const Alert: React.FC<AlertProps> = ({
  title,
  message,
  showClose,
  actionButtonText,
  downloadButtonText,
  href,
  onAction,
  onDownload,
  alertType,
}) => {
  const [isVisible, setIsVisible] = useState(true);

  const handleClose = () => {
    console.log("Panel alert CERRADO");
    setIsVisible(false);
  };

  if (!isVisible) return null; // Si no es visible, no se renderiza

  return (
    <div className={`alert_container alert_${alertType}`}>
      <div className="alert_header">
        <IonIcon
          className={`alert_icon alert_icon_${alertType}`}
          icon={getAlertIcon(alertType)}
        />
        {/**El titulo solo lo mostramos si existe */}
        <div className="alert_text_container">
          {title && (
            <div className={`alert_title alert_icon_${alertType}`}>{title}</div>
          )}
          <div className={`alert_message alert_text_${alertType}`}>
            {message}
          </div>
        </div>
        {/**Boton de cerrar arriba a la derecha, se puede no poner*/}
        {showClose && (
          <IonButton
            fill="clear"
            size="small"
            onClick={handleClose}
            className="alert_close_button"
          >
            <IonIcon size="large" icon={closeOutline} />
          </IonButton>
        )}
      </div>

      {/*Botones de accion y download. Zona inferior derecha */}
      <div className="alert_buttons_container">
        <div className="alert_action_buttons">
          {downloadButtonText && (
            <IonButton
              className={`alert_action_button alert_hoja_reparto alert_icon_${alertType}`}
              fill="outline"
              size="small"
              onClick={onDownload}
            >
              {downloadButtonText}
            </IonButton>
          )}
          {actionButtonText && (
            <IonButton
              className={`alert_action_button alert_icon_${alertType}`}
              fill="outline"
              size="small"
              href={href}
              onClick={onAction}
            >
              {actionButtonText}
            </IonButton>
          )}
        </div>
      </div>
    </div>
  );
};

export default Alert;
