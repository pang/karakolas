import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import {
  IonTabBar,
  IonTabButton,
  IonLabel,
  IonIcon,
} from "@ionic/react";
import {
  personCircleOutline,
  peopleCircleOutline,
  nutrition,
  calculator,
  people,
  basket,
} from "ionicons/icons";
import { useAuth } from "../context/AuthContext";
import "./Tabs.css";

const Tabs: React.FC = () => {
  const { user, group, logout } = useAuth();
  const history = useHistory();

  const handleLogout = async () => {
    try {
      await logout();
      history.push("/");
    } catch (error) {
      console.log(error);
    }
  };


  return (
      <IonTabBar slot="bottom" className="show_on_mobile">
        <IonTabButton className="small_tabs" tab="tabPedidosGrupo" href="/tab1">
        <IonIcon aria-hidden="true" icon={people} />
        <IonLabel>PEDIDOS GRUPO</IonLabel>
        </IonTabButton>
        <IonTabButton className="small_tabs" tab="tabGrupo" href="/tab1">
        <IonIcon aria-hidden="true" icon={peopleCircleOutline} />
        <IonLabel className="uppercase_text">{group && group.nombre}{" >"}</IonLabel>
        </IonTabButton>
        <IonTabButton  className="tab_pedir" tab="tabPedir" onClick={() => history.push("/order/dashboard")}
        >
          <IonIcon aria-hidden="true" icon={basket} />
          <IonLabel>PEDIR</IonLabel>
        </IonTabButton>

        <IonTabButton className="small_tabs" tab="tabProductores" href={`productores/${group.id}`}>
          <IonIcon aria-hidden="true" icon={nutrition} />
          <IonLabel>PRODUCTORES</IonLabel>
        </IonTabButton>
        <IonTabButton className="small_tabs"  tab="tabContabilidad" href="/tab3">
          <IonIcon aria-hidden="true" icon={calculator} />
          <IonLabel>CONTABILIDAD</IonLabel>
        </IonTabButton>
      </IonTabBar>
  );
};

export default Tabs;
