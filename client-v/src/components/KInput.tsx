import React, { FC } from "react";
import { IonItem, IonLabel, IonInput } from "@ionic/react";

export interface KInputProps {
  name: string;
  register?: Register;
  label?: string;
  type?: string;
  given_value?: string;
  on_change_fn?: Object;
  component?: JSX.Element;
}

const KInput: FC<KInputProps> = ({
  name,
  register,
  type,
  given_value,
  on_change_fn,
  // control,
  // component,
  label,
}) => {
  
    
    // onIonChange = { (e) => console.log("ionChange", e) }
    // onIonChange = { (e) => console.log("ionChange", e) }
    
  const custom_fn = (e) => {
    console.log(`Gotcha! :: event :`)
    console.log(e)
  }
  
  return (
    <>
      <IonItem>
        {label && (
          <IonLabel position="floating">{label}</IonLabel>
        )}
        <IonInput
            type={type}
            { ...register(name, {
                value: given_value,
            })} 
            onIonChange={on_change_fn}
        />
      </IonItem>
    </>
  );
};

export default KInput;
