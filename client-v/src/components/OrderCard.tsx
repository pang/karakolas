import {
  IonButton,
  IonCard,
  IonText,
  IonCardTitle,
  IonCardContent,
  IonBadge,
  IonIcon,
} from "@ionic/react";

import { createOutline, closeOutline } from "ionicons/icons";

import Circle from "./common/Circle";
import Badge from "./common/Badge";
import "./OrderCard.css";
//3 tipos distintos de OrderCard. Pedir, editar y reabrir
//Para cambiar ligeramente los botones de la parte inferior
type ButtonType = "PEDIR" | "EDITAR" | "REABRIR" | null;

interface OrderCardProps {
  record: Record<string, any>;
  butonType?: ButtonType;
}

const OrderCard: React.FC<OrderCardProps> = ({ record, actionType = null }) => {
  const handleClick = (id: number, cancelar?: boolean) => {
    switch (actionType) {
      case "PEDIR":
        console.log(`Pedido ${id} clicado para hacer un pedido`);
        break;
      case "EDITAR"://En ese tipo hay 2 botones Editar-Cancelar pedirdo
        if (cancelar) {
          console.log(`TODO: Cancelar pedido ${id}`);
        } else {
          console.log(`TODO: Editar pedido ${id}`);
        }
        break;
      case "REABRIR":
        console.log(`TODO: Solicitar reapertura ${id}`);
        break;
      default:
        console.log(`Ordercard sin botones para el pedido ${id}`);
    }
  };

  const renderActionButton = () => {
    switch (actionType) {
      case "PEDIR":
        return (
          <IonButton
            expand="block"
            href={`/order/${record.pedido.id}`}
            onClick={() => handleClick(record.pedido.id)}
          >
            PEDIR
          </IonButton>
        );
      case "EDITAR":
        return (
          <>
            <IonButton
              size="small"
              fill="outline"
              onClick={() => handleClick(record.pedido.id)}
            >
              <IonIcon icon={createOutline} /> EDITAR PEDIDO
            </IonButton>
            <IonButton
              size="small"
              fill="clear"
              onClick={() => handleClick(record.pedido.id, true)}
              color="danger"
            >
              <IonIcon icon={closeOutline} /> CANCELAR PEDIDO
            </IonButton>
          </>
        );
      case "REABRIR":
        return (
          <IonButton
            className="ordercard_link_reapertura ordercard_button"
            fill="clear"
            expand="block"
            onClick={() => handleClick(record.pedido.id)}
          >
            Solicitar reapertura
          </IonButton>
        );
      default:
        return null;
    }
  };
  
  // Abierto/cerrado
  const p_status = record.pedido.abierto ? "Abierto" : "Cerrado"
  
  return (
    <IonCard className="ordercard_main" key={record.pedido.id}>
      <Badge status={p_status} type="Esquina" />
      <img src="https://ionicframework.com/docs/img/demos/card-media.png" />
      <div className="ordercard_date_header">
        <IonText className="ordercard_cierre_date">
          <Circle color={"blue"} />
          CIERRE:
          {record.pedido.info_cierre
            ? record.pedido.info_cierre
            : "No hay información asociada"}
        </IonText>
        <IonText className="ordercard_reparto_date">
          <Circle color={"purple"} />
          REPARTO: {record.pedido.fecha_reparto}
        </IonText>
      </div>
      <IonCardContent className="ordercard_content">
        <IonCardTitle className="ordercard_pedido_title">
          Pedido {record.productor.nombre}
        </IonCardTitle>

        <IonText className="ordercard_pedido_text">
          INFORMACIÓN ADICIONAL A PROPORCIONAR
        </IonText>
        <div className="ordercard_button">{renderActionButton()}</div>
      </IonCardContent>
    </IonCard>
  );
};

export default OrderCard;
