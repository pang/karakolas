import axios from 'axios'
import { useQuery, useMutation } from 'react-query'
import toast from 'react-hot-toast'

// const base_url = `https://v2.jokeapi.dev`
// Can be passed as param as well
const BASE_URL = `https://v2.jokeapi.dev`


// Error handlers - should go on a separate file
// Custom error handling function - display toast
const handleError = (error: unknown, query: Query) => {
    console.log(`[api.ts] : handleError :: ${error.message} : showing toast`);
    toast.error(`${error.message}`, { position: 'bottom-center'} )
    console.log(`query was:\n ${JSON.stringify(query,null,2)}`);
}



// TODO - Use api instance - constructor with url
/*
const apiClient = axios.create({
  baseURL: BASE_URL,
  headers: {
    'Content-Type': 'application/json',
    // You can add other headers like authorization token here
  },
});
*/
// Basic axios functions
// GET
const _myget =  async (endpoint: string, base_url:string = BASE_URL, display: Boolean =false) => {

    const api_url = `${base_url}/${endpoint}`
    console.log(`GET to ${api_url}`)
    
    // Credentials modification, needs to be true with py4web API to manage JWT
    axios.defaults.withCredentials = true
    if (base_url == BASE_URL) {
      axios.defaults.withCredentials = false
    }
    const response =  await axios.get(api_url)
    console.log(`GET [${response.status}] ${api_url}`)
    
    if (display) {
        console.log(`${JSON.stringify(response,null,2)}`)
    }
    //Data only
    // console.log(`data   :\n${JSON.stringify(response.data,null,2)}`)
    return response.data
}

// DELETE
const _mydelete =  async (endpoint: string, base_url:string = BASE_URL, delete_id:string, display: Boolean =false) => {

    const api_url = `${base_url}/${endpoint}/${delete_id}`
    console.log(`DELETE to ${api_url}`)
    
    const response =  await axios.delete(api_url)
    console.log(`DELETE [${response.status}] ${api_url}`)
    
    if (display) {
        console.log(`${JSON.stringify(response,null,2)}`)
    }
    //Data only
    // console.log(`data   :\n${JSON.stringify(response.data,null,2)}`)
    return response.data
}

// POST / PUT
const _mydatareq =  async (req_type: string='POST', endpoint: string, base_url:string = BASE_URL, api_data, display: Boolean = false) => {

    const api_url = `${base_url}/${endpoint}`
    console.log(`${req_type} to ${api_url}`)
    
    const response = req_type==='PUT'
        ? await axios.put(api_url, api_data)
        : await axios.post(api_url, api_data)
    console.log(`${req_type} [${response.status}] ${api_url}`)
    
    if (display) {
        // Whole response
        console.log(`${JSON.stringify(response,null,2)}`)
    }
    // Data only
    // console.log(`data   :\n${JSON.stringify(response.data,null,2)}`)
    return response.data
}


/** useQuery based functions
 *  returns whatever useQuery is returning
 */
// const _queryget = (key, config = {}) => {   
const _queryget = (querykey, base_url=BASE_URL, endpoint: string, display: Boolean = false, queryConfigParams = {}) => {   
  return useQuery({
    queryKey: [ querykey ],
    queryFn: () => _myget (endpoint, base_url, display), // fn
    onError : handleError,
    retry: false,
    ...queryConfigParams,
  })
}

/** useMutation based - returns mutation object **/
const _mut = (req_type:string, querykey, base_url=BASE_URL, endpoint: string, display: Boolean = false, queryConfigParams = {}) => {   
   
  return useMutation({
    mutationFn: (newRecord) => _mydatareq (req_type, endpoint, base_url, newRecord, display), // fn  return axios.post('/todos', newTodo)
    onError : handleError,
    ...queryConfigParams,
  })
}

const _mdelete = (querykey, base_url=BASE_URL, endpoint: string, display: Boolean = false, queryConfigParams = {}) => {   
    
  return useMutation({
    mutationFn: (delete_id) => _mydelete (endpoint, base_url, delete_id, display),
    onError : handleError,
    ...queryConfigParams,
  })
}


// Export API methods
export { 
    _myget, 
    _queryget,
    _mut,
    _mdelete,
};
