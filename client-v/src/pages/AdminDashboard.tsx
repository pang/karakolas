import { IonContent, IonPage, IonItem, IonGrid, IonRow, IonIcon, IonCol, IonDatetime, IonButton, IonProgressBar } from '@ionic/react';
import { useEffect, useState } from "react";
import { useHistory } from 'react-router-dom';
// import { useNavigate } from "react-router-dom";

import { _queryget, _mut, _mdelete } from '../adapters/api'
import Header from '../components/Header';
import { useAuth } from '../context/AuthContext';
import { closeOutline, colorFill, colorFillOutline, colorFillSharp, colorFilter, colorFilterOutline, colorFilterSharp, colorPalette, colorWand, colorWandOutline, colorWandSharp, informationCircle, informationCircleOutline } from 'ionicons/icons';

import './AdminDashboard.css'

const api_url = `http://localhost:7000`

const AdminDashboard: React.FC = () => {
  
  let history = useHistory();
  const { user, group } = useAuth();

  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    if (user && group){
        setLoading(false)
    }
  }, [user, group])
  
  const lastRequestMock = [
    {date: '8/8/2024', name:'Vega del tajuña', type:'reparto', progress: '5/14'},
    {date: '23/8/2024', name:'Huerta Pepines', type:'abierto', progress: '10/14'},
    {date: '23/8/2024', name:'Quesos Zarandiel', type:'abierto', progress: '2/14'},
    {date: '28/8/2024', name:'Menudos Huertos', type:'abierto', progress: '5/14'},
    {date: '28/8/2024', name:'Ché Madrid Dulces y Salados', type:'abierto', progress: '5/14'},
  ]

  const progressStrToValue = (progressStr) => parseInt(progressStr.split("/")[0]) / parseInt(progressStr.split("/")[1])

  const pedidosRow = lastRequestMock.map((request) => {
    if (request.type === 'abierto'){
        return (
            <tr>
                <td>{request.date}</td>
                <td><strong>{request.name}</strong></td>
                <td><div className='badgePedido' style={{backgroundColor:'#27ae60'}}>Abierto</div></td>
                <td> <div style={{display:'flex', flexDirection:'column', justifyContent:'center', alignItems:'center', height:'100%'}}>{request.progress}</div><IonProgressBar color='success' value={progressStrToValue(request.progress)}/></td>
                <td><IonButton color='success' fill='outline'>Ver pedido</IonButton> <IonButton fill='outline'>Cerrar pedido</IonButton></td>
            </tr>
        )
    } else if (request.type === 'reparto'){
        return (
            <tr>
                <td>{request.date}</td>
                <td><strong>{request.name}</strong></td>
                <td><div className='badgePedido' style={{backgroundColor:'#9b51e0'}}>En reparto</div></td>
                <td> <div style={{display:'flex', flexDirection:'column', justifyContent:'center', alignItems:'center', height:'100%'}}>{request.progress}</div><IonProgressBar color='success' value={progressStrToValue(request.progress)}/></td>
                <td><IonButton style={{color:'#9747FF', border:'0.2em solid #9747FF', borderRadius:'0.2em'}} fill='clear'>Hacer recuento de pedido</IonButton></td>
            </tr>
        )
    }
  })

  return (
    <IonPage>
      <Header></Header>
      { !loading || !user || !group ? (
      <IonContent fullscreen>
       <IonGrid style={{margin: '0 5%', paddingLeft: '4em', paddingRight: '4em', paddingTop:'4em'}}>
          <IonRow>
            <div style={{fontSize: '3em', paddingBottom:'0.5em'}}>
                ¡Hola, {user && user.username}!
            </div>
            </IonRow>
          <IonRow>
            <div style={{fontSize: '1em', paddingBottom:'0.5em'}}>
              Estás en el grupo <strong>"{group ? group.name : 'ERROR'}"</strong> <IonIcon icon={informationCircleOutline}/>
            </div>
          </IonRow>
          <IonRow>
          <IonCol size='9' style={{padding: '0'}}>
          <div style={{
                  border: '8px',
                  padding: '8px',
                  backgroundColor: '#EBF2FFCC',
                  maxWidth: '60%',
                  position:'relative'
                 }}>
                  <div style={{display:'flex', paddingBottom:'3em'}}><IonIcon style={{color: '#0054e9'}} size='large' icon={informationCircle}/><div style={{ paddingTop:'0.2em' ,marginLeft:'1em'}}>La fecha de cierre de pedido de <strong>"Huerta Pepines"</strong> en reparto el 23/8/2024 es <strong>hoy</strong></div><div style={{paddingLeft: '1em'}}><IonButton fill='clear' size='small'><IonIcon size='large' icon={closeOutline}/></IonButton></div></div>
                  <div style={{position: 'absolute', right: '1em', bottom: '1em'}}><IonButton fill='outline'>Cerrar pedido</IonButton></div>
                </div>
              <IonRow>
                <div style={{fontSize: '1.5em', paddingTop:'0.5em', paddingBottom:'0.5em'}}>
                  <strong>Últimos pedidos</strong>
                </div>
              </IonRow>
              <table>
                {pedidosRow}
              </table>
            </IonCol>
            <IonCol style={{width: '40%'}}>
              <IonDatetime 
                presentation="date"
                // max="2027" max year to be able to select, if not, it's present year
                // highlightedDates={[
                  // {
                  //   date: '2023-01-05',
                  //   textColor: '#800080',
                  //   backgroundColor: '#ffc0cb',
                  // },]}
              ></IonDatetime>
              <div style ={{fontSize: '0.8em'}}>
              <div style={{
            borderRadius:'50%',
            backgroundColor: 'green',
            display:'inline-block',
            width: '0.8em',
            height: '0.8em'
          }}></div> APERTURA    <div style={{
            borderRadius:'50%',
            backgroundColor: 'blue',
            display:'inline-block',
            width: '0.8em',
            height: '0.8em'
          }}></div> CIERRE    <div style={{
            borderRadius:'50%',
            backgroundColor: 'purple',
            display:'inline-block',
            width: '0.8em',
            height: '0.8em'
          }}></div> REPARTO</div>
            </IonCol>
            </IonRow>
        </IonGrid>
      </IonContent>
          ) : (<IonContent fullscreen>
            <IonItem>Loading...</IonItem>
        </IonContent>)
    }
    </IonPage>
  );
};

export default AdminDashboard;
