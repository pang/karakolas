import { IonCard, IonContent, IonPage, IonImg } from "@ionic/react";
import { useHistory } from "react-router-dom";
import "./Groups.css";
import { useAuth } from "../context/AuthContext";
import { useEffect } from "react";

const Groups: React.FC = () => {
  let history = useHistory();

  const { user, group, setGroup } = useAuth();

  const handleCardClick = async (grupo: any) => {
    await localStorage.setItem("group", JSON.stringify(grupo));
    await setGroup(grupo);
  };

  useEffect(() => {
    if (group !== null) {
      console.log("group distinto de null:", group);
      history.push("/home");
    }
  }, [group]);

  const groupCards =
    user &&
    user.groups.map((grupo: Record<string, any>) => {
      return (
        <div key={grupo.id} className="group_card_container ion-text-center">
          <IonCard
            onClick={() => handleCardClick(grupo)}
            className="group_card"
          >
            <img
              className="group_card_img"
              alt=""
              src="/assets/groups/ciguena.svg"
            />
          </IonCard>
          <div className="group_name"> {grupo.nombre}</div>
          {/*Usa distinta clase css dependiendo de admin o no */}
          <div
            className={`badgeRol ${
              grupo.isAdmin ? "admin_badge" : "member_badge"
            }`}
          >
            <div className="badge_text">
              {grupo.isAdmin ? "Admin" : "Miembro"}
            </div>
          </div>
        </div>
      );
    });

  return (
    <IonPage>
      {/* <Header title="Karakolas"/> */}
      <IonContent fullscreen>
        <div className="logo_container">
          <IonImg src="/assets/logo_completo.svg" />
        </div>
        <div className="content_container">
          <div className="title">Tus grupos de consumo</div>
          <div className="group_cards_container">{groupCards}</div>
        </div>
      </IonContent>
    </IonPage>
  );
};

export default Groups;
