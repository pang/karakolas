import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonButton, IonItem, IonGrid} from '@ionic/react';
import { useEffect, useState } from "react";
import ExploreContainer from '../components/ExploreContainer';
import './Tab3.css';
import KToast from '../components/KToast';
import { getAPI } from "../services/cust_api";


const Tab3: React.FC = () => {
  
  const [ name, setName ] = useState('Dear');
  
  const [data, setData] = useState({});
  const [message, setMessage] = useState({});
  const [err, setErr] = useState(false);
  
  const api_url = `https://v2.jokeapi.dev/joke/any?type=single`
  const err_url = `http://localhost:7000/error`
    
  // Start by loading data
  useEffect(() => { // Runs after every render
    console.log("Tab3 : useEffect !")
    loadData();
  }, []);
    
  const [toastProps, setToastProps] = useState({
    isToastOpen: false,
    message: "",
  });
  

  /** Use getAPI() calls **/
  async function loadData() {
      const loadedData = await getAPI( api_url );
      console.log(`loadedData: ${JSON.stringify(loadedData)}`)
      
      if ( loadedData.error !== true ) {
        
        setData(loadedData);
      }
      else { // Else - error - display toast?
        console.log (`Error : msg : ${loadedData.error_msg} : setting state.err to 'true'`)
        setMessage (`${loadedData.error_msg}`)
        setData(loadedData);
        setErr (true)
        openToast("Error with /jokes API")
      }
      
  }

  /**  Will always get error -> oppen toast **/
  async function getError() {
    const errData = await getAPI( err_url );
    console.log(`errData: ${JSON.stringify(errData,null,2)}`)
    if ( errData.error === true ) {
        openToast("An error (expected) has occured")
    }
  }
  
  /** Button Click handlers **/
  // Another one
  const handleClick = () => {
    console.log(`clicked! Getting new data`)
    loadData() 
  }  
  
  // Fail button (/error endpt)
  const handleFailClick = () => {
    console.log(`clicked! Getting Failed`)
    getError()
  }  
  
  /** KToast handlers **/
  const openToast = (message_str) => {
    console.log(`Opening toast`)
    setToastProps({message: message_str, isToastOpen: true})
  }  
  
  const dismissHandler = () => {
    console.log("Dismissed mf!! set isToastOpen to false")
    setToastProps({...toastProps, isToastOpen: false})
  }
  
  // const error_item = message == '{}'
  const error_item = err
    ? ( <IonItem>
          <p> Error : {message}</p>
        </IonItem> )
    : null
  
  const joke_item = ! err
    ? <IonItem>
        <p> Joke : {data.joke} </p>
      </IonItem>
    : null
  
  // const btn_disabled = false
  const btn_disabled = err ? true : false
  
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Jokes requests</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Requests</IonTitle>
          </IonToolbar>
        </IonHeader>

        <IonItem>
          <h2> Hi {name}!</h2>
        </IonItem>
        
        {joke_item}
        
        {error_item}
        
        <IonItem>
          <IonButton onClick = {handleClick} disabled = {btn_disabled} expand="block">Another one!</IonButton>
          <IonButton color="danger" onClick = {handleFailClick} expand="block">Fail</IonButton>

        </IonItem>
        
        <KToast message={toastProps.message} open={toastProps.isToastOpen} dismissHandler={dismissHandler}/>

      </IonContent>
    </IonPage>
  );
  
};

export default Tab3;
