// src/pages/Login.tsx
import React, { useState } from "react";
import {
  IonButton,
  IonInput,
  IonContent,
  IonPage,
  IonImg,
  IonTitle,
  IonGrid,
  IonRow,
  IonCol,
  IonAlert,
} from "@ionic/react";
import { useAuth } from "../context/AuthContext";
import { _queryget } from "../adapters/api";
import { useHistory } from "react-router-dom";
import "./Login.css";

const Login: React.FC = () => {
  const { login } = useAuth();
  const [loginUsername, setLoginUsername] = useState("");
  let history = useHistory();

  // const [username, setUsername] = useState('');
  const [password, setPassword] = useState("");

  // Para errores en el login
  const [showErrorAlert, setShowErrorAlert] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  const handleLogin = async () => {
    try {
      if (!password || !loginUsername) {
        console.log("Password and username needed");
        setErrorMessage("Introduce usuario y contraseña");
        setShowErrorAlert(true);
      } else {
        let userlogged = await login(loginUsername, password);
        console.log(`User logged: ${userlogged}`);
        history.push("/");
      }
    } catch (error) {
      console.error("Login error:", error);
      setErrorMessage(
        "Error en el inicio de sesión. Verifica tus datos e inténtalo nuevamente."
      );
      setShowErrorAlert(true);
    }
  };

  const handleKeyUp = (event: React.KeyboardEvent) => {
    if (event.key === "Enter") {
      handleLogin();
    }
  };

  return (
    <IonPage>
      <IonContent fullscreen>
        <IonGrid>
          <IonRow>
            {/* Columna para la imagen: se muestra solo en pantallas grandes */}
            <IonCol sizeMd="6" className="ion-hide-md-down log_left_column">
              <IonImg
                className="log_img_container"
                src="/assets/ilustracion_login.png"
              />
            </IonCol>

            {/* Columna para el formulario de login */}
            <IonCol size="12" sizeMd="6">
              <div className="log_login_container">
                <IonImg
                  className="log_karakolas_logo"
                  src="/assets/logo_completo.svg"
                />
                {/*div para toda la zona de textos, input y botones*/}
                <div className="log_text_container">
                  <IonTitle className="log_title">Inicio de sesión</IonTitle>
                  <p className="log_text_def">
                    Introduce tu nombre de usuario o correo electrónico y
                    contraseña para acceder a la plataforma de Karakolas.
                  </p>
                  <div className="log_user_inputs">
                    <IonInput
                      fill="outline"
                      placeholder="nombre de usuario"
                      value={loginUsername}
                      onIonChange={(e) => setLoginUsername(e.detail.value!)}
                      onKeyUp={handleKeyUp}
                    />
                  </div>
                  <div className="log_user_inputs">
                    <IonInput
                      fill="outline"
                      type="password"
                      placeholder="contraseña"
                      value={password}
                      onIonChange={(e) => setPassword(e.detail.value!)}
                      onKeyUp={handleKeyUp}
                    />
                  </div>
                  <div className="log_forgot_password">
                    <IonButton fill="clear">
                      ¿Has olvidado tu contraseña?
                    </IonButton>
                  </div>
                  <div className="log_login_buttons">
                    <IonButton expand="block" onClick={handleLogin}>
                      Entrar
                    </IonButton>
                    <IonButton expand="block" href="/signup">
                      Registrarse
                    </IonButton>
                  </div>
                </div>
              </div>
            </IonCol>
          </IonRow>
        </IonGrid>

        {/* Modal de error de login */}
        <IonAlert
          isOpen={showErrorAlert}
          onDidDismiss={() => setShowErrorAlert(false)}
          header={"Error de inicio de sesión"}
          message={errorMessage}
          buttons={["OK"]}
        />
      </IonContent>
    </IonPage>
  );
};

export default Login;
