import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonButton, IonItem, IonGrid} from '@ionic/react';

import { useQuery } from 'react-query'
import { _myget, _queryget } from '../adapters/api'

const Requests: React.FC = () => {
 
  const display = false // used to debug _queryget calls
  
  // Jokes data
  const { isLoading, isError, error, data, refetch } = _queryget('jokes',undefined,`joke/any?type=single`, display)
  
  // This should throw a network error  
  const netwk_rsp = _queryget('network', 'http://localhost:4444', 'anydata', false, {enabled: false})
  
  
  // jokes /employees is a 404
  const fail_rsp = _queryget('fail', undefined, 'employees', true, {enabled: false})
  
  const fail_btn_text = fail_rsp.status
  
  // Button Clicks
  const handleClick = () => {
    console.log(`Another one clicked! Getting another joke`)
    refetch()  
  }  
  
  const handleNetworkClick = () => {
    console.log(`clicked! Getting network`)
    netwk_rsp.refetch()
    console.log(`network error: ${netwk_rsp.isError}`)
  }  
  
  const handleFailClick = () => {
    console.log(`clicked! Getting Failed`)
    fail_rsp.refetch()
    console.log(`Fail error: ${fail_rsp.isError}`)
  }  
  
  let body_data = null
  if (isLoading) {
    body_data = <IonItem><p>Loading...</p></IonItem>
  }
  if (isError) {
    console.log(`error found!`)
    body_data = <IonItem><p>Error: {error.message}</p></IonItem>
  }
  if (data) {
    body_data = <IonItem><p>{data.joke}</p></IonItem>
  }
  
  const btn_disabled = isError ? true : false
  
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Requests Playground</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Requests</IonTitle>
          </IonToolbar>
        </IonHeader>

        <IonItem>
          <p> This is a sample playground to try and manage API GET requests</p>
        </IonItem>
        <IonItem>
          <p> Jokes are auto-fetched on load, and refetched with the "Another one" button.</p>
        </IonItem>
        <IonItem>
          <p> Fail button points to an invalid endpoint: will receive a 404 after a few seconds and show a toast. mutation.status is shown in [brackets]</p>
        </IonItem>
        <IonItem>
          <p> Neterror button points to a non-reachable backend: will instantly show a network error toast</p>
        </IonItem>
        
        <IonItem>
          <h3> Joke:</h3>
        </IonItem>
        
        {body_data}
        
        <IonItem>
          <IonButton onClick = {handleClick} disabled = {btn_disabled} expand="block">Another one</IonButton>
          <IonButton color="danger"  onClick = {handleNetworkClick} expand="block">NetError</IonButton>
          <IonButton color="warning" onClick = {handleFailClick} expand="block">Fail (404) [{fail_btn_text}]</IonButton>
        </IonItem>
        
      </IonContent>
    </IonPage>
  );
  
};


export default Requests;
