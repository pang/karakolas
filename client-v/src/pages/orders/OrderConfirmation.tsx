import { IonPage, IonContent, IonItem, IonIcon, IonText, IonButton  } from "@ionic/react";
import { useEffect, useCallback, useState } from "react";
import { useHistory } from "react-router-dom";
import { closeOutline, informationCircle } from 'ionicons/icons';


import { useAuth } from '../../context/AuthContext';
import './OrderConfirmation.css'

import Header from "../../components/Header";

const OrderConfirmation: React.FC = () => {

    const { user, group } = useAuth();
    let history = useHistory();
    const [loading, setLoading] = useState<boolean>(true);
    
    useEffect(() => {
        if (user && group){
            setLoading(false)
        }
    }, [user, group])

    // const [isPopOverUsuarioMiembroOpen, setPopOverUsuarioMiembroOpen] = useState(false);
    // const [isSubmenuGrupomiembroOpen, setSubmenuGrupomiembroOpen] = useState(false);
    // const openPopOverUsuarioMiembro = useCallback(() => {
    // setPopOverUsuarioMiembroOpen(true);
    // }, []);
    // const closePopOverUsuarioMiembro = useCallback(() => {
    // setPopOverUsuarioMiembroOpen(false);
    // }, []);
    // const openSubmenuGrupomiembro = useCallback(() => {
    // setSubmenuGrupomiembroOpen(true);
    // }, []);
    // const closeSubmenuGrupomiembro = useCallback(() => {
    // setSubmenuGrupomiembroOpen(false);
    // }, []);
    // const onButtonContainerClick = useCallback(() => {
    // // Add your code here
    // }, []);
    // return (
    // {/* {isPopOverUsuarioMiembroOpen && (
    // <PortalPopup
    // overlayColor="rgba(113, 113, 113, 0.3)"
    // placement="Centered"
    // onOutsideClick={closePopOverUsuarioMiembro}
    // >
    // <PopOverUsuarioMiembro onClose={closePopOverUsuarioMiembro} />
    // </PortalPopup>
    // )}
    // {isSubmenuGrupomiembroOpen && (
    // <PortalPopup
    // overlayColor="rgba(113, 113, 113, 0.3)"
    // placement="Centered"
    // onOutsideClick={closeSubmenuGrupomiembro}
    // >
    // <SubmenuGrupomiembro onClose={closeSubmenuGrupomiembro} />
    // </PortalPopup>
    // )} */}
    // );
    // };
    
    return (<IonPage>
        <Header></Header>
        { !loading || !user || !group ? (
        <IonContent fullscreen>
            <div style={{paddingLeft: '10%', paddingTop:'3em'}}>
                <div style={{ width: '50%', fontSize: '32px', paddingBottom:'1em'}}>Tus pedidos a Menudos Huertos y Ché Madrid Dulces y Salados han sido enviados</div>
                <div style={{
                  border: '8px',
                  padding: '8px',
                  backgroundColor: '#EBF2FFCC',
                  maxWidth: '40%',
                  position:'relative',
                 }}>
                    <div style={{display:'flex'}}><IonIcon style={{color: '#0054e9'}} size='large' icon={informationCircle}/><div style={{ paddingTop:'0.2em' ,marginLeft:'1em'}}>
                        Recuerda que los precios de los pedidos son orientativos cuando los productos se piden por unidades y se cobran por kg. (PU/CK)
                    </div>
                    <div style={{paddingLeft: '1em'}}><IonButton fill='clear' size='small'><IonIcon size='large' icon={closeOutline}/></IonButton></div></div>
                </div>
                <table>
                    <tr>
                        <td>Pedido a Menudos Huertos</td>
                        <td><strong>50.8€</strong></td>
                        <td>
                            <div className='fullBadgeCierre'>
                                <div style={{position:'relative', lineHeight:'140%', fontWeight:'500'}}>
                                    <div className='badgeCierre' style={{backgroundColor:'#d4e4ff'}}>
                                        Cierre:
                                    </div>
                                </div>
                                16/8/24
                            </div>
                        </td>
                        <td>
                            <div className='fullBadgeCierre'>
                                <div style={{position:'relative', lineHeight:'140%', fontWeight:'500'}}>
                                    <div className='badgeCierre' style={{backgroundColor:'#f1e1ff'}}>
                                        En reparto
                                    </div>
                                </div>
                                28/8/24
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Ché Madrid Dulces y salados</td>
                        <td><strong>50.8€</strong></td>
                        <td>
                            <div className='fullBadgeCierre'>
                                <div style={{position:'relative', lineHeight:'140%', fontWeight:'500'}}>
                                    <div className='badgeCierre' style={{backgroundColor:'#d4e4ff'}}>
                                        Cierre:
                                    </div>
                                </div>
                                16/8/24
                            </div>
                        </td>
                        <td>
                            <div className='fullBadgeCierre'>
                                <div style={{position:'relative', lineHeight:'140%', fontWeight:'500'}}>
                                    <div className='badgeCierre' style={{backgroundColor:'#f1e1ff'}}>
                                        En reparto
                                    </div>
                                </div>
                                28/8/24
                            </div>
                        </td>
                    </tr>
                </table>
                <div style={{ width: '60%', paddingTop:'1em'}} >
                    Una vez el grupo reciba los pedidos y se haya verificado qué productos han llegado, podrás hacer los pagos. Espera a que el administrador del productor de el visto bueno para proceder con los pagos antes de pagar.
                </div>
                <div style={{ paddingTop: '3em'}} >
                    <IonButton>Volver al inicio</IonButton>
                    <IonButton fill='outline'>Editar pedido</IonButton>
                    <IonButton fill='outline'>Hacer otro pedido</IonButton>
                    <IonButton fill='clear' style={{color:'red'}}><IonIcon icon={closeOutline}/>Cancelar pedido</IonButton>
                 </div>
            </div>

            </IonContent>
        ) : (<IonContent fullscreen>
        <IonItem>Loading...</IonItem>
        </IonContent>)
        }
        </IonPage>)

}

export default OrderConfirmation