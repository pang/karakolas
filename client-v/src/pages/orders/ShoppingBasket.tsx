import { IonPage, IonContent, IonItem, IonIcon, IonText, IonButton, IonButtons  } from "@ionic/react";
import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

import { useAuth } from '../../context/AuthContext';

import Header from "../../components/Header";
import { arrowBackOutline } from "ionicons/icons";
import OrderTable from "../../components/OrderTable";


const ShoppingBasket: React.FC = () => {

    const { user, group } = useAuth();
    let history = useHistory();
    const [loading, setLoading] = useState<boolean>(true);
    
    useEffect(() => {
        if (user && group){
            setLoading(false)
        }
    }, [user, group])

    const mockOrders1 = [
        {product: 'Lechuga batavia', number: '1', observations: '400g lechuga aprox', price: '2', units:'kg'},
        {product: 'Sandía', number: '1', observations: 'Pesan entre 4 y 6 kg. Se cobra por peso, se pide por unidad', price: '1.4', units:'kg'},
        {product: 'Pimiento verde italiano', number: '2', observations: 'Unidad 150g aprox', price: '4', units:'kg'},
        {product: 'Tomate jumato', number: '2', observations: '', price: '4', units:'kg'},
    ]

    const mockOrders2 = [
        {product: 'Alfajor', number: '2', observations: 'Fileteado', price: '2.9', units:'ud'},
        {product: 'Cookie', number: '2', observations: '4uds 40gr', price: '1.9', units:'ud'},
        {product: 'Pan centeno 100%', number: '1', observations: '', price: '3.9', units:'ud'},
        {product: 'Pan espelta integral', number: '1', observations: '550gr', price: '1.9', units:'ud'},
    ]
    return (<IonPage>
        <Header></Header>
        { !loading || !user || !group ? (
        <IonContent fullscreen>
            <IonButton fill="clear" style={{color:'black'}} onClick={()=> history.push("/order")}><IonIcon icon={arrowBackOutline}/> Seguir pidiendo</IonButton>
            <div style={{paddingLeft: '10%'}}>
                <h1>Tu cesta</h1>
                <div style={{display:'flex'}}>
                    <h4>Pedido #1 Menudos Huertos</h4> 
                    <div style={{display:'flex', paddingLeft:'2em',  paddingTop:'0.5em' ,justifyItems:'center', alignItems: 'center'}}>
                        <div className="ion-margin-end" style={{
                            position: 'relative',
                            borderRadius: '8px',
                            backgroundColor: '#f1e1ff',
                            width: '100%',
                            display: 'flex',
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'center',
                            padding: '4px 6px',
                            boxSizing: 'border-box',
                            }}>REPARTO EL 28/7/24</div>
                    </div>
                </div>
                <OrderTable order={mockOrders1}/>
                <div style={{display:'flex'}}>
                    <h4>Pedido #2 Ché Madrid Dulces y Salados</h4> 
                    <div style={{display:'flex', paddingLeft:'2em',  paddingTop:'0.5em' ,justifyItems:'center', alignItems: 'center'}}>
                        <div className="ion-margin-end" style={{
                            position: 'relative',
                            borderRadius: '8px',
                            backgroundColor: '#f1e1ff',
                            width: '100%',
                            display: 'flex',
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'center',
                            padding: '4px 6px',
                            boxSizing: 'border-box',
                            }}>REPARTO EL 28/7/24</div>
                    </div>
                </div>
                <OrderTable order={mockOrders2}/>
                <div style={{display: 'flex', justifyContent:'space-between', width:'929px', padding:'1em 1em 5em 1em'}}>
                    <div>
                        <IonButton color='danger'>Eliminar pedidos</IonButton>
                    </div>
                    <div>
                        <IonButton fill='outline'>Seguir pidiendo</IonButton>
                        <IonButton onClick={() => history.push('/order/confirmation')} style={{ position:'relative', justifyContent: 'flex-end'}}>Enviar pedidos</IonButton>
                    </div>
                </div>
            </div>
            </IonContent>
          ) : (<IonContent fullscreen>
            <IonItem>Loading...</IonItem>
        </IonContent>)
    }
    </IonPage>)
}

export default ShoppingBasket