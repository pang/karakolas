import { IonGrid, IonPage, IonContent, IonItem, IonLabel, IonRow, IonButton, IonIcon, IonAccordion, IonAccordionGroup, IonSearchbar, IonSelect } from "@ionic/react";
import Header from "../../components/Header";
import { useAuth } from "../../context/AuthContext";
import { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import ProductCard from "../../components/ProductCard";
import {  cartSharp, chevronDownOutline, close, grid, listOutline } from "ionicons/icons";

const Order: React.FC = () => {
  
    let history = useHistory();
    
    const { user, group } = useAuth();
    const [loading, setLoading] = useState<boolean>(true);


    useEffect(() => {
        if (user && group){
            setLoading(false)
        }
    }, [user, group])
    
    /** Mocks area */
    
    const mockProducers = [
        { id: 461, producer: 'Eco Sancho', categories: ['Fruta', 'Verdura']},
        { id: 462, producer: 'Menudos Huertos', categories: ['Frutas y verduras']},
        { id: 463, producer: 'Ché Madrid Dulces y Salados', categories: ['Panes', 'Bollería']},
        { id: 464, producer: 'Silvano frutas y conservas', categories: ['Frutas', 'Conservas']}
    ]

    const mockProducts = [
        { name: 'Pepino', price: '2', units: 'kg', img:'https://images.unsplash.com/photo-1449300079323-02e209d9d3a6?q=80&w=1974&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D'},
        { name: 'Berenjena', price: '2.2', units: 'kg', img:'https://images.unsplash.com/photo-1683543122945-513029986574?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTZ8fGJlcmVuamVuYXxlbnwwfHwwfHx8MA%3D%3D'},
        { name: 'Lechuga batavia', additionalInfo: '400gr', price: '1.1', units: 'ud', img:'https://images.unsplash.com/photo-1720456764346-1abff7d43efe?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTZ8fGxlY2h1Z2F8ZW58MHx8MHx8fDA%3D'},
        { name: 'Melón', additionalInfo:'Entre 2 y 3.5kg', price: '1.6', units: 'kg', img:'https://plus.unsplash.com/premium_photo-1675040830254-1d5148d9d0dc?q=80&w=1227&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D'},
        { name: 'Sandía', additionalInfo:'Pesan entre 4y 6kg. Se cobran por peso, se pide por unidad', price: '2', units: 'kg', img:'https://images.unsplash.com/photo-1526841535633-ef3be0b21fd2?q=80&w=1170&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D'},
        { name: 'Nectarina', price: '2', units: 'kg', img:'https://images.unsplash.com/photo-1690474102165-3fb6b57ab5b4?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTJ8fG5lY3RhcmluYXxlbnwwfHwwfHx8MA%3D%3D'},
        { name: 'Tomate kumato', price: '2', units: 'kg', img:'https://images.unsplash.com/photo-1511915274835-e2e34894d687?q=80&w=1032&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D'},
        { name: 'Plátano', additionalInfo:'Procedente de canarias', price: '2', units: 'kg', img:'https://images.unsplash.com/photo-1523667864248-fc55f5bad7e2?q=80&w=1170&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D'}
    ]

    const groupList = mockProducers && mockProducers.map((producer, index) => {
        let groupItem
        console.debug(`mapping producer: [${index}]: ${JSON.stringify(producer)}`)
        if (producer.categories.length > 1){
            groupItem = (
                <IonAccordion
                    key = {producer.id}
                >
                    <IonItem slot="header">
                    <IonLabel>{producer.producer}</IonLabel>
                    </IonItem>
                    <IonItem button  style={{paddingLeft: '1em'}} onClick={ () => console.log("pressed")} slot='content'>
                        Todos
                    </IonItem>
                    {producer.categories.map((category, idx) => {
                        // console.log(`mapping category: [${idx}]: ${category}`) // debug
                        return (
                        <IonItem 
                            key = {idx}
                            button style={{paddingLeft: '1em'}} onClick={ () => console.log("pressed")} slot='content'>
                            {category}
                        </IonItem>
                        )
                    })}

                </IonAccordion>
            )
        } 
        // Just 1 cat
        else {
            groupItem = (
                <IonItem key = {producer.id} button onClick={ () => console.log("pressed")}>
                    {producer.producer}
                </IonItem>
            )
        }
        return groupItem
    })

    const productsList = mockProducts && mockProducts.map((product, idx) => {
        return (
            <ProductCard key={idx} product={product}/>
        )
    })

    return (
        <IonPage>
        <Header/>
        <IonContent fullscreen>
        { !loading || !user || !group ? (
            <>
            <div style={{
                width: '20%',
                position: 'fixed',
                borderRight: '2px solid black',
                height: '100%',
                paddingLeft: '1em'
            }}>
                <h1>Pedidos abiertos</h1>
                <IonAccordionGroup>
                    {groupList}
                </IonAccordionGroup>
                <h1>Otros productores</h1>
            </div>
            <div style={{
                marginLeft: '20%',
                width:'85%',
                padding: '2em',
                paddingRight:'10%'
            }}>
                <div style={{display: 'flex', paddingRight:'4%'}}>
                    <IonSearchbar placeholder='Busca un producto...' autocapitalize='off'></IonSearchbar>
                    <IonItem button>Categoría <div style={{paddingLeft: '1em', paddingRight:'2em'}}><IonIcon icon={chevronDownOutline} /></div></IonItem>
                    <IonButton onClick={() => history.push('/order/shopbasket')} fill="outline"><IonIcon icon={cartSharp}/> Cesta </IonButton>
                </div>
                <div style={{display:'flex'}}>
                    <h1>Menudos huertos</h1> 
                    <div style={{display:'flex', paddingLeft:'2em',  paddingTop:'0.5em' ,justifyItems:'center', alignItems: 'center'}}>
                    <div className="ion-margin-end" style={{
                        position: 'relative',
                        borderRadius: '8px',
                        backgroundColor: '#f1e1ff',
                        width: '100%',
                        display: 'flex',
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                        padding: '4px 6px',
                        boxSizing: 'border-box',
                        }}>REPARTO EL 28/7/24</div>
                    </div>
                </div>
                <div className="ion-margin-end" style={{
                  borderRadius: '0.5em',
                  border: 'solid #3880FF 0.1em',
                  borderColor: '#3880FF',
                  maxWidth: '45%',
                  position: 'relative'
                 }}>
                    <IonButton onClick={() => console.log("click")} fill="clear" style={{position:'absolute', right:'0em', color:'gray' }}><IonIcon icon={close}/></IonButton>
                  <div style={{ paddingLeft:'1.5em', paddingTop:'1em', color:'#666666'}}>¿Deseas recargar el último pedido (17/6/2024) con Menudos Huertos? <br/>
                  Se añadirán a la cesta los productos que pediste en tu último pedido</div> 
                  <div style={{ padding: '0.8em', display:'flex', justifyContent:'flex-end'}}><IonButton slot='end'>CARGAR ÚLTIMO PEDIDO</IonButton></div>
                </div>
                <div style={{display: 'flex', justifyContent:'space-between'}}>
                    <h2>Frutas y verduras</h2> 
                    <div style={{paddingRight: '4%'}}>
                        <IonButton fill='clear' style={{ color:'#666666' }}><IonIcon icon={listOutline}/></IonButton>
                        <IonButton fill='clear' style={{ color:'#666666' }}><IonIcon icon={grid}/></IonButton>
                    </div>
                </div>
                <IonGrid>
                    <IonRow>
                    {productsList}
                    </IonRow>
                </IonGrid>
            </div>
            </>
        ) : (
            <IonItem>Loading...</IonItem>
        )}
        </IonContent>
        </IonPage>
    )

}


export default Order;