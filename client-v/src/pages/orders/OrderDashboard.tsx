import { IonContent, IonPage, IonItem, IonGrid, IonRow, IonCol, IonDatetime } from '@ionic/react';
import { useEffect, useState } from "react";
import { useHistory } from 'react-router-dom';
// import { useNavigate } from "react-router-dom";

import { _queryget, _mut } from '../../adapters/api'
import Header from '../../components/Header';
import { useAuth } from '../../context/AuthContext';
import OrderCard from '../../components/OrderCard';

const api_url = `http://localhost:7000`

const OrderDashboard: React.FC = () => {
  
  // For Navigation to 'New Record' form
  // let navigate = useNavigate();
  let history = useHistory();
  const { user, group } = useAuth();

  // const [group, setGroup] = useState<string>('')
  const [loading, setLoading] = useState<boolean>(true);

  // const { g_id } = useParams<RouteParams>()

  /** API handlers */
  
  const g_id = group ? group.id : ''
  // const g_id = 521 // Gondor
  
  const grp_orders_endpoint = g_id 
    ? `api/orders/recent?group_id=${g_id}`
    : `api/orders/recent`
  console.log (`grp_orders_endpoint: ${grp_orders_endpoint}`)
  
  // Query to get list of records 
  // let records_rsp = _queryget('records', api_url, `api/records/${group ? group.id : ''}`, true, {enabled: true})
  // const records_rsp = _queryget('records', api_url, `api/records/${g_id}`, false, {enabled: true})
  const records_rsp = _queryget('orders', api_url, grp_orders_endpoint, true, {enabled: true})
  
  // Records array
  const records = records_rsp.data ? records_rsp.data.pedidos : []
  
  useEffect(() => {
    if (user && group){
        setLoading(false)
    }
  }, [user, group])
  
    
  /** Handlers */
  // Records Button Click 
  const handleFetchClick = () => {
    records_rsp.refetch()
  }  
  
  const handleClickPedir = (e, id) => {
    e.preventDefault()
    console.log(`handleClickPedir: ${id}`)
    history.push (`/order/${id}`)
  }
  
  /** Part renders */
  const recordsCards = records && records.map((record, index) => {
    return (
      
      <OrderCard 
        key={record.pedido.id}
        record={record} 
        onClick={(e) => handleClickPedir (e, record.pedido.id)} 
    ></OrderCard>
    )
  });

  return (
    <IonPage>
      <Header></Header>
      { !loading || !user || !group ? (
      <IonContent fullscreen>
        <IonGrid style={{margin: '0 5%', paddingLeft: '4em', paddingRight: '4em', paddingTop:'4em'}}>
          <IonRow>
            <div style={{fontSize: '2em', paddingBottom:'0.5em'}}>
                Pedidos abiertos
            </div>
            </IonRow>
          <IonRow>
            <div style={{fontSize: '1em', paddingBottom:'0.5em'}}>
              Hay 5 pedidos abiertos
            </div>
          </IonRow>
          {/* <IonItem>
            <IonButton onClick = {handleFetchClick}>Fetch records</IonButton>
            <IonButton color="success" onClick = {handleNewClick}>new</IonButton>
          </IonItem> */}

            <IonRow>
            <IonCol size='9' style={{padding: '0'}}>
              <IonRow>
                <div style={{fontSize: '1.5em', paddingTop:'0.5em', paddingBottom:'0.5em'}}>
                  <strong>Últimos pedidos</strong>
                </div>
              </IonRow>
              <IonRow style={{padding: '0'}}>
              {recordsCards ? recordsCards : handleFetchClick}
              </IonRow>
            </IonCol>
            <IonCol style={{width: '40%'}}>
              <IonDatetime 
                presentation="date"
                // max="2027" max year to be able to select, if not, it's present year
                // highlightedDates={[
                  // {
                  //   date: '2023-01-05',
                  //   textColor: '#800080',
                  //   backgroundColor: '#ffc0cb',
                  // },]}
              ></IonDatetime>
              <div style ={{fontSize: '0.8em'}}>
              <div style={{
            borderRadius:'50%',
            backgroundColor: 'green',
            display:'inline-block',
            width: '0.8em',
            height: '0.8em'
          }}></div> APERTURA    <div style={{
            borderRadius:'50%',
            backgroundColor: 'blue',
            display:'inline-block',
            width: '0.8em',
            height: '0.8em'
          }}></div> CIERRE    <div style={{
            borderRadius:'50%',
            backgroundColor: 'purple',
            display:'inline-block',
            width: '0.8em',
            height: '0.8em'
          }}></div> REPARTO</div>
            </IonCol>
            </IonRow>
        </IonGrid>
      </IonContent>
          ) : (<IonContent fullscreen>
            <IonItem>Loading...</IonItem>
        </IonContent>)
    }
    </IonPage>
  );
};

export default OrderDashboard;
