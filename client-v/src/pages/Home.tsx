import React from 'react';
import { IonContent, IonPage, IonGrid, IonRow, IonCol, IonText } from '@ionic/react';
import Header from '../components/Header';
import { colorFill, informationCircle } from 'ionicons/icons';

import './Home.css'


const Home: React.FC = () => {
  return (
    <IonPage>
      <Header />
      <IonContent fullscreen>
        <IonGrid className="content-grid">
          <IonRow className="ion-justify-content-center ion-padding-top">
            <IonCol size="auto">
              <IonText color="secondary" className="ion-text-center text-responsive">
                HAS CARGADO LOS DATOS DE PRUEBA
              </IonText>
            </IonCol>
          </IonRow>

          <IonRow className="ion-padding-horizontal ion-padding-top">
            <IonCol size="12" size-md="8" offset-md="2" size-lg="6" offset-lg="3">
              <IonText>
                <p>Hola:</p>
                <p>Has cargado los datos de prueba.</p>
                <p>
                  Tienes varios usuarios: <strong>gandalf</strong>, <strong>frodo</strong>,
                  <strong>bilbo</strong>, <strong>aragorn</strong>, <strong>faramir</strong>,
                  <strong>sauron</strong>, etc., que son admins de varios grupos y redes.
                </p>
                <p>
                  Podéis probar los distintos roles, o usar <strong>"anillo"</strong>, un
                  usuario que no es miembro de ningún grupo pero es admin de todos.
                </p>
                <p>El password de todos los users es <strong>karakolas</strong>.</p>
                <p>Saludos</p>
              </IonText>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default Home;
