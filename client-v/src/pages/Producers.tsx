import { IonContent, IonHeader, IonLabel, IonPage, IonTitle, IonToolbar, IonButton, IonItem, IonList, IonText, IonGrid, IonRow, IonCol, IonIcon} from '@ionic/react';
import { addOutline, shareSocial } from 'ionicons/icons'; 

import { API_BASE_URL } from '../config'
import { _myget, _queryget, _mdelete } from '../adapters/api'
import Layout from '../components/Layout';
import { useParams } from 'react-router-dom';
import './Producers.css';

interface ProducersProps{
    isAdmin? : boolean;
}

const Producers: React.FC<ProducersProps> = ({isAdmin = false}) => {
 
    type RouteParams = {
        g_id : string
    }

    const { g_id } = useParams<RouteParams>()

    const display = true // used to debug _queryget calls
  
    // const producer_rsp = _queryget('producers', API_BASE_URL,`api/producers/${g_id}`, display)
    // const producers = producer_rsp.data ? producer_rsp.data.productores : []

    const producers = [
        {id: 1, nombre: 'Cerveza Veer', email:'cervezavegana@cervezaveer.com', telefono: '612345678 / 612345678', direccion:'Sevúlcor, Segovia', contacto:'Maria Pradas', pago:'Maria Pradas'},
        {id: 2, nombre: 'Ché Madrid Dulces y Salados', email:'', telefono: '612345678', direccion:'Calle de la Solana Blanca, 3, 28231 Rozas de Madrid (las), Madrid', contacto:'Casilda Pando', pago:'Casilda Pando'},
        {id: 3, nombre: 'Eco Trailla', email:'', telefono: '612345678', direccion:'', contacto:'Javier González', pago:'Javier González'},
        {id: 4, nombre: 'Huerta Pepines', email:'huertapepines@gmail.com', telefono: '612345678', direccion:'Valdetorres del Jarama', contacto:'Javier González', pago:'Javier González'},
        {id: 5, nombre: 'Miel Ramayal', email:'', telefono: '612345678', direccion:'El bierzo', contacto:'Miguel Benito', pago:'Miguel Benito'}
    ]

    const headersRow = (
        <IonRow  className="bottom_line_border">
            <IonCol className="hide_on_small" size='2'>Productor</IonCol>
            <IonCol className="hide_on_small" size='3'>Contacto</IonCol>
            <IonCol className="hide_on_small" size='2'>Unidades encargadas</IonCol>
            {/* Boton VerInfo */}
            <IonCol></IonCol>
            
            {/* Acciones admin */}
            {isAdmin ? (
            <>
             <IonCol></IonCol>
             <IonCol></IonCol>
             <IonCol></IonCol>
            </>
            ) : (<IonCol></IonCol>)}
        </IonRow>
    )
    
    const listProducers = producers.map(producer => (
        <IonRow className="bottom_line_border" key={producer.id}>
            <IonCol sizeXs="12" sizeSm='2'>{producer.nombre}</IonCol>
            <IonCol sizeXs="12" sizeSm='3'>
                <strong>Email</strong>: {producer.email ? producer.email : <i>Sin especificar</i>} <br />
                <strong>Teléfono:</strong> {producer.telefono ? producer.telefono : <i>Sin especificar</i>} <br />
                <strong>Dirección:</strong> {producer.direccion ? producer.direccion : <i>Sin especificar</i>}
            </IonCol>
            <IonCol sizeXs="12" sizeSm='2'>
                <strong>Contacto</strong>: {producer.contacto} <br />
                <strong>Pago</strong>: {producer.pago}
            </IonCol>
            <IonCol>
              <IonButton fill="outline"> Ver Info </IonButton>
            </IonCol>
            
            {/* Add Admin actions buttons*/}
            
            {isAdmin ? (
              <>
                <IonCol>
                    <IonButton fill="outline"> Editar datos</IonButton>
                </IonCol>
                <IonCol>
                    <IonButton> Editar productos</IonButton>
                </IonCol>
                <IonCol>
                    <IonButton fill="outline" shape="round">
                        <IonIcon slot="icon-only" icon={shareSocial}></IonIcon>
                    </IonButton>
                </IonCol>
              </>
                ) : (
                  <IonCol>
                    <IonButton> Ver Productos</IonButton>
                  </IonCol>
            )}
              
            
        </IonRow>
    ))

  return (
    <IonPage>
      <Layout>
        <IonContent fullscreen>
          <IonGrid className="grid_margin">
            <IonText>
              <h1>Productores</h1>
            </IonText>
            {/* Añadir un filtro */}
            <IonGrid>
              {headersRow}
              {listProducers}
              {isAdmin && (
                <IonRow>
                  <IonButton>
                    <IonIcon icon={addOutline} />
                    <IonText> Añadir productor</IonText>
                  </IonButton>
                </IonRow>
              )}
            </IonGrid>
          </IonGrid>
        </IonContent>
      </Layout>
    </IonPage>
  );
  
};


export default Producers;
