

import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonList, IonButton, IonItem, IonLabel, IonInput } from '@ionic/react';

import { useState } from "react";
import './Users.css';

import { _queryget, _mut } from '../adapters/api'

const api_url = `http://localhost:7000`

const Users: React.FC = () => {
  
  // Inits: Empty user list
  // const [users, setUsers] = useState([]);
  const [newUser, setNewUser] = useState(false); // new user "modal"
  
  // Query to users endpoint - disabled
  const users_base_url = `http://localhost:7000`
  const users_rsp = _queryget('users', users_base_url,`users/`, true, {enabled: false})
  
  const users = users_rsp.data ? users_rsp.data.rows : []
  
  // Reference to jokes queryKey (fired in 'Jokes' tab
  const { isLoading, isError, error, data, refetch } = _queryget('jokes',undefined,`joke/any?type=single`, true, {enabled: false})
  
  const order_mut = _mut('POST', 'orders', api_url, `pedidos`, true, {enabled: false})
  
  const joke_status = data?.id ? <p>OK : Joke id : {data?.id}</p> : null
  
  // Users Button Click 
  const handleFetchClick = () => {
    users_rsp.refetch()
  }  
  
  const new_rec_data = {
        "id_productor":459, 
        "fecha_activacion": "2024-09-13",
  }
  
  const handleNewClick = () => {
    // setNewUser(true)
    order_mut.mutate(new_rec_data)
  }  
  
  /** Part renders */
  const post_area = order_mut.isPending
    ? <div>Loading...</div>
    : <p> REady! </p>
  
  const listUsers = users.map(user => (
    <IonItem key = {user.id} >
      <IonLabel>
        <h1>{user.first_name} ({user.id})</h1>
        <p>{user.email}</p>
      </IonLabel>
    </IonItem>
  ));
  
  
  const newUserForm = newUser ? (
    <IonList>
       <IonItem>
        <IonInput label="Name" placeholder="Éowyn"></IonInput>
      </IonItem>

      <IonItem>
        <IonInput label="Email" type="email" placeholder="email@domain.com"></IonInput>
      </IonItem>
      
      <IonItem>
        <IonInput label="Pass" type="password" value="password"></IonInput>
      </IonItem>

      <IonItem>
        <IonButton color="success">Crear</IonButton>
      </IonItem>

      
    </IonList>
    ) : null
  
    
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>User List Enhanced</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Tab 2</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonItem>
          <IonButton onClick = {handleFetchClick}>Fetch users</IonButton>
          <IonButton color="success" onClick = {handleNewClick}>new (pedido)</IonButton>
        </IonItem>
        
        
        <IonItem>
          { joke_status }
        </IonItem>
        
        
        
        {newUserForm}
        
        <IonList>
          {listUsers}
        </IonList>
        
      </IonContent>
    </IonPage>
  );
};

export default Users;
