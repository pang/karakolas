import { IonContent, IonHeader, IonPage, IonTitle, IonText, IonToolbar, IonList, IonButton, IonItem, IonLabel, IonInput } from '@ionic/react';
import { useState }  from 'react';
import { useParams, useLocation} from 'react-router-dom'
 
import toast from 'react-hot-toast'

import KForm from '../components/KForm';
import { InputProps } from '../components/Input';

import { _queryget, _mut } from '../adapters/api'
const api_url = `http://localhost:7000`

export interface KFormProps {
  name?: string;
  endpoint: string;
  record_data?: Object;
  form_type?: string; // 'new' or 'edit'
}

const NewRecord: React.FC<KFormProps> = ({ 
  record_data,
  form_type, //: string, 
  endpoint, //: string, // '/pedidos'
}) => {


  // url params
  const { r_id } = useParams()
  console.log(`NewRecord :: form_type : ${form_type}  :: endpoint : ${endpoint} :: r_id : ${r_id}`) // debug
  
  // const params = useParams()
  // console.log(`Params ::`);console.log(params) // debug
  
  
  // new/edit labels
  let form_title, header_title, btn_text, loading_btn_text, api_method, api_endpoint
  if (form_type==='edit') {
    header_title = 'Edit Record'
    form_title = `[${r_id}] : Actualizar`
    btn_text='Actualizar'
    loading_btn_text='Actualizando...'
    api_method = 'PUT'
    api_endpoint = `${endpoint}${r_id}`
  }
  else {
    header_title = 'New Record'
    form_title = 'Nuevo'
    btn_text = 'Crear'
    loading_btn_text='Creando...'
    api_method = 'POST'
    api_endpoint = `${endpoint}`
  }
  
  // const [values, setValues] = useState(location_data)
  // console.log(`setting API...`)
  /** API handlers **/
  // GET - disabled - data might be available from useLocaction
  const record_to_edit = _queryget('records', api_url, `${endpoint}${r_id}`, false, {enabled: false})
  
  // POST/PUT/DELETE
  const display=true
  // Actions to perform on POST/PUT success
  const mutate_on_success = (rsp_data, vars, ctx) => {
    console.log(`mutate_on_success :: data :\n${JSON.stringify(rsp_data,null,2)}`)
    // Display response success message if any
    if (rsp_data.message) { toast.success(`${rsp_data.message}`, { position: 'bottom-center'} ) }
    // Go back to /newrecord
    // history.back();
    // Reset button state (just in case)
    setSubmitProps({btn_text:btn_text, enabled:true})
    
  }
  const record_mut = _mut(api_method, 'records', api_url, api_endpoint, display, {onSuccess: mutate_on_success})
  


  // State
  const [initialData, setInitialData] = useState()
  
  // Enable/disable button
  const [submitProps, setSubmitProps] = useState({
      enabled: true,
      btn_text: form_type === 'edit' ? 'Actualizar' : 'Crear'
  }); 

  
  /** Set/get initial Form data */
  // Get data passed along with history.push(...)
  const location_data = useLocation().state
  
  // New form
  if(form_type === 'new') {
    console.log(`New record form : no need for initial data`)
  }
  // Populate form data if passed through history location
  else if ( location_data && !initialData ) {
    console.debug(`Loaded location_data ::`); console.log(location_data) // debug
    setInitialData(location_data)
  }
  // Edit form, initial data set into state
  else if (initialData) {
        console.log(`initialData is ready! : `)
        console.log(initialData)
  }
  // Might need to /GET data
  else {
    console.debug(`GET query status : ${record_to_edit.status}`)
    
    if(record_to_edit.status === 'idle') {
        console.debug(`Initial data not found :: trying to GET`)
        record_to_edit.refetch()
    }
    else if(record_to_edit.status === 'loading') {
        console.log(`/GET still loading...`)
        // TODO - Show spinner
        return (<IonPage> Loading record {r_id}... </IonPage>)
    }
    else if(record_to_edit.status === 'success' && !initialData) {
        console.log(`/GET Data ready! setInitialData`)
        console.debug(record_to_edit.data?.data) //
        setInitialData(record_to_edit.data?.data[0])
    }
    // Else - error ?
    
  }
  
  /** Form definition : list of fields **/
  const form_fields: InputProps[] = [
    {
      label: "ID Productor",
      name: "id_productor",
      type: "number",
    },
    {
      label: "Info cierre",
      name: "info_cierre",
      type: "text",
      // type: "date",
    },
    {
      label: "Fecha activacion",
      name: "fecha_activacion",
      type: "date",
    },
    {
      label: "Fecha de reparto",
      name: "fecha_reparto",
      type: "date",
    },
  ]
  
  // Leftovers/samples
  const user_form_fields: InputProps[] = [
    {
      name: "fullName",
      label: "Full Name",
    },
    {
      name: "password",
      type: "password",
      component: <IonInput type="password" clearOnEdit={false} />,
      label: "Password",
    },
  ]
  
  const submitHandler = (data) => {
    setSubmitProps({btn_text:loading_btn_text, enabled:false})
    console.log(`${loading_btn_text} record with data:\n${JSON.stringify(data,null,2)}`)
    record_mut.mutate(data)
  }

  // Debug : check initial data
  console.debug(`initialData is here (about to render) :: `)
  console.debug(initialData)
  
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle> {header_title} : <IonText>{record_mut.status}</IonText></IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        
        <KForm 
          form_title = {form_title}
          kfields={form_fields}
          given_values={initialData}
          submitFn={submitHandler}
          submitEnabled={submitProps.enabled && (record_mut.status !== 'pending') }
          submitBtnText={submitProps.btn_text}
        />
      
      </IonContent>
    </IonPage>
  );
};

export default NewRecord;
