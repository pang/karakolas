import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonList, IonButton, IonItem, IonLabel, IonInput } from '@ionic/react';
import { useState } from "react";
import { useHistory } from "react-router-dom";
import { useNavigate } from "react-router-dom";

import { _queryget, _mut, _mdelete } from '../adapters/api'

const api_url = `http://localhost:7000`

const Records: React.FC = () => {
  
  // For Navigation to 'New Record' form
  // let navigate = useNavigate();
  let history = useHistory();

  
  // Query to get list of records 
  const records_rsp = _queryget('records', api_url, `pedidos/`, false, {enabled: true})
  
  // Records array
  const records = records_rsp.data ? records_rsp.data.data : []
  
  if (records_rsp.isSuccess) {
    // console.log(`data   :\n${JSON.stringify(records_rsp.data.data,null,2)}`)
  }
  
  // Delete mutation handler
  const delete_record = _mdelete('d_orders', api_url, `pedidos`, false)
  
  // Records Button Click 
  const handleFetchClick = () => {
    records_rsp.refetch()
  }  
  
  const new_rec_data = {
        "id_productor":459, 
        "fecha_activacion": "2024-09-13",
  }
  
  const handleNewClick = () => {
    
    console.log("Insert new!")
    console.log("Navegando a muerte")
    // navigate("/records/new");
    history.push("/records/new")
  }  
  
  const handleEdit = (e, record_id) => {
    // e.preventDefault() // Avoid href redirection
    console.log(`Trying to edit :  ${record_id}`)
    // Get data
    const clicked_record = records.filter(rec => {return rec.id === record_id})[0]
    // console.log(`clicked_record data ::\n${JSON.stringify(clicked_record)}`)
    const next_url = `/records/${record_id}/edit`
    // history.push()
    // setState("cubierto":"tenedor")
    history.push(next_url, clicked_record)
    
    
  }  
  
  const handleDelete = (record_id) => {
    console.log(`Trying to delete :  ${record_id}`)
    delete_record.mutate(record_id)
  }  
  
  /** Part renders */
  const delete_btn_cnt = delete_record.isPending
    ? '...'
    : 'X'
  
  const listRecords = records && records.map(record => (
    <IonItem key = {record.id} >
      <IonLabel>
        <h1>{record.fecha_activacion} ({record.id})</h1>
        <p>{record.id_productor}</p>
      </IonLabel>
      {/* TODO : href={`records/${record.id}/edit`}  */}
      <IonButton color="success" onClick={(e) => handleEdit (e, record.id)}>E</IonButton>
      <IonButton color="danger" onClick={() => handleDelete (record.id)}>{delete_btn_cnt}</IonButton>
    </IonItem>
  ));
  
  
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Record List Enhanced</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Tab 2</IonTitle>
          </IonToolbar>
        </IonHeader>
        <IonItem>
          <IonButton onClick = {handleFetchClick}>Fetch records</IonButton>
          <IonButton color="success" onClick = {handleNewClick}>new</IonButton>
        </IonItem>
        
        <IonList>
          {listRecords}
        </IonList>
        
      </IonContent>
    </IonPage>
  );
};

export default Records;
