import { IonContent, IonPage, IonTitle, IonButton, IonItem, IonLabel, IonGrid, IonRow, IonCol } from '@ionic/react';
import { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";

import { _queryget, _mut, _mdelete } from '../adapters/api'
import Header from '../components/Header';
import { useAuth } from '../context/AuthContext';

const api_url = `http://localhost:7000`

const RecordsDashboard: React.FC = () => {
  
  // For Navigation to 'New Record' form
  // let navigate = useNavigate();
  let history = useHistory();
  const { user } = useAuth();
  const [group, setGroup] = useState<string>('')
  const [loading, setLoading] = useState<boolean>(true);



  type RouteParams = {
    g_id : string
  }

  const { g_id } = useParams<RouteParams>()
  // Query to get list of records
  const records_rsp = _queryget('records', api_url, `api/records/${g_id}`, false, {enabled: true})

  useEffect(() => {
    if (user){
        let usergroup = user.groups.find(group => group.id = g_id)
        if (!usergroup){
            history.push('/requests')
        }
        setGroup(usergroup)
        setLoading(false)
        // records_rsp.refetch()
    }
  }, [user])


  // Records array
  const records = records_rsp.data ? records_rsp.data.pedidos : []

  if (records_rsp.isSuccess) {
    console.log(`data   :\n${JSON.stringify(records_rsp.items,null,2)}`)
  }
  
  // Delete mutation handler
  const delete_record = _mdelete('d_orders', api_url, `pedidos`, false)
  
  // Records Button Click 
  const handleFetchClick = () => {
    records_rsp.refetch()
  }  
  
  // Mock
  const new_rec_data = {
        "id_productor":459, 
        "fecha_activacion": "2024-09-13",
  }
  
  const handleNewClick = () => {
    
    console.log("Insert new!")
    console.log("Navegando a muerte")
    // navigate("/records/new");
    history.push("/records/new")
  }  
  
  const handleEdit = (e, record_id) => {
    // e.preventDefault() // Avoid href redirection
    console.log(`Trying to edit :  ${record_id}`)
    // Get data
    const clicked_record = records.filter(rec => {return rec.id === record_id})[0]
    // console.log(`clicked_record data ::\n${JSON.stringify(clicked_record)}`)
    const next_url = `/records/${record_id}/edit`
    // history.push()
    // setState("cubierto":"tenedor")
    history.push(next_url, clicked_record)
    
    
  }  
  
  const handleDelete = (record_id) => {
    console.log(`Trying to delete :  ${record_id}`)
    delete_record.mutate(record_id)
  }  
  
  /** Part renders */
  const delete_btn_cnt = delete_record.isPending
    ? '...'
    : 'X'
  
  const listRecords = records && records.map(record => (
    <IonRow key = {record.pedido.id}>
        <IonCol>{record.pedido.fecha_reparto}</IonCol>
        <IonCol>{record.productor.nombre}</IonCol>
        <IonCol>{record.pedido.abierto ? 'Abierto' : 'Cerrado'}</IonCol>
        <IonCol>¿Estado?</IonCol>
        <IonCol>BOTONES ACCIONES</IonCol>
        <IonCol>BOTONES DOCUMENTOS</IonCol>
        <IonCol>
            <IonButton color="success" onClick={(e) => handleEdit (e, record.id)}>E</IonButton>
            <IonButton color="danger" onClick={() => handleDelete (record.id)}>{delete_btn_cnt}</IonButton>
        </IonCol>
    </IonRow>
  ));
  

  return (
    <IonPage>
        <Header></Header>
    { !loading ? (
      <IonContent fullscreen>
        <IonGrid style={{margin: '0 5%'}}>
        <IonItem>
            <IonTitle>Gestión de pedidos del grupo {group.nombre} </IonTitle>
        </IonItem>
        <IonItem>
          <IonButton onClick = {handleFetchClick}>Fetch records</IonButton>
          <IonButton color="success" onClick = {handleNewClick}>new</IonButton>
        </IonItem>
        <br/>
        <IonItem>
        <IonGrid>
            <IonRow>
                <IonCol>Fecha reparto</IonCol>
                <IonCol>Productor</IonCol>
                <IonCol>Estado pedido</IonCol>
                <IonCol></IonCol>
                <IonCol>Acciones</IonCol>
                <IonCol>Documentos y enlaces</IonCol>
                <IonCol></IonCol>
            </IonRow>
            { listRecords ? listRecords : handleFetchClick}
        </IonGrid>
        </IonItem>
        </IonGrid>
      </IonContent>
    ) : (<IonContent fullscreen>
        <IonItem>Loading...</IonItem>
    </IonContent>)
    }
    </IonPage>
  );
};

export default RecordsDashboard;
