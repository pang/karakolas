import {
  IonContent,
  IonPage,
  IonTitle,
  IonList,
  IonButton,
  IonItem,
  IonCard,
  IonCardTitle,
  IonCardContent,
  IonText,
  IonGrid,
  IonRow,
  IonCol,
  IonDatetime,
  IonIcon,
} from "@ionic/react";
import { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
// import { useNavigate } from "react-router-dom";
import moment from "moment";

import { _queryget, _mut } from "../adapters/api";

import { useAuth } from "../context/AuthContext";
import Layout from "../components/Layout";
import OrderCard from "../components/OrderCard";
import LoadingPage from "../components/common/LoadingPage";
import ErrorPage from "../components/common/ErrorPage";
import Alert from "../components/Alert";
import {
  closeOutline,
  cube,
  download,
  informationCircle,
  informationCircleOutline,
} from "ionicons/icons";
import "./MemberDashboard.css";

const api_url = `http://localhost:7000`;

const MemberDashboard: React.FC = () => {
  // For Navigation to 'New Record' form
  // let navigate = useNavigate();
  let history = useHistory();
  const { user, group } = useAuth(); // User and group info comes from context
  console.log(`Active user / group (from Auth) : ${JSON.stringify(user)} :: ${JSON.stringify(group)}`) // debug auth

  // const [group, setGroup] = useState<string>('')
  const [loading, setLoading] = useState<boolean>(true);

  // const { g_id } = useParams<RouteParams>()

  // ?? What do we need to do - remove loading after group is available
  useEffect(() => {
    if (user && group) {
      // let usergroup = user.groups.find(group => group.id = group)
      // if (!usergroup){
      //     history.push('/requests')
      // }
      // setGroup(usergroup)
      // What for ??
      console.log(`useEffect : user : ${JSON.stringify(user)}`);
      setLoading(false);
    }
    // })
  }, [user, group]);
  // */

  console.log(`Group after useEffect:`);
  console.log(group);

  const g_id = group ? group.id : "";
  
  const grp_orders_endpoint = g_id
    ? `api/orders/recent?group_id=${g_id}`
    : `api/orders/recent`;
  console.log(`grp_orders_endpoint: ${grp_orders_endpoint}`);

  /** API handlers */
  // Query to get list of open orders
  let display = false;
  const orders_rsp = _queryget(
    "orders",
    api_url,
    grp_orders_endpoint,
    display,
    { enabled: true } // Fire query on load
  );

  // orders array - all recent
  const orders_arr = orders_rsp.data ? orders_rsp.data.pedidos : []; // /orders/recent
  // const orders_arr = orders_rsp.data ? orders_rsp.data.items : [] // Rest API

  const query_status = orders_rsp.status;
  console.log(`query_status: ${query_status}`);

  if (orders_rsp.isLoading) {
    console.log("Still loading..!");
    return <LoadingPage />;
  }

  if (orders_rsp.isSuccess) {
    // REST API will be data.data?
    console.log(
      `Response data   :\n${JSON.stringify(orders_rsp.data, null, 2)}`
    );
    console.log(`Backend returned [${orders_arr.length}] results`);
  }

  let error_message;
  // Handle API errors
  if (query_status === "error") {
    console.log(`API error`);
    // TODO - Properly get other error data - should be handled up above
    if (orders_rsp.data?.error) {
      console.log(`Error  message :${orders_rsp.data.error}`);
      error_message = orders_rsp.data.error;
    }
    return <ErrorPage error_message={error_message} />;
  }

  /** Event handlers */
  // Refetch Button Click
  const handleFetchClick = () => {
    orders_rsp.refetch();
  };

  // TODO - Move to some utils
  const goToOrder = (e, id) => {
    e.preventDefault();
    console.log(`goToOrder: ${id}`);
    history.push(`/order/${id}`);
  };

  // TODO
  const downloadHojaReparto = (id) => {
    console.log(`downloadHojaReparto: ${id}`);
  };

  
  /** Part renders */
  // Check for orders closing soon
  //   or having distribution date in the next N days
  const close_soon_arr = [];
  const next_distrib_arr = [];
  let closest_date;
  let msg_reparto = <>No hay repartos en los próximos días.</>;
  let btns_reparto = null;
  let msg_closing = <>No hay cierre de pedidos en los próximos días.</>;
  let next_dist; // object
  let next_closing; // object
  let next_closing_btn_props = {}
  
  // let 
  
  // Traverse recent orders - get closing soon and next tasks
  orders_arr.forEach((o) => {
    // console.log(`Order: ${JSON.stringify(o)})`) // debug full info
    let fecha_reparto = o.pedido?.fecha_reparto;
    let info_cierre = o.pedido?.info_cierre;
    let fecha_cierre;
    let d_cierre = "";

    let abierto = o.pedido.abierto ? "Abierto" : "Cerrado";

    // Try to parse fecha_cierre
    if (o.pedido.abierto) {
      // fecha_cierre = moment(fecha_reparto, 'YYYY-MM-DD').format('DD MMM')
      fecha_cierre = moment(info_cierre, "YYYY-MM-DD");
      d_cierre = `: cierre : ${fecha_cierre?.format("DD MMM")}`;
    }

    // console.log(`${o.pedido?.id} : ${o.productor.nombre} : ${fecha_reparto} [${abierto}] ${d_cierre}` ); // trace each

    // Check if fecha_reparto is within the next 7 days
    if (
      moment(fecha_reparto).isBefore(moment().add(7, "days")) &&
      moment(fecha_reparto).isAfter(moment().subtract(1, "days"))
    ) {
      let reparto_from_now = moment(fecha_reparto, "YYYY-MM-DD").fromNow();
      console.log(`${o.productor.nombre} : ${fecha_reparto} : distribution is soon! : ${reparto_from_now}`);

      next_distrib_arr.push(o);
      if (
        next_dist === undefined ||
        next_dist?.reparto_from_now > reparto_from_now
      ) {
        o.reparto_from_now = reparto_from_now;
        o.disp_date = moment(fecha_reparto).format("DD/MM");
        console.log(
          `Next dist! ${fecha_reparto} : distribution ${reparto_from_now}`
        );
        next_dist = o; // update
      }
    }
    // ? 'previo' : 'posterior'

    // Check if fecha_cierre is within the next 7 days
    if (
      fecha_cierre !== undefined &&
      fecha_cierre.isBefore(moment().add(7, "days")) &&
      fecha_cierre.isAfter(moment().subtract(1, "days"))
    ) {
      let closing_from_now = moment(fecha_cierre, "YYYY-MM-DD").fromNow();
      console.log(`${o.productor.nombre} : ${fecha_cierre.format("DD MMM")} : Closing soon! : ${fecha_cierre.fromNow()}`);

      close_soon_arr.push(o);
      if (
        next_closing === undefined ||
        next_closing?.closing_from_now > closing_from_now
      ) {
        o.closing_from_now = closing_from_now;
        o.disp_close_date = moment(fecha_cierre).format("DD/MM");
        console.log(`Closest! ${fecha_cierre} : closing  ${closing_from_now}`);
        next_closing = o; // update
      }
    }

    // console.log (`Seven: ${f_seven}`)
  });

  // Build message {msg_reparto}
  if (next_closing) {
    
    console.log(`Closing soon! : ${JSON.stringify(next_closing)}`)
    const open_order_id = next_closing.pedido.id
    
    msg_closing = <>El pedido de "{next_closing.productor?.nombre}" cierra el <strong>{next_closing.disp_close_date}</strong>.</> // mock
    // TODO:
    // msg_closing = <>Quedan <strong>2 días</strong> para el cierre del pedido de "Quesos Zarandiel"</> // mock
    
    next_closing_btn_props = {
      actionButtonText: 'Pedir',
      href:`/order/${open_order_id}` , 
      onAction: (e) => goToOrder(e, open_order_id)
    }
  }
   
  if (next_dist) {
    moment.locale("es");
    // const disp_date =
    msg_reparto = (
      <>
        Hay reparto de <strong>{next_dist.productor.nombre}</strong> el{" "}
        <strong>{next_dist.disp_date}</strong>
      </>
    );
    // TODO - Specific "Hoy" / "Mañana"
    console.log(
      `next_dist is ${next_dist.pedido?.id} : ${next_dist.reparto_from_now}`
    );
    const o_id = next_dist.pedido?.id;

    btns_reparto = (
      <div>
        <IonButton className="reparto_download_icon" fill="clear">
          <IonIcon icon={download} />
          Hoja reparto
        </IonButton>
        <IonButton
          fill="clear"
          className="reparto_recuento_button"
          href={`/order/${o_id}`}
          onClick={(e) => goToOrder(e, o_id)}
        >
          Hacer recuento de pedido
        </IonButton>
      </div>
    );
  }

  // "Hay reparto del grupo XX el día DD"
  const alert_soon = (
    <Alert
    className="alert_container"
      alertType="reparto"
      title="Repartos"
      message={msg_reparto}
      {...btns_reparto}
    />
  );
   
  // "Quedan X días para el cierre del pedido A"
  const alert_next_closing = (
    <Alert
      className="alert_container"
      alertType="info"
      message={msg_closing}
      {...next_closing_btn_props}
      showClose
    />
  )

  // Cards con los pedidos abiertos
  const ordersCards =
    orders_arr &&
    orders_arr.slice(0, 3).map((record, index) => {
      // console.log(`Rendering ${JSON.stringify(record)}`)
      const id_pedido = record.pedido.id;
      console.log(`Key: ${record.pedido.id}`);
      const url_to = `/order/${id_pedido}`;
      return (
        <OrderCard
          key={id_pedido ? id_pedido : index}
          actionType="PEDIR"
          record={record}
          onClick={(e) => goToOrder(e, id_pedido)}
        ></OrderCard>
      );
    });
  
  /** render return **/
  return (
    <IonPage>
      <Layout>
        <IonContent fullscreen>
          <IonGrid className="dashboard_grid">
            <IonRow>
              <div className="dashboard_hello">
                ¡Hola, {user && user.username}!
              </div>
            </IonRow>
            <IonRow>
              <div className="dashboard_group">
                Estás en el grupo{" "}
                <strong>"{group ? group.nombre : "ERROR"}"</strong>{" "}
                <IonIcon icon={informationCircleOutline} />
              </div>
            </IonRow>

            <IonRow>
              <IonCol size="12" sizeMd="8" sizeLg="9" className="no_padding">
                <IonRow>
                  {alert_soon}
                  {alert_next_closing}
                </IonRow>
                <IonRow>
                  <div className="ultimos_pedidos">
                    <strong>Últimos pedidos</strong>
                  </div>
                </IonRow>

                <IonRow className="no_padding">
                  {ordersCards ? ordersCards : <IonText>Loading..</IonText>}
                </IonRow>
              </IonCol>
              <IonCol size="12" sizeMd="4" sizeLg="3">
                <IonDatetime
                  presentation="date"
                  // max="2027" max year to be able to select, if not, it's present year
                  // highlightedDates={[
                  // {
                  //   date: '2023-01-05',
                  //   textColor: '#800080',
                  //   backgroundColor: '#ffc0cb',
                  // },]}
                ></IonDatetime>
                <div className="calendar_bottom_text">
                  <div className="calendar_legend_circle apertura"></div>{" "}
                  APERTURA <div className="calendar_legend_circle cierre"></div>{" "}
                  CIERRE <div className="calendar_legend_circle reparto"></div>{" "}
                  REPARTO
                </div>
              </IonCol>
            </IonRow>
          </IonGrid>
        </IonContent>
      </Layout>
    </IonPage>
  );
};

export default MemberDashboard;
