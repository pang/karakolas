import React from 'react';
import { createRoot } from 'react-dom/client';
import { QueryClient, QueryClientProvider, QueryCache } from 'react-query'
import { AuthProvider } from './context/AuthContext'
import toast, { Toaster } from 'react-hot-toast'

import App from './App';

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false, 
      refetchOnReconnect: true 
    }
  },
  queryCache: new QueryCache({
    onError: (error, query) => {
      console.log(`Handling error in main.tsx: ${error.message}`);
      // Only show error toasts for failed background updates
      if (query.state?.data !== undefined) {
        console.log(`Showing toast from main`);
        console.log(`query.state.data is something:\n${JSON.stringify(query.state.data,null,2)}`);
        toast.error(`Something went wrong: ${error.message}`, {position: 'bottom'})
      }
      else {
        console.log(`[main.tsx] query.state.data is undefined`)
        // console.log(`${JSON.stringify(query.state)}`);
        
      }
    },
  }),
}) 

const container = document.getElementById('root');
const root = createRoot(container!);
root.render(
  <React.StrictMode>
    <QueryClientProvider client={queryClient}>
      <AuthProvider>
        <App />
      </AuthProvider>
      <Toaster/>
    </QueryClientProvider>
  </React.StrictMode>
);