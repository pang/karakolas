/** Utils to handle API calls to a backend 

Exported as an async fn

Can be used with:

import { getAPI } from "../services/api";

const api_resp = await getAPI( api_url );

Generic:

Header list:
- Request 'application/json'

Handles/responses:
- Network error: 

- Server error:

- Data error
    - wrong json/array
    - will happen (and should be handled) in the component calling getAPI()


*/

import { API_BASE_URL } from '../config'

export const getAPI = async ( url ) => {
  
  console.log (`getAPI : url : ${url}`)
  
  const headerList = {
      "Content-Type": "application/json",
      // Authorization: `Client-ID ${ACCESS_KEY}`,
  };
  
  try {
    
    const response = await fetch(url, {
      headers: headerList,
      credentials: 'include'
    });
    
    // TODO - Use better : parse response.body
    console.log('response.status: ', response.status);
    // console.log (`getAPI () : response : ${JSON.stringify(response)}`)
    const api_resp = await response.json();
    console.log (`getAPI () : response.json : ${api_resp}`)
    response.error = false;
    // return response;
    return api_resp;
    
  } catch (err) {
    
    // TODO - Toast should be shown here
    console.log (`getAPI err : ${url} : ${err}`)
    console.log (err)
    
    const api_resp = {}
    api_resp.error = true;
    api_resp.error_obj = err;
    api_resp.error_msg = err;
    
    return api_resp
  }
};
