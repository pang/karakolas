import { Redirect, Route } from 'react-router-dom';
import {
  IonApp,
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs,
  setupIonicReact
} from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { ellipse, square, triangle, happy, peopleOutline, keyOutline } from 'ionicons/icons';
import Tab1 from './pages/Tab1';
import Users from './pages/Users';
import Records_old from './pages/Records_old';
// import Records from './pages/Records';
import RecordsDashboard from './pages/RecordsDashboard';
import Tab3 from './pages/Tab3';
import Requests from './pages/Requests';
import NewRecord from './pages/NewRecord';
import Login from './pages/Login';
import Producers from './pages/Producers';
import Home from './pages/Home';
import ProtectedRoute from './components/ProtectedRoute';
import Groups from './pages/Groups';
import MemberDashboard from './pages/MemberDashboard';

import { useAuth } from './context/AuthContext';
/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';


/* Theme variables */
import './theme/variables.css';
import OrderDashboard from './pages/orders/OrderDashboard';
import Order from './pages/orders/Order';
import ShoppingBasket from './pages/orders/ShoppingBasket';
import OrderConfirmation from './pages/orders/OrderConfirmation';
import AdminDashboard from './pages/AdminDashboard';

/* Global css */
import './theme/global.css';

setupIonicReact();

const App: React.FC = () => {

  const { user } = useAuth()

  return (
    <IonApp>
      <IonReactRouter>
        <IonRouterOutlet>
          <Route exact path="/index">
            <Home />
          </Route>
          <Route exact path="/tab1">
            <Tab1 />
          </Route>
          <Route exact path="/users">
            <Users />
          </Route>
          <Route path="/tab3">
            <Tab3 />
          </Route>
          <Route path="/groups">
            <Groups/>
          </Route>

          <Route path="/home">
            <MemberDashboard/>
          </Route>

          <Route exact path="/order">
            <Order/>
          </Route>
          
          <Route exact path="/order/:o_id">
            <Order/>
          </Route>
          
          <Route exact path="/order/dashboard">
            <OrderDashboard/>
          </Route>
          
          <Route path="/order/shopbasket">
            <ShoppingBasket/>
          </Route>

          <Route path="/order/confirmation">
            <OrderConfirmation/>
          </Route>

          <Route exact path="/admin">
            <AdminDashboard/>
          </Route>

          <Route exact path="/admin/pedidos/:g_id">
            <RecordsDashboard />
          </Route>
          <Route exact path="/productores/:g_id">
            <Producers />
          </Route>
          <Route path="/admin/productores/:g_id">
            <Producers isAdmin={true} />
          </Route>

          {/* Samples area */}
          <Route exact path="/records">
            <Records_old />
          </Route>
          <Route path="/records/:r_id/edit">
            <NewRecord form_type="edit" endpoint="pedidos/" />
          </Route>
          <Route path="/records/new">
            <NewRecord form_type="new" endpoint="pedidos/" />
          </Route>

          <Route path="/requests">
            <Requests />
          </Route>
          <Route path="/login" exact>
            <Login/>
          </Route>
          {user ? (
            <Route exact path="/">
              <Redirect to="/groups" />
            </Route>
            ) : (
            <Route exact path="/">
              <Redirect to="/index" />
            </Route>)
          }
        </IonRouterOutlet>
      </IonReactRouter>
    </IonApp>
  );
}

export default App;
