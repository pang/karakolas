import { createContext, useContext, useState, ReactNode, useEffect } from 'react';
import { API_BASE_URL } from '../config'

interface AuthContextProps {
  // isAuthenticated : boolean;
  user: Record<string,any> | null;
  setUser: (username: string | null) => void;
  group: Record<string,any>  | null;
  setGroup: (group:  Record<string,any> | null) => void;
  login: ( username: string | null, password: string) => Promise<string | null>;
  logout: () => void;
  get_user_with_token: () => Promise<string | null>;
}

const AuthContext = createContext<AuthContextProps | undefined>(undefined);

interface AuthProviderProps {
  children: ReactNode;
}

export function AuthProvider({ children }: AuthProviderProps) {

  const [user, setUser] = useState<string | null>(() => {
    const storedUser = localStorage.getItem('user')
    return storedUser && storedUser !== 'undefined' ? JSON.parse(storedUser) : null
  });
  const [group, setGroup] = useState<string | null>(() => {
    const storedGroup = localStorage.getItem('group')
    return storedGroup && storedGroup !== 'undefined' ? JSON.parse(storedGroup) : null
  });

  const get_user_with_token = async () => {
    try {
      const user = await fetch(`${API_BASE_URL}/get_user`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        },
        credentials: 'include'
      });
      const data = await user.json()
      if (data) {
        setUser(data.user)
        localStorage.setItem('user', JSON.stringify(data.user))
        return data.user
      } else {
        setUser(null)
        localStorage.removeItem('user')
        localStorage.removeItem('group')
        return null
      }
    } catch (error){
      console.log(error)
    }
  }

  useEffect(() => {
    if (user === null){
      get_user_with_token()
    }
  })

  const login = async  (username: string | null, password: string) => {
    try {
      if (!username){
        throw new Error('Missing username')
      }
      const response = await fetch(`${API_BASE_URL}/api/login`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ username, password }),
        credentials: 'include'
      });
  
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
  
      const data = await response.json();
      if (data){
        setUser(data.user)
        localStorage.setItem('user', JSON.stringify(data.user))
        return username
      }
      return null
    } catch (error) {
      throw error;
    }
  }

  const logout = async () => {
    try {
      const response = await fetch(`${API_BASE_URL}/api/logout`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        credentials: 'include'
      });
  
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
  
      const data = await response.json();
      setUser(null)
      localStorage.removeItem('user')
      setGroup(null)
      localStorage.removeItem('group')
    } catch (error) {
      throw error;
    }
  }

  return (
    <AuthContext.Provider value={{ user, setUser, group, setGroup, login, logout, get_user_with_token }}>
      {children}
    </AuthContext.Provider>
  );
}

export function useAuth(): AuthContextProps {
  const context = useContext(AuthContext);
  if (context === undefined) {
    throw new Error('useAuth must be used within a AuthProvider');
  }
  return context;
}
