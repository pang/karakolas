# Client-v

_Scaffold for new frontend_

## Quick start

Just run:
```
docker compose -f ./devops/docker-compose-client-v.yml up
```

## Details
- Based in `ionic` framework
- Using react for this first prototype
- And `vite` to have a lighter dev environment

- For the moment is the basic `ionic`+`react`+`vite` sample

## API sample
- defined in `services/api.ts`
    - does a basic `GET`
    - `url` is passed as a parameter
- To use in a page:
    - hooks are useful:
        - `useState` - placeholder where the data will be kept
        - `useEffect` - place to do the first `GET`

## Next steps
- Upload docker images in dockerhub
- Do not create `noce_modules` outside the container
- Include 'loading' spinner for API calls
- Connect API calls to `py4web` backend

## Stories

Some notes on how we fixed or worked around the different issues we encountered

- [api - cors](../docs/historias/cors.md)
