#!make

WEB2PY_IMAGE ?= web2py-karakolas
PY4WEB_IMAGE ?= py4web-karakolas
CLIENT_V_IMAGE ?= librecoop/client-v-dev

SHELL := /bin/bash

ENV_FILE := ./devops/.env

ifneq ($(wildcard $(ENV_FILE)),)
	include $(ENV_FILE)
endif


help: ## 💬 This help message
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(firstword $(MAKEFILE_LIST)) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

deploy-test: set-env ## 🚀 Deploy test enviroment with docker containers
	mkdir -p w2p-karakolas/databases \
	&& mkdir -p py4web/uploads \
	&& mkdir -p py4web/private \
	&& cp w2p-karakolas/private/auth.key py4web/private/auth.key \
	&& cd devops \
	&& docker compose -f docker-compose-dev-all.yml up -d

drop-test: ## 💀 Drop test environment
	cd devops \
	&& docker compose -f docker-compose-dev-all.yml down

clean-test:  ## 🧹 Clean up test files (need sudo permission)
	sudo rm -rf devops/data/ \
	&& sudo rm -rf w2p-karakolas/databases \
	&& sudo rm -rf w2p-karakolas/errors

database-test: ## 💍 Set up test database
	docker exec -it -w /home/web2pyuser/web2py/applications/karakolas/private/testdata ${WEB2PY_IMAGE} ./import_database.py mysql://${MARIADB_USER}:${MARIADB_PASSWORD}@karakolasdb/${MARIADB_DATABASE} lotr_deep

image: ## 🔨 Build container images from Dockerfile 
	docker build -f devops/Dockerfile-client-v -t ${CLIENT_V_IMAGE} . \
	&& docker build -f devops/Dockerfile-py4web -t ${PY4WEB_IMAGE} . \
	&& docker build -f devops/Dockerfile-web2py -t ${WEB2PY_IMAGE} .

# push:  ## 📤 Push container image to registry 

# ============================================================================

set-env:
	@bash ./devops/tools/set_env.sh
