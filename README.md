# Introduction

This branch was made in order to migrate the application from web2py to py4web + ionic.

<div style="text-align:center;">
  <img src="img/old_karakolas.svg" alt="Old Karakolas">
  <p style="margin-top:10px;">Initial state</p>
</div>

<div style="text-align:center;">
  <img src="img/new_karakolas.svg" alt="New Karakolas">
  <p style="margin-top:10px;">Final state</p>
</div>

The main goal is to keep web2py for admin tasks and ionic + py4web for common users

# Setup

First of all you will need to create your own `.env` file in the folder `devops`. You have a sample named [`.env_sample`](./devops/.env_sample) with the following content:

```ini
MARIADB_USER = <db_user>
MARIADB_PASSWORD = <db_user_password>
MARIADB_DATABASE = <db_name>
MARIADB_ROOT_PASSWORD = <db_root_password>
MARIADB_LOCAL_PORT = <port>
USER_ID = <user_uid> # Use 'echo $UID' to check your UID
```

You must specify the parameters of your configuration.

To config the web2py application, you should change the file name [web2py/private/appconfig_sample.ini](web2py/private/appconfig_sample.ini) to `web2py/private/appconfig.ini` and modify the uri parameter. The file should look like this:

```ini
; configuracion de la base de datos
[db]
; sqlite (no recomendado)
;uri       = sqlite://storage.sqlite
;mariadb (o mysql)
uri    = mysql://<db_user>:<db_user_password>@<mariadb_ip>:3306/<db_name>
```

You should change the parameters to match the defined on the `.env` file. The `mariadb_ip` is the one defined on the docker-compose file when you started.

> Default `mariadb_ip` is 172.18.0.10

You will need to build the images for `web2py` and `py4web` the first time before deploying (or if you changed something from the Dockerfile).

```bash
make image
```

> If there's no folder named `uploads` under folder py4web you need to create it empty before deployment.

## Deployment

Once everything is configured, if you want to deploy all the components at once, run the following command:

```bash
make deploy-test
```

If you want to remove it:

```bash
make drop-test
```

### Deploy database test

There's a test database created (documented [here](./web2py/TEST_DATA_README)) for testing and you can deploy it with:

```bash
make database-test
```

If you want to clean the databases and the errors from web2py, run:

```bash
make clean-test
```

### Other

#### Check back / front logs
Just attach to docker containers

```bash
docker logs py4web-karakolas -f
```

